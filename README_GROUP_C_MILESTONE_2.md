

Milestone 2: Spawning Processes
===============================

# Paging

Our paging implementation is now able to serve mapping requests with frames of size multiple of 4 KiB. It returns a virtual address with the same size consisting of 4 KiB pages and dynamically tracks the total allocated region. So far our system only supports pages of size 4 KiB.
Additionally it is also able to serve mapping request with fixed addresses which is used for allocating the ELF file.

Initially the paging implementation considers the virtual address space above 1 GiB VA as unallocated (between 1 GiB and 2 GiB for the init process). The region below 1 GiB is reserved for the ELF file. For every mapping request the allocated region will be grown starting from 1 GiB towards higher addresses. Our mapping lazily creates ARM L2 pagetables upon every mapping request, i.e., the L2 pagetable is only created at the first request of the region this L2 pagetable would span.

One big challenge turned out to be how to stop recursive paging_map_fixed_attr invocations during the creation of an L2 pagetable. The critical situation could occur during arml2_alloc where the slot allocator could trigger a slab refill which again triggers a paging_map_fixed_attr. In the invocation of paging_map_fixed_attr of the slab allocator it then would map a slab to a non-existing L2 pagetable. We solved this by using our recursion break mechanism implemented in libmm.

Another feature is the passing of the child paging state from parent to child process. For this to happen we created two structs:
- paging_serialize_data where our base address for paging (1 GiB) and the end of the allocated region is stored as well as the offset and length information where the paging_serialize_l2pt_entry array is located.
- paging_serialize_l2pt_entry contains information which slots of the child L1 pagetable have been already allocated during the spawn process.
Both datastructures are written onto the arguments frame by the parent along with the argument and environment arrays. Finally this frame is then mapped to the virtual address space of the child. There the child process reads out the datastructures and constructs its paging state accordingly.


Limitations: so far our paging implementation is not able to:
- unmap frames given their virtual address.
- unmap frames within a paging region.
These features might be considered in future work.


# Spawning

In implementing the spawning of new processes, we were mainly guided by the book and the source code in upstream Barrelfish, file ```lib/spawndomain/spawn.c```. Most of the needed process setup -- like setting up the child's CSpace and VSpace, loading and mapping the ELF binary into the child as well as setting up and invoking the dispatcher -- could be devised with help from these resources.

Additionally to the core spawning functionality, we are able to pass arguments as well as a serialized paging state to the child process. We also worked out a way to kill a process (without cleaning up) via backported Barrelfish functions of usr/monitor (```monitor_delete_last()``` & dependencies) and implemented this feature, and process invocation and listing in a domain manager. It allows to start, observe and optionally kill all processes running in the system.

We observed a limitation of 30 processes in total that could be spawned. This limitation is due to ~30 MiB of memory needed for a process that never gets freed afterwards due to the lack of cleaning up. We thought about how to implement the clean up of processes and allocated resources, but realized that the functionality to successfully revoke a capability is missing in the current setup without message passing and without monitor.

We demonstrate the working of our solution via several tests in ```lib/testing/t02_process_creation.c```. They can be started easily from our console testing menu.

Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
