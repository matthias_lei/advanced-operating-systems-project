

Milestone 1: Memory manager
===========================

Our memory manager works robustly for all the requirements of milestone 1.
We are implementing a buddy memory allocator. The version of this commit
already performs the splits but not yet the coalescing part during the
free operation. This part is currently in testing phase and will be part
of the next milestone.

The provided memory region is split into the largest possible blocks that allow
for all blocks the alignment to be at least as large as their size. With the
provided ~ 1 GiB region on the board, sizes from 4 KiB, 8 KiB, ... , 512 MiB
are initially made available. If more smaller blocks are needed, larger blocks
are split into 2 equally sized smaller blocks. This function is used recursively.
Thus, large blocks can be kept as large as possible for as long as possible.

Free capabilities are tracked with lists of mmnodes; one list per size. This
allows lookup for free capabilities of adequate size in O(1). The recursive
splitting of larger blocks to smaller is amortized O(1), too.

Refill operations of slot and slab allocators need RAM capabilites to retype
RAM to L2CNodes in CSpace for slots, and to frames mapped to virtual addresses
in VSpace for slabs. Therefore, they call mm_alloc_aligned(). During this
indirect recursion, our internal splitting operation, which would need again
mmnodes from slabs allocator and free slots, must **not** start.
Thus, we have established an invariant for the memory allocator that assures
a minimum reserve of RAM capabilities in proper size after each allocation
to warrant these refill operations to complete without any need to split
capabilities during this critical phase of execution.

Allocated memory is tracked. Double frees prevented.

Limitations based on our design choices at the moment:
- for all allocations, requirement alignment <= size;
  but all allocated RAM capabilities are aligned at least to their size.
  Alignment may be better than requested size, but it is not guarenteed.
- internal fragmentation based on capability sizes limited to
  multiples of 2, e.g. 4 KiB, 8 KiB, 16 KiB, ...,
- external fragmentation: on mm_free(), no merging operation is implemented
  yet in the code of this commit for freed neighboring memory blocks.

However, a newer version of the memory manager is already in the testing phase
that will implement also the coalescing part of a buddy memory allocator. This
version will include freeing of unused capabilities (after merge) and tracking
of these free slots to allow reusing them, a functionality not provided by the
slot allocator. It will reduce the problem of external fragmentation and will
be used in the next milestone. Lookup of mmnodes based on base address will
be in O(1) then by using a lookup table; currently O(n) for the number of
allocated RAM capabilities of the same size.

We have test cases for specific corner cases, allocating numbers of capabilities
that trigger refills of slot allocator (new L2CNode, 256 slots) and slab allocator
for mmnodes (currently 102 mmnodes / 4 KiB page), and also an infinitely running
alloc / free test to illustrate no leakage. We have also a test case that shows
mapping the same frame to two different pages. Our own testing framework
(see lib/testing, linked to init) allows interactive execution of the tests.
For this purpose, we adjusted the dummy_terminal_read() function in lib/aos/init.c
to allow simple inputs from the terminal.

During the work on milestone 1, we have detected 2 bugs, one in Barrelfish when
trying to allocate more than 10'000 capabilities and one in the printf() function,
which we will report separately with more detailed information.
There are 2 test cases here that illustrate the bugs.


Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
