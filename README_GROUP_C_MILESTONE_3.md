
Milestone 3: Remote Procedure Calls & Lightweight Message Passing
=================================================================

# Distributed system

In this milestone, a typical client / server infrastructure is built to offer key
services to all domains running on Barrelfish. The functionalities are available
to all domains (clients) by remote procedure calls (RPC). Stubs in libaos map the
function calls to a communication protocol currently based on Lightweight Message
Passing (LMP).

At the moment, all services are offered by a global event handler (server) running
in ```init```. Each dispatcher endpoint maintains a direct communication channel
with this monolithic service provider. Internally, the servers are already split
into specific function groups (services).

We are close to spawn them into separate processes, where each dispatcher will
maintain separate channels to such services (as needed). They are established on
demand upon the first RPC call of the associated service using a name service that
offers service registration and lookups.


# Message Passing Architecture

## Lightweight Message Passing (LMP)
The provided LMP library was used to establish channels between dispatcher endpoints.
They allow sending a limited amount of data (using registers) and a capability in
these channels. On ARMv7, 9 words (36 bytes) can be sent. Send and receive operations
are non-blocking, i.e. the actual operations are written in an event-triggered
approach. Specific event handlers are registered in the dispatcher that handle the
actual data processing.

## Our message protocol
LMP messages are used to transfer meta data (header, 2 words): service type, function ID,
message size. For short message payloads (currenlty <= (9-2) * 4 = 28 bytes), this
payload is also embedded directly into the message. For larger messages, a frame
is allocated by the sender, mapped, data copied from initial buffer to this mapped
address, the page unmapped, and the frame capability sent to receiver using LMP,
where it is mapped into the virtual address space of the receiver. The payload can
be used directly from this frame (zero-copy). By convention of our protocol, the
recipient keeps the frame for any later use (e.g. in another message) or later free.
Each message from a client is replied by a message by the server.
A success/error code is embedded in the metadata.

## Remote Procedure Calls (RPC)
The given RPC interface was implemented as specified. The regular user program
can use them without configuration. We are planning on adding some extensions
in the future.


# Services

## Common service
```sendNumber()``` and ```sendString()``` are offered in all services, mainly also
for testing purpose.

## Name service - Gaia
Offers registration and lookup for all the other services mentioned below. This
is one key element for the distributed services system. Not yet complete.
Thus, all services are running in init at the moment.

## Memory service - Dionysos
Dionysos provides RAM capabilities. Internally, it uses our Memory Manager (MM)
implementation linked to the service. Currently, one function is offered:
RAM allocation based on requested size and alignment.

Technical note: The RPC implementation must be reentrant safe. While an RPC call
is being processed, internally additional calls (e.g. for RAM capability allocation
during slot allocator refill) can be triggered before the first call even can finish.
We use the stack to protect status information of already started RPC from being
overwritten during these indirect recursive function calls.

## Process service - Demeter
Using the provided RPC functions, programs provided as boot modules can be spawned,
and the current status of this domain management can be controlled (lookup of list
of domain IDs and names for each of the IDs). It is also crucial for launching
the other services. This service embeds functionality of spawn and domain manager
from earlier milestones.

## Serial I/O service - Hermes
Hermes handles output to and input from the terminal via stdio functions. The glue
code in ```libc``` redirects the basic stdio functions to the RPC implementations
for all domains except ```init```. Hermes currently offers a line buffer per client
to reduce interleavings by concurrently running threads. While lines may be printed
in interleaving patterns, each line is completely from one client without cross-
contamination by characters of other threads. Terminal input is currently not handled
specially. A ```getchar()``` from any domain may read from the terminal at any time,
and the user cannot distinguish at the moment which domain is listening.

However, an implementation of a simple windowing system is on the way. It buffers
entire pages (rolling) for each client, and only displays the content of the
currently active domain (focus). Then it is also clear to the user, that this
program receives the input. Other ```getchar()``` requests are blocked until
their window reeives the focus. Upon switch, the recent buffer content is replayed
to have consistent views for each program.

## Device service - Hades
Establishes the connection with the hardware. Not yet implemented


# Additional binaries

## Domain manager - Zeus
Basically "just" a user interface for the process service. It has the potential
to evolve into a shell in the future.

## Test suite - Artemis
This program offers our established modular test suite system.

## Various test programs
In particular ```memeater```.


# Milestone
Our current implementation runs all described RPC functionalities. The provided
test program ```memeater``` runs and successfully performs all tasks.


# Usage
After booting, our low-level test suite allows launching of some tests from earlier
milestones that are not (yet) available in Artemis. Upon exit of this menu (use #),
the services are initialized and the event handler loop starts listening to
incoming requests. Also the test program Artemis is launched tha offers
various tests, including launching of Zeus or memeater.


Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
