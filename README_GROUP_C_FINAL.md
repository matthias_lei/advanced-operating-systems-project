
Final Milestone: Welcome to Barrelfish OlympOS
==============================================

This final version here matches exactly the version that was shown today.
It boots directly into our shell Zeus. The default picocom setting and network
settings can be used as mentioned in the book.

The shell offers a help function that gives additional information on the commands.
More details about usage, in particular of the specific projects, are described
in the report.

Additionally, we refer to the README files of our former milestones.

Please feel free to ask if you have any questions.


Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
