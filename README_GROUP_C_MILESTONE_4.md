
Milestone 4: Page fault handling / Self paging
==============================================

# Organization VSpace using paging regions
The VSpace of each domain is organized with this scheme:
- reserved [0,1) GiB: sections of the ELF binary (r/w and r/x rights as indicated by ELF)
- managed [1,2) GiB: we allow 768 MiB for the heap and 256 MiB for other regions like stacks
- restricted kernel space [2,4) GiB: not accessible from user space

Paging regions are used to keep various regions in the managed VSpace organized.
A large region of 768 MiB is granted to morecore to serve malloc() with more
virtual memory when required. Other requests -- like stacks or some slab allocators --
are using separate regions in the remaining 256 MiB of the managed VSpace.

Each region has a guard page at the bottom that will never be mapped and is
recognized while handling page faults. This is in particular useful for stacks,
where we can detect stack overflows.

Each region could additionally have an upper guard page. But at the moment, this option
is not active. With the bottom guards, there is already one guard page between adjacent
regions.


# Page fault handler
We have implemented a page fault handler as indicated. Each thread gets its own
stack (currently 64 KiB), exception stack (currently 16 KiB) and exception
handler. Paging regions are kept in a list for lookup during page fault handling.
Specific exception types -- such as null pointer exceptions
or right access violations for mapped but e.g. not writable pages -- are
recognized and indicated.


# Lazy memory allocation
The self-paging mechanism allows a lazy memory allocation. While memory
allocated e.g. on the heap can be several MiB large, only actually accessed
parts are backed by physical frames on demand. RAM capabilities are requested
via RPC from our memory manager Dionysos.

In addition to the heap, also stacks and some types of slab allocators
(when using paging regions) are allocated lazily. While regions
are also used for exception stacks, an actual mapping with enough frames
is triggered manually before registration of the thread's event handler,
since no additional exceptions should be triggered by the same thread while
handling an exception. Regions are still convenient since they
provide the guards as a built-in feature. To solve the bootstrap problem,
the first exception stack of each domain is using a buffer that is 
statically allocated within the BSS section of the ELF binary.


# Observations
Thanks to self paging and lazy memory allocation, much fewer RAM capabilities
are used compared to our earlier implementations. This allows more programs
to be launched (still no cleanup/revoke procedure). However, as a side effect,
it takes longer until a launched program is actually running.


# Some binaries

## Domain manager - Zeus
Basically "just" a user interface for the process service Demeter. It has the potential
to evolve into a shell in the future.

## Test suite - Artemis
This program offers our established modular test suite system.

## Various test programs
In particular ```memeater```, ```stackeater```, ```threads``` and a collection of
test programs triggering specific exceptions.


# Milestone
Our current implementation runs all described RPC functionalities. The provided
test program ```memeater``` runs and successfully performs all tasks.


# Usage
After booting, our low-level test suite allows launching some tests from earlier
milestones that are not (yet) available in Artemis. Upon exit of this menu (use #),
the services are initialized and the event handler loop starts listening to
incoming requests. Also the test program Artemis is launched that offers
various tests, as well as Zeus, our command line interface for the process server.

## Arguments passing from modified usbboot to init domain
We have implemented a mechanism to pass arguments from the host to our Barrelfish
instance without the need for recompilation or modification of the final binary on
the disk that is used for transmission to the Pandaboard.
Usage with the patched usbboot: ```tools/bin/usbboot <image> --initargs <args...>```.
To achieve this, we allocate a static buffer of 4 KiB in ```init``` and fill it with
a (hopefully unique) pattern (0xcafecafe). Before loading the image to the board in
```usbboot```, we search the binary image for this pattern and simply patch in the
provided command line arguments.


# Testing
As in earlier milestones, one key observation was: if the implementation works, one does not
see much on first glance. It just works as before. :-) However, using debug outputs we could
observe what is all going on during self paging including memory allocation via RPC, of course.
On the other hand, tracking down a problem can be a challenge.


Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
