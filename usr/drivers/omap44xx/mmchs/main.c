/**
 * \file
 * \brief MMCHS Driver main routine.
 */
/*
 * Copyright (c) 2013, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <stdio.h>
#include <aos/aos.h>

#include <aos/connect/all.h>
#include <aos_rpc_server/aos_rpc_server_blockdev.h>
#include <nameserver_types.h>

#include <mmchs/mmchs.h>

#define NAME "Electra MMCHS Driver Service"

static void init_service(void)
{
    errval_t err = SYS_ERR_OK;

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/dev/blockdev"
    #define SHORT_NAME "blockdev"
    #define DESCRIPTION "MMCHS driver service"
    #define PATRON "Electra"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        USER_PANIC_ERR(err, "create_kv_store() failed");
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct aos_rpc_request_queue blockdev_req_queue;
    aos_rpc_request_queue_init(&blockdev_req_queue);
    aos_rpc_set_my_request_queue(&blockdev_req_queue);

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_BLOCKDEV, true,
                                   store, aos_rpc_blockdev_server_event_handler, &reply);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "register_service() failed");
    }

    // for local blockdev needs:
    struct aos_rpc blockdev_rpc;
    init_local_channel(&blockdev_rpc, AOS_RPC_INTERFACE_BLOCKDEV, &aos_rpc_blockdev_local_vtable);
    set_blockdev_rpc(&blockdev_rpc);

    // enter the event dispatching loop
    struct waitset *default_ws = get_default_waitset();
    while (true) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }
}


int main(int argc, char **argv)
{
    cm2_init();
    ti_twl6030_init();
    ctrlmod_init();
    cm2_enable_hsmmc1();
    sdmmc1_enable_power();

    errval_t err = mmchs_init();
    if (err_is_fail(err)) {
        debug_printf("mmchs_init failed. Do you have an SD card attached?\n");
    }

    init_service();

    return 0;
}
