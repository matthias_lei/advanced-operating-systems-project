/**
 * \file
 * \brief apollo
 * Son of Zeus and Leto, and the twin brother of Artemis.
 *
 * technical note: the program is linked with nameserver_ui_client library where the actual work is done are defined.
 *
 * A library was used to allow integration of the functionality also as a builtin program in the shell
 *
 * group C, individual project: Nameserver
 *
 * version 2017-12-20, Pirmin Schmid
 */


#include <stdio.h>

#include <nameserver_ui_client.h>

#define AOS_TESTING_DEBUGGING_MAIN
#include <testing/debugging.h>

int main(int argc, char *argv[])
{
    debug_printf("Apollo sends you wisdom about everything and more. What was your name again?\n");

    return nameserver_ui_client(argc, argv, stdin, stdout);
}
