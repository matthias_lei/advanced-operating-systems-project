#include <aos/aos.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <aos/aos_rpc.h>
#include <aos/sys_debug.h>

int main(int argc, char *argv[])
{
    debug_printf("mem_if started: testing memory serving RPC\n");

    struct aos_rpc *memserv = aos_rpc_get_memory_channel();

    errval_t err;
    size_t retbytes = 0;
    struct capref cap, frame;
    err = slot_alloc(&frame);
    assert(err_is_ok(err));
    for (size_t request = 1UL << 12; request < 1UL << 21; request = request << 1) {
        debug_printf("testing with %zu bytes:\n", request);
        err = aos_rpc_get_ram_cap(memserv, request, BASE_PAGE_SIZE, &cap, &retbytes);
        assert(err_is_ok(err));
        if (retbytes < request) {
            debug_printf("  retbytes = %zu\n", retbytes);
        }
        struct frame_identity fi;
        err = invoke_frame_identify(cap, &fi);
        assert(err_is_ok(err));
        if (fi.bytes < request) {
            debug_printf("  fi.bytes = %zu\n", fi.bytes);
        }
        err = cap_retype(frame, cap, 0, ObjType_Frame, fi.bytes, 1);
        if (err_is_fail(err)) {
            debug_printf("  retype returned %s\n", err_getcode(err));
            continue;
        }
        void *vbuf;
        err = paging_map_frame(get_current_paging_state(),
                &vbuf, fi.bytes, frame, NULL, NULL);
        if (err_is_fail(err)) {
            debug_printf("  paging_map_frame() returned %s\n", err_getcode(err));
            continue;
        }

        debug_printf("  touching and checking mapped frame\n");
        char *buf = vbuf;
        for (int i = 0; i < retbytes; i++) {
            buf[i] = i % 256;
        }
        sys_debug_flush_cache();
        for (int i = 0; i < retbytes; i++) {
            assert(buf[i] == i % 256);
        }
        debug_printf("  deleting frame\n");
        //XXX: should unmap here!
        err = cap_delete(frame);
        assert(err_is_ok(err));
    }

    debug_printf("mem_if: done\n");
    return 0;
}
