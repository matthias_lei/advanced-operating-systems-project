/**
 * \file
 * \brief domain manager UI
 *
 * Domain manager in user space program. It's actually just the UI for domain management.
 * The heavy lifting is done by the Demeter process service.
 *
 * technical notes:
 * - the code comes mainly from the domain manager in the milestone 2 test cases.
 * - this program may build a basis for a complete shell program in the future.
 *
 * version 2017-11-12, group C
 */


#include <stdio.h>
#include <stdlib.h>

#include <aos/aos_rpc.h>

#define WHITE "\033[0m"
#define RED   "\033[31m"
#define GREEN "\033[32m"
#define BLUE  "\033[34m"

// NOTE: with current line buffering in output, colors must be changed back to
// white before each LF
// This will not be a requirement anymore with the full windowing system.


//--- available user programs for the menu ---------------------------------------------------------
// this needs to be configured still manually at the moment
// I suggest, only having actual user programs here. The services usr programs should be started
// by init (demeter directly, and the others via demeter). They should not be launched by the user
// at the moment. On the other hand, we may need a watchdog that can check whether one of the
// services ended and automatically relaunch it. However, not first priority at the moment.

static const char control_chars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

typedef struct programdef {
    char *name; // no const here to avoid problems with given aos_rpc_process_spawn() interface
    char launch;
} programdef_t;

static programdef_t programs[] = {
    {   .name = "artemis" },
    {   .name = "tests/hello" },
    {   .name = "tests/memeater" },
    {   .name = "tests/t02_spawn_args_passing" },
    {   .name = "tests/t02_spawn_infinitely" },
    {   .name = "tests/t02_spawn_two_1" },
    {   .name = "tests/t02_spawn_two_2" },
    {   .name = "tests/t04_bigmalloc" },
    {   .name = "tests/t04_nullpointer" },
    {   .name = "tests/t04_paging_state_transfer" },
    {   .name = "tests/t04_stackeater" },
    {   .name = "tests/t04_threads" },
    {   .name = "tests/t05_stack_trace_recursion" },
    {   .name = "tests/t_windowing_system_1" },
    {   .name = "tests/t_windowing_system_2" },
    {   .name = "tests/test_stat_functions" },
    {   .name = "tests/net_udp_echo_server"},

    /* add more structs into this array with more programs */
};

static const int programs_size = sizeof(programs) / sizeof(programs[0]);

STATIC_ASSERT(sizeof(programs) / sizeof(programs[0]) <= sizeof(control_chars) - 1, "Not enough control characters available!");


//--- UI input helper ------------------------------------------------------------------------------

typedef enum input_type {
    DOMAINMAN_INPUT_TYPE_NOTHING, /* also in case wrong inputs */
    DOMAINMAN_INPUT_TYPE_ALL,
    DOMAINMAN_INPUT_TYPE_CHARACTER,
    DOMAINMAN_INPUT_TYPE_INTEGER,
    DOMAINMAN_INPUT_TYPE_MENU,
    DOMAINMAN_INPUT_TYPE_EXIT
} input_type_t;

typedef struct input {
    input_type_t type;
    int value;
    char c;
} input_t;

#define MAX_NUMBER_LEN 4
// for formatting reasons

/**
 * This helper function reads chars and integers from the terminal
 * integers are recognized by digits and are read until terminator sign $
 * is given.
 * exit sign # is also recognized
 * + is recognized as "all" at the moment
 * space just displays the menu again
 * The function may return DOMAINMAN_INPUT_TYPE_NOTHING in case of parsing error.
 *
 * The function also provides the echo for the terminal.
 */
static input_t read_input(void) {
    input_t r = {};
    char c = getchar();
    if (c == '#') {
        printf("#\nexit\n");
        r.type = DOMAINMAN_INPUT_TYPE_EXIT;
        return r;
    }
    else if (c == '+') {
        printf("+\nkill all running domains\n");
        r.type = DOMAINMAN_INPUT_TYPE_ALL;
        return r;
    }
    else if (c == ' ') {
        r.type = DOMAINMAN_INPUT_TYPE_MENU;
        return r;
    }
    else if ('1' <= c && c <= '9') {
        int value = c - '0';
        int max = INT_MAX / 10;
        while (value <= max) {
            c = getchar();
            if (c == '$') {
                r.type = DOMAINMAN_INPUT_TYPE_INTEGER;
                r.value = value;
                printf("%d\n", value);
                return r;
            }

            // not yet terminated
            if ('0' <= c && c <= '9') {
                value *= 10;
                value += c - '0';
            }
            else {
                printf("Invalid input (not an integer followed by $)\n");
                r.type = DOMAINMAN_INPUT_TYPE_NOTHING;
                return r;
            }
        }

        // overrun
        printf("Invalid input (not an integer followed by $; overflow)\n");
        r.type = DOMAINMAN_INPUT_TYPE_NOTHING;
        return r;
    }
    else {
        r.type = DOMAINMAN_INPUT_TYPE_CHARACTER;
        r.c = c;
        printf("%c\n", c);
        return r;
    }
}

//--- list helper to cache info that was received from Demeter -------------------------------------
// some internal static values to allow domainman to work
// technical note: in contrast to the domainman in the low-level test menu, that is using a list,
// we can use an array here for the domain nodes. After lookup of all pids, we get to know
// the current numer of processes.

typedef struct domaininfo {
    domainid_t domain_id;
    char *name;
    // additional metadata in the future
} domaininfo_t;

static domaininfo_t *domaininfo_array = NULL;
static size_t domaininfo_array_size = 0;


//--- interface with Demeter process service -------------------------------------------------------

static struct aos_rpc *demeter;

static void clean_domaininfo_list(void) {
    if (domaininfo_array) {
        for (int i = 0; i < domaininfo_array_size; i++) {
            if (domaininfo_array[i].name) {
                free(domaininfo_array[i].name);
            }
        }

        free(domaininfo_array);
        domaininfo_array = NULL;
        domaininfo_array_size = 0;
    }
}


static int domain_id_cmp(const void *a_, const void *b_) {
    const domainid_t *a = a_;
    const domainid_t *b = b_;
    return (*a < *b) ? -1 :
           (*a > *b) ? 1 : 0;
}

static errval_t prepare_domaininfo_list(void) {
    assert(demeter);
    assert(!domaininfo_array);

    // note: according to API, the RPC creates the list; but it will be freed here after use.
    // as discussed: we cannot assume that this list is sorted.
    domainid_t *domain_ids = NULL;
    size_t domain_ids_size = 0;
    errval_t err = aos_rpc_process_get_all_pids(demeter, &domain_ids, &domain_ids_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s(): error while calling aos_rpc_process_get_all_pids()\n", __func__);
        return err;
    }

    if (domain_ids_size == 0) {
        // nothing to do; just free any memory if it was sent by Demeter
        // (not sure how it is implemented there in this corner case)
        // thus: we are save here.
        if (domain_ids) {
            free(domain_ids);
        }
        return SYS_ERR_OK;
    }

    assert(domain_ids);
    domaininfo_array = calloc(domain_ids_size, sizeof(domaininfo_t));
    if (!domaininfo_array) {
        debug_printf("%s(): not enough heap memory for allocation of a new domaininfo_array.\n", __func__);
        return LIB_ERR_MALLOC_FAIL;
    }
    domaininfo_array_size = domain_ids_size;

    // sort the list
    qsort(domain_ids, domain_ids_size, sizeof(domainid_t), domain_id_cmp);

    for (size_t i = 0; i < domain_ids_size; i++) {
        // according to the documentation, the memory for the name is allocated by rpc
        // and must be freed by
        domainid_t current = domain_ids[i];
        domaininfo_array[i].domain_id = current;
        err = aos_rpc_process_get_name(demeter, current, &domaininfo_array[i].name);
        // note: it is assumed that the received string pointer is NULL in case of an error
        // in RPC that did not lead to a malloc()
        // in case name != NULL -> assumed that there was a successful alloc -> free() call in cleanup
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "%s(): error while calling aos_rpc_process_get_name()\n", __func__);
            clean_domaininfo_list();
            // note: goal is to get a complete list or no list (atomic)
            return err;
        }

        assert(domaininfo_array[i].name);
    }

    return SYS_ERR_OK;
}


/**
 * \param found               index position in the programs array
 * \param core                defines core on which it shall run
 * \param wait_for_any_key    waits for a pressed key before returning if true; not otherwise
 */
static int launch_program(int found, coreid_t core, bool wait_for_any_key) {
    assert(demeter);
    // note: there should be less output interference later when the Hermes windowing system
    // is active. Then only debug_printf() will actually come through.
    // Regular output will be redirected to the window in the background.

    if (wait_for_any_key) {
        printf("\n" GREEN "launching program %s on core %" PRIuCOREID WHITE "\n"
               "\nNOTE: domain manager waits with refresh of the menu until a key is pressed\n"
               "to avoid interference with initial program output.\n"
               "\nPRESS ANY KEY to refresh domain manager menu.\n\n",
                programs[found].name, core);
    }
    else {
        printf("\n" GREEN "launching program %s on core %" PRIuCOREID WHITE "\n", programs[found].name, core);
    }

    // note: no domaininfo list management needed here. The list will be updated
    // in the next iteration with information directly received from Demeter process service

    domainid_t domain_id;
    errval_t err = aos_rpc_process_spawn(demeter, programs[found].name, core, &domain_id);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_process_spawn() failed.\n");
        return 1;
    }

    printf("\nSuccesfully spawned new domain %" PRIuDOMAINID " with program %s on core %" PRIuCOREID ".\n\n",
           domain_id, programs[found].name, core);

    if (wait_for_any_key) {
        // wait for a "space" to avoid interference with initial output of the
        // started program
        getchar();
    }

    return 0;
}


static void kill_program(domaininfo_t *current) {
    assert(demeter);

    printf("\n" RED "killing %4" PRIuDOMAINID " %s" WHITE "\n", current->domain_id, current->name);

    // TODO: adjust as soon as a KILL RPC is available in the RPC API
    printf("NOTE: killing domains is not yet available via RPC.\n"
           "Please use the domain manager in the low-level test suite\n"
           "of Milestone 2 to test this feature at the moment.\n");


    // TODO pisch: we should have some check to distinguish the programs that can be killed
    // by the user and others that are protected (like our services or init)
    // such a check is best done in Demeter, of course.
    // Thus, it can already be handled save now (by just receiving an error code).
    // But with a lookup info, we could already adjust the UI providing a nicer user experience.

    // note: no domaininfo list management needed here. The list will be updated
    // in the next iteration with information directly received from Demeter process service
}

//--- domain management UI -------------------------------------------------------------------------

/**
 * the domain manager is currently limited
 * - re number of launchable programs (a-z).
 * - number of digits for ID (see MAX_NUMBER_LEN)
 * - input syntax:
 *   #        -> exit
 *   a-z      -> launch defined program
 *   <value>$ -> kill program with id
 *   +        -> kill all programs except init
 * - all lookups happen in O(n) time with n == associated list length
 */

// note: core 1 is the new default core for applications
#define DEFAULT_CORE 1

static coreid_t select_core(void) {
    // returns the default core, except input is valid for core 0
    printf("On which core? [0|1], default is %" PRIuCOREID ".\n", DEFAULT_CORE);
    char c = getchar();
    switch (c) {
        case '0':
            return 0;
        case '1':
            return 1;
        default:
            return DEFAULT_CORE;
    }
}

static int domain_manager(void) {
    bool run = true;
    while (run) {
        // prepare list
        errval_t err = prepare_domaininfo_list();
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "FATAL ERROR in Domain Manager UI: Could not connect with Demeter process server.\n");
            return 1;
        }

        // output
        printf("\n\n------------------ Domain Manager UI: Group C domain manager ------------------\n");
        printf("Launch available programs (use <space> to re-fresh menu):\n");
        for (int i = 0; i < programs_size; i++) {
            printf(" %c %s\n", programs[i].launch, programs[i].name);
        }
        printf("\n");
        printf("Running programs (kill program by entering the ID followed by $)\n  ID name\n");
        if (domaininfo_array) {
            for (size_t i = 0; i < domaininfo_array_size; i++) {
                printf("%4" PRIuDOMAINID " %s\n", domaininfo_array[i].domain_id, domaininfo_array[i].name);
            }
        }
        printf("\n<space> refresh menu\n   +    kill all domains\n   #    exit domain manager\n\nPlease select\n");

        // wait for input
        input_t r = read_input();

        // handle input
        switch (r.type) {
            case DOMAINMAN_INPUT_TYPE_NOTHING:
                // error message has already been printed
                break;

            case DOMAINMAN_INPUT_TYPE_CHARACTER:
                {
                    // spawn new program, if found
                    int found = -1;
                    for (int i = 0; i < programs_size; i++) {
                        if (r.c == programs[i].launch) {
                            found = i;
                            break;
                        }
                    }

                    if (0 <= found) {
                        int e = launch_program(found, select_core(), true);
                        if (e != 0) {
                            return e;
                        }
                    }
                    else {
                        printf("ERROR: character not associated with defined program\n");
                    }
                    break;
                }

            case DOMAINMAN_INPUT_TYPE_INTEGER:
                {
                    // kill domain if found
                    domainid_t id = (domainid_t)r.value;
                    size_t i;
                    for (i = 0; i < domaininfo_array_size; i++) {
                        if (domaininfo_array[i].domain_id == id) {
                            kill_program(&domaininfo_array[i]);
                        }
                    }

                    if (i == domaininfo_array_size) {
                        printf("Unknown id %d\n", r.value);
                    }

                    break;
                }

            case DOMAINMAN_INPUT_TYPE_ALL:
                {
                    // kill all domains
                    for (size_t i = 0; i < domaininfo_array_size; i++) {
                        kill_program(&domaininfo_array[i]);
                    }
                }

            case DOMAINMAN_INPUT_TYPE_MENU:
                // just do a refresh in the next iteration
                break;

            case DOMAINMAN_INPUT_TYPE_EXIT:
                run = false;
                break;

            default:
                debug_printf("input type not yet implemented\n");
        }

        // cleanup
        clean_domaininfo_list();
    }

    return 0;
}

//--- main -----------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
    debug_printf("Starting Domain Manager UI.\n");

    demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("FATAL ERROR: Could not connect with Demeter process service.\n");
        return 1;
    }

    for (int i = 0; i < programs_size; i++) {
        programs[i].launch = control_chars[i];
    }

    return domain_manager();
}
