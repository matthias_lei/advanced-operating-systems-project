/**
 * \file
 * \brief init process for child spawning
 */

/*
 * Copyright (c) 2007, 2008, 2009, 2010, 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

/**
 * AOS class 2017, Group C: spawn and bootstrap concept
 * ----------------------------------------------------
 *
 * Note: init starts with 64 KiB stack and 16 MiB heap. Additionally, it needs 64 MiB
 * for all the spawns. The rest can be given to Dionysos.
 *
 * general spawn includes:
 * - monitor channel
 * - nameserver channel
 *
 * all other services via late binding. this includes the memory service
 *
 * This could be reduced to min. 1 channel with the cost of repeating some parts of the
 * bootstrap problem over and over again. In the current boot concept, after special
 * spawning of memory and nameserver, all spawns follow the same principle without
 * overhead nor headache.
 *
 * bootstrap process
 * 1) spawn memory service Dionysos (only receives /core/monitor/0) -> /core/memory/0
 *    no registration yet
 * 2) spawn nameserver service Gaia (receives /core/monitor/0) -> /core/name/0
 *    nameserver connects with /core/memory/0 upon registration of the memory service
 * 3) RPC sent to Dionysos to register himself to Gaia
 *
 * 4) spawn new monitor on core 1 -> /core/monitor/1
 *    receives /core/monitor/0 as intermon connection
 *             /core/name/0
 *
 *    registers itself with Gaia as /core/monitor/1
 *
 * 5) spawn process service Demeter -> /core/domain/0
 *    Demeter can run on either core 0 or core 1
 *    As a simplification for the transition from monolithic to distributed system,
 *    it is launched on core 0 to be on the same core as memory and name server.
 *    as all services from now on, it registers with Gaia upon launch
 *
 * 6) RPC calls to Demeter to manually add init, dionysos, gaia, monitor1 and demeter to the list
 *    including proper core_id in the name.
 *    alternative: they can also be predefined within demeter, who is aware of the
 *    bootstrap sequence
 *
 * spawn rest of services / domains via Demeter either on core 0 or core 1 as desired
 * Demeter registers them automatically during the spawn.
 * Each service registers itself with Gaia after internal initialization
 *
 * 7) serial I/O Hermes -> /core/serial/0 (independent on which core it is running, of course)
 *
 * 8) additional services: network, filesystem,...
 *
 * 9) more...
 *
 * 10) launch Zeus via demeter
 *
 * concept version 2017-12-02, pisch
 */


#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/deferred.h>

#include "mem_alloc.h"

#include <testing/testing.h>

#define AOS_TESTING_DEBUGGING_MAIN
#include <testing/debugging.h>

#include <domman/domman.h>

#include "bootstrap.h"
#include "setup.h"
#include "spawn_core.h"

#include "tests.h"

#include <aos_rpc_server/aos_rpc_server_common.h>
#include <aos_rpc_server/aos_rpc_server_device.h>
#include <aos_rpc_server/aos_rpc_server_global.h>
#include <aos_rpc_server/aos_rpc_server_memory.h>
#include <aos_rpc_server/aos_rpc_server_monitor.h>
#include <aos_rpc_server/aos_rpc_server_intermon.h>
#include <aos_rpc_server/aos_rpc_server_process.h>
#include <aos_rpc_server/aos_rpc_server_serial.h>

#define CA1 0xca
#define FE1 0xfe
#define CAFE4 CA1, FE1, CA1, FE1
#define CAFE16 CAFE4, CAFE4, CAFE4, CAFE4
#define CAFE64 CAFE16, CAFE16, CAFE16, CAFE16
#define CAFE256 CAFE64, CAFE64, CAFE64, CAFE64
#define CAFE1024 CAFE256, CAFE256, CAFE256, CAFE256
#define CAFE4096 CAFE1024, CAFE1024, CAFE1024, CAFE1024

// 4 KiB region filled with 0xcafecafe used to patch in
// command line arguments for init (see also /tools/usbboot/usbboot.c)
static char init_args[] = { CAFE4096 };

struct bootinfo *bi;

/*
    extended dispatch loop, that additionally regularly polls for user input
    to buffer it and check it for desired window switch
*/
static void event_loop(bool serial_polling)
{
    struct waitset *ws = get_default_waitset();
    while (true) {
        // check for local events
        errval_t err = check_for_event(ws);
        if (err_is_ok(err)) {
            err = event_dispatch(ws);
            if (err_is_fail(err)) {
                // fatal error
                DEBUG_ERR(err, "event_dispatch failed.\n");
                abort();
            }
        }
        else if (err_no(err) != LIB_ERR_NO_EVENT) {
            // fatal error
            DEBUG_ERR(err, "check_for_event failed.\n");
            abort();
        }

        // TODO jmeier: this temporary hack only enables Hermes on core 0
        //              and will be removed once we have a single global
        //              instance
        //
        // note pisch: This hack keeps working well with unified Hermes on core 0
        if (serial_polling && disp_get_core_id() == 0) {
            err = handle_input_characters();
            if (err_is_fail(err)) {
                // just log the error, but we want to continue polling
                DEBUG_ERR(err, "handle_input_characters failed.\n");
            }
        }

    }
}

__attribute__((unused))
static void show_low_level_menu(void)
{
    // this low-level menu is only shown on BSP core
    // note: this test suite is linked with testing_init
    // it may only run correctly if init holds all the servers at the moment
    // some tests in the ti01 and ti02 files may need adjustment if we start distributing the servers

    debug_printf("\nPlease exit this initial test menu to launch the event handlers and the actual test program: Artemis\n");


    // the user has to exit the test suite menu to enter the message handler loop below!!!
    testing_add_tests(); // prepare the test suite
    //testing_run_all_tests(); // to run the full test suite
    testing_run_shell(); // to show a menu and select some tests
    // cleanup if the menu data structure is not used anymore at a future time
    testing_cleanup();
}

static errval_t setup_local_server_channels_bsp_monolithic(void) {
    struct aos_rpc *init_rpc = malloc(sizeof(*init_rpc));
    if (!init_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local monitor channel\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(init_rpc, AOS_RPC_INTERFACE_MONITOR, &aos_rpc_monitor_local_vtable);
    set_init_rpc(init_rpc);

    struct aos_rpc *device_rpc = malloc(sizeof(*device_rpc));
    if (!device_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local channel to memory server\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(device_rpc, AOS_RPC_INTERFACE_DEVICE, &aos_rpc_device_local_vtable);
    set_device_rpc(device_rpc);

    struct aos_rpc *memory_rpc = malloc(sizeof(*memory_rpc));
    if (!memory_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local channel to memory server\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(memory_rpc, AOS_RPC_INTERFACE_MEMORY, &aos_rpc_memory_local_vtable);
    set_memory_rpc(memory_rpc);

    struct aos_rpc *process_rpc = malloc(sizeof(*process_rpc));
    if (!process_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local channel to process server\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(process_rpc, AOS_RPC_INTERFACE_PROCESS, &aos_rpc_process_local_vtable);
    set_process_rpc(process_rpc);

    struct aos_rpc *serial_rpc = malloc(sizeof(*serial_rpc));
    if (!serial_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local channel to serial server\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(serial_rpc, AOS_RPC_INTERFACE_SERIAL, &aos_rpc_serial_local_vtable);
    set_serial_rpc(serial_rpc);

    // note: INTERMON is not setup locally here. There is no point for that at the moment.
    // it would actually disturb the system because each monitor is client and server
    // and needs to use the remote functions all the time.

    return SYS_ERR_OK;
}

static errval_t setup_local_server_channels_bsp_distributed(void) {
    struct aos_rpc *init_rpc = malloc(sizeof(*init_rpc));
    if (!init_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local monitor channel\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(init_rpc, AOS_RPC_INTERFACE_MONITOR, &aos_rpc_monitor_local_vtable);
    set_init_rpc(init_rpc);

    struct aos_rpc *device_rpc = malloc(sizeof(*device_rpc));
    if (!device_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local channel to memory server\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(device_rpc, AOS_RPC_INTERFACE_DEVICE, &aos_rpc_device_local_vtable);
    set_device_rpc(device_rpc);

    // note: INTERMON is not setup locally here. There is no point for that at the moment.
    // it would actually disturb the system because each monitor is client and server
    // and needs to use the remote functions all the time.

    return SYS_ERR_OK;
}

static errval_t setup_local_server_channels_app(void) {
    struct aos_rpc *init_rpc = malloc(sizeof(*init_rpc));
    if (!init_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local monitor channel\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    init_local_channel(init_rpc, AOS_RPC_INTERFACE_MONITOR, &aos_rpc_monitor_local_vtable);
    set_init_rpc(init_rpc);

    // note: INTERMON is not setup locally here. There is no point for that at the moment.
    // it would actually disturb the system because each monitor is client and server
    // and needs to use the remote functions all the time.

    return SYS_ERR_OK;
}


int main(int argc, char *argv[])
{
    /* Parse arguments passed to boot (see also /tools/usbboot/usbboot.c) */
    errval_t err = parse_arguments(init_args);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "main(): parse_arguments failed\n");
    }

    // here, dynamic configurations can be made based on parsed arguments

    // e.g. config_set_spawn_load_symbols(bool value)
    // currently, the default is used (set in lib/aos/init.c)

    /* Set the core id in the disp_priv struct */
    coreid_t my_core_id = -1;
    err = invoke_kernel_get_core_id(cap_kernel, &my_core_id);
    assert(err_is_ok(err));
    disp_set_core_id(my_core_id);

    debug_printf("init: on core %" PRIuCOREID " invoked as:", my_core_id);
    for (int i = 0; i < argc; i++) {
       printf(" %s", argv[i]);
    }
    printf("\n");

    // currently fixed to distributed
    // TODO: monitor/0: read mode from input arguments or user input
    //       monitor/1: read from provided arguments during spawn
    enum rpc_services_mode mode = RPC_SERVICES_MODE_DISTRIBUTED;
    debug_printf("services mode: %zu :: DISTRIBUTED\n", (size_t)mode);

    /* First argument contains the bootinfo location, if it's not set */
    bi = (struct bootinfo*)strtol(argv[1], NULL, 10);

    // bi should only be a valid pointer on the bootstrapping core
    assert((bi == NULL) ^ (my_core_id == 0));

    if (my_core_id == 0) {
        setup_init_bsp(mode);

        if (mode == RPC_SERVICES_MODE_DISTRIBUTED) {
            err = setup_local_server_channels_bsp_distributed();
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "setting up local server channels failed\n");
                abort();
            }
        }
        else {
            err = setup_local_server_channels_bsp_monolithic();
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "setting up local server channels failed\n");
                abort();
            }
        }
    }
    else {
        // TODO: replace hardcoded routing here wirh argument from input info.
        setup_init_app(AOS_RPC_ROUTING_SET(0, my_core_id));

        err = setup_local_server_channels_app();
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "setting up local server channels failed\n");
            abort();
        }
    }


    // load symbols for init
    // note: this must come after initialization of RAM and paging.
    err = load_elf_symbols_for_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Could not load debug symbols for init. Process continues.\n");
        // continue
    }

    if (my_core_id == 0) {
        // not possible in distributed mode (no aos_mm has been initialized)
        // TODO: add switch
        //show_low_level_menu();

        // bootstrap
        bootstrap_initial_services_before_core_spawn();

        spawn_second_core();

        bootstrap_initial_services_after_core_spawn();

        if (mode != RPC_SERVICES_MODE_DISTRIBUTED) {
            domain_manager_init();
        }

        // An additional test
        printf("init: Testing lazy binding of serial I/O service: OK, if you see this line.\n");

        // LOGO generated using http://patorjk.com/software/taag/#p=display&f=Big&t=OlympOS
        debug_printf("   ____  _                        ____   _____\n");
        debug_printf("  / __ \\| |                      / __ \\ / ____|\n");
        debug_printf(" | |  | | |_   _ _ __ ___  _ __ | |  | | (___\n");
        debug_printf(" | |  | | | | | | '_ ` _ \\| '_ \\| |  | |\\___ \\\n");
        debug_printf(" | |__| | | |_| | | | | | | |_) | |__| |____) |\n");
        debug_printf("  \\____/|_|\\__, |_| |_| |_| .__/ \\____/|_____/\n");
        debug_printf("            __/ |         | |\n");
        debug_printf("           |___/          |_|\n");
        debug_printf("\n");

        // start the shell
        domainid_t zeus_pid;
        err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "zeus", disp_get_core_id(), &zeus_pid);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "aos_rpc_process_spawn failed to spawn zeus.\n");
            abort();
        }
    }

    event_loop(mode == RPC_SERVICES_MODE_MONOLITHIC);

    return EXIT_SUCCESS;
}
