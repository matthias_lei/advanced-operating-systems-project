/**
 * Group C: some tests that needed to run in init
 * They are typically deactivated in the merged/final version
 */

#include "tests.h"

#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/inthandler.h>
#include <aos/waitset_chan.h>
#include <netutil/user_serial.h>
#include <maps/omap44xx_map.h>
#include <driverkit/driverkit.h>
#include <net/socket_interface_local.h>
#include <net/slip.h>
#include <net/ip_impl.h>
#include <net/ip_prot.h>
#include <net/inet_descriptorring.h>
#include <net/udp_impl.h>
#include <net/udp_prot_local.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>
#include <spawn/spawn.h>

static void slip_input_handler_test(uint8_t c);
static void slip_packet_end_handler_test(void);
static void ip_impl_packet_recv_handler_test(struct ip *packet);
static void waitset_handler_test(void *arg);

/**
 * only for testing purpose. May make other URPC receiver handler registration fail
 * since it is piggy back using / "borrowing" cap_urpc.
 */
void quick_ump_chan_test(void)
{
    // establishes an UMP channel from core 0 (client) to core 1 (server)
    struct ump_chan chan;
    void *frame_ptr = NULL;
    // note re VREGION_FLAGS_READ_WRITE_MPB: this message passing buffer specific flag MPB is not active on ARM.
    // thus, regular R/W flags are used, which allow caching
    // (see _NO_CACHE that would prevent caching and is **NOT** desired for UMP)
    errval_t err = paging_map_frame_attr(get_current_paging_state(), &frame_ptr, MON_URPC_SIZE, cap_urpc, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    assert(err_is_ok(err));

    bool as_client = disp_get_core_id() == 0;
    err = ump_chan_init(&chan, frame_ptr, MON_URPC_SIZE, as_client);
    assert(err_is_ok(err) && (chan.connstate == UMP_CONNECTED));
    debug_printf("UMP chan test with handshake: UMP successfully connected as %s\n", as_client ? "client" : "server");
}

void quick_irq_test(void) {

    debug_printf("quick_irq_test launched\n");

    lvaddr_t uart4_vaddr;
    errval_t err = map_device_register(OMAP44XX_MAP_L4_PER_UART4, OMAP44XX_MAP_L4_PER_UART4_SIZE, &uart4_vaddr);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "map_device_register failed\n");
    }

    err = serial_init(uart4_vaddr, UART4_IRQ);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "serial_init failed\n");
    }

    debug_printf("quick_irq_test exited\n");
}

static void slip_input_handler_test(uint8_t c) {
    debug_printf("received byte: %c % 1x\n", c, c);
}

static void slip_packet_end_handler_test(void) {
    debug_printf("slip packet finished\n");
}

void quick_slip_test(void) {

    errval_t err = slip_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slip_init failed\n");
    }

    slip_register_input_handler(slip_input_handler_test);
    slip_register_packet_end_handler(slip_packet_end_handler_test);

    return;
}

static void ip_impl_packet_recv_handler_test(struct ip *packet) {
    //dump_ip_header((struct ip *) packet);
    debug_printf("ip packed finished\n");
}

void quick_ip_impl_test(void) {

    errval_t err = ip_impl_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ip_impl_init failed\n");
    }
    ip_impl_register_packet_recv_handler(ip_impl_packet_recv_handler_test);
}


void quick_ip_prot_test(void) {
    errval_t err = ip_prot_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ip_impl_init failed\n");
    }
}

__attribute__((unused))
static void waitset_handler_test(void *arg) {
    char *arg_string = (char *)arg;
    debug_printf("received: %s\n", arg_string);
}

void quick_waitset_test(void) {

    struct waitset_chanstate *wscs = malloc(sizeof(*wscs));

    waitset_chanstate_init(wscs, CHANTYPE_UMP_IN);

    //char *msg = "hello world!";

    errval_t err = waitset_chan_trigger_closure(get_default_waitset(), wscs, MKCLOSURE(waitset_handler_test, NULL));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "waitset_chan_trigger_closure failed\n");
    }
    debug_printf("wscs chan: 0x%x state: %u\n", wscs, wscs->state);
}

void quick_udp_impl_test(void) {

    errval_t err = ip_prot_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ip_impl_init failed\n");
        return;
    }

    struct udp_socket_state ss;

    err = udp_impl_socket_state_init(&ss, UDP_DGRAM_DEFAULT_SEND_BUF_SIZE, UDP_DGRAM_DEFAULT_RECV_BUF_SIZE);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "udp_impl_socket_state_init failed\n");
        return;
    }

    u_short port = 1234;

    struct sockaddr_in addr;

    addr.sin_family = AF_INET;
    addr.sin_port = port;
    addr.sin_addr.s_addr = 0x0a000202;

    if (!udp_impl_bind_socket_state(&ss, &addr)) {
        debug_printf("udp_impl_bind_socket_state failed\n");
        return;
    }

    struct ip *ip_packet;
    size_t len;
    struct udp *udp_packet;
    char msg[128];


    while (true) {
        if (udp_impl_read_from_buffer(&ss, &ip_packet, &len)) {
            udp_packet = (struct udp *) ((uint32_t *)ip_packet + ip_packet->ip_hl);
            memset(&msg, 0, 128);
            memcpy(&msg, &udp_packet->payload, ip_packet->ip_len - 28);
            debug_printf("packet received: %s\n", &msg);
        }

        err = event_dispatch(get_default_waitset());
        if (err_is_fail(err)) {
            // we want to continue on error here, so just log
            DEBUG_ERR(err, "quick_udp_impl_test(): event_dispatch failed.\n");
        }
    }
}

void quick_socket_interface_local_test(void) {

    debug_printf("starting quick_socket_interface_local_test\n");

    errval_t err = socket_interface_local_init();
    if(err_is_fail(err)) {
        DEBUG_ERR(err, "socket_interface_local_init failed\n");
        return;
    }

    uint32_t socket;
    socket = socket_interface_local_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket == -1) {
        debug_printf("socket() failed: %u\n", errno);
        return;
    }

    struct sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = 6789;
    host_addr.sin_addr.s_addr = INADDR_ANY;

    uint32_t errcode;
    errcode = socket_interface_local_bind(socket, (struct sockaddr *)&host_addr, sizeof(host_addr));
    if (errcode == -1) {
        debug_printf("bind() failed: %u\n", errno);
        return;
    }

    struct sockaddr_in bound_addr;
    socklen_t bound_addr_len = sizeof(struct sockaddr_in);

    errcode = socket_interface_local_getsockname(socket, (struct sockaddr *)&bound_addr, &bound_addr_len);
    if (errcode == -1) {
        debug_printf("getsockname() failed: %u\n", errno);
        return;
    }

    debug_printf("socket nr %u successfully bound to:\n", socket);
    debug_printf("addr: %s:%hu\n", inet_ntoa(bound_addr.sin_addr), bound_addr.sin_port);

    size_t msg_buf_len = 128;
    char msg_buf[msg_buf_len];
    struct sockaddr_in src_addr;
    socklen_t src_addr_len = sizeof(src_addr);
    ssize_t msg_recv_len;
    ssize_t msg_send_len;

    // NOTE mlei: very very rudimentary udp echo server
    memset(&msg_buf, 0, msg_buf_len);
    while (true) {
        msg_recv_len = socket_interface_local_recvfrom(socket, &msg_buf, msg_buf_len, 0, (struct sockaddr *)&src_addr, &src_addr_len);
        if (msg_recv_len == -1) {
            debug_printf("udp_prot_local_recvfrom failed: %u Continuing...\n", errno);
            continue;
        }
        debug_printf("Msg from: %s:%hu\n", inet_ntoa(src_addr.sin_addr), src_addr.sin_port);
        debug_printf("%s\n", &msg_buf);

        msg_send_len = socket_interface_local_sendto(socket, msg_buf, msg_recv_len, 0, (struct sockaddr *)&src_addr, sizeof(src_addr));
        if (msg_send_len == -1) {
            debug_printf("sendto() failed: Continuing...%u\n", errno);
            continue;
        }

        memset(&msg_buf, 0, msg_buf_len);
    }
}

void quick_udp_echo_server_spawn_test(void) {

    domainid_t newpid;
    errval_t err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "net_udp_echo_server", 0, &newpid);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "first remote spawn failed\n");
        return;
    }
    debug_printf("first spawn successful: %u\n", newpid);

    err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "net_udp_echo_server", 0, &newpid);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "second remote spawn failed\n");
        return;
    }
    debug_printf("second spawn successful: %u\n", newpid);

    return;
}
