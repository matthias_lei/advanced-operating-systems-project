/**
 * Interface for spawning the 2nd core
 * 
 * Group C
 */

#ifndef USR_INIT_SPAWN_CORE_H_
#define USR_INIT_SPAWN_CORE_H_

void spawn_second_core(void);

#endif // USR_INIT_SPAWN_CORE_H_
