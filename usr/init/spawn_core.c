/**
 * Group C
 */

#include <stdio.h>
#include <stdlib.h>

#include <aos/aos.h>
#include <aos/connect/ump.h>
#include <aos/coreboot.h>
#include <aos/caddr.h>
#include <aos/kernel_cap_invocations.h>
#include <aos/deferred.h>

#include "spawn_core.h"
#include "setup.h"

#include <mm/mm.h>

#include <barrelfish_kpi/arm_core_data.h>

#include <spawn/spawn.h>
#include <spawn/multiboot.h>

#include <aos_rpc_server/aos_rpc_server_global.h>
#include <aos/rpc_shared/aos_rpc_shared_intermon.h>
#include <aos_rpc_server/aos_rpc_server_monitor.h>



//helper functions
static void print_core_data(struct arm_core_data *core_data);
static errval_t create_core_data_partial(struct arm_core_data **core_data, forvaddr_t *entrypoint);
static errval_t core_data_fill_in_init_data(struct arm_core_data *core_data);
static errval_t core_data_fill_in_ump_data(struct arm_core_data *core_data);
static errval_t core_data_fill_in_relocatable_data(struct arm_core_data *core_data);

extern coreid_t my_core_id;
extern struct bootinfo *bi;
extern struct mm aos_mm;

static errval_t spawn_bootstrap_core(void *ump_buffer, routing_info_t intermon_routing)
{
    errval_t err = SYS_ERR_OK;
    // server and client on core 0
    // again, first is what is done first, i.e. on core 0: server, then client connection
    // note: on core 1, the order is reversed, of course
    // also: the ump_chan_init() including handshakes need to be established before
    // the actual RPC chan can be established
    size_t half = MON_URPC_SIZE >> 1;
    void *first = ump_buffer;
    void *second = ((char *)ump_buffer) + half;

    struct ump_service_create_data data;
    err = setup_ump_service_part1_from_buffer(first, half, &data);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "setup_ump_service_part1 failed\n");
    }
    // NOTE: deliberately, the monitor handler is used here (which can also handle intermon)
    err = setup_ump_service_part2(&data, MKCLOSURE(aos_rpc_monitor_event_handler, NULL));
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "setup_ump_service_part2 failed\n");
    }

    struct aos_rpc *intermon_client;
    err = setup_ump_client_from_buffer(second, half, AOS_RPC_INTERFACE_INTERMON, &aos_rpc_intermon_remote_vtable,
                           intermon_routing, &intermon_client);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "setup_ump_client failed\n");
    }
    set_intermon_rpc(AOS_RPC_ROUTING_SERVER_CORE_ID(intermon_routing), intermon_client);

    debug_printf("intermon handshakes completed\n");
    return SYS_ERR_OK;
}

static errval_t wait_for_core(const char *name)
{
    // no need to keep asking too early
    barrelfish_msleep(500); // aritrary time value; haigh at the moment for debugging

    // start asking
    struct aos_rpc *name_rpc = aos_rpc_get_name_channel();
    service_handle_t handle;
    errval_t err = aos_rpc_name_lookup_service_by_name(name_rpc, name, &handle);
    struct waitset *default_ws = get_default_waitset();
    while (err_no(err) == LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
        err = event_dispatch_non_block(default_ws);
        if (err_is_fail(err) && err_no(err) != LIB_ERR_NO_EVENT) {
            DEBUG_ERR(err, "error in dispatcher\n");
        }

        err = aos_rpc_name_lookup_service_by_name(name_rpc, name, &handle);
    }

    return err;
}

void spawn_second_core(void)
{
    coreid_t core_id = disp_get_core_id();
    assert(core_id == 0);

    // generate arm_core_data struct
    struct arm_core_data *core_data;
    forvaddr_t entrypoint;

    errval_t err = create_core_data_partial(&core_data, &entrypoint);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not create core_data struct\n");
    }

    // fill in data for init
    err = core_data_fill_in_init_data(core_data);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not fill in init data for second core\n");
    }

    // fill in core id data
    core_data->src_core_id = core_id;
    core_data->dst_core_id = APP_CORE_ID;

    err = core_data_fill_in_relocatable_data(core_data);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not fill in relocatable data of cpu driver elf\n");
    }

    /* Initialize URPC for boot core */
    size_t urpc_ret_size;
    err = frame_create(cap_urpc, MON_URPC_SIZE, &urpc_ret_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_create failed for URPC frame.\n");
        abort();
    }

    void *ump_buffer;
    err = paging_map_frame_attr(get_current_paging_state(), &ump_buffer, MON_URPC_SIZE, cap_urpc, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed for URPC frame.\n");
        abort();
    }

    memset(ump_buffer, 0, MON_URPC_SIZE);

    err = core_data_fill_in_ump_data(core_data);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not fill in intermon UMP data for second core\n");
    }

    // fence, clean (flush) & invalidate everything we just filled in
    // please note: flush to poc & invalidate of *core data* here is crucial to make the data
    // available to the 2nd core that starts with deactivated MMU (which also overrides any
    // cache setting, i.e. cache is off, too) reading data only from RAM.
    // On the other hand, the *URPC frame* is not used on the 2nd core before the MMU is
    // initialized, so theoretically a flush of the URPC frame should not be necessary here.
    // Nevertheless, we consider it safe and defensive practice to flush this initialized
    // URPC frame here to RAM too, in order to absolutely positively have a consistent state
    // for both cores.
    atomic_thread_fence_rel();
    sys_armv7_cache_clean_poc(ump_buffer, ump_buffer + MON_URPC_SIZE);
    sys_armv7_cache_clean_poc(core_data, (core_data + 1));
    sys_armv7_cache_invalidate(ump_buffer, ump_buffer + MON_URPC_SIZE);
    sys_armv7_cache_invalidate(core_data, (core_data + 1));
    atomic_thread_fence_acq();

    //boot second core
    err = invoke_monitor_spawn_core(APP_CORE_ID, CPU_ARM7, entrypoint);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "second core could not be booted");
    }

    // from this point onwards, it is absolutely safe to start sending URPC messages to the
    // newly spawned core, since invoke_monitor_spawn_core only returns after the  MMU of
    // the newly spawned core is enabled.
    // Please note: that the actual URPC message from core 0 to core 1 has not been
    // sent until this moment. The message will be sent from within
    // spawn_bootstrap_core(), which will also wait for a reply by core 1.

    err = spawn_bootstrap_core(ump_buffer, AOS_RPC_ROUTING_SET(APP_CORE_ID, BSP_CORE_ID));
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "failed to bootstrap second core\n");
    }

    // wait until /core/monitor/1 has completed the setup (which is indicated by its registration)
    // this prevents interleaving of the setups of both cores
    // but mainly, it allows spawning already demeter (next service) on the app core, if desired
    err = wait_for_core(APP_CORE_NAME);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "failed to bootstrap second core\n");
    }
    debug_printf("Setup of " APP_CORE_NAME " complete\n");
}


/** \brief  returns a pointer to a partially filled
*           arm_core_data struct and its kernel-virtual
*           address needed for invoke_monitor_spawn_core
*/
static errval_t create_core_data_partial(struct arm_core_data **core_data, forvaddr_t *entrypoint) {

    //create kernel control block
    struct capref kcb_ram;

    errval_t err = ram_alloc(&kcb_ram, OBJSIZE_KCB);
    if (err_is_fail(err)) {
        debug_printf("Cannot get RAM for KCB of 2nd core\n");
        return err;
    }

    struct capref kcb_cap;
    err = slot_alloc(&kcb_cap);
    if (err_is_fail(err)) {
        debug_printf("slot_alloc for KCB of 2nd core failed\n");
        return err;
    }

    err = cap_retype(kcb_cap, kcb_ram, 0, ObjType_KernelControlBlock, OBJSIZE_KCB, 1);
    if (err_is_fail(err)) {
        debug_printf("cap_retype for KCB of 2nd core failed\n");
        return err;
    }

    //retrieve core_data
    struct capref core_data_frame;
    size_t core_data_ret_size;

    err = frame_alloc(&core_data_frame, sizeof(struct arm_core_data), &core_data_ret_size);
    if (err_is_fail(err)) {
        err_push(err, LIB_ERR_FRAME_ALLOC);
        debug_printf("Cannot allocate URPC frame\n");
        return err;
    }

    err = invoke_kcb_clone(kcb_cap, core_data_frame);
    if (err_is_fail(err)) {
        debug_printf("invoke_kcb_clone for 2nd core failed\n");
        return err;
    }

    void *arm_core_data_buf;
    err = paging_map_frame_attr(get_current_paging_state(), &arm_core_data_buf, core_data_ret_size, core_data_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        debug_printf("could not map frame for core_data struct\n");
        return err;
    }

    //assign the new arm_core_data struct to out parameter
    *core_data = (struct arm_core_data *)arm_core_data_buf;

    //also fill in the kcb field of the core_data struct as we have all the necessary local variables
    struct frame_identity kcb_frame_identity;
    err = frame_identify(kcb_cap, &kcb_frame_identity);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "frame_identify failed for kcb\n");
        return err;
    }

    (*core_data)->kcb = kcb_frame_identity.base;

    //retrieve kernel-virtual address of struct
    struct frame_identity core_data_identity;
    err = frame_identify(core_data_frame, &core_data_identity);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "frame_identify failed for arm_core_data\n");
    }

    *entrypoint = core_data_identity.base;

    //apparently this field has to be from kernel-virtual space
    (*core_data)->cmdline = ((lvaddr_t) core_data_identity.base + ((lvaddr_t) (*core_data)->cmdline_buf) - ((lvaddr_t)(*core_data)));

    return SYS_ERR_OK;
}


/**
*   \brief  fills in init data for arm_core_data struct
*           consists of 2 phases:
*           1. allocate space for init data structures
*           2. pass information about init module location
*/
static errval_t core_data_fill_in_init_data(struct arm_core_data *core_data) {

    //allocate space for init
    struct capref init_frame;
    size_t init_ret_size;

    // note: we offer much more initial memory to the app core (5500 instead of 1100 pages,
    // i.e. approx 20 MiB instead of approx 4 MiB). This is based on the fact that we link
    // MM into init, too, for allowing the system running in a monolithic way.
    // And MM needs about 8 MiB of RAM alone for tracking of the allocated RAM blocks.
    // Thus, we could be less generous (change factor 4 back to 1) if we wanted only a
    // distributed system.
    #define RAM_MULTIPLIER 5

    errval_t err = frame_alloc(&init_frame, ARM_CORE_DATA_PAGES * BASE_PAGE_SIZE * RAM_MULTIPLIER, &init_ret_size);
    if (err_is_fail(err)) {
        debug_printf("Cannot allocate space for init\n");
        return err;
    }

    struct frame_identity init_frame_identity;
    err = frame_identify(init_frame, &init_frame_identity);
    if (err_is_fail(err)) {
        debug_printf("frame_identify failed for init frame\n");
        return err;
    }

    core_data->memory_base_start = init_frame_identity.base;
    core_data->memory_bytes = init_frame_identity.bytes;

    //fill in init module info
    struct mem_region *init_modinfo;
    init_modinfo = multiboot_find_module(bi, "init");
    if (!init_modinfo) {
        debug_printf("init module not found\n");
        return err;
    }

    core_data->monitor_module.mod_start = init_modinfo->mr_base;
    core_data->monitor_module.mod_end = init_modinfo->mr_base + init_modinfo->mrmod_size;
    core_data->monitor_module.string = init_modinfo->mrmod_data;

    return SYS_ERR_OK;
}


/**
 * \brief  adds the key info of the intermon UMP frame to core data
 */
static errval_t core_data_fill_in_ump_data(struct arm_core_data *core_data) {

    // fill in the core data struct members
    struct frame_identity ump_frame_identity;
    errval_t err = frame_identify(cap_urpc, &ump_frame_identity);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_identify failed for intermon UMP frame.\n");
        return err;
    }

    core_data->urpc_frame_base = ump_frame_identity.base;
    core_data->urpc_frame_size = ump_frame_identity.bytes;

    return SYS_ERR_OK;
}

static errval_t core_data_fill_in_relocatable_data(struct arm_core_data *core_data)
{
    //arguments for load_cpu_relocatable_segment()
    struct mem_region *cpu_driver_mod_info;
    cpu_driver_mod_info = multiboot_find_module(bi, "cpu_omap44xx");
    if (!cpu_driver_mod_info) {
        debug_printf("cpu driver module not found\n");
        return SPAWN_ERR_FIND_MODULE;
    }

    struct capref cpu_driver_frame_cap = {
        .cnode = cnode_module,
        .slot = cpu_driver_mod_info->mrmod_slot
    };

    void *cpu_driver_elf_data;

    errval_t err = paging_map_frame_attr(get_current_paging_state(), &cpu_driver_elf_data, cpu_driver_mod_info->mrmod_size, cpu_driver_frame_cap, VREGION_FLAGS_READ, NULL, NULL);
    if (err_is_fail(err)) {
        debug_printf("Cannot map URPC frame\n");
        return err;
    }

    struct capref reloc_frame;
    size_t reloc_ret_size;

    //TODO mlei: now we allocate way too much. size of the whole module instead of just size of segment
    err = frame_alloc(&reloc_frame, cpu_driver_mod_info->mrmod_size, &reloc_ret_size);
    if (err_is_fail(err)) {
        err_push(err, LIB_ERR_FRAME_ALLOC);
        debug_printf("Cannot allocate relocation frame\n");
        return err;
    }

    void *reloc_buf;
    err = paging_map_frame_attr(get_current_paging_state(), &reloc_buf, reloc_ret_size, reloc_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        debug_printf("Cannot map relocatable frame\n");
        return err;
    }

    struct frame_identity reloc_identity;
    err = frame_identify(reloc_frame, &reloc_identity);
        if (err_is_fail(err)) {
        debug_printf("frame_identify failed for relocation\n");
        return err;
    }

    err = load_cpu_relocatable_segment(cpu_driver_elf_data, reloc_buf, reloc_identity.base, core_data->kernel_load_base, &(core_data->got_base));
    if (err_is_fail(err)) {
        debug_printf("failed load_cpu_relocatable_segment\n");
        return err;
    }

    atomic_thread_fence_rel();
    sys_armv7_cache_clean_poc(reloc_buf, ((char *)reloc_buf) + reloc_ret_size);
    sys_armv7_cache_invalidate(reloc_buf, ((char *)reloc_buf) + reloc_ret_size);
    atomic_thread_fence_acq();

    return SYS_ERR_OK;
}

__attribute__((unused))
static void print_core_data(struct arm_core_data *core_data) {
    debug_printf("--core_data info--\n");

    debug_printf("multiboot_header: %p\n", core_data->multiboot_header);
    debug_printf("kernel_l1_low: %p\n", core_data->kernel_l1_low);
    debug_printf("kernel_l1_high: %p\n", core_data->kernel_l1_high);
    debug_printf("kernel_l2_vec: %p\n", core_data->kernel_l2_vec);
    debug_printf("entry_point: %p\n", core_data->entry_point);
    debug_printf("stack_base: %p\n", core_data->stack_base);
    debug_printf("stack_top: %p\n", core_data->stack_top);
    debug_printf("    --kernel module--\n");
    debug_printf("    mod_start: %p\n", core_data->kernel_module.mod_start);
    debug_printf("    mod_end: %p\n", core_data->kernel_module.mod_end);
    debug_printf("    string: %p\n", core_data->kernel_module.string);
    debug_printf("    reserved: %p\n", core_data->kernel_module.reserved);
    debug_printf("    --kernel module end--\n");

    debug_printf("    --kernel elf--\n");
    debug_printf("    num: %d\n", core_data->kernel_elf.num);
    debug_printf("    size: %u\n", core_data->kernel_elf.size);
    debug_printf("    addr: %p\n", core_data->kernel_elf.addr);
    debug_printf("    shndx: %p\n", core_data->kernel_elf.shndx);
    debug_printf("    --kernel elf end--\n");

    debug_printf("kcb: %p\n", core_data->kcb);
    debug_printf("cmdline_buf: %s\n", core_data->cmdline_buf);
    debug_printf("cmdline: %p\n", core_data->cmdline);
    debug_printf("urpc_frame_base: %p\n", core_data->urpc_frame_base);
    debug_printf("MON_URPC_SIZE: %p\n", core_data->urpc_frame_size);
    debug_printf("chan_id: %u\n", core_data->chan_id);

    debug_printf("    --monitor_module--\n");
    debug_printf("    mod_start: %p\n", core_data->monitor_module.mod_start);
    debug_printf("    mod_end: %p\n", core_data->monitor_module.mod_end);
    debug_printf("    string: %p\n", core_data->monitor_module.string);
    debug_printf("    reserved: %p\n", core_data->monitor_module.reserved);
    debug_printf("    --monitor_module end--\n");

    debug_printf("memory_base_start %p\n", core_data->memory_base_start);
    debug_printf("memory_bytes %p\n", core_data->memory_bytes);
    debug_printf("src_core_id: %u\n", core_data->src_core_id);
    debug_printf("dst_core_id: %u\n", core_data->dst_core_id);
    debug_printf("arch_id: %u\n", core_data->src_arch_id);
    debug_printf("global: %p\n", core_data->global);
    debug_printf("target_bootrecs: %p\n", core_data->target_bootrecs);
    //struct gnu_build_id build_id;
    debug_printf("    --build id--\n");
    debug_printf("    data: %s\n", core_data->build_id.data);
    debug_printf("    length: %u\n", core_data->build_id.length);
    debug_printf("    --build id end--\n");
    debug_printf("kernel_load_base: %p\n", core_data->kernel_load_base);
    debug_printf("got_base: %p\n", core_data->got_base);

    debug_printf("init_name: %s\n", core_data->init_name);

    debug_printf("--core data end--\n\n");

}
