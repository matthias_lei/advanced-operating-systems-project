/**
 * Utility functions used during the setup of the init domain
 *
 * Group C
 */

#ifndef USR_INIT_SETUP_H_
#define USR_INIT_SETUP_H_

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#define BSP_CORE_ID 0

#define APP_CORE_ID 1
#define APP_CORE_NAME "/core/monitor/1"

/**
 * Parses the command line arguments passed to init via usbboot
 */
errval_t parse_arguments(char *init_args);

/**
 * Called by init to set itself up on the bootstrap core. It
 * initializes the RAM allocator.
 */
void setup_init_bsp(enum rpc_services_mode mode);

/**
 * Called by init to set itself up on an application core. It
 * sets up a URPC channel to init on the first core, receives
 * bootstrapping information and configures itself according
 * to this information.
 */
void setup_init_app(routing_info_t routing);

#endif // USR_INIT_SETUP_H_
