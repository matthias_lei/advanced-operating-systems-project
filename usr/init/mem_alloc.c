/**
 * \file
 * \brief Local memory allocator for init till mem_serv is ready to use
 */
#include "mem_alloc.h"
#include <mm/mm.h>
#include <aos/paging.h>


/// MM allocator instance data
struct mm aos_mm;

static errval_t aos_ram_alloc_aligned(struct capref *ret, size_t size, size_t alignment)
{
    return mm_alloc_aligned(&aos_mm, size, alignment, ret);
}

errval_t aos_ram_free(struct capref cap)
{
    errval_t err;
    struct frame_identity fi;
    err = frame_identify(cap, &fi);
    return mm_free(&aos_mm, cap, fi.base, fi.bytes);
}

/**
 * Helper function for shared code paths. Note: it uses some statically allocated
 * local variables to accomplish the task (as before).
 *
 * *ret_init_slot_alloc returns a ptr to the init slot alloc -- except ret_init_slot_alloc was NULL
 */
static errval_t shared_ram_alloc_init(struct slot_prealloc **ret_init_slot_alloc)
{
    // Init slot allocator
    static struct slot_prealloc init_slot_alloc;
    if (ret_init_slot_alloc) {
        *ret_init_slot_alloc = &init_slot_alloc;
    }

    struct capref cnode_cap = {
        .cnode = {
            .croot = CPTR_ROOTCN,
            .cnode = ROOTCN_SLOT_ADDR(ROOTCN_SLOT_SLOT_ALLOC0),
            .level = CNODE_TYPE_OTHER,
        },
        .slot = 0,
    };
    errval_t err = slot_prealloc_init(&init_slot_alloc, cnode_cap, L2_CNODE_SLOTS, &aos_mm);
    if (err_is_fail(err)) {
        return err_push(err, MM_ERR_SLOT_ALLOC_INIT);
    }

    // Initialize aos_mm
    err = mm_init(&aos_mm, ObjType_RAM, slab_default_refill,
                  slot_alloc_prealloc, slot_prealloc_refill,
                  &init_slot_alloc);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "Can't initalize the memory manager.");
    }

    // Give aos_mm a bit of memory for the initialization
    static char nodebuf[sizeof(struct mmnode)*64];
    slab_grow(&aos_mm.slabs, nodebuf, sizeof(nodebuf));

    return SYS_ERR_OK;
}


/**
 * \brief Setups a local memory allocator for init to use till the memory server
 * is ready to be used.
 *
 * Note pisch: This currently building the main memory allocator directly inside of init
 * of the boot core.
 * This works as long as the memory service (Dionysos) is also part of this init.
 * If we want to make Dionysos modular, we should
 * - initialize only a miniature MM here that is sufficient to handle boot core init up to the moment
 *   that the memory service is available
 * - the memory service shall receive the main part of the provided memory regions and built its
 *   differentiated tracking meta data infrastructure. Note: we can even re-introduce 1 KiB min RAM sizes if we like
 *   as an option.
 * To go already towards this direction, the initialize_ram_alloc() of non-boot cores (that needs forging of a ram cap)
 * is already written separately in a function below.
 */
errval_t initialize_ram_alloc_bsp(enum rpc_services_mode mode)
{
    assert(0 == disp_get_core_id());

    errval_t err = shared_ram_alloc_init(NULL);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "Error in shared_ram_alloc_init()");
    }

    // Walk bootinfo and add all RAM caps to allocator handed to us by the kernel
    uint64_t mem_avail = 0;
    struct capref mem_cap = {
        .cnode = cnode_super,
        .slot = 0,
    };

    // NOTE: a bit hacky splitting of some memory for init at the moment
    // however, it's not fully clear how many regions are provided and how they are
    // set in the array (if this shall remain generic)

    // TODO: this value can be tuned
    // it must be enough to spawn all the services and also the 2nd core
    // must be multiple of BASE_PAGE_SIZE
    // note: each service spawned before Dionysos is on needs a contiguous 16 MiB block
    #define MEMORY_FOR_INIT (64 * 1024 * 1024)

    for (int i = 0; i < bi->regions_length; i++) {
        if (bi->regions[i].mr_type == RegionType_Empty) {
            if (bi->regions[i].mr_bytes < MEMORY_FOR_INIT) {
                continue;
            }

            // only take a bit
            // note: we can split at the beginning or at the end.
            // currently we split at the end to get away with 1 retype only instead of 2
            // additionally, it tests a code path in mm_add that has not been tested yet due
            // to the nature of the received RAM block
            // side effect: largest RAM block is 256 MiB, which does not matter because
            // we are only using self-paging with 4 KiB pages.
            // note: we can adjust this later, of course, if desired.
            struct capref init_ram;
            err = slot_alloc(&init_ram);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "slot_alloc() failed\n");
                return err;
            }

            size_t offset = bi->regions[i].mr_bytes - MEMORY_FOR_INIT;
            err = cap_retype(init_ram, mem_cap, offset, ObjType_RAM, MEMORY_FOR_INIT, 1);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "cap_retype() failed\n");
                return err;
            }

            // adjust size for loading the ram to Dionysos
            bi->regions[i].mr_bytes -= MEMORY_FOR_INIT;

            err = mm_add(&aos_mm, init_ram, bi->regions[i].mr_base + offset, MEMORY_FOR_INIT);
            if (err_is_ok(err)) {
                mem_avail += MEMORY_FOR_INIT;
            } else {
                DEBUG_ERR(err, "Warning: adding RAM region %d (%p/%zu) FAILED", i, bi->regions[i].mr_base, bi->regions[i].mr_bytes);
            }

            err = slot_prealloc_refill(aos_mm.slot_alloc_inst);
            if (err_is_fail(err) && err_no(err) != MM_ERR_SLOT_MM_ALLOC) {
                USER_PANIC_ERR(err, "in slot_prealloc_refill() while initialising"
                                    " memory allocator");
            }

            mem_cap.slot++;

            // we only take one part for local memory in init
            break;
        }
    }
    debug_printf("Added %"PRIu64" MiB of physical memory.\n", mem_avail / 1024 / 1024);

    // call added for more detailed debug output
    mm_dump_mmnodes(&aos_mm);


    // note: this is an intermediary state for init (monitor0)
    // it is needed to allow spawning of domains (which is not possible
    // with the fixed allocator that only provides 4 KiB page size RAM)
    // as soon as the memory service (Dionysos) is online
    // also init switches to that service. see bootstrap.c
    err = ram_alloc_set(aos_ram_alloc_aligned);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "Error in ram_alloc_set()\n");
    }

    return SYS_ERR_OK;
}

/**
 * pisch: this is just here to illustrate the concept. This function can be called in init
 * on both cores, once the memory service dionysos is up. Currently, it should not be used in init on core 0.
 * Please also note that some preparations for distributed services are being prepared in the new_gaia branch.
 * Please look there or ask me.
 */
void switch_memory_allocation_to_memory_service(void) {
    errval_t err = ram_alloc_set(NULL);
    if (err_is_fail(err)) {
        err = LIB_ERR_RAM_ALLOC_SET;
        USER_PANIC_ERR(err, "Cannot switch RAM allocation to memory service");
    }
}


// only used for monolithic system
#include <aos/aos_rpc.h>
#include <aos_rpc_server/aos_rpc_server_memory.h>
#include <hashtable/serializable_key_value_store.h>

void memory_service_register_callback(void)
{
    errval_t err = SYS_ERR_OK;

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/core/memory"
    #define SHORT_NAME "memory"
    #define DESCRIPTION "Memory service"
    #define PATRON "Dionysos: ancestral god of grape harvest, winemaking and wine"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return;
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_MEMORY, false,
                                   store, aos_rpc_memserver_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return;
    }
}
