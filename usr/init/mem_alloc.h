/**
 * \file
 * \brief ram allocator functions
 */

/*
 * Copyright (c) 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetsstrasse 6, CH-8092 Zurich. Attn: Systems Group.
 */

#ifndef _INIT_MEM_ALLOC_H_
#define _INIT_MEM_ALLOC_H_

#include <stdio.h>
#include <aos/aos.h>
#include <spawn_core.h>

extern struct bootinfo *bi;
extern struct mm aos_mm;

errval_t initialize_ram_alloc_bsp(enum rpc_services_mode mode);

errval_t aos_ram_free(struct capref cap);

/**
 * pisch: this is just here to illustrate the concept. This function can be called in init
 * on both cores, once the memory service dionysos is up. Currently, it should not be used in init on core 0.
 * Please also note that some preparations for distributed services are being prepared in the new_gaia branch.
 * Please look there or ask me.
 */
void switch_memory_allocation_to_memory_service(void);

// only here in case of monolithic system
void memory_service_register_callback(void);

#endif /* _INIT_MEM_ALLOC_H_ */
