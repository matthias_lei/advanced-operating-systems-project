/**
 * special spawn helper functions during bootstrap
 * AOS class group C
 * version 2017-12-03
 */

#include "bootstrap.h"
#include "setup.h"

extern struct bootinfo *bi;

#include <spawn/spawn.h>
#include <aos/connect/all.h>
#include <nameserver_types.h>

#include <aos_rpc_server/aos_rpc_server_common.h>
#include <aos_rpc_server/aos_rpc_server_device.h>
//#include <aos_rpc_server/aos_rpc_server_global.h>
//#include <aos_rpc_server/aos_rpc_server_memory.h>
#include <aos_rpc_server/aos_rpc_server_monitor.h>
#include <aos_rpc_server/aos_rpc_server_intermon.h>
//#include <aos_rpc_server/aos_rpc_server_process.h>
//#include <aos_rpc_server/aos_rpc_server_serial.h>

static errval_t register_device(void);

const char *bootstrap_domain_names[] = {
    "init",
    "services/dionysos",
    "services/gaia",
    "monitor",
    "services/demeter"
};

coreid_t bootstrap_cores[] = {
    BSP_CORE_ID,
    BSP_CORE_ID,
    BSP_CORE_ID,
    APP_CORE_ID,
    BSP_CORE_ID
};

const size_t bootstrap_domains_count = sizeof(bootstrap_domain_names) / sizeof(bootstrap_domain_names[0]);


void bootstrap_initial_services_before_core_spawn(void)
{
    errval_t err = SYS_ERR_OK;

    // assure that local has monitor privileges
    // that includes: monitor (including spawn service for core 0), intermon, device
    // see settings in the rpc server Hakefile and monitor server FG function pointer table
    struct aos_rpc *monitor_rpc = malloc(sizeof(*monitor_rpc));
    if (!monitor_rpc) {
        USER_PANIC_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local init channel\n");
    }

    init_local_channel(monitor_rpc, AOS_RPC_INTERFACE_MONITOR, &aos_rpc_monitor_local_vtable);
    set_init_rpc(monitor_rpc);

    struct aos_rpc *intermon_rpc = malloc(sizeof(*intermon_rpc));
    if (!intermon_rpc) {
        USER_PANIC_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local intermon channel\n");
    }

    init_local_channel(intermon_rpc, AOS_RPC_INTERFACE_INTERMON, &aos_rpc_intermon_local_vtable);
    set_intermon_rpc(APP_CORE_ID, intermon_rpc);

    struct aos_rpc *device_rpc = malloc(sizeof(*device_rpc));
    if (!device_rpc) {
        USER_PANIC_ERR(LIB_ERR_MALLOC_FAIL, "could not initialize local device channel\n");
    }

    init_local_channel(device_rpc, AOS_RPC_INTERFACE_DEVICE, &aos_rpc_device_local_vtable);
    set_device_rpc(device_rpc);


    // memory service
    err = spawn_bootstrap_domain_monitor_only(bootstrap_domain_names[1], 1);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not launch memory service\n");
    }

    // easiest synchronisation possible (well, maybe 2nd easiest :-) )
    struct waitset *default_ws = get_default_waitset();
    while (!bootstrap_memory_ready) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch\n");
        }
    }

    // create RPC
    struct aos_rpc *memory_rpc;
    err = aos_rpc_connect_with_service(bootstrap_memory_service_cap, AOS_RPC_INTERFACE_MEMORY, AOS_RPC_CHAN_DRIVER_LMP,
                                       AOS_RPC_ROUTING_SET(0, 0), &memory_rpc, NULL);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not connect with memory service\n");
    }
    set_memory_rpc(memory_rpc);
    monitor_service_rpcs[AOS_RPC_INTERFACE_MEMORY] = memory_rpc;

    // Walk bootinfo and add all RAM caps to allocator handed to us by the kernel
    uint64_t mem_avail = 0;
    struct capref mem_cap = {
        .cnode = cnode_super,
        .slot = 0,
    };

    for (int i = 0; i < bi->regions_length; i++) {
        if (bi->regions[i].mr_type == RegionType_Empty) {
            debug_printf("Adding %" PRIu64 " MiB of physical memory to Dionysos.\n", bi->regions[i].mr_bytes / 1024 / 1024);

            err = aos_rpc_load_ram(memory_rpc, mem_cap, bi->regions[i].mr_base, bi->regions[i].mr_bytes);
            if (err_is_ok(err)) {
                mem_avail += bi->regions[i].mr_bytes;
            } else {
                DEBUG_ERR(err, "Warning: adding RAM region %d (%p/%zu) FAILED\n", i, bi->regions[i].mr_base, bi->regions[i].mr_bytes);
            }

            mem_cap.slot++;
        }
    }

    // switch memory allocation to Dionysos lookups
    // note: no late switching for monitors because they need
    // different RAM sizes early on. Thus: better check & test here that all works
    err = ram_alloc_set(NULL);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "ram_alloc_set() failed.\n");
    }

    // test memory system
    debug_printf("Testing memory service\n");
    void *test = calloc(1, 8000);
    if (!test) {
        USER_PANIC_ERR(LIB_ERR_MALLOC_FAIL, "memory system not fully working\n");
    }
    free(test);

    debug_printf("Memory system tested. Allocation switched from builtin to Dionysos\n");


    // name service
    err = spawn_bootstrap_domain_monitor_and_memory(bootstrap_domain_names[2], 2);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not launch name service\n");
    }

    // again for nameserver
    while (!bootstrap_name_ready) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch\n");
        }
    }

    struct aos_rpc *name_rpc;
    err = aos_rpc_connect_with_service(bootstrap_name_service_cap, AOS_RPC_INTERFACE_NAME, AOS_RPC_CHAN_DRIVER_LMP,
                                       AOS_RPC_ROUTING_SET(0, 0), &name_rpc, NULL);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "aos_rpc_connect_with_service() failed.\n");
    }
    set_name_rpc(name_rpc);
    monitor_service_rpcs[AOS_RPC_INTERFACE_NAME] = name_rpc;

    // register init == monitor/0
    err = register_monitor();
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "register_monitor() failed.\n");
    }

    // register device/0
    err = register_device();
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "register_device() failed.\n");
    }

    // make memory service register: first get a fresh endpoint
    struct capref fresh_name_cap;
    err = aos_rpc_request_new_endpoint(name_rpc, AOS_RPC_CHAN_DRIVER_LMP, &fresh_name_cap);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "aos_rpc_request_new_endpoint() failed.\n");
    }

    err = aos_rpc_memoryservice_register(memory_rpc, fresh_name_cap);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "aos_rpc_memoryservice_register() failed.\n");
    }
}

void bootstrap_initial_services_after_core_spawn(void)
{
    // process service
    errval_t err = spawn_load_by_name_with_id(bootstrap_domain_names[4], -1, 4);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not launch process service\n");
    }

    // process server gets info of spawned domains (see callback to monitor for lookup)
    // works well!

    // same synchronization as before
    struct waitset *default_ws = get_default_waitset();
    while (!bootstrap_process_ready) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch\n");
        }
    }

    // launch hermes by regular use of the process (Demeter) service
    // all future services / domains can be launched by Demeter now
    // also on core 0 to start with
    struct aos_rpc *process_rpc = aos_rpc_get_process_channel();
    if (!process_rpc) {
        // TODO: find better error code
        err = LIB_ERR_MALLOC_FAIL;
        USER_PANIC_ERR(err, "could not connect with process channel\n");
    }

    domainid_t newpid;
    err = aos_rpc_process_spawn(process_rpc, "services/hermes", BSP_CORE_ID, &newpid);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "could not launch serial I/O service\n");
    }

    if (newpid != 5) {
        debug_printf("%s: wrong pid for I/O service (Hermes). pid %zu; expected 5. Configuration error?\n",
                     __func__, (size_t)newpid);
    }

    // force synchronization barrier for later launches. note: the getter must be configured
    // to be blocking for this (see implementation)
    // this can also be used as a templater for later synced service launches
    struct aos_rpc *hermes = aos_rpc_get_serial_channel();
    if (!hermes) {
        USER_PANIC_ERR(LIB_ERR_SHOULD_NOT_GET_HERE, "could not establish connection with serial channel");
    }

    err  = aos_rpc_process_spawn(process_rpc, "services/charon", BSP_CORE_ID, &newpid);
    if (newpid != 6) {
        debug_printf("%s: wrong pid for network service (Charon). pid %zu; expected 6. Configuration error?\n",
                     __func__, (size_t)newpid);
    }

    domainid_t mmchs_pid;
    err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "mmchs", disp_get_core_id(), &mmchs_pid);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "aos_rpc_process_spawn failed to spawn mmchs.\n");
    }

    // wait for blockdev service to initialize
    struct aos_rpc *electra = aos_rpc_get_blockdev_channel();
    if (!electra) {
        USER_PANIC_ERR(LIB_ERR_SHOULD_NOT_GET_HERE, "could not establish connection to blockdev channel");
    }

    domainid_t filesystem_pid;
    err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "services/plutos", disp_get_core_id(), &filesystem_pid);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "aos_rpc_process_spawn failed to spawn plutos.\n");
    }

    // wait for filesystem service to initialize
    struct aos_rpc *plutos = aos_rpc_get_filesystem_channel();
    if (!plutos) {
        USER_PANIC_ERR(LIB_ERR_SHOULD_NOT_GET_HERE, "could not establish connection to filesystem channel");
    }
}


// can be used for both monitors!
errval_t register_monitor(void)
{
    errval_t err = SYS_ERR_OK;

    debug_printf("Poseidon on core %zu wishes you Fair Winds and Following Seas.\n", (size_t)disp_get_core_id());

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/core/monitor"
    #define SHORT_NAME "monitor"
    #define DESCRIPTION "Monitor service; includes Intermon service"
    #define PATRON "Poseidon: God of the Sea and other waters, protector of seafarers: Fair Winds and Following Seas."
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return err;
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_MONITOR, false,
                                   store, aos_rpc_monitor_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return err;
    }

    return SYS_ERR_OK;
}


static errval_t register_device(void)
{
    errval_t err = SYS_ERR_OK;

    debug_printf("Prometheus brings you devices with fire.\n");

    // registration: 1) of the event handler and 2) with the nameserver
    #define DEV_ABS_PATH "/core/device"
    #define DEV_SHORT_NAME "device"
    #define DEV_DESCRIPTION "Device service"
    #define DEV_PATRON "Prometheus: Titan who brought fire to humans"
    #define DEV_CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(DEV_CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return err;
    }

    kv_store_set(store, "description", DEV_DESCRIPTION);
    kv_store_set(store, "patron", DEV_PATRON);
    // TODO: add more meta data

    struct service_registration_reply *reply;
    // NOTE: as long as device is embedded in monitor/0, we need to register
    // aos_rpc_monitor_event_handler as event handler, which is adjusted accordingly
    // to also handle device FG.
    err = aos_rpc_service_register(DEV_ABS_PATH, DEV_SHORT_NAME, AOS_RPC_INTERFACE_DEVICE, false,
                                   store, aos_rpc_monitor_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return err;
    }

    return SYS_ERR_OK;
}
