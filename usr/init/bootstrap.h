/**
 * special spawn helper functions during bootstrap
 * AOS class group C
 * version 2017-12-03
 */

#ifndef USR_INIT_BOOTSTRAP_H_
#define USR_INIT_BOOTSTRAP_H_

#include <aos/aos.h>

#include <aos_rpc_server/aos_rpc_server_monitor.h>

extern const char *bootstrap_domain_names[];
extern coreid_t bootstrap_cores[];
extern const size_t bootstrap_domains_count;

void bootstrap_initial_services_before_core_spawn(void);

void bootstrap_initial_services_after_core_spawn(void);

errval_t register_monitor(void);

#endif // USR_INIT_BOOTSTRAP_H_
