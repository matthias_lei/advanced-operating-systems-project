/**
 * Group C: some tests that needed to run in init
 * They are typically deactivated in the merged/final version
 */

#ifndef USR_INIT_TESTS_H_
#define USR_INIT_TESTS_H_

void quick_ump_chan_test(void);

void quick_irq_test(void);

void quick_slip_test(void);

void quick_ip_impl_test(void);

void quick_ip_prot_test(void);

void quick_waitset_test(void);

void quick_udp_impl_test(void);

void quick_socket_interface_local_test(void);

void quick_udp_echo_server_spawn_test(void);

#endif // USR_INIT_TESTS_H_
