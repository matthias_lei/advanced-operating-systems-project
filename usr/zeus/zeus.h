/**
 *  \file
 *  \brief Shell "Zeus"
 */

#ifndef USR_ZEUS_SHELL_H_
#define USR_ZEUS_SHELL_H_

#define SHELL_MAX_ARG_LENGTH 200

typedef int (*shell_fun)(int argc, char **argv, FILE *std_in, FILE *std_out);

struct aos_shell_command {
    char name[SHELL_MAX_ARG_LENGTH];
    shell_fun fun;
};

#endif // USR_ZEUS_SHELL_H_
