/**
 *  \file
 *  \brief Environment variables
 */

#ifndef USR_ZEUS_ENVIRONMENT_H_
#define USR_ZEUS_ENVIRONMENT_H_

struct env_variable {
    char *name;
    char *val;
};

//extern struct env_variable env_variables[];

char** serialize_env_variables(void);

#endif // USR_ZEUS_ENVIRONMENT_H_
