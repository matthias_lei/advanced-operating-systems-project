/**
 *  \file
 *  \brief Informations about built in commands displayed via option -h
 */

#include <aos/aos.h>
#include <hashtable/hashtable.h>

#include "help.h"

// TODO maybe try to put the "help" information into already existing command hashtable

/**
 *  Options
 */

// RUN
static const option_descr_t run_option[] =
{
    {
        .name = "b",
        .description = "If set, program is run in background, i.e. in its own window."
    },
    {
        .name = "c 0|1",
        .description = "Program is run on core 0 or 1. Default is core 0."
    },
    {
        .name = "s",
        .description = "Shows the menu of avail. programs in the multiboot image and the chars to launch them."
    }

};

#define RUN_OPTION_LEN (sizeof(run_option) / sizeof(option_descr_t))

//RM
static const option_descr_t rm_option[] =
{
    {
        .name = "r",
        .description = "Works also on directories, removes directory recursively."
    }
};

#define RM_OPTION_LEN (sizeof(rm_option) / sizeof(option_descr_t))


/**
 *  Command infos
 */

command_info_t command_infos[] =
{
    {
        .name = "echo",
        .options = NULL,
        .options_len = 0,
        .arg = "[STRING]",
        .description = "Writes [STRING] to the console."
    },
    {
        .name = "run",
        .options = run_option,
        .options_len = RUN_OPTION_LEN,
        .arg = "[PROGRAM]",
        .description = "Runs [PROGRAM]."
    },
    {
        .name = "cd",
        .options = NULL,
        .options_len = 0,
        .arg = "[DIRECTORY]",
        .description = "Changes current working directory to [DIRECTORY]."
    },
    {
        .name = "ls",
        .options = NULL,
        .options_len = 0,
        .arg = "[DIRECTORY]..",
        .description = "Lists content of directories [DIRECTORY].. ."
    },
    {
        .name = "mkdir",
        .options = NULL,
        .options_len = 0,
        .arg = "[DIRECTORY]..",
        .description = "Creates directories [DIRECTORY].. ."
    },
    {
        .name = "touch",
        .options = NULL,
        .options_len = 0,
        .arg = "[FILE]..",
        .description = "Creates file [FILE].. ."
    },
    {
        .name = "rm",
        .options = rm_option,
        .options_len = RM_OPTION_LEN,
        .arg = "[FILE]..",
        .description = "Deletes file [FILE].. ."
    },
    {
        .name = "rmdir",
        .options = NULL,
        .options_len = 0,
        .arg = "[DIRECTORY]..",
        .description = "Deletes directories [DIRECTORY].. ."
    },
    {
        .name = "cat",
        .options = NULL,
        .options_len = 0,
        .arg = "[FILE]..",
        .description = "Display contents of [FILE].. ."
    },
    {
        .name = "mount",
        .options = NULL,
        .options_len = 0,
        .arg = "[MOUNTPOINT] [URI]",
        .description = "Mounts the filesystem [URI] at [MOUNTPOINT]. Available filesystems are \"multiboot://\" and \"mmchs\""
	},
	{
        .name = "cp",
        .options = NULL,
        .options_len = 0,
        .arg = "SOURCE DEST",
        .description = "Copy SOURCE to directory DEST. Note that DEST must already exist."
    }

    // TODO add more
};

static const int num_shell_commands = sizeof(command_infos) / sizeof(command_infos[0]);


/*
 *  Hashtable
 *
 *  maps command names to structs containing command information.
*/

static struct hashtable *command_info_table;

static int put_command_info(struct hashtable *ht, char *name, command_info_t *command_info) {
    return ht->d.put_word(&ht->d, name, strlen(name), (uintptr_t) command_info);
}

static ENTRY_TYPE get_command_info(struct hashtable *ht, char *name, command_info_t **command_info) {
    return ht->d.get(&ht->d, name, strlen(name), (void **) command_info);
}

static int fill_table(void) {
    for (size_t i = 0; i < num_shell_commands; i++) {
        int err = put_command_info(command_info_table,
                            command_infos[i].name,
                            &command_infos[i]);
        if (err != 0) {
            debug_printf("%s: put_command failed.\n");
            return err;
        }
    }

    return 0;
}

// ----------------------------------------------------------------------------



int init_command_info(void) {
    // capacity mainly denotes number of buckets and should be prime
    command_info_table = create_hashtable2(23, 75);

    int err2 = fill_table();
    if (err2 != 0) {
        debug_printf("Could not fill hashtable.\n");
        return 1;
    }

    return 0;
}

static void print_command_info(command_info_t ci, FILE *std_out) {
    // usage
    fprintf(std_out, "Usage: %s  %s%s\n", ci.name, ci.options == NULL ? "" : "[OPTION]  ", ci.arg);

    // description
    fprintf(std_out, "%s\n", ci.description);

    // options
    if (ci.options != NULL) {
        fprintf(std_out, "OPTIONS:\n");
        size_t len = ci.options_len;
        for (size_t i = 0; i < len; i++) {
            fprintf(std_out, "-%s\t\t%s\n", ci.options[i].name, ci.options[i].description);
        }
    }

    return;
}

void print_usage(char *command_name, FILE *std_out) {
    command_info_t *ci;
    get_command_info(command_info_table, command_name, &ci);

    print_command_info(*ci, std_out);

    return;
}
