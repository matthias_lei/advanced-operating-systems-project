/**
 *  \file
 *  \brief Informations about built in commands displayed via option -h
 */

#ifndef USR_ZEUS_HELP_H_
#define USR_ZEUS_HELP_H_

struct option_descr {
    char* name;
    char* description;
};

typedef struct option_descr option_descr_t;

struct command_info {
    char *name;
    char *arg;
    char *description;
    const option_descr_t *options;
    const size_t options_len;
};

typedef struct command_info command_info_t;

int init_command_info(void);
void print_usage(char* command, FILE *std_out);

extern command_info_t command_infos[];

#endif // USR_ZEUS_HELP_H_
