/**
 *  \file
 *  \brief program manager
 *
 *  Basically all functionality of original domain manager that is still useful
 *  for shell Zeus.
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include <aos/aos_rpc.h>
#include "program_manager.h"

// managing of running domains adapted from original domain manager

programdef_t programs[] = {
    {   .name = "artemis" },
    {   .name = "tests/hello" },
    {   .name = "tests/memeater" },
    {   .name = "zeus" },
    {   .name = "tests/t02_spawn_args_passing" },
    {   .name = "tests/t02_spawn_infinitely" },
    {   .name = "tests/t02_spawn_two_1" },
    {   .name = "tests/t02_spawn_two_2" },
    {   .name = "tests/t04_bigmalloc" },
    {   .name = "tests/t04_nullpointer" },
    {   .name = "tests/t04_paging_state_transfer" },
    {   .name = "tests/t04_stackeater" },
    {   .name = "tests/t04_threads" },
    {   .name = "tests/t05_stack_trace_recursion" },
    {   .name = "tests/t_windowing_system_1" },
    {   .name = "tests/t_windowing_system_2" },
    {   .name = "tests/test_stat_functions" },
    {   .name = "tests/net_udp_echo_server"},
    {   .name = "apollo" },
    {   .name = "tests/test_service" },

    /* add more structs into this array with more programs */
    {   .name = "serialtest" },
    {   .name = "memtest" },
    {   .name = "memtest_mt" },
    {   .name = "mem_if" },
    {   .name = "spawntest" },
    {   .name = "procutils" },
    {   .name = "rpctest" },
    {   .name = "m7_fs" },
    {   .name = "m2_test" },
    {   .name = "m2_echo_args" },
    {   .name = "spin" },
    {   .name = "m6_test" },
};

const int programs_size = sizeof(programs) / sizeof(programs[0]);

STATIC_ASSERT(sizeof(programs) / sizeof(programs[0]) <= sizeof(control_chars) - 1, "Not enough control characters available!");

//--- list helper to cache info that was received from Demeter -------------------------------------
// some internal static values to allow domainman to work
// technical note: in contrast to the domainman in the low-level test menu, that is using a list,
// we can use an array here for the domain nodes. After lookup of all pids, we get to know
// the current numer of processes.

static domaininfo_t *domaininfo_array = NULL;
static size_t domaininfo_array_size = 0;

//--- interface with Demeter process service -------------------------------------------------------

struct aos_rpc *process_rpc = NULL;

void init_program_manager(void) {
    process_rpc = aos_rpc_get_process_channel();
}

void clean_domaininfo_list(void) {
    if (domaininfo_array) {
        for (int i = 0; i < domaininfo_array_size; i++) {
            if (domaininfo_array[i].name) {
                free(domaininfo_array[i].name);
            }
        }

        free(domaininfo_array);
        domaininfo_array = NULL;
        domaininfo_array_size = 0;
    }
}


static int domain_id_cmp(const void *a_, const void *b_) {
    const domainid_t *a = a_;
    const domainid_t *b = b_;
    return (*a < *b) ? -1 :
           (*a > *b) ? 1 : 0;
}

__attribute__((unused))
errval_t prepare_domaininfo_list(void) {
    assert(process_rpc);
    assert(!domaininfo_array);

    // note: according to API, the RPC creates the list; but it will be freed here after use.
    // as discussed: we cannot assume that this list is sorted.
    domainid_t *domain_ids = NULL;
    size_t domain_ids_size = 0;
    errval_t err = aos_rpc_process_get_all_pids(process_rpc, &domain_ids, &domain_ids_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s(): error while calling aos_rpc_process_get_all_pids()\n", __func__);
        return err;
    }

    if (domain_ids_size == 0) {
        // nothing to do; just free any memory if it was sent by Demeter
        // (not sure how it is implemented there in this corner case)
        // thus: we are save here.
        if (domain_ids) {
            free(domain_ids);
        }
        return SYS_ERR_OK;
    }

    assert(domain_ids);
    domaininfo_array = calloc(domain_ids_size, sizeof(domaininfo_t));
    if (!domaininfo_array) {
        debug_printf("%s(): not enough heap memory for allocation of a new domaininfo_array.\n", __func__);
        return LIB_ERR_MALLOC_FAIL;
    }
    domaininfo_array_size = domain_ids_size;

    // sort the list
    qsort(domain_ids, domain_ids_size, sizeof(domainid_t), domain_id_cmp);

    for (size_t i = 0; i < domain_ids_size; i++) {
        // according to the documentation, the memory for the name is allocated by rpc
        // and must be freed by
        domainid_t current = domain_ids[i];
        domaininfo_array[i].domain_id = current;
        err = aos_rpc_process_get_name(process_rpc, current, &domaininfo_array[i].name);
        // note: it is assumed that the received string pointer is NULL in case of an error
        // in RPC that did not lead to a malloc()
        // in case name != NULL -> assumed that there was a successful alloc -> free() call in cleanup
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "%s(): error while calling aos_rpc_process_get_name()\n", __func__);
            clean_domaininfo_list();
            // note: goal is to get a complete list or no list (atomic)
            return err;
        }

        assert(domaininfo_array[i].name);
    }

    return SYS_ERR_OK;
}

__attribute__((unused))
void kill_program(domaininfo_t *current) {
    assert(process_rpc);

    //printf("\n" RED "killing %4" PRIuDOMAINID " %s" WHITE "\n", current->domain_id, current->name);

    // TODO: adjust as soon as a KILL RPC is available in the RPC API
    printf("NOTE: killing domains is not yet available via RPC.\n"
           "Please use the domain manager in the low-level test suite\n"
           "of Milestone 2 to test this feature at the moment.\n");


    // TODO pisch: we should have some check to distinguish the programs that can be killed
    // by the user and others that are protected (like our services or init)
    // such a check is best done in Demeter, of course.
    // Thus, it can already be handled save now (by just receiving an error code).
    // But with a lookup info, we could already adjust the UI providing a nicer user experience.

    // note: no domaininfo list management needed here. The list will be updated
    // in the next iteration with information directly received from Demeter process service
}
