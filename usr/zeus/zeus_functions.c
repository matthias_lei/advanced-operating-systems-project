/**
 *  \file
 *  \brief Zeus functions
 *
 *  Built-in commands of our shell "Zeus"
 */

#include <stdlib.h>
#include <getopt.h>


#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/deferred.h>

#include <fs/dirent.h>

#include <shell_shared.h>

#include "zeus_functions.h"
#include "help.h"

/**
 *  Initialization
**/

static void init_run_menu(void) {
    for (int i = 0; i < programs_size; i++) {
        programs[i].launch = control_chars[i];
    }
}

void initialize_shell_commands(void) {
    assert(demeter);
    assert(hermes);
    // help menus
    init_command_info();
    // run command
    init_run_menu();
    // program manager
    init_program_manager();
}

/**
 *  Public helper functions
 **/


static errval_t concatenate_paths(char *path_prefix, char *path_suffix, char **ret_path);

// returned string has to be freed by caller!
errval_t to_full_path(char *shell_path, char **ret_path) {
    assert(shell_path);
    errval_t err = SYS_ERR_OK;

    char *full_path = NULL;

    switch(shell_path[0]) {
        // absolute path
        case '/':
            full_path = malloc(strlen(shell_path) + 1);
            if (!full_path) {
                err = LIB_ERR_MALLOC_FAIL;
                DEBUG_ERR(err, "malloc failed.\n");
                return err;
            }
            strcpy(full_path, shell_path);
            break;
        case '~':
            if (shell_path[1] == '/' || shell_path[1] == '\0') {
                char *home = getenv("HOME");
                if (!home) {
                    err = LIB_ERR_SHOULD_NOT_GET_HERE;
                    DEBUG_ERR(err, "getenv() returned NULL.\n");
                    return err;
                }
                err = concatenate_paths(home, &shell_path[1], &full_path);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "concatenate_paths failed.\n");
                    return err;
                }
            }
            else {
                char *pwd = getenv("PWD");
                if (!pwd) {
                    err = LIB_ERR_SHOULD_NOT_GET_HERE;
                    DEBUG_ERR(err, "getenv() returned NULL.\n");
                    return err;
                }
                err = concatenate_paths(pwd, shell_path, &full_path);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "concatenate_paths failed.\n");
                    return err;
                }
            }
            break;
        case '.':
            switch(shell_path[1]) {
                // .
                case '\0':
                // fall through
                case '/':
                {
                    char *pwd = getenv("PWD");
                    if (!pwd) {
                        err = LIB_ERR_SHOULD_NOT_GET_HERE;
                        DEBUG_ERR(err, "getenv() returned NULL.\n");
                        return err;
                    }
                    err = concatenate_paths(pwd, &shell_path[1], &full_path);
                    if (err_is_fail(err)) {
                        DEBUG_ERR(err, "concatenate_paths failed.\n");
                        return err;
                    }
                    break;
                }
                // ..
                // we do not consider .. as part of filepaths
                case '.':
                {
                    char *rel_dir = getenv("PWD");
                    if (!rel_dir) {
                        err = LIB_ERR_SHOULD_NOT_GET_HERE;
                        DEBUG_ERR(err, "getenv() returned NULL.\n");
                        return err;
                    }
                    char *last_dir = strrchr(rel_dir, '/');
                    full_path = calloc((last_dir - rel_dir) + 2, 1);
                    if (!full_path) {
                        err = LIB_ERR_MALLOC_FAIL;
                        DEBUG_ERR(err, "malloc failed.\n");
                        return err;
                    }
                    strncpy(full_path, rel_dir, MAX((last_dir - rel_dir), 1));
                    break;
                }
                // just name starting with .
                default:
                {
                    char *pwd = getenv("PWD");
                    if (!pwd) {
                        err = LIB_ERR_SHOULD_NOT_GET_HERE;
                        DEBUG_ERR(err, "getenv() returned NULL.\n");
                        return err;
                    }
                    err = concatenate_paths(pwd, shell_path, &full_path);
                    if (err_is_fail(err)) {
                        DEBUG_ERR(err, "concatenate_paths failed.\n");
                        return err;
                    }
                    break;
                }
            }
            break;
        default:
        {
            char *pwd = getenv("PWD");
            if (!pwd) {
                err = LIB_ERR_SHOULD_NOT_GET_HERE;
                DEBUG_ERR(err, "getenv() returned NULL.\n");
                return err;
            }
            err = concatenate_paths(pwd, shell_path, &full_path);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "concatenate_paths failed.\n");
                return err;
            }
            break;
        }
    }

    //debug_printf("%s     ---->       %s\n", shell_path, full_path);

    *ret_path = full_path;

    return SYS_ERR_OK;
}

static uint8_t char_to_number(char c);

bool to_number(char *str, uint8_t *ret_n) {
    *ret_n = 0;
    while(*str) {
        uint8_t val = char_to_number(*str);
        if (val == 0xFF) {
            return false;
        }
        *ret_n *= 10;
        *ret_n += val;
        str++;
    }
    return true;
}

static size_t get_dir_size(char *full_path);

// assumes full file path is inserted
errval_t list_dir_contents(char *path, char ***ret_dir_contents, size_t *size, bool enable_error_output) {
    errval_t err = SYS_ERR_OK;

    char *full_path;
    err = to_full_path(path, &full_path);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "to_full_path failed.\n");
        return err;
    }

    size_t dir_size = get_dir_size(full_path);

    fs_dirhandle_t dir_handle;
    err = opendir(full_path, &dir_handle);
    if (err_no(err) == FS_ERR_NOTDIR) {
        if (enable_error_output) printf("Error: '%s' is not a directory.\n", path);
        free(full_path);
        return err;
    }
    if (err_no(err) == FS_ERR_NOTFOUND) {
        if (enable_error_output) printf("Error: did not find directory '%s'.\n", path);
        free(full_path);
        return err;
    }
    else if (err_is_fail(err)) {
        if (enable_error_output) printf("Error: Failed to open directory '%s'.\n", path);
        free(full_path);
        return err;
    }

    free(full_path);

    *ret_dir_contents = malloc(dir_size * sizeof(char*));
    for (size_t i = 0; i < dir_size; i++) {
        char *name;
        err = readdir(dir_handle, &name);
        if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
            break;
        }
        else if(err_is_fail(err)) {
            return err;
        }

        (*ret_dir_contents)[i] = name;
    }

    err = closedir(dir_handle);
    if (err_is_fail(err)) {
        return err;
    }

    *size = dir_size;

    return SYS_ERR_OK;
}

// --------------------------- actual shell commands ------------------------//

// HELP

// TODO iterate over some structure where all commands are stored.
static char* help_string =  "Available commands:\n\n" \
                            "echo\n" \
                            "help\n" \
                            "run\n" \
                            "cd\n" \
                            "ls\n" \
                            "mkdir\n" \
                            "rm\n" \
                            "rmdir\n" \
                            "touch\n" \
                            "cat\n" \
                            "mount\n" \
                            "cp\n" \
                            "apollo\n\n" \
                            "Get more information about a command using the option -h.";

int help(int argc, char **argv, FILE *std_in, FILE *std_out) {
    fprintf(std_out, "%s\n", help_string);
    return 0;
}



// ECHO

/*
 *  echo string to the console
 *
 *  echo [string]
 *
 *  OPTIONS:
 *
 *  -h      display help
*/
int echo(int argc, char **argv, FILE *std_in, FILE *std_out) {

    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("echo", std_out);
        return 0;
    }

    for (size_t i = optind; i < argc; i++) {
        fprintf(std_out, "%s ", argv[i]);
    }

    return 0;
}

// RUN

// Helpers

struct process_config current_process_config;
static coreid_t core = 1;
bool waiting_for_termination;

static uint8_t char_to_number(char c) {
    if (c >= '0' && c <= '9') {
        return (uint8_t) c - '0';
    }

    return 0xFF;
}

static char* find_program(char c) {
    int found = -1;
    for (int i = 0; i < programs_size; i++) {
        if (c == programs[i].launch) {
            found = i;
            break;
        }
    }

    return found == -1 ? NULL : programs[found].name;

}


static int launch_program(bool from_menu) {
    assert(demeter);
    errval_t err = SYS_ERR_OK;
    domainid_t domain_id;

    if (from_menu) {
        // always run shell in background
        if (!strncmp(current_process_config.name, "zeus", 4)) {
            current_process_config.background = true;
            //current_process_config.core = 0;
        }
        err = aos_rpc_process_spawn_from_shell(demeter,
                                    current_process_config.name,
                                    current_process_config.core,
                                    current_process_config.shell_id,
                                    &domain_id);
        if (err_is_fail(err)) {
            debug_printf("spawn_from_shell failed.\n");
            return 1;
        }
    }
    else {
        // always run shell in background
        char *name = strrchr(current_process_config.path, '/') + 1;
        if (!strncmp(name, "zeus", 4)) {
            current_process_config.background = true;
            //current_process_config.core = 0;
        }
        err = aos_rpc_process_spawn_from_file(demeter,
                                            current_process_config.path,
                                            current_process_config.command_line,
                                            current_process_config.core,
                                            current_process_config.shell_id,
                                            &domain_id);
        if (err_is_fail(err)) {
            debug_printf("spawn_from_file failed.\n");
            return 1;
        }
    }

    return 0;
}

static void show_menu(void) {
    printf("Programs available in multiboot image.\n");
    printf("A program can be started by passing its assigned character to run.\n");
    for (int i = 0; i < programs_size; i++) {
        printf(" %c %s\n", programs[i].launch, programs[i].name);
    }
}


static int check_binary_file(char *path) {
    FILE *file = fopen(path, "r");
    if (!file) {
        printf("Error: Could not open file %s.\n", path);
        return 1;
    }

    // check magic byte of ELF file
    size_t elf_mb_len = 4;
    char *buf = calloc(elf_mb_len + 1, 1);
    if (!buf) {
        fclose(file);
        printf("Error: Could not allocate buffer to read file %s\n", path);
        return 1;
    }
    size_t read_size = fread(buf, 1, elf_mb_len, file);
    fclose(file);
    if (read_size < elf_mb_len) {
        free(buf);
        printf("Error: Could not read all of file %s\n", path);
        return 1;
    }

    char magic_byte[] = {0x7f, 'E', 'L', 'F'};
    if (strncmp(magic_byte, buf, elf_mb_len)) {
        printf("Error: File %s is not a ELF binary file.\n", path);
        free(buf);
        return 1;
    }

    return 0;
}

static inline void cleanup_process_config(void) {
    if (current_process_config.std_in_path) {
        free(current_process_config.std_in_path);
        current_process_config.std_in_path = NULL;
    }

    if (current_process_config.std_out_path) {
        free(current_process_config.std_out_path);
        current_process_config.std_out_path = NULL;
    }

    if (current_process_config.path) {
        free(current_process_config.path);
        current_process_config.path = NULL;
        current_process_config.command_line = NULL;
    }

    return;
}

/*
 * run programs from the multiboot image
 *
 * run [OPTIONS...] [char specifying which multiboot program in list]
 *
 * options:
 * -b               launch as background process
 * -c [core id]     launch on core [core id]
 * -s               show menu of available programs
 * -h               display help
*/
int run(int argc, char **argv, FILE *std_in, FILE *std_out) {

    bool background_flag = false;
    bool core_flag = false;
    bool menu_flag = false;
    bool help_flag = false;
    bool fail_flag = false;

    // have to close theses filehandles here, because
    // these files will be opened in the spawned processes

    // 0 : STDIN_FILENO
    // we cannot include <unistd.h> due to naming clashes with fs functions
    if (std_in->_file != 0) {
        int err = fclose(std_in);
        if (err) {
            debug_printf("fclose failed.\n");
        }
        std_in->_file = 0;
    }

    // 1 : STDOUT_FILENO
    // we cannot include <unistd.h> due to naming clashes with fs functions
    if (std_out->_file != 1) {
        int err = fclose(std_out);
        if (err) {
            debug_printf("fclose failed.\n");
        }
        std_out->_file = 1;
    }

    int option = 0;
    while ((option = getopt(argc, argv, "bc:sh")) != -1) {
        switch(option) {
            case 'b':
                background_flag = true;
                break;
            case 'c':
                core_flag = true;
                core = (coreid_t) char_to_number(*optarg);
                if (core > 1) {
                    printf("Invalid core id.\n");
                    fail_flag = true;
                }
                break;
            case 's':
                menu_flag = true;
                break;
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", option);
                fail_flag = true;
        }
    }

    if (fail_flag) {
        return 1;
    }

    if (help_flag) {
        print_usage("run", std_out);
        return 0;
    }

    if (menu_flag) {
        show_menu();
        return 0;
    }

    // still give possibility to select program via launch chars
    bool from_menu = false;
    if (strlen(argv[optind]) == 1) {
        from_menu = true;
        char *program = find_program(*(argv[optind]));
        if (!program) {
            printf("Could not find program at menu point %c.\n", *argv[argc - 1]);
            return 1;
        }
        current_process_config.name = program;
    }
    // spawn from file
    else {
        // handle case of no arguments
        size_t path_len = MIN(strlen(argv[optind]), strchr(argv[optind], ' ') - argv[optind]);
        char *path = calloc(path_len + 1, 1);
        if (!path) {
            printf("Error: Calloc failed.\n");
            return 1;
        }
        strncpy(path, argv[optind], path_len);
        char *full_path;
        errval_t err = to_full_path(path, &full_path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            free(path);
            return 1;
        }

        int erri = check_binary_file(full_path);
        if (erri != 0) {
            free(path);
            free(full_path);
            return erri;
        }

        free(path);

        current_process_config.path = full_path;

         // binary name + arguments
        current_process_config.command_line = strrchr(current_process_config.path, '/') + 1;

    }

    // use the core of the Zeus instance as default core ID
    core = core_flag ? core : disp_get_core_id();

    current_process_config.background = background_flag;
    current_process_config.core = core;

    if (current_process_config.std_in_path) {
        char *std_in_path;
        errval_t err = to_full_path(current_process_config.std_in_path, &std_in_path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            cleanup_process_config();
            return 1;
        }
        current_process_config.std_in_path = std_in_path;
    }
    if (current_process_config.std_out_path) {
        char* std_out_path;
        errval_t err = to_full_path(current_process_config.std_out_path, &std_out_path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            cleanup_process_config();
            return 1;
        }
        current_process_config.std_out_path = std_out_path;
    }

    int err = launch_program(from_menu);
    if (err != 0) {
        printf("Error: Spawning of process failed.\n");
        cleanup_process_config();
        return 1;
    }

    waiting_for_termination = true;

    // react to events
    struct waitset *default_ws = get_default_waitset();
    while (waiting_for_termination) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }

    if (!background_flag) {
        // grab window back
        err = aos_rpc_serial_get_window(hermes, disp_get_domain_id());
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "rpc_serial_get_window failed.\n");
            cleanup_process_config();
            return 1;
        }
    }

    cleanup_process_config();

    return 0;

}

/**
 *  ------------------- Built-in commands to use Filesystem--------------------
 */

/**
 *  Helpers
 */

// assumes path_prefix is valid path
static errval_t concatenate_paths(char *path_prefix, char *path_suffix, char **ret_path) {
    assert(path_prefix);
    assert(path_suffix);
    errval_t err = SYS_ERR_OK;

    // if path suffix does not start with "/", we add it
    char *path_suffix_cpy = path_suffix;
    if(strlen(path_suffix) > 0 && path_suffix[0] != '/') {
        path_suffix_cpy = malloc(strlen(path_suffix) + 2);
        if (!path_suffix_cpy) {
            err = LIB_ERR_MALLOC_FAIL;
            DEBUG_ERR(err, "malloc failed.\n");
            return err;
        }
        strcpy(path_suffix_cpy, "/");
        strcat(path_suffix_cpy, path_suffix);
    }

    char *full_path = NULL;
    // DIR = "/"
    if(strlen(path_prefix) == 1) {
        if (strlen(path_suffix_cpy) == 0) {
            full_path = malloc(strlen(path_prefix) + 1);
            if (!full_path) {
                goto malloc_fail;
            }
            strcpy(full_path, path_prefix);
        }
        else {
            full_path = malloc(strlen(path_suffix_cpy) + 1);
            if (!full_path) {
                goto malloc_fail;
            }
            strcpy(full_path, path_suffix_cpy);
        }
    }
    else if (strlen(path_suffix_cpy) == 0){
        full_path = malloc(strlen(path_prefix) + 1);
        if (!full_path) {
            goto malloc_fail;
        }
        strcpy(full_path, path_prefix);
    }
    else {
        full_path = malloc(strlen(path_prefix) + strlen(path_suffix_cpy) + 1);
        if (!full_path) {
            goto malloc_fail;
        }
        strcpy(full_path, path_prefix);
        strcat(full_path, path_suffix_cpy);
    }

    // free local copy
    if (path_suffix != path_suffix_cpy) free(path_suffix_cpy);

    *ret_path = full_path;

    return SYS_ERR_OK;

malloc_fail:
    err = LIB_ERR_MALLOC_FAIL;
    DEBUG_ERR(err, "malloc failed.\n");
    if (path_suffix != path_suffix_cpy) free(path_suffix_cpy);
    return err;
}

static errval_t check_dir_path(char *dir_name, char *path) {
    errval_t err = SYS_ERR_OK;
    // open and close the directory to see if it exists
    fs_dirhandle_t dir_handle;
    err = opendir(path, &dir_handle);
    if (err_no(err) == FS_ERR_NOTDIR) {
        printf("Error: '%s' is not a directory.\n", dir_name);
        return err;
    }
    if (err_no(err) == FS_ERR_NOTFOUND) {
        printf("Error: did not find directory '%s'.\n", dir_name);
        return err;
    }
    else if (err_is_fail(err)) {
        printf("Error: open directory '%s' failed.\n", dir_name);
        return err;
    }

    err = closedir(dir_handle);
    if (err_is_fail(err)) {
        printf("Error: Closing directory '%s' failed.\n", dir_name);
        return err;
    }

    return SYS_ERR_OK;
}

static size_t get_dir_size(char *full_path) {
    fs_dirhandle_t dir_handle;
    errval_t err = opendir(full_path, &dir_handle);
    if (err_is_fail(err)) {
        return 0;
    }

    size_t dir_size = 0;
    while (true) {
        char *name;
        err = readdir(dir_handle, &name);
        if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
            break;
        }
        else if(err_is_fail(err)) {
            return 0;
        }
        free(name);
        dir_size++;
    }

    err = closedir(dir_handle);
    if (err_is_fail(err)) {
        return 0;
    }

    return dir_size;
}

static errval_t print_dir_contents(char *path, FILE *std_out) {
    errval_t err = SYS_ERR_OK;

    char **dir_contents;
    size_t dir_size;
    err = list_dir_contents(path, &dir_contents, &dir_size, true);
    if (err_is_fail(err)) {
        return err;
    }

    // TODO use different colors for files and directories
    for (size_t i = 0; i < dir_size; i++) {
        fprintf(std_out, "%s\n", dir_contents[i]);
    }

    return SYS_ERR_OK;
}

/**
 *  Actual commands
 */

/*
 * ls [FOLDER(s)]
 *
 * OPTIONS:
 *
 * -h      display help
*/
int ls(int argc, char **argv, FILE *std_in, FILE *std_out) {
    errval_t err = SYS_ERR_OK;

    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("ls", std_out);
        return 0;
    }

    if (argc <= optind) {
        // display contents of current directory
        char *pwd = getenv("PWD");
        if(!pwd) {
            err = LIB_ERR_SHOULD_NOT_GET_HERE;
            DEBUG_ERR(err, "getenv() returned NULL.\n");
            return 1;
        }
        err = print_dir_contents(pwd, std_out);
        if (err_is_fail(err)) {
            return 1;
        }
    }
    else {
        // display contents of arg directories
        for (size_t i = optind; i < argc; i++) {
            char *path = argv[i];
            err = print_dir_contents(path, std_out);
            if (err_is_fail(err)) {
                return 1;
            }
        }
    }

    return 0;
}

/*
 * mkdir [FOLDER(s)]
 *
 * OPTIONS:
 *
 * -h      display help
*/
int aos_mkdir(int argc, char **argv, FILE *std_in, FILE *std_out) {
    errval_t err = SYS_ERR_OK;

    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("mkdir", std_out);
        return 0;
    }

    for (size_t i = optind; i < argc; i++) {
        char *new_path;
        err = to_full_path(argv[i], &new_path);
        if(err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            return 1;
        }
        err = mkdir(new_path);
        if(err_no(err) == FS_ERR_EXISTS) {
            printf("Error: directory '%s' already exists.\n", argv[i]);
            free(new_path);
            return 1;
        }
        else if(err_no(err) == FS_ERR_NOTFOUND) {
            printf("Error: Invalid path.\n");
            free(new_path);
            return 1;
        }
        else if (err_is_fail(err)) {
            printf("Error: mkdir failed.\n");
            free(new_path);
            return 1;
        }

        free(new_path);
    }

    return 0;
}

/*
 * cd [DIRECTORY]
 *
 * OPTIONS:
 *
 * -h      display help
*/

int cd(int argc, char **argv, FILE *std_in, FILE *std_out) {
    errval_t err = SYS_ERR_OK;

    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("cd", std_out);
        return 0;
    }

    char *path;
    err = to_full_path(argv[optind], &path);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "to_full_path failed.\n");
        return 1;
    }

    err = check_dir_path(argv[optind], path);
    if (err_is_fail(err)) {
        free(path);
        return 1;
    }

    // remove trailing slash as not desired in env variable
    size_t len = strlen(path);
    if (len > 1 && path[len - 1] == '/') {
        path [len - 1] = '\0';
    }

    setenv("PWD", path, 1);

    free(path);
    return 0;
}

/*
 * touch [FILE(s)]
 *
 * OPTIONS:
 *
 * -h display help
*/

int touch(int argc, char **argv, FILE *std_in, FILE *std_out) {
    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("touch", std_out);
        return 0;
    }


    for (size_t i = optind; i < argc; i++) {
        char *path;
        errval_t err = to_full_path(argv[i], &path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            return 1;
        }

        FILE *file = fopen(path, "w");
        if (!file) {
            printf("Could not create file %s\n", argv[i]);
            free(path);
            return 1;
        }

        fclose(file);
        free(path);
    }

    return 0;
}

/*
 * rm [FILE(s)]
 *
 * OPTIONS:
 *
 * -h display help
 * -r recursive removal
*/

static errval_t recursive_rm(const char *path) {
    errval_t err = rmdir(path);
    if (err_is_fail(err)) {
        switch(err_no(err)) {
            case FS_ERR_NOTEMPTY:
            {
                fs_dirhandle_t dir_handle;
                while (true) {
                    // open directory
                    err = opendir(path, &dir_handle);
                    if (err_is_fail(err)) {
                        printf("Error: Could not open directory %s.\n", path);
                        return err;
                    }

                    // read entry
                    char *name;
                    err = readdir(dir_handle, &name);

                    // close directory for further removals
                    errval_t err_close = closedir(dir_handle);
                    if (err_is_fail(err_close)) {
                        return err_close;
                    }
                    dir_handle = NULL;

                    // remove entry recursively
                    if (err_is_ok(err)) {
                        char *rec_path = malloc(strlen(path) + strlen(name) + 2);
                        strcpy(rec_path, path);
                        strcat(rec_path, "/");
                        strcat(rec_path, name);

                        err = recursive_rm(rec_path);
                        if (err_is_fail(err)) {
                            free(rec_path);
                            free(name);
                            return err;
                        }
                        free(rec_path);
                    }
                    // remove file
                    else if (err_no(err) == FS_ERR_NOTDIR) {
                        err = rm(name);
                        if (err_is_fail(err)) {
                            printf("Error: Could not remove file %s.\n", name);
                            free(name);
                            return err;
                        }
                    }
                    // no more entries
                    else if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
                        free(name);
                        break;
                    }
                    // unknown error
                    else {
                        if (name) free(name);
                        return err;
                    }
                    free(name);
                }
                // remove empty directory
                err = rmdir(path);
                if (err_is_fail(err)) {
                    return err;
                }
                break;
            }
            case FS_ERR_NOTDIR:
                // remove file
                err = rm(path);
                if (err_is_fail(err)) {
                    return err;
                }
                break;
            case FS_ERR_NOTFOUND:
                printf("Error: Could not find directory %s.\n", path);
                //fall through
            default:
                return err;
                break;
        }
    }

    return SYS_ERR_OK;
}

int aos_rm(int argc, char **argv, FILE *std_in, FILE *std_out) {
    errval_t err = SYS_ERR_OK;

    bool help_flag = false;
    bool recursive_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "hr")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            case 'r':
                recursive_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("rm", std_out);
        return 0;
    }

    for (size_t i = optind; i < argc; i++) {

        char *path;
        err = to_full_path(argv[i], &path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            return 1;
        }

        if (recursive_flag) {
            err = recursive_rm(path);
            if (err_is_fail(err)) {
                printf("Error: Recursive removal failed.\n");
                free(path);
                return 1;
            }
        }
        else {
	        err = rm(path);
	        if (err_is_fail(err)) {
	            printf("Could not delete file %s\n", argv[i]);
	            free(path);
	            return 0;
	        }
	    }
	    free(path);
    }

    return 0;
}

/*
 * rmdir [DIRECTORY]..
 *
 * OPTIONS:
 *
 * -h display help
*/

int aos_rmdir(int argc, char **argv, FILE *std_in, FILE *std_out) {
    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("rmdir", std_out);
        return 0;
    }

    for (size_t i = optind; i < argc; i++) {
        char *path;
        errval_t err = to_full_path(argv[i], &path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            return 1;
        }
        err = rmdir(path);
        if (err_is_fail(err)) {
            switch(err_no(err)) {
                case FS_ERR_NOTEMPTY:
                    printf("Error: Directory '%s' is not empty.\n", argv[i]);
                    break;
                case FS_ERR_NOTDIR:
                    printf("Error: '%s' is not a directory.\n", argv[i]);
                    break;
                case FS_ERR_NOTFOUND:
                    printf("Error: Could not find directory '%s'.\n", argv[i]);
                    break;
                default:
                    printf("Error: Removing directory '%s' failed.\n", argv[i]);
                    break;
            }
            free(path);
            return 1;
        }

        free(path);
    }

    return 0;
}

/*
 * cat [FILE(s)]
 *
 * OPTIONS:
 *
 * -h display help
*/

int aos_cat(int argc, char **argv, FILE *std_in, FILE *std_out) {
    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("cat", std_out);
        return 0;
    }


    for (size_t i = optind; i < argc; i++) {

        char *path;
        errval_t err = to_full_path(argv[i], &path);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "to_full_path failed.\n");
            return 1;
        }

        FILE *file = fopen(path, "r");
        if (!file) {
            printf("Could not open file %s\n", argv[i]);
            free(path);
            return 0;
        }
        if (fseek(file, 0, SEEK_END)) {
            fclose(file);
            printf("Could not determine file size of file %s\n", argv[i]);
            free(path);
            return 0;
        }
        long size = ftell(file);
        if (size <= 0) {
            fclose(file);
            free(path);
            continue;
        }
        if (fseek(file, 0, SEEK_SET)) {
            fclose(file);
            printf("Could not start reading file %s\n", argv[i]);
            free(path);
            return 0;
        }
        char *buf = malloc(size);
        if (!buf) {
            fclose(file);
            printf("Could not allocate buffer to read file %s\n", argv[i]);
            free(path);
            return 0;
        }
        size_t read_size = fread(buf, 1, size, file);
        fclose(file);
        if (read_size < size) {
            free(buf);
            printf("Could not read all of file %s\n", argv[i]);
            free(path);
            return 0;
        }
        assert(read_size == size);
        int written_size = fwrite(buf, 1, read_size, std_out);
        if (written_size < read_size) {
            free(buf);
            printf("Could not print all contents of file %s\n", argv[i]);
            free(path);
            return 0;
        }
        assert(written_size = size);
        free(buf);
        free(path);
    }

    return 0;
}

/*
 * mount [MOUNTPOINT] [URI]
 *
 * OPTIONS:
 *
 * -h display help
*/

int aos_mount(int argc, char **argv, FILE *std_in, FILE *std_out) {
    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {
        print_usage("mount", std_out);
        return 0;
    }

    char *mount_path = argv[optind];
    char *mount_path_full;
    errval_t err = to_full_path(mount_path, &mount_path_full);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "to_full_path_failed.\n");
        return 1;
    }
    char *uri = argv[optind + 1];
    err = aos_rpc_filesystem_mount(aos_rpc_get_filesystem_channel(), mount_path_full, uri);
    free(mount_path_full);
    if (err_is_fail(err)) {
        printf("Mounting failed.\n");
    }

    return 0;
}


static int read_file_content(const char *path, char **ret_buf, size_t *ret_buflen) {
    FILE *file = fopen(path, "r");
    if (!file) {
        printf("Could not open file %s\n", path);
        return 1;
    }
    if (fseek(file, 0, SEEK_END)) {
        fclose(file);
        printf("Could not determine file size of file %s\n", path);
        return 1;
    }
    long size = ftell(file);
    if (size <= 0) {
        fclose(file);
        *ret_buflen = size;
        *ret_buf = NULL;
        return 0;
    }
    if (fseek(file, 0, SEEK_SET)) {
        fclose(file);
        printf("Could not start reading file %s\n", path);
        return 1;
    }
    char *buf = malloc(size);
    if (!buf) {
        fclose(file);
        printf("Could not allocate buffer to read file %s\n", path);
        return 1;
    }
    size_t read_size = fread(buf, 1, size, file);
    fclose(file);
    if (read_size < size) {
        free(buf);
        printf("Could not read all of file %s\n", path);
        return 1;
    }
    assert(read_size == size);

    *ret_buf = buf;
    *ret_buflen = size;

    return 0;
}

static int file_cp(const char *source, const char *dest) {
    // read source
    char *buf = NULL;
    size_t buflen = 0;
    int err = read_file_content(source, &buf, &buflen);
    if (err != 0) {
        return 1;
    }

    // write to destination
    char *file_name = strrchr(source, '/');
    char *dest_file = malloc(strlen(dest) + strlen(file_name) + 1);
    if (!dest_file) {
        printf("Error: Malloc failed.\n");
        return 1;
    }
    strcpy(dest_file, dest);
    strcat(dest_file, file_name);

    FILE *file = fopen(dest_file, "w");
    if (!file) {
        printf("Could not open file %s\n", dest_file);
        return 1;
    }
    if (buflen > 0) {
        // TODO reset file content first?

        // do a poor man's truncate, since ftruncate is not available
        if (fseek(file, buflen - 1, SEEK_SET)) {
            printf("Could not reserve file space\n");
            return 1;
        }
        char c = '\0';
        if (fwrite(&c, 1, 1, file) != 1) {
            printf("Could not reserve file space\n");
            return 1;
        }
        if (fflush(file)) {
            printf("Could not reserve file space\n");
            return 1;
        }
        if (fseek(file, 0, SEEK_SET)) {
            printf("Could not reserve file space\n");
            return 1;
        }
        int written_size = fwrite(buf, 1, buflen, file);
        if (written_size < buflen) {
            free(buf);
            printf("Could not print all contents of file %s\n", dest);
            return 1;
        }
        assert(written_size == buflen);
        free(buf);
    }

    fclose(file);

    return 0;
}

static int recursive_cp(const char *source, const char *dest) {
    // directory
    fs_dirhandle_t dir_handle;
    errval_t err = opendir(source, &dir_handle);
    if (err_no(err) == FS_ERR_NOTDIR) {
        // copy file
        return file_cp(source, dest);
    }
    else if (err_no(err) == FS_ERR_NOTFOUND) {
        printf("Error: did not find directory '%s'.\n", source);
        return 1;
    }
    else if (err_is_fail(err)) {
        printf("Error: Failed to open directory '%s'.\n", source);
        return 1;
    }

    // succesfully opened directory
    // -> make directory at destination
    char *dirname = strrchr(source, '/');
    char *dest_dir = malloc(strlen(dest) + strlen(dirname) + 1);
    if (!dest_dir) {
        printf("Error: Malloc failed.\n");
        closedir(dir_handle);
        return 1;
    }

    // special case for root folder
    size_t len_dest = strlen(dest) > 1 ? strlen(dest) : 0;
    if (len_dest > 1) {
        strcpy(dest_dir, dest);
    }
    strcpy(dest_dir + len_dest, dirname);

    err = mkdir(dest_dir);
    if(err_no(err) == FS_ERR_EXISTS) {
        printf("Error: directory '%s' already exists.\n", dest_dir);
        free(dest_dir);
        closedir(dir_handle);
        return 1;
    }
    else if(err_no(err) == FS_ERR_NOTFOUND) {
        printf("Error: Invalid path.\n");
        free(dest_dir);
        closedir(dir_handle);
        return 1;
    }
    else if (err_is_fail(err)) {
        printf("Error: mkdir failed.\n");
        free(dest_dir);
        closedir(dir_handle);
        return 1;
    }
    // -> recursively call cp with updated paths for directory entries
    while (true) {
        char *name;
        err = readdir(dir_handle, &name);
        if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
            break;
        }
        else if(err_is_fail(err)) {
            return err;
        }

        char *new_source = malloc(strlen(source) + 1 + strlen(name) + 1);
        if (!new_source) {
            printf("Error: Malloc failed.\n");
            free(dest_dir);
            free(name);
            closedir(dir_handle);
            return 1;
        }

        strcpy(new_source, source);
        strcat(new_source, "/");
        strcat(new_source, name);

        recursive_cp(new_source, dest_dir);

        free(new_source);

        free(name);
    }

    closedir(dir_handle);

    return 0;
}


/*
 * cp SOURCE DEST
 *
 * OPTIONS:
 *
 * -h display help
*/

int cp(int argc, char **argv, FILE *std_in, FILE *std_out) {
    bool help_flag = false;
    bool unrecognized_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "h")) != -1) {
        switch(option) {
            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", optopt);
                unrecognized_flag = true;
        }
    }

    if (unrecognized_flag) {
        return 0;
    }

    if (help_flag) {

        print_usage("cp", std_out);
        return 0;
    }

    char *source;
    errval_t errf = to_full_path(argv[optind], &source);
    if (err_is_fail(errf)) {
        DEBUG_ERR(errf, "to_full_path failed.\n");
        return 1;
    }
    char *dest;
    errf = to_full_path(argv[optind + 1], &dest);
    if (err_is_fail(errf)) {
        DEBUG_ERR(errf, "to_full_path failed.\n");
        free(source);
        return 1;
    }

    int err = recursive_cp(source, dest);
    free(source);
    free(dest);

    return err;
}
