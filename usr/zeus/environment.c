/**
 *  \file
 *  \brief Environment variables
 */
#include <aos/aos.h>
#include "environment.h"

// environment variables
static struct env_variable env_variables[] =
{
    {
        .name   =   "HOME",
        .val    =   "/"
    },
    {
        .name   =   "PATH",
        .val    =   "/:/tests:/bin"
    },
    {
        .name   =   "PWD",
        .val    =   "/"
    }

    // TODO add more...
};

static size_t num_env_variables = sizeof(env_variables) / sizeof(env_variables[0]);

STATIC_ASSERT(sizeof(env_variables) / sizeof(env_variables[0]) <= MAX_ENVIRON_VARS, "Too many environment variables. At most %d allowed.\n");

char** serialize_env_variables(void) {
    char **env_string = malloc((num_env_variables + 1) * sizeof(char*));
    assert(env_string);

    for (size_t i = 0; i < num_env_variables; i++) {
        size_t name_len = strlen(env_variables[i].name);
        size_t val_len = strlen(env_variables[i].val);
        size_t tot_len = name_len + val_len + 2;

        env_string[i] = malloc(tot_len);
        assert(env_string[i]);

        strcpy(env_string[i], env_variables[i].name);
        strcpy(env_string[i] + name_len, "=");
        strcpy(env_string[i] + name_len + 1, env_variables[i].val);
    }

    env_string[num_env_variables] = NULL;

    return env_string;
}
