/**
 * \file
 * \brief Shell "Zeus"
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <hashtable/hashtable.h>

#include <linenoise/linenoise.h>

#include <fs/fs.h>

#include <ctype.h>

#include "zeus.h"
#include "zeus_functions.h"
#include "environment.h"
#include <shell_shared.h>


// needed for server setup of shell
#include <aos/connect/all.h>
#include <aos/aos_rpc_function_ids.h>
#include <aos/rpc_shared/aos_rpc_shared_monitor.h>
#include <nameserver_types.h>

// include service server functions
// adjust Hakefile to link with associated library
#include <aos_rpc_server/aos_rpc_server_shell.h>

/**
 * built-in shell commands
 */

static struct aos_shell_command shell_commands[] = {
    {
        .name = "help",
        .fun = help
    },
    {
        .name = "echo",
        .fun = echo
    },
    {
        .name = "run",
        .fun = run
    },
    // Filesystem
    {
        .name = "mkdir",
        .fun = aos_mkdir
    },
    {
        .name = "ls",
        .fun = ls
    },
    {
        .name = "cd",
        .fun = cd
    },
    {
        .name = "touch",
        .fun = touch
    },
    {
        .name = "rm",
        .fun = aos_rm
    },
    {
        .name = "rmdir",
        .fun = aos_rmdir
    },
    {
        .name = "cat",
        .fun = aos_cat
    },
    {
        .name = "mount",
        .fun = aos_mount
    },
    {
        .name = "cp",
        .fun = cp
    },
    // nameservice
    {
        .name = "apollo",
        .fun = nameserver_ui_client
    }

    // TODO add more shell commands
};

static const int num_shell_commands = sizeof(shell_commands) / sizeof(shell_commands[0]);

/*
 *  Hashtable
 *
 *  maps command names to structs containing meta data and function pointers.
*/

static struct hashtable *command_table;

static int put_command(struct hashtable *ht, char *name, struct aos_shell_command *command) {
    return ht->d.put_word(&ht->d, name, strnlen(name, SHELL_MAX_ARG_LENGTH), (uintptr_t) command);
}

static ENTRY_TYPE get_command(struct hashtable *ht, char *name, struct aos_shell_command **command) {
    return ht->d.get(&ht->d, name, strnlen(name, SHELL_MAX_ARG_LENGTH), (void **) command);
}

static int fill_table(void) {
    for (size_t i = 0; i < num_shell_commands; i++) {
        int err = put_command(command_table,
                            shell_commands[i].name,
                            &shell_commands[i]);
        if (err != 0) {
            debug_printf("%s: put_command failed.\n");
            return err;
        }
    }

    return 0;
}

/*
 *  I/O functions
*/

/*
 *  Parsing functions
**/

// assumes argv is an array of valid pointers to buffer of size SHELL_MAX_ARG_LEN
static bool parse_command(char *cmd_string, int *argc, char **argv) {
    assert(argc);
    assert(argv);

    if (!cmd_string) {
        *argc = 0;
        return true;
    }

    // get args, command name parsed as first arg
    size_t argument_count = 0;
    while (*cmd_string && argument_count < MAX_CMDLINE_ARGS) {
        // strip leading whitespaces
        while (isspace(*cmd_string)) {
            cmd_string++;
        }
        // parse arg
        const char *cur_arg = cmd_string;
        while (*cmd_string && !isspace(*cmd_string)) {
            cmd_string++;
        }
        size_t arg_len = cmd_string - cur_arg;
        if (!arg_len) {
            continue;
        }
        if (arg_len > SHELL_MAX_ARG_LENGTH) {
            printf("Error: Argument %d too long.\n", argument_count + 1);
            return false;
        }
        strncpy(argv[argument_count], cur_arg, arg_len);
        argument_count++;
    }

    if (*cmd_string) {
        while (isspace(*cmd_string)) {
            cmd_string++;
        }
        if (*cmd_string) {
            printf("Too many arguments. Discarded some.\n");
        }
    }

    *argc = argument_count;

    return true;
}

enum parsing_state {
    READ_COMMAND,
    HAS_COMMAND,
    READ_REDIRECTION
};

// reads line l until sep and stores this in ret_token
// returns NULL on error
static char* read_token(const char *l, char sep, char** ret_token) {
    assert(l);

    char *cur = (char*) l;
    char *start = cur;
    while (*cur && *cur != sep) {
        cur++;
    }
    size_t len = cur - start;
    *ret_token = (char *) calloc(len + 1, 1);
    if (!*ret_token) {
        errval_t err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "calloc failed.\n");
        return NULL;
    }
    memcpy(*ret_token, start, len);

    return cur;
}

static char *read_token_multi_sep(const char *l, char *sep, size_t sep_len, char **ret_token) {
    assert(l);

    char *cur = (char*) l;
    char *start = cur;
    bool stop = false;
    while (*cur && !stop) {
        for (size_t i = 0; i < sep_len; i++) {
            if (*cur == sep[i]) {
                stop = true;
                cur--;
                break;
            }
        }
        cur++;
    }
    size_t len = cur - start;
    *ret_token = (char *) calloc(len + 1, 1);
    if (!*ret_token) {
        errval_t err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "calloc failed.\n");
        return NULL;
    }
    memcpy(*ret_token, start, len);

    return cur;
}

static char shell_sep[3] = {' ', '<', '>'};
static char cmd_sep[2] = {'<', '>'};

static bool tokenize_line(const char *l, char** command, char **file_in, char **file_out) {
    assert(l);

    char *line_index = (char*) l;

    *command = NULL;
    *file_in = NULL;
    *file_out = NULL;

    char **redir_file = NULL;

    enum parsing_state state = READ_COMMAND;
    bool parsing = true;

    while (parsing) {
        switch (state) {
            case READ_COMMAND:
                // remove whitespaces
                while (*line_index && isspace(*line_index)) {
                    line_index++;
                }
                line_index = read_token_multi_sep(line_index, cmd_sep, 2, command);
                if (!line_index) {
                    debug_printf("read_token_multi_sep failed.\n");
                    goto error;
                }
                state = HAS_COMMAND;
                break;
            case HAS_COMMAND:
                // remove whitespaces
                while (*line_index && isspace(*line_index)) {
                    line_index++;
                }
                switch(*line_index) {
                    case '>':
                        if (*file_out != NULL){
                            printf("Error: Only one stdout allowed.\n");
                            goto error;
                        }
                        line_index++;
                        redir_file = file_out;
                        state = READ_REDIRECTION;
                        break;
                    case '<':
                        if (*file_in != NULL) {
                            printf("Error: Only one stdin allowed.\n");
                            goto error;
                        }
                        line_index++;
                        redir_file = file_in;
                        state = READ_REDIRECTION;
                        break;
                    case '\0':
                        parsing = false;
                        break;
                    default:
                        printf("Error: Invalid syntax.\n");
                        goto error;
                        break;
                }
                break;
            case READ_REDIRECTION:
                while (isspace(*line_index)) {
                    line_index++;
                }
                line_index = read_token_multi_sep(line_index, shell_sep, 3, redir_file);
                if (!line_index) {
                    debug_printf("read_token_multi_sep failed.\n");
                    goto error;
                }
                redir_file = NULL;
                state = HAS_COMMAND;
                break;
            default:
                break;
        }
    }

    return true;

error:
    if (*command) free(*command);
    *command = NULL;
    if (*file_in) free(*file_in);
    *file_in = NULL;
    if (*file_out) free(*file_out);
    *file_out = NULL;
    return false;
}

static bool parse_line(char *l, char **cmdstr, char **cmdname,
                        char **arg_stdin, char **arg_stdout) {
    assert(l);

    bool success = true;
    success = tokenize_line(l, cmdstr, arg_stdin, arg_stdout);
    if (cmdstr) {
        success = success && read_token(*cmdstr, ' ', cmdname);
    }

    return success;
}

/*
 *  Linenoise callbacks
*/

static char* find_command_by_prefix(const char *prefix) {
    for (size_t i = 0; i < num_shell_commands; i++) {
        if (!strncmp(shell_commands[i].name, prefix, strlen(prefix))){
            return shell_commands[i].name;
        }
    }

    return NULL;
}

static char* find_file_by_prefix(const char *prefix) {
    char *path_prefix = NULL;        // entered path up to (excluding) last "/"
    char *file_prefix = (char *) prefix; // part of prefix we actually want to compare
    char *end = strrchr(prefix, '/');
    if (end != NULL) {
        // separators in prefix
        size_t len = end - prefix;  // len without terminating "/"
        path_prefix = calloc(len + 1, 1);
        strncpy(path_prefix, prefix, len);
        file_prefix = end + 1;
    }

    char **dir_contents;
    size_t dir_size;
    errval_t err = list_dir_contents(path_prefix ? path_prefix : "", &dir_contents, &dir_size, false);
    if (err_is_fail(err)) {
        // error probably due to inexisting path
        // do not handle error
        if (path_prefix) free(path_prefix);
        return NULL;
    }

    char *ret_name = NULL;
    bool found_match = false;
    for (size_t i = 0; i < dir_size; i++) {
        // we only consider first match
        if (!strncmp(dir_contents[i], file_prefix, strlen(file_prefix)) && !found_match) {
            found_match = true;
            if (path_prefix == NULL) {
                // path prefix is empty -> just complete the file/dir name
                ret_name = malloc(strlen(dir_contents[i]) + 1);
                strcpy(ret_name, dir_contents[i]);
            }
            else {
                // path prefix does not end on "/" -> add a "/"
                ret_name = malloc(strlen(path_prefix) + 1 + strlen(dir_contents[i]) + 1);
                strcpy(ret_name, path_prefix);
                strcat(ret_name, "/");
                strcat(ret_name, dir_contents[i]);
            }
        }
        free(dir_contents[i]);
    }

    if (path_prefix) free(path_prefix);

    free(dir_contents);
    return ret_name;

}

static void completion(const char *buf, linenoiseCompletions *lc) {

    // complete command
    char *cmd = find_command_by_prefix(buf);
    if (cmd != NULL) {
        linenoiseAddCompletion(lc, cmd);
        return;
    }

    // complete last token
    char *token = strrchr((char *)buf, ' ');
    // might also be only one token that is not a command
    token = token ? token + 1 : (char*)buf;
    char *file = find_file_by_prefix(token);
    if (file != NULL) {
        // found matching prefix, now have to reconstruct full line
        size_t buf_len = (token - buf);
        char *l = calloc(buf_len + strlen(file) + 1, 1);
        strncpy(l, buf, buf_len);
        strcat(l, file);
        linenoiseAddCompletion(lc, l);
        free(file);
        free(l);
        return;
    }

    return;
}

/*
 *  Shell
*/

char *line;
char *prompt;
char *parsed_cmdstring;
char *parsed_cmdname;
char *parsed_stdin;
char *parsed_stdout;
FILE *stream_in;
FILE *stream_out;

static inline void cleanup(void) {
    if (line)               free(line);
    if (prompt)             free(prompt);
    if (parsed_cmdstring)   free(parsed_cmdstring);
    if (parsed_cmdname)     free(parsed_cmdname);
    if (parsed_stdin)       free(parsed_stdin);
    if (parsed_stdout)      free(parsed_stdout);
    return;
}

static inline void reset(void) {
    line = NULL;
    prompt = NULL;
    parsed_cmdstring = NULL;
    parsed_cmdname = NULL;
    parsed_stdin = NULL;
    parsed_stdout = NULL;
    stream_in = stdin;
    stream_out = stdout;
    return;
}

#define BLUE "\033[34m"
#define YELLOW "\033[33m"
#define RESET "\033[0m"

#define SHELL_NAME "zeus"

static int shell(void) {
    printf("\n\n------------------ Zeus: Group C shell ------------------\n");
    printf("Type 'help' to list all the available commands.\n\n");

    char *argv[MAX_CMDLINE_ARGS];
    for (size_t i = 0; i < MAX_CMDLINE_ARGS; i++) {
        argv[i] = malloc(SHELL_MAX_ARG_LENGTH);
        if (!argv[i]) {
            debug_printf("Malloc failed.\n");
            goto exit;
        }
    }
    int argc;

    /* Set the completion callback. This will be called every time the
     * user uses the <tab> key. */
    linenoiseSetCompletionCallback(completion);

    bool run = true;
    while (run) {

        // reset buffers
        for (size_t i = 0; i < MAX_CMDLINE_ARGS; i++) {
            memset(argv[i], 0, SHELL_MAX_ARG_LENGTH);
        }
        argc = 0;

        // reset globals
        reset();

        // parameters used by getopt(), have to be reset manually
        optind = 1;
        optarg = NULL;
        opterr = 0;
        optopt = 0;


        // read next input line
        char *pwd = getenv("PWD");
        if (!pwd){
            debug_printf("PWD is NULL.\n");
            goto exit;
        }

        prompt = malloc(3 * strlen(YELLOW) + strlen(SHELL_NAME) + strlen(pwd) + 4);
        if (!prompt) {
            debug_printf("malloc failed.\n");
            continue;
        }

        sprintf(prompt, YELLOW "" SHELL_NAME ":" BLUE "%s" RESET "$ ", pwd);
        line = linenoise(prompt);
        if (line == NULL) {
            debug_printf("linenoise failed.\n");
            free(prompt);
            continue;
        }
        else if (line[0] != '\0' && line[0] != '/') {
            linenoiseHistoryAdd(line); /* Add to the history. */
            // linenoiseHistorySave("history.txt"); /* Save the history on disk. */
        }

        // parse input
        if (!parse_line(line, &parsed_cmdstring, &parsed_cmdname, &parsed_stdin, &parsed_stdout)) {
            cleanup();
            continue;
        }

        // set stdin/stdout in this struct to make it available in run & on server
        current_process_config.std_in_path = parsed_stdin;
        current_process_config.std_out_path = parsed_stdout;

        // get shell function corresponding to command from hashtable
        struct aos_shell_command *command = NULL;
        if (parsed_cmdname != NULL) {
            get_command(command_table, parsed_cmdname, &command);
        }

        if (command == NULL) {
            if (strlen(parsed_cmdname) > 0) {
                printf("Shell function with name '%s' is not available.\n", parsed_cmdname);
            }
        }
        else {
            // parse arguments
            if (!parse_command(parsed_cmdstring, &argc, argv)) {
                cleanup();
                continue;
            }
            // set stdin/stdout for command
            stream_in = stdin;
            if (parsed_stdin) {
                char *path;
                errval_t err = to_full_path(parsed_stdin, &path);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "to_full_path failed.\n");
                    cleanup();
                    continue;
                }
                FILE *f = fopen(path, "r");
                stream_in = f ? f : stdin;
                free(path);
            }
            stream_out = stdout;
            if (parsed_stdout) {
                char *path;
                errval_t err = to_full_path(parsed_stdout, &path);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "to_full_path failed.\n");
                    cleanup();
                    continue;
                }
                FILE *f = fopen(path, "w");
                stream_out = f ? f : stdout;
                free(path);
            }
            command->fun(argc, (char **)argv, stream_in, stream_out);
        }

        if (stream_in->_file >= 0 && stream_in->_file != STDIN_FILENO) {
            int err = fclose(stream_in);
            if (err) {
                debug_printf("fclose failed.\n");
            }
        }

        if (stream_out->_file >= 0 && stream_out->_file != STDOUT_FILENO) {
            int err = fclose(stream_out);
            if (err) {
                debug_printf("fclose failed.\n");
            }
        }

        cleanup();

        printf("\n");

    }

exit:
    for (size_t i = 0; i < MAX_CMDLINE_ARGS; i++) {
        if (argv[i]) free(argv[i]);
    }

    return 0;
}

//--- main -------------------------------------------------------------------/

struct aos_rpc *hermes = NULL;
struct aos_rpc *demeter = NULL;

extern char **environ;

int main(int argc, char *argv[])
{
    debug_printf("Greetings, stranger! You are entering the realms of Zeus. Behave!\n");

    errval_t err = SYS_ERR_OK;

    demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("FATAL ERROR: Could not connect with Demeter process service.\n");
        return 1;
    }

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/app/shell"
    #define SHORT_NAME "zeus"
    #define DESCRIPTION "A Shell"
    #define PATRON "Zeus: He is the boss"
    #define CAPACITY 3

    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return 1;
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct aos_rpc_request_queue shell_req_queue;
    aos_rpc_request_queue_init(&shell_req_queue);
    aos_rpc_set_my_request_queue(&shell_req_queue);

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_SHELL, false,
                                   store, aos_rpc_shell_server_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return 1;
    }

    free(store);

    // shell id string
    char *shell_id_str = strrchr(reply->name, '/') + 1;
    uint8_t n;
    bool success = to_number(shell_id_str, &n);
    if (!success) {
        debug_printf("Could not decode shell id, setting it to 0.\n");
        n = 0;
    }
    current_process_config.shell_id = n;

    // struct serializable_key_value_store *info;
    // struct aos_rpc *name_rpc = aos_rpc_get_name_channel();

    // err = aos_rpc_name_get_service_info(name_rpc, reply->handle, &info);
    // if (err_is_fail(err)) {
    //     DEBUG_ERR(err, "get_service_info() failed");
    // }

    // debug_printf("info: %s\n", kv_store_get(info, "description"));

    // capacity mainly denotes number of buckets and should be prime
    command_table = create_hashtable2(23, 75);

    int err2 = fill_table();
    if (err2 != 0) {
        debug_printf("Could not fill hashtable.\n");
        return 1;
    }

    environ = serialize_env_variables();

    hermes = aos_rpc_get_serial_channel();

    initialize_shell_commands();

    return shell();
}
