/**
 *  \file
 *  \brief Program Manager
 *
 *  Basically all functionality of original domain manager that is still useful
 *  for shell Zeus.
 *
 */


#ifndef USR_ZEUS_PROGRAM_MANAGER_H_
#define USR_ZEUS_PROGRAM_MANAGER_H_

//--- available user programs for the menu ---------------------------------------------------------
// this needs to be configured still manually at the moment
// I suggest, only having actual user programs here. The services usr programs should be started
// by init (demeter directly, and the others via demeter). They should not be launched by the user
// at the moment. On the other hand, we may need a watchdog that can check whether one of the
// services ended and automatically relaunch it. However, not first priority at the moment.

static const char control_chars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

typedef struct programdef {
    char *name; // no const here to avoid problems with given aos_rpc_process_spawn() interface
    char launch;
} programdef_t;

extern programdef_t programs[];

extern const int programs_size;

typedef struct domaininfo {
    domainid_t domain_id;
    char *name;
    // additional metadata in the future
} domaininfo_t;

void init_program_manager(void);
void clean_domaininfo_list(void);
errval_t prepare_domaininfo_list(void);
void kill_program(domaininfo_t *current);

#endif // USR_ZEUS_PROGRAM_MANAGER_H_
