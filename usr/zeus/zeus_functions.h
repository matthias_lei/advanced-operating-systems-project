/**
 *  \file
 *  \brief Zeus functions
 *
 *  Built-in commands of our shell "Zeus"
 */

#ifndef USR_ZEUS_SHELL_FUNCTIONS_H_
#define USR_ZEUS_SHELL_FUNCTIONS_H_

#include "program_manager.h"

extern struct aos_rpc *hermes;
extern struct aos_rpc *demeter;

void initialize_shell_commands(void);

/**
 *  Public helper functions
 */

errval_t to_full_path(char *shell_path, char **ret_path);
bool to_number(char *str, uint8_t *ret_n);
errval_t list_dir_contents(char *full_path, char ***ret_dir_contents,
                            size_t *size, bool enable_error_output);

/**
 *  General Built-in commands
 */

int help(int argc, char **argv, FILE *std_in, FILE *std_out);
int echo(int argc, char **argv, FILE *std_in, FILE *std_out);
int run(int argc, char **argv, FILE *std_in, FILE *std_out);

/**
 *  Built-in commands to use Filesystem
 */

int aos_mkdir(int argc, char **argv, FILE *std_in, FILE *std_out);
int ls(int argc, char **argv, FILE *std_in, FILE *std_out);
int cd(int argc, char **argv, FILE *std_in, FILE *std_out);
int touch(int argc, char **argv, FILE *std_in, FILE *std_out);
int aos_rm(int argc, char **argv, FILE *std_in, FILE *std_out);
int aos_rmdir(int argc, char **argv, FILE *std_in, FILE *std_out);
int aos_cat(int argc, char **argv, FILE *std_in, FILE *std_out);
int aos_mount(int argc, char **argv, FILE *std_in, FILE *std_out);
int cp(int argc, char **argv, FILE *std_in, FILE *std_out);

/**
 *  Built-in commands to use Nameserver UI client
 *  -> see linked library
 */
#include <nameserver_ui_client.h>

#endif // USR_ZEUS_SHELL_FUNCTIONS_H_
