/**
 * \file
 * \brief artemis
 * Daughter of Zeus and Leto, and the twin sister of Apollo.
 * Among other things, the Hellenic goddess of the hunt, wild animals, wilderness.
 * Thus, perfect to supervise all the wild test cases that are hunting for bugs in the wilderness.
 *
 * Adventure is out there! (UP!)
 * Be a wilderness explorer.
 * 
 * technical note: the program is linked with testing_program where the actual tests are defined.
 *
 * version 2017-10-25, group C
 */


#include <stdio.h>

#include <testing/testing.h>

#define AOS_TESTING_DEBUGGING_MAIN
#include <testing/debugging.h>


int main(int argc, char *argv[])
{
    debug_printf("Artemis kindly greets you. Happy bug hunting!\n");

    testing_add_tests();
    testing_run_shell();
    testing_cleanup();

    return 0;
}
