/**
 * \file
 * \brief Filesystem Driver main routine.
 */
/*
 * Copyright (c) 2013, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <stdio.h>
#include <aos/aos.h>

#include <aos/connect/all.h>
#include <aos_rpc_server/aos_rpc_server_filesystem.h>
#include <nameserver_types.h>

#include <fs/fs.h>

#define NAME "Plutos Filesystem Service"

int main(int argc, char **argv)
{
    errval_t err = SYS_ERR_OK;

    // for local filesystem needs:
    struct aos_rpc filesystem_rpc;
    init_local_channel(&filesystem_rpc, AOS_RPC_INTERFACE_FILESYSTEM, &aos_rpc_filesystem_local_vtable);
    set_filesystem_rpc(&filesystem_rpc);

    // mount a RAMFS as root filesystem
    err = aos_rpc_filesystem_mount(aos_rpc_get_filesystem_channel(), "/", "ramfs://");
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "aos_rpc_filesystem_mount failed.\n");
    }

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/dev/filesystem"
    #define SHORT_NAME "filesystem"
    #define DESCRIPTION "Filesystem service"
    #define PATRON "Plutos: god of wealth and guardian of cereal supply"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        USER_PANIC_ERR(err, "create_kv_store() failed");
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct aos_rpc_request_queue filesystem_req_queue;
    aos_rpc_request_queue_init(&filesystem_req_queue);
    aos_rpc_set_my_request_queue(&filesystem_req_queue);

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_FILESYSTEM, true,
                                   store, aos_rpc_filesystem_server_event_handler, &reply);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "register_service() failed");
    }

    // enter the event dispatching loop
    struct waitset *default_ws = get_default_waitset();
    while (true) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }
}
