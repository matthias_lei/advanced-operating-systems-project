/**
 * \file
 * \brief demeter
 * Demeter is the goddess of the harvest and agriculture.
 *
 * technical note: the program is linked with aos_rpc_server_process and domman where the services are defined.
 *
 * bootstrap info:
 * - some functionality is used during bootstrap in monitor 0 to launch
 *   1) memory service Dionysos and 2) name service Gaia
 * - Demeter itself is launched as 3rd service, potentially after spawn of 2nd core
 * - all other services / domains are then launched via Demeter.
 *
 * version 2017-12-03, group C
 */


#include <stdio.h>
#include <aos/aos.h>
#include <aos/connect/all.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>
#include <aos/rpc_shared/aos_rpc_shared_monitor.h>
#include <nameserver_types.h>

// include service server functions
// adjust Hakefile to link with associated library
#include <aos_rpc_server/aos_rpc_server_process.h>
#include <domman/domman.h>

#define NAME "Demeter process service"

static errval_t get_bootstrap_info(void)
{
    struct aos_rpc_get_bootstrap_pids_res_payload res;
    errval_t err = aos_rpc_monitor_get_bootstrap_pids(aos_rpc_get_monitor_channel(), &res);
    //debug_printf("Receiving info from monitor/0:\n");
    for (size_t i = 0; i < res.count; i++) {
        struct spawninfo si;
        si.binary_name = res.names[i];
        domainid_t dummy;
        //debug_printf("- %s on core %zu\n", res.names[i], (size_t)res.cores[i]);
        err = add_domain(&si, res.cores[i], &dummy);
        if (err_is_fail(err)) {
            return err;
        }
    }

    return SYS_ERR_OK;
}

int main(int argc, char *argv[])
{
    // launch message
    debug_printf("Demeter wishes to new domains: live long and prosper!\n");

    errval_t err = SYS_ERR_OK;

    // service initialization
    err = domain_manager_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "domain_manager_init() failed");
        return 1;
    }

    // get bootstrap related infos from monitor/0
    err = get_bootstrap_info();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "get_bootstrap_info() failed");
        return 1;
    }

    // connect with spawn services in the monitors
    err = init_spawn_rpcs();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "init_rpcs() failed");
        return 1;
    }

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/core/process"
    #define SHORT_NAME "process"
    #define DESCRIPTION "Process service"
    #define PATRON "Demeter: is the goddess of the harvest and agriculture"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return 1;
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_PROCESS, false,
                                   store, aos_rpc_processserver_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return 1;
    }

    // for local process needs:
    struct aos_rpc process_rpc;
    init_local_channel(&process_rpc, AOS_RPC_INTERFACE_PROCESS, &aos_rpc_process_local_vtable);
    set_process_rpc(&process_rpc);

    // react to events
    struct waitset *default_ws = get_default_waitset();
    while (true) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }

    return 0;
}
