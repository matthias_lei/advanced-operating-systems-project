/**
 * \file
 * \brief hermes
 * Hermes is the emissary and messenger of the gods.
 *
 * technical note: the program is linked with aos_rpc_server_serial where the services are defined.
 *
 * NOTE: it includes a specialized event handler loop that also checks for new incoming characters.
 *
 * version 2017-12-06, group C
 */


#include <stdio.h>
#include <aos/aos.h>
#include <aos/connect/all.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>
#include <nameserver_types.h>

// include service server functions
// adjust Hakefile to link with associated library
#include <aos_rpc_server/aos_rpc_server_serial.h>

int main(int argc, char *argv[])
{
    // launch message
    debug_printf("Hermes sends you a window-like welcome message.\n");

    errval_t err = SYS_ERR_OK;

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/core/serial"
    #define SHORT_NAME "serial"
    #define DESCRIPTION "I/O service"
    #define PATRON "Hermes: emissary and messenger of the gods"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return 1;
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_SERIAL, true,
                                   store, aos_rpc_serialserver_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return 1;
    }

    // for local process needs:
    struct aos_rpc serial_rpc;
    init_local_channel(&serial_rpc, AOS_RPC_INTERFACE_SERIAL, &aos_rpc_serial_local_vtable);
    set_serial_rpc(&serial_rpc);

    // This test works
    //printf("This test may trigger some reflexive late binding.\n");

    // please note: in the current setting, this test triggers a lookup for /core/serial
    // at nameserver and then a bind of a LMP channel of Hermes service with itself.
    // it works.

    // as discussed: it may be beneficial to implement a "local" version of the remote functions
    // at least for output to again activate this local vtable again.

    // extended dispatch loop, that additionally regularly polls for user input
    struct waitset *ws = get_default_waitset();
    while (true) {
        // non-blocking check for events
        err = check_for_event(ws);
        if (err_is_ok(err)) {
            err = event_dispatch(ws);
            if (err_is_fail(err)) {
                // fatal error
                DEBUG_ERR(err, "event_dispatch failed.\n");
                abort();
            }
        }
        else if (err_no(err) != LIB_ERR_NO_EVENT) {
            DEBUG_ERR(err, "check_for_event failed.\n");
        }

        // non-blocking polling on terminal input
        err = handle_input_characters();
        if (err_is_fail(err)) {
            // just log the error, but we want to continue polling
            DEBUG_ERR(err, "handle_input_characters failed.\n");
        }
    }

    return 0;
}
