/**
 * \file
 * \brief gaia
 * Gaia is the ancestral mother of all life: the primal Mother Earth goddess.
 *
 * technical note: the program is linked with aos_rpc_server_name where the services are defined.
 *
 * This file has also been designed as a template for other service programs.
 * Similar to Gaia as the origin of the other gods.
 *
 * bootstrap info:
 * - Gaia is launched 2nd after Dionysos. It receives a regular memory channel.
 * - offers its service to monitor/0 manually, which is used to indirectly register
 *   the memory service.
 *
 * version 2017-12-03, group C
 */


#include <stdio.h>
#include <aos/aos.h>
#include <aos/connect/lmp.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>

// include service server functions
// adjust Hakefile to link with associated library
#include <aos_rpc_server/aos_rpc_server_name.h>
#include <nameserver.h>

#define NAME "Gaia name service"

// specialized for gaia
// self-registration in database
static errval_t self_register_service(void)
{
    // register
    #define ABS_PATH "/core/name"
    #define SHORT_NAME "name"
    #define DESCRIPTION "Name service"
    #define PATRON "Gaia: ancestral mother of all life: the primal Mother Earth goddess"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct service_registration_request request = {
        .core_id = 0,
        .interface = AOS_RPC_INTERFACE_NAME,
        .contact_chan_type = AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP,
        .high_bandwidth_service = false,
        .abs_path = ABS_PATH,
        .short_name_prefix = SHORT_NAME,
        .kv_store = store
    };

    struct service_registration_reply *reply;

    // NOTE: no cap is registered
    // also no auto ping needed, of course
    errval_t err = service_register(&request, NULL_CAP, &reply);
    if (err_is_fail(err)) {
        return err;
    }

    return SYS_ERR_OK;
}


int main(int argc, char *argv[])
{
    // launch message
    debug_printf("Gaia kindly greets you. May there be names!\n");

    errval_t err = SYS_ERR_OK;

    // service initialization
    bool ok = init_nameserver();
    if (!ok) {
        debug_printf("Could not initialize name server! Service stopped!\n");
        return 1;
    }

    // self-registration
    err = self_register_service();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "self_register_service() failed.\n");
        return 1;
    }

    // bootstrapping: offer service to monitor/0
    // note: the most simple LMP is fine. No large amounts; no FLMP is needed
    // identical mechanism as in memory/0
    struct capref service_for_monitor0;
    err = slot_alloc(&service_for_monitor0);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slot_alloc for service_for_monitor0 failed.\n");
        return 1;
    }

    err = setup_lmp_service(service_for_monitor0, MKCLOSURE(aos_rpc_nameserver_event_handler, NULL));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not create LMP service channel for monitor/0.\n");
        return 1;
    }

    err =  aos_rpc_monitor_bootstrap_offer_service(aos_rpc_get_monitor_channel(), AOS_RPC_INTERFACE_NAME, service_for_monitor0);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not offer memory service to monitor/0.\n");
        return 1;
    }

    // for local name needs: only for symmetry. No actual need
    struct aos_rpc name_rpc;
    init_local_channel(&name_rpc, AOS_RPC_INTERFACE_NAME, &aos_rpc_name_local_vtable);
    set_name_rpc(&name_rpc);

    // periodic check thread is launched by nameserver itself after memory service has registered
    // note: memory service is needed to have enough memory for an additional thread.

    // react to events
    struct waitset *default_ws = get_default_waitset();
    while (true) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }

    return 0;
}
