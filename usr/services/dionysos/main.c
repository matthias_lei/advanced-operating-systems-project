/**
 * \file
 * \brief dionysos
 * Dionysos is the ancestral god of grape harvest, winemaking and wine.
 *
 * technical note: the program is linked with aos_rpc_server_memory and mm where the services are defined.
 *
 * version 2017-12-03, group C
 */


#include <stdio.h>
#include <aos/aos.h>
#include <aos/connect/lmp.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>

// include service server functions
// adjust Hakefile to link with associated library
#include <aos_rpc_server/aos_rpc_server_memory.h>

#include <mm/mm.h>

#define NAME "Dionysos memory service"

/// MM allocator instance data
extern struct mm dionysos_mm;

static errval_t init_mm(void)
{
    // init slot allocator
    static struct slot_prealloc init_slot_alloc;

    struct capref cnode_cap = {
        .cnode = {
            .croot = CPTR_ROOTCN,
            .cnode = ROOTCN_SLOT_ADDR(ROOTCN_SLOT_SLOT_ALLOC0),
            .level = CNODE_TYPE_OTHER,
        },
        .slot = 0,
    };
    errval_t err = slot_prealloc_init(&init_slot_alloc, cnode_cap, L2_CNODE_SLOTS, &dionysos_mm);
    if (err_is_fail(err)) {
        return err_push(err, MM_ERR_SLOT_ALLOC_INIT);
    }

    // Initialize dionysos_mm
    err = mm_init(&dionysos_mm, ObjType_RAM, slab_default_refill,
                  slot_alloc_prealloc, slot_prealloc_refill,
                  &init_slot_alloc);
    if (err_is_fail(err)) {
        USER_PANIC_ERR(err, "Can't initalize the memory manager.");
    }

    // Give dionysos_mm a bit of memory for the initialization
    static char nodebuf[sizeof(struct mmnode)*64];
    slab_grow(&dionysos_mm.slabs, nodebuf, sizeof(nodebuf));

    return SYS_ERR_OK;
}

void memory_service_register_callback(void);

void memory_service_register_callback(void)
{
    errval_t err = SYS_ERR_OK;

    // registration: 1) of the event handler and 2) with the nameserver
    #define ABS_PATH "/core/memory"
    #define SHORT_NAME "memory"
    #define DESCRIPTION "Memory service"
    #define PATRON "Dionysos: ancestral god of grape harvest, winemaking and wine"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return;
    }

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);
    // TODO: add more meta data

    struct service_registration_reply *reply;
    err = aos_rpc_service_register(ABS_PATH, SHORT_NAME, AOS_RPC_INTERFACE_MEMORY, false,
                                   store, aos_rpc_memserver_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return;
    }
}

int main(int argc, char *argv[])
{
    // launch message
    debug_printf("Dionysos has arrived. Memory is available in the system. Cheers!\n");

    errval_t err = SYS_ERR_OK;

    // init MM
    err = init_mm();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "init_mm() failed.\n");
        return 1;
    }

    // bootstrapping: offer service to monitor/0
    // note: the most simple LMP is fine. No large amounts; no FLMP is needed
    struct capref service_for_monitor0;
    err = slot_alloc(&service_for_monitor0);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slot_alloc for service_for_monitor0 failed.\n");
        return 1;
    }

    err = setup_lmp_service(service_for_monitor0, MKCLOSURE(aos_rpc_memserver_event_handler, NULL));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not create LMP service channel for monitor/0.\n");
        return 1;
    }

    err =  aos_rpc_monitor_bootstrap_offer_service(aos_rpc_get_monitor_channel(), AOS_RPC_INTERFACE_MEMORY, service_for_monitor0);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not offer memory service to monitor/0.\n");
        return 1;
    }

    // monitor/0 will send 2 types of events
    // - AOS_RPC_MEMORY_FID_LOAD_RAM: once or several times to load RAM
    // - AOS_RPC_MEMORY_FID_REGISTER: time to register with name server

    // for local memory needs:
    struct aos_rpc memory_rpc;
    init_local_channel(&memory_rpc, AOS_RPC_INTERFACE_MEMORY, &aos_rpc_memory_local_vtable);
    set_memory_rpc(&memory_rpc);

    err = ram_alloc_set(NULL);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_RAM_ALLOC_SET);
    }

    // react to events
    struct waitset *default_ws = get_default_waitset();
    while (true) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
            abort();
        }
    }

    return 0;
}
