/**
 * \file
 * \brief charon
 *  Charon is the ferryman of the Styx
 *
 * technical note: the program is linked with aos_rpc_server_network and net where the services are defined.
 *
 * version 2017-12-03, group C
 */

#include <stdio.h>
#include <aos/aos.h>
#include <aos/connect/all.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>
#include <aos/rpc_shared/aos_rpc_shared_network.h>
#include <aos/caddr.h>
#include <net/network_driver.h>
#include <sys/socket.h>


// include service server functions
// adjust Hakefile to link with associated library
#include <aos_rpc_server/aos_rpc_server_network.h>

static errval_t register_network(void) {

    errval_t err = SYS_ERR_OK;

    #define NET_ABS_PATH "/core/network"
    #define NET_SHORT_NAME "network"
    #define NET_DESCRIPTION "Network service"

    #define NET_PATRON "Charon: Ferryman of the Styx"
    #define NET_CAPACITY 5

    struct serializable_key_value_store *store = create_kv_store(NET_CAPACITY);
    if (!store) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "create_kv_store() failed");
        return err;
    }

    kv_store_set(store, "description", NET_DESCRIPTION);
    kv_store_set(store, "patron", NET_PATRON);

    struct service_registration_reply *reply;
    // NOTE: as long as network is embedded in monitor/0, we need to register
    // aos_rpc_monitor_event_handler as event handler, which is adjusted accordingly
    // to also handle network FG.
    err = aos_rpc_service_register(NET_ABS_PATH, NET_SHORT_NAME, AOS_RPC_INTERFACE_NETWORK, true,
                                   store, aos_rpc_network_server_event_handler, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_service() failed");
        return err;
    }

    return SYS_ERR_OK;
}

int main(int argc, char *argv[]) {

    struct capref cap;
    errval_t err = aos_rpc_device_get_irq_cap(aos_rpc_get_device_channel(), &cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_device_get_irq_cap.\n");
        return 1;
    }

    err = cap_copy(cap_irq, cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "cap_copy.\n");
        return 1;
    }

        // bootstrap network driver
    err = network_driver_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "network_driver_init.\n");
        return 1;
    }

    // register network/0
    err = register_network();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "register_network() failed.\n");
        return 1;
    }

    debug_printf("Charon Ferryman of the Styx greets you.\n");

    while (true) {
        err = event_dispatch(get_default_waitset());
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }



    return 0;
}
