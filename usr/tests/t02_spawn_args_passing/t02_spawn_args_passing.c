#include <stdio.h>

#include <aos/aos_rpc.h>

int main(int argc, char *argv[])
{
    debug_printf("Start test\n");
    if (argc != 7) {
        debug_printf("Test failed!\n");
        debug_printf("The number of arguments does not match.\n");
        return -1;
    }
    char *argv_ref[] = {
        "t02_spawn_args_passing",
        "Hello",
        "world",
        "from",
        "arguments",
        "passing",
        "test"
    };
    for (int i = 0; i < argc; i++) {
        if (strncmp(argv[i], argv_ref[i], strlen(argv_ref[i]))) {
            debug_printf("Test failed!\n");
            debug_printf("Expected argument %d to be \"%s\" but received \"%s\".\n", i + 1, argv_ref[i], argv[i]);
            return -2;
        }
        else {
            debug_printf("Argument %d: %s\n", i + 1, argv[i]);
        }
    }
    debug_printf("All passed arguments match the expected values.\n");
    debug_printf("Test successful!\n");
    debug_printf("Press any key to continue.\n");
    return 0;
}
