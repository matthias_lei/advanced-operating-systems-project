/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */


#include <stdio.h>
#include <math.h>

#include <aos/aos_rpc.h>

#define ALLOC_SIZE 1024

static void rec(uint32_t i) {
    char alloc[ALLOC_SIZE];
    alloc[0] = '\0';
    printf("Completed recursion %u, stack at %p.\n", i, &alloc[0]);
    rec(i+1);
}

int main(int argc, char *argv[])
{
    printf("Starting test Stackeater, i.e. a infinite recursion allocating about %d KiB on stack per recursive iteration.\n", ALLOC_SIZE / 1024);

    rec(0);
    
    return 0;
}
