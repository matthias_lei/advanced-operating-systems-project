/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */


#include <stdio.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#define MALLOC_SIZE 100*1024*1024

#define ADDR1 0
#define ADDR2 BASE_PAGE_SIZE
#define ADDR3 (BASE_PAGE_SIZE * 10)
#define ADDR4 (BASE_PAGE_SIZE * 100 + 2048)
#define ADDR5 (BASE_PAGE_SIZE * 1000 + 42)
#define ADDR6 (MALLOC_SIZE - 1)


int main(int argc, char *argv[])
{
    printf("Starting test bigmalloc, that mallocs %d MiB and accesses some virtual adresses in the malloced region.\n", MALLOC_SIZE / 1024 / 1024);
    
    paging_print_state(get_current_paging_state());
    
    char *ptr = malloc(MALLOC_SIZE);

    // write somewhere into malloced region
    
    ptr[ADDR1] = 'M';
    ptr[ADDR2] = 'A';
    ptr[ADDR3] = 'L';
    ptr[ADDR4] = 'L';
    ptr[ADDR5] = 'O';
    ptr[ADDR6] = 'C';

    // check if everything was actually written
    assert(ptr[ADDR1] == 'M');
    assert(ptr[ADDR2] == 'A');
    assert(ptr[ADDR3] == 'L');
    assert(ptr[ADDR4] == 'L');
    assert(ptr[ADDR5] == 'O');
    assert(ptr[ADDR6] == 'C');

    // give also visual feedback
    printf("Written chars:\n");
    printf("%c at %p \n", ptr[ADDR1], &ptr[ADDR1]);
    printf("%c at %p \n", ptr[ADDR2], &ptr[ADDR2]);
    printf("%c at %p \n", ptr[ADDR3], &ptr[ADDR3]);
    printf("%c at %p \n", ptr[ADDR4], &ptr[ADDR4]);
    printf("%c at %p \n", ptr[ADDR5], &ptr[ADDR5]);
    printf("%c at %p \n", ptr[ADDR6], &ptr[ADDR6]);

    paging_print_state(get_current_paging_state());
    
    return 0;
}
