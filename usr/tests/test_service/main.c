/**
 * This test service simulates an /app/test service, currently offering a shell FG
 * that stops auto-pinging to the nameserver due to any reason.
 *
 * This is simulated by an incomplete manual registration (without auto-ping).
 *
 * Test procedure:
 * - start the service (run -b t)
 * - launch apollo and e.g. look for /app prefix services: e.g. apollo -p /app
 * - this service shows up
 * - after some time the service does no longer show up because it was
 *   automatically de-registered
 *
 * The test can be repeated by another launch
 *
 * AOS group C, individual projects: nameserver
 * version 2017-12-21, Pirmin Schmid
 */

#include <stdio.h>
#include <aos/aos.h>
#include <aos/deferred.h>
#include <aos/connect/all.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>

#include <aos_rpc_server/aos_rpc_server_shell.h>
#include <nameserver_types.h>
#include <shell_shared.h>

struct process_config current_process_config;
bool waiting_for_termination;

#define NAME "Anonymous test service"

// specialized for test service
// incomplete self-registration in database without auto-ping
// it pretends to offer SHELL functionality
static errval_t self_register_service(void)
{
    // register
    #define ABS_PATH "/app/test"
    #define SHORT_NAME "test"
    #define DESCRIPTION "Test service"
    #define PATRON "Anonymous test service"
    #define CAPACITY 3
    struct serializable_key_value_store *store = create_kv_store(CAPACITY);

    kv_store_set(store, "description", DESCRIPTION);
    kv_store_set(store, "patron", PATRON);

    struct aos_rpc *rpc = aos_rpc_get_name_channel();
    if (!rpc) {
        debug_printf("%s: name RPC channel not found.\n", __func__);
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    // create new service endpoint
    struct capref service_cap;
    errval_t err = slot_alloc(&service_cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in slot_alloc()\n");
        return err;
    }

    #define NAME_CORE 0
    bool same_core = NAME_CORE == disp_get_core_id();

    struct ump_service_create_data data;
    if (same_core) {
        err = setup_lmp_service(service_cap, MKCLOSURE(aos_rpc_shell_server_event_handler, NULL));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in setup_lmp_service()\n");
            return err;
        }
    }
    else {
        // frame needs to be allocated before calling the setup
        size_t size;
        err = frame_alloc(&service_cap, BASE_PAGE_SIZE, &size);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Cannot allocate frame\n");
            return err;
        }

        err = setup_ump_service_part1(service_cap, &data);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in setup_ump_service_part1()\n");
            return err;
        }
    }

    // register
    struct service_registration_request request = {
        .core_id = disp_get_core_id(),
        .interface = AOS_RPC_INTERFACE_SHELL,
        .contact_chan_type = same_core ? AOS_RPC_CHAN_DRIVER_LMP : AOS_RPC_CHAN_DRIVER_UMP,
        .high_bandwidth_service = false,
        .abs_path = ABS_PATH,
        .short_name_prefix = SHORT_NAME,
        .kv_store = store
    };

    struct service_registration_reply *reply;
    err = aos_rpc_name_service_register(rpc, &request, service_cap, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_name_service_register() failed\n");
        return err;
    }

    if (!same_core) {
        err = setup_ump_service_part2(&data, MKCLOSURE(aos_rpc_shell_server_event_handler, NULL));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in setup_ump_service_part2()\n");
            return err;
        }
    }

    return SYS_ERR_OK;
}


int main(int argc, char *argv[])
{
    // launch message
    debug_printf("Greetings from an anonymous test service\n");

    errval_t err = SYS_ERR_OK;

    // self-registration
    err = self_register_service();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "self_register_service() failed.\n");
        return 1;
    }

    // react to events
    struct waitset *default_ws = get_default_waitset();
    while (true) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }

    return 0;
}
