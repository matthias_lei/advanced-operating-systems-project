/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

#include <aos/aos.h>
#include <aos/aos_rpc.h>

int main(int argc, char* argv[]) {
    while (true) {
        domainid_t did;
        errval_t err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "hello", disp_get_core_id(), &did);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "main(): aos_rpc_process_spawn failed\n");
            return -1;
        }
    }
}
