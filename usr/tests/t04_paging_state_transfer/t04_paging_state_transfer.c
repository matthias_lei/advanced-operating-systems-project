/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

#include <aos/aos.h>
#include <arch/arm/aos/dispatcher_arch.h>
#include <barrelfish_kpi/dispatcher_handle.h>
#include <barrelfish_kpi/domain_params.h>

static char test_char = 't';

// This test tries to map a frame at a virtual address space that is known to have already been mapped.
// Such a location is for example the code section of the currently executing dispatcher,
// thus we can extract the program counter from register r15 and try to map a frame at this address again.
// Another address to try, is the address of a static global variable such as the "test_char" defined above.
int main(int argc, char* argv[])
{
    uintptr_t program_counter;
    __asm("mov %[program_counter], r15" : [program_counter] "=r" (program_counter));
    debug_printf("Program counter is at %p\n", program_counter);
    lvaddr_t base = ((lvaddr_t)program_counter & (~BASE_PAGE_MASK));
    struct capref frame;
    size_t ret_bytes;
    errval_t err = frame_alloc(&frame, BASE_PAGE_SIZE, &ret_bytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "main(): frame_alloc failed.\n");
        return -1;
    }
    if (ret_bytes != BASE_PAGE_SIZE) {
        debug_printf("Did not receive the expected frame size.\n");
        return -1;
    }
    err = paging_map_fixed_attr(get_current_paging_state(), base, frame, ret_bytes, VREGION_FLAGS_READ_WRITE);
    if (!err_is_fail(err)) {
        debug_printf("Test failed.\n");
        debug_printf("It should not be allowed to map at an already mapped address in the virtual address space.\n");
        return -1;
    }
    debug_printf("First test successful.\n");
    debug_printf("It was not allowed to map at an already mapped address in the virtual address space.\n");

    debug_printf("Static character is at %p\n", &test_char);
    base = ((lvaddr_t)&test_char & (~BASE_PAGE_MASK));
    err = paging_map_fixed_attr(get_current_paging_state(), base, frame, ret_bytes, VREGION_FLAGS_READ_WRITE);
    if (!err_is_fail(err)) {
        debug_printf("Test failed.\n");
        debug_printf("It should not be allowed to map at an already mapped address in the virtual address space.\n");
        return -1;
    }
    debug_printf("Second test successful.\n");
    debug_printf("It was not allowed to map at an already mapped address in the virtual address space.\n");
    debug_printf("Test finished.\n");

    return 0;
}
