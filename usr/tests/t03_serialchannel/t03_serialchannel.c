#include <stdio.h>

#include <aos/aos_rpc.h>

int main(int argc, char *argv[])
{

    debug_printf("TEST saying hello via RPC.\n");

    printf("Hello world via RPC!\n");

    struct aos_rpc *serial_channel = aos_rpc_get_serial_channel();

    debug_printf("TEST getting character from terminal via RPC.\n");

    char c;
    errval_t err = aos_rpc_serial_getchar(serial_channel, &c);
    if (err_is_fail(err)) {
        debug_printf("aos_rpc_serial_getchar() failed.\n");
        return err;
    }

    debug_printf("Got character: %c.\n", c);
    
    return 0;
}
