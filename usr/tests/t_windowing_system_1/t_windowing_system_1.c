/*
    Used in test t02_spawn_two_test
*/

#include <stdio.h>

#define SLEEP_CONST 300

static void sleep_helper(int approx_ms) {
    volatile int dummy = 0;
    for (int i = 0; i < approx_ms; i++) {
        for (int j = 0; j < SLEEP_CONST; j++) {
            dummy = 0;
            for (int k = 0; k < SLEEP_CONST; k++) {
                dummy += 1;
            }
        }
    }
}


#define WAIT_APPROX_MS 1000

int main(int argc, char *argv[])
{
    
    int iteration = 1;    
    while (1) {
        // 20 whitespaces
        printf("                    Hello from process 1! Iteration %d.\n", iteration);

        if (iteration % 4 == 0) {
            printf("                    Getting char:\n");
            char c = getchar();
            printf("                    Got: %c.\n", c);
            
        }

        sleep_helper(WAIT_APPROX_MS);
        iteration++;
    }

    return 0;
}
