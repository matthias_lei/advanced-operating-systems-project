#include <stdio.h>

#include <aos/aos.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    printf("Starting UDP echo server\n");

    int socket_id;
    socket_id = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_id == -1) {
        debug_printf("socket failed: %u\n", errno);
        return 1;        
    }
    
    printf("socket created with id: %u\n", socket);

    struct sockaddr_in host_addr;
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = 0;
    host_addr.sin_addr.s_addr = INADDR_ANY;

    int ret_value;
    ret_value = bind(socket_id, (struct sockaddr *)&host_addr, sizeof(struct sockaddr_in));
    if (ret_value != 0) {
        debug_printf("bind failed: %u\n", errno);
        return 2;
    }

    printf("socket bound\n");

    struct sockaddr_in bound_addr;
    socklen_t bound_addr_len = sizeof(struct sockaddr_in);

    ret_value = getsockname(socket_id, (struct sockaddr *)&bound_addr, &bound_addr_len);
    if (ret_value != 0) {
        debug_printf("getsockname failed: %u\n", errno);
        return 3;
    }

    printf("socket address: %s:%hu\n", inet_ntoa(bound_addr.sin_addr), bound_addr.sin_port);

    size_t msg_buf_len = 128;
    char msg_buf[msg_buf_len];
    struct sockaddr_in src_addr;
    socklen_t src_addr_len = sizeof(src_addr);
    ssize_t msg_recv_len;
    ssize_t msg_send_len;

    memset(&msg_buf, 0, msg_buf_len);

    while(true) {

        printf("receiving...\n");

        msg_recv_len = recvfrom(socket_id, &msg_buf, msg_buf_len, 0, (struct sockaddr *)&src_addr, &src_addr_len);
        if (msg_recv_len == -1) {
            debug_printf("recvfrom failed: %u\n", errno);
            debug_printf("Continuing...\n");
            continue;
        }
        
        printf("message received from %s:%hu (payload: size %u)\n", inet_ntoa(src_addr.sin_addr), src_addr.sin_port, msg_recv_len);
        printf("Message (first 32 chars): %.32s\n", msg_buf);

        msg_send_len = sendto(socket_id, &msg_buf, msg_recv_len, 0, (struct sockaddr *)&src_addr, sizeof(src_addr));
        if (msg_send_len == -1 || msg_send_len != msg_recv_len) {
            debug_printf("sendto failed: %u\n", errno);
            debug_printf("Continuing...\n");
            memset(&msg_buf, 0, msg_buf_len);
            continue;
        }

        memset(&msg_buf, 0, msg_buf_len);
    }    


    return 0;
}