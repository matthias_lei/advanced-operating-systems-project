/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */


#include <stdio.h>

#include <aos/aos_rpc.h>

int main(int argc, char *argv[])
{
    printf("Hello, world! from userspace\n");

    // note: we use the serial channel instead of the init channel to be able to test
    // LMP and UMP communication depending on which core Hello is launched
    debug_printf("TEST sending a number over rpc\n");
    errval_t err = aos_rpc_send_number(aos_rpc_get_serial_channel(), 42);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failure during aos_rpc_send_number\n");
    }

    err = aos_rpc_send_number(aos_rpc_get_serial_channel(), 99);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failure during aos_rpc_send_number 2\n");
    }

    err = aos_rpc_send_number(aos_rpc_get_serial_channel(), 12345678);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failure during aos_rpc_send_number 3\n");
    }

    debug_printf("TEST ended. Did the number print?\n");
    return 0;
}
