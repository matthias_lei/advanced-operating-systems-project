/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */


#include <stdio.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#define SLEEP_CONST 300

// the one and only sleep function by pisch
static void sleep_helper(int approx_ms) {
    volatile int dummy = 0;
    for (int i = 0; i < approx_ms; i++) {
        for (int j = 0; j < SLEEP_CONST; j++) {
            dummy = 0;
            for (int k = 0; k < SLEEP_CONST; k++) {
                dummy += 1;
            }
        }
    }
}


#define N_THREADS_1 2
#define N_THREADS_2 2
#define N_THREADS_3 2

#define MALLOC_SIZE 20 * 1024 * 1024
#define ADDR1 0
#define ADDR2 MALLOC_SIZE / 2
#define N_PAGES 100

STATIC_ASSERT(ADDR1 + BASE_PAGE_SIZE * N_PAGES <= MALLOC_SIZE, "parameters would lead to access outside of malloced region.");
STATIC_ASSERT(ADDR2 + BASE_PAGE_SIZE * N_PAGES <= MALLOC_SIZE, "parameters would lead to access outside of malloced region.");


static int thread1(void *arg) {
    uint32_t id = thread_get_id(thread_self());
    ATOMIC_DEBUG_PRINTF("Hello from thread %d.\n", id);

    sleep_helper(500);

    char *ptr = malloc(MALLOC_SIZE);
    if (!ptr) {
        ATOMIC_DEBUG_PRINTF("malloc of thread %d failed\n", id);
        return 1;
    }

    for (uint32_t i = 0; i < N_PAGES; i++) {
        ptr[ADDR1 + i*BASE_PAGE_SIZE] = '1';
        ptr[ADDR2 + i*BASE_PAGE_SIZE] = '2';

        if (i % 10 == 0) {
            sleep_helper(10);
        }
    }

    return 0;
}

static int thread2(void *arg) {
    uint32_t id = thread_get_id(thread_self());
    ATOMIC_DEBUG_PRINTF("Hello from thread %d.\n", id);

    sleep_helper(500);

    char *ptr = malloc(MALLOC_SIZE);    
    if (!ptr) {
        ATOMIC_DEBUG_PRINTF("malloc of thread %d failed\n", id);
        return 1;
    }

    for (uint32_t i = 0; i < N_PAGES; i++) {
        ptr[ADDR1 + i*BASE_PAGE_SIZE] = '1';
        ptr[ADDR2 + i*BASE_PAGE_SIZE] = '2';

        if (i % 10 == 0) {
            sleep_helper(50);
        }
    }

    return 0;
}

static int thread3(void *arg) {
    uint32_t id = thread_get_id(thread_self());
    ATOMIC_DEBUG_PRINTF("Hello from thread %d.\n", id);

    sleep_helper(500);

    char *ptr = malloc(MALLOC_SIZE);
    if (!ptr) {
        ATOMIC_DEBUG_PRINTF("malloc of thread %d failed\n", id);
        return 1;
    }  

    for (uint32_t i = 0; i < N_PAGES; i++) {
        ptr[ADDR1 + i*BASE_PAGE_SIZE] = '1';
        ptr[ADDR2 + i*BASE_PAGE_SIZE] = '2';

        if (i % 10 == 0) {
            sleep_helper(100);
        }
    }

    return 0;
}


int main(int argc, char *argv[])
{
    
    ATOMIC_DEBUG_PRINTF("Creating %d threads, triggering page faults.\n", N_THREADS_1 + N_THREADS_2 + N_THREADS_3);

    struct thread *threads[N_THREADS_1 + N_THREADS_2 + N_THREADS_3];

    for (int i = 0; i < N_THREADS_1; i++) {
        threads[i] = thread_create(thread1, NULL);
        if (!threads[i]) {
            ATOMIC_DEBUG_PRINTF("Creation of thread %d failed.\n", i);
            return 1;
        }
    }

    for (int i = 0; i < N_THREADS_2; i++) {
        threads[N_THREADS_1 + i] = thread_create(thread2, NULL);
        if (!threads[N_THREADS_1 + i]) {
            ATOMIC_DEBUG_PRINTF("Creation of thread %d failed.\n", N_THREADS_1 + i);
            return 1;
        }
    }

    for (int i = 0; i < N_THREADS_3; i++) {
        threads[N_THREADS_1 + N_THREADS_2 + i] = thread_create(thread3, NULL);
        if (!threads[N_THREADS_1 + N_THREADS_2 + i]) {
            ATOMIC_DEBUG_PRINTF("Creation of thread %d failed.\n", N_THREADS_1 + N_THREADS_2 + i);
            return 1;
        }
    }

    errval_t err;
    int ret; // needed but not used
    for (int i = 0; i < N_THREADS_1 + N_THREADS_2 + N_THREADS_3; i++) {
        err = thread_join(threads[i], &ret);
        if (err_is_fail(err)) {
            ATOMIC_DEBUG_PRINTF("Error while joining thread %d.\n", thread_get_id(threads[i]));
            return 1;
        }
    }

    ATOMIC_DEBUG_PRINTF("All threads joined succesfully.\n");
    
    return 0;
}
