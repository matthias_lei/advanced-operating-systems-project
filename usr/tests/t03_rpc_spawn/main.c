/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */


#include <stdio.h>

#include <aos/aos_rpc.h>

int main(int argc, char *argv[])
{
    printf("Hello, world! from userspace\n");

    debug_printf("My domain ID is %d.\n", disp_get_domain_id());

    debug_printf("Spawning a new hello domain.\n");

    domainid_t newpid;
    errval_t err = aos_rpc_process_spawn(aos_rpc_get_process_channel(), "t03_rpc_spawn", 0, &newpid);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Failed to spawn new process.\n");
        return -1;
    }
    debug_printf("Successfully spawned a new hello domain with ID %d.\n", newpid);
    return 0;
}

