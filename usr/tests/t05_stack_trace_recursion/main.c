/**
 * \file
 * \brief Short test program to see a stack trace
 * version 2017-11-05 Group C
 */


#include <stdio.h>

#include <aos/aos.h>

#define AOS_TESTING_DEBUGGING_MAIN
#include <testing/debugging.h>

#define RECURSION_N 3

#define SLEEP_CONST 300

// note: some content is needed in the functions to prevent the compiler to make too many optimizations

// the one and only sleep function by pisch
static void sleep_helper(int approx_ms) {
    volatile int dummy = 0;
    for (int i = 0; i < approx_ms; i++) {
        for (int j = 0; j < SLEEP_CONST; j++) {
            dummy = 0;
            for (int k = 0; k < SLEEP_CONST; k++) {
                dummy += 1;
            }
        }
    }
}


// it's interesting to see what happens if one function in the ABC chain is defined static
// then it gets just inlined with a shorter stack trace despite proper output
// Thus, the ABC chain is defined in 2 variants

// also note: the stack trace name for static function D differs whether it is called
// with the same argument or different arguments.
// D with different arguments
// D.constprop.0 with the same argument
// see compiler optimizations

int A1(int n);
int B1(int n);
int C1(int n);

int A2(int n);
int C2(int n);

// to compare the behavior with a static function without symbol information stored
// BTW: tests show that this function just gets inlined by the compiler
static int D(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(400);
    DEBUG_STACK_TRACE_FROM_HERE("Hi from a static function");
    int value = n;
    DEBUGGING_EXIT;
    return value;
}

int C1(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(300);
    if (n >= RECURSION_N) {
        DEBUG_STACK_TRACE_FROM_HERE("Hi from several recursive function calls");
        int value = D(6); // an adjustment to have 42 at the end
        DEBUGGING_EXIT;
        return value;
    }

    int value = A1(n + 1) + n + 3;
    DEBUGGING_EXIT;
    return value;
}


int B1(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(200);
    int value = C1(n) + n + 2;
    DEBUGGING_EXIT;
    return value;
}

int A1(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(100);
    int value = B1(n) + n + 1;
    DEBUGGING_EXIT;
    return value;
}


int C2(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(300);
    if (n >= RECURSION_N) {
        DEBUG_STACK_TRACE_FROM_HERE("Hi from several recursive function calls");
        int value = D(6); // an adjustment to have 42 at the end
        DEBUGGING_EXIT;
        return value;
    }

    int value = A2(n + 1) + n + 3;
    DEBUGGING_EXIT;
    return value;
}


static int B2(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(200);
    int value = C2(n) + n + 2;
    DEBUGGING_EXIT;
    return value;
}

int A2(int n)
{
    DEBUGGING_ENTER;
    sleep_helper(100);
    int value = B2(n) + n + 1;
    DEBUGGING_EXIT;
    return value;
}


int main(int argc, char *argv[])
{
    DEBUGGING_ENTER;
    paging_print_state(get_current_paging_state());
    int sum = A1(0);
    debug_printf("sum %d\n", sum);

    sum = A2(0);
    debug_printf("sum %d\n", sum);
    DEBUGGING_EXIT;       
    return 0;
}
