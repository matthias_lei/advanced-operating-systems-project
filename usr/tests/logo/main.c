/**
 * \file
 * \brief Hello world application
 */

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */


#include <stdio.h>

int main(int argc, char *argv[]) {
    // LOGO generated using http://patorjk.com/software/taag/#p=display&f=Big&t=OlympOS
    printf("   ____  _                        ____   _____\n");
    printf("  / __ \\| |                      / __ \\ / ____|\n");
    printf(" | |  | | |_   _ _ __ ___  _ __ | |  | | (___\n");
    printf(" | |  | | | | | | '_ ` _ \\| '_ \\| |  | |\\___ \\\n");
    printf(" | |__| | | |_| | | | | | | |_) | |__| |____) |\n");
    printf("  \\____/|_|\\__, |_| |_| |_| .__/ \\____/|_____/\n");
    printf("            __/ |         | |\n");
    printf("           |___/          |_|\n");
    printf("\n");
    return 0;
}
