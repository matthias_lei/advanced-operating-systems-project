#include <fs/multibootfs.h>

/**
 * structure for the common fields of a directory entry
 * (file or directory) in the multiboot FS
 */
struct multiboot_dir_entry {
    struct multiboot_dir_entry *next;   //< next directory entry on the same level (sibling)
    struct multiboot_dir_entry *child;  //< first child (directories only)
    enum fs_filetype type;              //< entry type (file or directory)
    char *name;                         //< name of the entry
};

/**
 * structure for a file entry in the multiboot FS
 */
struct multiboot_file_entry {
    struct multiboot_dir_entry dir;     //< common directory entry fields
    char *path;                         //< full path of the file used for easy module lookup
    size_t size;                        //< size of the file
    void *content;                      //< pointer to the begin of the file content (NULL if not yet loaded)
};

/**
 * structure for the common handle fields
 */
struct multiboot_common_handle {
    struct multiboot_dir_entry *dir_entry;  //< the directory entry this handle refers to
};

/**
 * structure for a directory handle
 */
struct multiboot_dir_handle {
    struct multiboot_common_handle common;  //< common handle fields
    struct multiboot_dir_entry *current;    //< pointer to the current child in the read_next_dir iteration
};

/**
 * structure for a file handle
 */
struct multiboot_file_handle {
    struct multiboot_common_handle common;  //< common handle fields
    size_t data_offset;                     //< current seek position
};

/**
 * structure for storing mount information
 */
struct multiboot_mount {
    struct multiboot_dir_entry root;        //< root directory entry
};

//#define ENABLE_MULTIBOOT_DEBUGGING

#ifdef ENABLE_MULTIBOOT_DEBUGGING
#define MULTIBOOT_DEBUG(msg...) debug_printf(msg)
#define MULTIBOOT_DEBUG_ERR(args...) DEBUG_ERR(args)
#else
#define MULTIBOOT_DEBUG(msg...)
#define MULTIBOOT_DEBUG_ERR(args...)
#endif
