/**
 * Private API and structure definitions for the FAT filesystem
 * implementation.
 *
 * Group C
 */

#ifndef LIB_FS_INCLUDE_FAT_FATFS_P_H_
#define LIB_FS_INCLUDE_FAT_FATFS_P_H_

#include <fs/fatfs.h>

struct FAT_BPB {
    /*
    Jump instruction to boot code. This field has two allowed forms:
    jmpBoot[0] = 0xEB, jmpBoot[1] = 0x??, jmpBoot[2] = 0x90
    and
    jmpBoot[0] = 0xE9, jmpBoot[1] = 0x??, jmpBoot[2] = 0x??

    0x?? indicates that any 8-bit value is allowed in that byte. What this
    forms is a three-byte Intel x86 unconditional branch (jump)
    instruction that jumps to the start of the operating system bootstrap
    code. This code typically occupies the rest of sector 0 of the volume
    following the BPB and possibly other sectors. Either of these forms
    is acceptable. JmpBoot[0] = 0xEB is the more frequently used
    format.
    */
    uint8_t BS_jmpBoot[3];

    /*
    “MSWIN4.1” There are many misconceptions about this field. It is
    only a name string. Microsoft operating systems don’t pay any
    attention to this field. Some FAT drivers do. This is the reason that
    the indicated string, “MSWIN4.1”, is the recommended setting,
    because it is the setting least likely to cause compatibility problems.
    If you want to put something else in here, that is your option, but
    the result may be that some FAT drivers might not recognize the
    volume. Typically this is some indication of what system formatted
    the volume.
    */
    char BS_OEMName[8];

    /*
    Count of bytes per sector. This value may take on only the
    following values: 512, 1024, 2048 or 4096. If maximum
    compatibility with old implementations is desired, only the value
    512 should be used. There is a lot of FAT code in the world that is
    basically “hard wired” to 512 bytes per sector and doesn’t bother to
    check this field to make sure it is 512. Microsoft operating systems
    will properly support 1024, 2048, and 4096.

    Note: Do not misinterpret these statements about maximum
    compatibility. If the media being recorded has a physical sector size
    N, you must use N and this must still be less than or equal to 4096.
    Maximum compatibility is achieved by only using media with
    specific sector sizes.
    */
    uint16_t BPB_BytsPerSec;

    /*
    Number of sectors per allocation unit. This value must be a power
    of 2 that is greater than 0. The legal values are 1, 2, 4, 8, 16, 32, 64,
    and 128. Note however, that a value should never be used that
    results in a “bytes per cluster” value (BPB_BytsPerSec *
    BPB_SecPerClus) greater than 32K (32 * 1024). There is a
    misconception that values greater than this are OK. Values that
    cause a cluster size greater than 32K bytes do not work properly; do
    not try to define one. Some versions of some systems allow 64K
    bytes per cluster value. Many application setup programs will not
    work correctly on such a FAT volume.
    */
    uint8_t BPB_SecPerClus;

    /*
    Number of reserved sectors in the Reserved region of the volume
    starting at the first sector of the volume. This field must not be 0.
    For FAT12 and FAT16 volumes, this value should never be
    anything other than 1. For FAT32 volumes, this value is typically
    32. There is a lot of FAT code in the world “hard wired” to 1
    reserved sector for FAT12 and FAT16 volumes and that doesn’t
    bother to check this field to make sure it is 1. Microsoft operating
    systems will properly support any non-zero value in this field.
    */
    uint16_t BPB_RsvdSecCnt;

    /*
    The count of FAT data structures on the volume. This field should
    always contain the value 2 for any FAT volume of any type.
    Although any value greater than or equal to 1 is perfectly valid,
    many software programs and a few operating systems’ FAT file
    system drivers may not function properly if the value is something
    other than 2. All Microsoft file system drivers will support a value
    other than 2, but it is still highly recommended that no value other
    than 2 be used in this field.

    The reason the standard value for this field is 2 is to provide redun-
    dancy for the FAT data structure so that if a sector goes bad in one
    of the FATs, that data is not lost because it is duplicated in the other
    FAT. On non-disk-based media, such as FLASH memory cards,
    where such redundancy is a useless feature, a value of 1 may be
    used to save the space that a second copy of the FAT uses, but some
    FAT file system drivers might not recognize such a volume
    properly.
    */
    uint8_t BPB_NumFATs;

    /*
    For FAT12 and FAT16 volumes, this field contains the count of 32-
    byte directory entries in the root directory. For FAT32 volumes, this
    field must be set to 0. For FAT12 and FAT16 volumes, this value
    should always specify a count that when multiplied by 32 results in
    an even multiple of BPB_BytsPerSec. For maximum compatibility,
    FAT16 volumes should use the value 512.
    */
    uint16_t BPB_RootEntCnt;

    /*
    This field is the old 16-bit total count of sectors on the volume. This
    count includes the count of all sectors in all four regions of the
    volume. This field can be 0; if it is 0, then BPB_TotSec32 must be
    non-zero. For FAT32 volumes, this field must be 0. For FAT12 and
    FAT16 volumes, this field contains the sector count, and
    BPB_TotSec32 is 0 if the total sector count “fits” (is less than
    0x10000).
    */
    uint16_t BPB_TotSec16;

    /*
    0xF8 is the standard value for “fixed” (non-removable) media. For
    removable media, 0xF0 is frequently used. The legal values for this
    field are 0xF0, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, and
    0xFF. The only other important point is that whatever value is put
    in here must also be put in the low byte of the FAT[0] entry. This
    dates back to the old MS-DOS 1.x media determination noted
    earlier and is no longer usually used for anything.
    */
    uint8_t BPB_Media;

    /*
    This field is the FAT12/FAT16 16-bit count of sectors occupied by
    ONE FAT. On FAT32 volumes this field must be 0, and
    BPB_FATSz32 contains the FAT size count.
    */
    uint16_t BPB_FATSz16;

    /*
    Sectors per track for interrupt 0x13. This field is only relevant for
    media that have a geometry (volume is broken down into tracks by
    multiple heads and cylinders) and are visible on interrupt 0x13.
    This field contains the “sectors per track” geometry value.
    */
    uint16_t BPB_SecPerTrk;

    /*
    Number of heads for interrupt 0x13. This field is relevant as
    discussed earlier for BPB_SecPerTrk. This field contains the one
    based “count of heads”. For example, on a 1.44 MB 3.5-inch floppy
    drive this value is 2.
    */
    uint16_t BPB_NumHeads;

    /*
    Count of hidden sectors preceding the partition that contains this
    FAT volume. This field is generally only relevant for media visible
    on interrupt 0x13. This field should always be zero on media that
    are not partitioned. Exactly what value is appropriate is operating
    system specific.
    */
    uint32_t BPB_HiddSec;

    /*
    This field is the new 32-bit total count of sectors on the volume.
    This count includes the count of all sectors in all four regions of the
    volume. This field can be 0; if it is 0, then BPB_TotSec16 must be
    non-zero. For FAT32 volumes, this field must be non-zero. For
    FAT12/FAT16 volumes, this field contains the sector count if
    BPB_TotSec16 is 0 (count is greater than or equal to 0x10000).
    */
    uint32_t BPB_TotSec32;
} __attribute__((packed));

struct FAT_BPB_FAT16 {
    /*
    Int 0x13 drive number (e.g. 0x80). This field supports MS-DOS
    bootstrap and is set to the INT 0x13 drive number of the media
    (0x00 for floppy disks, 0x80 for hard disks).
    NOTE: This field is actually operating system specific.
    */
    uint8_t BS_DrvNum;

    /*
    Reserved (used by Windows NT). Code that formats FAT volumes
    should always set this byte to 0.
    */
    uint8_t BS_Reserved1;

    /*
    Extended boot signature (0x29). This is a signature byte that
    indicates that the following three fields in the boot sector are
    present.
    */
    uint8_t BS_BootSig;

    /*
    Volume serial number. This field, together with BS_VolLab,
    supports volume tracking on removable media. These values allow
    FAT file system drivers to detect that the wrong disk is inserted in a
    removable drive. This ID is usually generated by simply combining
    the current date and time into a 32-bit value.
    */
    uint32_t BS_VolID;

    /*
    Volume label. This field matches the 11-byte volume label recorded
    in the root directory.
    NOTE: FAT file system drivers should make sure that they update
    this field when the volume label file in the root directory has its
    name changed or created. The setting for this field when there is no
    volume label is the string “NO NAME    ”.
    */
    char BS_VolLab[11];

    /*
    One of the strings “FAT12   ”, “FAT16   ”, or “FAT     ”.
    NOTE: Many people think that the string in this field has
    something to do with the determination of what type of FAT—
    FAT12, FAT16, or FAT32—that the volume has. This is not true.
    You will note from its name that this field is not actually part of the
    BPB. This string is informational only and is not used by Microsoft
    file system drivers to determine FAT typ,e because it is frequently
    not set correctly or is not present. See the FAT Type Determination
    section of this document. This string should be set based on the FAT
    type though, because some non-Microsoft FAT file system drivers
    do look at it.
    */
    char BS_FilSysType[8];
} __attribute__((packed));

struct FAT_BPB_FAT32 {
    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media. This field is the FAT32 32-bit count of
    sectors occupied by ONE FAT. BPB_FATSz16 must be 0.
    */
    uint32_t BPB_FATSz32;

    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media.
    Bits 0-3    --  Zero-based number of active FAT. Only valid if mirroring is
                    disabled.
    Bits 4-6    --  Reserved.
    Bit 7       --  0 means the FAT is mirrored at runtime into all FATs.
                --  1 means only one FAT is active; it is the one referenced
                    in bits 0-3.
    Bits 8-15   --  Reserved.
    */
    uint16_t BPB_ExtFlags;

    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media. High byte is major revision number. Low
    byte is minor revision number. This is the version number of the
    FAT32 volume. This supports the ability to extend the FAT32 media
    type in the future without worrying about old FAT32 drivers
    mounting the volume. This document defines the version to 0:0. If
    this field is non-zero, back-level Windows versions will not mount
    the volume.
    NOTE: Disk utilities should respect this field and not operate on
    volumes with a higher major or minor version number than that for
    which they were designed. FAT32 file system drivers must check
    this field and not mount the volume if it does not contain a version
    number that was defined at the time the driver was written.
    */
    uint8_t BPB_FSVer[2];

    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media. This is set to the cluster number of the
    first cluster of the root directory, usually 2 but not required to be 2.
    NOTE: Disk utilities that change the location of the root directory
    should make every effort to place the first cluster of the root
    directory in the first non-bad cluster on the drive (i.e., in cluster 2,
    unless it’s marked bad). This is specified so that disk repair utilities
    can easily find the root directory if this field accidentally gets
    zeroed.
    */
    uint32_t BPB_RootClus;

    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media. Sector number of FSINFO structure in the
    reserved area of the FAT32 volume. Usually 1.
    NOTE: There will be a copy of the FSINFO structure in BackupBoot,
    but only the copy pointed to by this field will be kept up to date (i.e.,
    both the primary and backup boot record will point to the same
    FSINFO sector).
    */
    uint16_t BPB_FSInfo;

    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media. If non-zero, indicates the sector number
    in the reserved area of the volume of a copy of the boot record.
    Usually 6. No value other than 6 is recommended.
    */
    uint16_t BPB_BkBootSec;

    /*
    This field is only defined for FAT32 media and does not exist on
    FAT12 and FAT16 media. Reserved for future expansion. Code that
    formats FAT32 volumes should always set all of the bytes of this
    field to 0.
    */
    char BPB_Reserved[12];

    /*
    This fields have the same definition as they do for FAT12 and FAT16.
    The only difference for FAT32 is that the fields are at a
    different offset in the boot sector.

    NOTE: BS_FilSysType should always contain the string "FAT32   ".
    */
    struct FAT_BPB_FAT16 BPB_FAT16;
} __attribute__((packed));

struct FAT {
    struct FAT_BPB FAT_BPB;
    union {
        struct FAT_BPB_FAT16 FAT_BPB_FAT16;
        struct FAT_BPB_FAT32 FAT_BPB_FAT32;
    };
} __attribute__((packed));

struct FAT_BootSector {
    struct FAT FAT;
    char Reserved[510 - sizeof(struct FAT)];
    uint16_t BS_Signature;
} __attribute__((packed));

struct FAT_DirEntry {
    /*
    Short name, split into name and extension <name>.<ext>

    -   If DIR_Name[0] == 0xE5, then the directory entry is free
        (there is no file or directory name in this entry).
    -   If DIR_Name[0] == 0x00, then the directory entry is free
        (same as for 0xE5), and there are no allocated directory
        entries after this one (all of the DIR_Name[0] bytes in
        all of the entries after this one are also set to 0).
        The special 0 value, rather than the 0xE5 value, indicates
        to FAT file system driver code that the rest of the entries
        in this directory do not need to be examined because they
        are all free.
    -   If DIR_Name[0] == 0x05, then the actual file name character
        for this byte is 0xE5. 0xE5 is actually a valid KANJI lead
        byte value for the character set used in Japan. The special
        0x05 value is used so that this special file name case for
        Japan can be handled properly and not cause FAT file system
        code to think that the entry is free.

    DIR_Name[0] may not equal 0x20. There is an implied ‘.’ character
    between the main part of the name and the extension part of the
    name that is not present in DIR_Name. Lower case characters are
    not allowed in DIR_Name (what these characters are is country
    specific).

    The following characters are not legal in any bytes of DIR_Name:
    -   Values less than 0x20 except for the special case of 0x05
        in DIR_Name[0] described above.
    -   0x22, 0x2A, 0x2B, 0x2C, 0x2E, 0x2F, 0x3A, 0x3B, 0x3C, 0x3D,
        0x3E, 0x3F, 0x5B, 0x5C, 0x5D, and 0x7C.
    */
    char DIR_Name[8];
    char DIR_NameExt[3];

    /*
    File attributes:
    ATTR_READ_ONLY      0x01
    ATTR_HIDDEN         0x02
    ATTR_SYSTEM         0x04
    ATTR_VOLUME_ID      0x08
    ATTR_DIRECTORY      0x10
    ATTR_ARCHIVE        0x20
    ATTR_LONG_NAME      ATTR_READ_ONLY | ATTR_HIDDEN | ATTR_SYSTEM | ATTR_VOLUME_ID

    The upper two bits of the attribute byte are reserved and should
    always be set to 0 when a file is created and never modified or
    looked at after that.
    */
    uint8_t DIR_Attr;

    /*
    Reserved for use by Windows NT. Set value to 0 when a file is
    created and never modify or look at it after that.
    */
    uint8_t DIR_NTRes;

    /*
    Millisecond stamp at file creation time. This field actually
    contains a count of tenths of a second. The granularity of the
    seconds part of DIR_CrtTime is 2 seconds so this field is a
    count of tenths of a second and its valid value range is 0-199
    inclusive.

    NOTE jmeier: This seems to be wrong in the Microsoft Documentation.
                 The field actually stores the millisecond stamp in a
                 resolution of 10ms, which actually makes sense with a
                 range from 0 to 199, wen DIR_CrtTime has a resolution
                 of 2 seconds.
    */
    uint8_t DIR_CrtTimeTenth;

    /*
    Time file was created.

    A FAT directory entry time stamp is a 16-bit field that has a
    granularity of 2 seconds. Here is the format (bit 0 is the LSB
    of the 16-bit word, bit 15 is the MSB of the 16-bit word).

    Bits 0–4:   2-second count, valid value range 0–29 inclusive
                (0 – 58 seconds).
    Bits 5–10:  Minutes, valid value range 0–59 inclusive.
    Bits 11–15: Hours, valid value range 0–23 inclusive.

    The valid time range is from Midnight 00:00:00 to 23:59:58.
    */
    uint16_t DIR_CrtTime;

    /*
    Date file was created.

    A FAT directory entry date stamp is a 16-bit field that is basically
    a date relative to the MS-DOS epoch of 01/01/1980. Here is the format
    (bit 0 is the LSB of the 16-bit word, bit 15 is the MSB of the 16-bit
    word):

    Bits 0–4:   Day of month, valid value range 1-31 inclusive.
    Bits 5–8:   Month of year, 1 = January, valid value range 1–12 inclusive.
    Bits 9–15:  Count of years from 1980, valid value range 0–127 inclusive (1980–2107).
    */
    uint16_t DIR_CrtDate;

    /*
    Last access date. Note that there is no last access time, only a date.
    This is the date of last read or write. In the case of a write, this
    should be set to the same date as DIR_WrtDate.
    */
    uint16_t DIR_LstAccDate;

    /*
    High word of this entry’s first cluster number (always 0 for a
    FAT12 or FAT16 volume).
    */
    uint16_t DIR_FstClusHI;

    /*
    Time of last write. Note that file creation is considered a write.
    */
    uint16_t DIR_WrtTime;

    /*
    Date of last write. Note that file creation is considered a write.
    */
    uint16_t DIR_WrtDate;

    /*
    Low word of this entry’s first cluster number.
    */
    uint16_t DIR_FstClusLO;

    /*
    32-bit DWORD holding this file’s size in bytes.
    */
    uint32_t DIR_FileSize;
};

#define LAST_LONG_ENTRY 0x40

struct FAT_LongDirEntry {
    /*
    The order of this entry in the sequence of long dir entries
    associated with the short dir entry at the end of the long dir set.

    If masked with 0x40 (LAST_LONG_ENTRY), this indicates the
    entry is the last long dir entry in a set of long dir entries. All
    valid sets of long dir entries must begin with an entry having this
    mask.
    */
    uint8_t LDIR_Ord;

    /*
    Characters 1-5 of the long-name sub-component in this dir entry.
    */
    uint16_t LDIR_Name1[5];

    /*
    Attributes - must be ATTR_LONG_NAME
    */
    uint8_t LDIR_Attr;

    /*
    If zero, indicates a directory entry that is a sub-component of a
    long name. NOTE: Other values reserved for future extensions.

    Non-zero implies other dirent types.
    */
    uint8_t LDIR_Type;

    /*
    Checksum of name in the short dir entry at the end of the long
    dir set.
    */
    uint8_t LDIR_Chksum;

    /*
    Characters 6-11 of the long-name sub-component in this dir
    entry.
    */
    uint16_t LDIR_Name2[6];

    /*
    Must be ZERO. This is an artifact of the FAT "first cluster" and
    must be zero for compatibility with existing disk utilities. It's
    meaningless in the context of a long dir entry.
    */
    uint16_t LDIR_FstClusLO;

    /*
    Characters 12-13 of the long-name sub-component in this dir
    entry.
    */
    uint16_t LDIR_Name3[2];
} __attribute__((packed));

#define ATTR_NONE       0x00
#define ATTR_READ_ONLY  0x01
#define ATTR_HIDDEN     0x02
#define ATTR_SYSTEM     0x04
#define ATTR_VOLUME_ID  0x08
#define ATTR_DIRECTORY  0x10
#define ATTR_ARCHIVE    0x20
#define ATTR_LONG_NAME  (ATTR_READ_ONLY | ATTR_HIDDEN | ATTR_SYSTEM | ATTR_VOLUME_ID)

#define ATTR_LONG_NAME_MASK (ATTR_READ_ONLY | ATTR_HIDDEN | ATTR_SYSTEM | ATTR_VOLUME_ID | ATTR_DIRECTORY | ATTR_ARCHIVE)

enum FAT_Type {
    FAT12,
    FAT16,
    FAT32
};

#define FAT_FREE_CLUSTER_ENTRY 0x0
#define FAT32_FAT_MASK MASK_T(uint32_t, 28)
#define fat_read_cluster_from_fat_entry(fat_entry) ((fat_entry) & FAT32_FAT_MASK)
#define fat_write_cluster_to_fat_entry(fat_entry, cluster) (((fat_entry) & ~FAT32_FAT_MASK) | ((cluster) & FAT32_FAT_MASK))

#define FAT16_EOF 0x0000FFF8
#define FAT32_EOF 0x0FFFFFF8
// cluster zero is set for empty files in their directory entry
#define fat_is_eof_fat16(cluster) (((cluster) >= FAT16_EOF) | ((cluster) == 0))
#define fat_is_eof_fat32(cluster) (((cluster) >= FAT32_EOF) | ((cluster) == 0))

#define FAT16_BAD_CLUSTER 0x0000FFF7
#define FAT32_BAD_CLUSTER 0xFFFFFFF7
#define fat_is_bad_cluster_fat16(cluster) ((cluster) == FAT16_BAD_CLUSTER)
#define fat_is_bad_cluster_fat32(cluster) ((cluster) == FAT32_BAD_ClUSTER)

#define fat_first_cluster_sector(mount, cluster) ((((cluster) - 2) * (mount)->cluster_size) + (mount)->data_begin_sector)
#define fat_cluster_from_dir_entry(dir_entry) (((dir_entry)->DIR_FstClusHI << 16) | (dir_entry)->DIR_FstClusLO)
#define fat_cluster_hi_from_cluster(cluster) ((uint16_t)((cluster) >> 16))
#define fat_cluster_lo_from_cluster(cluster) ((uint16_t)(cluster))

#define fat_sector_from_cluster_fat32(mount, cluster) ((mount)->fat_begin_sector + (((cluster) << 2) / (mount)->sector_size))
#define fat_sector_from_cluster_fat16(mount, cluster) ((mount)->fat_begin_sector + (((cluster) << 1) / (mount)->sector_size))
#define fat_offset_from_cluster_fat32(mount, cluster) ((((cluster) << 2) % (mount)->sector_size) >> 2)
#define fat_offset_from_cluster_fat16(mount, cluster) ((((cluster) << 1) % (mount)->sector_size) >> 1)

#define fat_is_dir(dir_entry) ((dir_entry)->DIR_Attr & ATTR_DIRECTORY)

#define FAT_EMPTY_DIR_LIST_FLAG 0x00
#define FAT_EMPTY_DIR_ENTRY_FLAG 0xE5

#define fat_is_empty_dir_entry(dir_entry) ((dir_entry)->DIR_Name[0] == FAT_EMPTY_DIR_LIST_FLAG || (dir_entry)->DIR_Name[0] == FAT_EMPTY_DIR_ENTRY_FLAG)

#define FAT_SHORT_NAME_NAME_LENGTH (sizeof(((struct FAT_DirEntry*)0)->DIR_Name))
#define FAT_SHORT_NAME_EXTENSION_LENGTH (sizeof(((struct FAT_DirEntry*)0)->DIR_NameExt))
#define FAT_SHORT_NAME_LENGTH (FAT_SHORT_NAME_NAME_LENGTH + FAT_SHORT_NAME_EXTENSION_LENGTH)

#define FAT_MAX_PATH_LENGTH 260
#define FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH 13
#define FAT_LONG_DIR_ENTRY_PADDING 0xFFFF
#define FAT_LONG_DIR_ENTRY_ORDER_MASK MASK_T(uint8_t, 4)
#define fat_long_dir_entry_mask_order(order) ((order) & FAT_LONG_DIR_ENTRY_ORDER_MASK)
#define fat_long_dir_num_fragments(len) (((len) + FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH - 1) / FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH)

#define FAT_MAX_DIR_ENTRIES 65536
#define FAT_MAX_FILE_SIZE ((uint64_t) 0xFFFFFFFF)

static inline uint8_t fat_short_name_checksum(const uint8_t *short_name) {
    uint8_t checksum = 0;
    for (uint8_t i = 0; i < 11; i++) {
        checksum = ((checksum & 1) ? 0x80 : 0) + (checksum >> 1) + *short_name++;
    }
    return checksum;
}

static const char fat_illegal_long_name_chars[] = {
          0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
                0x22,
                0x2A,                         0x2F,
                0x3A,       0x3C,       0x3E, 0x3F,
                            0x5C,
                            0x7C,             0x7F,
    0x00
};

static const char fat_illegal_short_name_chars[] = {
    0x2B, 0x2C, 0x3B, 0x3D, 0x5B, 0x5D, 0x00
};

//#define ENABLE_FAT_DEBUGGING

#ifdef ENABLE_FAT_DEBUGGING
#define FAT_DEBUG(msg...) debug_printf(msg)
#define FAT_DEBUG_ERR(args...) DEBUG_ERR(args)
#else
#define FAT_DEBUG(msg...)
#define FAT_DEBUG_ERR(args...)
#endif

struct fat_sector_iter_state {
    uint32_t prev_cluster;
    uint32_t cluster;
    uint8_t offset;
};

struct fat_dir_iter_state {
    uint16_t entry;
    bool eof;
};

struct fat_dir_entry {
    char long_name[260];
    struct FAT_DirEntry fat_dir_entry;
    struct fat_sector_iter_state sector_iter_state;
    struct fat_dir_iter_state dir_iter_state;
    struct fat_sector_iter_state long_name_sector_iter_state;
    struct fat_dir_iter_state long_name_dir_iter_state;
};

struct fat_sector_iter {
    struct fat_mount *mount;
    const struct fat_dir_entry *dir_entry;
    struct fat_sector_iter_state state;
};

struct fat_dir_iter {
    struct fat_sector_iter sector_iter;
    struct fat_dir_iter_state state;
};

#define FAT_SECTOR_CACHE_SIZE 16384

struct fat_cached_sector {
    struct fat_cached_sector *q_prev;
    struct fat_cached_sector *q_next;
    struct fat_cached_sector *h_prev;
    struct fat_cached_sector *h_next;
    void *data;
    uint32_t number;
};

struct fat_mount;

struct fat_sector_cache {
    struct fat_mount *mount;
    struct fat_cached_sector *head;
    struct fat_cached_sector *tail;
    struct fat_cached_sector *sectors;
    struct fat_cached_sector **hash;
    uint32_t size;
};

struct fat_mount {
    uint32_t vol_offset;            //< volume offset in number of sectors on disk before the boot sector of this volume
    enum FAT_Type type;             //< the filesystem type FAT12, FAT16 or FAT32
    uint32_t fat_size;              //< the size of one file allocation table
    uint32_t num_fats;              //< the number of file allocation tables (can be larger than one for backups)
    uint32_t active_fat;            //< index of the active file allocation table in case mirroring is disabled
    bool fat_mirroring;             //< flag indicating whether file allocation table mirroring is enabled
    uint32_t data_begin_sector;     //< sector index relative to the volume offset of the first sector in the file and directory data region
    uint32_t fat_begin_sector;      //< sector index relative to the volume offset of the first block in the file allocation table region
                                    //  (this is equivalent to the number of sectors in the reserved region)
    uint32_t num_root_dir_sectors;  //< number of sectors in the root directory table (only relevant for FAT12/FAT16)
    uint32_t cluster_size;          //< number of sectors per cluster
    uint32_t root_cluster;          //< cluster index of the root directory table (only relevant for FAT32)
    uint32_t sector_size;           //< number of bytes per sector
    uint32_t sector_device_blocks;  //< number of devices blocks per sector
    uint32_t num_sectors;           //< total number of sectors in this volume
    uint32_t max_cluster_index;     //< the maximum cluster index on this volume
    uint32_t device_block_size;     //< the size in bytes of a block of the underlying device
    struct fat_sector_cache cache;  //< last recently used cache for sectors
};

struct fat_common_handle {
    bool is_dir;
    struct fat_dir_entry dir_entry;
};

struct fat_file_handle {
    struct fat_common_handle common;
    struct fat_sector_iter_state iter_state;
    bool iter_state_valid;
    uint32_t data_offset;
};

struct fat_dir_handle {
    struct fat_common_handle common;
    struct fat_dir_iter dir_iter;
};

#endif // LIB_FS_INCLUDE_FAT_FATFS_P_H_
