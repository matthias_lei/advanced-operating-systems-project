/**
 * Functions supporting the debugging of the FAT filesystem
 * implementation.
 *
 * Group C
 */
#ifndef LIB_FS_INCLUDE_FAT_FATFS_DEBUG_H_
#define LIB_FS_INCLUDE_FAT_FATFS_DEBUG_H_

#include <fat/fatfs_p.h>

void dump_BPB(const struct FAT_BPB *bpb);

void dump_BPB_FAT16(const struct FAT_BPB_FAT16 *bpb_fat16);

void dump_BPB_FAT32(const struct FAT_BPB_FAT32 *bpb_fat32);

void dump_FAT(const struct FAT *fat);

void dump_FAT_BootSector(const struct FAT_BootSector *boot_sector);

void dump_FAT_DirEntry(const struct FAT_DirEntry *dir_entry);

errval_t fat_debug_print_dirs_recursive(struct fat_mount *mount, const char *parent);

#endif // LIB_FS_INCLUDE_FAT_FATFS_DEBUG_H_
