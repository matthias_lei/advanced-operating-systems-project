#include <fs/vfs.h>

struct vfs_mount_vtable {
    errval_t (*lookup)(void *st, const char *path);
    errval_t (*open)(void *st, const char *path, vfs_handle_t *rethandle);
    errval_t (*create)(void *st, const char *path, vfs_handle_t *rethandle);
    errval_t (*remove)(void *st, const char *path);
    errval_t (*read)(void *st, vfs_handle_t handle, void *buffer, size_t bytes,
                        size_t *bytes_read);
    errval_t (*write)(void *st, vfs_handle_t handle, const void *buffer,
                        size_t bytes, size_t *bytes_written);
    errval_t (*truncate)(void *st, vfs_handle_t handle, size_t bytes);
    errval_t (*tell)(void *st, vfs_handle_t handle, size_t *pos);
    errval_t (*stat)(void *st, vfs_handle_t inhandle, struct fs_fileinfo *info);
    errval_t (*seek)(void *st, vfs_handle_t handle, enum fs_seekpos whence,
                        off_t offset);
    errval_t (*close)(void *st, vfs_handle_t inhandle);
    errval_t (*opendir)(void *st, const char *path, vfs_handle_t *rethandle);
    errval_t (*dir_read_next)(void *st, vfs_handle_t inhandle, char **retname,
                                struct fs_fileinfo *info);
    errval_t (*closedir)(void *st, vfs_handle_t dhandle);
    errval_t (*mkdir)(void *st, const char *path);
    errval_t (*rmdir)(void *st, const char *path);
    errval_t (*mount)(const char *uri, void **retst);
};

struct vfs_mount {
    struct vfs_mount *prev;
    struct vfs_mount *next;
    char *dev;
    char *path;
    struct vfs_mount_vtable *ops;
    void *fs_mount;
};

struct vfs_handle {
    struct vfs_mount *mount;
    struct vfs_handle *next;
    struct vfs_handle *prev;
    char *path;
    void *mount_handle;
};

//#define ENABLE_VFS_DEBUGGING

#ifdef ENABLE_VFS_DEBUGGING
#define VFS_DEBUG(msg...) debug_printf(msg)
#define VFS_DEBUG_ERR(args...) DEBUG_ERR(args)
#else
#define VFS_DEBUG(msg...)
#define VFS_DEBUG_ERR(args...)
#endif
