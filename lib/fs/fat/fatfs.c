#include <fat/fatfs_p.h>

#include <fat/fatfs_debug.h>

#include <aos/aos_rpc.h>
#include <ctype.h>

// ========== forward declarations ========== //

static errval_t fat_get_free_cluster(struct fat_mount *mount, uint32_t *cluster);

// ========== block and sector read/write functions ========== //

static errval_t fat_read_block(const struct fat_mount *mount, uint32_t block, void *buffer) {
    assert(mount);
    assert(buffer);

    // bounds check
    if (mount->device_block_size * block > mount->num_sectors * mount->sector_size) {
        FAT_DEBUG("The requested block index exceeds the maximum valid block index.\n");
        return FAT_ERR_BLOCK_BOUNDS;
    }
    errval_t err = aos_rpc_blockdev_read_block(aos_rpc_get_blockdev_channel(), mount->vol_offset + block, buffer, mount->device_block_size);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "aos_rpc_blockdev_read_block failed for block number 0x%x\n", block);
        return err;
    }
    return SYS_ERR_OK;
}

static errval_t fat_write_block(const struct fat_mount *mount, uint32_t block, const void *buffer) {
    assert(mount);
    assert(buffer);

    // bounds check
    if (mount->device_block_size * block > mount->num_sectors * mount->sector_size) {
        FAT_DEBUG("The requested block index exceeds the maximum valid block index.\n");
        return FAT_ERR_BLOCK_BOUNDS;
    }
    errval_t err = aos_rpc_blockdev_write_block(aos_rpc_get_blockdev_channel(), mount->vol_offset + block, buffer, mount->device_block_size);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "aos_rpc_blockdev_write_block failed for block number 0x%x\n", block);
        return err;
    }
    return SYS_ERR_OK;
}

static errval_t fat_read_sector(const struct fat_mount *mount, uint32_t sector, void *buffer) {
    assert(mount);
    assert(buffer);

    uint32_t sector_begin_block = sector*mount->sector_device_blocks;
    for (uint16_t i = 0; i < mount->sector_device_blocks; i++) {
        errval_t err = fat_read_block(mount, sector_begin_block + i, ((char*)buffer) + i*mount->device_block_size);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_read_block failed.\n");
            return err;
        }
    }
    return SYS_ERR_OK;
}

static errval_t fat_write_sector(const struct fat_mount *mount, uint32_t sector, const void *buffer) {
    assert(mount);
    assert(buffer);

    uint32_t sector_begin_block = sector*mount->sector_device_blocks;
    for (uint16_t i = 0; i < mount->sector_device_blocks; i++) {
        errval_t err = fat_write_block(mount, sector_begin_block + i, ((char*)buffer) + i*mount->device_block_size);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_write_block failed.\n");
            return err;
        }
    }
    return SYS_ERR_OK;
}

// ========== sector cache implementation ========== //

static errval_t fat_sector_cache_init(struct fat_sector_cache *cache, struct fat_mount *mount, uint32_t size) {
    cache->size = size;
    cache->mount = mount;
    cache->sectors = calloc(size, sizeof(struct fat_cached_sector));
    if (!cache->sectors) {
        return LIB_ERR_MALLOC_FAIL;
    }
    cache->hash = calloc(size, sizeof(struct fat_cached_sector *));
    if (!cache->hash) {
        free(cache->sectors);
        return LIB_ERR_MALLOC_FAIL;
    }
    for (uint32_t i = 0; i < size - 1; i++) {
        cache->sectors[i].q_next = &cache->sectors[i+1];
        cache->sectors[i+1].q_prev = &cache->sectors[i];
        cache->sectors[i].h_next = &cache->sectors[i+1];
        cache->sectors[i+1].h_prev = &cache->sectors[i];
    }
    cache->head = &cache->sectors[0];
    cache->tail = &cache->sectors[size-1];
    cache->hash[0] = &cache->sectors[0];

    return SYS_ERR_OK;
}

static inline void fat_sector_cache_enqueue(struct fat_sector_cache *cache, struct fat_cached_sector *sector) {
    if (!sector->q_next) {
        // we found the tail, nothing to do
        return;
    }
    // extract sector from the queue
    if (!sector->q_prev) {
        // we found the head
        cache->head = sector->q_next;
        cache->head->q_prev = NULL;
    }
    else {
        // we are in the middle
        sector->q_prev->q_next = sector->q_next;
        sector->q_next->q_prev = sector->q_prev;
    }
    // insert sector as the tail again
    sector->q_next = NULL;
    sector->q_prev = cache->tail;
    sector->q_prev->q_next = sector;
    cache->tail = sector;
    return;
}

static struct fat_cached_sector* fat_sector_cache_recycle(struct fat_sector_cache *cache, uint32_t sector_nr) {
    struct fat_cached_sector *sector = cache->head;
    uint32_t new_hash = sector_nr % cache->size;
    uint32_t old_hash = sector->number % cache->size;
    if (new_hash != old_hash) {
        // we need to put the sector in another hash bucket
        if (!sector->h_prev) {
            // we found the head of the hash list
            cache->hash[old_hash] = sector->h_next;
        }
        if (sector->h_next) {
            sector->h_next->h_prev = sector->h_prev;
        }
        if (sector->h_prev) {
            sector->h_prev->h_next = sector->h_next;
        }

        sector->h_prev = NULL;
        sector->h_next = cache->hash[new_hash];
        cache->hash[new_hash] = sector;
        if (sector->h_next) {
            sector->h_next->h_prev = sector;
        }
    }
    if (!sector->data) {
        sector->data = malloc(cache->mount->sector_size);
        if (!sector->data) {
            return NULL;
        }
    }
    sector->number = sector_nr;
    return sector;
}

static struct fat_cached_sector* fat_sector_cache_lookup(struct fat_sector_cache *cache, uint32_t sector_nr) {
    uint32_t sector_hash = sector_nr % cache->size;
    struct fat_cached_sector *sector = cache->hash[sector_hash];
    while (sector) {
        if (sector->number == sector_nr && sector->data) {
            return sector;
        }
        sector = sector->h_next;
    }
    return NULL;
}

static errval_t fat_sector_cache_read(struct fat_sector_cache *cache, uint32_t sector_nr, void **data) {
    *data = NULL;
    struct fat_cached_sector *sector = fat_sector_cache_lookup(cache, sector_nr);
    if (!sector) {
        sector = fat_sector_cache_recycle(cache, sector_nr);
        if (!sector) {
            return LIB_ERR_MALLOC_FAIL;
        }
        errval_t err = fat_read_sector(cache->mount, sector_nr, sector->data);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_read_sector failed.\n");
            return err;
        }
    }
    fat_sector_cache_enqueue(cache, sector);
    *data = sector->data;
    return SYS_ERR_OK;
}

static errval_t fat_sector_cache_write(struct fat_sector_cache *cache, uint32_t sector_nr, const void *data) {
    struct fat_cached_sector *sector = fat_sector_cache_lookup(cache, sector_nr);
    if (sector) {
        // sector is in cache, so update if necessary
        if (sector->data != data) {
            memcpy(sector->data, data, cache->mount->sector_size);
        }
    }
    errval_t err = fat_write_sector(cache->mount, sector_nr, data);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_write_sector failed.\n");
        return err;
    }
    return SYS_ERR_OK;
}

// ========== sector iterator implementation ========== //

static void fat_sector_iter_init(struct fat_sector_iter *sector_iter, struct fat_mount *mount,
                                 const struct fat_dir_entry *dir_entry) {
    assert(sector_iter);
    assert(mount);
    // dir_entry can be NULL

    sector_iter->mount = mount;
    sector_iter->dir_entry = dir_entry;
    // zero is an invalid cluster number, so we can safely use it for initialization
    sector_iter->state.cluster = dir_entry ? fat_cluster_from_dir_entry(&dir_entry->fat_dir_entry) : 0;
    sector_iter->state.prev_cluster = 0;
    sector_iter->state.offset = 0;
}

static void fat_sector_iter_init_with_state(struct fat_sector_iter *sector_iter, struct fat_mount *mount,
                                            const struct fat_dir_entry *dir_entry, const struct fat_sector_iter_state *state) {
    assert(sector_iter);
    assert(mount);
    // dir_entry can be NULL
    assert(state);

    fat_sector_iter_init(sector_iter, mount, dir_entry);

    sector_iter->state = *state;
}

static errval_t fat_sector_iter_next(struct fat_sector_iter *sector_iter) {
    assert(sector_iter);

    sector_iter->state.offset++;
    if (sector_iter->state.offset >= sector_iter->mount->cluster_size) {
        assert(sector_iter->state.offset == sector_iter->mount->cluster_size);

        sector_iter->state.offset = 0;

        uint32_t fat_sector = fat_sector_from_cluster_fat32(sector_iter->mount, sector_iter->state.cluster);
        uint32_t fat_entry_offset = fat_offset_from_cluster_fat32(sector_iter->mount, sector_iter->state.cluster);

        uint32_t *fat = NULL;
        errval_t err = fat_sector_cache_read(&sector_iter->mount->cache, fat_sector, (void**)&fat);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
            return err;
        }
        sector_iter->state.prev_cluster = sector_iter->state.cluster;
        sector_iter->state.cluster = fat_read_cluster_from_fat_entry(fat[fat_entry_offset]);
        assert(sector_iter->state.cluster);
    }
    return SYS_ERR_OK;
}

static errval_t fat_sector_iter_init_from_fhandle(struct fat_sector_iter *sector_iter,
                                                  struct fat_mount *mount,
                                                  const struct fat_file_handle *fhandle) {
    assert(sector_iter);

    errval_t err = SYS_ERR_OK;

    if (fhandle->iter_state_valid) {
        fat_sector_iter_init_with_state(sector_iter, mount, &fhandle->common.dir_entry, &fhandle->iter_state);
    }
    else {
        fat_sector_iter_init(sector_iter, mount, &fhandle->common.dir_entry);

        uint32_t data_sector_offset = fhandle->data_offset / mount->sector_size;
        for (uint32_t i = 0; i < data_sector_offset; i++) {
            err = fat_sector_iter_next(sector_iter);
            if (err_is_fail(err)) {
                FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
                return err;
            }
        }
    }

    return err;
}

#define fat_sector_iter_cur_sector(sector_iter) (fat_first_cluster_sector((sector_iter)->mount, (sector_iter)->state.cluster) + (sector_iter)->state.offset)

#define fat_sector_iter_eof(sector_iter) (fat_is_eof_fat32((sector_iter)->state.cluster))

#define fat_sector_iter_state_valid(sector_iter_state) ((sector_iter_state)->cluster)

static errval_t fat_sector_iter_read_sector(struct fat_sector_iter *sector_iter, void **buffer) {
    uint32_t sector = fat_first_cluster_sector(sector_iter->mount, sector_iter->state.cluster) + sector_iter->state.offset;
    errval_t err = fat_sector_cache_read(&sector_iter->mount->cache, sector, buffer);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
        *buffer = NULL;
        return err;
    }
    return SYS_ERR_OK;
}

static errval_t fat_sector_iter_write_sector(struct fat_sector_iter *sector_iter, const void *buffer) {
    uint32_t sector = fat_first_cluster_sector(sector_iter->mount, sector_iter->state.cluster) + sector_iter->state.offset;
    errval_t err = fat_sector_cache_write(&sector_iter->mount->cache, sector, buffer);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_cache_write failed.\n");
        return err;
    }
    return SYS_ERR_OK;
}

static errval_t fat_sector_iter_append_cluster(struct fat_sector_iter *sector_iter) {
    assert(sector_iter);
    // sector iterator must be at the end of the cluster chain
    assert(fat_sector_iter_eof(sector_iter));

    uint32_t cluster = 0;
    errval_t err = fat_get_free_cluster(sector_iter->mount, &cluster);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_get_free_cluster failed.\n");
        return err;
    }
    assert(cluster);

    uint32_t fat_sector = fat_sector_from_cluster_fat32(sector_iter->mount, cluster);
    uint32_t fat_entry_offset = fat_offset_from_cluster_fat32(sector_iter->mount, cluster);

    uint32_t *fat;
    err = fat_sector_cache_read(&sector_iter->mount->cache, fat_sector, (void**)&fat);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
        return err;
    }
    fat[fat_entry_offset] = fat_write_cluster_to_fat_entry(fat[fat_entry_offset], FAT32_EOF);

    err = fat_sector_cache_write(&sector_iter->mount->cache, fat_sector, fat);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_write_block failed.\n");
        return err;
    }

    if (sector_iter->state.prev_cluster) {
        // we have a previous cluster, so we must update the file allocation table
        fat_sector = fat_sector_from_cluster_fat32(sector_iter->mount, sector_iter->state.prev_cluster);
        fat_entry_offset = fat_offset_from_cluster_fat32(sector_iter->mount, sector_iter->state.prev_cluster);

        err = fat_sector_cache_read(&sector_iter->mount->cache, fat_sector, (void**)&fat);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
            return err;
        }

        fat[fat_entry_offset] = fat_write_cluster_to_fat_entry(fat[fat_entry_offset], cluster);

        err = fat_sector_cache_write(&sector_iter->mount->cache, fat_sector, fat);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_cache_write failed.\n");
            return err;
        }
    }
    else {
        // we have no previous cluster, so we must update the directory entry
        assert(sector_iter->dir_entry);

        struct fat_sector_iter dir_sector_iter;
        fat_sector_iter_init_with_state(&dir_sector_iter, sector_iter->mount, NULL, &sector_iter->dir_entry->sector_iter_state);
        struct FAT_DirEntry *entry_table;
        err = fat_sector_iter_read_sector(&dir_sector_iter, (void**)&entry_table);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
            return err;
        }
        struct FAT_DirEntry *dir_entry = &entry_table[sector_iter->dir_entry->dir_iter_state.entry];
        dir_entry->DIR_FstClusHI = fat_cluster_hi_from_cluster(cluster);
        dir_entry->DIR_FstClusLO = fat_cluster_lo_from_cluster(cluster);

        err = fat_sector_iter_write_sector(&dir_sector_iter, entry_table);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
            return err;
        }
    }

    sector_iter->state.cluster = cluster;
    sector_iter->state.offset = 0;

    struct fat_sector_iter cluster_sector_iter;
    fat_sector_iter_init_with_state(&cluster_sector_iter, sector_iter->mount, NULL, &sector_iter->state);

    char sector_buffer[sector_iter->mount->sector_size];
    memset(sector_buffer, 0, sector_iter->mount->sector_size);
    for (uint16_t i = 0; i < sector_iter->mount->cluster_size; i++) {
        assert(!fat_sector_iter_eof(&cluster_sector_iter));
        err = fat_sector_iter_write_sector(&cluster_sector_iter, sector_buffer);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter write_sector failed.\n");
            return err;
        }
        err = fat_sector_iter_next(&cluster_sector_iter);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
            return err;
        }
    }
    assert(fat_sector_iter_eof(&cluster_sector_iter));

    return SYS_ERR_OK;
}

// ========== directory iterator implementation ========== //

static struct fat_dir_entry empty_dir_entry;

static void fat_dir_iter_init(struct fat_dir_iter *dir_iter, struct fat_mount *mount,
                              const struct fat_dir_entry *dir_entry) {
    assert(dir_iter);
    assert(mount);

    fat_sector_iter_init(&dir_iter->sector_iter, mount, dir_entry);
    dir_iter->state.entry = 0;
    dir_iter->state.eof = false;
}

#define fat_dir_iter_is_eof(dir_iter) ((dir_iter)->eof)

static errval_t fat_dir_iter_next(struct fat_dir_iter *dir_iter, struct fat_dir_entry *dir_entry) {
    assert(dir_iter);
    assert(dir_entry);

    *dir_entry = empty_dir_entry;

    if (dir_iter->state.eof) {
        return FS_ERR_INDEX_BOUNDS;
    }

    const struct fat_mount* mount = dir_iter->sector_iter.mount;

    bool found_next_entry = false;
    // next_long_name stores the expected order of the next
    // long directory entry, while reading a long name
    // if next_long_name is set to 0, we are currently
    // not reading a long name and can start reading at the
    // next entry with ATTR_LONG_NAME set
    uint8_t next_long_name = 0;
    // stores the checksum while reading a long name for
    // verification
    uint8_t long_name_checksum = 0;
    do {
        errval_t err;
        // read the next sector if necessary
        if (dir_iter->state.entry*sizeof(struct FAT_DirEntry) >= mount->sector_size) {
            assert(dir_iter->state.entry*sizeof(struct FAT_DirEntry) == mount->sector_size);
            dir_iter->state.entry = 0;
            err = fat_sector_iter_next(&dir_iter->sector_iter);
            if (err_is_fail(err)) {
                FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
                return err;
            }
        }
        // the last entry in the directory table is not necessarily
        // an all free entry (DIR_Name[0] == 0x00), so we need to
        // check for the EOF marker in the directory table chain, in
        // case the last directory table entry is the last entry
        // in the previous cluster
        if (fat_sector_iter_eof(&dir_iter->sector_iter)) {
            dir_iter->state.eof = true;
            return FS_ERR_INDEX_BOUNDS;
        }

        struct FAT_DirEntry *dir_list;
        err = fat_sector_iter_read_sector(&dir_iter->sector_iter, (void**)&dir_list);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_cur_sector failed.\n");
            return err;
        }

        // read the entry and advance the iterator
        struct FAT_DirEntry *cur_dir_entry = &dir_list[dir_iter->state.entry++];
        if (cur_dir_entry->DIR_Name[0] == 0x00) {
            // all entries after this will be free
            dir_iter->state.eof = true;
            return FS_ERR_INDEX_BOUNDS;
        }
        else if (cur_dir_entry->DIR_Name[0] == 0xE5) {
            // this is a free entry, do nothing
            continue;
        }
        else if ((cur_dir_entry->DIR_Attr & ATTR_LONG_NAME_MASK) == ATTR_LONG_NAME) {
            struct FAT_LongDirEntry *long_dir_entry = (void*) cur_dir_entry;
            uint8_t current_order = 0xFF;
            if (next_long_name < 20) {
                // previously read long name entries might have been orphans
                // and this is the start of a real long name
                if (long_dir_entry->LDIR_Ord & LAST_LONG_ENTRY) {
                    next_long_name = 0;
                    // fall-through
                }
                else {
                    // try to continue reading a long name
                    current_order = fat_long_dir_entry_mask_order(long_dir_entry->LDIR_Ord);
                    if (current_order != next_long_name) {
                        // we are reading orphaned entries
                        next_long_name = 0;
                        continue;
                    }
                    if (long_dir_entry->LDIR_Chksum != long_name_checksum) {
                        // checksum does not match up, we must be reading orphaned entries
                        next_long_name = 0;
                        continue;
                    }
                }

            }
            if (next_long_name == 0) {
                dir_entry->long_name_sector_iter_state = dir_iter->sector_iter.state;
                dir_entry->long_name_dir_iter_state = dir_iter->state;
                // we need to subtract one here, since it was already increased above
                dir_entry->long_name_dir_iter_state.entry--;
                // try starting to read a new long name
                if (!(long_dir_entry->LDIR_Ord & LAST_LONG_ENTRY)) {
                    // we encountered an orphan, just ignore
                    continue;
                }
                current_order = fat_long_dir_entry_mask_order(long_dir_entry->LDIR_Ord);
                if (current_order > 20) {
                    // something is wrong, the maximally allowed order of
                    // a long name entry is 20, treat as an orphan
                    continue;
                }
                if (long_dir_entry->LDIR_Name3[1] != FAT_LONG_DIR_ENTRY_PADDING) {
                    // special case for long names that are stored without
                    // terminating character and padding, add termination
                    // manually
                    dir_entry->long_name[(current_order + 1) * FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH] = '\0';
                }
                long_name_checksum = long_dir_entry->LDIR_Chksum;
            }
            assert(current_order != 0xFF);

            // note: for the last entry with current_order = 1, this automatically resets
            // next_long_name to 0
            next_long_name = current_order - 1;
            // current_order is a one-based index, therefore we can use next_long_index to
            // compute the buffer offset
            char *name_buffer = dir_entry->long_name + next_long_name * FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH;

            // For simplicity we only support the Basic Latin (C0) unicode block of UCS-2
            // in the long name format, which is better known as the ASCII table containing
            // 128 code points. All characters outside this unicode block are converted to
            // the asterisk symbol (0x2A). This also greatly simplifies handling the case-
            // insensitivity of FAT, since there is a one-to-one mapping of the upper case
            // character to its lower case counterpart within this unicode block.
            for (uint8_t i = 0; i < sizeof(long_dir_entry->LDIR_Name1)/sizeof(long_dir_entry->LDIR_Name1[0]); i++) {
                name_buffer[i] = (long_dir_entry->LDIR_Name1[i] < 128) ? (char)long_dir_entry->LDIR_Name1[i] : '*';
            }
            name_buffer += sizeof(long_dir_entry->LDIR_Name1)/sizeof(long_dir_entry->LDIR_Name1[0]);
            for (uint8_t i = 0; i < sizeof(long_dir_entry->LDIR_Name2)/sizeof(long_dir_entry->LDIR_Name2[0]); i++) {
                name_buffer[i] = long_dir_entry->LDIR_Name2[i] < 128 ? (char)long_dir_entry->LDIR_Name2[i] : '*';
            }
            name_buffer += sizeof(long_dir_entry->LDIR_Name2)/sizeof(long_dir_entry->LDIR_Name2[0]);
            for (uint8_t i = 0; i < sizeof(long_dir_entry->LDIR_Name3)/sizeof(long_dir_entry->LDIR_Name3[0]); i++) {
                name_buffer[i] = long_dir_entry->LDIR_Name3[i] < 128 ? (char)long_dir_entry->LDIR_Name3[i] : '*';
            }
        }
        else if (cur_dir_entry->DIR_Name[0] == '.') {
            // ignore dot and dotdot entries
            continue;
        }
        else {
            dir_entry->fat_dir_entry = *cur_dir_entry;
            dir_entry->sector_iter_state = dir_iter->sector_iter.state;
            dir_entry->dir_iter_state = dir_iter->state;
            // we need to subtract one here, since it was already increased above
            dir_entry->dir_iter_state.entry--;
            if (next_long_name != 0) {
                // we were in the middle of reading a long name, when encountering an entry
                // that does not belong to the long name, so the long name must be orphaned
                next_long_name = 0;
            }
            else if (fat_short_name_checksum((uint8_t*) dir_entry->fat_dir_entry.DIR_Name) != long_name_checksum) {
                // a potentially previously read long name does not belong to this entry
                // and we reset the read long name
                dir_entry->long_name[0] = '\0';
            }
            found_next_entry = true;
        }
    } while (!found_next_entry);
    return SYS_ERR_OK;
}

// ========== FAT helper functions ========== //

static errval_t fat_get_free_cluster(struct fat_mount *mount, uint32_t *cluster) {
    assert(mount);
    assert(cluster);

    *cluster = 0;

    uint32_t fat_sector = fat_sector_from_cluster_fat32(mount, 0);

    uint32_t entries_per_sector = mount->sector_size/sizeof(uint32_t);
    uint32_t *fat_buffer = NULL;
    for (uint32_t i = 0; i <= mount->max_cluster_index; i += entries_per_sector) {
        errval_t err = fat_sector_cache_read(&mount->cache, fat_sector, (void**)&fat_buffer);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
            return err;
        }
        uint32_t block_max = MIN(entries_per_sector, mount->max_cluster_index - i);
        for (uint32_t j = 0; j < block_max; j++) {
            if (fat_read_cluster_from_fat_entry(fat_buffer[j]) == FAT_FREE_CLUSTER_ENTRY) {
                // we found an empty cluster
                *cluster = i + j;
                return SYS_ERR_OK;
            }
        }
        fat_sector++;
    }
    return FS_ERR_INDEX_BOUNDS;
}

static errval_t fat_get_free_dir_entries(struct fat_mount *mount, const struct fat_dir_entry *parent_entry,
                                         uint8_t num_entries, struct fat_dir_entry *start_entry) {
    assert(mount);
    assert(parent_entry);
    assert(start_entry);

    // the zero entry is never a valid start entry, since in each directory the zero
    // and one entries are occupied by the dot and dotdot entries
    *start_entry = empty_dir_entry;
    if (!num_entries) {
        return FS_ERR_INDEX_BOUNDS;
    }

    struct fat_sector_iter sector_iter;
    fat_sector_iter_init(&sector_iter, mount, parent_entry);

    errval_t err = SYS_ERR_OK;

    uint8_t free_entries = 0;
    uint32_t sector_entries = 0;
    do {
        struct FAT_DirEntry *entry_table;
        err = fat_sector_iter_read_sector(&sector_iter, (void**)&entry_table);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
            break;
        }

        uint16_t entry = 0;
        while (entry*sizeof(struct FAT_DirEntry) < mount->sector_size && free_entries < num_entries) {
            if (fat_is_empty_dir_entry(&entry_table[entry])) {
                if (free_entries == 0) {
                    // we discovered a new block of free entries
                    start_entry->sector_iter_state = sector_iter.state;
                    start_entry->dir_iter_state.entry = entry;
                    start_entry->long_name_sector_iter_state = sector_iter.state;
                    start_entry->long_name_dir_iter_state.entry = entry;
                }
                free_entries++;
            }
            else {
                *start_entry = empty_dir_entry;
                free_entries = 0;
            }
            entry++;
        }
        sector_entries += entry;
        err = fat_sector_iter_next(&sector_iter);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
            break;
        }
    } while (free_entries < num_entries && !fat_sector_iter_eof(&sector_iter));

    if (err_is_fail(err)) {
        return err;
    }

    if (free_entries >= num_entries) {
        assert(free_entries == num_entries);
        return SYS_ERR_OK;
    }

    while (sector_entries < FAT_MAX_DIR_ENTRIES && free_entries < num_entries) {
        err = fat_sector_iter_append_cluster(&sector_iter);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_append_cluster.\n");
            return err;
        }
        uint32_t new_entries = mount->cluster_size*mount->sector_size/sizeof(struct FAT_DirEntry);
        free_entries += new_entries;
        sector_entries += new_entries;
    }

    if (sector_entries >= FAT_MAX_DIR_ENTRIES) {
        FAT_DEBUG("No free entries found within the allowed bounds.\n");
        return FS_ERR_INDEX_BOUNDS;
    }

    return SYS_ERR_OK;
}

static void fat_fill_long_dir_entry(struct FAT_LongDirEntry *long_dir_entry, const char *long_name, uint8_t order) {
    uint8_t long_name_len = MIN(FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH, strnlen(long_name, FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH) + 1);
    for (uint8_t i = 0; i < sizeof(long_dir_entry->LDIR_Name1)/sizeof(long_dir_entry->LDIR_Name1[0]); i++) {
        long_dir_entry->LDIR_Name1[i] = long_name_len ? long_name_len--, *long_name++ : FAT_LONG_DIR_ENTRY_PADDING;
    }
    for (uint8_t i = 0; i < sizeof(long_dir_entry->LDIR_Name2)/sizeof(long_dir_entry->LDIR_Name2[0]); i++) {
        long_dir_entry->LDIR_Name2[i] = long_name_len ? long_name_len--, *long_name++ : FAT_LONG_DIR_ENTRY_PADDING;
    }
    for (uint8_t i = 0; i < sizeof(long_dir_entry->LDIR_Name3)/sizeof(long_dir_entry->LDIR_Name3[0]); i++) {
        long_dir_entry->LDIR_Name3[i] = long_name_len ? long_name_len--, *long_name++ : FAT_LONG_DIR_ENTRY_PADDING;
    }
    long_dir_entry->LDIR_Ord = order | (long_name_len ? 0 : LAST_LONG_ENTRY);
}

static errval_t fat_write_dir_entries(struct fat_mount *mount, const struct fat_dir_entry *parent_entry,
                                      const struct fat_dir_entry *start_entry, const char *long_name,
                                      const char *short_name, bool is_dir, struct fat_dir_entry *dir_entry) {
    assert(mount);
    assert(parent_entry);
    assert(long_name);
    assert(short_name);
    assert(dir_entry);

    *dir_entry = empty_dir_entry;

    struct fat_sector_iter sector_iter;
    fat_sector_iter_init_with_state(&sector_iter, mount, parent_entry, &start_entry->sector_iter_state);

    errval_t err = SYS_ERR_OK;

    uint16_t entries_per_sector = mount->sector_size / sizeof(struct FAT_DirEntry);
    uint16_t offset = start_entry->dir_iter_state.entry;

    uint16_t long_name_len = strnlen(long_name, FAT_MAX_PATH_LENGTH);
    // rounding up here
    uint8_t entries_to_write = fat_long_dir_num_fragments(long_name_len);
    if (entries_to_write) {
        dir_entry->long_name_sector_iter_state = sector_iter.state;
        dir_entry->long_name_dir_iter_state.entry = offset;
        struct FAT_LongDirEntry *entry_table;
        struct FAT_LongDirEntry long_dir_entry;
        long_dir_entry.LDIR_Attr = ATTR_LONG_NAME;
        long_dir_entry.LDIR_Type = 0;
        long_dir_entry.LDIR_Chksum = fat_short_name_checksum((uint8_t*)short_name);
        long_dir_entry.LDIR_FstClusLO = 0;
        while (entries_to_write) {
            err = fat_sector_iter_read_sector(&sector_iter, (void**)&entry_table);
            if (err_is_fail(err)) {
                FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
                break;
            }
            for (; offset < entries_per_sector && entries_to_write; offset++) {
                fat_fill_long_dir_entry(&long_dir_entry, long_name + (entries_to_write - 1) * FAT_LONG_DIR_ENTRY_FRAGMENT_LENGTH, entries_to_write);
                entry_table[offset] = long_dir_entry;
                entries_to_write--;
            }
            if (offset >= entries_per_sector) {
                assert(offset == entries_per_sector);
                err = fat_sector_iter_write_sector(&sector_iter, entry_table);
                if (err_is_fail(err)) {
                    FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
                    break;
                }
                err = fat_sector_iter_next(&sector_iter);
                if (err_is_fail(err)) {
                    FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
                    break;
                }
                offset = 0;
            }
        }
        if (err_is_fail(err)) {
            return err;
        }
    }

    struct FAT_DirEntry *entry_table;
    err = fat_sector_iter_read_sector(&sector_iter, (void**)&entry_table);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
        return err;
    }

    dir_entry->sector_iter_state = sector_iter.state;
    dir_entry->dir_iter_state.entry = offset;
    strncpy(dir_entry->long_name, long_name, FAT_MAX_PATH_LENGTH);

    struct FAT_DirEntry *fat_dir_entry = &dir_entry->fat_dir_entry;
    // clean the whole entry
    memset(fat_dir_entry, 0, sizeof(struct FAT_DirEntry));
    // copy the short name and set the attribute
    memcpy(fat_dir_entry->DIR_Name, short_name, FAT_SHORT_NAME_LENGTH);
    fat_dir_entry->DIR_Attr = is_dir ? ATTR_DIRECTORY : ATTR_NONE;

    entry_table[offset] = dir_entry->fat_dir_entry;

    err = fat_sector_iter_write_sector(&sector_iter, entry_table);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
        *dir_entry = empty_dir_entry;
        return err;
    }

    return SYS_ERR_OK;
}

static errval_t fat_delete_dir_entry(struct fat_mount *mount, const struct fat_dir_entry *dir_entry) {
    uint32_t cluster = fat_cluster_from_dir_entry(&dir_entry->fat_dir_entry);

    // delete the directory entries
    struct fat_sector_iter sector_iter;
    uint16_t offset;
    if (fat_sector_iter_state_valid(&dir_entry->long_name_sector_iter_state)) {
        // long name must not be empty in this case
        assert(dir_entry->long_name[0]);
        fat_sector_iter_init_with_state(&sector_iter, mount, NULL, &dir_entry->long_name_sector_iter_state);
        offset = dir_entry->long_name_dir_iter_state.entry;
    }
    else {
        fat_sector_iter_init_with_state(&sector_iter, mount, NULL, &dir_entry->sector_iter_state);
        offset = dir_entry->dir_iter_state.entry;
    }

    errval_t err = SYS_ERR_OK;

    uint16_t long_name_len = strnlen(dir_entry->long_name, FAT_MAX_PATH_LENGTH);
    uint8_t entries_to_delete = 1 + fat_long_dir_num_fragments(long_name_len);

    uint16_t entries_per_sector = mount->sector_size / sizeof(struct FAT_DirEntry);

    while (entries_to_delete) {
        struct FAT_DirEntry *entry_table;
        err = fat_sector_iter_read_sector(&sector_iter, (void**)&entry_table);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
            break;
        }

        for (; offset < entries_per_sector && entries_to_delete; offset++) {
            entry_table[offset].DIR_Name[0] = FAT_EMPTY_DIR_ENTRY_FLAG;
            entries_to_delete--;
        }

        err = fat_sector_iter_write_sector(&sector_iter, entry_table);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
            break;
        }

        err = fat_sector_iter_next(&sector_iter);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
            break;
        }

        // reset the sector offset
        offset = 0;
    }

    if (err_is_fail(err)) {
        return err;
    }

    // delete the cluster chain
    while (!fat_is_eof_fat32(cluster)) {
        uint32_t fat_sector = fat_sector_from_cluster_fat32(mount, cluster);
        uint32_t fat_offset = fat_offset_from_cluster_fat32(mount, cluster);

        uint32_t *fat_buffer = NULL;
        err = fat_sector_cache_read(&mount->cache, fat_sector, (void**)&fat_buffer);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
            return err;
        }

        // read the next cluster index
        cluster = fat_read_cluster_from_fat_entry(fat_buffer[fat_offset]);
        // free the current cluster
        fat_buffer[fat_offset] = fat_write_cluster_to_fat_entry(fat_buffer[fat_offset], FAT_FREE_CLUSTER_ENTRY);

        err = fat_sector_cache_write(&mount->cache, fat_sector, fat_buffer);
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_sector_cache_write failed.\n");
            return err;
        }
    }

    return SYS_ERR_OK;
}

static errval_t fat_resolve_rel_path(struct fat_mount *mount, const struct fat_dir_entry *parent,
                                     const char *path, struct fat_dir_entry *dir_entry) {
    assert(mount);
    assert(parent);
    assert(path);
    assert(dir_entry);

    *dir_entry = empty_dir_entry;

    uint16_t path_len = strnlen(path, FAT_MAX_PATH_LENGTH);
    if (path_len >= FAT_MAX_PATH_LENGTH) {
        FAT_DEBUG("Path too long in fat_resolve_rel_path.\n");
        return FAT_ERR_BAD_FILENAME;
    }

    uint16_t path_seg_bpos = 0;
    if (path[path_seg_bpos] == FS_PATH_SEP) {
        path_seg_bpos++;
    }

    if (path[path_seg_bpos] == '\0') {
        // we have an empty path, parent is requested
        *dir_entry = *parent;
        return SYS_ERR_OK;
    }

    uint16_t path_seg_epos;
    for (path_seg_epos = path_seg_bpos; path[path_seg_epos] && path[path_seg_epos] != FS_PATH_SEP; path_seg_epos++);

    // match each path segment consecutively
    struct fat_dir_entry cur_dir_entry = *parent;
    do {
        uint16_t path_seg_len = path_seg_epos - path_seg_bpos;

        uint16_t path_seg_dpos;
        for (path_seg_dpos = path_seg_bpos; path_seg_dpos < path_seg_epos && path[path_seg_dpos] != '.'; path_seg_dpos++);

        uint16_t name_len = path_seg_dpos - path_seg_bpos;
        if (path[path_seg_dpos] == '.') {
            path_seg_dpos++;
        }
        uint16_t ext_len = path_seg_epos - path_seg_dpos;

        struct fat_dir_iter dir_iter;
        fat_dir_iter_init(&dir_iter, mount, &cur_dir_entry);

        errval_t err = SYS_ERR_OK;

        bool match = false;
        while (!match && err_is_ok(err = fat_dir_iter_next(&dir_iter, &cur_dir_entry))) {
            struct FAT_DirEntry *cur_fat_dir_entry = &cur_dir_entry.fat_dir_entry;
            assert(!fat_is_empty_dir_entry(cur_fat_dir_entry));
            assert((cur_fat_dir_entry->DIR_Attr & ATTR_LONG_NAME_MASK) != ATTR_LONG_NAME);
            if (cur_fat_dir_entry->DIR_Attr & ATTR_VOLUME_ID) {
                // ignore the volume ID entry
                continue;
            }
            // compare with short name if applicable
            if (name_len <= FAT_SHORT_NAME_NAME_LENGTH &&
                    ext_len <= FAT_SHORT_NAME_EXTENSION_LENGTH &&
                    !strncasecmp(cur_fat_dir_entry->DIR_Name, path + path_seg_bpos, name_len) &&
                    !strncasecmp(cur_fat_dir_entry->DIR_NameExt, path + path_seg_dpos, ext_len)) {
                // we actually found an entry whose short name prefix matches the current path segment
                match = true;
                // check whether we actually have a full match
                uint16_t len = name_len;
                while (match && len < FAT_SHORT_NAME_NAME_LENGTH) {
                    if (cur_fat_dir_entry->DIR_Name[len] != ' ') {
                        match = false;
                    }
                    len++;
                }
                len = ext_len;
                while (match && len < FAT_SHORT_NAME_EXTENSION_LENGTH) {
                    if (cur_fat_dir_entry->DIR_NameExt[len] != ' ') {
                        match = false;
                    }
                    len++;
                }
            }
            // if we didn't match the short name, try the long name
            if (!match && !strncasecmp(cur_dir_entry.long_name, path + path_seg_bpos, path_seg_len) &&
                     cur_dir_entry.long_name[path_seg_len] == '\0') {
                // we actually found an entry whose long name matches the current path segment
                match = true;
            }
        }
        if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
            err = SYS_ERR_OK;
        }
        if (err_is_fail(err)) {
            FAT_DEBUG_ERR(err, "fat_dir_iter_next failed.\n");
            return err;
        }
        if (!match) {
            // we have no match
            return FS_ERR_NOTFOUND;
        }
        if (!fat_is_dir(&cur_dir_entry.fat_dir_entry) && path[path_seg_epos]) {
            // an intermediate path segment is not a directory
            return FS_ERR_NOTFOUND;
        }
        if (path[path_seg_epos] == FS_PATH_SEP) {
            path_seg_epos++;
        }
        path_seg_bpos = path_seg_epos;
        for (path_seg_epos = path_seg_bpos; path[path_seg_epos] && path[path_seg_epos] != FS_PATH_SEP; path_seg_epos++);
    } while (path[path_seg_bpos] != '\0');
    // if we reach here, we matched every path segment
    *dir_entry = cur_dir_entry;
    assert(!fat_is_empty_dir_entry(&dir_entry->fat_dir_entry));
    return SYS_ERR_OK;
}

static errval_t fat_resolve_path(struct fat_mount *mount, const char *path, struct fat_dir_entry *dir_entry) {
    assert(mount);
    assert(path);
    assert(dir_entry);

    *dir_entry = empty_dir_entry;

    uint16_t path_len = strnlen(path, FAT_MAX_PATH_LENGTH);
    if (path_len >= FAT_MAX_PATH_LENGTH) {
        FAT_DEBUG("Path too long in fat_resolve_path.\n");
        return FAT_ERR_BAD_FILENAME;
    }

    uint16_t path_seg_bpos = 0;
    if (path[path_seg_bpos] == FS_PATH_SEP) {
        path_seg_bpos++;
    }

    struct fat_dir_entry root_dir_entry = empty_dir_entry;
    struct FAT_DirEntry *root_fat_dir_entry = &root_dir_entry.fat_dir_entry;
    root_fat_dir_entry->DIR_Name[0] = FS_PATH_SEP;
    root_fat_dir_entry->DIR_Attr = ATTR_DIRECTORY;
    root_fat_dir_entry->DIR_FstClusHI = fat_cluster_hi_from_cluster(mount->root_cluster);
    root_fat_dir_entry->DIR_FstClusLO = fat_cluster_lo_from_cluster(mount->root_cluster);

    return fat_resolve_rel_path(mount, &root_dir_entry, path, dir_entry);
}

static errval_t fat_compute_short_name(char *long_name, char *short_name, bool *lossy) {
    uint16_t long_name_len = strlen(long_name);
    // strip trailing spaces and dots
    while (long_name[long_name_len - 1] == ' ' || long_name[long_name_len - 1] == '.') {
        long_name[long_name_len - 1] = '\0';
        long_name_len--;
    }

    uint16_t i = 0;
    while (i < long_name_len) {
        if (long_name[i] >= 128) {
            // we do not support the extended ASCII characters
            return FAT_ERR_BAD_FILENAME;
        }
        i++;
    }

    char *illegal_char = strpbrk(long_name, fat_illegal_long_name_chars);
    if (illegal_char) {
        // an illegal character was found in the directory name
        return FAT_ERR_BAD_FILENAME;
    }

    i = 0;
    *lossy = false;
    // strip leading spaces and dots
    while (long_name[i] == ' ' || long_name[i] == '.') {
        i++;
        *lossy = true;
    }

    uint16_t last_dot = long_name_len;
    char *last_dot_p = strrchr(&long_name[i], '.');
    if (last_dot_p) {
        last_dot = last_dot_p - long_name;
    }

    uint8_t j = 0;
    bool ext = false;
    while (j < FAT_SHORT_NAME_LENGTH && i < long_name_len) {
        if (i == last_dot || (j == FAT_SHORT_NAME_NAME_LENGTH && !ext)) {
            // change to extension
            ext = true;
            *lossy |= i != last_dot;
            i = last_dot + 1;
            // pad name with spaces
            while (j < FAT_SHORT_NAME_NAME_LENGTH) {
                short_name[j++] = ' ';
            }
            continue;
        }
        // strip embedded spaces and dots
        if (long_name[i] == ' '|| long_name[i] == '.') {
            i++;
            *lossy = true;
            continue;
        }
        // convert illegal short name characters
        if (strchr(fat_illegal_short_name_chars, long_name[i])) {
            short_name[j++] = '_';
            i++;
            *lossy = true;
            continue;
        }
        // convert character to upper case
        short_name[j] = toupper(long_name[i]);
        *lossy |= short_name[j] != long_name[i];
        i++;
        j++;
    }
    *lossy |= i != long_name_len;
    // pad extension with spaces
    while (j < FAT_SHORT_NAME_LENGTH) {
        short_name[j++] = ' ';
    }
    return SYS_ERR_OK;
}

static errval_t fat_initialize_dir(struct fat_mount *mount, const struct fat_dir_entry *parent_entry, const struct fat_dir_entry *dir_entry) {
    assert(mount);
    assert(dir_entry);

    struct fat_sector_iter sector_iter;
    fat_sector_iter_init(&sector_iter, mount, dir_entry);

    // initializing a directory is only possible on an empty cluster chain
    assert(fat_sector_iter_eof(&sector_iter));

    errval_t err = fat_sector_iter_append_cluster(&sector_iter);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_append_cluster failed.\n");
        return err;
    }

    struct FAT_DirEntry entry_table[mount->sector_size / sizeof(struct FAT_DirEntry)];
    memset(entry_table, 0, mount->sector_size);

    struct FAT_DirEntry *dot_entry = &entry_table[0];
    memset(dot_entry->DIR_Name, ' ', FAT_SHORT_NAME_LENGTH);
    dot_entry->DIR_Name[0] = '.';
    dot_entry->DIR_Attr = ATTR_DIRECTORY;
    dot_entry->DIR_FstClusHI = dir_entry->fat_dir_entry.DIR_FstClusHI;
    dot_entry->DIR_FstClusLO = dir_entry->fat_dir_entry.DIR_FstClusLO;

    struct FAT_DirEntry *dot_dot_entry = &entry_table[1];
    memset(dot_dot_entry->DIR_Name, ' ', FAT_SHORT_NAME_LENGTH);

    dot_dot_entry->DIR_Name[0] = '.';
    dot_dot_entry->DIR_Name[1] = '.';
    dot_dot_entry->DIR_Attr = ATTR_DIRECTORY;
    // cluster index must be zero for the root cluster
    if (fat_cluster_from_dir_entry(&parent_entry->fat_dir_entry) != mount->root_cluster) {
        dot_dot_entry->DIR_FstClusHI = parent_entry->fat_dir_entry.DIR_FstClusHI;
        dot_dot_entry->DIR_FstClusLO = parent_entry->fat_dir_entry.DIR_FstClusLO;
    }

    err = fat_sector_iter_write_sector(&sector_iter, entry_table);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

static errval_t fat_create_dir_entry(struct fat_mount *mount, const char *path, bool is_dir,
                                     struct fat_dir_entry *parent_entry, struct fat_dir_entry *dir_entry) {
    *parent_entry = empty_dir_entry;
    *dir_entry = empty_dir_entry;

    if (!path) {
        // invalid path
        return FAT_ERR_BAD_FILENAME;
    }

    uint16_t path_len = strnlen(path, FAT_MAX_PATH_LENGTH);
    if (path_len >= FAT_MAX_PATH_LENGTH) {
        // the path is too long
        return FAT_ERR_BAD_FILENAME;
    }

    char path_copy[FAT_MAX_PATH_LENGTH];
    strncpy(path_copy, path, FAT_MAX_PATH_LENGTH);
    char *parent = path_copy;
    char *long_name = strrchr(parent, FS_PATH_SEP);
    if (long_name) {
        // cut parent directory name
        long_name[0] = '\0';
        // start long name without path separator
        long_name++;
    }
    else {
        // parent directory is root
        parent = "/";
        // long name is the whole path
        long_name = path_copy;
    }

    // check whether the parent directory exists
    errval_t err = fat_resolve_path(mount, parent, parent_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_resolve_path failed.\n");
        return err;
    }

    // check whether the path to create already exists
    err = fat_resolve_rel_path(mount, parent_entry, long_name, dir_entry);
    if (err_is_ok(err)) {
        FAT_DEBUG("File or directory already exists.\n");
        return FS_ERR_EXISTS;
    }
    if (err_no(err) != FS_ERR_NOTFOUND) {
        // an internal error occured
        FAT_DEBUG_ERR(err, "fat_resolve_rel_path failed.\n");
        return err;
    }

    // compute the short name
    char short_name[FAT_SHORT_NAME_LENGTH];
    bool lossy;
    err = fat_compute_short_name(long_name, short_name, &lossy);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_compute_short_name failed.\n");
        return err;
    }
    if (!lossy) {
        // we only need the short name
        long_name[0] = '\0';
    }
    else {
        // number the short name
        char *short_name_ext = short_name + FAT_SHORT_NAME_NAME_LENGTH;
        char numbered_short_name[FAT_SHORT_NAME_LENGTH + 1];
        uint8_t max_short_name_len = FAT_SHORT_NAME_NAME_LENGTH - 2;
        uint8_t short_name_len = FAT_SHORT_NAME_NAME_LENGTH;
        // strip trailing whitespaces
        while (short_name_len > 0 && short_name[short_name_len - 1] == ' ') {
            short_name_len--;
        }
        uint8_t short_name_ext_len = FAT_SHORT_NAME_EXTENSION_LENGTH;
        while (short_name_ext_len > 0 && short_name_ext[short_name_ext_len - 1] == ' ') {
            short_name_ext_len--;
        }
        uint32_t shorten_index = 10;
        uint32_t i = 1;
        for (; i < 999999; i++) {
            if (i == shorten_index) {
                max_short_name_len--;
                shorten_index *= 10;
            }
            uint8_t name_len = MIN(short_name_len, max_short_name_len);
            sprintf(numbered_short_name, "%.*s~%" PRIu32 "%.1s%.*s", name_len, short_name, i,
                    short_name_ext[0] != ' ' ? "." : "", short_name_ext_len, short_name_ext);
            err = fat_resolve_rel_path(mount, parent_entry, numbered_short_name, dir_entry);
            if (err_is_ok(err)) {
                // entry already exists
                continue;
            }
            if (err_no(err) != FS_ERR_NOTFOUND) {
                // an internal error occured
                FAT_DEBUG_ERR(err, "fat_resolve_rel_path failed.\n");
                return err;
            }
            // we found an unoccupied short name
            short_name_len = name_len + (FAT_SHORT_NAME_NAME_LENGTH - max_short_name_len);
            break;
        }
        if (i == 1000000) {
            // we should never reach here, since the maximum number
            // of files and directories in a directory is 65'536
            assert(false);
        }
        strncpy(short_name, numbered_short_name, short_name_len);
        memset(short_name + short_name_len, ' ', FAT_SHORT_NAME_NAME_LENGTH - short_name_len);
        strncpy(short_name_ext, numbered_short_name + FAT_SHORT_NAME_NAME_LENGTH + 1, short_name_ext_len);
        memset(short_name_ext + short_name_ext_len, ' ', FAT_SHORT_NAME_EXTENSION_LENGTH - short_name_ext_len);
    }

    uint16_t long_name_len = strnlen(long_name, FAT_MAX_PATH_LENGTH);
    uint8_t num_free_dir_entries = 1 + fat_long_dir_num_fragments(long_name_len);

    struct fat_dir_entry start_entry;
    err = fat_get_free_dir_entries(mount, parent_entry, num_free_dir_entries, &start_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_get_free_dir_entries failed.\n");
        return err;
    }

    err = fat_write_dir_entries(mount, parent_entry, &start_entry, long_name, short_name, is_dir, dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_write_dir_entries failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

static errval_t fat_open_file_handle(struct fat_dir_entry *dir_entry, struct fat_file_handle **fhandle) {
    *fhandle = malloc(sizeof(struct fat_file_handle));
    if (!*fhandle) {
        return LIB_ERR_MALLOC_FAIL;
    }

    (*fhandle)->common.is_dir = false;
    (*fhandle)->common.dir_entry = *dir_entry;
    (*fhandle)->data_offset = 0;
    (*fhandle)->iter_state_valid = false;

    return SYS_ERR_OK;
}

// ========== debugging functions ========== //

__attribute__((unused))
static errval_t fat_debug_print_dirs_recursive_internal(struct fat_mount *mount, const struct fat_dir_entry *parent_entry, const char *parent) {
    assert(mount);
    assert(parent);

    struct fat_dir_iter dir_iter;
    fat_dir_iter_init(&dir_iter, mount, parent_entry);

    uint16_t parent_len = strnlen(parent, FAT_MAX_PATH_LENGTH);
    if (parent_len >= FAT_MAX_PATH_LENGTH) {
        FAT_DEBUG("Parent file name too long in fat_debug_print_dirs_recursive_internal.\n");
        return FAT_ERR_BAD_FILENAME;
    }

    errval_t err = SYS_ERR_OK;

    struct fat_dir_entry dir_entry;
    while (err_is_ok(err = fat_dir_iter_next(&dir_iter, &dir_entry))) {
        struct FAT_DirEntry *fat_dir_entry = &dir_entry.fat_dir_entry;
        assert(!fat_is_empty_dir_entry(fat_dir_entry));
        assert((fat_dir_entry->DIR_Attr & ATTR_LONG_NAME_MASK) != ATTR_LONG_NAME);
        if (fat_dir_entry->DIR_Attr & ATTR_VOLUME_ID) {
            // ignore the volume ID entry
            continue;
        }
        if (fat_dir_entry->DIR_Name[0] == '.') {
            // these are special dot and dotdot entries
            continue;
        }
        char path[FAT_MAX_PATH_LENGTH];
        strcpy(path, parent);
        uint16_t new_name_len = FAT_MAX_PATH_LENGTH;
        if (dir_entry.long_name[0] != '\0') {
            uint16_t long_name_len = strnlen(dir_entry.long_name, FAT_MAX_PATH_LENGTH);
            new_name_len = parent_len + long_name_len;
            if (new_name_len >= FAT_MAX_PATH_LENGTH) {
                FAT_DEBUG("Combined file name too long in fat_debug_print_dirs_recursive_internal.\n");
                err = FAT_ERR_BAD_FILENAME;
                break;
            }
            strcat(path, dir_entry.long_name);
        }
        else {
            // only strip trailing spaces from name and extension,
            // as leading spaces are not allowed
            uint8_t name_len = FAT_SHORT_NAME_NAME_LENGTH;
            while (name_len > 0 && fat_dir_entry->DIR_Name[name_len - 1] == ' ') name_len--;
            uint8_t ext_len = FAT_SHORT_NAME_EXTENSION_LENGTH;
            while (ext_len > 0 && fat_dir_entry->DIR_NameExt[ext_len - 1] == ' ') ext_len--;
            new_name_len = parent_len + name_len + ext_len + (ext_len ? 1 : 0);
            if (new_name_len >= FAT_MAX_PATH_LENGTH) {
                FAT_DEBUG("Combined file name too long in fat_debug_print_dirs_recursive_internal.\n");
                err = FAT_ERR_BAD_FILENAME;
                break;
            }
            strncat(path, fat_dir_entry->DIR_Name, name_len);
            if (ext_len) {
                strcat(path, ".");
                strncat(path, fat_dir_entry->DIR_NameExt, ext_len);
            }
        }
        printf("%s\n", path);
        if (fat_is_dir(fat_dir_entry)) {
            if (new_name_len + 1 >= FAT_MAX_PATH_LENGTH) {
                FAT_DEBUG("Directory name too long in fat_debug_print_dirs_recursive_internal.\n");
                err = FAT_ERR_BAD_FILENAME;
                break;
            }
            strcat(path, "/");
            err = fat_debug_print_dirs_recursive_internal(mount, &dir_entry, path);
            if (err_is_fail(err)) {
                FAT_DEBUG_ERR(err, "fat_debug_print_dirs_recursive_internal failed.\n");
                break;
            }
        }
    }
    if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
        err = SYS_ERR_OK;
    }
    return err;
}

// ========== mount initialization ========== //

static errval_t fat_mount_init(struct fat_mount *mount, uint32_t vol_offset, struct FAT_BootSector *boot_sector) {
    assert(mount);
    assert(boot_sector);

    // check if we deal with a valid BPB
    if (!(boot_sector->BS_Signature == 0xAA55 &&
            ((boot_sector->FAT.FAT_BPB.BS_jmpBoot[0] == 0xEB && boot_sector->FAT.FAT_BPB.BS_jmpBoot[2] == 0x90)
            || boot_sector->FAT.FAT_BPB.BS_jmpBoot[0] == 0xE9))) {
        FAT_DEBUG("Bootsector signature or jmpBoot content do not match for FAT.\n");
        return FAT_ERR_BAD_FS;
    }

    // check the number of bytes per sector
    uint16_t allowed_BPB_BytsPerSec[] = { 512, 1024, 2048, 4096 };
    mount->sector_size = 0;
    for (uint8_t i = 0; i < sizeof(allowed_BPB_BytsPerSec)/sizeof(allowed_BPB_BytsPerSec[0]); i++) {
        if (boot_sector->FAT.FAT_BPB.BPB_BytsPerSec == allowed_BPB_BytsPerSec[i]) {
            mount->sector_size = boot_sector->FAT.FAT_BPB.BPB_BytsPerSec;
            break;
        }
    }
    if (!mount->sector_size) {
        FAT_DEBUG("Encountered invalid value %" PRIu16 " for the number of bytes per sector.\n", boot_sector->FAT.FAT_BPB.BPB_BytsPerSec);
        return FAT_ERR_BAD_FS;
    }

    // check the number of sectors per cluster
    uint8_t allowed_BPB_SecPerClus[] = { 1, 2, 4, 8, 16, 32, 64, 128 };
    mount->cluster_size = 0;
    for (uint8_t i = 0; i < sizeof(allowed_BPB_SecPerClus)/sizeof(allowed_BPB_SecPerClus[0]); i++) {
        if (boot_sector->FAT.FAT_BPB.BPB_SecPerClus == allowed_BPB_SecPerClus[i]) {
            mount->cluster_size = boot_sector->FAT.FAT_BPB.BPB_SecPerClus;
        }
    }
    if (!mount->cluster_size) {
        FAT_DEBUG("Encountered invalid value %" PRIu8 " for the number of sectors per cluster.\n", boot_sector->FAT.FAT_BPB.BPB_SecPerClus);
        return FAT_ERR_BAD_FS;
    }
    if (mount->sector_size * mount->cluster_size > 32*1024) {
        FAT_DEBUG("The number of bytes per cluster is larger than the maximally allowed 32 KiB.\n");
        return FAT_ERR_BAD_FS;
    }

    // check the number of reserved sector
    if (!boot_sector->FAT.FAT_BPB.BPB_RsvdSecCnt) {
        FAT_DEBUG("The number of reserved sections must be larger than zero.\n");
        return FAT_ERR_BAD_FS;
    }
    // note that the number of sectors in the reserved region is equivalent
    // to the index of the first sector in the file allocation table region
    mount->fat_begin_sector = boot_sector->FAT.FAT_BPB.BPB_RsvdSecCnt;

    // check the number of file allocation tables (FATs)
    if (!boot_sector->FAT.FAT_BPB.BPB_NumFATs) {
        FAT_DEBUG("The number of FATs must be larger than zero.\n");
        return FAT_ERR_BAD_FS;
    }
    mount->num_fats = boot_sector->FAT.FAT_BPB.BPB_NumFATs;

    mount->num_sectors = boot_sector->FAT.FAT_BPB.BPB_TotSec16 ?
        boot_sector->FAT.FAT_BPB.BPB_TotSec16 : boot_sector->FAT.FAT_BPB.BPB_TotSec32;

    // check the number of total sectors
    if (!mount->num_sectors) {
        FAT_DEBUG("The number of total sectors must be larger than zero.\n");
        return FAT_ERR_BAD_FS;
    }

    mount->fat_size = boot_sector->FAT.FAT_BPB.BPB_FATSz16 ?
        boot_sector->FAT.FAT_BPB.BPB_FATSz16 : boot_sector->FAT.FAT_BPB_FAT32.BPB_FATSz32;

    // check the size of the FAT
    if (!mount->fat_size) {
        FAT_DEBUG("The size in sectors of a FAT must be larger than zero.\n");
        return FAT_ERR_BAD_FS;
    }

    mount->num_root_dir_sectors = ((boot_sector->FAT.FAT_BPB.BPB_RootEntCnt * 32) + (mount->sector_size - 1)) / mount->sector_size;

    // sanity check the number of total sectors
    mount->data_begin_sector = mount->fat_begin_sector + (mount->num_fats * mount->fat_size) + mount->num_root_dir_sectors;
    if (mount->num_sectors < mount->data_begin_sector) {
        FAT_DEBUG("The total number of sectors does not even include all non-data sectors.\n");
        return FAT_ERR_BAD_FS;
    }

    // determine the FAT type
    uint32_t data_sectors = mount->num_sectors - mount->data_begin_sector;

    uint32_t num_clusters = data_sectors / mount->cluster_size;

    mount->type = FAT32;
    if (num_clusters < 4085) {
        mount->type = FAT12;
    }
    if (num_clusters < 65525) {
        mount->type = FAT16;
    }

    if (mount->type != FAT32) {
        FAT_DEBUG("Only FAT32 is supported.\n");
        return FAT_ERR_BAD_FS;
    }

    // check that bad cluster tag is not an allocatable cluster
    // note that num_clusters + 1 is the maximum valid cluster index for the volume,
    // since the special clusters 0 and 1 are not contained in the num_clusters count
    mount->max_cluster_index = num_clusters + 1;
    if (mount->max_cluster_index >= FAT32_BAD_CLUSTER) {
        FAT_DEBUG("The BAD CLUSTER tag is an allocatable cluster.\n");
        return FAT_ERR_BAD_FS;
    }

    // check the number of root directory entries
    if (mount->type == FAT32) {
        if (boot_sector->FAT.FAT_BPB.BPB_RootEntCnt) {
            FAT_DEBUG("The number of root directory entries must be zero for FAT32.\n");
            return FAT_ERR_BAD_FS;
        }
    }
    else {
        if ((boot_sector->FAT.FAT_BPB.BPB_RootEntCnt * sizeof(struct FAT_DirEntry)) & ((mount->sector_size) << 1)) {
            FAT_DEBUG("The number of root directory entries multiplied with 32 must be an even multiple of the number of bytes per sector.\n");
            return FAT_ERR_BAD_FS;
        }
    }

    // check the 16-bit total sector count
    if (mount->type == FAT32 && boot_sector->FAT.FAT_BPB.BPB_TotSec16) {
        FAT_DEBUG("The old 16-bit total sector count must be zero for FAT32.\n");
        return FAT_ERR_BAD_FS;
    }

    // check 16-bit count of FAT sectors
    if (mount->type == FAT32 && boot_sector->FAT.FAT_BPB.BPB_FATSz16) {
        FAT_DEBUG("The 16-bit count of FAT sectors must be zero for FAT32.\n");
        return FAT_ERR_BAD_FS;
    }

    uint8_t allowed_BPB_Media[] = { 0xF0, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF };
    uint8_t media = 0;
    for (uint8_t i = 0; i < sizeof(allowed_BPB_Media)/sizeof(allowed_BPB_Media[0]); i++) {
        if (boot_sector->FAT.FAT_BPB.BPB_Media == allowed_BPB_Media[i]) {
            media = boot_sector->FAT.FAT_BPB.BPB_Media;
            break;
        }
    }
    if (!media) {
        FAT_DEBUG("Encountered the illegal media type 0x%.2x.\n", boot_sector->FAT.FAT_BPB.BPB_Media);
        return FAT_ERR_BAD_FS;
    }

    // check the boot sector signature
    uint8_t boot_signature;
    if (mount->type == FAT32) {
        boot_signature = boot_sector->FAT.FAT_BPB_FAT32.BPB_FAT16.BS_BootSig;
    }
    else {
        boot_signature = boot_sector->FAT.FAT_BPB_FAT16.BS_BootSig;
    }
    if (boot_signature != 0x29) {
        FAT_DEBUG("Encountered invalid boot sector signature 0x%.2x.\n", boot_signature);
        return FAT_ERR_BAD_FS;
    }

    // sanity check for the filesystem type
    char *BS_FilSysType;
    if (mount->type == FAT32) {
        BS_FilSysType = boot_sector->FAT.FAT_BPB_FAT32.BPB_FAT16.BS_FilSysType;
    }
    else {
        BS_FilSysType = boot_sector->FAT.FAT_BPB_FAT16.BS_FilSysType;
    }
    char *neutral_BS_FilSysType = "FAT     ";
    char registered_BS_FilSysType[3][9];
    strcpy(registered_BS_FilSysType[FAT12], "FAT12   ");
    strcpy(registered_BS_FilSysType[FAT16], "FAT16   ");
    strcpy(registered_BS_FilSysType[FAT32], "FAT32   ");
    bool match_type = false;
    for (uint8_t i = 0; i < sizeof(registered_BS_FilSysType)/sizeof(registered_BS_FilSysType[0]); i++) {
        if (!strncmp(BS_FilSysType, registered_BS_FilSysType[i], sizeof(boot_sector->FAT.FAT_BPB_FAT16.BS_FilSysType))) {
            if (i != mount->type) {
                FAT_DEBUG("Derived filesystem type does not match the stored filesystem type.\n");
                return FAT_ERR_BAD_FS;
            }
            match_type = true;
            break;
        }
    }
    if (!match_type) {
        if (strncmp(BS_FilSysType, neutral_BS_FilSysType, sizeof(boot_sector->FAT.FAT_BPB_FAT16.BS_FilSysType))) {
            FAT_DEBUG("Encountered unknown filesystem type string \"%.*s\".\n", sizeof(boot_sector->FAT.FAT_BPB_FAT16.BS_FilSysType), BS_FilSysType);
            return FAT_ERR_BAD_FS;
        }
    }

    // check FAT mirroring
    mount->fat_mirroring = !FIELD(7, 0, boot_sector->FAT.FAT_BPB_FAT32.BPB_ExtFlags);

    mount->active_fat = FIELD(0, 4, boot_sector->FAT.FAT_BPB_FAT32.BPB_ExtFlags);
    if (!mount->fat_mirroring && mount->active_fat >= mount->num_fats) {
        FAT_DEBUG("The index of the active FAT is out of bounds.\n");
        return FAT_ERR_BAD_FS;
    }

    // TODO jmeier: check FAT consistency if mirroring is enabled an there is more than one FAT

    // check version compatibility
    if (boot_sector->FAT.FAT_BPB_FAT32.BPB_FSVer[1] > 0 || boot_sector->FAT.FAT_BPB_FAT32.BPB_FSVer[0] > 0) {
        FAT_DEBUG("Encountered newer FAT version %" PRIu8 "." PRIu8 " than can be handled by this driver.\n",
            boot_sector->FAT.FAT_BPB_FAT32.BPB_FSVer[1],
            boot_sector->FAT.FAT_BPB_FAT32.BPB_FSVer[0]);
        return FAT_ERR_BAD_FS;
    }

    // check root directory cluster
    if (mount->type == FAT32) {
        mount->root_cluster = boot_sector->FAT.FAT_BPB_FAT32.BPB_RootClus;
        if (mount->root_cluster < 2) {
            FAT_DEBUG("The root directory cluster must have an index larger than 1.\n");
            return FAT_ERR_BAD_FS;
        }
    }

    mount->vol_offset = vol_offset;

    // TODO jmeier: build free list, based on free clusters when the volume is mounted writable

    // TODO jmeier: invalidate the free count and next free field in the FSInfo structure
    //              when the volume is mounted writable, since we won't bother updating them

    // check device block and sector size
    if (mount->device_block_size != (0x1 << log2floor(mount->device_block_size))) {
        FAT_DEBUG("The device block size is not a power of two.\n");
        return FAT_ERR_BAD_FS;
    }

    if (mount->device_block_size > mount->sector_size) {
        FAT_DEBUG("The sector size must be at least as large as the device block size.\n");
        return FAT_ERR_BAD_FS;
    }

    mount->sector_device_blocks = mount->sector_size / mount->device_block_size;

    if (mount->sector_device_blocks*mount->device_block_size != mount->sector_size) {
        FAT_DEBUG("The sector size must be a multiple of the device block size.\n");
        return FAT_ERR_BAD_FS;
    }

    errval_t err = fat_sector_cache_init(&mount->cache, mount, FAT_SECTOR_CACHE_SIZE);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_cache_init failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

// ========== public API for file handling ========== //

errval_t fat_lookup(void *st, const char *path) {
    assert(st);

    struct fat_mount *mount = st;
    struct fat_dir_entry dir_entry;
    errval_t err = fat_resolve_path(mount, path, &dir_entry);
    if (err_is_fail(err)) {
        return err;
    }
    if (!(dir_entry.fat_dir_entry.DIR_Attr & ATTR_DIRECTORY)) {
        return FS_ERR_NOTDIR;
    }
    return SYS_ERR_OK;
}

errval_t fat_open(void *st, const char *path, fat_handle_t *rethandle) {
    assert(st);

    if (!rethandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    *rethandle = NULL;
    if (!path) {
        // invalid path
        return FS_ERR_NOTFOUND;
    }

    struct fat_mount *mount = st;

    struct fat_dir_entry dir_entry;
    errval_t err = fat_resolve_path(mount, path, &dir_entry);
    if (err_no(err) == FS_ERR_NOTFOUND) {
        // we could not find a file system object at the given path
        return err;
    }
    if (err_is_fail(err)) {
        // an internal error occured
        FAT_DEBUG_ERR(err, "fat_resolve_path failed.\n");
        return err;
    }

    if (fat_is_dir(&dir_entry.fat_dir_entry)) {
        // we expected a file
        return FS_ERR_NOTFILE;
    }

    err = fat_open_file_handle(&dir_entry, (struct fat_file_handle**)rethandle);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_open_file_handle failed.\n");
        *rethandle = NULL;
        return err;
    }

    return SYS_ERR_OK;
}

errval_t fat_create(void *st, const char *path, fat_handle_t *rethandle) {
    assert(st);

    if (rethandle) {
        *rethandle = NULL;
    }

    struct fat_mount *mount = st;
    struct fat_dir_entry parent_entry, dir_entry;
    errval_t err = fat_create_dir_entry(mount, path, false, &parent_entry, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_create_dir_entry failed.\n");
        return err;
    }

    struct fat_file_handle *fhandle;
    err = fat_open_file_handle(&dir_entry, &fhandle);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_open_file_handle failed.\n");
        return err;
    }

    if (rethandle) {
        *rethandle = fhandle;
    }

    return SYS_ERR_OK;
}

errval_t fat_remove(void *st, const char *path) {
    assert(st);

    if (!path) {
        // invalid path
        return FS_ERR_NOTFOUND;
    }

    struct fat_mount *mount = st;

    struct fat_dir_entry dir_entry;
    errval_t err = fat_resolve_path(mount, path, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_resolve_path failed.\n");
        return err;
    }

    if (fat_is_dir(&dir_entry.fat_dir_entry)) {
        // we expected a file
        return FS_ERR_NOTFILE;
    }

    err = fat_delete_dir_entry(mount, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_delete_dir_entry failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

errval_t fat_read(void *st, fat_handle_t handle, void *buffer, size_t bytes,
                  size_t *bytes_read) {
    assert(st);
    struct fat_mount *mount = st;

    size_t cur_bytes_read;
    if (!bytes_read) {
        bytes_read = &cur_bytes_read;
    }

    *bytes_read = 0;
    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct fat_file_handle *fhandle = handle;

    if (fhandle->common.is_dir) {
        // we cannot read a directory
        return FS_ERR_NOTFILE;
    }

    if (!buffer) {
        // we got no buffer, which is not a problem if we do not read anything
        return SYS_ERR_OK;
    }

    // don't read past the end of the file
    if (fhandle->data_offset + bytes > fhandle->common.dir_entry.fat_dir_entry.DIR_FileSize) {
        bytes = fhandle->common.dir_entry.fat_dir_entry.DIR_FileSize - fhandle->data_offset;
    }

    if (!bytes) {
        // reading zero bytes is trivial
        return SYS_ERR_OK;
    }

    struct fat_sector_iter sector_iter;
    errval_t err = fat_sector_iter_init_from_fhandle(&sector_iter, mount, fhandle);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_init_from_fhandle failedl\n");
        return err;
    }

    char *read_buffer = buffer;
    while (*bytes_read < bytes) {
        uint32_t data_remaining = bytes - *bytes_read;
        // the sector size must be a power of two, so it is fine to use
        // one less than the sector size as a bitmask to compute the
        // offset inside the sector
        uint16_t sector_offset = fhandle->data_offset & (mount->sector_size - 1);
        uint32_t sector_data_size = MIN(data_remaining, mount->sector_size - sector_offset);

        void *sector;
        err = fat_sector_iter_read_sector(&sector_iter, &sector);
        if (err_is_fail(err)) {
            fhandle->iter_state_valid = false;
            FAT_DEBUG_ERR(err, "fat_sector_cache_read failed.\n");
            return err;
        }
        memcpy(read_buffer, sector + sector_offset, sector_data_size);
        read_buffer += sector_data_size;
        *bytes_read += sector_data_size;
        fhandle->data_offset += sector_data_size;
        if (*bytes_read >= bytes) {
            // we read all there is to read
            assert(*bytes_read == bytes);
            // if we read up until the end of a sector, we need to advance
            // the sector iterator
            if (sector_offset + sector_data_size == mount->sector_size) {
                err = fat_sector_iter_next(&sector_iter);
                if (err_is_fail(err)) {
                    fhandle->iter_state_valid = false;
                    FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
                    return err;
                }
            }
            break;
        }
        err = fat_sector_iter_next(&sector_iter);
        if (err_is_fail(err)) {
            fhandle->iter_state_valid = false;
            FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
            return err;
        }
    }
    fhandle->iter_state = sector_iter.state;
    fhandle->iter_state_valid = true;
    return SYS_ERR_OK;
}

errval_t fat_write(void *st, fat_handle_t handle, const void *buffer,
                   size_t bytes, size_t *bytes_written) {
    assert(st);
    struct fat_mount *mount = st;

    size_t cur_bytes_written;
    if (!bytes_written) {
        bytes_written = &cur_bytes_written;
    }

    *bytes_written = 0;
    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct fat_file_handle *fhandle = handle;

    if (fhandle->common.is_dir) {
        // we cannot read a directory
        return FS_ERR_NOTFILE;
    }

    if (!buffer) {
        // we got no buffer, which is not a problem
        return SYS_ERR_OK;
    }

    // don't expand files to more than the allowed size
    uint64_t new_filesize = fhandle->data_offset + bytes;
    if (new_filesize > FAT_MAX_FILE_SIZE) {
        bytes = FAT_MAX_FILE_SIZE - fhandle->data_offset;
    }

    if (!bytes) {
        // writing zero bytes is trivial
        return SYS_ERR_OK;
    }

    assert(fat_sector_iter_state_valid(&fhandle->common.dir_entry.sector_iter_state));

    // update file size
    struct fat_sector_iter sector_iter;
    fat_sector_iter_init_with_state(&sector_iter, mount, NULL, &fhandle->common.dir_entry.sector_iter_state);

    struct FAT_DirEntry *entry_table;
    errval_t err = fat_sector_iter_read_sector(&sector_iter, (void**)&entry_table);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
        return err;
    }

    entry_table[fhandle->common.dir_entry.dir_iter_state.entry].DIR_FileSize = (uint32_t)new_filesize;
    err = fat_sector_iter_write_sector(&sector_iter, entry_table);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
        return err;
    }

    // write file content
    err = fat_sector_iter_init_from_fhandle(&sector_iter, mount, fhandle);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_sector_iter_init_from_fhandle failedl\n");
        return err;
    }

    const char *write_buffer = buffer;
    while (*bytes_written < bytes) {
        if (fat_sector_iter_eof(&sector_iter)) {
            // we need to expand the file with a new cluster
            err = fat_sector_iter_append_cluster(&sector_iter);
            if (err_is_fail(err)) {
                fhandle->iter_state_valid = false;
                FAT_DEBUG_ERR(err, "fat_sector_iter_append_cluster failed.\n");
                return err;
            }
        }
        uint32_t data_remaining = bytes - *bytes_written;
        // the sector size must be a power of two, so it is fine to use
        // one less than the sector size as a bitmask to compute the
        // offset inside the sector
        uint16_t sector_offset = fhandle->data_offset & (mount->sector_size - 1);
        uint32_t sector_data_size = MIN(data_remaining, mount->sector_size - sector_offset);
        if (sector_data_size == mount->sector_size) {
            // aligned write, we can directly write the sector
            err = fat_sector_iter_write_sector(&sector_iter, write_buffer);
            if (err_is_fail(err)) {
                fhandle->iter_state_valid = false;
                FAT_DEBUG_ERR(err, "fat_sector_iter_write_sector failed.\n");
                return err;
            }
        }
        else {
            // unaligned write, we need to read the sector first into a temporary buffer
            // write into the buffer and then write the sector back
            char *sector;
            err = fat_sector_iter_read_sector(&sector_iter, (void**)&sector);
            if (err_is_fail(err)) {
                fhandle->iter_state_valid = false;
                FAT_DEBUG_ERR(err, "fat_sector_iter_read_sector failed.\n");
                return err;
            }
            memcpy(sector + sector_offset, write_buffer, sector_data_size);
            err = fat_sector_iter_write_sector(&sector_iter, sector);
        }
        write_buffer += sector_data_size;
        *bytes_written += sector_data_size;
        fhandle->data_offset += sector_data_size;
        if (*bytes_written >= bytes) {
            // we read all there is to read
            assert(*bytes_written == bytes);
            // if we wrote up until the end of a sector, we need to advance
            // the sector iterator
            if (sector_offset + sector_data_size == mount->sector_size) {
                err = fat_sector_iter_next(&sector_iter);
                if (err_is_fail(err)) {
                    fhandle->iter_state_valid = false;
                    FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
                    return err;
                }
            }
            break;
        }
        err = fat_sector_iter_next(&sector_iter);
        if (err_is_fail(err)) {
            fhandle->iter_state_valid = false;
            FAT_DEBUG_ERR(err, "fat_sector_iter_next failed.\n");
            return err;
        }
    }
    fhandle->iter_state = sector_iter.state;
    fhandle->iter_state_valid = true;
    return SYS_ERR_OK;
}

errval_t fat_truncate(void *st, fat_handle_t handle, size_t bytes) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t fat_tell(void *st, fat_handle_t handle, size_t *pos) {
    assert(st);

    if (!pos) {
        // we don't care when the user doesn't provide a pointer
        return SYS_ERR_OK;
    }

    *pos = 0;
    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct fat_common_handle *chandle = handle;

    if (chandle->is_dir) {
        // tell is only supported on files
        return FS_ERR_NOTFILE;
    }
    struct fat_file_handle *fhandle = handle;

    *pos = fhandle->data_offset;

    return SYS_ERR_OK;
}

errval_t fat_stat(void *st, fat_handle_t inhandle, struct fs_fileinfo *info) {
    assert(st);

    info->size = 0;
    info->type = FS_DIRECTORY;

    if (!inhandle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }

    struct fat_common_handle *chandle = inhandle;
    if (!chandle->is_dir) {
        struct fat_file_handle *fhandle = inhandle;
        info->type = FS_FILE;
        info->size = fhandle->common.dir_entry.fat_dir_entry.DIR_FileSize;
    }

    return SYS_ERR_OK;
}

errval_t fat_seek(void *st, fat_handle_t handle, enum fs_seekpos whence,
                  off_t offset) {
    assert(st);

    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct fat_common_handle *chandle = handle;

    if (chandle->is_dir) {
        // seek is not allowed on directories
        return FS_ERR_NOTFILE;
    }
    struct fat_file_handle *fhandle = handle;
    switch(whence) {
        case FS_SEEK_SET:
            fhandle->data_offset = offset;
            break;
        case FS_SEEK_CUR:
            fhandle->data_offset += offset;
            break;
        case FS_SEEK_END:
            fhandle->data_offset = fhandle->common.dir_entry.fat_dir_entry.DIR_FileSize;
            break;
        default:
            FAT_DEBUG("Invalid whence argument in fat_seek.\n");
            break;
    }
    fhandle->iter_state_valid = false;

    return SYS_ERR_OK;
}

errval_t fat_close(void *st, fat_handle_t inhandle) {
    assert(st);

    if (!inhandle) {
        // we got an invalid file handle
        return FS_ERR_INVALID_FH;
    }
    struct fat_common_handle *chandle = inhandle;
    if (chandle->is_dir) {
        // directories must be closed with closedir
        return FS_ERR_NOTFILE;
    }

    free(inhandle);

    return SYS_ERR_OK;
}

// ========== public API for directory handling ========== //

errval_t fat_opendir(void *st, const char *path, fat_handle_t *rethandle) {
    assert(st);

    if (!rethandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    *rethandle = NULL;
    if (!path) {
        // invalid path
        return FS_ERR_NOTFOUND;
    }

    struct fat_mount *mount = st;

    struct fat_dir_entry dir_entry;
    errval_t err = fat_resolve_path(mount, path, &dir_entry);
    if (err_no(err) == FS_ERR_NOTFOUND) {
        // we could not find a file system object at the given path
        return err;
    }
    if (err_is_fail(err)) {
        // an internal error occured
        FAT_DEBUG_ERR(err, "fat_resolve_path failed.\n");
        return err;
    }

    if (!fat_is_dir(&dir_entry.fat_dir_entry)) {
        // we expected a folder
        return FS_ERR_NOTDIR;
    }

    struct fat_dir_handle *dhandle = malloc(sizeof(struct fat_dir_handle));
    if (!dhandle) {
        return LIB_ERR_MALLOC_FAIL;
    }

    dhandle->common.is_dir = true;
    dhandle->common.dir_entry = dir_entry;
    fat_dir_iter_init(&dhandle->dir_iter, mount, &dhandle->common.dir_entry);

    *rethandle = dhandle;

    return SYS_ERR_OK;
}

errval_t fat_dir_read_next(void *st, fat_handle_t inhandle, char **retname,
                           struct fs_fileinfo *info) {
    assert(st);

    *retname = NULL;

    if (!inhandle) {
        // we got an invalid file handle
        return FS_ERR_INVALID_FH;
    }

    struct fat_common_handle *chandle = inhandle;
    if (!chandle->is_dir) {
        // dir_read_next is not allowed on directories
        return FS_ERR_NOTDIR;
    }

    struct fat_dir_handle *dhandle = inhandle;

    errval_t err;
    struct fat_dir_entry dir_entry;
    while (err_is_ok(err = fat_dir_iter_next(&dhandle->dir_iter, &dir_entry))) {
        // ignore the volume ID entry
        if (!(dir_entry.fat_dir_entry.DIR_Attr & ATTR_VOLUME_ID)) {
            break;
        }
    }
    if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
        // there are no more directory entries to enumerate
        return err;
    }
    if (err_is_fail(err)) {
        // an internal error occured
        FAT_DEBUG_ERR(err, "fat_dir_iter_next failed.\n");
        return err;
    }

    if (retname != NULL) {
        if (dir_entry.long_name[0]) {
            // we have a long name for this entry
            *retname = strdup(dir_entry.long_name);
        }
        else {
            // we need to assemble the short name
            struct FAT_DirEntry *fat_dir_entry = &dir_entry.fat_dir_entry;
            uint8_t name_len = FAT_SHORT_NAME_NAME_LENGTH;
            while (name_len > 0 && fat_dir_entry->DIR_Name[name_len - 1] == ' ') name_len--;
            uint8_t ext_len = FAT_SHORT_NAME_EXTENSION_LENGTH;
            while (ext_len > 0 && fat_dir_entry->DIR_NameExt[ext_len - 1] == ' ') ext_len--;
            uint8_t tot_len = name_len + ext_len + 1 + (ext_len ? 1 : 0);
            *retname = calloc(1, tot_len);
            if (*retname) {
                strncpy(*retname, fat_dir_entry->DIR_Name, name_len);
                if (ext_len) {
                    strcat(*retname, ".");
                    strncat(*retname, fat_dir_entry->DIR_NameExt, ext_len);
                }
            }
        }
        if (!*retname) {
            FAT_DEBUG("Failed to allocate memory for the directory entry name.\n");
            return LIB_ERR_MALLOC_FAIL;
        }
    }

    if (info) {
        if (dir_entry.fat_dir_entry.DIR_Attr & ATTR_DIRECTORY) {
            // we have a directory entry
            info->type = FS_DIRECTORY;
            info->size = 0;
        }
        else {
            // we have a file entry
            info->type = FS_FILE;
            info->size = dir_entry.fat_dir_entry.DIR_FileSize;
        }
    }

    return SYS_ERR_OK;
}

errval_t fat_closedir(void *st, fat_handle_t handle) {
    assert(st);

    if (!handle) {
        // we got an invalid file handle
        return FS_ERR_INVALID_FH;
    }

    struct fat_common_handle *chandle = handle;
    if (!chandle->is_dir) {
        // files must be closed with close
        return FS_ERR_NOTFILE;
    }

    struct fat_dir_handle *dhandle = handle;

    free(dhandle);

    return SYS_ERR_OK;
}

errval_t fat_mkdir(void *st, const char *path) {
    assert(st);

    struct fat_mount *mount = st;
    struct fat_dir_entry parent_entry, dir_entry;
    errval_t err = fat_create_dir_entry(mount, path, true, &parent_entry, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_create_dir_entry failed.\n");
        return err;
    }

    err = fat_initialize_dir(mount, &parent_entry, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_initialize_dir failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

errval_t fat_rmdir(void *st, const char *path) {
    assert(st);

    if (!path) {
        // invalid path
        return FS_ERR_NOTFOUND;
    }

    struct fat_mount *mount = st;

    struct fat_dir_entry dir_entry;
    errval_t err = fat_resolve_path(mount, path, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_resolve_path failed.\n");
        return err;
    }

    if (!fat_is_dir(&dir_entry.fat_dir_entry)) {
        // we expected a directory
        return FS_ERR_NOTDIR;
    }

    struct fat_dir_iter dir_iter;
    fat_dir_iter_init(&dir_iter, mount, &dir_entry);

    struct fat_dir_entry dummy_entry;
    err = fat_dir_iter_next(&dir_iter, &dummy_entry);
    if (err_is_ok(err)) {
        // the directory is not empty
        return FS_ERR_NOTEMPTY;
    }
    if (err_no(err) != FS_ERR_INDEX_BOUNDS) {
        FAT_DEBUG_ERR(err, "fat_dir_iter_next failed.\n");
        return err;
    }

    err = fat_delete_dir_entry(mount, &dir_entry);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_delete_dir_entry failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

// ========== public API mounting ========== //

errval_t fat_mount(const char *uri, fat_mount_t *retst) {
    assert(uri);
    assert(retst);

    *retst = NULL;

    size_t device_block_size;
    errval_t err = aos_rpc_blockdev_get_block_size(aos_rpc_get_blockdev_channel(), &device_block_size);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "aos_rpc_blockdev_get_block_size failed.\n");
        return FAT_ERR_BAD_FS;
    }

    if (device_block_size < sizeof(struct FAT_BootSector)) {
        FAT_DEBUG("The device block size is smaller than the boot sector.\n");
        return FAT_ERR_BAD_FS;
    }

    void *boot_sector_buf = malloc(device_block_size);
    if (!boot_sector_buf) {
        FAT_DEBUG("Failed to allocate memory to read the boot sector.\n");
        return LIB_ERR_MALLOC_FAIL;
    }

    err = aos_rpc_blockdev_read_block(aos_rpc_get_blockdev_channel(), 0, boot_sector_buf, device_block_size);
    if (err_is_fail(err)) {
        free(boot_sector_buf);
        FAT_DEBUG_ERR(err, "Failed to read the boot sector block.\n");
        return err_push(err, FAT_ERR_BAD_FS);
    }

    struct fat_mount *mount = calloc(1, sizeof(*mount));
    if (!mount) {
        free(boot_sector_buf);
        FAT_DEBUG("Failed to allocate memory for the mount data structure.\n");
        return LIB_ERR_MALLOC_FAIL;
    }

    mount->device_block_size = device_block_size;

    struct FAT_BootSector *boot_sector = boot_sector_buf;

    err = fat_mount_init(mount, 0, boot_sector);
    free(boot_sector_buf);
    if (err_is_fail(err)) {
        free(mount);
        FAT_DEBUG_ERR(err, "fat_mount_init failed.\n");
        return err;
    }

    *retst = mount;

    return SYS_ERR_OK;
}
