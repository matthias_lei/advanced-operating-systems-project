#include <fat/fatfs_debug.h>

void dump_BPB(const struct FAT_BPB *bpb) {
    printf("BS_jmpBoot:\t\t 0x%x, 0x%x, 0x%x\n", bpb->BS_jmpBoot[0], bpb->BS_jmpBoot[1], bpb->BS_jmpBoot[2]);
    printf("BS_OEMName:\t\t %.8s\n", bpb->BS_OEMName);
    printf("BPB_BytsPerSec:\t\t %" PRIu16 " bytes/sector\n", bpb->BPB_BytsPerSec);
    printf("BPB_SecPerClus:\t\t %" PRIu8 " sectors/cluster\n", bpb->BPB_SecPerClus);
    printf("BPB_RsvdSecCnt:\t\t %" PRIu16 " reserved sectors\n", bpb->BPB_RsvdSecCnt);
    printf("BPB_NumFATs:\t\t %" PRIu8 " FAT structures\n", bpb->BPB_NumFATs);
    printf("BPB_RootEntCnt:\t\t %" PRIu16 " directory entries (only FAT12/FAT16)\n", bpb->BPB_RootEntCnt);
    printf("BPB_TotSec16:\t\t %" PRIu16 " total sectors (only FAT12/FAT16)\n", bpb->BPB_TotSec16);
    printf("BPB_Media:\t\t 0x%x\n", bpb->BPB_Media);
    printf("BPB_FATSz16:\t\t %" PRIu16 " FAT sector count (only FAT12/FAT16)\n", bpb->BPB_FATSz16);
    printf("BPB_SecPerTrk:\t\t %" PRIu16 " sectors per track\n", bpb->BPB_SecPerTrk);
    printf("BPB_NumHeads:\t\t %" PRIu16 " number of heads\n", bpb->BPB_NumHeads);
    printf("BPB_HiddSec:\t\t %" PRIu32 " hidden sectors\n", bpb->BPB_HiddSec);
    printf("BPB_TotSec32:\t\t %" PRIu32 " total sectors (optional for FAT12/FAT16)\n", bpb->BPB_TotSec32);
}

void dump_BPB_FAT16(const struct FAT_BPB_FAT16 *bpb_fat16) {
    printf("BS_DrvNum:\t\t %" PRIu8 "\n", bpb_fat16->BS_DrvNum);
    printf("BS_Reserved1:\t\t %" PRIu8 "\n", bpb_fat16->BS_Reserved1);
    printf("BS_BootSig:\t\t 0x%x\n", bpb_fat16->BS_BootSig);
    printf("BS_VolID:\t\t 0x%x\n", bpb_fat16->BS_VolID);
    printf("BS_VolLab:\t\t %.11s\n", bpb_fat16->BS_VolLab);
    printf("BS_FilSysType:\t\t %.8s\n", bpb_fat16->BS_FilSysType);
}

void dump_BPB_FAT32(const struct FAT_BPB_FAT32 *bpb_fat32) {
    printf("BPB_FATSz32:\t\t %" PRIu32 " FAT sector count\n", bpb_fat32->BPB_FATSz32);
    printf("BPB_ExtFlags:\t\t Fat mirroring %s, Active FAT: %" PRIu16 "\n", ((bpb_fat32->BPB_ExtFlags >> 7) & 0x1) ? "disabled" : "enabled", bpb_fat32->BPB_ExtFlags & 0xf);
    printf("BPB_FSVer:\t\t %d.%d\n", bpb_fat32->BPB_FSVer[1], bpb_fat32->BPB_FSVer[0]);
    printf("BPB_RootClus:\t\t first cluster of root directory: %d\n", bpb_fat32->BPB_RootClus);
    printf("BPB_FSInfo:\t\t sector number of FSINFO in reserved area: %" PRIu16 "\n", bpb_fat32->BPB_FSInfo);
    printf("BPB_BkBootSec:\t\t sector number of boot record copy in reserved area: %" PRIu16 "\n", bpb_fat32->BPB_BkBootSec);
    printf("BPB_Reserved:\t\t %.12s\n", bpb_fat32->BPB_Reserved);
    dump_BPB_FAT16(&bpb_fat32->BPB_FAT16);
}

void dump_FAT(const struct FAT *fat) {
    dump_BPB(&fat->FAT_BPB);
    dump_BPB_FAT32(&fat->FAT_BPB_FAT32);
}

void dump_FAT_BootSector(const struct FAT_BootSector *boot_sector) {
    dump_FAT(&boot_sector->FAT);
    printf("BS_Signature:\t\t 0x%x\n", boot_sector->BS_Signature);
}

#define APPEND_ATTR(entry, string, attr)    \
    do {                                    \
        if ((entry)->DIR_Attr & attr) {     \
            strcat((string), #attr " ");    \
        }                                   \
    } while(0)                              \

void dump_FAT_DirEntry(const struct FAT_DirEntry *dir_entry) {
    printf("DIR_Name:\t\t %.8s.%.3s\n", dir_entry->DIR_Name, dir_entry->DIR_NameExt);
    char attrs[100];
    attrs[0] = '\0';
    if ((dir_entry->DIR_Attr & ATTR_LONG_NAME) == ATTR_LONG_NAME) {
        strcpy(attrs, "ATTR_LONG_NAME");
    }
    else {
        APPEND_ATTR(dir_entry, attrs, ATTR_READ_ONLY);
        APPEND_ATTR(dir_entry, attrs, ATTR_HIDDEN);
        APPEND_ATTR(dir_entry, attrs, ATTR_SYSTEM);
        APPEND_ATTR(dir_entry, attrs, ATTR_VOLUME_ID);
        APPEND_ATTR(dir_entry, attrs, ATTR_DIRECTORY);
        APPEND_ATTR(dir_entry, attrs, ATTR_ARCHIVE);
    }
    printf("DIR_Attr:\t\t %s\n", attrs);
    printf("DIR_NTRes:\t\t %" PRIu8 "\n", dir_entry->DIR_NTRes);
    printf("DIR_CrtTime:\t\t %.2" PRIu16 ".%.2" PRIu16 ".%.4" PRIu16 " %.2" PRIu16 ":%.2" PRIu16 ":%.2" PRIu16 ".%.3" PRIu8 "\n",
        dir_entry->DIR_CrtDate & 0x1f,
        (dir_entry->DIR_CrtDate >> 5) & 0xf,
        ((dir_entry->DIR_CrtDate >> 9) & 0xff) + 1980,
        (dir_entry->DIR_CrtTime >> 11) & 0x1f,
        (dir_entry->DIR_CrtTime >> 5) & 0x3f,
        (dir_entry->DIR_CrtTime & 0x1f) + dir_entry->DIR_CrtTimeTenth / 100,
        dir_entry->DIR_CrtTimeTenth % 100);

    printf("DIR_LstAccDate\t\t %.2" PRIu16 ".%.2" PRIu16 ".%.4" PRIu16 "\n",
        dir_entry->DIR_LstAccDate & 0x1f,
        (dir_entry->DIR_LstAccDate >> 5) & 0xf,
        ((dir_entry->DIR_LstAccDate >> 9) & 0xff) + 1980);

    uint32_t cluster = dir_entry->DIR_FstClusHI << 16 | dir_entry->DIR_FstClusLO;
    printf("DIR_FstClus:\t\t %" PRIu32 "\n", cluster);

    printf("DIR_WrtTime:\t\t %.2" PRIu16 ".%.2" PRIu16 ".%.4" PRIu16 " %.2" PRIu16 ":%.2" PRIu16 ":%.2" PRIu16 "\n",
        dir_entry->DIR_WrtDate & 0x1f,
        (dir_entry->DIR_WrtDate >> 5) & 0xf,
        ((dir_entry->DIR_WrtDate >> 9) & 0xff) + 1980,
        (dir_entry->DIR_WrtTime >> 11) & 0x1f,
        (dir_entry->DIR_WrtTime >> 5) & 0x3f,
        dir_entry->DIR_WrtTime & 0x1f);

    printf("DIR_FileSize:\t\t %" PRIu32 "\n", dir_entry->DIR_FileSize);
}

errval_t fat_debug_print_dirs_recursive(struct fat_mount *mount, const char *parent) {
    assert(mount);
    assert(parent);

    fat_handle_t dhandle;
    errval_t err = fat_opendir(mount, parent, &dhandle);
    if (err_is_fail(err)) {
        FAT_DEBUG_ERR(err, "fat_opendir failed for %s.\n", parent);
        return err;
    }

    char *name;
    struct fs_fileinfo fi;
    while (err_is_ok(err = fat_dir_read_next(mount, dhandle, &name, &fi))) {
        if (!name) {
            continue;
        }
        if(!strcmp(name, ".") || !strcmp(name, "..")) {
            free(name);
            continue;
        }
        char path[260];
        strcpy(path, parent);
        strcat(path, name);
        free(name);
        printf("%s\n", path);
        if (fi.type == FS_DIRECTORY) {
            strcat(path, "/");
            err = fat_debug_print_dirs_recursive(mount, path);
            if (err_is_fail(err)) {
                break;
            }
        }
    }

    if (err_no(err) == FS_ERR_INDEX_BOUNDS) {
        err = SYS_ERR_OK;
    }

    errval_t err2 = fat_closedir(mount, dhandle);
    if (err_is_fail(err2)) {
        FAT_DEBUG_ERR(err2, "fat_closedir failed.\n");
        err = err_push(err, err2);
    }
    return err;
}
