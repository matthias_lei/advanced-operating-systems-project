#include <vfs/vfs_p.h>

#include <fs/fatfs.h>
#include <fs/multibootfs.h>
#include <fs/ramfs.h>

static struct vfs_mount_vtable fatfs_vtable = {
    .lookup = fat_lookup,
    .open = fat_open,
    .create = fat_create,
    .remove = fat_remove,
    .read = fat_read,
    .write = fat_write,
    .truncate = fat_truncate,
    .tell = fat_tell,
    .stat = fat_stat,
    .seek = fat_seek,
    .close = fat_close,
    .opendir = fat_opendir,
    .dir_read_next = fat_dir_read_next,
    .closedir = fat_closedir,
    .mkdir = fat_mkdir,
    .rmdir = fat_rmdir,
    .mount = fat_mount
};

static struct vfs_mount_vtable multibootfs_vtable = {
    .lookup = multiboot_lookup,
    .open = multiboot_open,
    .create = multiboot_create,
    .remove = multiboot_remove,
    .read = multiboot_read,
    .write = multiboot_write,
    .truncate = multiboot_truncate,
    .tell = multiboot_tell,
    .stat = multiboot_stat,
    .seek = multiboot_seek,
    .close = multiboot_close,
    .opendir = multiboot_opendir,
    .dir_read_next = multiboot_dir_read_next,
    .closedir = multiboot_closedir,
    .mkdir = multiboot_mkdir,
    .rmdir = multiboot_rmdir,
    .mount = multiboot_mount
};

static struct vfs_mount_vtable ramfs_vtable = {
    .lookup = ramfs_lookup,
    .open = ramfs_open,
    .create = ramfs_create,
    .remove = ramfs_remove,
    .read = ramfs_read,
    .write = ramfs_write,
    .truncate = ramfs_truncate,
    .tell = ramfs_tell,
    .stat = ramfs_stat,
    .seek = ramfs_seek,
    .close = ramfs_close,
    .opendir = ramfs_opendir,
    .dir_read_next = ramfs_dir_read_next,
    .closedir = ramfs_closedir,
    .mkdir = ramfs_mkdir,
    .rmdir = ramfs_rmdir,
    .mount = ramfs_mount
};

static struct vfs_mount *mounts = NULL;
static struct vfs_handle *open_handles = NULL;

static void vfs_add_open_handle(struct vfs_handle *handle) {
    handle->prev = NULL;
    handle->next = open_handles;
    open_handles = handle;
    if (handle->next) {
        handle->next->prev = handle;
    }
}

static void vfs_remove_open_handle(struct vfs_handle *handle) {
    if (handle->prev) {
        handle->prev->next = handle->next;
    }
    else {
        open_handles = handle->next;
    }
    if (handle->next) {
        handle->next->prev = handle->prev;
    }
}

static bool vfs_has_open_handle(const char *path) {
    struct vfs_handle *handle = open_handles;
    while (handle) {
        if (!strcmp(path, handle->path)) {
            return true;
        }
        handle = handle->next;
    }
    return false;
}

static errval_t vfs_find_mount(const char *path, struct vfs_mount **mount, const char **rel_path) {
    assert(path);
    assert(mount);
    assert(rel_path);

    *mount = NULL;
    *rel_path = NULL;

    struct vfs_mount *cur_mount = mounts;
    struct vfs_mount *match = NULL;
    int32_t match_len = -1;
    while (cur_mount) {
        int32_t mount_len = strlen(cur_mount->path);
        if (mount_len > match_len && !strncmp(path, cur_mount->path, mount_len) && (!path[mount_len] || path[mount_len] == FS_PATH_SEP)) {
            match = cur_mount;
            match_len = mount_len;
        }
        cur_mount = cur_mount->next;
    }

    if (!match) {
        // we have no matching mount
        return FS_ERR_NOTFOUND;
    }

    *mount = match;
    *rel_path = path + match_len;

    return SYS_ERR_OK;
}

static errval_t vfs_lookup(const char *path) {
    assert(path);

    if (path[0] && path[0] == FS_PATH_SEP && !path[1]) {
        // root is requested
        return SYS_ERR_OK;
    }

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    return mount->ops->lookup(mount->fs_mount, rel_path);
}

errval_t vfs_open(const char *path, vfs_handle_t *rethandle) {
    if (!path) {
        return FS_ERR_NOTFOUND;
    }

    if (!rethandle) {
        return FS_ERR_INVALID_FH;
    }

    if (vfs_has_open_handle(path)) {
        // can only open a file once
        return FS_ERR_BUSY;
    }

    *rethandle = NULL;

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    void *handle;
    err = mount->ops->open(mount->fs_mount, rel_path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    struct vfs_handle *vhandle = malloc(sizeof(struct vfs_handle));
    if (!vhandle) {
        mount->ops->close(mount->fs_mount, handle);
        return LIB_ERR_MALLOC_FAIL;
    }

    vhandle->mount = mount;
    vhandle->mount_handle = handle;
    vhandle->path = strdup(path);
    if (!vhandle->path) {
        free(vhandle);
        mount->ops->close(mount->fs_mount, handle);
        return LIB_ERR_MALLOC_FAIL;
    }

    vfs_add_open_handle(vhandle);

    *rethandle = vhandle;

    return SYS_ERR_OK;
}

errval_t vfs_create(const char *path, vfs_handle_t *rethandle) {
    if (!path) {
        return FS_ERR_NOTFOUND;
    }

    if (!rethandle) {
        return FS_ERR_INVALID_FH;
    }

    *rethandle = NULL;

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    void *handle;
    err = mount->ops->create(mount->fs_mount, rel_path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    struct vfs_handle *vhandle = malloc(sizeof(struct vfs_handle));
    if (!vhandle) {
        mount->ops->close(mount->fs_mount, handle);
        return err;
    }

    vhandle->mount = mount;
    vhandle->mount_handle = handle;
    vhandle->path = strdup(path);
    if (!vhandle->path) {
        free(vhandle);
        mount->ops->close(mount->fs_mount, handle);
        return LIB_ERR_MALLOC_FAIL;
    }

    vfs_add_open_handle(vhandle);

    *rethandle = vhandle;

    return SYS_ERR_OK;
}

errval_t vfs_remove(const char *path) {
    if (!path) {
        return FS_ERR_NOTFOUND;
    }

    if (vfs_has_open_handle(path)) {
        // the path to remove is still open
        return FS_ERR_BUSY;
    }

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    return mount->ops->remove(mount->fs_mount, rel_path);
}

errval_t vfs_read(vfs_handle_t handle, void *buffer, size_t bytes,
                  size_t *bytes_read) {
    if (!handle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = handle;
    return vhandle->mount->ops->read(vhandle->mount->fs_mount, vhandle->mount_handle, buffer, bytes, bytes_read);
}

errval_t vfs_write(vfs_handle_t handle, const void *buffer,
                   size_t bytes, size_t *bytes_written) {
    if (!handle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = handle;
    return vhandle->mount->ops->write(vhandle->mount->fs_mount, vhandle->mount_handle, buffer, bytes, bytes_written);
}

errval_t vfs_truncate(vfs_handle_t handle, size_t bytes) {
    if (!handle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = handle;
    return vhandle->mount->ops->truncate(vhandle->mount->fs_mount, vhandle->mount_handle, bytes);
}

errval_t vfs_tell(vfs_handle_t handle, size_t *pos) {
    if (!handle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = handle;
    return vhandle->mount->ops->tell(vhandle->mount->fs_mount, vhandle->mount_handle, pos);
}

errval_t vfs_stat(vfs_handle_t inhandle, struct fs_fileinfo *info) {
    if (!inhandle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = inhandle;
    return vhandle->mount->ops->stat(vhandle->mount->fs_mount, vhandle->mount_handle, info);
}

errval_t vfs_seek(vfs_handle_t handle, enum fs_seekpos whence,
                  off_t offset) {
    if (!handle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = handle;
    return vhandle->mount->ops->seek(vhandle->mount->fs_mount, vhandle->mount_handle, whence, offset);
}

errval_t vfs_close(vfs_handle_t inhandle) {
    if (!inhandle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = inhandle;
    errval_t err = vhandle->mount->ops->close(vhandle->mount->fs_mount, vhandle->mount_handle);
    vfs_remove_open_handle(vhandle);
    free(vhandle);
    return err;
}

errval_t vfs_opendir(const char *path, vfs_handle_t *rethandle) {
    if (!path) {
        return FS_ERR_NOTFOUND;
    }

    if (!rethandle) {
        return FS_ERR_INVALID_FH;
    }

    *rethandle = NULL;

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    void *handle;
    err = mount->ops->opendir(mount->fs_mount, rel_path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    struct vfs_handle *vhandle = malloc(sizeof(struct vfs_handle));
    if (!vhandle) {
        mount->ops->closedir(mount->fs_mount, handle);
        return err;
    }

    vhandle->mount = mount;
    vhandle->mount_handle = handle;
    vhandle->path = strdup(path);
    if (!vhandle->path) {
        free(vhandle);
        mount->ops->closedir(mount->fs_mount, handle);
        return LIB_ERR_MALLOC_FAIL;
    }

    vfs_add_open_handle(vhandle);

    *rethandle = vhandle;

    return SYS_ERR_OK;
}

errval_t vfs_dir_read_next(vfs_handle_t inhandle, char **retname,
                             struct fs_fileinfo *info) {
    if (!inhandle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = inhandle;
    return vhandle->mount->ops->dir_read_next(vhandle->mount->fs_mount, vhandle->mount_handle, retname, info);
}

errval_t vfs_closedir(vfs_handle_t dhandle) {
    if (!dhandle) {
        return FS_ERR_INVALID_FH;
    }

    struct vfs_handle *vhandle = dhandle;
    errval_t err = vhandle->mount->ops->closedir(vhandle->mount->fs_mount, vhandle->mount_handle);
    vfs_remove_open_handle(vhandle);
    free(vhandle);
    return err;
}

errval_t vfs_mkdir(const char *path) {
    if (!path) {
        return FS_ERR_NOTFOUND;
    }

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    return mount->ops->mkdir(mount->fs_mount, rel_path);
}

errval_t vfs_rmdir(const char *path) {
    if (!path) {
        return FS_ERR_NOTFOUND;
    }

    if (vfs_has_open_handle(path)) {
        // cannot remove a directory with open handles
        return FS_ERR_BUSY;
    }

    struct vfs_mount *mount = NULL;
    const char *rel_path = NULL;
    errval_t err = vfs_find_mount(path, &mount, &rel_path);
    if (err_is_fail(err)) {
        // mount not found
        return err;
    }

    return mount->ops->rmdir(mount->fs_mount, rel_path);
}

errval_t vfs_mount(const char *path, const char *uri) {
    assert(path);
    assert(uri);

    if (path[0] != FS_PATH_SEP) {
        // path is not an absolute path
        return VFS_ERR_BAD_MOUNTPOINT;
    }

    const char *uri_colon = strchr(uri, ':');
    if (!uri_colon) {
        // uri does not contain a colon
        return VFS_ERR_BAD_URI;
    }

    const char *args = uri_colon + 1;
    if (!(args[0] && args[0] == '/' && args[1] && args[1] == '/')) {
        // uri does not have // after the colon
        return VFS_ERR_BAD_URI;
    }
    args += 2;

    errval_t err = vfs_lookup(path);
    if (err_is_fail(err)) {
        if (err_no(err) != FS_ERR_NOTDIR || err_no(err) != FS_ERR_NOTFOUND) {
            // internal error
            VFS_DEBUG_ERR(err, "vfs_resolve_path failed.\n");
        }
        return err;
    }

    if (!path[1]) {
        path++;
    }

    char *dev = malloc(uri_colon - uri + 1);
    if (!dev) {
        return LIB_ERR_MALLOC_FAIL;
    }

    memcpy(dev, uri, uri_colon - uri);
    dev[uri_colon - uri] = '\0';

    struct vfs_mount *mount = mounts;
    while (mount) {
        if (!strcmp(dev, mount->dev)) {
            // the device is already mounted
            free(dev);
            return VFS_ERR_BAD_MOUNTPOINT;
        }
        if (!strcmp(path, mount->path)) {
            free(dev);
            // the path is already mounted
            return VFS_ERR_BAD_MOUNTPOINT;
        }
        mount = mount->next;
    }

    mount = malloc(sizeof(struct vfs_mount));
    if (!mount) {
        free(dev);
        return LIB_ERR_MALLOC_FAIL;
    }

    mount->path = strdup(path);
    if (!mount->path) {
        free(dev);
        return LIB_ERR_MALLOC_FAIL;
    }

    if (!strcmp(dev, "mmchs")) {
        err = fat_mount(uri, &mount->fs_mount);
        mount->ops = &fatfs_vtable;
    }
    else if (!strcmp(dev, "multiboot")) {
        err = multiboot_mount(uri, &mount->fs_mount);
        mount->ops = &multibootfs_vtable;
    }
    else if (!strcmp(dev, "ramfs")) {
        err = ramfs_mount(uri, &mount->fs_mount);
        mount->ops = &ramfs_vtable;
    }
    else {
        err = VFS_ERR_BAD_URI;
    }

    if (err_is_fail(err)) {
        free(dev);
        free(mount->path);
        free(mount);
        return err;
    }

    // insert mount into the mount list
    mount->dev = dev;
    mount->prev = NULL;
    mount->next = mounts;
    if (mount->next) {
        mount->next->prev = mount;
    }
    mounts = mount;

    return SYS_ERR_OK;
}
