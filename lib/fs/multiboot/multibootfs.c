#include <multiboot/multibootfs_p.h>

#include <aos/aos_rpc.h>

static errval_t multiboot_dir_entry_init(struct multiboot_dir_entry *handle, const char *name, enum fs_filetype type) {
    handle->next = NULL;
    handle->child = NULL;
    handle->type = type;
    handle->name = strdup(name);
    if (!handle->name) {
        return LIB_ERR_MALLOC_FAIL;
    }
    return SYS_ERR_OK;
};

static errval_t multiboot_resolve_path(struct multiboot_mount *mount, const char *path, struct multiboot_dir_entry **outhandle) {
    assert(mount);
    assert(path);
    assert(outhandle);

    *outhandle = NULL;

    if (path[0] == FS_PATH_SEP) {
        path++;
    }

    char *path_copy = strdup(path);
    if (!path_copy) {
        return LIB_ERR_MALLOC_FAIL;
    }

    struct multiboot_dir_entry *parent = &mount->root;
    char *path_seg = strtok(path_copy, "/");
    while (path_seg) {
        struct multiboot_dir_entry *child = parent->child;
        while (child) {
            if (!strcmp(path_seg, child->name)) {
                break;
            }
            child = child->next;
        }
        if (!child) {
            free(path_copy);
            return FS_ERR_NOTFOUND;
        }
        path_seg = strtok(NULL, "/");
        parent = child;
    }
    free(path_copy);
    *outhandle = parent;
    return SYS_ERR_OK;
}

static void multiboot_destroy_tree(struct multiboot_dir_entry *root) {
    struct multiboot_dir_entry *child = root->child;
    while (child) {
        struct multiboot_dir_entry *next_child = child->next;
        multiboot_destroy_tree(child);
        child = next_child;
    }
    if (root->type == FS_FILE) {
        struct multiboot_file_entry *file = (void*)root;
        free(file->path);
    }
    free(root->name);
    free(root);
}

errval_t multiboot_lookup(void *st, const char *path) {
    assert(st);

    struct multiboot_mount *mount = st;

    struct multiboot_dir_entry *handle;
    errval_t err = multiboot_resolve_path(mount, path, &handle);
    if (err_is_fail(err)) {
        return err;
    }
    if (handle->type != FS_DIRECTORY) {
        return FS_ERR_NOTDIR;
    }
    return SYS_ERR_OK;
}

errval_t multiboot_open(void *st, const char *path, multiboot_handle_t *rethandle) {
    assert(st);

    if (!rethandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    *rethandle = NULL;

    if (!path) {
        // invalid path
        return FS_ERR_NOTFOUND;
    }

    struct multiboot_mount *mount = st;

    struct multiboot_dir_entry *dir_entry;
    errval_t err = multiboot_resolve_path(mount, path, &dir_entry);
    if (err_is_fail(err)) {
        MULTIBOOT_DEBUG_ERR(err, "multiboot_resolve_path failed.\n");
        return err;
    }

    if (dir_entry->type != FS_FILE) {
        return FS_ERR_NOTFILE;
    }

    struct multiboot_file_handle *fhandle = malloc(sizeof(struct multiboot_file_handle));
    if (!fhandle) {
        return LIB_ERR_MALLOC_FAIL;
    }

    fhandle->common.dir_entry = dir_entry;
    fhandle->data_offset = 0;

    *rethandle = fhandle;

    return SYS_ERR_OK;
}

errval_t multiboot_create(void *st, const char *path, multiboot_handle_t *rethandle) {
    // multiboot FS is read only
    return VFS_ERR_NOT_SUPPORTED;
}

errval_t multiboot_remove(void *st, const char *path) {
    // multiboot FS is read only
    return VFS_ERR_NOT_SUPPORTED;
}

errval_t multiboot_read(void *st, multiboot_handle_t handle, void *buffer, size_t bytes,
                        size_t *bytes_read) {
    assert(st);

    *bytes_read = 0;
    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct multiboot_common_handle *chandle = handle;

    if (chandle->dir_entry->type != FS_FILE) {
        // we cannot read a directory
        return FS_ERR_NOTFILE;
    }

    struct multiboot_file_handle *fhandle = handle;

    if (!buffer) {
        // we got no buffer, which is not a problem if we do not read anything
        return SYS_ERR_OK;
    }

    struct multiboot_file_entry *fentry = (void*)fhandle->common.dir_entry;

    // don't read past the end of the file
    if (fhandle->data_offset + bytes > fentry->size) {
        bytes = fentry->size - fhandle->data_offset;
    }

    if (!bytes) {
        // reading zero bytes is trivial
        return SYS_ERR_OK;
    }

    if (!fentry->content) {
        // we first need to load the content
        struct capref frame;
        errval_t err = aos_rpc_monitor_get_multiboot_module(aos_rpc_get_monitor_channel(), fentry->path, &frame);
        if (err_is_fail(err)) {
            MULTIBOOT_DEBUG_ERR(err, "aos_rpc_monitor_get_multiboot_module failed. %u\n", err_no(err));
            return err;
        }

        err = paging_map_frame_attr(get_current_paging_state(), &fentry->content, fentry->size, frame, VREGION_FLAGS_READ, NULL, NULL);
        if (err_is_fail(err)) {
            MULTIBOOT_DEBUG_ERR(err, "paging_map_frame_attr failed.\n");
            return err;
        }
    }

    // now read the actual data
    memcpy(buffer, fentry->content + fhandle->data_offset, bytes);
    fhandle->data_offset += bytes;

    *bytes_read = bytes;

    return SYS_ERR_OK;
}

errval_t multiboot_write(void *st, multiboot_handle_t handle, const void *buffer,
                         size_t bytes, size_t *bytes_written) {
    // multiboot FS is read only
    return VFS_ERR_NOT_SUPPORTED;
}

errval_t multiboot_truncate(void *st, multiboot_handle_t handle, size_t bytes) {
    // multiboot FS is read only
    return VFS_ERR_NOT_SUPPORTED;
}

errval_t multiboot_tell(void *st, multiboot_handle_t handle, size_t *pos) {
    assert(st);

    if (!pos) {
        // we don't care when the user doesn't provide a pointer
        return SYS_ERR_OK;
    }

    *pos = 0;
    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct multiboot_common_handle *chandle = handle;

    if (chandle->dir_entry->type != FS_FILE) {
        // tell is only supported on files
        return FS_ERR_NOTFILE;
    }
    struct multiboot_file_handle *fhandle = handle;

    *pos = fhandle->data_offset;

    return SYS_ERR_OK;
}

errval_t multiboot_stat(void *st, multiboot_handle_t inhandle, struct fs_fileinfo *info) {
    assert(st);

    info->type = FS_FILE;
    info->type = 0;

    if (!inhandle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }

    struct multiboot_common_handle *chandle = inhandle;

    info->type = chandle->dir_entry->type;
    if (info->type == FS_FILE) {
        struct multiboot_file_entry *fentry = (void*)chandle->dir_entry;
        info->size = fentry->size;
    }

    return SYS_ERR_OK;
}

errval_t multiboot_seek(void *st, multiboot_handle_t handle, enum fs_seekpos whence,
                        off_t offset) {
    assert(st);

    if (!handle) {
        // we got an invalid handle
        return FS_ERR_INVALID_FH;
    }
    struct multiboot_common_handle *chandle = handle;

    if (chandle->dir_entry->type != FS_FILE) {
        // seek is not allowed on directories
        return FS_ERR_NOTFILE;
    }

    struct multiboot_file_handle *fhandle = handle;

    struct multiboot_file_entry *fentry = (void*)fhandle->common.dir_entry;
    switch(whence) {
        case FS_SEEK_SET:
            fhandle->data_offset = offset;
            break;
        case FS_SEEK_CUR:
            fhandle->data_offset += offset;
            break;
        case FS_SEEK_END:
            fhandle->data_offset = fentry->size;
            break;
        default:
            MULTIBOOT_DEBUG("Invalid whence argument in fat_seek.\n");
            break;
    }

    return SYS_ERR_OK;
}

errval_t multiboot_close(void *st, multiboot_handle_t inhandle) {
    assert(st);

    if (!inhandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    free(inhandle);

    return SYS_ERR_OK;
}

errval_t multiboot_opendir(void *st, const char *path, multiboot_handle_t *rethandle) {
    assert(st);

    if (!rethandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    *rethandle = NULL;

    if (!path) {
        // invalid path
        return FS_ERR_NOTFOUND;
    }

    struct multiboot_mount *mount = st;

    struct multiboot_dir_entry *dir_entry;
    errval_t err = multiboot_resolve_path(mount, path, &dir_entry);
    if (err_is_fail(err)) {
        MULTIBOOT_DEBUG_ERR(err, "multiboot_resolve_path failed.\n");
    }

    if (dir_entry->type != FS_DIRECTORY) {
        return FS_ERR_NOTDIR;
    }

    struct multiboot_dir_handle *dhandle = malloc(sizeof(struct multiboot_dir_handle));
    if (!dhandle) {
        return LIB_ERR_MALLOC_FAIL;
    }

    dhandle->common.dir_entry = dir_entry;
    dhandle->current = dir_entry->child;

    *rethandle = dhandle;

    return SYS_ERR_OK;
}

errval_t multiboot_dir_read_next(void *st, multiboot_handle_t inhandle, char **retname,
                                 struct fs_fileinfo *info) {
    assert(st);

    if (!inhandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    struct multiboot_common_handle *chandle = inhandle;
    if (chandle->dir_entry->type != FS_DIRECTORY) {
        return FS_ERR_NOTDIR;
    }

    struct multiboot_dir_handle *dhandle = inhandle;

    if (!dhandle->current) {
        return FS_ERR_INDEX_BOUNDS;
    }

    if (retname) {
        *retname = strdup(dhandle->current->name);
        if (!*retname) {
            return LIB_ERR_MALLOC_FAIL;
        }
    }

    if (info) {
        // TODO jmeier: add file size
        info->size = 0;
        info->type = dhandle->current->type;
    }

    dhandle->current = dhandle->current->next;

    return SYS_ERR_OK;
}

errval_t multiboot_closedir(void *st, multiboot_handle_t dhandle) {
    assert(st);

    if (!dhandle) {
        // invalid handle
        return FS_ERR_INVALID_FH;
    }

    free(dhandle);

    return SYS_ERR_OK;
}

errval_t multiboot_mkdir(void *st, const char *path) {
    // multiboot FS is read only
    return VFS_ERR_NOT_SUPPORTED;
}

errval_t multiboot_rmdir(void *st, const char *path) {
    // multiboot FS is read only
    return VFS_ERR_NOT_SUPPORTED;
}

errval_t multiboot_mount(const char *uri, multiboot_mount_t *retst) {
    char *name_list;
    size_t list_len;

    errval_t err = aos_rpc_monitor_get_multiboot_module_names(aos_rpc_get_monitor_channel(), &name_list, &list_len);
    if (err_is_fail(err)) {
        MULTIBOOT_DEBUG_ERR(err, "aos_rpc_monitor_get_multiboot_module_names failed.\n");
        return err;
    }

    struct multiboot_mount *mount = malloc(sizeof(struct multiboot_mount));
    if (!mount) {
        free(name_list);
        return LIB_ERR_MALLOC_FAIL;
    }

    err = multiboot_dir_entry_init(&mount->root, "/", true);
    if (err_is_fail(err)) {
        free(name_list);
        free(mount);
        MULTIBOOT_DEBUG_ERR(err, "multiboot_dir_entry_init failed.\n");
        return err;
    }

    char *cur_name = name_list;
    while (*cur_name) {
        size_t name_len = strnlen(cur_name, list_len) + 1;
        // the list is terminated by two zero termination characters, so even
        // the name length with its zero termination character must always be
        // smaller than the remaining list length minus the size of the module
        // length field, whichs is directly appended to the string
        assert(name_len < list_len - sizeof(size_t));
        if (!name_len) {
            continue;
        }

        char *full_path = strdup(cur_name);
        if (!full_path) {
            multiboot_destroy_tree(&mount->root);
            free(mount);
            free(cur_name);
            return LIB_ERR_MALLOC_FAIL;
        }

        char *name = cur_name;
        if (name[0] == FS_PATH_SEP) {
            name++;
        }
        char *path_seg = strtok(name, "/");
        struct multiboot_dir_entry *parent = &mount->root;
        while (path_seg) {
            struct multiboot_dir_entry *child = parent->child;
            while (child) {
                if (!strcmp(path_seg, child->name)) {
                    break;
                }
                child = child->next;
            }
            char *next_path_seg = strtok(NULL, "/");
            if (!child) {
                struct multiboot_dir_entry *new_child = malloc(next_path_seg ? sizeof(struct multiboot_dir_entry) : sizeof(struct multiboot_file_entry));
                if (!new_child) {
                    multiboot_destroy_tree(&mount->root);
                    free(mount);
                    free(cur_name);
                    return LIB_ERR_MALLOC_FAIL;
                }
                err = multiboot_dir_entry_init(new_child, path_seg, next_path_seg != NULL);
                if (err_is_fail(err)) {
                    multiboot_destroy_tree(&mount->root);
                    free(mount);
                    free(cur_name);
                    MULTIBOOT_DEBUG_ERR(err, "multiboot_dir_entry_init failed.\n");
                    return err;
                }
                new_child->next = parent->child;
                parent->child = new_child;
            }
            parent = parent->child;
            path_seg = next_path_seg;
        }
        assert(parent->type == FS_FILE);
        struct multiboot_file_entry *fentry = (void*)parent;

        cur_name += name_len;

        size_t *modsize = (size_t*) cur_name;
        fentry->size = *modsize;
        fentry->path = full_path;
        fentry->content = NULL;

        cur_name += sizeof(size_t);
        list_len -= name_len + sizeof(size_t);
    }
    // only the last zero termination character should be remaining
    assert(list_len == 1);

    free(name_list);

    *retst = mount;

    return SYS_ERR_OK;
}
