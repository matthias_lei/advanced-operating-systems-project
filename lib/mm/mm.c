/**
 * \file
 * \brief A library for managing physical memory (i.e., caps)
 *
 * The library provides allocations of various sizes and alignments with the following restrictions:
 * - alignment <= size !!!
 *   this restriction is sensible and ok but we need to clearly document it (feedback from AOS support).
 * - min. size returned at the moment 4 KiB (based on restriction of cap_retype)
 * - other sizes available in multiples of 2, i.e. 8 KiB, 16 KiB,... up to memory limit
 * - for each size, an equivalent alignment is guaranteed for that block, e.g. 16 KiB alignment for 16 KiB block
 * - if a size is requested between these bins, e.g. 7 KiB, the next larger block is returned (8 KiB)
 *   this leads to wasting max 1/2 of memory at the moment in worst case situation,
 *   e.g. request 17 KiB -> receives 32 KiB, 15 KiB lost at the moment.
 *   note: splitting at 20 KiB, return 20 KiB and splitting the remaining 12 KiB into a 8 KiB and 4 KiB
 *   blocks can also lead to fragmentation. Example: if the 32 KiB block given for the 17 KiB
 *   request is freed, then again a 32 KiB region is available. However, splitting into 20 KiB
 *   and remainders will hardly allow to ever regenerate the 32 KiB region. Additionally, when
 *   the 20 KiB is freed, additional slicing would be needed to keep the blocks inserted into
 *   the buckets consistend with power of 2 distribution. Thus, additional fragmentation.
 *   In conclusion: We keep the current system that wastes some memory but keeps the blocks
 *   consistent with alignment.
 * - mm_free(): the provided parameters of base and size are ignored (see mail from AOS support).
 *
 * - optional tracking of allocated RAM capabilities
 *   -> allows checking whether free is only called on actually allocated blocks, O(n) in mm_free()
 *   -> similar checking is done if deactivated by looking up whether not found in free list
 *
 * - at the moment, base addresses and sizes of capabilities are checked by a call to
 *   frame_identify() for verification. This may be replaced by our own offset caclulations
 *   without this call for performance reasons.
 *
 * - internally, free memory regions are kept in single linked lists for each memory block size.
 *   note: not all buckets will be used, of course (in particular low bit buckets)
 *   but having them, simplifies access to them.
 *
 * v1.4 2017-10-25, Group C
 */

#include <bitmacros.h>
#include <mm/mm.h>
#include <aos/debug.h>

// compute slots for the table that maps RAM block base addresses to mmnodes
#if MM_MIN_BUCKET_INDEX == 10
    #define MAPL1SLOT_MASK MASK_T(unsigned, 12)
#elif MM_MIN_BUCKET_INDEX == 11
    #define MAPL1SLOT_MASK MASK_T(unsigned, 11)
#elif MM_MIN_BUCKET_INDEX == 12
    #define MAPL1SLOT_MASK MASK_T(unsigned, 10)
#else
    #error Invalid MM_MIN_BUCKET_INDEX
#endif

#define MAPL1SLOT(addr) (((addr) >> ((MM_MIN_BUCKET_INDEX) + 10)) & (MAPL1SLOT_MASK))
#define MAPL2SLOT(addr) (((addr) >> (MM_MIN_BUCKET_INDEX)) & MASK_T(unsigned, 10))


//--- some list management helper functions --------------------------------------------------------
//    note: these functions can be used to manage various mmnode lists, of course
//    just provide a separate list header

static inline void init_list(struct mmnode_list_header *header) {
    assert(header);
    header->head = NULL;
    header->size = 0;
}

static inline size_t list_size(struct mmnode_list_header *header) {
    assert(header);
    return header->size;
}

// node must *not* be element of the list
static inline void insert_mmnode(struct mmnode_list_header *header, struct mmnode *node) {
    assert(header);
    assert(node);
    if (header->head) {
        header->head->prev = node;
    }
    node->prev = NULL;
    node->next = header->head;
    header->head = node;
    header->size++;
}

static inline void remove_mmnode(struct mmnode_list_header *header, struct mmnode *node) {
    assert(header);
    assert(node);
    if (node->prev) {
        node->prev->next = node->next;
    }
    else {
        header->head = node->next;
    }
    if (node->next) {
        node->next->prev = node->prev;
    }
    header->size--;
}


// combines 'finding' *and* removing the node that is retrieved and returned
// NULL indicates that the element was not found (i.e. list was empty)
static inline struct mmnode *retrieve_first_node(struct mmnode_list_header *header) {
    assert(header);

    struct mmnode *head = header->head;
    if (head) {
        header->head = head->next;
        if (head->next) {
            head->next->prev = NULL;
        }
        header->size--;
        head->prev = NULL;
        head->next = NULL;
    }

    return head;
}


//--------------------------------------------------------------------------------------------------

/** invariant control: read documentation above the implementation
 *  for details.
 */
static errval_t assure_invariants(struct mm *mm);
static errval_t assure_slot_slab_alloc_invariant(struct mm *mm);
static errval_t assure_bucket_counts_invariant(struct mm *mm);

/**
 *  Allocates one or two consecutive capability slots from the internal
 *  slot pool.
 */
static errval_t mm_slot_alloc(struct mm *mm, size_t n, struct capref *ret) {
    assert(mm);
    assert(ret);
    assert(0 < n && n <= 2);
    if (n == 1) {
        // return the single reserve slot if available
        if (!capref_is_null(mm->free_slot_pair_list.single_reserve)) {
            *ret = mm->free_slot_pair_list.single_reserve;
            mm->free_slot_pair_list.single_reserve = NULL_CAP;
            return SYS_ERR_OK;
        }
    }
    // check if there are free slot pairs available in the pool
    if (!mm->free_slot_pair_list.head) {
        // this should never happen, since assure_slot_slab_alloc_invariant
        // must make sure that there are always enough slot pairs available
        debug_printf("mm_slot_alloc(): Free slot pair list is empty.\n");
        // error exit without allocations
        return MM_ERR_SLOT_NOSLOTS;
    }
    // get the first available pair
    struct capref_pair_list_elem *cap_pair = mm->free_slot_pair_list.head;
    *ret = cap_pair->cap;
    if (n == 1) {
        // copy the unused slot into the reserve
        mm->free_slot_pair_list.single_reserve = cap_pair->cap;
        mm->free_slot_pair_list.single_reserve.slot++;
    }
    // remove the pair from the list
    mm->free_slot_pair_list.head = cap_pair->next;
    mm->free_slot_pair_list.size--;
    // free the list element
    slab_free(&mm->slabs, cap_pair);

    return SYS_ERR_OK;
}

/**
 *  Returns a pair of free slots to the internal slot pool.
 */
static errval_t mm_slot_pair_free(struct mm *mm, struct capref_pair_list_elem *pair) {
    assert(mm);
    assert(pair);

    pair->next = mm->free_slot_pair_list.head;
    mm->free_slot_pair_list.head = pair;
    mm->free_slot_pair_list.size++;
    return SYS_ERR_OK;
}

/**
 * Returns a single slot to the internal slot pool
 */
static errval_t mm_slot_single_free(struct mm *mm, struct capref cap) {
    assert(mm);

    if (capref_is_null(mm->free_slot_pair_list.single_reserve)) {
        mm->free_slot_pair_list.single_reserve = cap;
        return SYS_ERR_OK;
    }

    // TODO: tracking more single slots; this needs some change in the MM struct
    // thus, let's first see whether it's needed
    // note: we cannot just assume that 2 caprefs are pairs.
    debug_printf("mm_slot_single_free(): single reserve slot is already full.\n");
    return SYS_ERR_OK;
}

/**
 *  Frees a pair of mmnodes with consecutive capability slots and returns
 *  the pair of slots to the internal slot pool.
 */
static errval_t mm_mmnode_pair_free(struct mm *mm, struct mmnode *node_a, struct mmnode *node_b) {
    assert(mm);
    assert(node_a);
    assert(node_b);
    assert(node_a->cap.slot + 1 == node_b->cap.slot || node_a->cap.slot == node_b->cap.slot + 1);

    struct mmnode *node_hold = node_a->cap.slot < node_b->cap.slot ? node_b : node_a;
    struct mmnode *node_free = node_a->cap.slot < node_b->cap.slot ? node_a : node_b;

    // use the mmnode slab of node_hold to store the new capability pair list element
    struct capref_pair_list_elem* free_slot_pair = (struct capref_pair_list_elem*) node_hold;
    free_slot_pair->cap = node_free->cap;
    mm_slot_pair_free(mm, free_slot_pair);

    // only free the slab that is not used anymore
    slab_free(&mm->slabs, node_free);
    return SYS_ERR_OK;
}


/**
 * Initialize the memory manager.
 *
 * \param  mm               The mm struct to initialize.
 * \param  objtype          The cap type this manager should deal with.
 * \param  slab_refill_func Custom function for refilling the slab allocator.
 * \param  slot_alloc_func  Function for allocating capability slots.
 * \param  slot_refill_func Function for refilling (making) new slots.
 * \param  slot_alloc_inst  Pointer to a slot allocator instance (typically passed to the alloc and refill functions).
 */
errval_t mm_init(struct mm *mm, enum objtype objtype,
                 slab_refill_func_t slab_refill_func,
                 slot_alloc_t slot_alloc_func,
                 slot_refill_t slot_refill_func,
                 void *slot_alloc_inst)
{
    assert(mm != NULL);

    // the size of struct capref_pair_list_elem has to be smaller or equal
    // to the size of struct mmnode, since the slabs for struct mmnode are
    // also used to store the free slot pairs in struct capref_pair_list_elem
    assert(sizeof(struct capref_pair_list_elem) <= sizeof(struct mmnode));
    slab_init(&mm->slabs, sizeof(struct mmnode), slab_refill_func);

    mm->slot_alloc = slot_alloc_func;
    mm->slot_refill = slot_refill_func;
    mm->slot_alloc_inst = slot_alloc_inst;
    mm->objtype = objtype;
    mm->is_internal_alloc = false;
    mm->free_slot_pair_list.size = 0;
    mm->free_slot_pair_list.head = NULL;
    mm->free_slot_pair_list.single_reserve = NULL_CAP;

    // using loops and manual init to be positively sure
    // *mm = {} before starting initializations here does not work with all compilers
    for (int i = 0; i < MM_BUCKETS_SIZE; i++) {
        init_list(&mm->buckets[i]);
    }

    // assure that there are enough slots and slabs available
    errval_t err = assure_slot_slab_alloc_invariant(mm);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_init(): assure_slot_slab_alloc_invariant failed.\n");
        // error path: no deallocations needed / possible here
        return err;
    }

    debug_printf("mm_init(): init complete with mmnode size %u -> %u mmnodes / %d KiB page.\n",
                 sizeof(struct mmnode), BASE_PAGE_SIZE / sizeof(struct mmnode), BASE_PAGE_SIZE / 1024);

    return SYS_ERR_OK;
}

// pisch: function freshly implemented following the declaration in the header file
void mm_dump_mmnodes(struct mm *mm) {
    debug_printf(" \n");
    debug_printf("Memory manager for objtype %d.\n", mm->objtype);
    debug_printf("- free mmnodes:\n");
    int index = 0;
    gensize_t sum_free = 0;

    for (int i = 0; i < MM_BUCKETS_SIZE; i++) {
        struct mmnode *current = mm->buckets[i].head;
        while (current) {
            debug_printf("  #%d: %" PRIuGENSIZE " KiB at 0x%" PRIxGENPADDR "\n", index, current->size / 1024, current->base);

            sum_free += current->size;
            index++;
            current = current->next;
        }
    }
    debug_printf("  sum: %" PRIuGENSIZE " KiB\n", sum_free / 1024);

    debug_printf("- allocated mmnodes:\n");
    index = 0;
    gensize_t sum_allocated = 0;
    gensize_t sum_free_check = 0;
    for (int i = 0; i < MM_MAP_L1_MAX_ENTRIES; i++) {
        for (int j = 0; j < MM_MAP_L2_MAX_ENTRIES; j++) {
            struct mmnode* node = mm->map_table[i][j];
            if (!node) {
                // no node mapped
                continue;
            }
            // parent nodes should never be in the map table
            assert (node->type != NodeType_Parent);
            if (node->type == NodeType_Free) {
                // free nodes already listed above
                sum_free_check += node->size;
                continue;
            }
            debug_printf("  #%d: %" PRIuGENSIZE " KiB at 0x%" PRIxGENPADDR "\n", index, node->size / 1024, node->base);

            sum_allocated += node->size;
            index++;
        }
    }

    debug_printf("  sum: %" PRIuGENSIZE " KiB\n", sum_allocated / 1024);

    gensize_t sum_total = sum_free + sum_allocated;
    debug_printf("  total mem: %" PRIuGENSIZE " KiB\n", sum_total / 1024);

    // sum of bytes from blocks in free list must match sum of bytes from free blocks stored in the map table
    assert(sum_free == sum_free_check);
    // number of managed bytes must match the combined sum of bytes from free and allocated blocks
    assert(mm->managed_mem == sum_total);

    debug_printf(" \n");
}

/**
 * Destroys the memory allocator.
 *
 * current implementation as hinted by reply in AOS support. No need to implement this function.
 *
 * It is also never called in the current code base that we have.
 */
void mm_destroy(struct mm *mm)
{
    assert(!"NYI");
}


/**
 * Splits parent into n (in range [1,2]) nodes of appropriate size.
 * Note: except in the beginning (loaading RAM regions), this function is only called with n=2.
 *
 * In case of an error, it unrolls its internal allocations, if possible.
 * because this is not always possible and a distinction cannot be made on the error codes
 * -- unless we add some arbitrary new ones to errno.fugu -- an additional argument
 * indicates in case of errors, whether the unroll was complete.
 *
 * \param err_unroll_complete  reference to this mentioned variable
 *                             note: the caller can set it to NULL if not interested in this information
 */

// note: offset and rest are by ref -> direct adjustment of the actual values
//
static errval_t mm_add_helper(struct mm *mm, struct mmnode* parent, struct capref cap, int bit, size_t n, gensize_t current_size,
                              gensize_t *offset, gensize_t *rest, bool *err_unroll_complete) {
    // we should never add more than 2 new nodes simultaneously
    assert(n <= 2);
    if (err_unroll_complete) {
        *err_unroll_complete = false;
    }
    struct capref new_capref;
    errval_t err2; // used during error handling
    errval_t err = mm_slot_alloc(mm, (uint64_t)n, &new_capref);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_add_helper(): error during mm_slot_alloc().\n");
        goto err_unroll_finished;
    }

    err = cap_retype(new_capref, cap, *offset, mm->objtype, current_size, n);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_add_helper(): error during cap_retype().\n");
        goto err_unroll_slot_alloc;
    }

    for (size_t i = 0; i < n; i++) {
        struct frame_identity info;
        err = frame_identify(new_capref, &info);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "mm_add_helper(): error during frame_identify().\n");
            goto err_unroll_retype;
        }

        // this alloc will always succeed until really no memory anymore
        // keeping the slab with more than enough free slabs is part
        // of the MM invariant
        struct mmnode *node = (struct mmnode *)slab_alloc(&mm->slabs);
        if (node == NULL) {
            err = MM_ERR_NEW_NODE;
            DEBUG_ERR(err, "mm_add_helper(): no mmnode available in slab; invariant not holding.\n");
            if (i == 0) {
                goto err_unroll_retype;
            }
            // if i /= 0, we cannot really do any somewhat easy unrolling then
            // nevertheless, this error case should actually never happen (see invariant)
            return err;
        }
        node->type = NodeType_Free;
        node->cap = new_capref;
        node->base = info.base;
        node->size = info.bytes;
        node->parent = parent;

        insert_mmnode(&mm->buckets[bit], node);

        // add node to mapping table
        mm->map_table[MAPL1SLOT(node->base)][MAPL2SLOT(node->base)] = node;

        // adjust offset for next allocation
        *offset += current_size;
        *rest -= current_size;

        new_capref.slot++;
    }

    return SYS_ERR_OK;

    // unroll allocations in case of error
err_unroll_retype:
    err2 = cap_delete(new_capref);
    if (err_is_fail(err2)) {
        // should not happen; but if it happens, we cannot proceed with recycling slots
        // thus exit already here
        DEBUG_ERR(err2, "mm_add_helper(): cap_delete() error in error path.\n");
        err_push(err, err2);
        return err;
    }
    if (n == 2) {
        new_capref.slot++;
        err2 = cap_delete(new_capref);
        if (err_is_fail(err2)) {
            // should not happen; but if it happens, we cannot proceed with recycling slots
            // thus exit already here
            DEBUG_ERR(err2, "mm_add_helper(): cap_delete() error in error path.\n");
            err_push(err, err2);
            return err;
        }
        new_capref.slot--;
    }

err_unroll_slot_alloc:
    if (n == 2) {
        // a pair
        struct capref_pair_list_elem *pair = (struct capref_pair_list_elem *)slab_alloc(&mm->slabs);
        // note: should never be NULL (see invariant)
        if (pair == NULL) {
            err2 = MM_ERR_NEW_NODE;
            DEBUG_ERR(err2, "mm_add_helper(): no mmnode available in slab during error exit; invariant not holding.\n");
            err_push(err, err2);
        }
        else {
            pair->cap = new_capref;
            err2 = mm_slot_pair_free(mm, pair);
            if (err_is_fail(err2)) {
                DEBUG_ERR(err2, "mm_add_helper(): error in mm_slot_pair_free()\n");
                err_push(err, err2);
            }
        }
    }
    else if (n == 1) {
        err2 = mm_slot_single_free(mm, new_capref);
        if (err_is_fail(err2)) {
            DEBUG_ERR(err2, "mm_add_helper(): error in mm_slot_single_free()\n");
            err_push(err, err2);
        }
    }
    else {
        // see initial assertion on n
        assert(false);
    }

err_unroll_finished:
    if (err_unroll_complete) {
        *err_unroll_complete = true;
    }
    return err;
}

/**
 * Adds a capability to the memory manager.
 *
 * additionally splits memory in suitable buckets to have size == alignment
 * for given Memory size of 1 GB and provided 64 mmmnode slots to start with,
 * this method uses at most 32 mmnodes and capref slots.
 *
 * \param  cap  Capability to add
 * \param  base Physical base address of the capability
 * \param  size Size of the capability (in bytes)
 */
errval_t mm_add(struct mm *mm, struct capref cap, genpaddr_t base, size_t size)
{
    // simple algorithm that starts with low size/alignment and slices the provided
    // capref into proper blocks to achieve larger alignments

    gensize_t offset = 0;
    gensize_t rest = (gensize_t)size;
    gensize_t current_size = 1;
    genpaddr_t current_base = base;
    int bit = 0; // to keep it around after the loop
    for (bit = 0; bit < MM_BUCKETS_SIZE; bit++) {
        if (current_size > rest) {
            current_size /= 2;
            bit -= 1;
            break;
        }

        if (current_base & (1 << bit)) {
            // get a slice of current_size to adjust alignment to next power of 2
            errval_t err = mm_add_helper(mm, NULL, cap, bit, 1, current_size, &offset, &rest, NULL);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "mm_add(): error during mm_add_helper(); increasing, bit nr %d.\n", bit);
                // error path: nothing that could be unrolled here
                return err_push(err, MM_ERR_MM_ADD);
            }
        }

        // prepare next iteration
        current_base = base + offset;
        current_size += current_size;
    }

    // on the peak level: loop for adding more of the same max size if available
    gensize_t n = rest / current_size;
    for (gensize_t i = 0; i < n; i++) {
        errval_t err = mm_add_helper(mm, NULL, cap, bit, 1, current_size, &offset, &rest, NULL);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "mm_add(): error during mm_add_helper(); loop %" PRIuGENSIZE "/%" PRIuGENSIZE ".\n", i, n);
            // error path: nothing that could be unrolled here
            return err_push(err, MM_ERR_MM_ADD);
        }
    }

    // final decreasing sizes again
    while (rest >= MM_MIN_MEMORY_ALLOCATION && bit > 0) {
        bit -= 1;
        current_size /= 2;
        if (current_size <= rest) {
            errval_t err = mm_add_helper(mm, NULL, cap, bit, 1, current_size, &offset, &rest, NULL);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "mm_add(): error during mm_add_helper(); decreasing, bit nr %d.\n", bit);
                // error path: nothing that could be unrolled here
                return err_push(err, MM_ERR_MM_ADD);
            }
        }
    }

    // establish the invariants
    errval_t err = assure_invariants(mm);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_add(): error while assuring invariant assure_absolutely_needed_bucket_counts().\n");
        // error path: nothing that could be unrolled here
        return err_push(err, MM_ERR_MM_ADD);
    }

    mm->managed_mem += size;

#ifdef MM_OPTIONAL_DETAILED_DEBUG_INFO
    // during debugging
    debug_printf("mm_add(): successfully added %u B (%u KiB, %u MiB) memory to MM.\n", size, size / 1024, size / 1024 / 1024);
    mm_dump_mmnodes(mm);
#endif

    return SYS_ERR_OK;
}

// (return value == SYS_ERR_OK) implies bucket "bucket_nr" is filled with at least
// the requested block number of blocks
//
// important note: count should not be so large that the entire recursive function
// may need more buckets / mmnodes than providable by about a 95% filled slab or slot allocator
// typical count values are 1-4 to start with.
static errval_t check_and_fill_buckets(struct mm *mm, int bucket_nr, size_t count) {
    // bucket "bucket_nr" is < requested count -> recursively go look for non-empty buckets
    // and split

    if (bucket_nr >= MM_BUCKETS_SIZE) {
        return MM_ERR_OUT_OF_BOUNDS;
    }

    size_t available = list_size(&mm->buckets[bucket_nr]);

    while (available < count) {
        errval_t err = check_and_fill_buckets(mm, bucket_nr + 1, (count - available + 1) >> 1);
        if(err_is_fail(err)){
          DEBUG_ERR(err, "check_and_fill_buckets(): error with bucket_nr: %d (most probable cause: not enough memory).\n", bucket_nr + 1);
          // error path: nothing that could be unrolled here
          return err;
        }

        // take block from bucket "bucket_nr+1", split it and put blocks in bucket "bucket_nr"
        // note: the successful completion of the recursive call guarantees that this
        // parent bucket is not empty
        //gensize_t is uint64_t

        gensize_t current_bucket_blocksize = 1 << bucket_nr;
        gensize_t parent_bucket_blocksize = current_bucket_blocksize << 1;

        struct mmnode *parent = retrieve_first_node(&mm->buckets[bucket_nr + 1]);
        assert(parent);

        struct capref cap_parent = parent->cap;
        gensize_t offset = 0;
        gensize_t rest = parent_bucket_blocksize;

        // add 2 nodes to bucket
        bool err_unroll_complete;
        err = mm_add_helper(mm, parent, cap_parent, bucket_nr, 2, current_bucket_blocksize, &offset, &rest, &err_unroll_complete);
        if(err_is_fail(err)){
            if (err_unroll_complete) {
                DEBUG_ERR(err, "check_and_fill_buckets(): error with bucket_nr %d while calling mm_add_helper(): unroll was complete -> unrolling here.\n", bucket_nr);
                parent->type = NodeType_Free;
                insert_mmnode(&mm->buckets[bucket_nr + 1], parent);
            }
            else {
                DEBUG_ERR(err, "check_and_fill_buckets(): error with bucket_nr %d while calling mm_add_helper(): unroll was not complete -> no unroll here -> potential leak.\n", bucket_nr);
            }
            return err;
        }
        // mark original block as parent
        // note: the parent node has to be kept around (no slab_free!), since it is referenced by its children
        parent->type = NodeType_Parent;

        // update available count
        available = list_size(&mm->buckets[bucket_nr]);
    }

    return SYS_ERR_OK;
}

/**
 * returns bucket nr without boundary testing
 * note: input type is gensize_t == uint64_t since the function is once called
 * with gensize_t and once with size_t (32 bit)
 */
static size_t convert_size_to_bucket_nr(gensize_t size) {
    size_t bucket_nr;
    for (bucket_nr = MM_BUCKETS_SIZE - 1; bucket_nr > MM_MIN_BUCKET_INDEX; bucket_nr--){
        //check position of MSB
        if (size & (1 << bucket_nr)) {
            break;
        }
    }

    if ((1 << bucket_nr) < size) {
        bucket_nr++;
    }

    return bucket_nr;
}


/**
 * The following functions establish invariants in the state of the memory
 * manager, such that recursive invocations of mm_alloc_aligned do not lead
 * to an infinite recursion.
 * We distinguish between two different invariants:
 * 1) There are enough slots and slabs available to fully serve two arbitrary
 *    allocation requests. This is required, since during an allocation request
 *    we might need to double the size of the L1 CNode. Thus, both the original
 *    allocation request, as well as the recursive request induced by the need
 *    to grow the L1 CNode, must be served without allocating slots or slabs
 *    in order to prevent further recursion.
 * 2) There are a certain minimum amount of RAM blocks of certain sizes available,
 *    in order to serve recursive allocation request induced by slab or slot
 *    allocator refills without using any further slots and slabs to prevent
 *    deeper recursion.
 *
 * These invariants are reestablished after every non-recursive allocation
 * request. This is ensured by the flag mm->is_internal_alloc.
 *
 * We have 32 free list buckets (although not all of them are really used) and
 * two arbitrary consecutive allocation requests can trigger at most 31 block
 * splitting operations. Thus, we ensure that there are always at least 32 slot
 * pairs and 32 slabs available. Note that actually we would need to have 64
 * slabs available, since each split requires two slots and two slabs. However,
 * allocating a slot pair always frees a slab that is subsequently available
 * again. Therefore, when splitting a block, we just have to make sure, that we
 * allocate the two slots before the two slabs.
 *
 * During an allocation request, there can be recursive allocation request for the
 * following reasons:
 * 1) 4 KiB for a slab allocator refill
 * 2) 4 KiB for an ARM L2 page table needed to map a frame for a slab allocator refill
 * 3) 16 KiB for a new L2 CNode caused by a slot allocator refill
 * 4) An arbitrary request for growing the L1 CNode (this is covered by invariant 1)
 *
 * The above mentioned cases could possibly be triggered by a single allocation request.
 *
 * Therefore we need at least 4 blocks of 4 KiB to cover the case that 1 and 2 happens
 * in the same request. Two additional blocks are required since the orignal request
 * might be for 4 KiB and case 4 could again try to grow to 4 KiB.
 *
 * Furthermore, we need at least 3 blocks of 16 KiB to cover case 3. Again two additional
 * blocks are required in case the original request as well as case 4 require 16 KiB.
 */

#define SLOT_SLAB_MIN_COUNT 32

#define BUCKET_4KiB_BIT 12
#define BUCKET_4KiB_MIN_COUNT 4
#define BUCKET_16KiB_BIT 14
#define BUCKET_16KiB_MIN_COUNT 3

/**
 * Assures that there are at least SLOT_SLAB_MIN_COUNT slots and slabs
 * available in the respective allocators or allocator pools.
 */
static errval_t assure_slot_slab_alloc_invariant(struct mm *mm) {

    while (mm->free_slot_pair_list.size < SLOT_SLAB_MIN_COUNT) {
        struct capref_pair_list_elem *cap_pair = slab_alloc(&mm->slabs);
        if (!cap_pair) {
            return MM_ERR_NEW_NODE;
        }
        errval_t err = mm->slot_alloc(mm->slot_alloc_inst, 2, &cap_pair->cap);
        if (err_is_fail(err)) {
            errval_t err2 = mm->slot_refill(mm->slot_alloc_inst);
            if (err_is_fail(err2)) {
                DEBUG_ERR(err, "assure_slot_slab_alloc_invariant(): error during slot_alloc() induced slot_refill().\n");
                // error path: no unrolling here; not sure what has already done during refill
                return err_push(err, err2);
            }

            // re-try allocation
            err2 = mm->slot_alloc(mm->slot_alloc_inst, 2, &cap_pair->cap);
            if (err_is_fail(err2)) {
                DEBUG_ERR(err, "assure_slot_slab_alloc_invariant(): error during 2nd slot_alloc() after slot_alloc() induced slot_refill().\n");
                // error path: not sure what happened during slot_refill above
                return err_push(err, err2);
            }
        }
        mm_slot_pair_free(mm, cap_pair);
    }

    if (slab_freecount(&mm->slabs) < SLOT_SLAB_MIN_COUNT) {
        // manual trigger is allowed despite auto-refill functionality of slab
        // and with manual trigger, we keep full control about when the refill is happening,
        // i.e. now :-)
        errval_t err = mm->slabs.refill_func(&mm->slabs);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "assure_slot_slab_alloc_invariant() encountered an error during slab refill.\n");
            // error path: nothing to unroll / not sure what could if the arbitrary refill_func partially succeeded
            return err;
        }
    }

    return SYS_ERR_OK;
}

/**
 *  Assures that a certain amount of blocks in certain sizes are available without splitting (see documentation above).
 */
static errval_t assure_bucket_counts_invariant(struct mm *mm) {
    // note: smaller RAM blocks need to be allocated first.
    // otherwise the larger ones will be consumed.
    errval_t err = check_and_fill_buckets(mm, BUCKET_4KiB_BIT, BUCKET_4KiB_MIN_COUNT);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "assure_bucket_counts_invariant(): encountered an error while splitting memory.\n");
        // error path: nothing to unroll; check_and_fill_buckets unrolls if possible to have
        // only atomic results with respect to allocated memory mmnodes (if possible)
        return err;
    }

    err = check_and_fill_buckets(mm, BUCKET_16KiB_BIT, BUCKET_16KiB_MIN_COUNT);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "assure_bucket_counts_invariant(): encountered an error while splitting memory.\n");
        // error path: nothing to unroll; check_and_fill_buckets unrolls if possible to have
        // only atomic results with respect to allocated memory mmnodes (if possible)
        return err;
    }

    return SYS_ERR_OK;
}

/**
 *  Assures all invariants that are described above.
 */
static errval_t assure_invariants(struct mm *mm) {
    errval_t err;

    err = assure_slot_slab_alloc_invariant(mm);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "assure_invariants(): assure_slot_slab_alloc_invariant failed.\n");
        // error path: nothing to unroll
        return err;
    }

    err = assure_bucket_counts_invariant(mm);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "assure_invariants(): assure_bucket_counts_invariant failed.\n");
        // error path: nothing to unroll
        return err;
    }

    return SYS_ERR_OK;
}

/**
 * Allocate aligned physical memory.
 *
 * current alignment policy:
 * - for all requests: *required* size >= alignment
 *   i.e. alignment requests > size are *not* fulfilled in general
 *   exception: for size < BASE_PAGE_SIZE (4 KiB), an alignment of up to 4 KiB
 *   can be provided even for smaller size requests
 *   this is accomplished by bumping the size up to the alignment (i.e. max. 4 KiB in that case)
 *
 * for successful allocations:
 * - actual alignment >= size is guaranteed
 * - all sizes / alignments are power of 2
 *
 * \param       mm        The memory manager.
 * \param       size      How much memory to allocate.
 * \param       alignment The alignment requirement of the base address for your memory.
 * \param[out]  retcap    Capability for the allocated region.
 */
errval_t mm_alloc_aligned(struct mm *mm, size_t size, size_t alignment, struct capref *retcap)
{
    // DONE:Implement
#ifdef MM_OPTIONAL_DETAILED_DEBUG_INFO
    debug_printf("mm_alloc_aligned(): requested memory size %u B, alignment %u B.\n", size, alignment);
#endif

    if (alignment > size) {
        if (alignment <= BASE_PAGE_SIZE) {
            int bucket_nr = convert_size_to_bucket_nr( (gensize_t)alignment );
            size = 1 << bucket_nr;
            alignment = size;
            assert(size <= BASE_PAGE_SIZE);
        }
        else {
            DEBUG_ERR(MM_ERR_OUT_OF_BOUNDS, "Alignment cannot be larger than size!\n");
            return MM_ERR_OUT_OF_BOUNDS;
        }
    }

    int bucket_nr = convert_size_to_bucket_nr( (gensize_t)size );
    if (bucket_nr >= MM_BUCKETS_SIZE) {
        // error path: nothing to unroll
        return MM_ERR_OUT_OF_BOUNDS;
    }
    assert(bucket_nr >= MM_MIN_BUCKET_INDEX);

#ifdef MM_OPTIONAL_DETAILED_DEBUG_INFO
    debug_printf("deduced bucket nr %d with size %u KiB\n", bucket_nr, (1 << bucket_nr) / 1024);
#endif

    errval_t err;

    bool is_internal_alloc = mm->is_internal_alloc;
    mm->is_internal_alloc = true;
    err = check_and_fill_buckets(mm, bucket_nr, 1);
    mm->is_internal_alloc = is_internal_alloc;
    if(err_is_fail(err)){
        DEBUG_ERR(err, "mm_alloc_aligned(%zu KiB, %zu B): error check_and_fill_buckets() did not complete for nr %d (out of memory).", size / 1024, alignment, bucket_nr);
        DEBUG_STACK_TRACE_FROM_HERE("out of memory");
        // error path: nothing to unroll; check_and_fill_buckets unrolls if possible to have
        // only atomic results with respect to allocated memory mmnodes (if possible)
        return err;
    }

    // here, we have assurance that the bucket list is not empty
    // hand block out (first node)
    struct mmnode *node = retrieve_first_node(&mm->buckets[bucket_nr]);
    assert(node);
    *retcap = node->cap;
    // mark this node as allocated
    node->type = NodeType_Allocated;

    if (!mm->is_internal_alloc) {
        mm->is_internal_alloc = true;
        err = assure_invariants(mm);
        mm->is_internal_alloc = false;
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "mm_alloc_aligned(): error while assuring invariant by assure_invariants().\n");

            // here, we need to free the already allocated memory block
            // since the function is returning an error value
            // otherwise, there is a memory leak under max. memory stress test
            // see test case.
            errval_t err2 = mm_free(mm, *retcap, 0, 0);
            if (err_is_fail(err2)) {
                DEBUG_ERR(err2, "mm_alloc_aligned(): error while returning already allocated memory after failed assure_invariants().");
                err_push(err, err2);
            }
            *retcap = NULL_CAP;

            return err_push(err, MM_ERR_SLOT_MM_ALLOC); // failing in this invariant
        }
    }

#ifdef MM_OPTIONAL_DETAILED_DEBUG_INFO
    // during debugging
    debug_printf("mm_alloc_aligned(): successfully allocated memory.\n");
    mm_dump_mmnodes(mm);
#endif

    return SYS_ERR_OK;
}

/**
 * Allocate physical memory.
 *
 * \param       mm        The memory manager.
 * \param       size      How much memory to allocate.
 * \param[out]  retcap    Capability for the allocated region.
 */
errval_t mm_alloc(struct mm *mm, size_t size, struct capref *retcap)
{
    // DONE: Implement
    return mm_alloc_aligned(mm, size, 1, retcap);
}

// get the sibling (buddy) of a node
static inline struct mmnode* mm_sibling(struct mm *mm, const struct mmnode* node) {
    assert(mm);
    assert(node);
    genpaddr_t sibling_base = node->base ^ node->size;
    return mm->map_table[MAPL1SLOT(sibling_base)][MAPL2SLOT(sibling_base)];
}

// Try to merge node with its sibling to a new larger block. If the merge is successful,
// merge is called recursively on the newly merged node. As soon as merging is not
// possible anymore, the largest block produced in the process is inserted into the
// list of free nodes. Therefore, node must not be in the list of free blocks!
static errval_t mm_merge(struct mm *mm, struct mmnode* node, size_t bucket_nr) {
    assert(mm);
    assert(node);
    assert(bucket_nr < MM_BUCKETS_SIZE);

    // node must not be in the list of free blocks
    assert(!node->prev);
    assert(!node->next);

    errval_t err;
    if (!node->parent) {
        // root nodes cannot be merged

        node->type = NodeType_Free;
        insert_mmnode(&mm->buckets[bucket_nr], node);
        mm->map_table[MAPL1SLOT(node->base)][MAPL2SLOT(node->base)] = node;
        return SYS_ERR_OK;
    }
    struct mmnode* sibling_node = mm_sibling(mm, node);

    // every node with a parent has a sibling
    assert(sibling_node);
    // if node has a parent then sibling must also have a parent
    assert(sibling_node->parent);

    if (sibling_node->type != NodeType_Free) {
        // free nodes with a non-free sibling node cannot be merged

        node->type = NodeType_Free;
        insert_mmnode(&mm->buckets[bucket_nr], node);
        mm->map_table[MAPL1SLOT(node->base)][MAPL2SLOT(node->base)] = node;
        return SYS_ERR_OK;
    }

    if (node->parent != sibling_node->parent) {
        // if the sibling has a different parent, then the sibling is
        // really a descendant of the actual sibling
        assert(node->parent->size != sibling_node->parent->size);

        node->type = NodeType_Free;
        insert_mmnode(&mm->buckets[bucket_nr], node);
        mm->map_table[MAPL1SLOT(node->base)][MAPL2SLOT(node->base)] = node;
        return SYS_ERR_OK;
    }

    // nodes with the same parent must have the same size
    assert(node->size == sibling_node->size);
    // sibling nodes must have adjacent CNode slots
    assert(node->cap.slot + 1 == sibling_node->cap.slot || node->cap.slot == sibling_node->cap.slot + 1);

    struct mmnode* parent = node->parent;
    // parent node must have parent type
    assert(parent->type == NodeType_Parent);
    // parent size must be twice as large
    assert(parent->size == 2*node->size);

    // remove sibling from free list
    remove_mmnode(&mm->buckets[bucket_nr], sibling_node);

    //TODO jmeier: discuss the order of the remainder of the function with
    //             regard to error cases, as well as potential clean up strategies

    // revoking a capability does not work currently (not implemented)
    /*err = cap_revoke(node->cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_merge(): could not revoke node capability.\n");
        return err;
    }*/
    err = cap_delete(node->cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_merge(): could not delete node capability.\n");
        // error path: nothing we can do here
        return err;
    }

    // revoking a capability does not work currently (not implemented)
    /*err = cap_revoke(sibling_node->cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_merge(): could not revoke sibling node capability.\n");
        return err;
    }*/
    err = cap_delete(sibling_node->cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "mm_merge(): could not delete sibling node capability.\n");
        // error path: nothing we can do here
        return err;
    }

    // remove the nodes from the mapping table and free them
    mm->map_table[MAPL1SLOT(node->base)][MAPL2SLOT(node->base)] = NULL;
    mm->map_table[MAPL1SLOT(sibling_node->base)][MAPL2SLOT(sibling_node->base)] = NULL;
    mm_mmnode_pair_free(mm, node, sibling_node);

    // recursively call merge on the new block
    return mm_merge(mm, parent, bucket_nr + 1);
}


/**
 * Free a certain region (for later re-use).
 *
 * base and size are ignored (checked with AOS support by mail).
 * base address is deduced from capref to check / find mmnode in allocated buckets
 * if allocated tracking is on (default).
 *
 * \param       mm        The memory manager.
 * \param       cap       The capability to free.
 * \param       base      The physical base address of the region. !!! ignored !!!
 * \param       size      The size of the region.                  !!! ignored !!!
 */
errval_t mm_free(struct mm *mm, struct capref cap, genpaddr_t base /* ignored */, gensize_t size /* ignored */)
{
    // DONE: Implement

    // ignore base and size arguments
    // get infos out of capref
    struct frame_identity info;
    errval_t err = frame_identify(cap, &info);
    if (err_is_fail(err)) {
         DEBUG_ERR(err, "mm_free(): error during frame_identify().\n");
         // error path: nothing to unroll
         return err_push(err, MM_ERR_MM_FREE);
    }

    struct mmnode* node = mm->map_table[MAPL1SLOT(info.base)][MAPL2SLOT(info.base)];
    if (!node) {
        err = MM_ERR_MM_FREE;
        DEBUG_ERR(err, "mm_free(): no corresponding node found.\n");
        // error path: nothing to unroll
        return err;
    }
    // the meta data of the node in the map table has to match the RAM block in cap
    assert(node->base == info.base);
    assert(node->size == info.bytes);

    // can only free allocated nodes
    if (node->type != NodeType_Allocated) {
        err = MM_ERR_MM_FREE;
        DEBUG_ERR(err, "mm_free(): corresponding node is not marked allocated.\n");
        // error path: nothing to unroll
        return err;
    }

    // convert size to bucket index
    size_t bucket_nr = convert_size_to_bucket_nr(node->size);
    if (bucket_nr >= MM_BUCKETS_SIZE) {
        err = MM_ERR_MM_FREE;
        DEBUG_ERR(err, "mm_free(): error during convert_size_to_bucket_nr().\n");
        // error path: nothing to unroll
        return err;
    }

    // recursively merge node to the largest possible block and insert into free list
    mm_merge(mm, node, bucket_nr);

#ifdef MM_OPTIONAL_DETAILED_DEBUG_INFO
    // at least during debug
    debug_printf("mm_free(): memory with address %"PRIxGENPADDR " and size %"
                 PRIuGENSIZE " KiB successfully moved from allocated to free memory.\n", node->cap.base, node->cap.size / 1024);
    mm_dump_mmnodes(mm);
#endif

    return SYS_ERR_OK;
}
