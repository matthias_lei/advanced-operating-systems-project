/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-10-24, Group C
 */

/**
 * device server specific functions.
 * version 2017-10-24, Group C
 */

#include <aos_rpc_server_device_p.h>
#include <aos/caddr.h>

// add specific includes here


//could not find DEVICES_BASE_ADDR in maps/omap44xx_map.h
#define DEVICES_BASE_ADDR 0x40000000

struct aos_rpc_interface_device_vtable aos_rpc_device_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .get_device_cap = aos_rpc_local_get_device_cap,
    .get_irq_cap = aos_rpc_local_get_irq_cap
};

// internal
void deviceserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;
    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_DEVICE_FID_GET_DEVICE_CAP: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_get_device_cap_req_payload));

            struct aos_rpc_get_device_cap_req_payload *req_payload = context->recv_env.buf;

            err = aos_rpc_local_get_device_cap(NULL, req_payload->paddr, req_payload->bytes, &context->send_env.cap);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            send_reply(state, err);
            return;
        }

        case AOS_RPC_DEVICE_FID_GET_IRQ_CAP: {
            assert(context->recv_env.buflen == 0);

            err = aos_rpc_local_get_irq_cap(NULL, &context->send_env.cap);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            send_reply(state, err);
            return;            
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}

errval_t aos_rpc_local_get_device_cap(struct aos_rpc *rpc, lpaddr_t paddr, size_t bytes, struct capref *frame)
{
    assert(frame);

    errval_t err = slot_alloc(frame);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slot_alloc failed\n");
        return err;
    }

    err = cap_retype(*frame, cap_devices, paddr - (gensize_t)DEVICES_BASE_ADDR, ObjType_DevFrame, bytes, 1);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "cap_retype\n");
        return err;
    }

    assert(!capref_is_null(*frame));

    return SYS_ERR_OK;
}

errval_t aos_rpc_local_get_irq_cap(struct aos_rpc *rpc, struct capref *cap) {
    *cap = cap_irq;
    return SYS_ERR_OK;
}


// public
// accepts deviceserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    deviceserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_deviceserver_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_deviceserver_event_handler);
}
