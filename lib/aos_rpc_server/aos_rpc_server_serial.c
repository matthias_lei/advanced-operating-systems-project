/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-10-24, Group C
 */

/**
 * serial / terminal IO server specific functions.
 * version 2017-10-24, Group C
 */

#include <aos_rpc_server_serial_p.h>

// add specific includes here

// local implementations used by Hermes when itself wants to issue printfs

static errval_t local_putchar(struct aos_rpc *rpc, char c) {
    return sys_print(&c, 1);
}

static errval_t local_putstring(struct aos_rpc *rpc, const char *s, size_t len) {
    return sys_print(s, len);
}

struct aos_rpc_interface_serial_vtable aos_rpc_serial_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .get_char = NULL,
    .put_char = local_putchar,
    .put_string = local_putstring,
    .get_window = NULL
};

// internal
// terminal I/O

/**
 *  Window list: a doubly-linked cyclic list
**/

struct window_data_list_header *channels;
struct window_data *current;

// list manipulation code

// insert
// assume that new_elem not present in list
static void insert_channel(struct window_data_list_header *l, struct window_data *new_elem) {
    assert(l);
    assert(new_elem);

    assert(!new_elem->prev);
    assert(!new_elem->next);

    // point to itself
    new_elem->next = new_elem;
    new_elem->prev = new_elem;

    if (l->head) {
        new_elem->next = l->head;
        new_elem->prev = l->head->prev;

        l->head->prev->next = new_elem;
        l->head->prev = new_elem;
    }

    l->head = new_elem;

    l->size++;
    return;
}

static struct window_data* get_channel(struct window_data_list_header *l, domainid_t pid) {
    assert(l);

    if (l->size == 0) {
        return NULL;
    }

    if (l->head->domain_id == pid) {
        return l->head;
    }

    struct window_data *current_elem = l->head->next;

    while (current_elem != l->head) {
        if (current_elem->domain_id == pid) {
            return current_elem;
        }
        current_elem = current_elem->next;
    }

    return NULL;
}

// removes window 'elem' from list
// assumes elem is part of the list
// elem is not freed
__attribute__((unused))
static void remove_channel(struct window_data_list_header *l, struct window_data *elem) {
    assert(l);
    assert(elem);
    assert(elem->prev);
    assert(elem->next);

    if (l->size == 1) {
        elem->next = NULL;
        elem->prev = NULL;
        l->head = NULL;
        l->size = 0;
        return;
    }

    if (l->head == elem) {
        l->head = elem->next;
    }

    elem->prev->next = elem->next;
    elem->next->prev = elem->prev;

    elem->prev = NULL;
    elem->next = NULL;

    l->size--;

    return;

}

// may return NULL
static struct window_data *init_window_data(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);
    assert(state->context->recv_env.buf);

    struct aos_rpc_serial_common_req_payload *payload = state->context->recv_env.buf;

    struct window_data *wd = calloc(1, sizeof(*wd));
    if (!wd) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "calloc() failed.\n");
        return NULL;
    }

    wd->domain_id = payload->domain_id;
    errval_t err = aos_rpc_process_get_name(aos_rpc_get_process_channel(), payload->domain_id, &wd->domain_name);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failed to get domain name\n");
        return NULL;
    }
    wd->current_line = 0;
    wd->current_column = 0;

    wd->pending = false;
    wd->state = state;
    wd->lines_used = 0;

    wd->write_state_machine.state = NORMAL;
    wd->write_state_machine.index = 0;

    return wd;
}

#define RESET "\033[0m"

// 0: no attributes
// 39: default foreground color
// 49: default background color
// static uint8_t default_colors[3] = {0, 39, 49};

static inline void output_color(struct character c) {
    // handle single value cases, e.g. RED \033[31m
    if (c.color[1] == 0 && c.color[2] == 0) {
        printf("\033[%dm", c.color[0]);
    }
    else if(c.color[2] == 0) {
        printf("\033[%d;%dm", c.color[0], c.color[1]);
    }
    else {
        printf("\033[%d;%d;%dm", c.color[0], c.color[1], c.color[2]);
    }
}

static inline void print_character(struct window_data *wd, struct character c) {
    if (c.color_valid) {
        // compose color
        output_color(c);
    }
    if (c.c) {
        putchar(c.c);
    }
}

static inline void print_line(struct window_data *wd, struct character *line) {
    for (size_t i = 0; i <= N_COLUMNS; i++) {
        print_character(wd, line[i]);
    }
}

// prints out the whole content of out_buffer
static void write_out_current_buffer(void) {
    // write out buffer
    uint32_t i = (current->lines_used == N_LINES) ? ((current->current_line + 1) % N_LINES) : 0;
    uint32_t last = (current->lines_used == N_LINES) ? current->current_line : current->lines_used;
    while (i % N_LINES != last ) {
        print_line(current, (struct character*)current->out_buffer[i]);

        putchar('\n');

        i = (i + 1) % N_LINES;
    }

    // last line
    print_line(current, (struct character*)current->out_buffer[i]);
}

#define MAGENTA_BOLD  "\033[1;35m"
#define LINE "----------------------------------------------------------------------------------------------------"

static void switch_window_specific(struct window_data *new_window) {
    printf("\n" MAGENTA_BOLD LINE RESET "\n"
            MAGENTA_BOLD "  Window switch from domain %s to domain %s" RESET "\n"
            MAGENTA_BOLD LINE RESET "\n",
            current->domain_name, new_window->domain_name);

    current = new_window;

    write_out_current_buffer();

    fflush(stdout);
}

static void switch_window_in_direction(int direction) {

    if (channels->size <= 1) {
        return;
    }

    struct window_data *new_window;
    if (direction >= 0) {
        new_window = current->next;
    }
    else {
        new_window = current->prev;
    }

    switch_window_specific(new_window);

}

static void send_char_to_window(struct window_data *wd, char c) {

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_send_msg_env send_env;
    send_env.header = AOS_RPC_ADD_ERROR(AOS_RPC_SERIAL_FID_GETCHAR, err);
    send_env.cap = NULL_CAP;
    send_env.buf = &c;
    send_env.buflen = sizeof(c);

    // directly send char to blocking process
    err = aos_rpc_generic_send(wd->state->chan, &send_env);
    if (err_is_fail(err)) {
        // we do nothing except logging the error
        DEBUG_ERR(err, "aos_rpc_generic_send failed\n");
    }
}

enum KEYS {
    CR      = 13,
    CTRL_X  = 24,
    CTRL_Y  = 25,
    ESC     = 27
};

errval_t handle_input_characters(void) {
    errval_t err = SYS_ERR_OK;

    char c = 0;
    err = sys_getchar(&c);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "sys_getchar failed.\n");
        return err;
    }

    if (c != 0){

        if (c == CTRL_X) {
            // window switch to the right
            assert(current);
            switch_window_in_direction(1);
        }
        else if (c == CTRL_Y) {
            // window switch to the left
            assert(current);
            switch_window_in_direction(-1);
        }
        else {
            // current process called get_char() and is blocking
            // --> send it the input char
            if (current->pending) {
                current->pending = false;
                send_char_to_window(current, c);
            }
            else {
                // fill next char into buffer
                if ((current->in + 1) % IN_BUF_LEN == current->out) {
                    //we would overwrite data, don't write
                    debug_printf("Input buffer is full. Discarded char.\n");
                }
                else {
                    current->in_buffer[current->in] = c;
                    current->in = (current->in + 1) % IN_BUF_LEN;
                }
            }

        }
    }
    return SYS_ERR_OK;
}


static errval_t internal_getchar(struct aos_rpc_server_comm_state *state, struct aos_rpc_serial_getchar_reply_payload *reply_payload) {
    assert(state);
    assert(state->server_persistent_data);
    assert(reply_payload);

    errval_t err = SYS_ERR_OK;

    struct window_data *wd = (struct window_data*) state->server_persistent_data;

    // check if we have something in buffer
    // != is fine as wd->out is never incremented somewhere else and wd->in is checked to not overrun wd->out
    if (wd->out != wd->in) {
        reply_payload->c = wd->in_buffer[wd->out];
        wd->out = (wd->out + 1) % IN_BUF_LEN;
    }
    else {
        // no char available in buffer
        // -> set pending flag and block until char available
        assert(!wd->pending);
        wd->pending = true;
    }

    return err;
}


static void add_char_to_out_buffer(struct window_data *wd, char c) {
    // go to next line on observing '\n'
    if (c == '\n') {
        struct character *cur_char = &wd->out_buffer[wd->current_line][wd->current_column];
        cur_char->c = '\0';
        cur_char->color_valid = wd->next_char_has_color;
        for (size_t i = 0; i < 3; i++) {
            cur_char->color[i] = wd->write_state_machine.values[i];
        }

        // next char only colored if another color is observed
        wd->next_char_has_color = false;
        memset(&wd->out_buffer[wd->current_line][wd->current_column + 1],
                0,
                (N_COLUMNS - wd->current_column) * sizeof(struct character));
        // out buffer allows overwriting
        wd->current_line = (wd->current_line + 1) % N_LINES;
        wd->lines_used = (wd->lines_used == N_LINES) ? N_LINES : wd->lines_used + 1;
        wd->current_column = 0;
        memset(&wd->out_buffer[wd->current_line][wd->current_column],
                0,
                (N_COLUMNS + 1) * sizeof(struct character));
    }
    else if (c == CR) {
        wd->current_column = 0;
    }
    // otherwise write char to buffer and take care not to write over line border
    else {
        struct character *cur_char = &wd->out_buffer[wd->current_line][wd->current_column];
        cur_char->c = c;
        cur_char->color_valid = wd->next_char_has_color;
        for (size_t i = 0; i < 3; i++) {
            cur_char->color[i] = wd->write_state_machine.values[i];
        }

        // next char only colored if another color is observed
        wd->next_char_has_color = false;

        if (wd->current_column < N_COLUMNS - 1) {
            wd->current_column = wd->current_column + 1;
        }
        else {
            // out buffer allows overwriting
            wd->current_line = (wd->current_line + 1) % N_LINES;
            wd->lines_used = (wd->lines_used == N_LINES) ? N_LINES : wd->lines_used + 1;
            wd->current_column = 0;
            memset(&wd->out_buffer[wd->current_line][wd->current_column],
                    0,
                    (N_COLUMNS + 1) * sizeof(struct character));
        }
    }
}

static inline void rollback_value(struct window_data *wd, uint8_t val) {
    uint8_t i = 0;
    char d[3];
    while (val != 0) {
        d[i] = '0' + (val % 10);
        val -= val % 10;
        val /= 10;
        i++;
    }
    for (int j = i - 1; j >= 0; j--) {
        add_char_to_out_buffer(wd, d[j]);
    }

    return;
}

static inline void rollback_value_list(struct window_data *wd, uint8_t *l, uint8_t ind) {
    for (uint8_t i = 0; i < ind; i++) {
        rollback_value(wd, l[i]);
        add_char_to_out_buffer(wd, ';');
    }
    rollback_value(wd, l[ind]);

    return;
}

static inline void handle_read_value_state(struct window_data *wd, bool *buffer_char, char c) {
    assert(wd);
    assert(buffer_char);
    assert(wd->write_state_machine.state == READ_VALUE);
    assert(wd->write_state_machine.index == 0);

    // short definitions
    uint8_t *index = &wd->write_state_machine.index;
    uint8_t *value = &wd->write_state_machine.values[*index];
    enum window_write_state *state = &wd->write_state_machine.state;

    switch (c) {
        // cursor forward
        case 'C':
            wd->current_column += *value;
            assert(wd->current_column < N_COLUMNS);
            *buffer_char = false;
            *state = NORMAL;
            break;
        // erase line
        case 'K':
            if (*value == 0) {
                memset(&wd->out_buffer[wd->current_line][wd->current_column],
                                        0,
                                        N_COLUMNS + 1 - wd->current_column);
                *buffer_char = false;
            }
            else {
                debug_printf("Unknown escape sequence in READ_VALUE state (case K).\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                rollback_value(wd, *value);
            }
            *state = NORMAL;
            break;
        // color identifier
        case 'm':
            // valid color
            if (*index == 0) {
                *buffer_char = false;
                // set flag that color was observed
                wd->next_char_has_color = true;
            }
            else {
                debug_printf("Unknown escape sequence in READ_VALUE state (case m).\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                rollback_value(wd, wd->write_state_machine.values[0]);
            }
            *state = NORMAL;
            break;
        // value list
        case ';':
            *index = 1;
            value = &wd->write_state_machine.values[*index];
            *value = 0;
            *buffer_char = false;
            *state = VALUE_LIST;
            break;
        // parse current value further
        default:
            if ('0' <= c && c <= '9') {
                *value *= 10;
                *value += c - '0';
                *buffer_char = false;
            }
            else {
                debug_printf("Unknown escape sequence in READ_VALUE state (default case).\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                rollback_value(wd, wd->write_state_machine.values[*index]);
                *state = NORMAL;
            }
            break;
    }

    return;
}

static inline void handle_value_list_state(struct window_data *wd, bool *buffer_char, char c) {
    assert(wd);
    assert(buffer_char);
    assert(wd->write_state_machine.state == VALUE_LIST);
    assert(wd->write_state_machine.index >= 1);

    // short definitions
    uint8_t *index = &wd->write_state_machine.index;
    uint8_t *value = &wd->write_state_machine.values[*index];
    enum window_write_state *state = &wd->write_state_machine.state;

    switch (c) {
        // list continues further
        case ';':
            if (*index < 2) {
                wd->write_state_machine.values[*index] = *value;
                (*index)++;
                value = &wd->write_state_machine.values[*index];
                *value = 0;
                *buffer_char = false;
            }
            else {
                debug_printf("Unknown escape sequence in VALUE_LIST state (case ;).\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                rollback_value_list(wd, wd->write_state_machine.values, *index);
                *state = NORMAL;
            }
            break;
        // color identifier
        case 'm':
            // valid color
            if (*index <= 2) {
                wd->write_state_machine.values[*index] = *value;
                *buffer_char = false;
                // set flag that color was observed
                wd->next_char_has_color = true;
            }
            // invalid color
            else {
                debug_printf("Unknown escape sequence in VALUE_LIST state (case m).\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                rollback_value_list(wd, wd->write_state_machine.values, *index);
                debug_printf("value %d: %d\n", *index, wd->write_state_machine.values[*index]);
            }
            *state = NORMAL;
            break;
        // parse current value further
        default:
            if ('0' <= c && c <= '9') {
                *value *= 10;
                *value += c - '0';
                *buffer_char = false;
            }
            else {
                debug_printf("Unknown escape sequence in VALUE_LIST state (default case).\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                rollback_value_list(wd, wd->write_state_machine.values, *index);
                *state = NORMAL;
            }
            break;
    }

    return;
}

static errval_t internal_putchar(struct window_data *wd, char c) {
    assert(wd);

    errval_t err = SYS_ERR_OK;

    assert(wd->current_line < N_LINES);
    assert(wd->current_column < N_COLUMNS);


    // state machine to filter out ANSI escape sequences and
    // perform required adjustments in buffer

    bool buffer_char = true;

    uint8_t *index = &wd->write_state_machine.index;
    uint8_t *values = wd->write_state_machine.values;
    enum window_write_state *state = &wd->write_state_machine.state;

    switch (*state) {
        case NORMAL:
            if (c == ESC) {
                buffer_char = false;
                *state = EXPECT_ESC;
            }
            break;
        case EXPECT_ESC:
            if (c == '[') {
                buffer_char = false;
                *state = ESC_STATE;
            }
            else {
                // rollback
                add_char_to_out_buffer(wd, ESC);
                *state = NORMAL;
            }
            break;
        case ESC_STATE:
            if ('0' <= c && c <= '9') {
                buffer_char = false;
                *index = 0;
                values[0] = c - '0';
                values[1] = values[2] = 0;
                *state = READ_VALUE;
            }
            else {
                debug_printf("Unknown escape sequence in ESC state.\n");
                // rollback
                add_char_to_out_buffer(wd, ESC);
                add_char_to_out_buffer(wd, '[');
                *state = NORMAL;
            }
            break;
        case READ_VALUE:
            handle_read_value_state(wd, &buffer_char, c);
            break;
        case VALUE_LIST:
            handle_value_list_state(wd, &buffer_char, c);
            break;
        default:
            break;
    }

    if (buffer_char) {
        add_char_to_out_buffer(wd, c);
    }

    // additionally print chars in current I/O window
    if (wd == current) {
        err = local_putchar(NULL, c);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "local_putchar failed.\n");
            return err;
        }
    }

    return SYS_ERR_OK;
}

static errval_t internal_putstring(struct window_data *wd, char* s, size_t len) {
    assert(wd);
    assert(s);

    errval_t err = SYS_ERR_OK;

    for (int i = 0; i < len; i++) {
        err = internal_putchar(wd, s[i]);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "internal_putchar() failed.\n");
            return err;
        }
    }

    return err;
}

static void reset_buffers(struct window_data *wd) {
    assert(wd);

    // reset in buffer pointers
    wd->out = 0;
    wd->in = 0;
    wd->pending = false;

    // reset input buffer
    memset(wd->in_buffer, 0, IN_BUF_LEN);

    // reset next line
    wd->current_line = (wd->current_line + 1) % N_LINES;
    memset(wd->out_buffer[wd->current_line], 0, (N_COLUMNS + 1) * sizeof(struct character));
    wd->current_column = 0;

    return;
}

static errval_t internal_get_window(struct aos_rpc_server_comm_state *state, domainid_t window_id) {
    struct window_data *wd = get_channel(channels, window_id);
    if (wd == NULL) {
        // TODO find better error message
        errval_t err = TERM_ERR_NOT_PART_OF_SESSION;
        DEBUG_ERR(err, "window with id %d is not available.\n", window_id);
        return err;
    }

    // reset buffers
    reset_buffers(wd);
    // set state, s.t. when blocking, chars are sent to this process
    wd->state = state;

    state->server_persistent_data = wd;

    return SYS_ERR_OK;
}

static errval_t init_window(struct aos_rpc_server_comm_state *state, domainid_t pid) {
    errval_t err = SYS_ERR_OK;

    // initialize list if not yet happened
    if (!channels) {
        channels = calloc(1, sizeof(*channels));
        if (!channels) {
            err = LIB_ERR_MALLOC_FAIL;
            DEBUG_ERR(err, "channels calloc() failed\n");
            return err;
        }

        channels->size = 0;
    }

    struct window_data *wd = NULL;

    wd = init_window_data(state);
    insert_channel(channels, wd);

    state->server_persistent_data = wd;

    if (current == NULL) {
        current = wd;
    }
    else {
        switch_window_specific(wd);
    }

    return SYS_ERR_OK;
}

void serialserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_SERIAL_FID_GETCHAR: {
            struct aos_rpc_serial_getchar_reply_payload payload;
            struct window_data *wd = state->server_persistent_data;

            // have to initialize window first
            if (wd == NULL) {
                struct aos_rpc_serial_getchar_req_payload *req = context->recv_env.buf;
                err = init_window(state, req->domain_id);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "init_window() failed.\n");
                    send_reply(state, err);
                    return;
                }
                wd = state->server_persistent_data;
            }

            err = internal_getchar(state, &payload);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "internal_getchar() failed.\n");
                send_reply(state, err);
                return;
            }

            // do not send a reply if no char was currently available
            if (wd->pending) {
                return;
            }

            context->send_env.buf = &payload.c;
            context->send_env.buflen = sizeof(payload.c);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_SERIAL_FID_PUTCHAR: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_serial_putchar_req_payload));
            struct aos_rpc_serial_putchar_req_payload *payload = context->recv_env.buf;
            struct window_data *wd = state->server_persistent_data;

            // have to initialize window first
            if (wd == NULL) {
                err = init_window(state, payload->domain_id);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "init_window() failed.\n");
                    send_reply(state, err);
                    return;
                }
                wd = state->server_persistent_data;
            }

            err = internal_putchar(wd, payload->c);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "internal_putchar failed.\n");
                send_reply(state, err);
                return;
            }

            // all OK
            send_reply(state, err);
            return;
        }

        case AOS_RPC_SERIAL_FID_PUTSTRING: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_serial_putstring_req_payload));
            struct aos_rpc_serial_putstring_req_payload *payload = context->recv_env.buf;
            struct window_data *wd = state->server_persistent_data;

            // have to initialize window first
            if (wd == NULL) {
                err = init_window(state, payload->domain_id);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "init_window() failed.\n");
                    send_reply(state, err);
                    return;
                }
                wd = state->server_persistent_data;
            }

            err = internal_putstring(wd, payload->s, payload->len);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "internal_putstring failed.\n");
                send_reply(state, err);
                return;
            }

            // all OK
            send_reply(state, err);
            return;
        }
        case AOS_RPC_SERIAL_FID_GET_WINDOW: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_serial_get_window_req_payload));
            struct aos_rpc_serial_get_window_req_payload *payload = context->recv_env.buf;

            err = internal_get_window(state, payload->window_id);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "internal_get_window() failed.\n");
                send_reply(state, err);
                return;
            }

            // all OK
            send_reply(state, err);
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}




// public
// terminal I/O
// accepts serial I/O (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    serialserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_serialserver_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_serialserver_event_handler);
}
