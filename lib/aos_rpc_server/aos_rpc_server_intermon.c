/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-11-24, Group C
 */

/**
 * intermon server specific functions.
 * version 2017-11-24, Group C
 */

#include <aos/aos_rpc.h>
#include <aos_rpc_server_monitor_p.h>
#include <aos_rpc_server_intermon_p.h>
#include <aos/rpc_shared/aos_rpc_shared_name.h>

#include <aos/deferred.h>

#include <spawn/spawn.h>

// add specific includes here

// local intermon vtable is not used at the moment.
struct aos_rpc_interface_intermon_vtable aos_rpc_intermon_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .request_bootstrap_info = NULL, /* deliberately no local version added */
    .forward_rpc_message = NULL     /* deliberately no local version added */
};


errval_t aos_rpc_intermon_forge_capability(enum aos_rpc_intermon_capability_type cap_type,
                                           genpaddr_t cap_base, gensize_t cap_size,
                                           struct capref *ret_cap)
{
    assert(ret_cap);

    errval_t err = SYS_ERR_OK;
    *ret_cap = NULL_CAP;

    if (cap_base == 0 || cap_size == 0) {
        err = LIB_ERR_SHOULD_NOT_GET_HERE;
        DEBUG_ERR(err, "did nor receive expected capability\n");
        return err;
    }

    coreid_t core_id = disp_get_core_id();
    struct capref cap;
    err = slot_alloc(&cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slot alloc failed\n");
        return err;
    }

    switch (cap_type) {
        case AOS_RPC_INTERMON_CAPABILITY_RAM:
            err = ram_forge(cap, cap_base, cap_size, core_id);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "ram_forge failed\n");
                return err;
            }
            break;

        case AOS_RPC_INTERMON_CAPABILITY_FRAME:
        case AOS_RPC_INTERMON_CAPABILITY_DEVFRAME:
            err = frame_forge(cap, cap_base, cap_size, core_id);
            if (err_is_fail(err)) {

                return err;
            }
            break;

        default:
            err = LIB_ERR_NOT_IMPLEMENTED;
            DEBUG_ERR(err, "capability type %zu cannot be handled.\n", (size_t)cap_type);
            return err;
    }

    *ret_cap = cap;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_intermon_request_bootstrap_info(struct aos_rpc *rpc /*unused*/, coreid_t core_id, struct aos_intermon_bootstrap_res *info)
{
    assert(info);

    errval_t err = SYS_ERR_OK;

    // name service
    struct aos_rpc *name_rpc = aos_rpc_get_name_channel();
    if (!name_rpc) {
        return LIB_ERR_MALLOC_FAIL;
    }

    struct capref name_cap;
    err = aos_rpc_request_new_endpoint(name_rpc, AOS_RPC_CHAN_DRIVER_UMP, &name_cap);
    struct frame_identity name_info;
    err = frame_identify(name_cap, &name_info);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_identify for name RPC frame failed\n");
        return err;
    }

    info->name_base = name_info.base;
    info->name_size = name_info.bytes;

    // bootinfo
    struct frame_identity bootinfo_identity;
    err = frame_identify(cap_bootinfo, &bootinfo_identity);
    if (err_is_fail(err)) {
        debug_printf("frame_identify for bootinfo failed\n");
        return err;
    }

    info->bootinfo_base = bootinfo_identity.base;
    info->bootinfo_size = bootinfo_identity.bytes;

    // mmstrings
    struct capref mmstrings_cap = {
        .cnode = cnode_module,
        .slot = 0
    };

    struct frame_identity mmstrings_identity;
    err = frame_identify(mmstrings_cap, &mmstrings_identity);
    if (err_is_fail(err)) {
        debug_printf("frame_identify for mmstrings_cap failed\n");
        return err;
    }

    info->mmstrings_base = mmstrings_identity.base;
    info->mmstrings_size = mmstrings_identity.bytes;

    return SYS_ERR_OK;
}

errval_t aos_rpc_local_intermon_forward_rpc_message(struct aos_rpc *rpc /* unused */, struct aos_rpc_intermon_forward_req_payload *send,
                                                    struct aos_rpc_intermon_forward_res_payload *recv)
{
    assert(send);
    assert(recv);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc *r = NULL;
    switch (send->interface) {
        case AOS_RPC_NAME_FG:
        case AOS_RPC_MEMORY_FG:
        case AOS_RPC_PROCESS_FG:
        case AOS_RPC_SERIAL_FG:
        case AOS_RPC_DEVICE_FG:
        case AOS_RPC_FILESYSTEM_FG:
        case AOS_RPC_NETWORK_FG:
        case AOS_RPC_BLOCKDEV_FG:
            r = monitor_service_rpcs[send->interface];
            break;

        case AOS_RPC_SHELL_FG:
            if (send->enumerator >= monitor_shell_rpcs_max) {
                err = LIB_ERR_NOT_IMPLEMENTED;
                DEBUG_ERR(err, "Requested shell instance %zu out of current range. Max. %zu shells allowed at the moment.\n", send->enumerator, monitor_shell_rpcs_max);
                return err;
            }

            r = monitor_shell_rpcs[send->enumerator];
            break;

        case AOS_RPC_COMMON_FG:
        case AOS_RPC_MONITOR_FG:
        case AOS_RPC_INTERMON_FG:
            debug_printf("%s: FG %zu is NOT allowed.\n", __func__, (size_t)send->interface);
            return LIB_ERR_NOT_IMPLEMENTED;

        default:
            debug_printf("%s: UNKNOWN FG %zu is NOT allowed.\n", __func__, (size_t)send->interface);
            return LIB_ERR_NOT_IMPLEMENTED;
    }

    if (!r) {
        debug_printf("%s: RPC channel for FG %zu could not be resolved.\n", __func__, (size_t)send->interface);
        return LIB_ERR_NOT_IMPLEMENTED;
    }

    enum aos_rpc_intermon_capability_type recv_cap_type = send->recv_cap_type;
    struct aos_rpc_send_msg_env *send_env = (void *)send->env;
    send_env->buf = (char *)send->env + sizeof(struct aos_rpc_send_msg_env);

    // no mallocs allowed: use a default buffer
    // note: must be uintptr_t aligned
    uintptr_t memory_buffer_res[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     send_env->buf, send_env->buflen, send->send_cap_type,
                                     memory_buffer_res, DEFAULT_RPC_BUFFER_SIZE, send->recv_cap_type);

    if (send->send_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
        err = aos_rpc_intermon_forge_capability(send->send_cap_type, send->cap_base, send->cap_size, &context.send_env.cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_intermon_forge_capability\n");
            return err;
        }
    }

    err = aos_rpc_generic_send_and_recv(r, send->fid, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling relaying aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // NOTE: deliberately NO check of the embedded return code from RPC
    // it is just forwarded to recipient

    // handle payload
    size_t env_size = sizeof(struct aos_rpc_recv_msg_env) + context.recv_env.buflen;
    //size_t size = sizeof(struct aos_rpc_intermon_forward_res_payload) + env_size;
    recv->env_size = env_size;
    memcpy(recv->env, &context.recv_env, sizeof(struct aos_rpc_recv_msg_env));
    if (0 < context.recv_env.buflen) {
        memcpy(recv->env + sizeof(struct aos_rpc_recv_msg_env), context.recv_env.buf, context.recv_env.buflen);
    }

    if (recv_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
        // note: only limited types of caps can be transferred, of course
        struct frame_identity info;
        // works also for RAM
        err = frame_identify(context.recv_env.cap, &info);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "frame_identify for cap failed\n");
            return err;
        }
        recv->cap_base = info.base;
        recv->cap_size = info.bytes;
    }

    // all OK
    return SYS_ERR_OK;
}

// internal
void intermonserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_INTERMON_FID_REQUEST_BOOTSTRAP_INFO: {
            assert(context->recv_env.buflen >= sizeof(size_t));
            size_t *payload = context->recv_env.buf;
            coreid_t core = (coreid_t)*payload;
            struct aos_intermon_bootstrap_res response;
            err = aos_rpc_local_intermon_request_bootstrap_info(NULL, core, &response);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &response;
            context->send_env.buflen = sizeof(response);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_INTERMON_FID_FORWARD_RPC_MESSAGE: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_intermon_forward_req_payload));
            struct aos_rpc_intermon_forward_req_payload *req = context->recv_env.buf;

            // no mallocs allowed: use a default buffer
            // note: must be uintptr_t aligned
            uintptr_t memory_buffer_res[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];
            struct aos_rpc_intermon_forward_res_payload *res = (void *)memory_buffer_res;

            // one FID special case for FG that are handled in the init monitor/0
            // -> that needs handling here
            bool monitor_get_new_endpoint = false;
            struct aos_rpc_common_request_new_endpoint_req_payload *ep_req = context->recv_env.buf;
            struct capref ep_cap;
            void *ep_data;
            if ( (req->fid == AOS_RPC_COMMON_FID_REQUEST_NEW_ENDPOINT)
                 && (   (req->interface == AOS_RPC_MONITOR_FG)
                     || (req->interface == AOS_RPC_DEVICE_FG)
                     || (req->interface == 9) ) ) {
                // TODO: replace 9 with AOS_RPC_NETWORK_FG after merge with network extension

                monitor_get_new_endpoint = true;
                // note: malloc() must be allowed here. More memory allocations are going on
                // in this path (see channel cration)
                struct aos_rpc_send_msg_env *ep_env = (void *)req->env;
                assert(sizeof(struct aos_rpc_common_request_new_endpoint_req_payload) <= ep_env->buflen);
                ep_env->buf = (char *)ep_env + sizeof(struct aos_rpc_send_msg_env);
                ep_req = (void *)ep_env->buf;

                res->cap_base = 0;
                res->cap_size = 0;
                res->env_size = sizeof(struct aos_rpc_recv_msg_env);
                struct aos_rpc_recv_msg_env *ep_res = (void *)res->env;
                ep_res->header = ep_env->header;
                ep_res->buf = NULL;
                ep_res->buflen = 0;

                errval_t err2 = aos_rpc_local_request_new_endpoint_part1(ep_req->chan_type, &ep_cap, &ep_data);
                if (err_is_fail(err2)) {
                    ep_res->header = AOS_RPC_ADD_ERROR(ep_res->header, err2);
                    monitor_get_new_endpoint = false; // deactivate follow up
                }


                // extract frame info
                struct frame_identity info;
                err = frame_identify(ep_cap, &info);
                if (err_is_fail(err)) {
                    DEBUG_ERR(err, "frame_identify for ep_cap failed\n");
                }
                else {
                    res->cap_base = info.base;
                    res->cap_size = info.bytes;
                }
            }
            else {
                // default path
                err = aos_rpc_local_intermon_forward_rpc_message(NULL, req, res);
                if (err_is_fail(err)) {
                    send_reply(state, err);
                    return;
                }
            }

            context->send_env.buf = res;
            context->send_env.buflen = sizeof(struct aos_rpc_intermon_forward_res_payload) + res->env_size;
            send_reply(state, err);

            if (monitor_get_new_endpoint) {
                // needs to happen after the reply has been sent
                aos_rpc_local_request_new_endpoint_part2(ep_req->chan_type, ep_cap, ep_data);
            }
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}


// public
// accepts nameserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    monitor_event_handler,
    intermonserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_intermonserver_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_intermonserver_event_handler);
}
