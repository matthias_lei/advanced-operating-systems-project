/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-11-06, Group C
 */

#include <aos_rpc_server_filesystem_p.h>

// add specific includes here

#include <fs/fatfs.h>
#include <fs/multibootfs.h>
#include <fs/ramfs.h>
#include <fs/vfs.h>

struct aos_rpc_interface_filesystem_vtable aos_rpc_filesystem_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .open = aos_rpc_local_open,
    .create = aos_rpc_local_create,
    .remove = aos_rpc_local_remove,
    .read = aos_rpc_local_read,
    .write = aos_rpc_local_write,
    .truncate = aos_rpc_local_truncate,
    .tell = aos_rpc_local_tell,
    .stat = aos_rpc_local_stat,
    .seek = aos_rpc_local_seek,
    .close = aos_rpc_local_close,
    .opendir = aos_rpc_local_opendir,
    .dir_read_next = aos_rpc_local_dir_read_next,
    .closedir = aos_rpc_local_closedir,
    .mkdir = aos_rpc_local_mkdir,
    .rmdir = aos_rpc_local_rmdir,
    .mount = aos_rpc_local_mount
};

errval_t aos_rpc_local_open(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle) {
    return vfs_open(path, outhandle);
}

errval_t aos_rpc_local_create(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle) {
    return vfs_create(path, outhandle);
}

errval_t aos_rpc_local_remove(struct aos_rpc *rpc, const char *path) {
    return vfs_remove(path);
}

errval_t aos_rpc_local_read(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read) {
    return vfs_read(inhandle, buffer, bytes, bytes_read);
}

errval_t aos_rpc_local_write(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written) {
    return vfs_write(inhandle, buffer, bytes, bytes_written);
}

errval_t aos_rpc_local_truncate(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes) {
    return vfs_truncate(inhandle, bytes);
}

errval_t aos_rpc_local_tell(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos) {
    return vfs_tell(inhandle, pos);
}

errval_t aos_rpc_local_stat(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info) {
    return vfs_stat(inhandle, info);
}

errval_t aos_rpc_local_seek(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset) {
    return vfs_seek(inhandle, whence, offset);
}

errval_t aos_rpc_local_close(struct aos_rpc *rpc, fs_filehandle_t inhandle) {
    return vfs_close(inhandle);
}

errval_t aos_rpc_local_opendir(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle) {
    return vfs_opendir(path, outhandle);
}

errval_t aos_rpc_local_dir_read_next(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info) {
    return vfs_dir_read_next(inhandle, retname, info);
}

errval_t aos_rpc_local_closedir(struct aos_rpc *rpc, fs_dirhandle_t inhandle) {
    return vfs_closedir(inhandle);
}

errval_t aos_rpc_local_mkdir(struct aos_rpc *rpc, const char *path) {
    return vfs_mkdir(path);
}

errval_t aos_rpc_local_rmdir(struct aos_rpc *rpc, const char *path) {
    return vfs_rmdir(path);
}

errval_t aos_rpc_local_mount(struct aos_rpc *rpc, const char *path, const char *uri) {
    return vfs_mount(path, uri);
}

// internal
static void filesystem_server_event_handler_sync(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_FILESYSTEM_FID_OPEN: {
            size_t path_len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so this assertion must hold
            assert(strnlen(context->recv_env.buf, path_len) < path_len);

            fs_filehandle_t outhandle;
            errval_t err = aos_rpc_local_open(NULL, context->recv_env.buf, &outhandle);

            context->send_env.buf = &outhandle;
            context->send_env.buflen = sizeof(outhandle);

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_CREATE: {
            size_t path_len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so this assertion must hold
            assert(strnlen(context->recv_env.buf, path_len) < path_len);

            fs_filehandle_t outhandle;
            errval_t err = aos_rpc_local_create(NULL, context->recv_env.buf, &outhandle);

            context->send_env.buf = &outhandle;
            context->send_env.buflen = sizeof(outhandle);

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_REMOVE: {
            size_t path_len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so this assertion must hold
            assert(strnlen(context->recv_env.buf, path_len) < path_len);

            errval_t err = aos_rpc_local_remove(NULL, context->recv_env.buf);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_READ: {
            assert(sizeof(struct aos_rpc_read_req_payload) == context->recv_env.buflen);

            struct aos_rpc_read_req_payload *req_payload = context->recv_env.buf;

            size_t res_payload_len = sizeof(struct aos_rpc_read_res_payload) + req_payload->bytes;
            struct aos_rpc_read_res_payload *res_payload = malloc(res_payload_len);
            if (!res_payload) {
                struct aos_rpc_read_res_payload err_payload = {
                    .bytes_read = 0
                };
                context->send_env.buf = &err_payload;
                context->send_env.buflen = sizeof(err_payload);
                send_reply(state, LIB_ERR_MALLOC_FAIL);
                return;
            }

            errval_t err = aos_rpc_local_read(NULL, req_payload->inhandle, res_payload->buffer, req_payload->bytes, &res_payload->bytes_read);

            context->send_env.buf = res_payload;
            context->send_env.buflen = sizeof(struct aos_rpc_read_res_payload) + res_payload->bytes_read;

            send_reply(state, err);

            free(res_payload);

            return;
        }
        case AOS_RPC_FILESYSTEM_FID_WRITE: {
            assert(sizeof(struct aos_rpc_write_req_payload) <= context->recv_env.buflen);

            struct aos_rpc_write_req_payload *req_payload = context->recv_env.buf;

            assert(context->recv_env.buflen == sizeof(struct aos_rpc_write_req_payload) + req_payload->bytes);

            size_t bytes_written;
            errval_t err = aos_rpc_local_write(NULL, req_payload->inhandle, req_payload->buffer, req_payload->bytes, &bytes_written);

            context->send_env.buf = &bytes_written;
            context->send_env.buflen = sizeof(bytes_written);

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_TRUNCATE: {
            assert(sizeof(struct aos_rpc_truncate_req_payload) == context->recv_env.buflen);

            struct aos_rpc_truncate_req_payload *req_payload = context->recv_env.buf;

            errval_t err = aos_rpc_local_truncate(NULL, req_payload->inhandle, req_payload->bytes);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_TELL: {
            assert(sizeof(fs_filehandle_t) == context->recv_env.buflen);

            fs_filehandle_t *inhandle = context->recv_env.buf;

            size_t pos;
            errval_t err = aos_rpc_local_tell(NULL, *inhandle, &pos);

            context->send_env.buf = &pos;
            context->send_env.buflen = sizeof(pos);

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_STAT: {
            assert(sizeof(fs_filehandle_t) == context->recv_env.buflen);

            fs_filehandle_t *inhandle = context->recv_env.buf;

            struct fs_fileinfo info;
            errval_t err = aos_rpc_local_stat(NULL, *inhandle, &info);

            context->send_env.buf = &info;
            context->send_env.buflen = sizeof(info);

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_SEEK: {
            assert(sizeof(struct aos_rpc_seek_req_payload) == context->recv_env.buflen);

            struct aos_rpc_seek_req_payload *req_payload = context->recv_env.buf;

            errval_t err = aos_rpc_local_seek(NULL, req_payload->inhandle, req_payload->whence, req_payload->offset);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;

        }
        case AOS_RPC_FILESYSTEM_FID_CLOSE: {
            assert(sizeof(fs_filehandle_t) == context->recv_env.buflen);

            fs_filehandle_t *inhandle = context->recv_env.buf;

            errval_t err = aos_rpc_local_close(NULL, *inhandle);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;

        }
        case AOS_RPC_FILESYSTEM_FID_OPENDIR: {
            size_t path_len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so this assertion must hold
            assert(strnlen(context->recv_env.buf, path_len) < path_len);

            fs_dirhandle_t outhandle;
            errval_t err = aos_rpc_local_opendir(NULL, context->recv_env.buf, &outhandle);

            context->send_env.buf = &outhandle;
            context->send_env.buflen = sizeof(outhandle);

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_DIR_READ_NEXT: {
            assert(sizeof(fs_dirhandle_t) == context->recv_env.buflen);

            fs_dirhandle_t *inhandle = context->recv_env.buf;

            char *retname = NULL;
            struct fs_fileinfo info;
            errval_t err = aos_rpc_local_dir_read_next(NULL, *inhandle, &retname, &info);

            if (!retname) {
                retname = "";
            }
            size_t retname_len = strlen(retname) + 1;

            size_t res_payload_len = sizeof(struct aos_rpc_dir_read_next_res_payload) + retname_len;
            struct aos_rpc_dir_read_next_res_payload *res_payload = malloc(res_payload_len);
            if (!res_payload) {
                struct aos_rpc_dir_read_next_res_payload err_payload = {
                    .info = info
                };

                context->send_env.buf = &err_payload;
                context->send_env.buflen = sizeof(err_payload);

                send_reply(state, err);
                return;
            }

            res_payload->info = info;
            memcpy(res_payload->retname, retname, retname_len);

            context->send_env.buf = res_payload;
            context->send_env.buflen = res_payload_len;

            send_reply(state, err);

            free(res_payload);

            return;

        }
        case AOS_RPC_FILESYSTEM_FID_CLOSEDIR: {
            assert(sizeof(fs_dirhandle_t) == context->recv_env.buflen);

            fs_dirhandle_t *inhandle = context->recv_env.buf;

            errval_t err = aos_rpc_local_closedir(NULL, *inhandle);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_MKDIR: {
            size_t path_len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so this assertion must hold
            assert(strnlen(context->recv_env.buf, path_len) < path_len);

            errval_t err = aos_rpc_local_mkdir(NULL, context->recv_env.buf);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_RMDIR: {
            size_t path_len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so this assertion must hold
            assert(strnlen(context->recv_env.buf, path_len) < path_len);

            errval_t err = aos_rpc_local_rmdir(NULL, context->recv_env.buf);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;
        }
        case AOS_RPC_FILESYSTEM_FID_MOUNT: {
            size_t len = context->recv_env.buflen;
            // strnlen always returns the string length without the zero terminator, however, we always
            // send the zero terminator of strings, so these assertions must hold
            size_t path_len = strnlen(context->recv_env.buf, len) + 1;
            assert(path_len <= len);

            const char *path = context->recv_env.buf;

            size_t uri_len = strnlen(context->recv_env.buf + path_len, len - path_len) + 1;
            assert(uri_len <= len - path_len);

            const char *uri = context->recv_env.buf + path_len;

            errval_t err = aos_rpc_local_mount(NULL, path, uri);
            send_reply(state, err);
            return;
        }
        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            errval_t err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}

void filesystem_server_event_handler(struct aos_rpc_server_comm_state *state) {
    aos_rpc_generic_handler_async(state, filesystem_server_event_handler_sync);
}


// public
// accepts deviceserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    filesystem_server_event_handler,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_filesystem_server_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_filesystem_server_event_handler);
}
