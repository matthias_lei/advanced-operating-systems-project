/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-10-24, Group C
 */

/**
 * device server specific functions.
 * version 2017-12-09, Group C
 */

#include <aos_rpc_server_blockdev_p.h>
#include <aos/caddr.h>

// add specific includes here
#include <mmchs/mmchs.h>

struct aos_rpc_interface_blockdev_vtable aos_rpc_blockdev_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .get_block_size = aos_rpc_local_get_block_size,
    .read_block = aos_rpc_local_read_block,
    .write_block = aos_rpc_local_write_block
};

#define SD_CARD_BLOCK_DEVICE_BLOCK_SIZE 512

errval_t aos_rpc_local_get_block_size(struct aos_rpc *rpc, size_t *block_size) {
    assert(block_size);

    *block_size = SD_CARD_BLOCK_DEVICE_BLOCK_SIZE;

    return SYS_ERR_OK;
}

errval_t aos_rpc_local_read_block(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen) {
    assert(buf);

    if (buflen != SD_CARD_BLOCK_DEVICE_BLOCK_SIZE) {
        return SYS_ERR_INVALID_SIZE;
    }

    return mmchs_read_block(block_nr, buf);
}

errval_t aos_rpc_local_write_block(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen) {
    assert(buf);

    if (buflen != SD_CARD_BLOCK_DEVICE_BLOCK_SIZE) {
        return SYS_ERR_INVALID_SIZE;
    }

    return mmchs_write_block(block_nr, buf);
}

// internal
void blockdev_server_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_BLOCKDEV_FID_GET_BLOCK_SIZE: {
            size_t block_size;
            err = aos_rpc_local_get_block_size(NULL, &block_size);
            context->send_env.buf = &block_size;
            context->send_env.buflen = sizeof(size_t);
            send_reply(state, err);
            return;
        }
        case AOS_RPC_BLOCKDEV_FID_READ_BLOCK: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_read_block_req_payload));

            struct aos_rpc_read_block_req_payload *req_payload = context->recv_env.buf;
            void *buf = malloc(req_payload->buflen);
            if (!buf) {
                context->send_env.buf = NULL;
                context->send_env.buflen = 0;
                send_reply(state, LIB_ERR_MALLOC_FAIL);
                return;
            }

            err = aos_rpc_local_read_block(NULL, req_payload->block_nr, buf, req_payload->buflen);
            if (err_is_fail(err)) {
                free(buf);
                context->send_env.buf = NULL;
                context->send_env.buflen = 0;
                send_reply(state, err);
                return;
            }

            context->send_env.buf = buf;
            context->send_env.buflen = req_payload->buflen;

            send_reply(state, err);

            free(buf);
            return;
        }
        case AOS_RPC_BLOCKDEV_FID_WRITE_BLOCK: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_write_block_req_payload));

            struct aos_rpc_write_block_req_payload *req_payload = context->recv_env.buf;

            assert(context->recv_env.buflen - sizeof(struct aos_rpc_write_block_req_payload) == req_payload->buflen);

            err = aos_rpc_local_write_block(NULL, req_payload->block_nr, req_payload->buf, req_payload->buflen);

            context->send_env.buf = NULL;
            context->send_env.buflen = 0;

            send_reply(state, err);
            return;
        }
        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}


// public
// accepts blockdev_server (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    blockdev_server_event_handler,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_blockdev_server_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_blockdev_server_event_handler);
}
