/**
 * shared specific functions.
 *
 * this part of the library gets linked into all the specific event handlers.
 *
 * version 2017-11-24, Group C
 */

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server_intermon_p.h>

#include <aos/connect/all.h>

// initialized with the first event handler that is used in a server
// but only with the first
waitlist_event_handler_t my_event_handler = NULL;

struct aos_rpc_interface_common_vtable aos_rpc_common_local_vtable = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT;

//--- internal helper functions --------------------------------------------------------------------
// these functions take the event handler state that must hold already the received message
// that consists of lmp_recf_msg and a capref.

// note: they are part of the internal API. They were not made static to allow using them
// also in the specialized internal event handlers.

void aos_rpc_comm_context_server_init(struct aos_rpc_comm_context *context) {
    assert(context);

    context->recv_env.cap = NULL_CAP;
    context->recv_env.buf = NULL;
    context->recv_env.buflen = 0;

    context->send_env.cap = NULL_CAP;
    context->send_env.buf = NULL;
    context->send_env.buflen = 0;
    context->is_server = true;
}

// also here
// note: actual payload can only be in array pos 1 to 8; 0 is overwritten by fid and err
void send_reply(struct aos_rpc_server_comm_state *state, errval_t err) {
    assert(state);
    assert(state->context);

    state->context->send_env.header = AOS_RPC_ADD_ERROR(state->fid, err);

    err = aos_rpc_generic_send(state->chan, &state->context->send_env);
    if (err_is_fail(err)) {
        // we do nothing except logging the error
        DEBUG_ERR(err, "aos_rpc_generic_send failed\n");
    }
}

errval_t aos_rpc_local_send_number(struct aos_rpc *rpc /*unused*/, uintptr_t val) {
    debug_printf("received number %u.\n", val);
    return SYS_ERR_OK;
}

// max number of characters printed at once using debug_printf
#define STRING_LIMIT 80

errval_t aos_rpc_local_send_string(struct aos_rpc *rpc /*unused*/, const char *string) {
    size_t len = strlen(string);
    if (len == 0) {
        debug_printf("received empty string.\n");
    }
    else if (len <= STRING_LIMIT) {
        // short strings work well with debug_printf();
        debug_printf("received string '%.*s' (len %u).\n", len, string, len);
    }
    else {
        // very long strings do not work well with debug_printf
        debug_printf("received long string (len %u):\n", len);
        while (len) {
            size_t print_len = MIN(len, STRING_LIMIT);
            debug_printf("%.*s\n", print_len, string);
            string += print_len;
            len -= print_len;
        }
    }
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_request_new_endpoint_part1(enum aos_rpc_chan_driver chan_type,
                                                  struct capref *ret_cap, void **ret_data)
{
    assert(ret_cap);
    assert(ret_data);

    errval_t err = SYS_ERR_OK;

    *ret_cap = NULL_CAP;
    *ret_data = NULL;

    switch (chan_type) {
        case AOS_RPC_CHAN_DRIVER_LMP: {
            struct capref endpoint_cap;
            err = slot_alloc(&endpoint_cap);
            if (err_is_fail(err)) {
                return err;
            }

            err = setup_lmp_service(endpoint_cap, MKCLOSURE(get_my_event_handler(), NULL));
            if (err_is_fail(err)) {
                return err;
            }

            // all OK
            *ret_cap = endpoint_cap;
            return SYS_ERR_OK;
        }

        case AOS_RPC_CHAN_DRIVER_FLMP: {
            struct capref endpoint_cap;
            err = slot_alloc(&endpoint_cap);
            if (err_is_fail(err)) {
                return err;
            }

            err = setup_flmp_service(endpoint_cap, MKCLOSURE(get_my_event_handler(), NULL));
            if (err_is_fail(err)) {
                return err;
            }

            // all OK
            *ret_cap = endpoint_cap;
            return SYS_ERR_OK;
        }

        case AOS_RPC_CHAN_DRIVER_UMP: {
            // 1st part: frame prep and mapping
            // I kept as much above the send_reply to catch errors if possible
            // However, some things need to be done afterwards.

            struct capref frame;
            size_t ret_bytes;

            // BASE_PAGE_SIZE for regular UMP buffers
            // TODO: check again size, can also be larger
            err = frame_alloc(&frame, BASE_PAGE_SIZE, &ret_bytes);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "frame_alloc failed\n");
                return err;
            }

            struct ump_service_create_data *data = calloc(1, sizeof(*data));
            if (!data) {
                return LIB_ERR_MALLOC_FAIL;
            }

            err = setup_ump_service_part1(frame, data);
            if (err_is_fail(err)) {
                free(data);
                return err;
            }

            *ret_cap = frame;
            *ret_data = data;
            return SYS_ERR_OK;
            // see you in part 2
        }

        default: {
            err = LIB_ERR_NOT_IMPLEMENTED;
            DEBUG_ERR(err, "not implemented for this channel type\n");
            return err;
        }
    }
}

void aos_rpc_local_request_new_endpoint_part2(enum aos_rpc_chan_driver chan_type,
                                              struct capref cap, void *data)
{
    // data may be NULL here

    errval_t err = SYS_ERR_OK;

    switch (chan_type) {
        case AOS_RPC_CHAN_DRIVER_LMP:
        case AOS_RPC_CHAN_DRIVER_FLMP: {
            // nothing
            return;
        }

        case AOS_RPC_CHAN_DRIVER_UMP: {
            //--- this 2nd part must come after sending the reply
            // chan init performs a handshake with the connecting client
            assert(data);
            err = setup_ump_service_part2(data, MKCLOSURE(get_my_event_handler(), NULL));
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "setup_ump_service_part2() failed\n");
                // there is nothing more to be done here; message already replied
                // note: all memory allocations (that could fail) have been done in part1 though
                // if part2 fails, something really is wrong with the system
            }

            // in all cases
            free(data);
            return;
        }

        default: {
            err = LIB_ERR_NOT_IMPLEMENTED;
            DEBUG_ERR(err, "not implemented for this channel type\n");
            return;
        }
    }
}

// internal API
void common_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_COMMON_FID_SEND_NUMBER: {
            // assume the number is in words[1]
            assert(context->recv_env.buflen >= sizeof(uintptr_t));
            uintptr_t *val = context->recv_env.buf;
            err = aos_rpc_local_send_number(NULL, *val);

            // both cases: error and OK
            send_reply(state, err);
            return;
        }

        case AOS_RPC_COMMON_FID_SEND_STRING: {
            // note: we always send the zero terminator of a string, since strnlen returns the length of the string without
            //       the zero terminator, buflen should always be strictly larger than the actual string length
            assert(strnlen(context->recv_env.buf, context->recv_env.buflen) < context->recv_env.buflen);
            err = aos_rpc_local_send_string(NULL, context->recv_env.buf);

            // both cases: error and OK
            send_reply(state, err);
            return;
        }

        case AOS_RPC_COMMON_FID_CONNECT_WITH_SERVICE: {
            // NOTE: this is not implemented on the server but in the client!
            //       if a request came here, something would have gone quite wrong.
            err = LIB_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
        }

        case AOS_RPC_COMMON_FID_REQUEST_NEW_ENDPOINT: {
            assert(sizeof(struct aos_rpc_common_request_new_endpoint_req_payload) <= context->recv_env.buflen);
            struct aos_rpc_common_request_new_endpoint_req_payload *req = context->recv_env.buf;

            struct capref cap;
            void *data;
            err = aos_rpc_local_request_new_endpoint_part1(req->chan_type, &cap, &data);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // OK: reply first and then finish the work
            context->send_env.cap = cap;
            send_reply(state, err);

            aos_rpc_local_request_new_endpoint_part2(req->chan_type, cap, data);
            return;
        }

        case AOS_RPC_COMMON_FID_BENCHMARKING: {
            struct aos_rpc_common_benchmarking_req_payload *payload =
                (struct aos_rpc_common_benchmarking_req_payload*) context->recv_env.buf;

            size_t res_payload_size = sizeof(struct aos_rpc_common_benchmarking_res_payload)
                                        + payload->exp_buflen * sizeof(char);
            struct aos_rpc_common_benchmarking_res_payload *res_payload =
                                                    calloc(res_payload_size, sizeof(char));
            if (!res_payload) {
                err = LIB_ERR_MALLOC_FAIL;
                DEBUG_ERR(err, "calloc failed.\n");
                send_reply(state, err);
                return;
            }

            res_payload->size = payload->exp_buflen;
            context->send_env.buf = res_payload;
            context->send_env.buflen = res_payload_size;

            send_reply(state, err);
            free(res_payload);
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}

void generic_event_handler(struct aos_rpc_server_comm_state *state,
                           aos_rpc_fg_event_handler_t *fg_access,
                           waitlist_event_handler_t event_handler) {
    assert(state);

    // registers the first event handler that is used
    set_my_event_handler(event_handler);

    // allocation of current data on stack
    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_server_init(&context);

    state->context = &context;
    errval_t err = aos_rpc_generic_recv(state->chan, &context.recv_env, true, context.recv_buffer);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "NON transient ERROR during receive_message(). NO re-registration of event handler.\n");
        goto exit;
    }

    // refill the channel receive cap slot if we received a capability just now
    if (!capref_is_null(state->context->recv_env.cap)) {
        struct capref new_cap;
        err = slot_alloc(&new_cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "slot_alloc failed.\n");
            goto exit;
        }
        state->chan->vtable->install_recv_slot(state->chan, new_cap);
    }

    state->fid = AOS_RPC_GET_FID(state->context->recv_env.header);

    // for debugging
    //debug_printf("%s: chan %x: handling fid %x\n", __func__, state->chan, state->fid);

    uint8_t fg = AOS_RPC_GET_FG(state->fid);
    assert(fg < AOS_RPC_FG_SIZE);
    aos_rpc_fg_event_handler_t fg_handler = fg_access[fg];
    if (fg_handler) {
        fg_handler(state);
    }
    else {
        debug_printf("%s: ERROR: unknown function group %u.\n", __func__, (unsigned)fg);
        send_reply(state, SYS_ERR_NOT_IMPLEMENTED);
    }

    // re-register the event handler for the next message
    err = state->chan->vtable->register_recv(state->chan, get_default_waitset(), MKCLOSURE(event_handler, state));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ERROR during re-registration of the event handler after handling the event.\n");
        // fall through
    }

exit:
    if (context.recv_env.buf && (context.recv_env.buf != context.recv_buffer)) {
        free(context.recv_env.buf);
    }
    // note: as discussed an in contrast to some safety policies, we do NOT erase the used memory region after
    // use for messaging at the moment.
    // consider calling event_handler_lmp_data_init(&d) again.
    state->context = NULL;
}

//--- serialized RPC request handling API ----------------------------------------------------------

struct aos_rpc_request_queue *my_request_queue;

void aos_rpc_request_queue_init(struct aos_rpc_request_queue *queue) {
    waitset_init(&queue->ws);
    queue->is_handling_sync_req = false;
}

static void aos_rpc_move_comm_context(struct aos_rpc_comm_context *src_context, struct aos_rpc_comm_context *dest_context) {
    // copy general fields
    dest_context->is_server = src_context->is_server;

    // copy receive envelope to destination
    dest_context->recv_env.header = src_context->recv_env.header;
    dest_context->recv_env.expected_cap_type = src_context->recv_env.expected_cap_type;
    dest_context->recv_env.cap = src_context->recv_env.cap;

    // make sure the internal buffer is handled correctly
    if (src_context->recv_env.buf == src_context->recv_buffer) {
        assert(src_context->is_server);
        assert(src_context->recv_env.buflen <= sizeof(dest_context->recv_buffer));
        memcpy(dest_context->recv_buffer, src_context->recv_buffer, src_context->recv_env.buflen);
        dest_context->recv_env.buf = dest_context->recv_buffer;
        dest_context->recv_env.buflen = src_context->recv_env.buflen;
    }
    else {
        dest_context->recv_env.buf = src_context->recv_env.buf;
        dest_context->recv_env.buflen = src_context->recv_env.buflen;
    }

    // clear receive envelope of source
    src_context->recv_env.header = 0;
    src_context->recv_env.expected_cap_type = AOS_RPC_INTERMON_CAPABILITY_NONE;
    src_context->recv_env.cap = NULL_CAP;
    src_context->recv_env.buf = NULL;
    src_context->recv_env.buflen = 0;

    // copy send envelope to destination
    dest_context->send_env.header = src_context->send_env.header;
    dest_context->send_env.cap_type = src_context->send_env.cap_type;
    dest_context->send_env.cap = src_context->send_env.cap;
    dest_context->send_env.buf = src_context->send_env.buf;
    dest_context->send_env.buflen = src_context->send_env.buflen;

    // clear send envelope of source
    src_context->send_env.header = 0;
    src_context->send_env.cap_type = AOS_RPC_INTERMON_CAPABILITY_NONE;
    src_context->send_env.cap = NULL_CAP;
    src_context->send_env.buf = NULL;
    src_context->send_env.buflen = 0;
}

struct aos_rpc_server_comm_state_sync {
    struct aos_rpc_server_comm_state state_async; // must be first, such that aos_rpc_server_comm_state_sync
                                                  // can be cast to aos_rpc_server_comm_state
    struct waitset_chanstate ws_chan;
    aos_rpc_fg_event_handler_t handler_sync;
};

static errval_t aos_rpc_server_comm_state_sync_init(struct aos_rpc_server_comm_state_sync *comm_state_sync,
                                                    struct aos_rpc_server_comm_state *comm_state_async,
                                                    aos_rpc_fg_event_handler_t handler_sync) {
    assert(comm_state_sync);
    assert(comm_state_async);
    assert(handler_sync);

    comm_state_sync->state_async.chan = comm_state_async->chan;
    comm_state_sync->state_async.fid = comm_state_async->fid;
    comm_state_sync->state_async.server_persistent_data = comm_state_async->server_persistent_data;
    comm_state_sync->handler_sync = handler_sync;
    waitset_chanstate_init(&comm_state_sync->ws_chan, CHANTYPE_OTHER);

    comm_state_sync->state_async.context = malloc(sizeof(struct aos_rpc_comm_context));
    if (!comm_state_sync->state_async.context) {
        return LIB_ERR_MALLOC_FAIL;
    }

    aos_rpc_move_comm_context(comm_state_async->context, comm_state_sync->state_async.context);
    return SYS_ERR_OK;
}

static void aos_rpc_server_comm_context_sync_destroy(struct aos_rpc_server_comm_state_sync *comm_state_sync) {
    waitset_chanstate_destroy(&comm_state_sync->ws_chan);
    free(comm_state_sync->state_async.context);
    free(comm_state_sync);
}

static void aos_rpc_generic_handler_sync(void *args) {
    assert(args);

    struct aos_rpc_server_comm_state_sync *state = args;
    state->handler_sync(&state->state_async);

    aos_rpc_server_comm_context_sync_destroy(state);
}

void aos_rpc_generic_handler_async(struct aos_rpc_server_comm_state *state, aos_rpc_fg_event_handler_t handler_sync) {
    assert(state);
    assert(state->context);
    assert(handler_sync);
    assert(my_request_queue);

    struct aos_rpc_comm_context *context_async = state->context;
    struct aos_rpc_server_comm_state_sync *comm_state_sync = malloc(sizeof(struct aos_rpc_server_comm_state_sync));
    if (!comm_state_sync) {
        context_async->send_env.buf = NULL;
        context_async->send_env.buflen = 0;
        send_reply(state, LIB_ERR_MALLOC_FAIL);
        return;
    }

    errval_t err = aos_rpc_server_comm_state_sync_init(comm_state_sync, state, handler_sync);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_server_comm_state_sync_init failed.\n");
        free(comm_state_sync);
        context_async->send_env.buf = NULL;
        context_async->send_env.buflen = 0;
        send_reply(state, err);
    }

    // if we are already handling a synchronous request, enqueue this request
    if (my_request_queue->is_handling_sync_req) {
        err = waitset_chan_trigger_closure(&my_request_queue->ws,
                                           &comm_state_sync->ws_chan,
                                           MKCLOSURE(aos_rpc_generic_handler_sync, comm_state_sync));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "waitset_chan_trigger_closure failed.\n");
            free(comm_state_sync);
            send_reply(state, err);
        }
    }
    // otherwise, handle it directly, and all the enqueued requests as well
    else {
        my_request_queue->is_handling_sync_req = true;
        aos_rpc_generic_handler_sync(comm_state_sync);
        struct waitset *ws = &my_request_queue->ws;
        // dispatch all events that queued up until now
        // this has to be non-blocking, since if the queue is empty
        // we want to return to the service event loop that dispatches
        // events from the default waitset
        while (err_is_ok(err = event_dispatch_non_block(ws)));
        if (err_no(err) != LIB_ERR_NO_EVENT) {
            DEBUG_ERR(err, "event_dispatch_non_block failed.\n");
        }
        my_request_queue->is_handling_sync_req = false;
        // we are done with all the enqueued requests
    }
}

//--- public API -----------------------------------------------------------------------------------
// initializes the state info
void aos_rpc_server_comm_state_init(struct aos_rpc_server_comm_state *state, struct aos_rpc_channel *chan) {
    assert(state);
    assert(chan);
    state->chan = chan;
    state->context = NULL;
    state->server_persistent_data = NULL;
}


static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
// accepts only common events
void aos_rpc_common_event_handler(void *arg) {
    assert(arg);

    set_my_event_handler(aos_rpc_common_event_handler);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_common_event_handler);
}
