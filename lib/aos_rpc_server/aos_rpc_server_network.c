/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-11-06, Group C
 */

#include <aos_rpc_server_network_p.h>

#include <net/socket_interface_local.h>
#include <errno.h>

struct aos_rpc_interface_network_vtable aos_rpc_network_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .accept = aos_rpc_local_accept,
    .bind = aos_rpc_local_bind,
    .connect = aos_rpc_local_connect,
    .getpeername = aos_rpc_local_getpeername,
    .getsockname = aos_rpc_local_getsockname,
    .getsockopt = aos_rpc_local_getsockopt,
    .listen = aos_rpc_local_listen,
    .recv = aos_rpc_local_recv,
    .recvfrom = aos_rpc_local_recvfrom,
    .recvmsg = aos_rpc_local_recvmsg,
    .send = aos_rpc_local_send,
    .sendmsg = aos_rpc_local_sendmsg,
    .sendto = aos_rpc_local_sendto,
    .setsockopt = aos_rpc_local_setsockopt,
    .shutdown = aos_rpc_local_shutdown,
    .socket = aos_rpc_local_socket,
    .socketpair = aos_rpc_local_socketpair
};

static void recv_handler(void *args);
static void init_network_recv_data(struct aos_rpc_network_recv_data *data, struct aos_rpc_server_comm_state *state);

errval_t aos_rpc_local_accept(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    *ret_value = socket_interface_local_accept(socket, address, address_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_bind(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len) {
    *ret_value = socket_interface_local_bind(socket, address, address_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_connect(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len) {
    *ret_value = socket_interface_local_connect(socket, address, address_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_getpeername(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    *ret_value = socket_interface_local_getpeername(socket, address, address_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_getsockname(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    *ret_value = socket_interface_local_getsockname(socket, address, address_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_getsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len) {
    *ret_value = socket_interface_local_getsockopt(socket, level, option_name, option_value, option_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_listen(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int backlog) {
    *ret_value = socket_interface_local_listen(socket, backlog);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_recv(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags) {
    *ret_size = socket_interface_local_recv(socket, buffer, length, flags);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_recvfrom(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {
    *ret_size = socket_interface_local_recvfrom(socket, buffer, length, flags, address, address_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_recvmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags) {
    *ret_size = socket_interface_local_recvmsg(socket, message, flags);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_send(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags) {
    *ret_size = socket_interface_local_send(socket, message, length, flags);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_sendmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags) {
    *ret_size = socket_interface_local_sendmsg(socket, message, flags);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_sendto(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
    *ret_size = socket_interface_local_sendto(socket, message, length, flags, dest_addr, dest_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_setsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len) {
    *ret_value = socket_interface_local_setsockopt(socket, level, option_name, option_value, option_len);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_shutdown(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int how) {
    *ret_value = socket_interface_local_shutdown(socket, how);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_socket(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol) {
    *ret_value = socket_interface_local_socket(domain, type, protocol);
    *err_no = errno;
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_socketpair(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]) {
    *ret_value = socket_interface_local_socketpair(domain, type, protocol, socket_vector);
    *err_no = errno;
    return SYS_ERR_OK;
}

static void recv_handler(void *args) {
    assert(args);

    struct aos_rpc_network_recv_data *recv_data = (struct aos_rpc_network_recv_data *)args;
    struct aos_rpc_server_comm_state *state = (struct aos_rpc_server_comm_state *)recv_data->state;
    struct aos_rpc_network_recvfrom_req_payload *req = &recv_data->req;

    bool can_recv;
    int err_code;
    err_code = socket_interface_local_can_recv(req->socket, &can_recv);
    // the handler can only be called upon packet delivery
    assert(err_code == 0);
    assert(can_recv);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_send_msg_env send_env;
    send_env.cap = NULL_CAP;
    send_env.header = AOS_RPC_ADD_ERROR(AOS_RPC_NETWORK_FID_RECVFROM, SYS_ERR_OK);

    struct aos_rpc_network_recvfrom_res_payload *res = (struct aos_rpc_network_recvfrom_res_payload *)malloc(sizeof(struct aos_rpc_network_recvfrom_res_payload) + req->length);
    if (!res) {
        goto send_error;
    }

    send_env.buf = res;
    send_env.buflen = sizeof(struct aos_rpc_network_recvfrom_res_payload) + req->length;

    err = aos_rpc_local_recvfrom(NULL,
                                 &res->ret_size,
                                 &res->err_no,
                                 req->socket,
                                 &res->buffer,
                                 req->length,
                                 req->flags,
                                 &res->addr,
                                 &res->addr_len);

    if (err_is_fail(err)) {
        free(res);
        goto send_error;
    }

    send_env.buf = res;
    send_env.buflen = sizeof(struct aos_rpc_network_recvfrom_res_payload) + req->length;
    err = aos_rpc_generic_send(state->chan, &send_env);
    if (err_is_fail(err)) {
        free(res);
         // we do nothing except logging the error and reregister (otherwise client blocks infinitely)
        socket_interface_local_set_recv_handler(req->socket, MKCLOSURE(recv_handler, args));
        DEBUG_ERR(err, "aos_rpc_generic_send failed\n");
        return;
    }
    free(res);
    free(args);
    return;

send_error:

    assert(err_is_fail(err));
    struct aos_rpc_network_recvfrom_res_payload err_res;

    send_env.header = AOS_RPC_ADD_ERROR(AOS_RPC_NETWORK_FID_RECVFROM, err);
    send_env.buf = &err_res;
    send_env.buflen = sizeof(err_res);
    err_res.ret_size = -1;
    if (res) {
        err_res.err_no = res->err_no;
        free(res);
    }
    // other fields in the payload do not matter
    err = aos_rpc_generic_send(state->chan, &send_env);
    if (err_is_fail(err)) {
        // we do nothing except logging the error and reregister (otherwise client blocks infinitely)
        socket_interface_local_set_recv_handler(req->socket, MKCLOSURE(recv_handler, args));
        DEBUG_ERR(err, "aos_rpc_generic_send failed\n");
        return;
    }
    free(args);
    return;
}

static void init_network_recv_data(struct aos_rpc_network_recv_data *data, struct aos_rpc_server_comm_state *state) {
    assert(data);
    assert(state);

    data->state = state;
    data->req = *((struct aos_rpc_network_recvfrom_req_payload *)state->context->recv_env.buf);
}


// internal
void networkserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_NETWORK_FID_ACCEPT:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_BIND: {
            assert(context->recv_env.buflen == sizeof(struct aos_rpc_network_bind_req_payload));

            struct aos_rpc_network_bind_req_payload *req = context->recv_env.buf;
            struct aos_rpc_network_bind_res_payload res;

            err = aos_rpc_local_bind(NULL,
                                     &res.ret_value,
                                     &res.err_no,
                                     req->socket,
                                     &req->addr,
                                     req->addr_len);
            if (err_is_fail(err)) {
                send_reply(state, err);
                break;
            }

            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);
            send_reply(state, err);
            break;
        }
        case AOS_RPC_NETWORK_FID_CONNECT:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_GETPEERNAME:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_GETSOCKNAME: {
            assert(context->recv_env.buflen == sizeof(struct aos_rpc_network_getsockname_req_payload));

            struct aos_rpc_network_getsockname_req_payload *req = context->recv_env.buf;
            struct aos_rpc_network_getsockname_res_payload res;

            err = aos_rpc_local_getsockname(NULL,
                                            &res.ret_value,
                                            &res.err_no,
                                            req->socket,
                                            &res.addr,
                                            &res.addr_len);
            if (err_is_fail(err)) {
                send_reply(state, err);
                break;
            }

            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);
            send_reply(state, err);
            break;
        }
        case AOS_RPC_NETWORK_FID_GETSOCKOPT:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_LISTEN:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_RECV:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_RECVFROM: {
            assert(context->recv_env.buflen == sizeof(struct aos_rpc_network_recvfrom_req_payload));

            struct aos_rpc_network_recvfrom_req_payload *req = context->recv_env.buf;
            bool can_recv;
            int err_code = socket_interface_local_can_recv(req->socket, &can_recv);
            if (err_code != 0) {
                struct aos_rpc_network_recvfrom_res_payload err_res;
                err_res.ret_size = -1;
                err_res.err_no = errno;
                context->send_env.buf = &err_res;
                context->send_env.buflen = sizeof(err_res);
                send_reply(state, SYS_ERR_OK);
                break;
            }

            if (!can_recv) {
                // do not send reply
                state->server_persistent_data = malloc(sizeof(struct aos_rpc_network_recv_data));
                if (!state->server_persistent_data) {
                    send_reply(state, LIB_ERR_MALLOC_FAIL);
                    break;
                }

                init_network_recv_data((struct aos_rpc_network_recv_data *)state->server_persistent_data, state);
                socket_interface_local_set_recv_handler(req->socket, MKCLOSURE(recv_handler, state->server_persistent_data));
                break;
            }


            struct aos_rpc_network_recvfrom_res_payload *res = (struct aos_rpc_network_recvfrom_res_payload *)malloc(sizeof(struct aos_rpc_network_recvfrom_res_payload) + req->length);
            if (!res) {
                send_reply(state, LIB_ERR_MALLOC_FAIL);
                break;
            }

            err = aos_rpc_local_recvfrom(NULL,
                                         &res->ret_size,
                                         &res->err_no,
                                         req->socket,
                                         &res->buffer,
                                         req->length,
                                         req->flags,
                                         &res->addr,
                                         &res->addr_len);
            if (err_is_fail(err)) {
                send_reply(state, err);
                free(res);
                break;
            }

            context->send_env.buf = res;
            context->send_env.buflen = sizeof(struct aos_rpc_network_recvfrom_res_payload) + req->length;
            send_reply(state, err);
            free(res);
            break;
        }
        case AOS_RPC_NETWORK_FID_RECVMSG:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_SEND:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_SENDMSG:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;
        case AOS_RPC_NETWORK_FID_SENDTO: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_network_sendto_req_payload));

            struct aos_rpc_network_sendto_req_payload *req = context->recv_env.buf;
            struct aos_rpc_network_sendto_res_payload res;

            err = aos_rpc_local_sendto(NULL,
                                       &res.ret_size,
                                       &res.err_no,
                                       req->socket,
                                       req->message,
                                       req->length,
                                       req->flags,
                                       &req->dest_addr,
                                       req->dest_len);
            if (err_is_fail(err)) {
                send_reply(state, err);
                break;
            }
            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);

            send_reply(state, err);
            break;
        }

        case AOS_RPC_NETWORK_FID_SETSOCKOPT:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_SHUTDOWN:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;

        case AOS_RPC_NETWORK_FID_SOCKET: {
            assert(context->recv_env.buflen == sizeof(struct aos_rpc_network_socket_req_payload));

            struct aos_rpc_network_socket_req_payload *req = context->recv_env.buf;
            struct aos_rpc_network_socket_res_payload res;

            err = aos_rpc_local_socket(NULL,
                                       &res.ret_value,
                                       &res.err_no,
                                       req->domain,
                                       req->type,
                                       req->protocol);
            if (err_is_fail(err)) {
                send_reply(state, err);
                break;
            }

            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);
            send_reply(state, err);
            break;
        }

        case AOS_RPC_NETWORK_FID_SOCKETPAIR:
            send_reply(state, LIB_ERR_NOT_IMPLEMENTED);
            break;
        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}


// public
// accepts networkserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    networkserver_event_handler,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_network_server_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_network_server_event_handler);
}
