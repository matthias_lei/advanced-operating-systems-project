/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-11-06, Group C
 */

#include <aos_rpc_server_shell_p.h>

// add specific includes here
#include <shell_shared.h>


struct aos_rpc_interface_shell_vtable aos_rpc_shell_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .request_params = NULL,
    .notify_termination = NULL
};


// internal
static void shell_server_event_handler_sync(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_SHELL_FID_REQUEST_PARAMS: {
            char *std_in_path = current_process_config.std_in_path ? current_process_config.std_in_path : "";
            char *std_out_path = current_process_config.std_out_path ? current_process_config.std_out_path : "";

            size_t reply_size = sizeof(struct shell_params)
                                + strlen(std_in_path) + 1
                                + strlen(std_out_path) + 1;
            struct shell_params *reply = malloc(reply_size);

            reply->background = current_process_config.background;
            reply->shell_pid = disp_get_domain_id();

            size_t len_std_in = strlen(std_in_path);
            strcpy(reply->io_redirection_paths, std_in_path);
            strcpy(reply->io_redirection_paths + len_std_in + 1, std_out_path);

            context->send_env.buf = reply;
            context->send_env.buflen = reply_size;

            errval_t err = SYS_ERR_OK;
            send_reply(state, err);
            free(reply);

            waiting_for_termination = !current_process_config.background;
            return;
        }
        case AOS_RPC_SHELL_FID_NOTIFY_TERMINATION: {
            errval_t err = SYS_ERR_OK;
            send_reply(state, err);
            waiting_for_termination = false;
            return;
        }
        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            errval_t err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}

void shell_server_event_handler(struct aos_rpc_server_comm_state *state) {
    aos_rpc_generic_handler_async(state, shell_server_event_handler_sync);
}


// public
// accepts deviceserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    shell_server_event_handler
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_shell_server_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_shell_server_event_handler);
}
