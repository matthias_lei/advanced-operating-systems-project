/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-10-24, Group C
 */

/**
 * process server specific functions.
 * update: spawn service is in monitor
 * version 2017-12-04, Group C
 */

#include <aos_rpc_server_process_p.h>

// add specific includes here
#include <aos/aos_rpc.h>
#include <aos/dispatcher_arch.h>
#include <domman/domman.h>


struct aos_rpc_interface_process_vtable aos_rpc_process_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .spawn = aos_rpc_local_process_spawn,
    .get_name = aos_rpc_local_process_get_name,
    .get_all_pids = aos_rpc_local_process_get_all_pids,
    .spawn_from_file = aos_rpc_local_process_spawn_from_file,
    .spawn_from_shell = aos_rpc_local_process_spawn_from_shell
};

#define N_CORES 2

struct aos_rpc *spawn_rpcs[N_CORES] = {
    NULL,
    NULL,
};

// needs to be called before the first spawn
// see usr/demeter/main.c
errval_t init_spawn_rpcs(void)
{
    struct aos_rpc *name_rpc = aos_rpc_get_name_channel();
    if (!name_rpc) {
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    bool blocking = true;
    for (size_t i = 0; i < N_CORES; i++) {
        debug_printf("connecting with spawn service in /core/monitor/%zu\n", i);
        errval_t err = aos_rpc_get_channel(&spawn_rpcs[i], blocking, i, NULL, NULL, "/core/monitor");
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
            return err;
        }
    }

    // all OK
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_process_spawn(struct aos_rpc *rpc /*unused*/, char *name,
                                     coreid_t core_id, domainid_t *newpid)
{
    assert(name);
    assert(newpid);

    if (N_CORES <= core_id) {
        debug_printf("%s: core id %zu not allowed. Only %zu cores available.\n",
                     __func__, (size_t)core_id, (size_t)N_CORES);
        return LIB_ERR_NOT_IMPLEMENTED;
    }

    // administration
    struct spawninfo si;
    si.binary_name = name;
    // TODO jmeier: rollback add_domain, when remote spawn fails
    errval_t err = add_domain(&si, core_id, newpid);
    if (err_is_fail(err)) {
        return err;
    }

    // actual spawn unit is in the monitor now
    struct aos_rpc *monitor_rpc = spawn_rpcs[core_id];
    if (!monitor_rpc) {
        // TODO: implement rollback as mentioned
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    err = aos_rpc_monitor_spawn_domain(monitor_rpc, name, core_id, -1, *newpid);
    if (err_is_fail(err)) {
        // TODO: implement rollback as mentioned
        return err;
    }

    // all OK
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_process_get_name(struct aos_rpc *rpc /*unused*/, domainid_t pid,
                                        char **name) {
    return get_domain_from_id(pid, name);
}

errval_t aos_rpc_local_process_get_all_pids(struct aos_rpc *rpc /*unused*/,
                                            domainid_t **pids, size_t *pid_count) {
    return get_all_domain_ids(pid_count, pids);
}

errval_t aos_rpc_local_process_spawn_from_file(struct aos_rpc *rpc /*unused*/, const char *path,
                                               const char *command_line, coreid_t core_id,
                                               int shell_id, domainid_t *newpid)
{
    assert(path);
    assert(command_line);
    assert(newpid);

    // administration
    struct spawninfo si;
    si.binary_name = strrchr(path, '/');
    if (!si.binary_name) {
        si.binary_name = path;
    }
    // TODO jmeier: rollback add_domain, when remote spawn fails
    errval_t err = add_domain(&si, core_id, newpid);
    if (err_is_fail(err)) {
        return err;
    }

    // actual spawn unit is in the monitor now
    struct aos_rpc *monitor_rpc = spawn_rpcs[core_id];
    if (!monitor_rpc) {
        // TODO: implement rollback as mentioned
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    err = aos_rpc_monitor_spawn_domain_from_file(monitor_rpc, path, command_line, core_id, shell_id, *newpid);
    if (err_is_fail(err)) {
        // TODO: implement rollback as mentioned
        return err;
    }

    // all OK
    return SYS_ERR_OK;
}

errval_t aos_rpc_local_process_spawn_from_shell(struct aos_rpc *rpc /*unused*/, char *name,
                                     coreid_t core_id, int shell_id, domainid_t *newpid)
{
    assert(name);
    assert(newpid);

    if (N_CORES <= core_id) {
        debug_printf("%s: core id %zu not allowed. Only %zu cores available.\n",
                     __func__, (size_t)core_id, (size_t)N_CORES);
        return LIB_ERR_NOT_IMPLEMENTED;
    }

    // administration
    struct spawninfo si;
    si.binary_name = name;
    // TODO jmeier: rollback add_domain, when remote spawn fails
    errval_t err = add_domain(&si, core_id, newpid);
    if (err_is_fail(err)) {
        return err;
    }

    // actual spawn unit is in the monitor now
    struct aos_rpc *monitor_rpc = spawn_rpcs[core_id];
    if (!monitor_rpc) {
        // TODO: implement rollback as mentioned
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    err = aos_rpc_monitor_spawn_domain(monitor_rpc, name, core_id, shell_id, *newpid);
    if (err_is_fail(err)) {
        // TODO: implement rollback as mentioned
        return err;
    }

    // all OK
    return SYS_ERR_OK;
}


// internal
void processserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_PROCESS_FID_SPAWN: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_process_spawn_req_payload));

            struct aos_rpc_process_spawn_req_payload *req_payload = context->recv_env.buf;

            size_t name_len = context->recv_env.buflen - sizeof(struct aos_rpc_process_spawn_req_payload);
            // note: we always send the zero terminator of a string, since strnlen returns the length of the string without
            //       the zero terminator, name_len should always be strictly larger than the actual string length
            assert(strnlen(req_payload->name, name_len) < name_len);

            domainid_t domain_id;
            err = aos_rpc_local_process_spawn(NULL, req_payload->name, req_payload->core_id, &domain_id);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &domain_id;
            context->send_env.buflen = sizeof(domain_id);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_PROCESS_FID_GET_NAME: {
            assert(context->recv_env.buflen >= sizeof(domainid_t));
            domainid_t *domain_id = context->recv_env.buf;
            char *name;
            err = aos_rpc_local_process_get_name(NULL, *domain_id, &name);
            if (err_is_fail(err)) {
                // on error, aos_rpc_server_process_get_name must not return a valid pointer
                assert(!name);
                send_reply(state, err);
                return;
            }

            // all OK
            size_t name_len = strlen(name) + 1;
            context->send_env.buf = name;
            context->send_env.buflen = name_len;
            send_reply(state, err);
            free(name);
            return;
        }

        case AOS_RPC_PROCESS_FID_GET_ALL_PID: {
            size_t size;
            domainid_t *domain_ids;
            err = aos_rpc_local_process_get_all_pids(NULL, &domain_ids, &size);
            if (err_is_fail(err)) {
                // on error, aos_rpc_server_process_get_all_pids must not return a valid pointer
                assert(!domain_ids);
                struct aos_rpc_process_get_all_pids_res_payload res_payload;
                res_payload.size = 0;
                // TODO: please check whether something is special here that it
                //       needs the buffer set also in error case.
                context->send_env.buf = &res_payload;
                context->send_env.buflen = sizeof(res_payload);
                send_reply(state, err);
                return;
            }

            size_t res_payload_size = sizeof(struct aos_rpc_process_get_all_pids_res_payload) + sizeof(*domain_ids)*size;
            struct aos_rpc_process_get_all_pids_res_payload *res_payload = malloc(res_payload_size);
            if (!res_payload) {
                struct aos_rpc_process_get_all_pids_res_payload res_payload_empty;
                res_payload_empty.size = 0;
                // TODO: please check whether something is special here that it
                //       needs the buffer set also in error case.
                context->send_env.buf = &res_payload_empty;
                context->send_env.buflen = sizeof(res_payload_empty);
                send_reply(state, err);
                free(domain_ids);
                return;
            }

            // all OK
            res_payload->size = size;
            memcpy(res_payload->domain_ids, domain_ids, sizeof(*domain_ids)*size);
            context->send_env.buf = res_payload;
            context->send_env.buflen = res_payload_size;
            send_reply(state, err);
            free(res_payload);
            free(domain_ids);
            return;
        }
        case AOS_RPC_PROCESS_FID_SPAWN_FROM_FILE: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_process_spawn_from_file_req_payload));

            struct aos_rpc_process_spawn_from_file_req_payload *req_payload = context->recv_env.buf;

            size_t data_len = context->recv_env.buflen - sizeof(struct aos_rpc_process_spawn_req_payload);
            // note: we always send the zero terminator of a string, since strnlen returns the
            //       length of the string without the zero terminator
            size_t path_len = strnlen(req_payload->data, data_len) + 1;
            assert(path_len <= data_len);
            const char *path = req_payload->data;

            size_t command_line_len = data_len - path_len;
            assert(strnlen(req_payload->data + path_len, command_line_len) < command_line_len);
            const char *command_line = req_payload->data + path_len;

            domainid_t domain_id;
            err = aos_rpc_local_process_spawn_from_file(NULL, path, command_line, req_payload->core_id, req_payload->shell_id, &domain_id);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &domain_id;
            context->send_env.buflen = sizeof(domain_id);
            send_reply(state, err);
            return;
        }
        case AOS_RPC_PROCESS_FID_SPAWN_FROM_SHELL: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_process_spawn_req_payload));

            struct aos_rpc_process_spawn_from_shell_req_payload *req_payload = context->recv_env.buf;

            size_t name_len = context->recv_env.buflen - sizeof(struct aos_rpc_process_spawn_from_shell_req_payload);
            // note: we always send the zero terminator of a string, since strnlen returns the length of the string without
            //       the zero terminator, name_len should always be strictly larger than the actual string length
            assert(strnlen(req_payload->name, name_len) < name_len);

            domainid_t domain_id;
            err = aos_rpc_local_process_spawn_from_shell(NULL, req_payload->name, req_payload->core_id, req_payload->shell_id, &domain_id);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &domain_id;
            context->send_env.buflen = sizeof(domain_id);
            send_reply(state, err);
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}


// public
// accepts processserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    processserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_processserver_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_processserver_event_handler);
}
