/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-10-24, Group C
 */

/**
 * global server specific functions.
 * version 2017-10-24, Group C
 */

#include <aos_rpc_server_global_p.h>

// all internal functions will be linked
// signatures are listed in internal API header files

//--- public: here, we only have the public handler

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    monitor_event_handler,
    intermonserver_event_handler,
    nameserver_event_handler,
    memserver_event_handler,
    processserver_event_handler,
    serialserver_event_handler,
    deviceserver_event_handler,
    filesystem_server_event_handler,
    networkserver_event_handler,
    blockdev_server_event_handler,
    shell_server_event_handler
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_global_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_global_event_handler);
}
