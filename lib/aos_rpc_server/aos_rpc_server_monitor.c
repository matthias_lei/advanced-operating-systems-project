/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * monitor specific functions
 */

#include <aos_rpc_server_monitor_p.h>
#include <aos_rpc_server_intermon_p.h>
#include <aos_rpc_server_device_p.h>
#include <aos_rpc_server_network_p.h>
#include <aos/rpc_shared/aos_rpc_shared_memory.h>

#include <aos/deferred.h>

#include <spawn/spawn.h>
#include <spawn/multiboot.h>

volatile bool bootstrap_memory_ready = false;
struct capref bootstrap_memory_service_cap;

volatile bool bootstrap_name_ready = false;
struct capref bootstrap_name_service_cap;

volatile bool bootstrap_process_ready = false;

// add specific includes here

struct aos_rpc_interface_monitor_vtable aos_rpc_monitor_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .request_rpc_forward = aos_rpc_local_monitor_request_rpc_forward,
    .spawn_domain = aos_rpc_local_monitor_spawn_domain,
    .spawn_domain_from_file = aos_rpc_local_monitor_spawn_domain_from_file,
    .bootstrap_offer_service = aos_rpc_local_monitor_bootstrap_offer_service,
    .offer_service = aos_rpc_local_monitor_offer_service,
    .get_bootstrap_pids = aos_rpc_local_monitor_get_bootstrap_pids,
    .get_multiboot_module_names = aos_rpc_local_monitor_get_multiboot_module_names,
    .get_multiboot_module = aos_rpc_local_monitor_get_multiboot_module
};

// handles the request without intermediary deserialization
// note: recv must be an allocated buffer of at least DEFAULT_RPC_BUFFER_SIZE and aligned to uintptr_t
static errval_t aos_rpc_local_monitor_request_rpc_forward_helper(struct aos_rpc_intermon_forward_req_payload *send,
                                                                 struct aos_rpc_intermon_forward_res_payload *recv,
                                                                 enum aos_rpc_intermon_capability_type recv_cap_type, struct capref *ret_cap)
{
    assert(send);
    assert(recv);
    assert(ret_cap);

    *ret_cap = NULL_CAP;

    errval_t err = SYS_ERR_OK;

    coreid_t server = AOS_RPC_ROUTING_SERVER_CORE_ID(send->routing);

    struct aos_rpc *irpc = aos_rpc_get_intermon_channel(server);
    if (!irpc) {
        err = MON_ERR_INVALID_CORE_ID;
        DEBUG_ERR(err, "unknown core ID\n");
        return err;
    }

    err = aos_rpc_intermon_forward_rpc_message(irpc, send, recv);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in intermon forward\n");
        return err;
    }

    if (recv_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
        err = aos_rpc_intermon_forge_capability(recv_cap_type, recv->cap_base, recv->cap_size, ret_cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_intermon_forge_capability\n");
            return err;
        }
    }

    return SYS_ERR_OK;
}

errval_t aos_rpc_local_monitor_request_rpc_forward(struct aos_rpc *rpc, enum aos_rpc_interface interface,
                                                   size_t enumerator, uint32_t fid, routing_info_t routing,
                                                   struct aos_rpc_send_msg_env *send_env,
                                                   struct aos_rpc_recv_msg_env *recv_env)
{
    // called from the monitors themselves
    // -> serialization / deserialization as in the _remote_ client version
    assert(rpc);
    assert(send_env);
    assert(recv_env);

    errval_t err = SYS_ERR_OK;

    // to avoid memory allocations during forwarding
    // note: both MUST be aligned to uintptr_t
    uintptr_t memory_buffer_req[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];
    uintptr_t memory_buffer_res[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];

    // prepare message
    size_t env_size = sizeof(struct aos_rpc_send_msg_env) + send_env->buflen;
    //size_t size = sizeof(struct aos_rpc_intermon_forward_req_payload) + env_size;
    struct aos_rpc_intermon_forward_req_payload *req;
    struct aos_rpc_intermon_forward_res_payload *res;
    req = (void *)memory_buffer_req;
    res = (void *)memory_buffer_res;
    memset(req, 0, DEFAULT_RPC_BUFFER_SIZE);
    memset(res, 0, DEFAULT_RPC_BUFFER_SIZE);

    req->interface = interface;
    req->enumerator = enumerator;
    req->fid = fid;
    req->routing = routing;
    req->send_cap_type = send_env->cap_type;
    req->recv_cap_type = recv_env->expected_cap_type;
    req->env_size = env_size;
    memcpy(req->env, send_env, sizeof(struct aos_rpc_send_msg_env));
    if (0 < send_env->buflen) {
        memcpy(req->env + sizeof(struct aos_rpc_send_msg_env), send_env->buf, send_env->buflen);
    }

    if (send_env->cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
        // note: only limited types of caps can be transferred, of course
        struct frame_identity info;
        // works also for RAM
        err = frame_identify(send_env->cap, &info);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "frame_identify for cap failed\n");
            return err;
        }
        req->cap_base = info.base;
        req->cap_size = info.bytes;
    }

    // direct call instead of sending/receiving
    enum aos_rpc_intermon_capability_type recv_cap_type = req->recv_cap_type;
    struct capref cap;
    err = aos_rpc_local_monitor_request_rpc_forward_helper(req, res, recv_cap_type, &cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from local RPC call.\n", __func__);
        return err;
    }

    // NOTE: deliberately NO check of the embedded return code from RPC
    // it is just forwarded to recipient

    // handle return payload
    // NOTE: the returning capref has already been forged in the helper function
    // the question here is mainly whether memory must be allocated to be freed for the caller
    // (see expectations) or whether a plain memcpy is enough.
    //
    // technical note: NO malloc() is allowed on the entire journey over the seas
    // however, being back now, it is allowed if needed.
    // Nevertheless: good RPC remote stubs requiring a capability transfer (and thus rerouting)
    // have small payload only and also fixed size / pre-allocated recv buffers
    // This handling here is for full compatibility only

    bool has_cap = recv_env->expected_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE;
    struct aos_rpc_recv_msg_env *env = (void *)res->env;
    env->buf = (char *)env + sizeof(struct aos_rpc_recv_msg_env);
    recv_env->header = env->header;

    if (has_cap) {
        recv_env->cap = cap;
    }

    if (0 < env->buflen) {
        if (!recv_env->buf) {
            if (recv_env->buflen == AOS_RPC_EXPECT_PAYLOAD) {
                // malloc allowed here
                recv_env->buf = malloc(env->buflen);
                if (!recv_env->buf) {
                    err = LIB_ERR_MALLOC_FAIL;
                    DEBUG_ERR(err, "payload expected; malloc() failed\n");
                    return err;
                }
            }
            else {
                err = LIB_ERR_SHOULD_NOT_GET_HERE;
                DEBUG_ERR(err, "no payload expected but received\n");
                return err;
            }
        }
        else {
            if (recv_env->buflen < env->buflen) {
                err = LIB_ERR_SHOULD_NOT_GET_HERE;
                DEBUG_ERR(err, "available buffer %zu B too small for received payload %zu B\n",
                          recv_env->buflen, env->buflen);
                return err;
            }
        }

        memcpy(recv_env->buf, env->buf, env->buflen);
    }
    recv_env->buflen = env->buflen;

    return SYS_ERR_OK;
}

errval_t aos_rpc_local_monitor_spawn_domain(struct aos_rpc *rpc /*unused*/, const char *name, coreid_t core_id, int shell_id, domainid_t pid)
{
    assert(name);

    // spawn
    if (core_id == disp_get_core_id()) {
        // local
        return spawn_load_by_name_with_id(name, shell_id, pid);
    }
    else {
        // remote
        debug_printf("%s: remote spawn should not get here. Error in client code.\n", __func__);
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
}

errval_t aos_rpc_local_monitor_spawn_domain_from_file(struct aos_rpc *rpc /*unused*/, const char *path,
                                                      const char *command_line, coreid_t core_id,
                                                      int shell_id, domainid_t pid)
{
    assert(path);
    assert(command_line);

    // spawn
    if (core_id == disp_get_core_id()) {
        // local
        return spawn_load_by_path_with_id(path, command_line, shell_id, pid);
    }
    else {
        // remote
        debug_printf("%s: remote spawn should not get here. Error in client code.\n", __func__);
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
}

errval_t aos_rpc_local_monitor_bootstrap_offer_service(struct aos_rpc *rpc /*unused*/, enum aos_rpc_interface interface, struct capref service_cap)
{
    switch (interface) {
        case AOS_RPC_INTERFACE_MEMORY:
            bootstrap_memory_service_cap = service_cap;
            bootstrap_memory_ready = true;
            return SYS_ERR_OK;

        case AOS_RPC_INTERFACE_NAME:
            bootstrap_name_service_cap = service_cap;
            bootstrap_name_ready = true;
            return SYS_ERR_OK;

        default:
            return LIB_ERR_NOT_IMPLEMENTED;
    }
}

#define MAX_SHELL_INSTANCES 1024
size_t monitor_shell_rpcs_max = MAX_SHELL_INSTANCES;
struct aos_rpc *monitor_shell_rpcs[MAX_SHELL_INSTANCES] = { NULL };
struct aos_rpc *monitor_service_rpcs[AOS_RPC_FG_SIZE] = { NULL };

errval_t aos_rpc_local_monitor_offer_service(struct aos_rpc *rpc /* unused */, enum aos_rpc_interface interface, size_t enumerator,
                                             enum aos_rpc_chan_driver chan_type, struct capref service_cap)
{
    switch (interface) {
        case AOS_RPC_NAME_FG:
        case AOS_RPC_MEMORY_FG:
        case AOS_RPC_PROCESS_FG:
        case AOS_RPC_SERIAL_FG:
        case AOS_RPC_DEVICE_FG:
        case AOS_RPC_FILESYSTEM_FG:
        case AOS_RPC_NETWORK_FG:
        case AOS_RPC_BLOCKDEV_FG:
        case AOS_RPC_INTERFACE_SHELL:
            break;

        case AOS_RPC_COMMON_FG:
        case AOS_RPC_MONITOR_FG:
        case AOS_RPC_INTERMON_FG:
        default:
            debug_printf("%s: interface %zu not implemented\n", __func__, (size_t)interface);
            return LIB_ERR_NOT_IMPLEMENTED;
    }

    switch (chan_type) {
        case AOS_RPC_CHAN_DRIVER_LMP:
        case AOS_RPC_CHAN_DRIVER_FLMP:
            break;

        default:
            debug_printf("%s: chan_type %zu not implemented\n", __func__, (size_t)chan_type);
            return LIB_ERR_NOT_IMPLEMENTED;
    }

    if (enumerator >= monitor_shell_rpcs_max) {
        debug_printf("%s: max %zu instances allowed\n", __func__, (size_t)monitor_shell_rpcs_max);
        return LIB_ERR_NOT_IMPLEMENTED;
    }

    // create RPC
    coreid_t core = disp_get_core_id();
    struct aos_rpc *new_rpc;
    errval_t err = aos_rpc_connect_with_service(service_cap, interface, chan_type, AOS_RPC_ROUTING_SET(core, core), &new_rpc, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not connect with service\n");
        return err;
    }

    switch (interface) {
        case AOS_RPC_INTERFACE_SHELL:
            monitor_shell_rpcs[enumerator] = new_rpc;
            break;

        default:
            monitor_service_rpcs[interface] = new_rpc;
            break;
    }

    return err;
}


// avoid circular includes; from usr/init/bootstrap.c
extern const char *bootstrap_domain_names[];
extern coreid_t bootstrap_cores[];
extern const size_t bootstrap_domains_count;

errval_t aos_rpc_local_monitor_get_bootstrap_pids(struct aos_rpc *rpc /*unused*/, struct aos_rpc_get_bootstrap_pids_res_payload *res)
{
    assert(res);

    memset(res, 0, sizeof(*res));
    for (size_t i = 0; i < bootstrap_domains_count; i++) {
        res->cores[i] = bootstrap_cores[i];
        strncpy(res->names[i], bootstrap_domain_names[i], MONITOR_BOOTSTRAP_MAX_NAME_LEN - 1);
    }
    res->count = bootstrap_domains_count;

    // all OK
    return SYS_ERR_OK;
}

extern struct bootinfo *bi;

errval_t aos_rpc_local_monitor_get_multiboot_module_names(struct aos_rpc *rpc, char **name_list, size_t *list_len)
{
    char *list = NULL;
    size_t len = 0;
    for(size_t i = 0; i < bi->regions_length; i++) {
        if (bi->regions[i].mr_type != RegionType_Module) {
            continue;
        }
        const char *modname = multiboot_module_name(&bi->regions[i]);
        if (!modname) {
            continue;
        }
        size_t modname_len = strlen(modname) + 1;
        list = realloc(list, len + modname_len + sizeof(size_t) + 1);
        if (!list) {
            return LIB_ERR_MALLOC_FAIL;
        }
        memcpy(list + len, modname, modname_len);
        size_t *modsize = (void*)(list + len + modname_len);
        *modsize = bi->regions[i].mrmod_size;
        len += modname_len + sizeof(size_t);
    }

    list[len] = '\0';
    *name_list = list;
    *list_len = len + 1;

    return SYS_ERR_OK;
}

errval_t aos_rpc_local_monitor_get_multiboot_module(struct aos_rpc *rpc, const char *name, struct capref *frame)
{
    struct mem_region *mr = multiboot_find_module(bi, name);
    if (!mr) {
        return SPAWN_ERR_FIND_MODULE;
    }

    *frame = (struct capref) {
        .cnode = cnode_module,
        .slot = mr->mrmod_slot
    };

    return SYS_ERR_OK;
}


// internal
void monitor_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_MONITOR_FID_REQUEST_RPC_FORWARD: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_intermon_forward_req_payload));
            // forwarded directly from here without deserialization
            // but we keep already the info here whether some cap is expected
            struct aos_rpc_intermon_forward_req_payload *req = context->recv_env.buf;
            enum aos_rpc_intermon_capability_type recv_cap_type = req->recv_cap_type;

            // no mallocs allowed: use a default buffer
            // note: must be uintptr_t aligned
            uintptr_t memory_buffer_res[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];
            struct aos_rpc_intermon_forward_res_payload *res = (void *)memory_buffer_res;
            struct capref cap;
            err = aos_rpc_local_monitor_request_rpc_forward_helper(req, res, recv_cap_type, &cap);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // forward the buffer
            context->send_env.buf = res;
            context->send_env.buflen = sizeof(struct aos_rpc_intermon_forward_res_payload) + res->env_size;

            // freshly minted cap
            context->send_env.cap = cap;

            send_reply(state, err);
            return;
        }

        case AOS_RPC_MONITOR_FID_SPAWN_DOMAIN: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_spawn_domain_req_payload));
            struct aos_rpc_spawn_domain_req_payload *req = context->recv_env.buf;
            err = aos_rpc_local_monitor_spawn_domain(NULL, req->name, req->core_id, req->shell_id, req->pid);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MONITOR_FID_SPAWN_DOMAIN_FROM_FILE: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_spawn_domain_from_file_req_payload));
            struct aos_rpc_spawn_domain_from_file_req_payload *req = context->recv_env.buf;

            size_t data_len = context->recv_env.buflen - sizeof(struct aos_rpc_spawn_domain_from_file_req_payload);
            // note: we always send the zero terminator of a string, since strnlen returns the
            //       length of the string without the zero terminator
            size_t path_len = strnlen(req->data, data_len) + 1;
            assert(path_len <= data_len);
            const char *path = req->data;

            size_t command_line_len = data_len - path_len;
            assert(strnlen(req->data + path_len, command_line_len) < command_line_len);
            const char *command_line = req->data + path_len;

            err = aos_rpc_local_monitor_spawn_domain_from_file(NULL, path, command_line, req->core_id, req->shell_id, req->pid);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MONITOR_FID_BOOTSTRAP_OFFER_SERVICE: {
            assert(context->recv_env.buflen >= sizeof(enum aos_rpc_interface));
            enum aos_rpc_interface *interface = context->recv_env.buf;
            err = aos_rpc_local_monitor_bootstrap_offer_service(NULL, *interface, context->recv_env.cap);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MONITOR_FID_OFFER_SERVICE: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_offer_service_req_payload));
            struct aos_rpc_offer_service_req_payload *p = context->recv_env.buf;
            err = aos_rpc_local_monitor_offer_service(NULL, p->interface, p->enumerator, p->chan_type, context->recv_env.cap);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MONITOR_FID_GET_BOOTSTRAP_PIDS: {
            struct aos_rpc_get_bootstrap_pids_res_payload res;
            err = aos_rpc_local_monitor_get_bootstrap_pids(NULL, &res);
            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);
            send_reply(state, err);

            #define SHORT_SLEEP 10
            barrelfish_msleep(SHORT_SLEEP);
            bootstrap_process_ready = true;
            return;
        }

        case AOS_RPC_MONITOR_FID_GET_MULTIBOOT_MODULE_NAMES: {
            assert(context->recv_env.buflen == 0);
            char *name_list;
            err = aos_rpc_local_monitor_get_multiboot_module_names(NULL, &name_list, &context->send_env.buflen);
            context->send_env.buf = name_list;
            send_reply(state, err);
            free(name_list);
            return;
        }

        case AOS_RPC_MONITOR_FID_GET_MULTIBOOT_MODULE: {
            err = aos_rpc_local_monitor_get_multiboot_module(NULL, context->recv_env.buf, &context->send_env.cap);
            send_reply(state, err);
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}




// public
// accepts monitor (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    monitor_event_handler,
    intermonserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    deviceserver_event_handler,
    NULL,
    networkserver_event_handler,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_monitor_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_monitor_event_handler);
}
