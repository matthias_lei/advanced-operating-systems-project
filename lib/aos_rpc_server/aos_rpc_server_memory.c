/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * version 2017-10-24, Group C
 */

/**
 * memory server specific functions.
 * version 2017-10-24, Group C
 */

#include <aos_rpc_server_memory_p.h>
#include <nameserver_types.h>
#include <hashtable/serializable_key_value_store.h>
#include <aos/connect/lmp.h>

// add specific includes here
#include <mm/mm.h>

struct aos_rpc_interface_memory_vtable aos_rpc_memory_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .get_ram_cap = aos_rpc_local_get_ram_cap,
    .load_ram = aos_rpc_local_load_ram,
    .memoryservice_register = aos_rpc_local_memoryservice_register,
    .get_ram_info = aos_rpc_local_get_ram_info,
};

struct mm dionysos_mm;

errval_t aos_rpc_local_get_ram_cap(struct aos_rpc *rpc /*unused*/, size_t bytes, size_t align,
                                   struct capref *retcap, size_t *ret_bytes) {
    *ret_bytes = bytes;
    errval_t err = mm_alloc_aligned(&dionysos_mm, bytes, align, retcap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "memserver_event_handler failed during ram_alloc_aligned\n");
        *retcap = NULL_CAP;
        *ret_bytes = 0;
    }

    return err;
}

errval_t aos_rpc_local_load_ram(struct aos_rpc *rpc /*unused*/, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size)
{
debug_printf("%s: hello\n", __func__);
    errval_t err = mm_add(&dionysos_mm, ram_cap, ram_base, ram_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Warning: adding RAM region (%p/%zu) FAILED", ram_base, ram_size);
        return err;
    }

    err = slot_prealloc_refill(dionysos_mm.slot_alloc_inst);
    if (err_is_fail(err) && err_no(err) != MM_ERR_SLOT_MM_ALLOC) {
        DEBUG_ERR (err, "in slot_prealloc_refill() while initializing memory allocator");
        return err;
    }

    // switch to regular memory allocation using MM
    err = ram_alloc_set(NULL);
    if (err_is_fail(err)) {
        err = err_push(err, LIB_ERR_RAM_ALLOC_SET);
        DEBUG_ERR(err, "Cannot switch RAM allocation to memory service");
        return err;
    }

    // at least during debugging
    // can happen after the reply has been sent
    debug_printf("Received %" PRIu64 " MiB of physical memory.\n", ram_size / 1024 / 1024);
    mm_dump_mmnodes(&dionysos_mm);

    return SYS_ERR_OK;
}

extern void memory_service_register_callback(void);

errval_t aos_rpc_local_memoryservice_register(struct aos_rpc *rpc, struct capref nameserver_cap)
{
    // create RPC
    struct aos_rpc *name_rpc;
    errval_t err = aos_rpc_connect_with_service(nameserver_cap, AOS_RPC_INTERFACE_NAME,
                                                AOS_RPC_CHAN_DRIVER_LMP,
                                                AOS_RPC_ROUTING_SET(0, 0), &name_rpc, NULL);
    if (err_is_fail(err)) {
        return err;
    }
    set_name_rpc(name_rpc);

    memory_service_register_callback();

    // all OK
    return SYS_ERR_OK;
}


errval_t aos_rpc_local_get_ram_info(struct aos_rpc *rpc /*unused*/, size_t *ret_allocated,
                                    size_t *ret_free, size_t *ret_free_largest)
{
    // TODO
    return LIB_ERR_NOT_IMPLEMENTED;
}

// internal
void memserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;
    switch (state->fid) {
        case AOS_RPC_MEMORY_FID_GET_RAM_CAP: {
            struct aos_rpc_comm_context *context = state->context;
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_get_ram_cap_req_payload));

            struct aos_rpc_get_ram_cap_req_payload *req_payload = context->recv_env.buf;

            size_t retsize;
            err = aos_rpc_local_get_ram_cap(NULL, req_payload->size, req_payload->align, &context->send_env.cap, &retsize);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &retsize;
            context->send_env.buflen = sizeof(retsize);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MEMORY_FID_LOAD_RAM: {
            struct aos_rpc_comm_context *context = state->context;
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_load_ram_req_payload));

            struct aos_rpc_load_ram_req_payload *req = context->recv_env.buf;
            err = aos_rpc_local_load_ram(NULL, context->recv_env.cap, req->base, req->size);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MEMORY_FID_REGISTER: {
            struct aos_rpc_comm_context *context = state->context;
            err = aos_rpc_local_memoryservice_register(NULL, context->recv_env.cap);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_MEMORY_FID_GET_INFO: {
            struct aos_rpc_comm_context *context = state->context;

            struct aos_rpc_get_ram_info_res_payload res;
            err = aos_rpc_local_get_ram_info(NULL, &res.allocated, &res.free, &res.free_largest);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);
            send_reply(state, err);
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}


// public
// accepts memserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    NULL,
    memserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_memserver_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_memserver_event_handler);
}
