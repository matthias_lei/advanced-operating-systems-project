/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * see AOS_RPC_HEADER_WORD that defines the function group, function ID, and status.
 *
 * AOS class 2017, Group C: Individual project
 * nameserver specific functions.
 * version 2017-12-01, Pirmin Schmid
 */

#include <aos_rpc_server_name_p.h>

#include <aos/rpc_shared/aos_rpc_shared_name.h>

#include <nameserver.h>

#include <hashtable/serializable_key_value_store.h>

// add specific includes here

// local functions would not make sense
struct aos_rpc_interface_name_vtable aos_rpc_name_local_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT,
    .service_register = NULL,
    .service_deregister = NULL,
    .service_ping = NULL,
    .service_update = NULL,
    .lookup_service_by_path = NULL,
    .lookup_service_by_short_name = NULL,
    .find_any_service_by_path = NULL,
    .find_any_service_by_filter = NULL,
    .get_service_info = NULL,
    .bind_service_lowlevel = NULL
};

// internal
void nameserver_event_handler(struct aos_rpc_server_comm_state *state) {
    assert(state);
    assert(state->context);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context *context = state->context;

    switch (state->fid) {
        case AOS_RPC_NAME_FID_SERVICE_REGISTER: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_name_service_register_req_payload));

            struct aos_rpc_name_service_register_req_payload *req_payload = context->recv_env.buf;
            struct service_registration_request *request = service_registration_request_deserialize(req_payload);
            if (!request) {
                err = LIB_ERR_MALLOC_FAIL;
                send_reply(state, err);
                return;
            }

            struct service_registration_reply *reply = NULL;
            err = service_register_part1(request, context->recv_env.cap, &reply);
            if (err_is_fail(err)) {
                // req_payload == recv_env.buf is freed by the generic event handler
                send_reply(state, err);
                return;
            }

            service_handle_t handle = reply->handle;
            uint64_t registration_key = reply->registration_key;

            debug_printf("%s: registration of %s as %s with handle %" PRIu64 "\n",
                         __func__, request->abs_path, reply->name, reply->handle);

            struct aos_rpc_name_service_register_res_payload *res_payload = service_registration_reply_serialize(reply);
            if (!res_payload) {
                err = LIB_ERR_MALLOC_FAIL;
                free(reply);
                // req_payload == recv_env.buf is freed by the generic event handler
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = res_payload;
            context->send_env.buflen = res_payload->buf_size;
            send_reply(state, err);
            // cleanup afterwards
            // note: req_payload will be freed by the generic event handler
            free(reply);
            free(res_payload);

            // after the message has been sent, complete part2 (i.e. UMP connection)
            err = service_register_part2(handle, registration_key);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "error in service_register_part2()\n");
                // no option to send error code
                // see header file how the client can still check for full registration
            }
            return;
        }

        case AOS_RPC_NAME_FID_SERVICE_DEREGISTER: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_name_handle_key_req_payload));

            struct aos_rpc_name_handle_key_req_payload *req = context->recv_env.buf;

            err = service_deregister(req->handle, req->registration_key);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK: no payload
            send_reply(state, err);
            return;
        }

        case AOS_RPC_NAME_FID_SERVICE_PING: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_name_handle_key_req_payload));

            struct aos_rpc_name_handle_key_req_payload *req = context->recv_env.buf;

            err = service_ping(req->handle, req->registration_key);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK: no payload
            send_reply(state, err);
            return;
        }

        case AOS_RPC_NAME_FID_SERVICE_UPDATE: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_name_update_req_payload));

            struct aos_rpc_name_update_req_payload *req = context->recv_env.buf;
            struct serializable_key_value_store *store = deserialize_kv_store(req->serialized_store, 0);
            if (!store) {
                err = LIB_ERR_MALLOC_FAIL;
                send_reply(state, err);
                return;
            }

            err = service_update(req->handle, req->registration_key, store);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK: no payload
            send_reply(state, err);
            destroy_kv_store_borrowed_buffer(&store);
            return;
        }

        case AOS_RPC_NAME_FID_LOOKUP_SERVICE_BY_PATH: {
            assert(context->recv_env.buflen >= 2); // at least one character must be there

            char *req_payload = context->recv_env.buf;
            service_handle_t ret_handle;

            err = lookup_service_by_path(req_payload, &ret_handle);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &ret_handle;
            context->send_env.buflen = sizeof(ret_handle);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_NAME_FID_LOOKUP_SERVICE_BY_SHORT_NAME: {
            assert(context->recv_env.buflen >= 2); // at least one character must be there

            char *req_payload = context->recv_env.buf;
            service_handle_t ret_handle;

            err = lookup_service_by_short_name(req_payload, &ret_handle);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &ret_handle;
            context->send_env.buflen = sizeof(ret_handle);
            send_reply(state, err);
            return;
        }

        case AOS_RPC_NAME_FID_FIND_ANY_SERVICE_BY_PATH: {
            assert(context->recv_env.buflen >= 2); // at least one character must be there

            char *req_payload = context->recv_env.buf;

            service_handle_t *handles;
            size_t count;
            err = find_any_service_by_path(req_payload, &handles, &count);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK -> put result into payload structure
            size_t reply_len = sizeof(struct aos_rpc_name_find_res_payload) + count * sizeof(service_handle_t);
            struct aos_rpc_name_find_res_payload *reply = malloc(reply_len);
            if (!reply) {
                err = LIB_ERR_MALLOC_FAIL;
                send_reply(state, err);
                free(handles);
                return;
            }
            reply->count = count;
            memcpy(reply->handles, handles, count * sizeof(service_handle_t));

            context->send_env.buf = reply;
            context->send_env.buflen = reply_len;
            send_reply(state, err);
            free(handles);
            free(reply);
            return;
        }

        case AOS_RPC_NAME_FID_FIND_ANY_SERVICE_BY_FILTER: {
            assert(context->recv_env.buflen >= 1);

            struct serialized_key_value_store *req = context->recv_env.buf;
            struct serializable_key_value_store *filter = deserialize_kv_store(req, 0);

            service_handle_t *handles;
            size_t count;
            err = find_any_service_by_filter(filter, &handles, &count);
            if (err_is_fail(err)) {
                send_reply(state, err);
                destroy_kv_store_borrowed_buffer(&filter);
                return;
            }

            // all OK -> put result into payload structure
            size_t reply_len = sizeof(struct aos_rpc_name_find_res_payload) + count * sizeof(service_handle_t);
            struct aos_rpc_name_find_res_payload *reply = malloc(reply_len);
            if (!reply) {
                err = LIB_ERR_MALLOC_FAIL;
                send_reply(state, err);
                free(handles);
                destroy_kv_store_borrowed_buffer(&filter);
                return;
            }
            reply->count = count;
            memcpy(reply->handles, handles, count * sizeof(service_handle_t));

            context->send_env.buf = reply;
            context->send_env.buflen = reply_len;
            send_reply(state, err);
            free(handles);
            free(reply);
            destroy_kv_store_borrowed_buffer(&filter);
            return;
        }

        case AOS_RPC_NAME_FID_GET_SERVICE_INFO: {
            assert(context->recv_env.buflen >= sizeof(service_handle_t));

            service_handle_t *handle = context->recv_env.buf;
            struct serialized_key_value_store *serialized_store;
            err = get_service_info_serialized(*handle, &serialized_store);
            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = serialized_store;
            context->send_env.buflen = serialized_store->buf_size;
            send_reply(state, err);
            free(serialized_store);
            return;
        }

        case AOS_RPC_NAME_FID_BIND_SERVICE_LOWLEVEL: {
            assert(context->recv_env.buflen >= sizeof(struct aos_rpc_name_bind_service_lowlevel_req_payload));

            struct aos_rpc_name_bind_service_lowlevel_req_payload *req = context->recv_env.buf;

            struct aos_rpc_name_bind_service_lowlevel_res_payload res;

            struct capref cap;
            bool self;
            void *data;
            err = bind_service_lowlevel_part1(req->handle, req->core_id,
                                              &res.routing_info,
                                              &res.interface,
                                              &res.enumerator,
                                              &res.chan_type,
                                              &cap, &self, &data);

            if (err_is_fail(err)) {
                send_reply(state, err);
                return;
            }

            // all OK
            context->send_env.buf = &res;
            context->send_env.buflen = sizeof(res);
            context->send_env.cap = cap;
            send_reply(state, err);

            // complete 2nd part after sending the reply
            bind_service_lowlevel_part2(res.chan_type, cap, self, data);
            return;
        }

        default:
            debug_printf("%s: ERROR: unknown function id %p.\n", __func__, state->fid);
            err = SYS_ERR_NOT_IMPLEMENTED;
            send_reply(state, err);
            return;
    }
}


// public
// accepts nameserver (and testing) events

static aos_rpc_fg_event_handler_t allowed_fg_handlers[] = {
    common_event_handler,
    NULL,
    NULL,
    nameserver_event_handler,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

STATIC_ASSERT(sizeof(allowed_fg_handlers) / sizeof(aos_rpc_fg_event_handler_t) == AOS_RPC_FG_SIZE, "allowed_fg_handlers[] size mismatch");

// public
void aos_rpc_nameserver_event_handler(void *arg) {
    assert(arg);

    struct aos_rpc_server_comm_state *state = arg;

    generic_event_handler(state, allowed_fg_handlers, aos_rpc_nameserver_event_handler);
}
