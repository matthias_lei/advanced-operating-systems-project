#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_BLOCKDEV_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_BLOCKDEV_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_blockdev.h>

void blockdev_server_event_handler(struct aos_rpc_server_comm_state *state);

errval_t aos_rpc_local_get_block_size(struct aos_rpc *rpc, size_t *block_size);
errval_t aos_rpc_local_read_block(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen);
errval_t aos_rpc_local_write_block(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_BLOCKDEV_P_
