#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_SHELL_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_SHELL_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_shell.h>

void shell_server_event_handler(struct aos_rpc_server_comm_state *state);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_SHELL_P_
