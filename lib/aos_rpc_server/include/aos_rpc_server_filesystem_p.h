#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_FILESYSTEM_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_FILESYSTEM_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_filesystem.h>

void filesystem_server_event_handler(struct aos_rpc_server_comm_state *state);

struct fs_fileinfo;
typedef void * fs_filehandle_t;
typedef void * fs_dirhandle_t;

errval_t aos_rpc_local_open(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
errval_t aos_rpc_local_create(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
errval_t aos_rpc_local_remove(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_local_read(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read);
errval_t aos_rpc_local_write(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written);
errval_t aos_rpc_local_truncate(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes);
errval_t aos_rpc_local_tell(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos);
errval_t aos_rpc_local_stat(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info);
errval_t aos_rpc_local_seek(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset);
errval_t aos_rpc_local_close(struct aos_rpc *rpc, fs_filehandle_t inhandle);
errval_t aos_rpc_local_opendir(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle);
errval_t aos_rpc_local_dir_read_next(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info);
errval_t aos_rpc_local_closedir(struct aos_rpc *rpc, fs_dirhandle_t inhandle);
errval_t aos_rpc_local_mkdir(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_local_rmdir(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_local_mount(struct aos_rpc *rpc, const char *path, const char *uri);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_FILESYSTEM_P_
