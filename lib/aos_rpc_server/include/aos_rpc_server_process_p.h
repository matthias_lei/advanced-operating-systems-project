#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_PROCESS_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_PROCESS_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_process.h>

errval_t aos_rpc_local_process_spawn(struct aos_rpc *rpc, char *name,
                                     coreid_t core_id, domainid_t *newpid);

errval_t aos_rpc_local_process_get_name(struct aos_rpc *rpc, domainid_t pid,
                                        char **name);

errval_t aos_rpc_local_process_get_all_pids(struct aos_rpc *rpc,
                                            domainid_t **pids, size_t *pid_count);

errval_t aos_rpc_local_process_spawn_from_file(struct aos_rpc *rpc, const char *path,
                                               const char *command_line, coreid_t core_id,
                                               int shell_id, domainid_t *newpid);

errval_t aos_rpc_local_process_spawn_from_shell(struct aos_rpc *rpc, char *name,
                                     coreid_t core_id, int shell_id, domainid_t *newpid);

void processserver_event_handler(struct aos_rpc_server_comm_state *state);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_PROCESS_P_
