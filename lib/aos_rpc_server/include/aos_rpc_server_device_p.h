#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_DEVICE_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_DEVICE_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_device.h>

void deviceserver_event_handler(struct aos_rpc_server_comm_state *state);

errval_t aos_rpc_local_get_device_cap(struct aos_rpc *rpc, lpaddr_t paddr, size_t bytes, struct capref *frame);
errval_t aos_rpc_local_get_irq_cap(struct aos_rpc *rpc, struct capref *cap);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_DEVICE_P_
