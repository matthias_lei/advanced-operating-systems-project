#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_INTERMON_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_INTERMON_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_intermon.h>

errval_t aos_rpc_intermon_forge_capability(enum aos_rpc_intermon_capability_type cap_type,
                                           genpaddr_t cap_base, gensize_t cap_size,
                                           struct capref *ret_cap);

errval_t aos_rpc_local_intermon_request_bootstrap_info(struct aos_rpc *rpc, coreid_t core_id, struct aos_intermon_bootstrap_res *info);

errval_t aos_rpc_local_intermon_forward_rpc_message(struct aos_rpc *rpc, struct aos_rpc_intermon_forward_req_payload *send,
                                                    struct aos_rpc_intermon_forward_res_payload *recv);

void intermonserver_event_handler(struct aos_rpc_server_comm_state *state);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_INTERMON_P_
