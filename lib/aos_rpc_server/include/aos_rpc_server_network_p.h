#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_NETWORK_P_
	
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_NETWORK_P_
	

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_network.h>

#include <sys/socket.h>

void networkserver_event_handler(struct aos_rpc_server_comm_state *state);

errval_t aos_rpc_local_accept(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_local_bind(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
errval_t aos_rpc_local_connect(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
errval_t aos_rpc_local_getpeername(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_local_getsockname(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_local_getsockopt(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len);
errval_t aos_rpc_local_listen(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, int backlog);
errval_t aos_rpc_local_recv(struct aos_rpc *rpc,ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags);
errval_t aos_rpc_local_recvfrom(struct aos_rpc *rpc,ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_local_recvmsg(struct aos_rpc *rpc,ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags);
errval_t aos_rpc_local_send(struct aos_rpc *rpc,ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags);
errval_t aos_rpc_local_sendmsg(struct aos_rpc *rpc,ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags);
errval_t aos_rpc_local_sendto(struct aos_rpc *rpc,ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
errval_t aos_rpc_local_setsockopt(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len);
errval_t aos_rpc_local_shutdown(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, int how);
errval_t aos_rpc_local_socket(struct aos_rpc *rpc,int *ret_value, int *err_no, int domain, int type, int protocol);
errval_t aos_rpc_local_socketpair(struct aos_rpc *rpc,int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_NETWORK_P_