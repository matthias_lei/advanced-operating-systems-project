#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_GLOBAL_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_GLOBAL_P_

#include <aos_rpc_server_common_p.h>

#include <aos_rpc_server_blockdev_p.h>
#include <aos_rpc_server_device_p.h>
#include <aos_rpc_server_intermon_p.h>
#include <aos_rpc_server_filesystem_p.h>
#include <aos_rpc_server_memory_p.h>
#include <aos_rpc_server_monitor_p.h>
#include <aos_rpc_server_name_p.h>
#include <aos_rpc_server_network_p.h>
#include <aos_rpc_server_process_p.h>
#include <aos_rpc_server_serial_p.h>
#include <aos_rpc_server_shell_p.h>

#include <aos_rpc_server/aos_rpc_server_global.h>

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_GLOBAL_P_
