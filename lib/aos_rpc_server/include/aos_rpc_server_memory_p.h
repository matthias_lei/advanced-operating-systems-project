#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_MEMORY_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_MEMORY_P_

#include <aos_rpc_server_common_p.h>
#include <aos_rpc_server/aos_rpc_server_memory.h>

errval_t aos_rpc_local_get_ram_cap(struct aos_rpc *rpc, size_t size, size_t align,
                                   struct capref *retcap, size_t *ret_size);

errval_t aos_rpc_local_load_ram(struct aos_rpc *rpc /*unused*/, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size);

errval_t aos_rpc_local_memoryservice_register(struct aos_rpc *rpc, struct capref nameserver_cap);


errval_t aos_rpc_local_get_ram_info(struct aos_rpc *rpc /*unused*/, size_t *ret_allocated,
                                    size_t *ret_free, size_t *ret_free_largest);

void memserver_event_handler(struct aos_rpc_server_comm_state *state);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_MEMORY_P_
