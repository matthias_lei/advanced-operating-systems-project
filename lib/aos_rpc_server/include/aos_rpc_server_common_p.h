#ifndef LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_COMMON_P_
#define LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_COMMON_P_

#include <aos/rpc_shared/aos_rpc_shared_common.h>
#include <aos_rpc_server/aos_rpc_server_common.h>

#define AOS_RPC_INTERFACE_COMMON_LOCAL_VTABLE_INIT {  \
    .send_number = aos_rpc_local_send_number,         \
    .send_string = aos_rpc_local_send_string,         \
    .connect_with_service = NULL,                     \
    .request_new_endpoint = NULL,                     \
    .benchmarking = NULL                              \
}

void aos_rpc_comm_context_server_init(struct aos_rpc_comm_context *context);

void send_reply(struct aos_rpc_server_comm_state *state, errval_t err);

void common_event_handler(struct aos_rpc_server_comm_state *state);

void generic_event_handler(struct aos_rpc_server_comm_state *state,
                           aos_rpc_fg_event_handler_t *fg_access,
                           waitlist_event_handler_t event_handler);

errval_t aos_rpc_local_send_number(struct aos_rpc *rpc, uintptr_t val);

errval_t aos_rpc_local_send_string(struct aos_rpc *rpc, const char *string);

#endif // LIB_AOS_RPC_SERVER_AOS_RPC_SERVER_COMMON_P_
