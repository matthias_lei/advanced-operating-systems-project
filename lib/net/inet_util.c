/**
 * \file
 * \brief inet helper function implementation
 *
 * v1.0 2017-12-04 Group C
 */

#include <net/inet_util.h>

#include <aos/aos.h>

// NOTE mlei: This checksum function was written in need for a 
// checksum function which does does not only take one buffer as the
// input but several buffer in order to avoid memcpys when pseudoheaders
// (e.g. UDP, TCP) are involved in the checksum calculation
/**
 *  \brief      computes checksum as defined in
 *              https://tools.ietf.org/html/rfc1071
 *  \params len length in bytes
 */

u_short inet_rfc_checksum_buf_arr(struct inet_buffer_data **buf_arr, size_t len) {
    assert(buf_arr);

    size_t sum = 0;

    for (size_t i = 0; i < len; i++) {

        u_short *buf = (u_short *)(*buf_arr[i]).buf;
        size_t buf_len = (*buf_arr[i]).len;

        while (buf_len > 1) {
            sum += *(buf++);
            if (sum & 0x80000000) {
                sum = (sum & 0xffff) + (sum >> 16);
            }
            buf_len -= 2;
        }

        if (buf_len) {
            sum += (u_short) *(u_char *)buf;
        }
    }

    while (sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }
    return (u_short) ~sum;
}
