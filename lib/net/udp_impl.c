/**
 * \file
 * \brief UDP Impl implementation for packet processing
 *
 * v1.0 2017-12-04 Group C
 */

#include <net/udp_impl.h>

#include <aos/aos.h>
#include <net/ip_prot.h>
#include <net/inet_util.h>
#include <net/inet_descriptorring.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <errno.h>
#include <arpa/inet.h>
#include <errno.h>

#define UDP_MAX_PACKET_SIZE 65507

// if no binding provided or provided addr is INADDR_ANY
#define UDP_DEFAULT_BIND_IP_ADDR "10.0.2.1"

struct udp_impl_state us;

static bool udp_impl_is_checksum_correct(struct ip *ip_packet, struct udp *udp_packet);
static void udp_impl_dump_udp_header(struct udp *udp_packet);
static void udp_correct_byte_ordering_header_network_host(struct udp *udp_packet);
static void udp_correct_byte_ordering_header_host_network(struct udphdr *udp_header);
static void udp_impl_deliver_packet(struct ip *ip_packet, struct udp *udp_packet);


void udp_impl_init(void) {
    us.socket_head = NULL;
    us.next_free_dynamic_port = IPPORT_EPHEMERALFIRST;
}


void ip_prot_udp_packet_handler(struct ip *ip_packet) {
    assert(ip_packet);

    struct udp *udp_packet = (struct udp *) ((uint32_t *)ip_packet + ip_packet->ip_hl);

    if (!udp_impl_is_checksum_correct(ip_packet, udp_packet)) {
        free(ip_packet);
        return;
    }

    udp_correct_byte_ordering_header_network_host(udp_packet);

    if (udp_packet->header.uh_ulen > UDP_MAX_PACKET_SIZE) {
        free(ip_packet);
        return;
    }

#ifdef _INET_DEBUG
    udp_impl_dump_udp_header(udp_packet);
#endif

    udp_impl_deliver_packet(ip_packet, udp_packet);
}


errval_t udp_impl_socket_state_init(struct udp_socket_state *ss, size_t send_buf_size, size_t recv_buf_size) {
    assert(ss);

    size_t recv_buf_len = sizeof(struct inet_descriptor_field) * UDP_DGRAM_DEFAULT_RECV_BUF_SIZE;
    void *recv_buf = malloc(recv_buf_len);
    if (!recv_buf) {
        errno = ENOMEM;
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc for udp recv buffer failed\n");
        return LIB_ERR_MALLOC_FAIL;
    }
    inet_descriptorring_init(&ss->recv_buf, recv_buf, recv_buf_len);

    size_t send_buf_len = sizeof(struct inet_descriptor_field) * UDP_DGRAM_DEFAULT_RECV_BUF_SIZE;
    void *send_buf = malloc(send_buf_len);
    if (!send_buf) {
        errno = ENOMEM;
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc for udp send buffer failed\n");
        free(recv_buf);
        return LIB_ERR_MALLOC_FAIL;
    }
    inet_descriptorring_init(&ss->send_buf, send_buf, send_buf_len);

    waitset_chanstate_init(&ss->chanstate, CHANTYPE_OTHER);

    ss->addr.sin_len = sizeof(ss->addr);
    ss->addr.sin_family = AF_INET;
    ss->addr.sin_port = 0;
    ss->addr.sin_addr.s_addr = INADDR_ANY;

    ss->recv_closure.handler = NULL;
    ss->recv_closure.arg = NULL;

    return SYS_ERR_OK;
}


bool udp_impl_bind_socket_state(struct udp_socket_state *ss, struct sockaddr_in *addr) {
    assert(ss);
    assert(addr);

    if (addr->sin_family != AF_INET) {
        errno = EAFNOSUPPORT;
        return false;
    }

    if (addr->sin_port) {
        if (us.udp_ports[addr->sin_port]) {
            errno = EADDRINUSE;
            return false;
        }
    }

    // if INADDR_ANY, use default address
    if (addr->sin_addr.s_addr == INADDR_ANY) {
        ss->addr.sin_addr.s_addr = inet_addr(UDP_DEFAULT_BIND_IP_ADDR);
    }
    else {
        ss->addr.sin_addr = addr->sin_addr;
    }

    // if dynamic port, find next free port
    if (!addr->sin_port) {
        while (us.udp_ports[us.next_free_dynamic_port]) {
            us.next_free_dynamic_port++;
        }
        ss->addr.sin_port = us.next_free_dynamic_port++;
    }
    else {
        ss->addr.sin_port = addr->sin_port;
    }

    ss->addr.sin_len = sizeof(struct sockaddr_in);
    ss->addr.sin_family = AF_INET;
    us.udp_ports[ss->addr.sin_port] = ss;    

    return true;
}


bool udp_impl_read_from_buffer(struct udp_socket_state *ss, struct ip **buf, size_t *len) {
    assert(ss);
    assert(buf);
    assert(len);

    if (!ss->addr.sin_port) {
        // TODO mlei: apparently the standard behaviour of recv without bind 
        // is an automatic bind but so far no official sources could be found 
        errno = ENOTCONN;
        return false;
    }

    struct inet_descriptorring *recv_buf = &ss->recv_buf;

    return inet_descriptorring_recv(recv_buf, (void **)buf, len, NULL, NULL);
}


bool udp_impl_send_to_buffer(struct udp_socket_state *ss, struct udp *udp_packet, size_t udp_packet_len, struct sockaddr_in *dest_addr) {
    assert(ss);
    assert(udp_packet);

    if (!inet_descriptorring_send(&ss->send_buf, udp_packet, udp_packet_len, dest_addr, sizeof(struct sockaddr_in))) {
        errno = ENOBUFS;
        return false;
    }

    errval_t err = waitset_chan_trigger_closure(get_default_waitset(), &ss->chanstate, MKCLOSURE(udp_impl_dispatch_packet, ss));
    if (err_is_fail(err)) {
        errno = ENOBUFS;
        return false;
    }
    return true;
}


// function for closure of waitset_chan_trigger_closure
void udp_impl_dispatch_packet(void *arg) {
    assert(arg);

    struct udp_socket_state *ss = (struct udp_socket_state *)arg;

    struct inet_descriptorring *send_buf = &ss->send_buf;

    void *udp_packet;
    size_t udp_packet_len;
    struct sockaddr_in *dest_addr;
    size_t dest_addr_len; /* not used */

    while (inet_descriptorring_recv(send_buf, &udp_packet, &udp_packet_len, (void **)&dest_addr, &dest_addr_len)) {
        ip_prot_send_udp_packet(udp_packet, udp_packet_len, ss->addr.sin_addr, dest_addr->sin_addr);
        free(dest_addr);
        free(udp_packet);
    }
}


static void udp_impl_deliver_packet(struct ip *ip_packet, struct udp *udp_packet) {
    assert(udp_packet == (struct udp *) ((uint32_t *)ip_packet + ip_packet->ip_hl));

    if (!us.udp_ports[udp_packet->header.uh_dport]) {
        //port not active, drop packet
        free(ip_packet);
        return;
    }

    struct udp_socket_state *ss = us.udp_ports[udp_packet->header.uh_dport];
    struct inet_descriptorring *dr = &ss->recv_buf;
    
    if (!inet_descriptorring_send(dr, ip_packet, ip_packet->ip_len, NULL, 0)) {
        // recv buffer full, drop packet
        free(ip_packet);
        return;
    }

    // someone is waiting for a packet to arrive
    if (ss->recv_closure.handler != NULL) {
        waitset_chan_trigger_closure(get_default_waitset(), &ss->chanstate, ss->recv_closure);
        ss->recv_closure.handler = NULL;
        ss->recv_closure.arg = NULL;
    }
}


bool udp_impl_generate_udp_header(struct udp_socket_state *ss, struct sockaddr_in *addr, struct udp *udp_packet, size_t len) {
    assert(ss);
    assert(addr);
    assert(udp_packet);
    assert(ss->addr.sin_port);
    assert(len >= sizeof(struct udphdr));

    if (!addr->sin_port) {
        errno = EFAULT;
        return false;
    }

    udp_packet->header.uh_sport = ss->addr.sin_port;
    udp_packet->header.uh_dport = addr->sin_port;
    udp_packet->header.uh_ulen = len;
    udp_packet->header.uh_sum = 0x0;

    struct ippseudo pseudo_header = {
        .ippseudo_src = ss->addr.sin_addr,
        .ippseudo_dst = addr->sin_addr,
        .ippseudo_pad = 0x0,
        .ippseudo_p = IPPROTO_UDP,
        .ippseudo_len = htons(udp_packet->header.uh_ulen)
    };

    struct inet_buffer_data pseudo_header_buf_data = {
        .buf = &pseudo_header,
        .len = sizeof(pseudo_header)
    };

    struct inet_buffer_data udp_packet_buf_data = {
        .buf = udp_packet,
        .len = udp_packet->header.uh_ulen
    };

    struct inet_buffer_data *buf_arr[2] = {&pseudo_header_buf_data, &udp_packet_buf_data};

    udp_correct_byte_ordering_header_host_network(&udp_packet->header);

    udp_packet->header.uh_sum = inet_rfc_checksum_buf_arr((struct inet_buffer_data **)&buf_arr, 2);
    return true;
}


static bool udp_impl_is_checksum_correct(struct ip *ip_packet, struct udp *udp_packet) {

    if (!udp_packet->header.uh_sum) {
        return true;
    }
    struct ippseudo pseudo_header = {
        .ippseudo_src = ip_packet->ip_src,
        .ippseudo_dst = ip_packet->ip_dst,
        .ippseudo_pad = 0x0,
        .ippseudo_p = IPPROTO_UDP,
        .ippseudo_len = udp_packet->header.uh_ulen
    };
    
    struct inet_buffer_data pseudo_header_buf_data = {
        .buf = &pseudo_header,
        .len = sizeof(pseudo_header)
    };
    struct inet_buffer_data udp_packet_buf_data = {
        .buf = udp_packet,
        .len = ntohs(udp_packet->header.uh_ulen)
    };
    struct inet_buffer_data *buf_arr[2] = {&pseudo_header_buf_data, &udp_packet_buf_data};

    return inet_rfc_checksum_buf_arr((struct inet_buffer_data **)&buf_arr, 2) == 0;
}


static void udp_correct_byte_ordering_header_network_host(struct udp *udp_packet) {
    udp_packet->header.uh_sport = ntohs(udp_packet->header.uh_sport);
    udp_packet->header.uh_dport = ntohs(udp_packet->header.uh_dport);
    udp_packet->header.uh_ulen = ntohs(udp_packet->header.uh_ulen);
    udp_packet->header.uh_sum = ntohs(udp_packet->header.uh_sum);
}


static void udp_correct_byte_ordering_header_host_network(struct udphdr *udp_header) {
    udp_header->uh_sport = htons(udp_header->uh_sport);
    udp_header->uh_dport = htons(udp_header->uh_dport);
    udp_header->uh_ulen = htons(udp_header->uh_ulen);
    udp_header->uh_sum = htons(udp_header->uh_sum);
}

__attribute__((unused))
static void udp_impl_dump_udp_header(struct udp *udp_packet) {
    debug_printf("----UDP-Packetheader----\n");
    debug_printf("SPort: 0x%02x (%hu)\n", udp_packet->header.uh_sport, udp_packet->header.uh_sport);
    debug_printf("DPort: 0x%02x (%hu)\n", udp_packet->header.uh_dport, udp_packet->header.uh_dport);
    debug_printf("Len: 0x%02x (%hu)\n", udp_packet->header.uh_ulen, udp_packet->header.uh_ulen);
    debug_printf("Sum: 0x%02x\n", udp_packet->header.uh_sum, udp_packet->header.uh_sum);
    debug_printf("----UDP-Packetheader----\n");
}
