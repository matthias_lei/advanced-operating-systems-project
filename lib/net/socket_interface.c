/**
 * \file
 * \brief Implementation of the UNIX Socket API
 *        as described in:
 *        http://pubs.opengroup.org/onlinepubs/7908799/xns/syssocket.h.html
 *
 * v1.0 2017-12-09 Group C
 */

#include <sys/socket.h>
#include <net/socket_interface_remote.h>


int accept(int socket, struct sockaddr *address, socklen_t *address_len) {
    return socket_interface_remote_accept(socket, address, address_len);
}


int bind(int socket, const struct sockaddr *address, socklen_t address_len) {
    return socket_interface_remote_bind(socket, address, address_len);
}


int connect(int socket, const struct sockaddr *address, socklen_t address_len) {
    return socket_interface_remote_connect(socket, address, address_len);
}


int getpeername(int socket, struct sockaddr *address, socklen_t *address_len) {
    return socket_interface_remote_getpeername(socket, address, address_len);
}


int getsockname(int socket, struct sockaddr *address, socklen_t *address_len) {
    return socket_interface_remote_getsockname(socket, address, address_len);
}


int getsockopt(int socket, int level, int option_name, void *option_value, socklen_t *option_len) {
    return socket_interface_remote_getsockopt(socket, level, option_name, option_value, option_len);
}


int listen(int socket, int backlog) {
    return socket_interface_remote_listen(socket, backlog);
}


ssize_t recv(int socket, void *buffer, size_t length, int flags) {
    return socket_interface_remote_recv(socket, buffer, length, flags);
}


ssize_t recvfrom(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {
    return socket_interface_remote_recvfrom(socket, buffer, length, flags, address, address_len);
}


ssize_t recvmsg(int socket, struct msghdr *message, int flags) {
    return socket_interface_remote_recvmsg(socket, message, flags);
}


ssize_t send(int socket, const void *message, size_t length, int flags) {
    return socket_interface_remote_send(socket, message, length, flags);
}


ssize_t sendmsg(int socket, const struct msghdr *message, int flags) {
    return socket_interface_remote_sendmsg(socket, message, flags);
}


ssize_t sendto(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
    return socket_interface_remote_sendto(socket, message, length, flags, dest_addr, dest_len);
}


int setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len) {
    return socket_interface_remote_setsockopt(socket, level, option_name, option_value, option_len);
}


int shutdown(int socket, int how) {
    return socket_interface_remote_shutdown(socket, how);
}


int socket(int domain, int type, int protocol) {
    return socket_interface_remote_socket(domain, type, protocol);
}


int socketpair(int domain, int type, int protocol, int socket_vector[2]) {
    return socket_interface_remote_socketpair(domain, type, protocol, socket_vector);
}

