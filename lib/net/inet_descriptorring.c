/**
 * \file
 * \brief single threaded descriptor ring implementation
 *
 * v1.0 2017-12-04 Group C
 */

#include <net/inet_descriptorring.h>

#include <aos/aos.h>

#define INET_DR_RECV_BIT 0
#define INET_DR_RECV_MASK (1 << INET_DR_RECV_BIT)
#define INET_DR_RECV_FLAG(flag) ((flag & INET_DR_RECV_MASK) >> INET_DR_RECV_BIT)

#define INET_DR_SET_RECV_FLAG(flag) ((flag) |= INET_DR_RECV_MASK)
#define INET_DR_CLEAR_RECV_FLAG(flag) ((flag) &= ~INET_DR_RECV_MASK)


/**
 *	\brief		Initializes the descriptorring with the given buffer
 *	\params	dr  address of the descriptorring struct
 *	\params buf address of the buffer for descriptor field array
 *	\params len length of the buffer in bytes
 */
void inet_descriptorring_init(struct inet_descriptorring *dr, void *buf, size_t len) {
	assert(dr);
	assert(buf);

	memset(buf, 0, len);

	dr->field = (struct inet_descriptor_field *)buf;

	dr->size = len / sizeof(struct inet_descriptor_field);
	dr->send_idx = 0;
	dr->recv_idx = 0;

	if (dr->is_sender) {
		waitset_chanstate_init(&dr->waitset_state, CHANTYPE_OTHER);
	}
}


bool inet_descriptorring_can_send(struct inet_descriptorring *dr) {
	return ~INET_DR_RECV_FLAG(dr->field[dr->send_idx].flags);
}


bool inet_descriptorring_can_recv(struct inet_descriptorring *dr) {
	return INET_DR_RECV_FLAG(dr->field[dr->recv_idx].flags);
}


bool inet_descriptorring_send(struct inet_descriptorring *dr, void *data_buf, size_t data_len, void *meta_buf, size_t meta_len) {
	assert(dr);
	assert(data_buf);
	
	if (!inet_descriptorring_can_send(dr)) {
		return false;
	}
	struct inet_descriptor_field *field = &(dr->field[dr->send_idx]);

	field->data_buf = data_buf;
	field->data_len = data_len;
	field->meta_buf = meta_buf;
	field->meta_len = meta_len;

	dr->send_idx = (dr->send_idx + 1) % dr->size;

	INET_DR_SET_RECV_FLAG(field->flags);
	return true;
}


bool inet_descriptorring_recv(struct inet_descriptorring *dr, void **data_buf, size_t *data_len, void **meta_buf, size_t *meta_len) {
	assert(dr);
	assert(data_buf);

	if (!inet_descriptorring_can_recv(dr)) {
		return false;
	}

	struct inet_descriptor_field *field = &(dr->field[dr->recv_idx]);

	*data_buf = field->data_buf;
	*data_len = field->data_len;
	
	if (meta_buf) {
		*meta_buf = field->meta_buf;
		*meta_len = field->meta_len;
	}

	dr->recv_idx = (dr->recv_idx + 1) % dr->size;

	INET_DR_CLEAR_RECV_FLAG(field->flags);
	return true;
}
