/**
 * \file
 * \brief SLIP implementation
 *
 * v1.0 2017-11-29 Group C
 */

#include <net/slip.h>

#include <aos/aos.h>
#include <aos/inthandler.h>
#include <netutil/user_serial.h>
#include <maps/omap44xx_map.h>
#include <driverkit/driverkit.h>

#define SLIP_END 0xc0
#define SLIP_ESC 0xdb
#define SLIP_NULL 0x00

#define SLIP_ESC_END 0xdc
#define SLIP_ESC_ESC 0xdd
#define SLIP_ESC_NUL 0xde

// small state machine only for incoming bytes
#define S_REGULAR 1
#define S_ESCAPE 2

struct slip_state {
    uint16_t current_state;
    lvaddr_t uart_vaddr;
    slip_input_handler_t input_handler;
    slip_packet_end_handler_t packet_end_handler;
};

static struct slip_state ss;

static void _slip_input(uint8_t *buf);
static void _serial_write(uint8_t c, size_t len);

static void slip_write_escaped(uint8_t c);

errval_t slip_init(void) {

    errval_t err = map_device_register(OMAP44XX_MAP_L4_PER_UART4, OMAP44XX_MAP_L4_PER_UART4_SIZE, &ss.uart_vaddr);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "map_device_register failed\n");
        return err;
    }

    err = serial_init(ss.uart_vaddr, UART4_IRQ);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "serial_init failed\n");
        return err;
    }

    ss.current_state = S_REGULAR;

    ss.input_handler = NULL;
    ss.packet_end_handler = NULL;

    return SYS_ERR_OK;
}

void slip_register_input_handler(slip_input_handler_t input_handler) {
    assert(input_handler);
    ss.input_handler = input_handler;
}

void slip_register_packet_end_handler(slip_packet_end_handler_t packet_end_handler) {
    assert(packet_end_handler);
    ss.packet_end_handler = packet_end_handler;
}

/**
 * Blocking write.
 */
void slip_write(void *buf, size_t len) {
    assert(buf);

    uint8_t *byte_buf = (uint8_t *)buf;

    for (size_t i = 0; i < len; i++) {

        slip_write_escaped(byte_buf[i]);
    }

    _serial_write(SLIP_END, 1);
}

/**
 *  \brief Writes from multiple buffers in the order
 *         laid out in the array (lowest index first) 
 *         before SLIP_END is sent.
 *         This comes handy when headers of different layers are generated
 *         in different places. Ultimately it will save us
 *         a lot of memcpys.
 *         Blocking write.
 */
void slip_write_buf_arr(struct slip_write_buffer_data **buf_arr, size_t len) {
    assert(buf_arr);

    for (size_t i = 0; i < len; i++) {

        struct slip_write_buffer_data* current = buf_arr[i];
        uint8_t *byte_buf = (uint8_t *)current->buf;

        for (size_t j = 0; j < current->len; j++) {

            slip_write_escaped(byte_buf[j]);
        }
    }

    _serial_write(SLIP_END, 1);
}

static void slip_write_escaped(uint8_t c){

    switch(c) {
        case SLIP_END:
            _serial_write(SLIP_ESC, 1);
            _serial_write(SLIP_ESC_END, 1);
            break;

        case SLIP_ESC:
            _serial_write(SLIP_ESC, 1);
            _serial_write(SLIP_ESC_ESC, 1);
            break;

        case SLIP_NULL:
            _serial_write(SLIP_ESC, 1);
            _serial_write(SLIP_ESC_NUL, 1);
            break;

        default:
            _serial_write(c, 1);
            break;
    }
}

static void _slip_input(uint8_t *buf) {
    
    if (ss.current_state == S_REGULAR) {

        switch(*buf) {
            case SLIP_ESC:
                ss.current_state = S_ESCAPE;
                break;
            case SLIP_END:
                ss.packet_end_handler();
                ss.current_state = S_REGULAR;
                break;
            default:
                ss.input_handler(*buf);
                ss.current_state = S_REGULAR;
                break;
        }
    }
    else if (ss.current_state == S_ESCAPE) {

        switch(*buf) {

            case SLIP_ESC_ESC:
                ss.input_handler(SLIP_ESC);
                ss.current_state = S_REGULAR;
                break;

            case SLIP_ESC_END:
                ss.input_handler(SLIP_END);
                ss.current_state = S_REGULAR;
                break;

            case SLIP_ESC_NUL:
                ss.input_handler(SLIP_NULL);
                ss.current_state = S_REGULAR;
                break;

            default:
                //debug_printf("SLIP: unexpected control character after escape: %1x\n", *buf);
                ss.current_state = S_REGULAR;
                break;
        }
    }
    else {

        debug_printf("SLIP: SLIP in undefined state\n");
    }
    return;
}

// this wrapper only exists because the serial_write does not
// accept macros as input
// NOTE: changing the macros to constants does not make it better
//       since case statements (switch-case) do not accept
//       static const integers
static void _serial_write(uint8_t c, size_t len) {
    serial_write(&c, len);
}



void serial_input(uint8_t *buf, size_t len) {
    assert(buf);
    assert(len);

    if (len != 1) {
        debug_printf("serial_input received more than one character at once\n");
    }
    _slip_input(buf);

    // TODO mlei: for testing purposes we echo the inputs directly back
    //            in order to make the response visible in wireshark
    //serial_write(buf, len);
}
