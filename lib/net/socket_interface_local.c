/**
 * \file
 * \brief local socket interface implementation
 *        The current implementation provides a socket basic interface
 *        (socket, bind, sendto, recvfrom, getsockname)
 *        This interface makes it convenient to add more protocols to
 *        network driver as the vtables can be extended
 *        The implementation followed the specifications as defined in:
 *        http://pubs.opengroup.org/onlinepubs/7908799/xns/syssocket.h.html
 *
 * v1.0 2017-12-08 Group C
 */

#include <net/socket_interface_local.h>

#include <aos/aos.h>
#include <net/ip_prot.h>
#include <net/udp_impl.h>
#include <net/inet_util.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <errno.h>
#include <sys/socket.h>

#define SOCKET_INTERFACE_LOCAL_MAX_ID IPPORT_MAX

extern struct inet_transport_vtable udp_prot_local_vtable;

struct socket_interface_local_socket_state {
    uint32_t prot;
    void *state;
};

struct socket_interface_local_state{
    uint32_t next_free_id;

    // maps ids to socket states
    struct socket_interface_local_socket_state *socket_states[SOCKET_INTERFACE_LOCAL_MAX_ID];
    struct inet_transport_vtable *vtables[IPPROTO_MAX];
};

static struct socket_interface_local_state sils;


errval_t socket_interface_local_init(void) {

    sils.next_free_id = SOCKET_ID_FIRST;
    sils.vtables[IPPROTO_UDP] = &udp_prot_local_vtable;

    errval_t err = ip_prot_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ip_impl_init failed\n");
        return err;
    }

    return SYS_ERR_OK;
}


int socket_interface_local_accept(int socket, struct sockaddr *address, socklen_t *address_len) {
    // not implemented
    return -1;
}


int socket_interface_local_bind(int socket, const struct sockaddr *address, socklen_t address_len) {

    struct socket_interface_local_socket_state *socket_state = sils.socket_states[socket];
    if (!socket_state) {
        errno = ENOTSOCK;
        return -1;
    }
    return sils.vtables[socket_state->prot]->bind(socket_state->state, address, address_len);
}


int socket_interface_local_connect(int socket, const struct sockaddr *address, socklen_t address_len) {
    // not implemented
    return -1;
}


int socket_interface_local_getpeername(int socket, struct sockaddr *address, socklen_t *address_len) {
    // not implemented
    return -1;
}


int socket_interface_local_getsockname(int socket, struct sockaddr *address, socklen_t *address_len) {

    struct socket_interface_local_socket_state *socket_state = sils.socket_states[socket];
    if (!socket_state) {
        errno = ENOTSOCK;
        return -1;
    }

    return sils.vtables[socket_state->prot]->getsockname(socket_state->state, address, address_len);
}


int socket_interface_local_getsockopt(int socket, int level, int option_name, void *option_value, socklen_t *option_len) {
    // not implemented
    return -1;
}


int socket_interface_local_listen(int socket, int backlog) {
    // not implemented
    return -1;
}


ssize_t socket_interface_local_recv(int socket, void *buffer, size_t length, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_local_recvfrom(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {

    struct socket_interface_local_socket_state *socket_state = sils.socket_states[socket];
    if (!socket_state) {
        errno = ENOTSOCK;
        return -1;
    }

    return sils.vtables[socket_state->prot]->recvfrom(socket_state->state, buffer, length, flags, address, address_len);
}


ssize_t socket_interface_local_recvmsg(int socket, struct msghdr *message, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_local_send(int socket, const void *message, size_t length, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_local_sendmsg(int socket, const struct msghdr *message, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_local_sendto(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
    
    struct socket_interface_local_socket_state *socket_state = sils.socket_states[socket];
    if (!socket_state) {
        errno = ENOTSOCK;
        return -1;
    }

    return sils.vtables[socket_state->prot]->sendto(socket_state->state, message, length, flags, dest_addr, dest_len);
}


int socket_interface_local_setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len) {
    // not implemented
    return -1;
}


int socket_interface_local_shutdown(int socket, int how) {
    // not implemented
    return -1;
}


int socket_interface_local_socket(int domain, int type, int protocol) {

    if (protocol >= IPPROTO_MAX) {
        errno = EPROTONOSUPPORT;
        return -1;
    }

    struct socket_interface_local_socket_state *socket_state = (struct socket_interface_local_socket_state *)malloc(sizeof(struct socket_interface_local_socket_state));
    if (!socket_state) {
        errno = ENOMEM;
        return -1;
    }

    void *state = sils.vtables[protocol]->socket(domain, type, protocol);
    if (!state) {
        free(socket_state);
        return -1;
    }

    socket_state->prot = protocol;
    socket_state->state = state;

    uint32_t id = sils.next_free_id++;
    sils.socket_states[id] = socket_state;

    return id;
}


int socket_interface_local_socketpair(int domain, int type, int protocol, int socket_vector[2]) {
    // not implemented
    return -1;
}


// --- helper functions ---
int socket_interface_local_can_recv(int socket, bool *can_recv) {
    struct socket_interface_local_socket_state *socket_state = sils.socket_states[socket];
    if (!socket_state) {
        errno = ENOTSOCK;
        return -1;
    }
    *can_recv = sils.vtables[socket_state->prot]->can_recv(socket_state->state);
    return 0;
}

int socket_interface_local_set_recv_handler(int socket, struct event_closure closure) {
    struct socket_interface_local_socket_state *socket_state = sils.socket_states[socket];
    if (!socket_state) {
        errno = ENOTSOCK;
        return -1;
    }

    sils.vtables[socket_state->prot]->set_recv_handler(socket_state->state, closure);
    return 0;
}