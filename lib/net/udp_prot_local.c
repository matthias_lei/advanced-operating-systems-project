/**
 * \file
 * \brief UDP local protocol implementation
 *
 * v1.0 2017-12-06 Group C
 */


#include <net/udp_prot_local.h>

#include <aos/aos.h>
#include <net/inet_descriptorring.h>
#include <net/ip_prot.h>
#include <net/socket_interface_local.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <errno.h>
#include <sys/socket.h>

extern struct udp_impl_state us;

struct inet_transport_vtable udp_prot_local_vtable = {
    .accept = NULL,
    .bind = udp_prot_local_bind,
    .connect = NULL,
    .getpeername = NULL,
    .getsockname = udp_prot_local_getsockname,
    .getsockopt = NULL,
    .listen = NULL,
    .recv = NULL,
    .recvfrom = udp_prot_local_recvfrom,
    .recvmsg = NULL,
    .send = NULL,
    .sendmsg = NULL,
    .sendto = udp_prot_local_sendto,
    .setsockopt = NULL,
    .shutdown = NULL,
    .socket = udp_prot_local_socket,
    .socketpair = NULL,
    .can_recv = udp_prot_local_can_recv,
    .set_recv_handler = udp_prot_local_set_recv_handler
};

int udp_prot_local_bind(void *state, const struct sockaddr *address, socklen_t address_len) {
    assert(state);
    assert(address);

    struct udp_socket_state *ss = (struct udp_socket_state *)state;

    struct sockaddr_in *in_addr = (struct sockaddr_in *)address;

    if (!udp_impl_bind_socket_state(ss, in_addr)) {
        return -1;
    }

    return 0;
}


int udp_prot_local_getsockname(void *state, struct sockaddr *address, socklen_t *address_len) {
    assert(state);
    assert(address);
    assert(address_len);

    struct udp_socket_state *ss = (struct udp_socket_state *)state;

    *address = *((struct sockaddr *)&ss->addr);
    *address_len = ss->addr.sin_len;

    return 0;
}


ssize_t udp_prot_local_sendto(void *state, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
    assert(state);

    struct udp_socket_state *ss = (struct udp_socket_state *)state;

    // if not bound then bind
    if (!ss->addr.sin_port) {
        struct sockaddr_in addr = {
            .sin_len = sizeof(addr),
            .sin_family = AF_INET,
            .sin_port = 0,
            .sin_addr.s_addr = INADDR_ANY
        };

        if (!udp_prot_local_bind(ss, (struct sockaddr *)&addr, sizeof(struct sockaddr_in))) {
            return -1;
        }
        
    }

    // copy payload
    size_t udp_packet_len = length + sizeof(struct udp);
    struct udp *udp_packet = (struct udp *)malloc(udp_packet_len);
    if (!udp_packet) {
        errno = ENOMEM;
        return -1;
    }
    memcpy(&udp_packet->payload, message, length);

    // copy dest_addr
    struct sockaddr_in *udp_dest_addr = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
    if (!udp_dest_addr) {
        errno = ENOMEM;
        free(udp_packet);
        return -1;
    }
    memcpy(udp_dest_addr, dest_addr, sizeof(struct sockaddr_in));

    if (!udp_impl_generate_udp_header(ss, udp_dest_addr, udp_packet, udp_packet_len)) {
        free(udp_packet);
        free(udp_dest_addr);
        return -1;
    }

    if (!udp_impl_send_to_buffer(ss, udp_packet, udp_packet_len, udp_dest_addr)) {
        free(udp_packet);
        free(udp_dest_addr);
        return -1;
    }

    return length;
}


ssize_t udp_prot_local_recvfrom(void *state, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {
    assert(state);
    assert(buffer);

    struct udp_socket_state *ss = (struct udp_socket_state *)state;

    struct ip *ip_packet;
    size_t ip_packet_len;

    while (!udp_impl_read_from_buffer(ss, &ip_packet, &ip_packet_len)) {
        // check for local events
        errval_t err = check_for_event(get_default_waitset());
        if (err_is_ok(err)) {
            err = event_dispatch(get_default_waitset());
            if (err_is_fail(err)) {
                // fatal error
                DEBUG_ERR(err, "event_dispatch failed.\n");
                abort();
            }
        }
        else if (err_no(err) != LIB_ERR_NO_EVENT) {
            // fatal error
            DEBUG_ERR(err, "check_for_event failed.\n");
            abort();
        }
    }

    struct udp *udp_packet = (struct udp *) ((uint32_t *)ip_packet + ip_packet->ip_hl);

    ssize_t length_res = MIN(length, udp_packet->header.uh_ulen - sizeof(struct udphdr));
    memcpy(buffer, &udp_packet->payload, length_res);

    if (address) {
        struct sockaddr_in *src_addr = (struct sockaddr_in *)address;
        src_addr->sin_len = sizeof(struct sockaddr_in);
        src_addr->sin_family = AF_INET;
        src_addr->sin_port = udp_packet->header.uh_sport;
        src_addr->sin_addr = ip_packet->ip_src;
    }

    free(ip_packet);

    return length_res;
}


void *udp_prot_local_socket(int domain, int type, int protocol) {
    assert(domain == AF_INET);
    assert(type == SOCK_DGRAM);
    assert(protocol == IPPROTO_UDP); 

    struct udp_socket_state *uss = (struct udp_socket_state *) malloc(sizeof(struct udp_socket_state));
    if (!uss) {
        errno = ENOMEM;
        return NULL;
    }

    errval_t err = udp_impl_socket_state_init(uss, UDP_DGRAM_DEFAULT_SEND_BUF_SIZE, UDP_DGRAM_DEFAULT_RECV_BUF_SIZE);
    if (err_is_fail(err)) {
        free(uss);
        return NULL;
    }
    return uss;
}


// NOTE mlei: these are helper functions in order to solve the
// same problem encountered for serial_rpc
// the remote recvfrom function must not set the network server
// into a polling state otherwise it will block itself as soon 
// as it starts dispatching other request.

bool udp_prot_local_can_recv(void *state) {
    assert(state);
    struct udp_socket_state *ss = (struct udp_socket_state *)state;

    return inet_descriptorring_can_recv(&ss->recv_buf); 
}

void udp_prot_local_set_recv_handler(void *state, struct event_closure closure) {
    assert(state);
    struct udp_socket_state *ss = (struct udp_socket_state *)state;
    ss->recv_closure = closure;
}
