/**
 * \file
 * \brief network driver implementation
 *
 * v1.0 2017-12-09 Group C
 */

#include <net/network_driver.h>

#include <aos/aos.h>
#include <net/socket_interface_local.h>

errval_t network_driver_init(void) {

    errval_t err = socket_interface_local_init();
    if(err_is_fail(err)) {
        DEBUG_ERR(err, "socket_interface_local_init failed\n");
        return err;
    }

    debug_printf("Network driver succesfully initialized\n");

    return err;
}