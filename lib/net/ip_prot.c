/**
 * \file
 * \brief IP protocol implementation
 *        This file serves as an multiplexer for the protocol numbers
 *        in the protocol field of the IPv4 header. The protocols are
 *        defined as in:
 *        http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml 
 *
 * v1.0 2017-12-01 Group C
 */

#include <net/ip_prot.h>

#include <aos/aos.h>
#include <net/ip_impl.h>
#include <net/icmp_prot.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include <errno.h>

typedef void (*ip_prot_packet_handler_t)(struct ip *);

static void ip_prot_packet_handler(struct ip *packet);

static ip_prot_packet_handler_t ip_prot_recv_handlers[IPPROTO_MAX] = { 0 }; 

errval_t ip_prot_init(void) {

    ip_prot_recv_handlers[IPPROTO_ICMP] = ip_prot_icmp_packet_handler;
    ip_prot_recv_handlers[IPPROTO_UDP] = ip_prot_udp_packet_handler;

    errval_t err = ip_impl_init();
    if (err_is_fail(err)) {
      DEBUG_ERR(err, "ip_impl_init failed\n");
      return err;
    }

    icmp_prot_init();
    udp_impl_init();

    ip_impl_register_packet_recv_handler(ip_prot_packet_handler);

    return SYS_ERR_OK;
}

static void ip_prot_packet_handler(struct ip *packet) {
    assert(packet);

    struct ip *ip_packet = packet;

    if (!ip_prot_recv_handlers[ip_packet->ip_p]) {
        // drop packet
        free(packet);
        return;
    }
    // deliver to upper layer
    ip_prot_recv_handlers[ip_packet->ip_p](packet);
}

#define IP_PROT_SEND_PACKET(prot_name, prot_number)                     \
bool ip_prot_send_ ## prot_name ## _packet(void *packet_buf,            \
                                           size_t len,                  \
                                           struct in_addr src_addr,     \
                                           struct in_addr dest_addr)    \
{                                                                       \
    assert(packet_buf);                                                 \
    return ip_impl_send_packet_no_options(packet_buf,                   \
                                          len,                          \
                                          prot_number,                  \
                                          src_addr,                     \
                                          dest_addr);                   \
}

IP_PROT_SEND_PACKET(icmp,IPPROTO_ICMP);
IP_PROT_SEND_PACKET(udp,IPPROTO_UDP);
