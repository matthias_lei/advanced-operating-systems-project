/**
 * \file
 * \brief ICMP implementation
 *
 * v1.0 2017-12-01 Group C
 */

#include <net/icmp_prot.h>

#include <aos/aos.h>
#include <net/inet_util.h>
#include <net/ip_prot.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netutil/checksum.h>

typedef void (*icmp_prot_packet_handler_t)(struct ip *, struct icmp *);

static void icmp_prot_echo_handler(struct ip *ip_packet, struct icmp *icmp_packet);
static bool icmp_prot_is_checksum_correct(struct icmp *header, uint16_t len);
static void icmp_correct_byte_ordering_header_network_host(struct icmp *header);
static void icmp_correct_byte_ordering_header_host_network(struct icmp *header);

static void icmp_dump_icmp_header(struct icmp *header);


static icmp_prot_packet_handler_t icmp_prot_packet_handlers[ICMP_TYPE_MAX] = { 0 };

// we do this only because we cannot initialize arrays partially
void icmp_prot_init(void) {
    icmp_prot_packet_handlers[ICMP_ECHO_TYPE_REQ] = icmp_prot_echo_handler;
}


// implentation of the ip_prot layer
void ip_prot_icmp_packet_handler(struct ip *packet) {
    assert(packet);

    struct icmp *icmp_packet = (struct icmp *) ((uint32_t *)packet + packet->ip_hl);

    if(!icmp_prot_is_checksum_correct(icmp_packet, packet->ip_len - (packet->ip_hl << 2))) {
        free(packet);
        return;
    }

    icmp_correct_byte_ordering_header_network_host(icmp_packet);

    icmp_prot_packet_handlers[icmp_packet->type](packet, icmp_packet);
}


/**
 *  \brief      check icmp packet has correct checksum
 *  \params len length of the icmp packet (header + payload) in bytes
 */
static bool icmp_prot_is_checksum_correct(struct icmp *header, u_short len) {

    return ~inet_checksum((u_short *)header, len);
}



static void icmp_correct_byte_ordering_header_network_host(struct icmp *header) {
    header->sum = ntohs(header->sum);
}


static void icmp_correct_byte_ordering_header_host_network(struct icmp *header) {
    header->sum = htons(header->sum);
}


static void icmp_prot_echo_handler(struct ip *ip_packet, struct icmp *icmp_packet) {
    assert(icmp_packet == (struct icmp *)((uint32_t *)ip_packet + ip_packet->ip_hl));

#ifdef _INET_DEBUG
    icmp_dump_icmp_header(icmp_packet);
#endif

    struct icmp_echo *icmp_echo_packet = (struct icmp_echo *)icmp_packet; 

    size_t rep_packet_len = ip_packet->ip_len - (ip_packet->ip_hl << 2);
    struct icmp_echo *rep_packet = (struct icmp_echo *)malloc(rep_packet_len);
    if (!rep_packet) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc for ping reply failed\n");
    }

    // copy payload
    memcpy(&rep_packet->payload, &icmp_echo_packet->payload, ip_packet->ip_len - (ip_packet->ip_hl << 2) - sizeof(struct icmp));

    // generate header
    rep_packet->icmp_header.type = ICMP_ECHO_TYPE_REP;
    rep_packet->icmp_header.code = ICMP_ECHO_CODE_REP;
    rep_packet->icmp_header.sum = 0x0;

    rep_packet->id = icmp_echo_packet->id;
    uint16_t icmp_echo_seq = ntohs(icmp_echo_packet->seq);
    rep_packet->seq = htons(++icmp_echo_seq);

    icmp_correct_byte_ordering_header_host_network((struct icmp *)rep_packet);

    rep_packet->icmp_header.sum = inet_checksum((u_short *)rep_packet, rep_packet_len);

    if (!ip_prot_send_icmp_packet(rep_packet, rep_packet_len, ip_packet->ip_dst, ip_packet->ip_src)){
        //do nothing
    }

    free(rep_packet);
}

__attribute__((unused))
static void icmp_dump_icmp_header(struct icmp *header) {
    debug_printf("----ICMP-Header----\n");
    debug_printf("Type: 0x%1x\n", header->type);
    debug_printf("Code: 0x%1x\n", header->code);
    debug_printf("Sum: 0x%2x\n", ntohs(header->sum));
    debug_printf("----ICMP-Header end----\n");
}
