/**
 * \file
 * \brief IP sublayer implementation for packet processing.
 *        No handling of services.
 *        Very important: since all UARTs share the same 64 byte FIFO buffer
 *        for input and output printfs and debug_printfs have to be used
 *        very sparsely. 
 *        Even with no output the maximum payload size for pings
 *        lies around 110.
 *       
 *
 * v1.0 2017-11-29 Group C
 */
#include <net/ip_impl.h>

#include <aos/aos.h>
#include <net/slip.h>
#include <net/icmp_prot.h>
#include <net/inet_util.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netutil/checksum.h>
#include <errno.h>

// small state machine only for incoming bytes
#define S_IP_UNKNOWN 1
#define S_IP_HEADER 2
#define S_IP_PAYLOAD 3

// sizes in bytes 
#define IP_MAX_HEADER_SIZE 60
#define IP_MAX_OPTION_SIZE (IP_MAX_HEADER_SIZE - sizeof(struct ip))
#define IP_MAX_PAYLOAD_NO_OPTION_SIZE (IP_MAXPACKET - sizeof(struct ip)) 

#define IP_HEADER_NO_OPTION_WORDS 5

#define IP_DEFAULT_TOS 0x0

struct ip_impl_state {

    uint16_t current_state;
    ip_impl_packet_recv_handler_t packet_recv_handler;

    // indicates whether ip_correct_byte_ordering_header_network_host has been called on
    // this packed and we are in the host endianness mode
    bool host_endianness;

    // byte offset into the current section (has to be 64bit in order to detect overflows)
    size_t offset;

    // buffer to be written in after payload length in lookahead buffer has been determined
    char *packet_buf;

    // this field acts as a lookahead buffer in for determining the size 
    // of the entire message and malloc accordingly
    // NOTE mlei: do not change order!
    char header_buf[IP_MAX_HEADER_SIZE];
    char option_buf[IP_MAX_OPTION_SIZE];
};

static struct ip_impl_state is;

// private input helper functions
static void ip_input_handler(uint8_t c);
static void ip_packet_end_handler(void);
static void ip_input_fill_header(uint8_t c);
static void ip_input_fill_payload(uint8_t c);
static bool ip_is_header_correct(void *header_buf, uint16_t len);
static void ip_correct_byte_ordering_header_network_host(struct ip *header);
static inline bool ip_more_fragments(struct ip *header);

// private output helper functions
static void ip_correct_byte_ordering_header_host_network(struct ip *header);

errval_t ip_impl_init(void) {

    errval_t err = slip_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slip_init failed\n");
        return err;
    }

    slip_register_input_handler(ip_input_handler);
    slip_register_packet_end_handler(ip_packet_end_handler);

    is.current_state = S_IP_UNKNOWN;
    is.offset = 0;
    is.host_endianness = false;

    return SYS_ERR_OK;
}


/**
 *  \brief Register the receive handler which is called.
 *         whenever a packet has arrived.
 */
void ip_impl_register_packet_recv_handler(ip_impl_packet_recv_handler_t packet_recv_handler) {
    is.packet_recv_handler = packet_recv_handler;
}

/**
 *  \brief          Sends a provided payload without any options.
 *  \params prot    Protocol number according to
 *                  http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
 */
bool ip_impl_send_packet_no_options(void *buf, size_t len, uint8_t prot, struct in_addr src_addr, struct in_addr dest_addr) {
    assert(buf);

    if (len > IP_MAX_PAYLOAD_NO_OPTION_SIZE) {
        errno = EOVERFLOW;
        return false;
    }

    struct ip header;

    header.ip_v = IPVERSION;
    header.ip_hl = IP_HEADER_NO_OPTION_WORDS;
    header.ip_tos = IP_DEFAULT_TOS;
    header.ip_len = len + sizeof(struct ip);
    header.ip_id = 0;
    header.ip_off = 0x0;
    header.ip_ttl = IPDEFTTL;
    header.ip_p = prot;
    header.ip_sum = 0x0000;
    header.ip_src = src_addr;
    header.ip_dst = dest_addr;

    ip_correct_byte_ordering_header_host_network(&header);

    header.ip_sum = inet_checksum(&header, sizeof(header));


    struct slip_write_buffer_data header_data = {
        .buf = &header,
        .len = sizeof(header)
    };

    struct slip_write_buffer_data payload_data = {
        .buf = buf,
        .len = len
    };

    struct slip_write_buffer_data *packet_data[] = {&header_data, &payload_data};

    slip_write_buf_arr((struct slip_write_buffer_data **)&packet_data, 2);

    return true;
}


static void ip_input_handler(uint8_t c) {

    switch(is.current_state) {
        case S_IP_UNKNOWN:
            // drop byte
            break;
        case S_IP_HEADER:
            ip_input_fill_header(c);
            break;
        case S_IP_PAYLOAD:
            ip_input_fill_payload(c);
            break;
        default:
            debug_printf("IP: ip state not set during ip_input_handler: %2x\n", is.current_state);
    }
}


static void ip_packet_end_handler(void) {

    struct ip *header = (struct ip *)&is.header_buf;
    void *ip_packet;

    switch (is.current_state) {
        case S_IP_UNKNOWN:
            is.current_state = S_IP_HEADER;
            is.offset = 0;
            is.host_endianness = false;
            break;

        case S_IP_HEADER:
            // packet got terminated before even the header was completed
            // or header CRC is false
            if (is.offset >> 2 < IP_HEADER_NO_OPTION_WORDS || !ip_is_header_correct(header, header->ip_hl << 2)) {
                // drop packet
                is.current_state = S_IP_HEADER;
                is.offset = 0;
                is.host_endianness = false;
                break;
            }

            ip_correct_byte_ordering_header_network_host(header);
#ifdef _INET_DEBUG
            dump_ip_header(header);
#endif

            // check if more fragments are incoming if yes drop whole packet
            // NOTE: this is part of the requirement in the script
            if (ip_more_fragments(header)) {
                is.current_state = S_IP_HEADER;
                is.offset = 0;
                is.host_endianness = false;
                break;
            }

            // copy the packet to heap space and deliver
            ip_packet = malloc(header->ip_len);
            if (!ip_packet) {
                // drop packet
                DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed for IP packet buffering. Continuing..\n");
                is.current_state = S_IP_HEADER;
                is.offset = 0;
                is.host_endianness = false;
                break;
            }
            memcpy(ip_packet, header, header->ip_len);
            is.packet_recv_handler((struct ip *)ip_packet);

            is.current_state = S_IP_HEADER;
            is.offset = 0;
            is.host_endianness = false;
            break;

        case S_IP_PAYLOAD:

            // NOTE mlei: this is a conservative implementation of receiving IP
            // packets as it expects the packet_recv_handler exactly to be called
            // when the offset is at the specified total_length - 1, otherwise it
            // will drop the packet
            if (is.offset != (size_t) header->ip_len) {
                is.current_state = S_IP_HEADER;
                is.offset = 0;
                is.host_endianness = false;
                free(is.packet_buf);
                break;
            }  
#ifdef _INET_DEBUG
            dump_ip_header(header);
#endif

            is.packet_recv_handler((struct ip *)is.packet_buf);

            is.current_state = S_IP_HEADER;
            is.offset = 0;
            is.host_endianness = false;
            break;

        default:
            debug_printf("IP: ip state not set during ip_packet_end_handler: %2x\n", is.current_state);
            break;
    }
    return;
}


static inline void ip_input_fill_header(uint8_t c) {
    assert(is.current_state == S_IP_HEADER);
    assert(is.offset < IP_MAX_HEADER_SIZE);

    struct ip *header = (struct ip *)&is.header_buf;

    is.header_buf[is.offset] = c;
    is.offset++;

    if (is.offset >= IP_MAX_HEADER_SIZE) {
        assert(is.offset == IP_MAX_HEADER_SIZE);

        // header CRC not correct
        if (!ip_is_header_correct(header, header->ip_hl << 2)) {
            is.current_state = S_IP_UNKNOWN;
            return;
        }

        ip_correct_byte_ordering_header_network_host(header);

        // check if more fragments are incoming if yes drop whole packet
        // NOTE: in the script fragmenting is not a requirement
        if (ip_more_fragments(header)) {
            is.current_state = S_IP_UNKNOWN;
            return;
        }


        // header CRC correct, malloc for payload
        is.packet_buf = malloc(header->ip_len);
        if (!is.packet_buf) {
            DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed for IP packet buffering. Continuing..\n");
            is.current_state = S_IP_UNKNOWN;
            return;
        }

        memcpy(is.packet_buf, &is.header_buf, is.offset);
        is.current_state = S_IP_PAYLOAD;
    }
}


static inline void ip_input_fill_payload(uint8_t c) {
    assert(is.current_state == S_IP_PAYLOAD);
    assert(is.packet_buf);
    assert(is.offset >= IP_MAX_HEADER_SIZE);

    struct ip *header = (struct ip *)&is.header_buf;

    if (is.offset >= header->ip_len) {
        // this char is not included in the packet size anymore
        // the next event is expected to call slip_register_packet_end_handler
        // reset
        is.current_state = S_IP_UNKNOWN;
        free(is.packet_buf);
        return;
    }

    is.packet_buf[is.offset] = c;
    is.offset++;
}


/**
 *  \brief      check if header is correct
 *  \params len length of the checked data in 32 bit words
 */
static bool ip_is_header_correct(void *header_buf, uint16_t len) {

    struct ip *header = (struct ip *)header_buf;

    return header->ip_sum ? inet_checksum(header, len) == 0 : true;
}


static void ip_correct_byte_ordering_header_network_host(struct ip *header) {
    assert(header);
    assert(!is.host_endianness);

    header->ip_len = ntohs(header->ip_len);
    header->ip_id = ntohs(header->ip_id);
    header->ip_off = ntohs(header->ip_off);
    header->ip_sum = ntohs(header->ip_sum);

    is.host_endianness = true;
}


// NOTE mlei: checking only the MORE_FRAGMENTS FLAG is not 
// sufficient as this flag is not set for the last fragment
static inline bool ip_more_fragments(struct ip *header) {
    assert(is.host_endianness);
    return (header->ip_off & IP_MF) || (header->ip_off & IP_OFFMASK);
}

static void ip_correct_byte_ordering_header_host_network(struct ip *header) {
    assert(header);

    header->ip_len = htons(header->ip_len);
    header->ip_id = htons(header->ip_id);
    header->ip_off = htons(header->ip_off);
    header->ip_sum = htons(header->ip_sum);
}

// pay attention whether ip_correct_byte_ordering_header_network_host has been called before
__attribute__((unused))
void dump_ip_header(struct ip *header) {

    char *src = (char *)&header->ip_src;
    char *dest = (char *)&header->ip_dst;

    debug_printf("\n");
    debug_printf("----IP-Packetheader----\n");
    debug_printf("IHL: 0x%1x\n",header->ip_hl);
    debug_printf("Version: 0x%1x\n",header->ip_v);
    debug_printf("TOS: 0x%1x\n",header->ip_tos);
    debug_printf("TOL: 0x%02x (%hu)\n",header->ip_len, header->ip_len);
    debug_printf("ID: 0x%x\n",header->ip_id);
    debug_printf("OFF: 0x%02x\n",header->ip_off);
    debug_printf("TTL: 0x%1x\n",header->ip_ttl);
    debug_printf("Prot: 0x%1x\n",header->ip_p);
    debug_printf("CRC: 0x%02x\n",header->ip_sum);
    debug_printf("Src: %d.%d.%d.%d\n", src[0], src[1], src[2], src[3]);
    debug_printf("Dest: %d.%d.%d.%d\n", dest[0], dest[1], dest[2], dest[3]);
    
    debug_printf("----IP-Packetheader end----\n");

}
