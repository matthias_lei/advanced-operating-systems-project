/**
 * \file
 * \brief remote socket interface implementation
 *        This layer wraps all the rpc calls to the the server as well as
 *        translating socket id. One reason for the translation is that UNIX file
 *        descriptor ids are only unique within a domain (this also prevents inference
 *        about the network driver state from ids)
 *
 * v1.0 2017-12-09 Group C
 */


#include <net/socket_interface_remote.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <net/inet_util.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <errno.h>
#include <sys/socket.h>

#define SOCKET_INTERFACE_REMOTE_MAX_ID 128

struct socket_interface_remote_socket_state {
    // socket id the server accepts
    int id;
    struct aos_rpc *rpc;
};

struct socket_interface_remote_state {

    uint32_t next_free_id;
    service_handle_t network_handle;
    //maps ids to rpcs
    struct socket_interface_remote_socket_state *socket_states[SOCKET_INTERFACE_REMOTE_MAX_ID];
};

static struct socket_interface_remote_state *sirs;

static int socket_interface_remote_init(void);
static int socket_interface_remote_get_socket_state_from_id(int socket, struct socket_interface_remote_socket_state **ss);


static int socket_interface_remote_init(void) {
    
    sirs = (struct socket_interface_remote_state *)malloc(sizeof(struct socket_interface_remote_state));
    if (!sirs) {
        debug_printf("malloc failed\n");
        return ENOMEM;
    }

    sirs->next_free_id = SOCKET_ID_FIRST;

    errval_t err = aos_rpc_name_lookup_service_by_path(aos_rpc_get_name_channel(), "/core/network", &sirs->network_handle);
    if (err_is_fail(err)) {
        free(sirs);
        DEBUG_ERR(err, "aos_rpc_name_lookup_service_by_path failed\n");
        return ENETUNREACH;
    }

    memset(&sirs->socket_states, 0, sizeof(sirs->socket_states));
    return 0;
}


static int socket_interface_remote_get_socket_state_from_id(int socket, struct socket_interface_remote_socket_state **ss) {
    assert(ss);

    // socket has been not called before yet
    if (!sirs) {
        return ENOTSOCK;
    }

    if ((uint32_t)socket >= SOCKET_INTERFACE_REMOTE_MAX_ID) {
        return ENOTSOCK;
    }

    *ss = sirs->socket_states[socket];
    if (!*ss) {
        return ENOTSOCK;
    }

    return 0;
}

int socket_interface_remote_accept(int socket, struct sockaddr *address, socklen_t *address_len) {
    // not implemented
    return -1;
}


int socket_interface_remote_bind(int socket, const struct sockaddr *address, socklen_t address_len) {
    assert(address);

    struct socket_interface_remote_socket_state *ss;
    int err_no = socket_interface_remote_get_socket_state_from_id(socket, &ss);
    if (err_no != 0) {
        errno = err_no;
        return -1;
    }
    
    int ret_value;
    errval_t err = aos_rpc_network_bind(ss->rpc, &ret_value, &errno, ss->id, address, address_len);
    if (err_is_fail(err) || ret_value == -1) {
        return -1;
    }

    return 0;
}


int socket_interface_remote_connect(int socket, const struct sockaddr *address, socklen_t address_len) {
    // not implemented
    return -1;
}


int socket_interface_remote_getpeername(int socket, struct sockaddr *address, socklen_t *address_len) {
    // not implemented
    return -1;
}


int socket_interface_remote_getsockname(int socket, struct sockaddr *address, socklen_t *address_len) {
    
    struct socket_interface_remote_socket_state *ss;
    int err_no = socket_interface_remote_get_socket_state_from_id(socket, &ss);
    if (err_no != 0) {
        errno = err_no;
        return -1;
    }

    int ret_value;
    errval_t err = aos_rpc_network_getsockname(ss->rpc, &ret_value, &errno, ss->id, address, address_len);
    if (err_is_fail(err) || ret_value == -1) {
        return -1;
    }

    return 0;
}


int socket_interface_remote_getsockopt(int socket, int level, int option_name, void *option_value, socklen_t *option_len) {
    // not implemented
    return -1;
}


int socket_interface_remote_listen(int socket, int backlog) {
    // not implemented
    return -1;
}


ssize_t socket_interface_remote_recv(int socket, void *buffer, size_t length, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_remote_recvfrom(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {

    struct socket_interface_remote_socket_state *ss;
    int err_no = socket_interface_remote_get_socket_state_from_id(socket, &ss);
    if (err_no != 0) {
        errno = err_no;
        return -1;
    }

    ssize_t ret_size;
    errval_t err = aos_rpc_network_recvfrom(ss->rpc, &ret_size, &errno, ss->id, buffer, length, flags, address, address_len);
    if (err_is_fail(err) || ret_size == -1) {
        return -1;
    }

    return ret_size;
}


ssize_t socket_interface_remote_recvmsg(int socket, struct msghdr *message, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_remote_send(int socket, const void *message, size_t length, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_remote_sendmsg(int socket, const struct msghdr *message, int flags) {
    // not implemented
    return -1;
}


ssize_t socket_interface_remote_sendto(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {

    struct socket_interface_remote_socket_state *ss;
    int err_no = socket_interface_remote_get_socket_state_from_id(socket, &ss);
    if (err_no != 0) {
        errno = err_no;
        return -1;
    }

    ssize_t ret_size;
    errval_t err = aos_rpc_network_sendto(ss->rpc, &ret_size, &errno, ss->id, message, length, flags, dest_addr, dest_len);
    if (err_is_fail(err) || ret_size == -1) {
        return -1;
    }

    return ret_size;
}


int socket_interface_remote_setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len) {
    // not implemented
    return -1;
}


int socket_interface_remote_shutdown(int socket, int how) {
    // not implemented
    return -1;
}


int socket_interface_remote_socket(int domain, int type, int protocol) {

    if (!sirs) {
        int err_no = socket_interface_remote_init();
        if (err_no != 0) {
            errno = err_no;
            return -1;
        }
    }

    // draw next id
    int socket_id = sirs->next_free_id++;
    if (socket_id >= SOCKET_INTERFACE_REMOTE_MAX_ID) {
        errno = EMFILE;
        return -1;
    }

    struct socket_interface_remote_socket_state *ss = (struct socket_interface_remote_socket_state *)malloc(sizeof(struct socket_interface_remote_socket_state));
    if (!ss) {
        errno = ENOMEM;
        sirs->next_free_id--;
        debug_printf("malloc failed: %u\n", errno);
        return -1;
    }

    // create new rpc to charon via gaia
    errval_t err = aos_rpc_name_bind_service(aos_rpc_get_name_channel(), sirs->network_handle, disp_get_core_id(), &ss->rpc);
    if (err_is_fail(err)) {
        errno = ENETDOWN;
        sirs->next_free_id--;
        free(ss);
        debug_printf("aos_rpc_name_bind_service failed: %u\n", errno);
        return -1;
    }

    assert(ss->rpc);

    // send socket request
    err = aos_rpc_network_socket(ss->rpc, &ss->id, &errno, domain, type, protocol);
    if (err_is_fail(err) || ss->id  == -1) {
        sirs->next_free_id--;
        free(ss);
        debug_printf("aos_rpc_network_socket failed: %u\n", err_no);
        return -1;        
    }

    sirs->socket_states[socket_id] = ss;
    return socket_id;
}

int socket_interface_remote_socketpair(int domain, int type, int protocol, int socket_vector[2]) {
    // not implemented
    return -1;
}

