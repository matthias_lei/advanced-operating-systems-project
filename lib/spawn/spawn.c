#include <aos/aos.h>
#include <spawn/spawn.h>

#include <elf/elf.h>
#include <aos/dispatcher_arch.h>
#include <barrelfish_kpi/paging_arm_v7.h>
#include <barrelfish_kpi/domain_params.h>
#include <spawn/multiboot.h>

#include <aos/paging.h>

#include <aos_rpc_server/aos_rpc_server_monitor.h>

#include <aos/debug.h>

#include <ctype.h>

#include <aos/connect/flmp.h>
#include <aos/connect/lmp.h>
#include <aos/connect/ump.h>

extern struct bootinfo *bi;

/**
 * Implementation of one spawn function at the moment:
 *     errval_t spawn_load_by_name(void * binary_name, struct spawninfo * si)
 *
 * The implementation follows the recommended code part of original Barrelfish
 * http://git.barrelfish.org/?p=barrelfish;a=blob;f=lib/spawndomain/spawn.c;h=c5f2d943c921b2a332499d2367cfa8d3906e0405;hb=HEAD
 * with various adjustments to different / limited settings of this version here on the Panda board.
 *
 * version 2017-10-11
 */

/**
 * spawn offers an extension: loading symbols
 * as a standard, the LOAD segments are loaded during spawn with the following sections
 * (r/x: .text .rodata .ARM.exidx; r/w: .fini_array .ctors .data.rel.ro .got .data .bss)
 * in our extension, the .symtab and .strtab are loaded in addition. They can be used for
 * debugging purpose at runtime, e.g. function symbols in stack trace
 * disadvantage: more memory is used
 * 2017-11-04 pisch
 */


/**
 * Update spawn procedure when adding distributed services and nameserver
 * ----------------------------------------------------------------------
 *
 * 2 RPC channels are provided to the freshly spawned domain:
 * - monitor (either /core/monitor/0 or /core/monitor/1)
 * - name service (/core/name/0)
 *
 * memory (/core/memory/0) is connected using late binding already in init
 *
 * The current endpoint is used as monitor (note: these functions only run within a monitor)
 * fresh caps are requested for memory and name for the spawn process the freshly spawned domain
 * will connect directly with them. This is in principly a 2 step version of the service_bind()
 * function offered by the nameservice. Of course, code is shared as much as possible for
 * both parts.
 *
 * Bootstrap:
 * Memory and name services are launched first. They only get partial information.
 * - memory: only monitor0. It will register itself to name service later.
 *   NOTE: memory must never use printf() or other stdlib I/O functions.
 * - name: monitor0 and memory. Also to keep things simple,
 *   name must never use printf() or other stdlib I/O functions.
 * Of course, access to nameserver could be backported to memory service. However with some
 * complexity only that I consider unneeded at the moment.
 * why should a memory / name service do direct I/O communication at all?
 *
 * concept version 2012-12-07, pisch
 */


enum memory_spawn_mode {
    MEMORY_NONE,
    MEMORY_PROVIDED,
    MEMORY_BINDING
};

//--- private helper functions ---------------------------------------------------------------------

// exported for ti02_process_creation low-level test menu
errval_t spawn_map_module(struct spawninfo *si, void **binary, struct mem_region **mr);

errval_t spawn_map_module(struct spawninfo *si, void **binary, struct mem_region **mr) {
    errval_t err;

    if(bi == NULL) {
        debug_printf("Could not get bootinfo struct from stack.\n");
        err = SPAWN_ERR_MAP_BOOTINFO; // cannot find a suited error code...
        return err;
    }

    // Find module <binary_name> in bootinfo structure
    *mr = multiboot_find_module(bi, si->binary_name);
    if (*mr == NULL) {
        debug_printf("Could not find module \"%s\".\n", si->binary_name);
        err = SPAWN_ERR_FIND_MODULE;
        return err;
    }

    // Create capability on binary's frame
    struct capref child_frame = {
        .cnode = cnode_module, //special constant "module cnode"
        .slot = (*mr)->mrmod_slot
    };

    // Map frame of binary into array buf in vspace
    err = paging_map_frame_attr(get_current_paging_state(), binary,
                                   (*mr)->mrmod_size, child_frame,
                                   VREGION_FLAGS_READ, NULL, NULL);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_VNODE_MAP);
    }


    return SYS_ERR_OK;
}


/**
 * \brief Setup an initial cspace
 *
 * Create an initial cspace layout
 *
 * note conventions: these key caprefs must be stored in pre-defined slots.
 */
static errval_t spawn_setup_cspace(struct spawninfo *si)
{
    struct capref t1; // temporary helper capref used in various locations

    // Create root CNode
    errval_t err = cnode_create_l1(&si->rootcn_cap, &si->rootcn);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_ROOTCN);
    }

    // Create taskcn; see below when the details of this capabilities are filled
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_TASKCN, &si->taskcn);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_TASKCN);
    }

    // Create slot_alloc_cnode: need 3 nodes (0, 1, 2)
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_SLOT_ALLOC0, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_SLOT_ALLOC1, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_SLOT_ALLOC2, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }


    // Create DCB: make si->dcb invokable,....
    err = slot_alloc(&si->dcb);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_SLOT_ALLOC);
    }

    err = dispatcher_create(si->dcb);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_DISPATCHER);
    }
    // ..., and copy DCB to new taskcn
    t1.cnode = si->taskcn;
    t1.slot  = TASKCN_SLOT_DISPATCHER;
    err = cap_copy(t1, si->dcb);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_CAP_COPY);
    }

    si->dcb_in_child_space = t1;


    // Give domain endpoint to itself (in taskcn)
    struct capref selfep = {
        .cnode = si->taskcn,
        .slot = TASKCN_SLOT_SELFEP,
    };
    err = cap_retype(selfep, si->dcb, 0, ObjType_EndPoint, 0, 1);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_SELFEP);
    }

    // Map root CNode (in taskcn)
    t1.cnode = si->taskcn;
    t1.slot  = TASKCN_SLOT_ROOTCN;
    err = cap_copy(t1, si->rootcn_cap);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_MINT_ROOTCN);
    }


    // SLOT_DISPFRAME
    // is done in spawn_setup_dispatcher()


    // SLOT_ARGSPG
    // is done in spawn_setup_env()


    // Fill up basecn (i.e. BASE_PAGE_CN)
    struct cnoderef basecn;

    // Create basecn in our rootcn so we can copy stuff in there
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_BASE_PAGE_CN, &basecn);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_CNODE_CREATE);
    }

    // get big RAM cap for L2_CNODE_SLOTS BASE_PAGE_SIZEd caps
    // modified to our ram_alloc that works in size space and not in log size space
    // this allocates 1 MiB of RAM and creates 256 smaller caps of size 4 KiB (see retype)
    // as required for BASE_PAGE_CN
    struct capref ram;
    err = ram_alloc(&ram, 1 << (L2_CNODE_BITS + BASE_PAGE_BITS) );
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_RAM_ALLOC);
    }

    // retype big RAM cap into small caps in new basecn
    struct capref base = {
        .cnode = basecn,
        .slot = 0,
    };
    err = cap_retype(base, ram, 0, ObjType_RAM, BASE_PAGE_SIZE, L2_CNODE_SLOTS);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_CAP_RETYPE);
    }


    // delete big RAM cap ???
    // TO DISCUSS pisch: I am not sure yet whether we better keep this large RAM
    // capability around e.g. in the spawninfo struct for later release
    // back to our RAM manager instead of destroying it.
    /*
    err = cap_destroy(ram);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_CAP_DESTROY);
    }
    //*/

    // thus: currently keep it around
    si->orig_base_page_cn_ram_capref = ram;

    // point 6: SLOT_PAGECN
    // -> this is done as first point in spawn_setup_vspace()

    return SYS_ERR_OK;
}


static errval_t spawn_setup_vspace(struct spawninfo *si)
{
    // Create pagecn (see SLOT_PAGECN)
    errval_t err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_PAGECN, &si->pagecn);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_PAGECN);
    }

    // Init pagecn's slot allocator
    si->pagecn_cap.cnode = si->rootcn;
    si->pagecn_cap.slot = ROOTCN_SLOT_PAGECN;

    // XXX: satisfy a peculiarity of the single_slot_alloc_init_raw API
    size_t bufsize = SINGLE_SLOT_ALLOC_BUFLEN(L2_CNODE_SLOTS);

    void *buf = malloc(bufsize);

    if (!buf) {
        return err_push(err, LIB_ERR_MALLOC_FAIL);
    }

    err = single_slot_alloc_init_raw(&si->pagecn_slot_alloc, si->pagecn_cap,
                                     si->pagecn, L2_CNODE_SLOTS,
                                     buf, bufsize);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_SINGLE_SLOT_ALLOC_INIT_RAW);
    }

    // Create root of pagetable
    err = si->pagecn_slot_alloc.a.alloc(&si->pagecn_slot_alloc.a, &si->pdir);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_SLOT_ALLOC);
    }

    // top-level table (L1) should always live in slot 0 of pagecn
    assert(si->pdir.slot == 0);

    // we use case CPU_ARM7 for Pandaboard
    err = vnode_create(si->pdir, ObjType_VNode_ARM_l1);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_VNODE);
    }

    // preparation of our own AOS specific data structures
    si->pstate = malloc(sizeof(struct paging_state));
    if (!si->pstate) {
        return err_push(err, LIB_ERR_MALLOC_FAIL);
    }

    // it is important to copy the pdir of the child CSpace
    // also into the CSpace of the parent -> parent_pdir
    // to allow initialization of paging_init_state() already
    // while working in parent CSpace during this spawn
    struct capref parent_pdir;

    err = slot_alloc(&parent_pdir);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_SLOT_ALLOC);
    }
    err = cap_copy(parent_pdir, si->pdir);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_SLOT_ALLOC);
    }

    err = paging_init_state(si->pstate, VADDR_OFFSET, parent_pdir, &si->pagecn_slot_alloc.a);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_VSPACE_INIT);
    }

    return SYS_ERR_OK;
}


/**
 * flags conversion
 */
static inline int elf_to_vregion_flags(uint32_t flags) {
    int vregion_flags = ((flags & PF_R) ? VREGION_FLAGS_READ : VREGION_FLAGS_NORIGHTS)
        | ((flags & PF_W) ? VREGION_FLAGS_WRITE : VREGION_FLAGS_NORIGHTS)
        | ((flags & PF_X) ? VREGION_FLAGS_EXECUTE : VREGION_FLAGS_NORIGHTS);
    return vregion_flags;
}


/*
    Mostly copied from the Barrelfish GitHub repository

    https://github.com/BarrelfishOS/barrelfish/blob/master/lib/spawndomain/arch/arm/spawn_arch.c
*/
/**
 * This function is used as callback function in elf_load(). See required interface.
 * The function prepares enough frames and maps them at a valid location in parent VSpace and
 * at defined location (see base) in the child VSpace. Location in parent VSpace is returned in *ret.
 */
static errval_t elf_allocate(void* state, genvaddr_t base, size_t size, uint32_t flags, void **ret) {
    assert(state);

    errval_t err = SYS_ERR_OK;

    struct spawninfo *si = state;


    // note: genvaddr_t is 64 bit unsigned
    // all other sizes / pointers are 32 bit unsigned on our ARMv7 Pandaboard

    size_t base_offset = (size_t) BASE_PAGE_OFFSET(base);
    size += base_offset;
    base -= base_offset;
    // Page-align
    size = ROUND_UP(size, BASE_PAGE_SIZE);

    struct capref frame;
    size_t retsize;
    err = frame_alloc(&frame, size, &retsize);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc() failed\n");
        return err;
    }

    err = paging_map_frame_attr(get_current_paging_state(), ret, retsize, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr() failed\n");
        return err;
    }

    *ret += base_offset;

    err = paging_map_fixed_attr(si->pstate, (lvaddr_t)base, frame, retsize, elf_to_vregion_flags(flags));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_fixed_attr() failed\n");
        return err;
    }

    // prepare meta data
    if (!si->pstate->elf_info.base) {
        si->pstate->elf_info.base = base;
    }
    else if (base < si->pstate->elf_info.base) {
        si->pstate->elf_info.base = base;
    }

    size_t new_size = base + size - si->pstate->elf_info.base;
    if (si->pstate->elf_info.size < new_size) {
        si->pstate->elf_info.size = new_size;
    }

    return SYS_ERR_OK;

}


/**
 * This function is used by us internally to load additional ELF sections like .symtab and .strtab
 * for debugging purposes. Thus, the function signature is already adjusted to be more specific.
 *
 * The function prepares enough frames and maps them at a valid location in parent VSpace and at
 * an arbitrary free location in the child VSpace.
 * Location in parent VSpace is returned in *parent_ret; location in child VSpace in *child_ret.
 * Note: it is save to transfer child lvaddr_t addresses from spawn here in parent to the
 * actual child process that will run later.
 */
static errval_t elf_allocate_position_independent(struct spawninfo *si, size_t size, uint32_t flags,
                                                  void **parent_ret, lvaddr_t *child_ret) {
    assert(si);

    errval_t err = SYS_ERR_OK;

    // Page-align
    size = ROUND_UP(size, BASE_PAGE_SIZE);

    struct capref frame;
    size_t retsize;
    err = frame_alloc(&frame, size, &retsize);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc() failed\n");
        return err;
    }

    err = paging_map_frame_attr(get_current_paging_state(), parent_ret, retsize, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr() failed\n");
        return err;
    }

    // convert ELF flags to VREGION flags
    err = paging_map_frame_attr(si->pstate, (void **)child_ret, retsize, frame, elf_to_vregion_flags(flags), NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_fixed_attr() failed\n");
        return err;
    }

    return SYS_ERR_OK;
}

/**
 * Loads an optional ELF section to an arbitrary free address in both.
 */
static errval_t load_additional_elf_section(struct spawninfo *si, void *elf_binary, size_t elf_binary_size,
                                            const char *section_name, lvaddr_t *ret_base, size_t *ret_size)
{
    assert(si);
    assert(elf_binary);
    assert(section_name);
    assert(ret_base);
    assert(ret_size);

    *ret_base = 0;
    *ret_size = 0;

    struct Elf32_Shdr *section = elf32_find_section_header_name((lvaddr_t) elf_binary, elf_binary_size, section_name);
    if (!section) {
        // not found
        return ELF_ERR_HEADER;
    }

    // All these sections are loaded with read-only rights in the child VSpace.
    // This may be changed into R/W if adjustments are needed due to code relocations.
    void *parent_addr = NULL;
    errval_t err = elf_allocate_position_independent(si, section->sh_size, PF_R, &parent_addr, ret_base);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "elf_allocate_position_independent() failed while loading section %s.\n", section_name);
        return err;
    }

    *ret_size = section->sh_size;

    if (elf_binary_size < (section->sh_offset + section->sh_size)) {
        err = ELF_ERR_FILESZ;
        err = err_push(err, SPAWN_ERR_MAP_MODULE);
        DEBUG_ERR(err, "the loaded ELF binary is not large enough to provide the entire section %s.\n", section_name);
        return err;
    }

    memcpy(parent_addr, elf_binary + section->sh_offset, section->sh_size);
    return err;
}


static errval_t spawn_load_elf(struct spawninfo *si, void *binary, size_t binary_size, lvaddr_t *got, uint32_t *entry)
{
    genvaddr_t retentry;
    errval_t err = elf_load(EM_ARM, elf_allocate, si, (lvaddr_t) binary, binary_size, &retentry);
    if (err_is_fail(err)) {
        return err;
    }
    *entry = (uint32_t)retentry;

    // get GOT section
    static const char* section_name = ".got";
    struct Elf32_Shdr *got_addr = elf32_find_section_header_name((lvaddr_t) binary, binary_size, section_name);
    if (got_addr == NULL) {
        debug_printf("Could not find section header with name \"%s\".", section_name);
        err = ELF_ERR_HEADER;
        return err;
    }

    *got = got_addr->sh_addr;

    if (config_get_spawn_load_symbols()) {
        // additionally load symbol table for runtime lookup
        // note: this has to happen here and cannot happen during elf_load() (at least not in current implementation)
        // also note: of course, only the relevant LOAD parts of the ELF file are loaded during elf_load(),
        // i.e. there is not even an ELF magic signature at the loaded address.
        // Therefore, also own accessors will be used for symbol and name lookups.
        err = load_additional_elf_section(si, binary, binary_size, ".symtab",
                                          &si->pstate->elf_info.symtab_base, &si->pstate->elf_info.symtab_size);
        if (err_is_fail(err)) {
            if (err_no(err) != ELF_ERR_HEADER) {
                // 'not found' is not an error; additionally, these errors do not stop the spawn process
                DEBUG_ERR(err, "load_additional_elf_section() failed while loading .symtab\n");
            }
        }

        err = load_additional_elf_section(si, binary, binary_size, ".strtab",
                                          &si->pstate->elf_info.strtab_base, &si->pstate->elf_info.strtab_size);
        if (err_is_fail(err)) {
            if (err_no(err) != ELF_ERR_HEADER) {
                // 'not found' is not an error; additionally, these errors do not stop the spawn process
                DEBUG_ERR(err, "load_additional_elf_section() failed while loading .strtab\n");
            }
        }
    }

    return SYS_ERR_OK;
}

static errval_t spawn_setup_dispatcher(struct spawninfo* si, lvaddr_t got_base, uint32_t entry)
{
    errval_t err;

    /* Create dispatcher frame (in taskcn) */
    si->dispframe.cnode = si->taskcn;
    si->dispframe.slot  = TASKCN_SLOT_DISPFRAME;
    err = frame_create(si->dispframe, (1 << DISPATCHER_FRAME_BITS), NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_DISPATCHER_FRAME);
    }

    /* Map in dispatcher frame */
    err = paging_map_frame_attr(get_current_paging_state(), (void**)&si->handle, 1ul << DISPATCHER_FRAME_BITS, si->dispframe, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_MAP_DISPATCHER_TO_SELF);
    }
    lvaddr_t spawn_dispatcher_base;
    err = paging_map_frame_attr(si->pstate, (void**)&spawn_dispatcher_base, 1UL << DISPATCHER_FRAME_BITS, si->dispframe, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_MAP_DISPATCHER_TO_NEW);
    }

    /* Set initial state */
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(si->handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(si->handle);
    struct dispatcher_shared_arm *disp_arm = get_dispatcher_shared_arm(si->handle);
    arch_registers_state_t *enabled_area = dispatcher_get_enabled_save_area(si->handle);
    arch_registers_state_t *disabled_area = dispatcher_get_disabled_save_area(si->handle);

    /* Place core_id */
    disp_gen->core_id = si->core_id;
    disp_gen->domain_id = si->domain_id;

    /* Setup dispatcher and make it runnable */
    disp->udisp = spawn_dispatcher_base;
    disp->disabled = 1;
    disp->fpu_trap = 1;

    // Copy the name for debugging
    const char *copy_name = strrchr(si->binary_name, '/');
    if (copy_name == NULL) {
        copy_name = si->binary_name;
    } else {
        copy_name++;
    }
    strncpy(disp->name, copy_name, DISP_NAME_LEN);

    disp_arm->got_base = got_base; // Address of .got in childs VSpace.
    enabled_area->regs[REG_OFFSET(PIC_REGISTER)] = got_base; //same as above
    disabled_area->regs[REG_OFFSET(PIC_REGISTER)] = got_base; //same as above
    disabled_area->named.pc = entry;

    enabled_area->named.cpsr = CPSR_F_MASK | ARM_MODE_USR;
    disabled_area->named.cpsr = CPSR_F_MASK | ARM_MODE_USR;
    disp_gen->eh_frame = 0;
    disp_gen->eh_frame_size = 0;
    disp_gen->eh_frame_hdr = 0;
    disp_gen->eh_frame_hdr_size = 0;

    return SYS_ERR_OK;
}

static errval_t spawn_setup_env(struct spawninfo *si, struct spawn_domain_params **ret_params)
{
    errval_t err;

    // create frame (actually multiple pages) for arguments
    si->argspg.cnode = si->taskcn;
    si->argspg.slot  = TASKCN_SLOT_ARGSPAGE;
    err = frame_create(si->argspg, ARGS_SIZE, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_CREATE_ARGSPG);
    }

    // map environment frame in parent
    char* envbuf;
    err = paging_map_frame_attr(get_current_paging_state(), (void*)&envbuf, ARGS_SIZE, si->argspg, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_MAP_ARGSPG_TO_SELF);
    }

    // map environment frame in child
    char* envbuf_child;
    err = paging_map_frame_attr(si->pstate, (void*)&envbuf_child, ARGS_SIZE, si->argspg, VREGION_FLAGS_READ, NULL, NULL);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_MAP_ARGSPG_TO_NEW);
    }

    // set virtual address of mapped arguments page
    arch_registers_state_t *enabled_area = dispatcher_get_enabled_save_area(si->handle);
    enabled_area->named.r0 = (uint32_t)envbuf_child;

    // setup the parameters
    size_t envbuf_len = ARGS_SIZE;
    assert(envbuf_len >= sizeof(struct spawn_domain_params));
    struct spawn_domain_params *params = (void*)envbuf;
    *ret_params = params;

    envbuf += sizeof(struct spawn_domain_params);
    envbuf_child += sizeof (struct spawn_domain_params);
    envbuf_len -= sizeof(struct spawn_domain_params);

    // set up the command line arguments
    params->argc = 0;

    char* args_base = envbuf;
    char* args_buf = envbuf;
    const char *opts = si->command_line;
    while (*opts && params->argc < MAX_CMDLINE_ARGS) {
        // strip leading whitespaces
        while (isspace(*opts)) {
            opts++;
        }
        // parse argument
        const char *arg = opts;
        while (*opts && !isspace(*opts)) {
            opts++;
        }
        size_t arg_len = opts - arg;
        if (!arg_len) {
            continue;
        }
        if (arg_len + 1 > envbuf_len) {
            return SPAWN_ERR_ARGSPG_OVERFLOW;
        }
        // copy argument to arguments page
        memcpy(args_buf, arg, arg_len);
        args_buf[arg_len] = '\0';
        params->argv[params->argc] = envbuf_child + (args_buf - args_base);
        params->argc++;
        args_buf += arg_len + 1;
        envbuf_len -= arg_len + 1;
    }
    if (*opts) {
        // we hit the maximum possible number of command line arguments
        // strip trailing whitespaces
        while (isspace(*opts)) {
            opts++;
        }
        // if we do not encounter a terminating 0 character here, we have an additional command line argument
        if (*opts) {
            debug_printf("Number of command line arguments exceeds maximal number %d of allowed arguments. Remaining arguments are ignored.\n", MAX_CMDLINE_ARGS);
        }
    }
    params->argv[params->argc] = NULL;

    envbuf = args_buf;
    envbuf_child += (args_buf - args_base);

    // set up the environment variables
    // TODO jmeier: implement passing of environment variables
    //              for now, we pass no environment variables at all
    params->envp[0] = NULL;

    // serialize the vspace

    // we need to align the buffer to pointer size (max alignment requirement)
    // in order to support accessing and storing arbitrary structs
    size_t align_offset = (char*)ROUND_UP((lvaddr_t)envbuf, sizeof(void*)) - envbuf;
    envbuf += align_offset;
    envbuf_child += align_offset;
    envbuf_len -= align_offset;

    params->vspace_buf = envbuf_child;
    err = paging_state_serialize(si->pstate, envbuf, envbuf_len, &params->vspace_buf_len);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_SERIALISE_VSPACE);
    }

    // set up the thread local storage
    params->tls_init_base = NULL;
    params->tls_init_len = 0;
    params->tls_total_len = 0;
    params->pagesize = BASE_PAGE_SIZE;

    params->shell_id = si->shell_id;

    return SYS_ERR_OK;
}

static errval_t spawn_common(struct spawninfo *si,
                             enum memory_spawn_mode memory_mode,
                             bool with_name)
{
    errval_t err = SYS_ERR_OK;

    si->core_id = disp_get_core_id();

    // - Setup childs cspace
    err = spawn_setup_cspace(si);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_SETUP_CSPACE);
    }

    // - Setup childs vspace
    err = spawn_setup_vspace(si);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "setup vspace\n");
        return err_push(err, SPAWN_ERR_VSPACE_INIT);
    }

    // - Load the ELF binary
    lvaddr_t got_base;
    uint32_t entry;
    err = spawn_load_elf(si, si->binary, si->binary_size, &got_base, &entry);
    if (err_is_fail(err) ) {
        return err_push(err, SPAWN_ERR_ELF_MAP);
    }

    // - Setup dispatcher
    err = spawn_setup_dispatcher(si, got_base, entry);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_SETUP_DISPATCHER);
    }

    // - Setup environment
    struct spawn_domain_params *params;
    err = spawn_setup_env(si, &params);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_SETUP_ENV);
    }

    // - Setup channel to monitor
    struct spawn_chan_params *mon = &params->monitor_params;
    struct capref monitor_endpoint_remote = {
        .cnode = si->taskcn,
        .slot = TASKCN_SLOT_MONITOREP
    };

    // currently LMP
    err = setup_lmp_service(monitor_endpoint_remote, MKCLOSURE(aos_rpc_monitor_event_handler, NULL));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "setup_flmp_service failed\n");
        return err;
    }

    coreid_t core = disp_get_core_id();
    mon->routing_info = AOS_RPC_ROUTING_SET(core, core);
    mon->interface = AOS_RPC_INTERFACE_MONITOR;
    mon->enumerator = (size_t)core;
    mon->chan_type = AOS_RPC_CHAN_DRIVER_LMP;


    // lookups and initial binding with memory and name services
    struct aos_rpc *name_rpc = aos_rpc_get_name_channel();

    // memory
    struct spawn_chan_params *mem = &params->memory_params;
    if (memory_mode == MEMORY_PROVIDED) {
        // TODO pisch: this mode can be removed if all keeps working well.
        service_handle_t memory_handle;
        err = aos_rpc_name_lookup_service_by_path(name_rpc, "/core/memory", &memory_handle);
        if (err_is_fail(err)) {
            return err;
        }

        struct capref new_memory_cap;
        err = aos_rpc_name_bind_service_lowlevel(name_rpc, memory_handle, core,
                                                &mem->routing_info,
                                                &mem->interface,
                                                &mem->enumerator,
                                                &mem->chan_type,
                                                &new_memory_cap);
        if (err_is_fail(err)) {
            return err;
        }

        struct capref memory_endpoint_remote = {
            .cnode = si->taskcn,
            .slot = TASKCN_SLOT_MEMORYEP
        };
        err = cap_copy(memory_endpoint_remote, new_memory_cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "cap_copy() failed.\n");
            return err;
        }
    }
    else if (memory_mode == MEMORY_BINDING) {
        mem->chan_type = AOS_RPC_CHAN_DRIVER_NONE_LATE_BINDING;
    }
    else {
        mem->chan_type = AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP;
    }

    // name
    struct spawn_chan_params *name = &params->name_params;
    if (with_name) {
        service_handle_t name_handle;
        err = aos_rpc_name_lookup_service_by_path(name_rpc, "/core/name", &name_handle);
        if (err_is_fail(err)) {
            return err;
        }

        struct capref new_name_cap;
        err = aos_rpc_name_bind_service_lowlevel(name_rpc, name_handle, core,
                                                &name->routing_info,
                                                &name->interface,
                                                &name->enumerator,
                                                &name->chan_type,
                                                &new_name_cap);
        if (err_is_fail(err)) {
            return err;
        }

        struct capref name_endpoint_remote = {
            .cnode = si->taskcn,
            .slot = TASKCN_SLOT_NAMEEP
        };
        err = cap_copy(name_endpoint_remote, new_name_cap);
        if (err_is_fail(err)) {
            return err;
        }
    }
    else {
        name->chan_type = AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP;
    }

    // - Make dispatcher runnable
    err = invoke_dispatcher(si->dcb, cap_dispatcher,
                  si->rootcn_cap, si->pdir,
                  si->dispframe, true);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_RUN);
    }

    return err;
}

static errval_t spawn_load_by_name_with_id_helper(const void *binary_name,
                                                  domainid_t domain_id,
                                                  int shell_id,
                                                  struct spawninfo *si,
                                                  enum memory_spawn_mode memory_mode,
                                                  bool with_name)
{
    assert(binary_name);
    assert(si);

    errval_t err = SYS_ERR_OK;

    // Init spawninfo
    memset(si, 0, sizeof(struct spawninfo));
    si->binary_name = binary_name;
    si->domain_id = domain_id;
    si->shell_id = shell_id;

    // - Map multiboot module in your address space
    struct mem_region *mr = NULL;
    err = spawn_map_module(si, &si->binary, &mr);
    if (err_is_fail(err)) {
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }

    si->command_line = multiboot_module_opts(mr);
    si->binary_size = mr->mrmod_size;

    return spawn_common(si, memory_mode, with_name);
}

static errval_t spawn_load_by_path_with_id_helper(const char *path,
                                                  const char *command_line,
                                                  domainid_t domain_id,
                                                  int shell_id,
                                                  struct spawninfo *si,
                                                  enum memory_spawn_mode memory_mode,
                                                  bool with_name)
{
    assert(path);
    assert(command_line);
    assert(si);

    si->binary_name = strrchr(path, '/');
    if (!si->binary_name) {
        si->binary_name = path;
    }
    si->command_line = command_line;
    si->domain_id = domain_id;
    si->shell_id = shell_id;

    FILE *binary_handle = fopen(path, "r");
    if (!binary_handle) {
        debug_printf("fopen failed.\n");
        return SPAWN_ERR_FIND_MODULE;
    }

    if (fseek(binary_handle, 0, SEEK_END)) {
        fclose(binary_handle);
        debug_printf("fclose failed.\n");
        return SPAWN_ERR_FIND_MODULE;
    }

    si->binary_size = ftell(binary_handle);
    if (si->binary_size <= 0) {
        debug_printf("ftell failed.\n");
        fclose(binary_handle);
        return SPAWN_ERR_FIND_MODULE;
    }

    if (fseek(binary_handle, 0, SEEK_SET)) {
        debug_printf("fseek failed.\n");
        fclose(binary_handle);
        return SPAWN_ERR_FIND_MODULE;
    }

    si->binary = malloc(si->binary_size);
    if (!si->binary) {
        debug_printf("malloc failed.\n");
        fclose(binary_handle);
        return LIB_ERR_MALLOC_FAIL;
    }

    size_t read_bytes = fread(si->binary, 1, si->binary_size, binary_handle);
    fclose(binary_handle);
    if (read_bytes != si->binary_size) {
        debug_printf("fread failed.\n");
        return SPAWN_ERR_FIND_MODULE;
    }

    return spawn_common(si, memory_mode, with_name);
}


//--- implementation of the public interface -------------------------------------------------------

// note: this function interface was provided. The function is no longer used by our implementation
errval_t spawn_load_by_name(const void * binary_name, struct spawninfo * si)
{
    return spawn_load_by_name_with_id_helper(binary_name, 0, -1, si, MEMORY_BINDING, true);
}


//--- Group C: implementation of our extended public interface -------------------------------------

errval_t spawn_bootstrap_domain_monitor_only(const void *binary_name, domainid_t domain_id)
{
    struct spawninfo si;
    return spawn_load_by_name_with_id_helper(binary_name, domain_id, -1, &si, MEMORY_NONE, false);
}

errval_t spawn_bootstrap_domain_monitor_and_memory(const void *binary_name, domainid_t domain_id)
{
    struct spawninfo si;
    // note: nameserver currently uses a different solution to get to memory
    // TODO pisch: this will be formalized in a next MR.
    // probably this function stub will be removed.
    return spawn_load_by_name_with_id_helper(binary_name, domain_id, -1, &si, MEMORY_NONE, false);
}

errval_t spawn_load_by_name_with_id(const void *binary_name, int shell_id, domainid_t domain_id)
{
    struct spawninfo si;
    return spawn_load_by_name_with_id_helper(binary_name, domain_id, shell_id, &si, MEMORY_BINDING, true);
}

errval_t spawn_load_by_path_with_id(const char *path, const char *command_line, int shell_id, domainid_t domain_id)
{
    struct spawninfo si;
    return spawn_load_by_path_with_id_helper(path, command_line, domain_id, shell_id, &si, MEMORY_BINDING, true);
}

/**
 * This function is only provided for init because all the other symbols are loaded during spawn when
 * the module / elf file is already in memory for loading of the other sections.
 *
 * Similar to the spawn process, this function checks config_get_spawn_load_symbols() before
 * actually loading the symbols.
 *
 * Note: this function must be called after memory management and paging infrastructure has been
 * established in init.
 *
 * This mapping comes with a memory cost: the init module is mapped again into its VSpace, which cannot be regained
 * until unmap / cleanup has been implemented. However, to not waste additional memory, the .symtab and .strtab
 * sections are not copied from there but directly used in that mapped memory space, which is read-only, too.
 *
 * If an unmap call is implemented here in the future; .symtab and .strtab need to be copied out first
 * similar to the procedure used during spawn.
 */
#define INIT_NAME "init"

static errval_t load_elf_symbols_for_init_helper(void *elf_binary, size_t elf_binary_size, const char *section_name,
                                                 lvaddr_t *ret_base, size_t *ret_size)
{
    struct Elf32_Shdr *section = elf32_find_section_header_name((lvaddr_t) elf_binary, elf_binary_size, section_name);
    if (!section) {
        // not found
        return ELF_ERR_HEADER;
    }

    *ret_base = (lvaddr_t)elf_binary + section->sh_offset;
    *ret_size = section->sh_size;
    return SYS_ERR_OK;
}


errval_t load_elf_symbols_for_init(void)
{
    if (!config_get_spawn_load_symbols()) {
        // consistency over the entire system.
        return SYS_ERR_OK;
    }

    struct spawninfo si;
    memset(&si, 0, sizeof(struct spawninfo));
    si.binary_name = INIT_NAME;

    void *elf_binary;
    struct mem_region *mr = NULL;

    errval_t err = spawn_map_module(&si, &elf_binary, &mr);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Could not map " INIT_NAME " module.\n");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    size_t elf_binary_size = mr->mrmod_size;

    struct paging_state *st = get_current_paging_state();
    assert(st);
    struct paging_elf_key_data *elf_info = &st->elf_info;

    err = load_elf_symbols_for_init_helper(elf_binary, elf_binary_size, ".symtab",
                                           &elf_info->symtab_base, &elf_info->symtab_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Could not load .symtab section.\n");
        return err;
    }

    err = load_elf_symbols_for_init_helper(elf_binary, elf_binary_size, ".strtab",
                                           &elf_info->strtab_base, &elf_info->strtab_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Could not load .strtab section.\n");
        return err;
    }

    return SYS_ERR_OK;
}
