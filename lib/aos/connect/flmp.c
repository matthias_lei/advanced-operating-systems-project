/**
 * Provides FLMP specific messaging functions.
 * AOS class 2017, group C
 */

#include <aos/connect/flmp.h>

#include <aos_rpc_server/aos_rpc_server_common.h>

//--- created service: includes registering an event handler  --------------------------------------

// it uses a Muenchhausen trick to keep the memory alive while needed but delete when finished
// see when it is allocated and when it is freed
struct flmp_data {
    struct flmp_chan *flmp_chan;
    struct event_closure event_handler;
};

static void spawn_flmp_endpoint_recv_handler(void *arg)
{
    assert(arg);

    struct flmp_data *data = arg;
    struct flmp_chan *flmp_chan = data->flmp_chan;

    errval_t err;
    // re-register if there is no message to be received yet
    if (!flmp_chan_can_recv_short(flmp_chan)) {
        struct waitset *ws = get_default_waitset();
        err = flmp_chan_register_recv(flmp_chan, ws, MKCLOSURE(spawn_flmp_endpoint_recv_handler, arg));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_register_recv failed.\n");
            free(data);
            return;
        }
    }

    struct lmp_recv_msg msg = LMP_RECV_MSG_INIT;
    struct capref recv_cap;

    err = flmp_chan_recv_short(flmp_chan, &msg, &recv_cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_recv failed.\n");
        free(data);
        return;
    }

    assert(!capref_is_null(recv_cap));
    // successfully established channel
    flmp_chan->lmp_chan.remote_cap = recv_cap;

    struct capref fast_frame;
    size_t ret_size;
    err = frame_alloc(&fast_frame, BASE_PAGE_SIZE, &ret_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): slot_alloc failed.\n");
        free(data);
        return;
    }

    void *fast_buf;
    err = paging_map_frame_attr(get_current_paging_state(), &fast_buf, ret_size, fast_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): paging_map_frame_attr failed.\n");
        free(data);
        return;
    }

    memset(fast_buf, 0, BASE_PAGE_SIZE);

    err = flmp_chan_connect(flmp_chan, fast_buf, ret_size, false);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): flmp_chan_connect failed.\n");
        free(data);
        return;
    }

    // acknowledge channel
    uintptr_t ack = AOS_RPC_ACK(0);
    err = flmp_chan_send_short(flmp_chan, LMP_SEND_FLAGS_DEFAULT, fast_frame, &ack, sizeof(ack));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_send1 failed.\n");
        free(flmp_chan);
        free(data);
        return;
    }

    struct aos_rpc_channel *rpc_chan = malloc(sizeof(*rpc_chan));
    if (!rpc_chan) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "endpoint_recv_handler(): malloc failed for aos_rpc_channel.\n");
        free(flmp_chan);
        free(data);
        return;
    }

    // setup rpc event handler
    struct aos_rpc_server_comm_state* init_handler_state = malloc(sizeof(*init_handler_state));
    if (!init_handler_state) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "endpoint_recv_handler(): malloc failed for aos_rpc_event_handler_state_init.\n");
        free(flmp_chan);
        free(rpc_chan);
        free(data);
        return;
    }

    aos_rpc_channel_flmp_init(rpc_chan, flmp_chan);

    // always have a slot prepared to receive a capability
    err = flmp_chan_alloc_recv_slot(rpc_chan->flmp_chan);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "flmp_chan_alloc_recv_slot failed.\n");
        free(flmp_chan);
        free(rpc_chan);
        free(data);
        return;
    }

    aos_rpc_server_comm_state_init(init_handler_state, rpc_chan);

    // register rpc event handler
    err = flmp_chan_register_recv(flmp_chan, get_default_waitset(), MKCLOSURE(data->event_handler.handler, init_handler_state));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_register_recv failed.\n");
        free(flmp_chan);
        free(rpc_chan);
        free(init_handler_state);
        free(data);
        return;
    }

    // all OK
    free(data);
}

// note: only the function of the event handler closure is used; data will be replaced
// with new RPC state
errval_t setup_flmp_service(struct capref endpoint_cap, struct event_closure event_handler)
{
    struct flmp_chan* flmp_chan = malloc(sizeof(*flmp_chan));
    if (!flmp_chan) {
        return LIB_ERR_MALLOC_FAIL;
    }

    flmp_chan_init(flmp_chan);

    errval_t err = flmp_chan_accept(flmp_chan, DEFAULT_LMP_BUF_WORDS, NULL_CAP);
    if (err_is_fail(err)) {
        free(flmp_chan);
        return err;
    }

    err = cap_copy(endpoint_cap, flmp_chan->lmp_chan.local_cap);
    if (err_is_fail(err)) {
        free(flmp_chan);
        return err;
    }

    err = flmp_chan_alloc_recv_slot(flmp_chan);
    if (err_is_fail(err)) {
        free(flmp_chan);
        return err;
    }

    struct flmp_data *data = malloc(sizeof(*data));
    if (!data) {
        free(flmp_chan);
        return err;
    }

    data->flmp_chan = flmp_chan;
    data->event_handler = event_handler;

    err = flmp_chan_register_recv(flmp_chan, get_default_waitset(), MKCLOSURE(spawn_flmp_endpoint_recv_handler, data));
    if (err_is_fail(err)) {
        free(flmp_chan);
        free(data);
        return err;
    }

    return SYS_ERR_OK;
}


//--- connect (see init and bind) ------------------------------------------------------------------

struct flmp_setup_state {
    struct flmp_chan* flmp_chan;
    bool complete;
    errval_t err;
};

static void flmp_setup_recv_handler(void *arg) {
    assert(arg);

    struct flmp_setup_state* state = arg;
    errval_t err;

    // re-register if there is no message to be received yet
    if (!flmp_chan_can_recv_short(state->flmp_chan)) {
        struct waitset *ws = get_default_waitset();
        err = flmp_chan_register_recv(state->flmp_chan, ws, MKCLOSURE(flmp_setup_recv_handler, arg));
        if (err_is_fail(err)) {
            state->err = err_push(err, LIB_ERR_CHAN_REGISTER_RECV);
            state->complete = true;
            return;
        }
    }

    struct lmp_recv_msg msg = LMP_RECV_MSG_INIT;
    struct capref fast_frame;
    err = flmp_chan_recv_short(state->flmp_chan, &msg, &fast_frame);
    if (err_is_fail(err)) {
        state->err = err_push(err, LIB_ERR_LMP_CHAN_RECV);
        state->complete = true;
        return;
    }
    if (!AOS_RPC_ACK_OK(msg.words[0])) {
        state->err = LIB_ERR_LMP_CHAN_BIND;
        state->complete = true;
        return;
    }

    void *fast_buf;
    err = paging_map_frame_attr(get_current_paging_state(), &fast_buf, BASE_PAGE_SIZE, fast_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        state->err = err;
        state->complete = true;
        return;
    }

    err = flmp_chan_connect(state->flmp_chan, fast_buf, BASE_PAGE_SIZE, true);
    if (err_is_fail(err)) {
        state->err = err;
        state->complete = true;
        return;
    }

    // successfully established channel
    state->err = SYS_ERR_OK;
    state->complete = true;
}

errval_t setup_flmp_client(struct capref remote_cap,
                           enum aos_rpc_interface interface, void *interface_vtable,
                           routing_info_t routing, struct aos_rpc **ret_rpc)
{

    /* allocate lmp channel structure */
    struct flmp_chan* flmp_chan = malloc(sizeof(*flmp_chan));
    if (!flmp_chan) {
        return LIB_ERR_MALLOC_FAIL;
    }
    flmp_chan_init(flmp_chan);

    /* create local endpoint */
    struct capref local_cap;
    errval_t err = endpoint_create(DEFAULT_LMP_BUF_WORDS, &local_cap, &flmp_chan->lmp_chan.endpoint);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_ENDPOINT_CREATE);
    }

    /* set remote endpoint to given endpoint */
    flmp_chan->lmp_chan.remote_cap = remote_cap;

    /* set receive handler */
    struct flmp_setup_state state = {
        .flmp_chan = flmp_chan,
        .complete = false
    };

    err = flmp_chan_alloc_recv_slot(flmp_chan);
    if (err_is_fail(err)) {
        free(flmp_chan);
        return err_push(err, LIB_ERR_LMP_ALLOC_RECV_SLOT);
    }

    err = flmp_chan_register_recv(flmp_chan, get_default_waitset(), MKCLOSURE(flmp_setup_recv_handler, &state));
    if (err_is_fail(err)) {
        free(flmp_chan);
        return err_push(err, LIB_ERR_CHAN_REGISTER_RECV);
    }

    /* send local ep to init */
    err = flmp_chan_send_short(flmp_chan, LMP_SEND_FLAGS_DEFAULT, local_cap, NULL, 0);
    if (err_is_fail(err)) {
        free(flmp_chan);
        return err_push(err, LIB_ERR_LMP_CHAN_SEND);
    }

    /* wait for init to acknowledge receiving the endpoint */
    struct waitset* ws = get_default_waitset();
    while (!state.complete) {
        err = event_dispatch(ws);
        if (err_is_fail(err)) {
            free(flmp_chan);
            return err_push(err, LIB_ERR_EVENT_DISPATCH);
        }
    }

    if (err_is_fail(state.err)) {
        free(flmp_chan);
        return state.err;
    }

    /* initialize init RPC client with lmp channel */
    struct aos_rpc_channel *chan = malloc(sizeof(*chan));
    if (!chan) {
        free(flmp_chan);
        return LIB_ERR_MALLOC_FAIL;
    }

    aos_rpc_channel_flmp_init(chan, flmp_chan);

    *ret_rpc = malloc(sizeof(**ret_rpc));
    if (!*ret_rpc) {
        free(flmp_chan);
        free(chan);
        return LIB_ERR_MALLOC_FAIL;
    }
    aos_rpc_init(*ret_rpc, chan, interface, interface_vtable, routing);

    return SYS_ERR_OK;
}
