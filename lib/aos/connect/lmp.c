/**
 * Provides LMP specific messaging functions.
 * AOS class 2017, group C
 */

#include <aos/connect/lmp.h>

#include <aos_rpc_server/aos_rpc_server_common.h>

//--- created service: includes registering an event handler  --------------------------------------

// it uses a Muenchhausen trick to keep the memory alive while needed but delete when finished
// see when it is allocated and when it is freed
struct lmp_data {
    struct lmp_chan *lmp_chan;
    struct event_closure event_handler;
};

static void spawn_lmp_endpoint_recv_handler(void *arg)
{
    assert(arg);

    struct lmp_data *data = arg;
    struct lmp_chan *lmp_chan = data->lmp_chan;

    errval_t err;
    // re-register if there is no message to be received yet
    if (!lmp_chan_can_recv(lmp_chan)) {
        struct waitset *ws = get_default_waitset();
        err = lmp_chan_register_recv(lmp_chan, ws, MKCLOSURE(spawn_lmp_endpoint_recv_handler, arg));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_register_recv failed.\n");
            free(data);
            return;
        }
    }

    struct lmp_recv_msg msg = LMP_RECV_MSG_INIT;
    struct capref recv_cap;

    err = lmp_chan_recv(lmp_chan, &msg, &recv_cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_recv failed.\n");
        free(data);
        return;
    }

    assert(!capref_is_null(recv_cap));
    // successfully established channel
    lmp_chan->remote_cap = recv_cap;

    // acknowledge channel
    err = lmp_chan_send1(lmp_chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, AOS_RPC_ACK(0));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_send1 failed.\n");
        free(lmp_chan);
        free(data);
        return;
    }

    struct aos_rpc_channel *rpc_chan = malloc(sizeof(*rpc_chan));
    if (!rpc_chan) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "endpoint_recv_handler(): malloc failed for aos_rpc_channel.\n");
        free(lmp_chan);
        free(data);
        return;
    }

    // setup rpc event handler
    struct aos_rpc_server_comm_state* init_handler_state = malloc(sizeof(*init_handler_state));
    if (!init_handler_state) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "endpoint_recv_handler(): malloc failed for aos_rpc_event_handler_state_init.\n");
        free(lmp_chan);
        free(rpc_chan);
        free(data);
        return;
    }

    aos_rpc_channel_lmp_init(rpc_chan, lmp_chan);

    // always have a slot prepared to receive a capability on the server side
    err = lmp_chan_alloc_recv_slot(rpc_chan->lmp_chan);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "lmp_chan_alloc_recv_slot failed.\n");
        free(lmp_chan);
        free(rpc_chan);
        free(data);
        return;
    }

    aos_rpc_server_comm_state_init(init_handler_state, rpc_chan);

    // register rpc event handler
    err = lmp_chan_register_recv(lmp_chan, get_default_waitset(), MKCLOSURE(data->event_handler.handler, init_handler_state));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_register_recv failed.\n");
        free(lmp_chan);
        free(rpc_chan);
        free(init_handler_state);
        free(data);
        return;
    }

    // all OK
    free(data);
}

// note: only the function of the event handler closure is used; data will be replaced
// with new RPC state
errval_t setup_lmp_service(struct capref endpoint_cap, struct event_closure event_handler)
{
    struct lmp_chan* lmp_chan = malloc(sizeof(*lmp_chan));
    if (!lmp_chan) {
        return LIB_ERR_MALLOC_FAIL;
    }

    lmp_chan_init(lmp_chan);

    errval_t err = lmp_chan_accept(lmp_chan, DEFAULT_LMP_BUF_WORDS, NULL_CAP);
    if (err_is_fail(err)) {
        free(lmp_chan);
        return err;
    }

    err = cap_copy(endpoint_cap, lmp_chan->local_cap);
    if (err_is_fail(err)) {
        free(lmp_chan);
        return err;
    }

    err = lmp_chan_alloc_recv_slot(lmp_chan);
    if (err_is_fail(err)) {
        free(lmp_chan);
        return err;
    }

    struct lmp_data *data = malloc(sizeof(*data));
    if (!data) {
        free(lmp_chan);
        return err;
    }

    data->lmp_chan = lmp_chan;
    data->event_handler = event_handler;

    err = lmp_chan_register_recv(lmp_chan, get_default_waitset(), MKCLOSURE(spawn_lmp_endpoint_recv_handler, data));
    if (err_is_fail(err)) {
        free(lmp_chan);
        free(data);
        return err;
    }

    return SYS_ERR_OK;
}


//--- connect (see init and bind) ------------------------------------------------------------------

struct lmp_setup_state {
    struct lmp_chan* lmp_chan;
    bool complete;
    errval_t err;
};

static void lmp_setup_recv_handler(void *arg) {
    assert(arg);

    struct lmp_setup_state* state = arg;
    errval_t err;

    // re-register if there is no message to be received yet
    if (!lmp_chan_can_recv(state->lmp_chan)) {
        struct waitset *ws = get_default_waitset();
        err = lmp_chan_register_recv(state->lmp_chan, ws, MKCLOSURE(lmp_setup_recv_handler, arg));
        if (err_is_fail(err)) {
            state->err = err_push(err, LIB_ERR_CHAN_REGISTER_RECV);
            state->complete = true;
            return;
        }
    }

    struct lmp_recv_msg msg = LMP_RECV_MSG_INIT;
    err = lmp_chan_recv(state->lmp_chan, &msg, NULL);
    if (err_is_fail(err)) {
        state->err = err_push(err, LIB_ERR_LMP_CHAN_RECV);
        state->complete = true;
        return;
    }
    if (!AOS_RPC_ACK_OK(msg.words[0])) {
        state->err = LIB_ERR_LMP_CHAN_BIND;
        state->complete = true;
        return;
    }
    // successfully established channel
    state->err = SYS_ERR_OK;
    state->complete = true;
}


errval_t setup_lmp_client(struct capref remote_cap,
                          enum aos_rpc_interface interface, void *interface_vtable,
                          routing_info_t routing, struct aos_rpc **ret_rpc)
{

    /* allocate lmp channel structure */
    struct lmp_chan* lmp_chan = malloc(sizeof(*lmp_chan));
    if (!lmp_chan) {
        return LIB_ERR_MALLOC_FAIL;
    }
    lmp_chan_init(lmp_chan);

    /* create local endpoint */
    struct capref local_cap;
    errval_t err = endpoint_create(DEFAULT_LMP_BUF_WORDS, &local_cap, &lmp_chan->endpoint);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_ENDPOINT_CREATE);
    }

    /* set remote endpoint to given endpoint */
    lmp_chan->remote_cap = remote_cap;

    /* set receive handler */
    struct lmp_setup_state state = {
        .lmp_chan = lmp_chan,
        .complete = false
    };

    err = lmp_chan_register_recv(lmp_chan, get_default_waitset(), MKCLOSURE(lmp_setup_recv_handler, &state));
    if (err_is_fail(err)) {
        free(lmp_chan);
        return err_push(err, LIB_ERR_CHAN_REGISTER_RECV);
    }

    /* send local ep to init */
    err = lmp_chan_send0(lmp_chan, LMP_SEND_FLAGS_DEFAULT, local_cap);
    if (err_is_fail(err)) {
        free(lmp_chan);
        return err_push(err, LIB_ERR_LMP_CHAN_SEND);
    }

    /* wait for init to acknowledge receiving the endpoint */
    struct waitset* ws = get_default_waitset();
    while (!state.complete) {
        err = event_dispatch(ws);
        if (err_is_fail(err)) {
            free(lmp_chan);
            return err_push(err, LIB_ERR_EVENT_DISPATCH);
        }
    }

    if (err_is_fail(state.err)) {
        free(lmp_chan);
        return state.err;
    }

    /* initialize init RPC client with lmp channel */
    struct aos_rpc_channel *chan = malloc(sizeof(*chan));
    if (!chan) {
        free(lmp_chan);
        return LIB_ERR_MALLOC_FAIL;
    }

    aos_rpc_channel_lmp_init(chan, lmp_chan);

    *ret_rpc = malloc(sizeof(**ret_rpc));
    if (!*ret_rpc) {
        free(lmp_chan);
        free(chan);
        return LIB_ERR_MALLOC_FAIL;
    }
    aos_rpc_init(*ret_rpc, chan, interface, interface_vtable, routing);

    return SYS_ERR_OK;
}
