/**
 * Group C
 *
 * 15.11.2017
 *
 * This file implements the ringbuffer functionality
 * for ump messages.
 */

#include <aos/aos.h>
#include <aos/connect/ump_ringbuffer.h>

#include <aos/caddr.h>
#include <aos/dispatcher_arch.h>
#include "waitset_chan_priv.h"


/** \brief Initializes the ringbuffer with bufferfields on given frame pointer
 *           if initialized as client
 */
errval_t ump_ringbuffer_init(struct ump_ringbuffer *rb, void *frame_ptr, size_t size, bool is_sender, size_t *ret_payload_size) {
    assert(rb);
    assert(frame_ptr);
    assert(ret_payload_size);

    void *aligned_frame_ptr = (void *) ROUND_UP((lvaddr_t)frame_ptr, UMP_RB_CACHE_LINE_SIZE);
    size_t aligned_size = size - ((char *)aligned_frame_ptr - (char *)frame_ptr);

    rb->buf = aligned_frame_ptr;

    rb->length = aligned_size / sizeof(struct ump_ringbuffer_field);

    rb->idx = 0;

    rb->is_sender = is_sender;

    *ret_payload_size = UMP_RB_PAYLOAD_SIZE;

    if (!rb->is_sender) {
        waitset_chanstate_init(&rb->waitset_state, CHANTYPE_UMP_IN);
    }

    return SYS_ERR_OK;
}


//=== interface / implementation of additional functions / structs defined in lmp_endpoint
//    these functions follow the LMP implementations
//    see also documentation there
//    modifications made as needed, but semantics is preserved

/**
 * see detailed explanation on this function in the header file.
 */
bool ump_ringbuffer_poll_disabled(struct waitset_chanstate *ws_chan, dispatcher_handle_t handle)
{
    assert_disabled(ws_chan);

    assert_disabled(offsetof(struct ump_ringbuffer, waitset_state) == 0);
    struct ump_ringbuffer *rb = (struct ump_ringbuffer *)ws_chan;

    // no need to check whether we are registered or pending here, since on polled
    // channels we only poll when registered
    if (ump_ringbuffer_can_recv(rb)) {
        errval_t err = waitset_chan_trigger_disabled(&rb->waitset_state, handle);
        assert_disabled(err_is_ok(err));
        return true;
    }
    return false;
}


errval_t ump_ringbuffer_register(struct ump_ringbuffer *rb, struct waitset *ws,
                                 struct event_closure closure)
{
    assert(rb);
    assert(ws);

    errval_t err = SYS_ERR_OK;

    dispatcher_handle_t handle = disp_disable();

    if (ump_ringbuffer_can_recv(rb)) {
        // trigger immediately
        err = waitset_chan_trigger_closure_disabled(ws, &rb->waitset_state,
                                                    closure, handle);
    }
    else {
        err = waitset_chan_register_polled_disabled(ws, &rb->waitset_state, closure, handle);
    }

    disp_enable(handle);

    return err;
}
