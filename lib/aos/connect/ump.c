/**
 * Provides UMP specific messaging functions.
 * AOS class 2017, group C
 */

#include <aos/connect/ump.h>

#include <aos_rpc_server/aos_rpc_server_common.h>

//--- created service: includes registering an event handler  --------------------------------------
// 2 parts: see handshake during ump_chan_init

errval_t setup_ump_service_part1(struct capref frame_cap, struct ump_service_create_data *data)
{
    assert(data);

    errval_t err = frame_identify(frame_cap, &data->info);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_identify failed for capability\n");
        return err;
    }

    err = paging_map_frame_attr(get_current_paging_state(), &data->ump_chan_buf,
                                   data->info.bytes, frame_cap,
                                   VREGION_FLAGS_READ_WRITE_MPB, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed\n");
        return err;
    }

    memset(data->ump_chan_buf, 0, data->info.bytes);

    return setup_ump_service_part1_from_buffer(data->ump_chan_buf, data->info.bytes, data);
}

errval_t setup_ump_service_part1_from_buffer(void *buf, size_t buf_size,
                                             struct ump_service_create_data *data)
{
    assert(data);

    data->ump_chan_buf = buf;
    data->info.base = (uintptr_t)buf;
    data->info.bytes = buf_size;

    data->ump_chan = malloc(sizeof(struct ump_chan));
    if (!data->ump_chan) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc for ump channel failed\n");
        return LIB_ERR_MALLOC_FAIL;
    }

    data->rpc_chan = malloc(sizeof(struct aos_rpc_channel));
    if (!data->rpc_chan) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed for aos_rpc_channel.\n");
        free(data->ump_chan);
        return LIB_ERR_MALLOC_FAIL;
    }

    // setup rpc event handler
    data->init_handler_state = malloc(sizeof(struct aos_rpc_server_comm_state));
    if (!data->init_handler_state) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "endpoint_recv_handler(): malloc failed for aos_rpc_event_handler_state_init.\n");
        free(data->rpc_chan);
        free(data->ump_chan);
        return LIB_ERR_MALLOC_FAIL;
    }

    return SYS_ERR_OK;
}


errval_t setup_ump_service_part2(struct ump_service_create_data *data, struct event_closure event_handler)
{
    assert(data);

    // makes a blocking handshake
    // note: we do not free here in these error cases anymore
    // if an error happens here, it's a bad situation in any case...
    // BTW: all potentially failing memory allocation has already happened in part 1
    // Thus, risk for failing here is very low.
    errval_t err = ump_chan_init(data->ump_chan, data->ump_chan_buf, data->info.bytes, false);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ump_chan_init failed\n");
        return err;
    }

    aos_rpc_channel_ump_init(data->rpc_chan, data->ump_chan);

    aos_rpc_server_comm_state_init(data->init_handler_state, data->rpc_chan);

    // register rpc event handler
    err = ump_chan_register_recv(data->ump_chan, get_default_waitset(), MKCLOSURE(event_handler.handler, data->init_handler_state));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "endpoint_recv_handler(): lmp_chan_register_recv failed.\n");
        return err;
    }

    return SYS_ERR_OK;
}

//--- connect (see init and bind) ------------------------------------------------------------------

errval_t setup_ump_client(struct capref frame_cap,
                          enum aos_rpc_interface interface, void *interface_vtable,
                          routing_info_t routing, struct aos_rpc **ret_rpc)
{
    struct frame_identity info;
    errval_t err = frame_identify(frame_cap, &info);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_identify failed for frame capability\n");
        return err;
    }

    void *ump_chan_buf;
    err = paging_map_frame_attr(get_current_paging_state(), &ump_chan_buf,
                                   info.bytes, frame_cap,
                                   VREGION_FLAGS_READ_WRITE_MPB, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed\n");
        return err;
    }

    return setup_ump_client_from_buffer(ump_chan_buf, info.bytes,
                                        interface, interface_vtable,
                                        routing, ret_rpc);
}


errval_t setup_ump_client_from_buffer(void *buf, size_t buf_size,
                                      enum aos_rpc_interface interface, void *interface_vtable,
                                      routing_info_t routing, struct aos_rpc **ret_rpc)
{
    assert(buf);
    assert(interface_vtable);
    assert(ret_rpc);

    errval_t err = SYS_ERR_OK;

    struct ump_chan* ump_chan = malloc(sizeof(*ump_chan));
    if (!ump_chan) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc for ump channel failed\n");
        return LIB_ERR_MALLOC_FAIL;
    }

    // note: makes a handshake with the server
    err = ump_chan_init(ump_chan, buf, buf_size, true);
    if (err_is_fail(err)) {
        free(ump_chan);
        DEBUG_ERR(err, "ump_chan_init failed\n");
        return err;
    }

    // Contrary to LMP, no setup handler is needed this is done internally in UMP

    struct aos_rpc_channel *chan = malloc(sizeof(*chan));
    if (!chan) {
        free(ump_chan);
        return LIB_ERR_MALLOC_FAIL;
    }

    aos_rpc_channel_ump_init(chan, ump_chan);

    *ret_rpc = malloc(sizeof(**ret_rpc));
    if (!*ret_rpc) {
        free(ump_chan);
        free(chan);
        return LIB_ERR_MALLOC_FAIL;
    }
    aos_rpc_init(*ret_rpc, chan, interface, interface_vtable, routing);

    return SYS_ERR_OK;
}
