/**
 * \file
 * \brief Bidirectional LMP channel implementation
 */

/*
 * Copyright (c) 2009, 2010, 2011, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/aos.h>
#include <aos/connect/flmp_chan.h>
#include <aos/dispatcher_arch.h>
#include <aos/caddr.h>
#include <aos/waitset_chan.h>
#include "waitset_chan_priv.h"

errval_t flmp_chan_connect(struct flmp_chan *flc, void *frame_ptr, size_t size, bool as_client)
{
    assert(flc);
    assert(frame_ptr);

    size_t half = size >> 1;
    void *first = frame_ptr;
    void *second = (char *)frame_ptr + half;

    void *send, *recv;
    if (as_client) {
        send = first;
        recv = second;
    }
    else {
        recv = first;
        send = second;
    }

    size_t check = 0;
    errval_t err = ump_ringbuffer_init(&flc->send_rb, send, half, true, &check);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_UMP_CHAN_INIT);
    }
    err = ump_ringbuffer_init(&flc->recv_rb, recv, half, false, &flc->max_payload_size);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_UMP_CHAN_INIT);
    }
    assert(check == flc->max_payload_size);

    return SYS_ERR_OK;
}
