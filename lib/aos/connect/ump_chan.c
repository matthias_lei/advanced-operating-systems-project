/**
 * UMP implementation
 * see detailed information in the header file
 *
 * Group C
 */


#include <aos/connect/ump_chan.h>

#define UMP_CHAN_INIT_REQUEST 31415
#define UMP_CHAN_INIT_REPLY 42

/**
 * technical detail: the provided frame is split into 2 halves. The first part is used for the function
 * that is used first, and the second for the second part, accordingly.
 *
 *            as_client    as_server
 *  first     send_rb      recv_rb
 *  second    recv_rb      send_rb
 *
 * Thus, the allocation is very clear.
 *
 * Crucial note: the entire frame must have been cleared (memset to 0) by the domain that has
 * initially acquired the frame from RAM manager *before* the address and size of this frame
 * are even sent to the other party of the UMP channel and before calling this init function.
 * This assures that no spurious dirty flags can be found within the used buffer.
 *
 * Note: The buffers and sizes are forwarded for use to the ringbuffer implementation as is.
 * The ringbuffer implementation checks itself for proper alignment and size requirements to
 * work well with machine dependent cache line size and chosen message base size.
 */
errval_t ump_chan_init(struct ump_chan *uc, void *frame_ptr, size_t size, bool as_client)
{
    assert(uc);
    assert(frame_ptr);

    uc->connstate = UMP_DISCONNECTED;

    size_t half = size >> 1;
    void *first = frame_ptr;
    void *second = (char *)frame_ptr + half;

    void *send, *recv;
    if (as_client) {
        send = first;
        recv = second;
    }
    else {
        recv = first;
        send = second;
    }

    size_t check = 0;
    errval_t err = ump_ringbuffer_init(&uc->send_rb, send, half, true, &check);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_UMP_CHAN_INIT);
    }
    err = ump_ringbuffer_init(&uc->recv_rb, recv, half, false, &uc->max_payload_size);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_UMP_CHAN_INIT);
    }
    assert(check == uc->max_payload_size);

    uc->connstate = UMP_BIND_WAIT;

/*
    // small handshake
    uintptr_t value;
    if (as_client) {
        value = UMP_CHAN_INIT_REQUEST;
        err = ump_chan_send(uc, &value, sizeof(uintptr_t));
        if (err_is_fail(err)) {
            goto handshake_error;
        }
        while(!ump_chan_can_recv(uc));
        err = ump_chan_recv(uc, &value, sizeof(uintptr_t));
        if (err_is_fail(err)) {
            goto handshake_error;
        }
        assert(value == UMP_CHAN_INIT_REPLY);
    }
    else {
        while(!ump_chan_can_recv(uc));
        err = ump_chan_recv(uc, &value, sizeof(uintptr_t));
        if (err_is_fail(err)) {
            goto handshake_error;
        }
        assert(value == UMP_CHAN_INIT_REQUEST);
        value = UMP_CHAN_INIT_REPLY;
        err = ump_chan_send(uc, &value, sizeof(uintptr_t));
        if (err_is_fail(err)) {
            goto handshake_error;
        }
    }
*/
    uc->connstate = UMP_CONNECTED;
    return SYS_ERR_OK;
/*
handshake_error:
    uc->connstate = UMP_DISCONNECTED;
    return err_push(err, LIB_ERR_UMP_CHAN_INIT);
*/
}


void ump_chan_destroy(struct ump_chan *uc)
{
    assert(uc);

    // no error check here
    ump_chan_deregister_recv(uc);

    uc->connstate = UMP_DISCONNECTED;
}
