/**
 * For detailed description of interface see header file
 *
 * Group C
 * version 2017-11-25
 */


#include <aos/rpc_shared/aos_rpc_shared_common.h>

#include <alloca.h>

//--- vtables --------------------------------------------------------------------------------------

/**
 *--- busy waiting ---
 * LMP is ready at the first iteration (as expected) -> no need to register the receive handler
 *     10 is just for some small safety bound
 *     calling the receive handler directly without registration also decreases the RTT for
 *     short messages (measure for latency) a tiny bit from approx. 153.1 ± 0.4 us to 148.0 ± 0.6 us
 *     (clearly not relevant; but 95% CI [152.6, 153.6] us vs [147.2, 148.8] us not overlapping
 *     already with quite small n=5). Basically, latency is about 75 us for LMP.
 *     Throughput for large messages remains identical with approx. 295 KiB/s
 *     (512 KiB block transmitted)
 *
 * UMP itself takes dozens to several thousands of iterations. Thus, 10000 was used as initial limit
 *     This impressively reduces the RTT for short messages from 40 ms to 7.6 us,
 *     which yields a current latency for UMP of about 3.8 us.
 *     Max. throughput for large messages remains the same with 32.1 ± 1.1 MiB/s
 *     (8 MiB block transmitted). Note: this 8 MiB block is much bigger than the ringbuffer
 *     size used for transmission (currently 2 KiB / ringbuffer; may become smaller in the
 *     future when we are testing influence of this size in performance; see small L1 cache size)
 *
 *     Note: to have a fast cross-core printf() output, this busy waiting must cover the entire time
 *     that Hermes is busy processing the sent string. With the current implementation with
 *     buffer and also syscall, this takes up to several 100'000 iterations.
 *     Thus, the limit is quite arbitrarily set to 1'000'000 at the moment.
 *     printf() on core 1 is now as fast as on core 0 (measured).
 *     The constant may change as soon as real serial driver via UART is implemented.
 *     I keep an eye on it. pisch
 *
 *     Additional note: this limit will also depend on other latency sensitive server activities
 *     in future additions (e.g. file system, network, DMA drivers,...).
 *     Let's sync on this while working on the individual projects.
 *
 * Technical note: the benchmarking system takes about 675 cycles (approx. 2.2 us) for the time
 * measurement. This time is not included in the numbers above, of course. Thus, also the small
 * numbers are valid for comparison.
 */
#define LMP_BUSY_WAIT_ITERATIONS 10
// tuning parameter:
//#define UMP_BUSY_WAIT_ITERATIONS 100000
#define UMP_BUSY_WAIT_ITERATIONS 1000000
//#define UMP_BUSY_WAIT_ITERATIONS 0xffffffff

//--- LMP vtable
static errval_t aos_rpc_lmp_register_recv(struct aos_rpc_channel *chan, struct waitset *ws, struct event_closure closure) {
    assert(chan->driver == AOS_RPC_CHAN_DRIVER_LMP);
    return lmp_chan_register_recv(chan->lmp_chan, ws, closure);
}

static void aos_rpc_lmp_send_handler(void *args);

static void aos_rpc_lmp_recv_handler(void *args);

static bool aos_rpc_lmp_can_recv(struct aos_rpc_channel *chan) {
    assert(chan->driver == AOS_RPC_CHAN_DRIVER_LMP);
    return lmp_chan_can_recv(chan->lmp_chan);
}

static size_t aos_rpc_lmp_get_busy_wait_iterations(void) {
    return LMP_BUSY_WAIT_ITERATIONS;
}

static void aos_rpc_lmp_install_recv_cap(struct aos_rpc_channel *chan, struct capref cap) {
    lmp_chan_set_recv_slot(chan->lmp_chan, cap);
}

struct aos_rpc_channel_driver_vtable aos_rpc_channel_lmp_driver_vtable = {
    .register_recv = aos_rpc_lmp_register_recv,
    .send_handler = aos_rpc_lmp_send_handler,
    .recv_handler = aos_rpc_lmp_recv_handler,
    .can_recv = aos_rpc_lmp_can_recv,
    .get_busy_wait_iterations = aos_rpc_lmp_get_busy_wait_iterations,
    .install_recv_slot = aos_rpc_lmp_install_recv_cap
};


//--- FLMP vtable
static errval_t aos_rpc_flmp_register_recv(struct aos_rpc_channel *chan, struct waitset *ws, struct event_closure closure) {
    assert(chan->driver == AOS_RPC_CHAN_DRIVER_FLMP);
    return flmp_chan_register_recv(chan->flmp_chan, ws, closure);
}

static void aos_rpc_flmp_send_handler(void *args);

static void aos_rpc_flmp_recv_handler(void *args);

static bool aos_rpc_flmp_can_recv(struct aos_rpc_channel *chan) {
    assert(chan->driver == AOS_RPC_CHAN_DRIVER_FLMP);
    return flmp_chan_can_recv_short(chan->flmp_chan);
}

static size_t aos_rpc_flmp_get_busy_wait_iterations(void) {
    return LMP_BUSY_WAIT_ITERATIONS;
}

static void aos_rpc_flmp_install_recv_cap(struct aos_rpc_channel *chan, struct capref cap) {
    lmp_chan_set_recv_slot(chan->lmp_chan, cap);
}

struct aos_rpc_channel_driver_vtable aos_rpc_channel_flmp_driver_vtable = {
    .register_recv = aos_rpc_flmp_register_recv,
    .send_handler = aos_rpc_flmp_send_handler,
    .recv_handler = aos_rpc_flmp_recv_handler,
    .can_recv = aos_rpc_flmp_can_recv,
    .get_busy_wait_iterations = aos_rpc_flmp_get_busy_wait_iterations,
    .install_recv_slot = aos_rpc_flmp_install_recv_cap
};


//--- UMP vtable
static errval_t aos_rpc_ump_register_recv(struct aos_rpc_channel *chan, struct waitset *ws, struct event_closure closure) {
    assert(chan->driver == AOS_RPC_CHAN_DRIVER_UMP);
    return ump_chan_register_recv(chan->ump_chan, ws, closure);
}

static void aos_rpc_ump_send_handler(void *args);

static void aos_rpc_ump_recv_handler(void *args);

static bool aos_rpc_ump_can_recv(struct aos_rpc_channel *chan) {
    assert(chan->driver == AOS_RPC_CHAN_DRIVER_UMP);
    return ump_chan_can_recv(chan->ump_chan);
}

static size_t aos_rpc_ump_get_busy_wait_iterations(void) {
    return UMP_BUSY_WAIT_ITERATIONS;
}

static void aos_rpc_ump_install_recv_cap(struct aos_rpc_channel *chan, struct capref cap) {
    // we cannot receive capabilities over UMP directly
    assert(false);
}

struct aos_rpc_channel_driver_vtable aos_rpc_channel_ump_driver_vtable = {
    .register_recv = aos_rpc_ump_register_recv,
    .send_handler = aos_rpc_ump_send_handler,
    .recv_handler = aos_rpc_ump_recv_handler,
    .can_recv = aos_rpc_ump_can_recv,
    .get_busy_wait_iterations = aos_rpc_ump_get_busy_wait_iterations,
    .install_recv_slot = aos_rpc_ump_install_recv_cap
};


//--- actual handlers ------------------------------------------------------------

/** \brief Sends a fragment and reregisters
 *         the handler if it was not the last fragment.
 */
static void aos_rpc_lmp_send_handler(void *args) {
    assert(args);

    struct aos_rpc_send_fragment_state *fs = (struct aos_rpc_send_fragment_state*) args;

    size_t new_offset = 0;

    errval_t err = SYS_ERR_OK;

    do {
        // special case for first fragment which includes head and message length
        if (fs->offset == 0) {
            uintptr_t payload[AOS_RPC_PAYLOAD_SIZE];
            memset(payload, 0, sizeof(uintptr_t)*AOS_RPC_PAYLOAD_SIZE);

            // copy the message into the payload
            new_offset = MIN(sizeof(uintptr_t)*AOS_RPC_PAYLOAD_SIZE, fs->send_env->buflen);
            memcpy((char *)payload , (char *)fs->send_env->buf, new_offset);

            err = lmp_chan_send(fs->chan->lmp_chan, LMP_SEND_FLAGS_DEFAULT, fs->send_env->cap, 9,
                                fs->send_env->header, fs->send_env->buflen, payload[0], payload[1], payload[2],
                                payload[3], payload[4], payload[5], payload[6]);
            if (err_is_fail(err)) {
                if(lmp_err_is_transient(err)) {
                    goto reregister;
                }
                DEBUG_ERR(err, "lmp_chan_send in aos_rpc_generic_send failed\n");
                goto continuation;
            }
        }
        // payload exceeds lmp message size
        else {
            uintptr_t payload[LMP_MSG_LENGTH];
            memset(payload, 0, sizeof(uintptr_t)*LMP_MSG_LENGTH);

            // copy the message into the payload
            new_offset = MIN(fs->offset + sizeof(uintptr_t)*LMP_MSG_LENGTH, fs->send_env->buflen);
            memcpy((char *)payload , (char *)fs->send_env->buf + fs->offset, new_offset - fs->offset);

            err = lmp_chan_send(fs->chan->lmp_chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, 9,
                                payload[0], payload[1], payload[2], payload[3], payload[4],
                                payload[5], payload[6], payload[7], payload[8]);
            if (err_is_fail(err)) {
                // a posteriori check of transient error due to the fact that lmp_chan_can_send does not exist
                if (lmp_err_is_transient(err)) {
                    goto reregister;
                }
                DEBUG_ERR(err, "lmp_chan_send failed\n");
                goto continuation;
            }
        }

        fs->offset = new_offset;

    } while (new_offset < fs->send_env->buflen);

    assert(new_offset == fs->send_env->buflen);

continuation:
    fs->err = err;
    fs->completed = true;
    if (fs->cont.handler) {
        fs->cont.handler(fs->cont.arg);
    }
    return;

reregister:

    err = lmp_chan_register_send(fs->chan->lmp_chan, get_default_waitset(), MKCLOSURE(aos_rpc_lmp_send_handler, fs));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "message fragment could not be sent\n");
        fs->err = err;
        fs->completed = true;
        return;
    }
    return;
}


/** \brief Receives a fragment from the channel, extracts the payload and
 *         reregisters the handler if it was not the last fragment.
 *         NOTE: this function is never parsing the header
 */
static void aos_rpc_lmp_recv_handler(void *args) {
    assert(args);

    struct aos_rpc_recv_fragment_state *fs = (struct aos_rpc_recv_fragment_state*) args;

    size_t new_offset = 0;

    errval_t err = SYS_ERR_OK;

    while (lmp_chan_can_recv(fs->chan->lmp_chan)) {

        //special case for first fragment which includes head and message length
        if (fs->offset == 0) {
            struct lmp_recv_msg recv_msg = (struct lmp_recv_msg) LMP_RECV_MSG_INIT;
            err = lmp_chan_recv(fs->chan->lmp_chan, &recv_msg, &fs->recv_env->cap);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "lmp_chan_recv failed");
                fs->recv_env->cap = NULL_CAP;
                goto continuation;
            }

            fs->recv_env->header = recv_msg.words[AOS_RPC_HEADER_WORD];
            size_t message_size = recv_msg.words[AOS_RPC_SIZE_WORD];

            // permission to malloc is granted if recv_env does not pass buffer
            // and sets the buffer length to AOS_RPC_EXPECT_PAYLOAD
            // possible pagefaults go through separate memory channel
            if (!fs->recv_env->buf) {
                if (fs->recv_env->buflen == AOS_RPC_EXPECT_PAYLOAD) {
                    if (fs->is_server && (message_size <= DEFAULT_RPC_BUFFER_SIZE)) {
                        fs->recv_env->buf = fs->server_default_recv_buffer;
                        fs->recv_env->buflen = DEFAULT_RPC_BUFFER_SIZE;
                    }
                    else {
                        fs->recv_env->buf = malloc(message_size);
                        if (!fs->recv_env->buf) {
                            DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed\n");
                            fs->recv_env->buflen = 0;
                            err = LIB_ERR_MALLOC_FAIL;
                            goto continuation;
                        }
                        fs->recv_env->buflen = message_size;
                    }
                }
                else {
                    //we do not want to have a buffer
                    fs->recv_env->buflen = 0;
                }
            }

            if (fs->recv_env->buflen < message_size) {
                DEBUG_ERR(LIB_ERR_LMP_BUFLEN_INVALID, "lmp channel received unexpected message size\n");
                err = LIB_ERR_LMP_BUFLEN_INVALID;
                goto continuation;
            }

            // set buflen equal to message size, in case the buffer
            // is larger than the message
            fs->recv_env->buflen = message_size;

            new_offset = MIN(fs->recv_env->buflen, sizeof(uintptr_t)*AOS_RPC_PAYLOAD_SIZE);

            if (fs->recv_env->buf && fs->recv_env->buflen > 0) {
                memcpy(fs->recv_env->buf, &recv_msg.words[AOS_RPC_PAYLOAD_WORD], new_offset);
            }
        }
        // payload exceeds lmp message size
        else {
            //check if we are not past the last fragment
            assert(fs->offset < fs->recv_env->buflen);

            struct lmp_recv_msg recv_msg = (struct lmp_recv_msg) LMP_RECV_MSG_INIT;

            err = lmp_chan_recv(fs->chan->lmp_chan, &recv_msg, NULL);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "lmp_chan_recv failed in aos_rpc_recv_fragment_handler\n");
                goto continuation;
            }

            new_offset = MIN(fs->offset + sizeof(uintptr_t)*LMP_MSG_LENGTH, fs->recv_env->buflen);

            memcpy((char *)fs->recv_env->buf + fs->offset, recv_msg.buf.words, new_offset - fs->offset);

        }

        fs->offset = new_offset;

        // if this was the last fragment return
        if (fs->offset >= fs->recv_env->buflen) {
            assert(fs->offset == fs->recv_env->buflen);
            goto continuation;
        }
    }

    //reregister if message was transient
    err = lmp_chan_register_recv(fs->chan->lmp_chan, get_default_waitset(), MKCLOSURE(aos_rpc_lmp_recv_handler, args));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failed to reregister aos_rpc_lmp_recv_handler upon transient incoming fragment\n");
        goto continuation;
    }
    return;

continuation:
    fs->err = err;
    fs->completed = true;
    if (fs->cont.handler) {
        fs->cont.handler(fs->cont.arg);
    }
    return;

}

#define AOS_RPC_FLMP_INTERNAL_NOTIFICATION_HEADER AOS_RPC_FG_FID_HELPER(AOS_RPC_RESERVED_FG, 0)

static void aos_rpc_flmp_send_handler(void *args) {
    assert(args);

    struct aos_rpc_send_fragment_state *fs = (struct aos_rpc_send_fragment_state*) args;

    errval_t err = SYS_ERR_OK;
    size_t new_offset = fs->offset;

    if (fs->offset == 0 && fs->send_env->buflen <= sizeof(uintptr_t)*AOS_RPC_PAYLOAD_SIZE) {
        uintptr_t payload[LMP_MSG_LENGTH];
        memset(payload, 0, sizeof(payload));

        payload[AOS_RPC_HEADER_WORD] = fs->send_env->header;
        payload[AOS_RPC_SIZE_WORD] = fs->send_env->buflen;

        memcpy((char *)&payload[AOS_RPC_PAYLOAD_WORD], (char *)fs->send_env->buf, fs->send_env->buflen);

        err = flmp_chan_send_short(fs->chan->flmp_chan, LMP_SEND_FLAGS_DEFAULT, fs->send_env->cap,
                                   payload, sizeof(payload));
        if (err_is_fail(err)) {
            if(flmp_err_is_transient(err)) {
                goto reregister;
            }
            DEBUG_ERR(err, "flmp_chan_send_short in aos_rpc_generic_send failed\n");
        }
        goto continuation;
    }

    size_t payload_size = fs->chan->flmp_chan->max_payload_size;

    while (new_offset < fs->send_env->buflen && flmp_chan_can_send_long(fs->chan->flmp_chan)) {
        size_t size = MIN(payload_size, fs->send_env->buflen - new_offset);

        err = flmp_chan_send_long(fs->chan->flmp_chan, (char *)fs->send_env->buf + new_offset, size);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "flmp_chan_send_long failed\n");
            goto continuation;
        }
        new_offset += size;
    }

    // Special case for first fragment which includes head and message length.
    // If sending the first fragment fails due to a transient error, fs->err is
    // set to this error, such that when aos_rpc_flmp_send_handler is called
    // again after re-registering, we try to resend the first fragment again
    if (fs->offset == 0 || flmp_err_is_transient(fs->err)) {
        fs->err = SYS_ERR_OK;

        uintptr_t payload[2];

        payload[AOS_RPC_HEADER_WORD] = fs->send_env->header;
        payload[AOS_RPC_SIZE_WORD] = fs->send_env->buflen;

        err = flmp_chan_send_short(fs->chan->flmp_chan, LMP_SEND_FLAGS_DEFAULT, fs->send_env->cap,
                                   payload, sizeof(payload));
        if (err_is_fail(err)) {
            if(flmp_err_is_transient(err)) {
                fs->err = err;
                goto reregister;
            }
            DEBUG_ERR(err, "flmp_chan_send_short in aos_rpc_generic_send failed\n");
            goto continuation;
        }
    }
    else {
        // send a notification message to indicate new data is available for receiving in the ringbuffer
        uintptr_t header = AOS_RPC_FLMP_INTERNAL_NOTIFICATION_HEADER;
        err = flmp_chan_send_short(fs->chan->flmp_chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, &header, sizeof(header));
        if (err_is_fail(err)) {
            if (flmp_err_is_transient(err)) {
                goto reregister;
            }
            DEBUG_ERR(err, "flmp_chan_send_short failed %d.\n", err_no(err));
            goto continuation;
        }
    }
    if (new_offset < fs->send_env->buflen) {
        goto reregister;
    }

    assert(new_offset == fs->send_env->buflen);

continuation:
    fs->err = err;
    fs->completed = true;
    if (fs->cont.handler) {
        fs->cont.handler(fs->cont.arg);
    }
    return;

reregister:

    fs->offset = new_offset;

    err = flmp_chan_register_send(fs->chan->flmp_chan, get_default_waitset(), MKCLOSURE(aos_rpc_flmp_send_handler, fs));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "message fragment could not be sent\n");
        fs->err = err;
        fs->completed = true;
        return;
    }
}

static void aos_rpc_flmp_recv_handler(void *args) {
    assert(args);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_recv_fragment_state *fs = (struct aos_rpc_recv_fragment_state*) args;
    // immediately reregister if there is no lmp message available yet
    if (!flmp_chan_can_recv_short(fs->chan->flmp_chan)) {
        goto reregister;
    }

    size_t payload_size = fs->chan->flmp_chan->max_payload_size;
    //special case for first fragment which includes head and message length
    if (fs->offset == 0) {
        struct lmp_recv_msg recv_msg = (struct lmp_recv_msg) LMP_RECV_MSG_INIT;
        err = flmp_chan_recv_short(fs->chan->flmp_chan, &recv_msg, &fs->recv_env->cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "lmp_chan_recv failed");
            fs->recv_env->cap = NULL_CAP;
            goto continuation;
        }

        fs->recv_env->header = recv_msg.words[AOS_RPC_HEADER_WORD];
        size_t message_size = recv_msg.words[AOS_RPC_SIZE_WORD];

        // permission to malloc is granted if recv_env does not pass buffer
        // and sets the buffer length to AOS_RPC_EXPECT_PAYLOAD
        // possible pagefaults go through separate memory channel
        if (!fs->recv_env->buf) {

            if (fs->recv_env->buflen == AOS_RPC_EXPECT_PAYLOAD) {
                if (fs->is_server && (message_size <= DEFAULT_RPC_BUFFER_SIZE)) {
                    fs->recv_env->buf = fs->server_default_recv_buffer;
                    fs->recv_env->buflen = DEFAULT_RPC_BUFFER_SIZE;
                }
                else {
                    fs->recv_env->buf = malloc(message_size);
                    if (!fs->recv_env->buf) {
                        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed\n");
                        fs->recv_env->buflen = 0;
                        err = LIB_ERR_MALLOC_FAIL;
                        goto continuation;
                    }
                    fs->recv_env->buflen = message_size;
                }
            }
            else {
                //we do not want to have a buffer
                fs->recv_env->buflen = 0;
            }
        }

        if (fs->recv_env->buflen < message_size) {
            DEBUG_ERR(LIB_ERR_LMP_BUFLEN_INVALID, "lmp channel received unexpected message size\n");
            err = LIB_ERR_LMP_BUFLEN_INVALID;
            goto continuation;
        }

        // set buflen equal to message size, in case the buffer
        // is larger than the message
        fs->recv_env->buflen = message_size;

        if (fs->recv_env->buflen <= sizeof(uintptr_t)*AOS_RPC_PAYLOAD_SIZE) {
            memcpy(fs->recv_env->buf, &recv_msg.words[AOS_RPC_PAYLOAD_WORD], fs->recv_env->buflen);
            err = SYS_ERR_OK;
            goto continuation;
        }
    }
    else {
        // receive notification indicating that new data is available for receiving in the ringbuffer
        struct lmp_recv_msg recv_msg = (struct lmp_recv_msg) LMP_RECV_MSG_INIT;
        err = flmp_chan_recv_short(fs->chan->flmp_chan, &recv_msg, NULL);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "flmp_chan_recv_short failed.\n");
            goto continuation;
        }
        // we should only ever receive notification messages from
        // aos_rpc_flmp_send_handler
        assert(recv_msg.words[AOS_RPC_HEADER_WORD] == AOS_RPC_FLMP_INTERNAL_NOTIFICATION_HEADER);
    }

    while (fs->offset < fs->recv_env->buflen && flmp_chan_can_recv_long(fs->chan->flmp_chan)) {
        //check if we are not past the last fragment
        assert(fs->offset < fs->recv_env->buflen);

        size_t new_offset = MIN(fs->offset + payload_size, fs->recv_env->buflen);

        err = flmp_chan_recv_long(fs->chan->flmp_chan, (char *)fs->recv_env->buf + fs->offset, new_offset - fs->offset);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "lmp_chan_recv failed in aos_rpc_recv_fragment_handler\n");
            goto continuation;
        }

        fs->offset = new_offset;
    }

    // if this was the last fragment return
    if (fs->offset >= fs->recv_env->buflen) {
        assert(fs->offset == fs->recv_env->buflen);
        goto continuation;
    }

reregister:
    //reregister if message was transient
    err = flmp_chan_register_recv(fs->chan->flmp_chan, get_default_waitset(), MKCLOSURE(aos_rpc_flmp_recv_handler, args));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failed to reregister aos_rpc_flmp_recv_handler upon transient incoming fragment\n");
        goto continuation;
    }
    return;

continuation:
    fs->err = err;
    fs->completed = true;
    if (fs->cont.handler) {
        fs->cont.handler(fs->cont.arg);
    }
    return;
}


static void aos_rpc_ump_send_handler(void *args) {
    assert(args);

    struct aos_rpc_send_fragment_state *fs = args;
    size_t payload_size = fs->chan->ump_chan->max_payload_size;

    uintptr_t meta[2] = { fs->send_env->header, fs->send_env->buflen };

    while (!ump_chan_can_send(fs->chan->ump_chan));

    errval_t err = ump_chan_send(fs->chan->ump_chan, meta, sizeof(meta));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ump_chan_send failed.\n");
        goto continuation;
    }

    const char *buf = fs->send_env->buf;
    const char *bufend = ((const char*)fs->send_env->buf) + fs->send_env->buflen;

    while (buf < bufend) {
        while (!ump_chan_can_send(fs->chan->ump_chan));

        size_t fragment_size = MIN(payload_size, bufend - buf);
        err = ump_chan_send(fs->chan->ump_chan, buf, fragment_size);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "ump_chan_send failed.\n");
            goto continuation;
        }
        buf += fragment_size;
    }

    assert(buf == bufend);
    err = SYS_ERR_OK;

continuation:
    fs->err = err;
    fs->completed = true;
    if (fs->cont.handler) {
        fs->cont.handler(fs->cont.arg);
    }
    return;
}

static void aos_rpc_ump_recv_handler(void *args) {
    assert(args);

    struct aos_rpc_recv_fragment_state *fs = args;
    size_t payload_size = fs->chan->ump_chan->max_payload_size;

    uintptr_t meta[2];

    errval_t err = SYS_ERR_OK;

    if (!ump_chan_can_recv(fs->chan->ump_chan)) {
        goto reregister;
    }

    err = ump_chan_recv(fs->chan->ump_chan, meta, 8);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "ump_chan_recv failed.\n");
        goto continuation;
    }

    fs->recv_env->header = meta[0];
    size_t message_size = meta[1];

    if (!fs->recv_env->buf) {
        if (fs->recv_env->buflen == AOS_RPC_EXPECT_PAYLOAD) {
            if (fs->is_server && (message_size <= DEFAULT_RPC_BUFFER_SIZE)) {
                fs->recv_env->buf = fs->server_default_recv_buffer;
                fs->recv_env->buflen = DEFAULT_RPC_BUFFER_SIZE;
            }
            else {
                fs->recv_env->buf = malloc(message_size);
                if (!fs->recv_env->buf) {
                    DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed\n");
                    fs->recv_env->buflen = 0;
                    err = LIB_ERR_MALLOC_FAIL;
                    goto continuation;
                }
                fs->recv_env->buflen = message_size;
            }
        }
        else {
            //we do not want to have a buffer
            fs->recv_env->buflen = 0;
        }
    }

    if (fs->recv_env->buflen < message_size) {
        DEBUG_ERR(LIB_ERR_UMP_BUFSIZE_INVALID, "ump channel received unexpected message size: available %u B, needed %u B\n",
                  fs->recv_env->buflen, message_size);
        err = LIB_ERR_UMP_BUFSIZE_INVALID;
        goto continuation;
    }

    // set buflen equal to message size, in case the buffer
    // is larger than the message
    fs->recv_env->buflen = message_size;

    char *buf = fs->recv_env->buf;
    char *bufend = ((char*)fs->recv_env->buf) + fs->recv_env->buflen;

    while (buf < bufend) {
        while (!ump_chan_can_recv(fs->chan->ump_chan));

        size_t fragment_size = MIN(payload_size, bufend - buf);
        err = ump_chan_recv(fs->chan->ump_chan, buf, fragment_size);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "ump_chan_recv failed.\n");
            goto continuation;
        }
        buf += fragment_size;
    }

    assert(buf == bufend);
    err = SYS_ERR_OK;

continuation:
    fs->err = err;
    fs->completed = true;
    if (fs->cont.handler) {
        fs->cont.handler(fs->cont.arg);
    }
    return;

reregister:
    //reregister if message was transient
    err = ump_chan_register_recv(fs->chan->ump_chan, get_default_waitset(), MKCLOSURE(aos_rpc_ump_recv_handler, args));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failed to reregister aos_rpc_ump_recv_handler upon transient incoming fragment\n");
        goto continuation;
    }
    return;

}


//--- Public interface -----------------------------------------------------------------------------

void aos_rpc_channel_lmp_init(struct aos_rpc_channel *chan, struct lmp_chan *lmp_chan) {
    assert(chan);
    assert(lmp_chan);

    chan->lmp_chan = lmp_chan;
    chan->driver = AOS_RPC_CHAN_DRIVER_LMP;
    chan->vtable = &aos_rpc_channel_lmp_driver_vtable;
    aos_rpc_channel_queue_init(&chan->queue);
    thread_mutex_init(&chan->mutex);
}

void aos_rpc_channel_flmp_init(struct aos_rpc_channel *chan, struct flmp_chan *flmp_chan) {
    assert(chan);
    assert(flmp_chan);

    chan->flmp_chan = flmp_chan;
    chan->driver = AOS_RPC_CHAN_DRIVER_FLMP;
    chan->vtable = &aos_rpc_channel_flmp_driver_vtable;
    aos_rpc_channel_queue_init(&chan->queue);
    thread_mutex_init(&chan->mutex);
}

void aos_rpc_channel_ump_init(struct aos_rpc_channel *chan, struct ump_chan *ump_chan) {
    assert(chan);
    assert(ump_chan);

    chan->ump_chan = ump_chan;
    chan->driver = AOS_RPC_CHAN_DRIVER_UMP;
    chan->vtable = &aos_rpc_channel_ump_driver_vtable;
    aos_rpc_channel_queue_init(&chan->queue);
    thread_mutex_init(&chan->mutex);
}

/**
 * \brief Generic interface for sending messages.
 *        This function can handle arbitrary size messages and will fragment
 *        a message buffer into smaller messages if necessary.
 */
errval_t aos_rpc_generic_send(struct aos_rpc_channel *chan, const struct aos_rpc_send_msg_env *send_env) {
    assert(chan);
    assert(send_env);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_send_fragment_state fs = {
        .completed = false,
        .err = SYS_ERR_OK,
        .offset = 0,
        .chan = chan,
        .send_env = send_env,
        .cont = NOP_CLOSURE
    };

    chan->vtable->send_handler(&fs);

    // block until transmission is completed
    // note: LMP registers a send handler if the message could not be sent entirely
    //       UMP has finished sending after send_handler() has finished; thus fs.completed is true
    struct waitset *default_ws = get_default_waitset();
    while (!fs.completed) {
        err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            // we want to continue on error here, so just log
            DEBUG_ERR(err, "error in dispatcher\n");
        }
    }

    return fs.err;
}


/** \brief Generic interface for recv messages.
 *         This function can handle arbitrary size messages therefore will also receive
 *         fragments of messages which are assembled back into the original message
 *
 * this helper function provides an abstraction level for the handlers below
 * in case other message primitives are used instead of lmp.
 * note: the is_server flag allows special handling of short messages without need for malloc on servers
 */
errval_t aos_rpc_generic_recv(struct aos_rpc_channel *chan, struct aos_rpc_recv_msg_env *recv_env,
                              bool is_server, void *server_default_recv_buffer) {
    assert(chan);
    assert(recv_env);

    struct aos_rpc_recv_fragment_state fs = {
        .completed = false,
        .err = SYS_ERR_OK,
        .offset = 0,
        .chan = chan,
        .recv_env = recv_env,
        .is_server = is_server,
        .server_default_recv_buffer = server_default_recv_buffer,
        .cont = NOP_CLOSURE
    };

    chan->vtable->recv_handler(&fs);

    // block until transmission is completed
    struct waitset *default_ws = get_default_waitset();
    while (!fs.completed) {
        errval_t err = event_dispatch(default_ws);
        if (err_is_fail(err)) {
            // we want to continue on error here, so just log
            DEBUG_ERR(err, "error in dispatcher\n");
        }
    }

    return fs.err;
}

/**
 * NOTE: the type of the capability refers to the type if it is transferred by UMP and
 * needs re-routing. No limitations re caps when transferred by LMP/FLMP.
 */
void aos_rpc_comm_context_client_init(struct aos_rpc_comm_context *context,
                                      const void *send_buf, size_t send_buflen, enum aos_rpc_intermon_capability_type send_cap_type,
                                      void *recv_buf, size_t recv_buflen, enum aos_rpc_intermon_capability_type expected_recv_cap_type)
{
    assert(context);
    // if send_buf is NULL, send_buflen has to be 0
    assert(send_buf == NULL ? send_buflen == 0 : true);

    // if recv_buf is not null, recv_buflen must not be 0
    assert(recv_buf != NULL ? recv_buflen > 0 : true);

    context->send_env.cap = NULL_CAP;
    context->send_env.buf = send_buf;
    context->send_env.buflen = send_buflen;
    context->send_env.cap_type = send_cap_type;

    context->recv_env.cap = NULL_CAP;
    context->recv_env.buf = recv_buf;
    context->recv_env.buflen = recv_buflen;
    context->recv_env.expected_cap_type = expected_recv_cap_type;
    context->is_server = false;
}

/**
 * Continuation for a communication round that starts receiving after
 * sending has finished
 */
static void aos_rpc_finish_send_start_recv(void *args) {
    assert(args);

    struct aos_rpc_client_comm_state *state = args;

    // propagate the error from the sending operation if there is one
    if (err_is_fail(state->send_state.err)) {
        state->err = state->send_state.err;
        state->complete = true;
        return;
    }

    // busy wait for some iterations to reduce latency when receiving
    size_t max_waiting = state->chan->vtable->get_busy_wait_iterations();
    for (size_t i = 0; i < max_waiting; i++) {
        if (state->chan->vtable->can_recv(state->chan)) {
            // we can receive at least the first fragment, if we do not
            // receive the whole message, the receive handler is automatically
            // registered again for receiving
            state->chan->vtable->recv_handler(&state->recv_state);
            // The following debug_printf() is just to get some hands-on experience on busy waiting.
            // It *must* be off in general, of course.
            //debug_printf("%s: busy waiting for %zu iterations.\n", __func__, i + 1);
            return;
        }
    }

    // if we still cannot receive, we call the receive handler anyway, since it will
    // automatically register itself for receiving
    state->chan->vtable->recv_handler(&state->recv_state);
}

/**
 * Continuation for a communication round, marking it as completed
 */
static void aos_rpc_finish_send_and_recv(void *args) {
    assert(args);

    struct aos_rpc_client_comm_state *state = args;

    // propagate the error from the receive operation
    state->err = state->recv_state.err;
    state->complete = true;
}

static errval_t aos_rpc_client_comm_state_init(struct aos_rpc_client_comm_state *state, struct aos_rpc_comm_context *context, struct aos_rpc_channel *chan) {
    assert(state);
    assert(chan);

    state->prev = NULL;
    state->next = NULL;
    state->chan = chan;
    state->complete = false;
    state->context = context;
    state->err = LIB_ERR_SHOULD_NOT_GET_HERE;

    // initializes the send state with a continuation that will start receiving
    // as soon as sending has finished
    state->send_state = (struct aos_rpc_send_fragment_state) {
        .completed = false,
        .err = SYS_ERR_OK,
        .offset = 0,
        .chan = chan,
        .send_env = &state->context->send_env,
        .cont = MKCLOSURE(aos_rpc_finish_send_start_recv, state)
    };

    // initializes the receive state with a continuation that marks the communication
    // round as completed as soon as receiving has finished
    state->recv_state = (struct aos_rpc_recv_fragment_state) {
        .completed = false,
        .err = SYS_ERR_OK,
        .offset = 0,
        .chan = chan,
        .recv_env = &state->context->recv_env,
        .is_server = false,
        .server_default_recv_buffer = NULL,
        .cont = MKCLOSURE(aos_rpc_finish_send_and_recv, state)
    };

    if (context->recv_env.expected_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
        errval_t err = slot_alloc(&state->recv_cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "slot_alloc failed.\n");
            return err;
        }
    }
    else {
        state->recv_cap = NULL_CAP;
    }
    waitset_chanstate_init(&state->ws_chan, CHANTYPE_OTHER);
    return SYS_ERR_OK;
}

static void aos_rpc_client_comm_state_destroy(struct aos_rpc_client_comm_state *state) {
    waitset_chanstate_destroy(&state->ws_chan);
}

/**
 *  \brief generic send for all rpc call
 *         block until reply received
 *
 *         Tuning: crucial for UMP; not needed but also not hurting for LMP:
 *         The recv_handler is not registered into the waitset all the time.
 *         Busy waiting in a loop is used for a definable number of cycles first.
 *         This was needed to reduce RTT for UMP from 40 ms (2 time slices) down to 7.6 us.
 *         Reason: In LMP, the waiting dispatcher is awaken immediately when a LMP message is
 *         received, thus, the registered recv_handler is called almost immediately upon received
 *         message. In contrast in UMP, the waiting dispatcher is not awoken if the dirty flag is
 *         set in the ringbuffer. It will be polled the next time it is done so actively, which is
 *         in poll_channels_disabled() in lib/aos/waitset.c. Hence the long RTT. This is reduced
 *         by busy waiting.
 *
 *         Additional note: Tests confirmed that the reply is available in the first iteration
 *         of busy waiting in LMP. Thus, typically, the recv_handler will not be registered
 *         in practice but called directly, which preserves the same principle.
 *         Since event handler registration always checks whether there is already an event
 *         waiting, and immediately triggers it then, not much is gained for LMP here (a few
 *         us on the RTT for short messages close to noise; see numbers above).
 *         Nevertheless, it's conecptually nice to allow the same system to run for UMP and LMP
 *         fine-tuned with pre-defined limits, and also to illustrate that also for LMP
 *         registration of the receive handler is typically not needed.
 */
errval_t aos_rpc_generic_send_and_recv(struct aos_rpc *rpc, uintptr_t fid, struct aos_rpc_comm_context *context) {
    assert(rpc);
    assert(context);

    errval_t err = SYS_ERR_OK;

    // check whether re-routing is needed
    if (AOS_RPC_ROUTING_NEEDED(rpc->routing, context->send_env.cap_type, context->recv_env.expected_cap_type)) {
        struct aos_rpc *mon_rpc = aos_rpc_get_monitor_channel();
        if (!mon_rpc) {
            return LIB_ERR_SHOULD_NOT_GET_HERE;
        }

        // start the travel over the seas
        context->send_env.header = fid;
        err = aos_rpc_monitor_request_rpc_forward(mon_rpc, rpc->interface,
                                                  rpc->enumerator,
                                                  fid, rpc->routing,
                                                  &context->send_env,
                                                  &context->recv_env);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_monitor_request_rpc_forward()\n");
            return err;
        }

        return err;
    }

    // make sure that no page-fault happens due to unpaged stack pages
    // when sending/receiving the message
    // Note: The number of reserved bytes used here has to be larger
    //       than the number of reserved bytes for the look ahead in
    //       disp_disable(), since some functions used while sending/
    //       receiving messages internally call disp_disable()
    #define LOOK_AHEAD_DISTANCE_BYTES (BASE_PAGE_SIZE >> 1)
    stack_look_ahead(LOOK_AHEAD_DISTANCE_BYTES);

    // create new state, initialize and assign the msg to it
    struct aos_rpc_client_comm_state state;
    err = aos_rpc_client_comm_state_init(&state, context, rpc->chan);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_client_comm_state_init failed.\n");
        return err;
    }
    context->send_env.header = fid;

    // NOTE jmeier: nested locking is fine here, as aos_rpc_generic_send can recursively call aos_rpc_generic_send_and_block
    //              while allocaing RAM via aos_rpc_get_ram_cap, aos_rpc_generic_send is designed for reentrancy
    thread_mutex_lock_nested(&state.chan->mutex);

    // enqueue our communication round on the channel
    aos_rpc_channel_queue_enqueue(&rpc->chan->queue, &state);

    struct waitset *ws = get_default_waitset();

    // dispatch events as long as our communication round has not yet finished
    while (!state.complete) {
        // take the next state from the channel queue
        struct aos_rpc_client_comm_state *next_state = aos_rpc_channel_queue_front(&rpc->chan->queue);
        assert(next_state);
        if (next_state == &state) {
            // if it is our state, install the cap slot for receiving if necessary and start the send handler
            if (state.context->recv_env.expected_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
                assert(!capref_is_null(state.recv_cap));
                state.chan->vtable->install_recv_slot(state.chan, state.recv_cap);
                state.recv_cap = NULL_CAP;
            }

            state.chan->vtable->send_handler(&state.send_state);
        }
        // dispatch events as long as the current communication round has not yet finished
        while (!next_state->complete) {
            err = event_dispatch(ws);
            if (err_is_fail(err)) {
                // we want to continue on error here, so just log
                DEBUG_ERR(err, "event_dispatch failed.\n");
            }
        }
        // if the current communication round is still current, remove it from the queue
        if (aos_rpc_channel_queue_front(&rpc->chan->queue) == next_state) {
            aos_rpc_channel_queue_dequeue(&rpc->chan->queue);
        }
    }

    aos_rpc_client_comm_state_destroy(&state);

    // for debugging
    //debug_printf("%s: chan %x, if %u: received reply fid %x\n", __func__, rpc->chan, rpc->interface, fid);

    thread_mutex_unlock(&state.chan->mutex);
    return state.err;
}

errval_t late_binding(const char *service_name, struct aos_rpc **ret_rpc)
{
    assert(service_name);
    assert(ret_rpc);

    errval_t err = SYS_ERR_OK;
    struct aos_rpc *name_rpc = aos_rpc_get_name_channel();
    if (!name_rpc) {
        err = LIB_ERR_SHOULD_NOT_GET_HERE;
        DEBUG_ERR(err, "Could not get name channel.\n");
        return err;
    }

    service_handle_t handle;
    // TODO: replace by find any service and check for count / pick first one
    err = aos_rpc_name_lookup_service_by_path(name_rpc, service_name, &handle);
    if (err_is_fail(err)) {
        if (err_no(err) != LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
            DEBUG_ERR(err, "error in aos_rpc_name_lookup_service_by_path()\n");
        }
        return err;
    }

    err = aos_rpc_name_bind_service(name_rpc, handle, disp_get_core_id(), ret_rpc);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_name_bind_service()\n");
        return err;
    }

    return SYS_ERR_OK;
}

/**
 * One iteration of checking for the channel. Deliberately no event dispatching inside of it.
 */
static errval_t aos_rpc_get_channel_helper(struct aos_rpc **target_rpc,
                                    aos_rpc_getter_function_t getter,
                                    aos_rpc_setter_function_t setter,
                                    const char *name)
{
    errval_t err = SYS_ERR_OK;

    if (*target_rpc) {
        return err;
    }

    if (getter) {
        *target_rpc = getter();
        if (*target_rpc) {
            return err;
        }
    }

    err = late_binding(name, target_rpc);
    if (err_no(err) == LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
        *target_rpc = NULL; // just to be 100% sure
        return err;
    }

    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in late_binding()\n");
        *target_rpc = NULL; // just to be 100% sure
        return err;
    }

    // all OK
    if (setter) {
        setter(*target_rpc);
    }
    return err;
}

/**
 * Contract: \return == SYS_ERR_OK <=> *target_rpc != NULL
 */
errval_t aos_rpc_get_channel(struct aos_rpc **target_rpc,
                             bool blocking,
                             ssize_t enumerator,
                             aos_rpc_getter_function_t getter,
                             aos_rpc_setter_function_t setter,
                             const char *name)
{
    assert(target_rpc);
    // getter and setter are optional
    assert(name);

    char *effective_name = (char *)name;

    // adjust name if enumerator is provided
    if (enumerator >= 0) {
        #define MAX_ENUM_LEN 16
        size_t len = strlen(name);
        effective_name = alloca(len + MAX_ENUM_LEN);
        if (!effective_name) {
            // should never happen
            return LIB_ERR_MALLOC_FAIL;
        }
        memset(effective_name, 0, len + MAX_ENUM_LEN);
        strcpy(effective_name, name);
        if (effective_name[len -1] != '/') {
            effective_name[len++] = '/';
        }
        sprintf(effective_name + len, "%zu", enumerator);
    }

    errval_t err = aos_rpc_get_channel_helper(target_rpc, getter, setter, effective_name);
    if (err_is_ok(err)) {
        return SYS_ERR_OK;
    }

    if (!blocking) {
        *target_rpc = NULL;
        return err;
    }

    struct waitset *default_ws = get_default_waitset();
    while (err_no(err) == LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
        err = event_dispatch_non_block(default_ws);
        if (err_is_fail(err) && err_no(err) != LIB_ERR_NO_EVENT) {
            DEBUG_ERR(err, "error in dispatcher\n");
        }

        err = aos_rpc_get_channel_helper(target_rpc, getter, setter, name);
    }

    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in late_binding.\n");
        *target_rpc = NULL;
        return err;
    }

    // all OK
    return SYS_ERR_OK;
}
