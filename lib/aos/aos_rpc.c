/**
 * \file
 * \brief Implementation of AOS rpc-like messaging
 */

/*
 * Copyright (c) 2013 - 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/aos_rpc.h>
#include <rpc_client/aos_rpc_client_common_p.h>

void aos_rpc_init(struct aos_rpc *rpc, struct aos_rpc_channel *chan,
                  enum aos_rpc_interface interface, void *interface_vtable,
                  routing_info_t routing)
{
    assert(rpc);
    assert(chan);
    assert(interface_vtable);

    rpc->chan = chan;
    rpc->interface = interface;
    rpc->interface_vtable = interface_vtable;
    rpc->domain_id = disp_get_domain_id();
    rpc->routing = routing;
}

/**
 * \brief Returns the RPC channel to init (a.k.a. monitor)
 */
static struct aos_rpc *init_rpc = NULL;

struct aos_rpc *aos_rpc_get_init_channel(void)
{
    if (!init_rpc) {
        init_rpc = get_init_rpc();
    }
    return init_rpc;
}
