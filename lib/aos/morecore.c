/**
 * \file
 * \brief Morecore implementation for malloc
 */

/*
 * Copyright (c) 2007 - 2016 ETH Zurich.
 * Copyright (c) 2014, HP Labs.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/aos.h>
#include <aos/core_state.h>
#include <aos/morecore.h>
#include <stdio.h>

typedef void *(*morecore_alloc_func_t)(size_t bytes, size_t *retbytes);
extern morecore_alloc_func_t sys_morecore_alloc;

typedef void (*morecore_free_func_t)(void *base, size_t bytes);
extern morecore_free_func_t sys_morecore_free;

// this define makes morecore use an implementation that just has a static
// 16MB heap.
//#define USE_STATIC_HEAP


#ifdef USE_STATIC_HEAP

// dummy mini heap (16M)

#define HEAP_SIZE (1<<24)

static char mymem[HEAP_SIZE] = { 0 };
static char *endp = mymem + HEAP_SIZE;

/**
 * \brief Allocate some memory for malloc to use
 *
 * This function will keep trying with smaller and smaller frames till
 * it finds a set of frames that satisfy the requirement. retbytes can
 * be smaller than bytes if we were able to allocate a smaller memory
 * region than requested for.
 */
static void *morecore_alloc(size_t bytes, size_t *retbytes)
{
    struct morecore_state *state = get_morecore_state();

    size_t aligned_bytes = ROUND_UP(bytes, sizeof(Header));
    void *ret = NULL;
    if (state->freep + aligned_bytes < endp) {
        ret = state->freep;
        state->freep += aligned_bytes;
    }
    else {
        aligned_bytes = 0;
    }
    *retbytes = aligned_bytes;
    return ret;
}

static void morecore_free(void *base, size_t bytes)
{
    return;
}

errval_t morecore_init(void)
{
    struct morecore_state *state = get_morecore_state();

    thread_mutex_init(&state->mutex);

    state->freep = mymem;

    sys_morecore_alloc = morecore_alloc;
    sys_morecore_free = morecore_free;
    return SYS_ERR_OK;
}

#else
// dynamic heap using lib/aos/paging features

// using constants as defined in include/aos/paging.h
#define HEAP_KNOWN_MAX_SIZE PAGING_MANAGED_MAX_SIZE

// All space of the managed paging space is given to heap,
// except this reserved 256 MiB, which can be used for other regions
// such as stack or device regions
#define HEAP_RESERVED  0x10000000

// current setting:
// - 1 GiB reserved for direct mappings (program binary, dynamicly linked libraries (option for future))
// - 768 MiB for heap
// - 256 MiB for other regions
#define MAX_HEAP_SIZE (HEAP_KNOWN_MAX_SIZE - HEAP_RESERVED)


/**
 * \brief Allocate some memory for malloc to use
 *
 * This function will keep trying with smaller and smaller frames till
 * it finds a set of frames that satisfy the requirement. retbytes can
 * be smaller than bytes if we were able to allocate a smaller memory
 * region than requested for.
 */
static void *morecore_alloc(size_t bytes, size_t *retbytes)
{
    struct morecore_state *state = get_morecore_state();

    // no recursive allowed: check by assert instead just detection by deadlock
    thread_mutex_lock_nested(&state->mutex);

    // reentrant lock because it is also used by malloc beforehand which then calls morecore_alloc
    assert(state->mutex.locked <= 2);

    errval_t err = SYS_ERR_OK;
    void *ret = NULL;

    // check simple implementation at the moment
    // just go with full requested size without checking smaller regions
    // to get experience
    size_t aligned_bytes = ROUND_UP(bytes, sizeof(Header));

    err = paging_region_map(&state->region, aligned_bytes, &ret, retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: paging_region_map() failed.", __func__);
        *retbytes = 0;
        goto unlock;
    }

    // all OK here
    // fall through to unlock

unlock:
    thread_mutex_unlock(&state->mutex);
    return ret;
}

static void morecore_free(void *base, size_t bytes)
{
    struct morecore_state *state = get_morecore_state();

    // no recursive allowed: check by assert instead just detection by deadlock
    thread_mutex_lock_nested(&state->mutex);

    // reentrant lock because it is also used by malloc beforehand which then calls morecore_alloc
    assert(state->mutex.locked <= 2);


    errval_t err = SYS_ERR_OK;

    // no alignment checks here
    // they will all be done within paging_region_unmap()

    err = paging_region_unmap(&state->region, (lvaddr_t) base, bytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: paging_region_unmap() failed.", __func__);
        goto unlock;
    }

    // all OK here
    // fall through to unlock

unlock:
    thread_mutex_unlock(&state->mutex);
}


errval_t morecore_init(void)
{
    struct morecore_state *mc_state = get_morecore_state();

    thread_mutex_init(&mc_state->mutex);


    // this was from USE_STATIC_HEAP
    mc_state->freep = NULL;

    struct paging_state *p_state = get_current_paging_state();

    errval_t err = paging_region_init(p_state, &mc_state->region, MAX_HEAP_SIZE);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: paging_region_init() failed.", __func__);
        return err;
    }

    sys_morecore_alloc = morecore_alloc;
    sys_morecore_free = morecore_free;
    return SYS_ERR_OK;
}

#endif

Header *get_malloc_freep(void);
Header *get_malloc_freep(void)
{
    return get_morecore_state()->header_freep;
}
