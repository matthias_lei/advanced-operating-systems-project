/*
 * Copyright (c) 2007, 2008, 2009, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/aos.h>
#include <aos/caddr.h>
#include <aos/debug.h>
#include <aos/dispatch.h>

#include "threads_priv.h"

// only needed for struct definitions
// ELF functions should not be used to avoid the need to link the elf library into all binaries
#include "elf/elf.h"


void debug_dump(arch_registers_state_t *archregs)
{
#define dpr(reg) debug_printf("%-6s 0x%08"PRIx32 "\n", #reg, archregs->named. reg)
    dpr(r0);    dpr(r1);        dpr(r2);        dpr(r3);
    dpr(r4);    dpr(r5);        dpr(r6);        dpr(r7);
    dpr(r9);    dpr(r10);       dpr(r11);       dpr(r12);
    dpr(stack); dpr(link);      dpr(pc);        dpr(cpsr);
}

void debug_call_chain(arch_registers_state_t *archregs)
{
    // TODO: use regs argument
    void* fp = __builtin_frame_address(0);
    void* ra = __builtin_return_address(0);
    if (fp != NULL) {
        debug_printf("%8d frame %p return %p\n", 0, fp, ra);
    }
}

void debug_print_save_area(arch_registers_state_t *state)
{
    debug_dump(state);
}


//=== Group C extensions ===========================================================================

//--- Modified ELF functions that work with tables at arbitrary locations --------------------------
// The symbols of the ELF file (.symtab, .strtab) are not loaded into the memory during elf_load().
// There are 3 options to provide runtime access to them:
// 1) access the external file again during runtime (not possible at the moment)
// 2) modify the internal functions of the ELF library to load more data into the fixed locations
//    during spawn (note: not even the ELF magic key is loaded at the moment)
// 3) Load them at an arbitrary location and use modified access functions. These functions are
//    basically identical to the ELF functions with the exception that they do not take the
//    main ELF base but directly the section header as base address.
// These functions below provide the functionalities needed to go with solution 3.
// Note: the functions below are already based on the fixed / extended versions of the ELF library.
//
// Additional reason to prefer solution 3 to 2: If we use the original ELF functions for the
// runtime lookup, *each* program file must link the elf library, which is not really needed/desired.
//
// Please note: the functions used here (e.g. variants of the ELF functions) have additional checks
// to make as sure as even possible that no additional exception might be thrown while executing
// them (defensive programming style). Thus, some additional differences with the fixed ELF functions.
//
// pisch


/**
 * \brief finds the function symbol by its predicted address
 *
 * note: there are 2 different function prologues use in the binaries that have a different
 * relation between fp and function base address. This lookup function here tests for both
 * prologue types in parallel while iterating over the .symtab. Thus, the function was also
 * additionally to renamed compared to the original elf function.
 *
 * \param symtab_base   address of the actual .symtab section
 * \param symtab_size   size of the buffer to protect against any outside memory access
 * \param addr      virtual address of the symbol
 * \param sindex    can be NULL if client is not interested in using the index
 *                  if /= NULL: 1) if found: index to found symbol
 *                                     else: index to end of list
 *                                 note: both can be used as is for the next search round
 *                              2) as input argument, indicates where the search has
 *                                 to start, i.e. it must be 0 on the first search
 *                                 for an address.
 *
 * \returns pointer to the symbol
 *          NULL if there is none
 */
static struct Elf32_Sym *debug_elf32_find_function_symbol_by_addr(void *symtab_base, size_t symtab_size,
                                                  lvaddr_t addr, uintptr_t *sindex)
{
    assert(symtab_base);

    uintptr_t idx = 0;
    if (sindex) {
        if (0 < *sindex) {
            // does still point to found symbol if last search has been a success
            idx = *sindex + 1;
        }
    }

    uintptr_t symbase = (uintptr_t)symtab_base;

    struct Elf32_Sym *sym = NULL;

    // assure that the last part of the buffer is an entire struct 
    uintptr_t max = (symtab_size / sizeof(struct Elf32_Sym)) * (sizeof(struct Elf32_Sym));

    // kept the uintptr_t implementation of the original; array access could be used as an alternative
    for (uintptr_t i = idx * sizeof(struct Elf32_Sym); i < max; i += sizeof(struct Elf32_Sym)) {
        // getting the symbol
        struct Elf32_Sym *test_sym = (struct Elf32_Sym *)(symbase + i);
        
        /* XXX: not handling relocatable symbols */
        // TODO pisch: this is the same state as the original elf32 function
        //             we adjust it here when we adjust it there.
        if (test_sym->st_value == addr) {
            sym = test_sym;
            break;
        }

        // test 2nd function prologue in parallel
        if (test_sym->st_value == (addr + 4)) {
            sym = test_sym;
            break;
        }

        idx++;
    }

    // it points to the end of the list if search has been unsuccessful
    if (sindex) {
        *sindex = idx;
    }

    return sym;
}


/**
 * Note: the proper .strtab must be provided that matches the .symtab
 * see struct Elf32_Shdr *strtab = shead+symtab->sh_link of the original
 * This is provided during the spawn process when the additional tables are loaded.
 */
static const char *debug_elf32_get_symbolname(void *strtab_base, size_t strtab_size, struct Elf32_Sym *sym)
{
    assert(strtab_base);
    assert(sym);

    // get the pointer to the symbol name from string table + string index
    // assure that the requested name is not outside of allocated memory region
    const char *name = ((const char *)strtab_base) + sym->st_name;

    if ((uintptr_t)name >= ((uintptr_t)strtab_base + strtab_size)) {
        return NULL;
    }

    return name;
}

//--- Stack trace ----------------------------------------------------------------------------------
// The stack trace is generated by following the frame pointers fp (r11) as indicated in the
// ARMv7 ABI.
// fp[-0] saved pc: hold the pc when this frame was created -> used to find the function base address
//                  the function label is 4 words (= 16 bytes) before -> adjustment
// fp[-1] saved lr: holds the return address for the function, which is one word after the
//        calling site. Thus, adjustment by one word again to get the proper call instruction.
// fp[-2] saved sp: not used at the moment
// fp[-3] saved fp: holds the fp of the calling function. It is NULL if the end of the call stack
// is reached. 
//
// reference: stackoverflow answer by artless noise and edited by Pod on
// https://stackoverflow.com/questions/15752188/arm-link-register-and-frame-pointer?answertab=votes#tab-top
//
// Two interfaces are offered:
// - with (arch_registers_state_t *), typically called from an exception handler
// - starting from current program counter (pc), typically used for other debugging activities
//
// Three termination options are used:
// - clear end of stack trace (return address == NULL)
// - for known stack sizes (if thread_self() != NULL): in case detected fp would go outside
//   of known stack
// - for unknown stack size: if an arbitrary recursion depth is reached
//
// As an option, the debug symbols can be read from the ELF file and be used for symbol
// printing during the stacktrace
//
// version 2017-11-03 pisch

// note: this value can be changed as desired
#define DEBUG_STACK_TRACE_MAX_DEPTH_WITH_UNKNOWN_STACK 20

/**
 * uses ELF information to retrieve the symbol name of the function address, if available
 * \return may be NULL if not found or if symtab_base or strtab_base not available
 *
 * note: multiple symbols are stored per address, e.g. $a with name index 1, st_info = 0,
 * Function symbols have values 2 or 18. Thus filtering for the bit set by STT_FUNC (= 2) 
 */ 
static const char *find_function_name(struct paging_elf_key_data *elf_info, uintptr_t *function)
{
    assert(elf_info);

    if(!elf_info->symtab_base) {
        return NULL;
    }

    if(!elf_info->strtab_base) {
        return NULL;
    }

    if (!function) {
        return NULL;
    }

    // currently, multiple symbols are stored per address
    // first a symbol $a is found with name index 1
    // then the actually desired function name symbol is found
    struct Elf32_Sym *sym = NULL;
    uintptr_t sindex = 0;
    do {
        sym = debug_elf32_find_function_symbol_by_addr((struct Elf32_Shdr *)elf_info->symtab_base, elf_info->symtab_size, (lvaddr_t)function, &sindex);
    } while ((sym) && !(sym->st_info & STT_FUNC));

    if (!sym) {
        return NULL;
    }

    return debug_elf32_get_symbolname((struct Elf32_Shdr *)elf_info->strtab_base, elf_info->strtab_size, sym);
}


static void debug_stack_unwind(struct paging_elf_key_data *elf_info, uintptr_t **frame_ptr, uintptr_t *stack, uintptr_t *stack_top, int count)
{
    uintptr_t *return_addr = *(frame_ptr - 1);
    if (!return_addr) {
        return;
    }

    uintptr_t *pc = return_addr - 1;
    uintptr_t **prior_fp = (uintptr_t **)*(frame_ptr - 3);
    uintptr_t *function = prior_fp ? (*(prior_fp) - 4) : NULL;

    if (stack && stack_top) {
        if ((uintptr_t **)stack < prior_fp && prior_fp < (uintptr_t **)stack_top) {
            debug_stack_unwind(elf_info, prior_fp, stack, stack_top, count + 1);
        }
    }
    else {
        if (count < DEBUG_STACK_TRACE_MAX_DEPTH_WITH_UNKNOWN_STACK) {
            debug_stack_unwind(elf_info, prior_fp, stack, stack_top, count + 1);
        }
        else {
            debug_printf("<max. stack depth reached for unknown stack size (thread_self() == NULL)>\n");
        }
    }

    const char *function_name = find_function_name(elf_info, function);

    if (function_name) {
        debug_printf("- in function '%s' (%p) at %p\n", function_name, function, pc);
    }
    else {
        debug_printf("- in function <name unavailable> (%p) at %p\n", function, pc);
    }
}


static void debug_stack_trace_helper(uintptr_t **frame_ptr, uintptr_t *pc, const char *title, const char *message)
{
    struct paging_state *st = get_current_paging_state();
    assert(st);
    struct paging_elf_key_data *elf_info = &st->elf_info;

    uintptr_t **prior_fp = (uintptr_t **)*(frame_ptr - 3);

    debug_printf("\n");
    debug_printf("%s -- stack trace:\n", title);

    struct thread *self = thread_self();
    if (self) {
        debug_stack_unwind(elf_info, prior_fp, self->stack, self->stack_top, 0);
    }
    else {
        debug_stack_unwind(elf_info, prior_fp, NULL, NULL, 0);
    }

    uintptr_t *function = *(frame_ptr) - 4;

    const char *function_name = find_function_name(elf_info, function);

    if (function_name) {
        debug_printf("- %s in function '%s' (%p) at %p\n", message, function_name, function, pc);
    }
    else {
        debug_printf("- %s in function <name unavailable> (%p) at %p\n", message, function, pc);
    }
    debug_printf("\n");
}

void debug_stack_trace(arch_registers_state_t *regs, const char *title, const char *message)
{
    assert(regs);
    assert(title);
    assert(message);

    debug_stack_trace_helper((uintptr_t **)regs->named.r11, (uintptr_t *)regs->named.pc, title, message);
}

void debug_stack_trace_from_pc(uintptr_t **frame_ptr, uintptr_t *pc, const char *title)
{
    assert(frame_ptr);
    assert(pc);
    assert(title);
    debug_stack_trace_helper(frame_ptr, pc, title, "current location");
}
