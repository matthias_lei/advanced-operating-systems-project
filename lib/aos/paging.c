/**
 * \file
 * \brief AOS paging helpers.
 */

/*
 * Copyright (c) 2012, 2013, 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/aos.h>
#include <aos/paging.h>
#include <aos/except.h>
#include <aos/slab.h>
#include "threads_priv.h"

#include <stdio.h>
#include <string.h>

#include <aos/threads.h>

#define EXCEPTION_STACK_SIZE (4 * BASE_PAGE_SIZE)

// note: currently a 16 byte alignment is guaranteed for stack_top
// this comes from the x86_64 specs and is probably not needed for ARMv7
// but it does not hurt
#define EXCEPTION_STACK_ALIGNMENT (sizeof(uint64_t) * 2)

static struct paging_state current;

static uintptr_t first_exception_stack[EXCEPTION_STACK_SIZE >> 2];

// internal helper functions
static errval_t paging_map_fixed_attr_helper(struct paging_state *st, lvaddr_t vaddr,
                                             struct capref frame, size_t bytes, int flags);

static void print_paging_region(struct paging_region *pr, const char *message);

static bool vspace_free(struct paging_state *st, lvaddr_t base, size_t size);

static void paging_init_onthread_helper(struct thread *t, void *exception_stack, size_t exception_stack_size);


// exception handler
void exception_handler(enum exception_type type, int subtype,
                       void *addr, arch_registers_state_t *regs,
                       arch_registers_fpu_state_t *fpuregs);

errval_t page_fault_handler(int subtype, void *addr);


/**
 * We use this function from upstream original barrelfish code, with modifications to our setting.
 * URL: https://github.com/BarrelfishOS/barrelfish/blob/master/lib/barrelfish/arch/arm/pmap_arch.c
 */
static inline uint64_t
vregion_flags_to_kpi_paging_flags(int flags)
{
    STATIC_ASSERT(0x2f == VREGION_FLAGS_MASK, "");
    STATIC_ASSERT(0x0f == KPI_PAGING_FLAGS_MASK, "");
    STATIC_ASSERT(VREGION_FLAGS_READ    == KPI_PAGING_FLAGS_READ,    "");
    STATIC_ASSERT(VREGION_FLAGS_WRITE   == KPI_PAGING_FLAGS_WRITE,   "");
    STATIC_ASSERT(VREGION_FLAGS_EXECUTE == KPI_PAGING_FLAGS_EXECUTE, "");
    STATIC_ASSERT(VREGION_FLAGS_NOCACHE == KPI_PAGING_FLAGS_NOCACHE, "");
    if ((flags & VREGION_FLAGS_MPB) != 0) {
        // XXX: ignore MPB flag on ARM, otherwise the assert below fires -AB
        flags &= ~VREGION_FLAGS_MPB;
    }
    if ((flags & VREGION_FLAGS_GUARD) != 0) {
        flags = 0;
    }
    assert(0 == (~KPI_PAGING_FLAGS_MASK & (uint64_t)flags));
    return (uint64_t)flags;
}

/**
 * \brief Helper function that allocates a slot and
 *        creates a ARM l2 page table capability
 */
static errval_t arml2_alloc(struct paging_state *st, struct paging_l2pt_entry *entry)
{
    assert(st);
    assert(entry);
    errval_t err = st->slot_alloc->alloc(st->slot_alloc, &entry->cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "st->slot_alloc->alloc failed.\n");
        return err;
    }

    err = vnode_create(entry->cap, ObjType_VNode_ARM_l2);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "vnode_create failed.\n");
        return err;
    }

    if (get_croot_addr(entry->cap) != CPTR_ROOTCN) {
        err = slot_alloc(&entry->invokable);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "slot_alloc failed.\n");
            return err;
        }
        err = cap_copy(entry->invokable, entry->cap);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "cap_copy failed.\n");
            assert(false);
            return err;
        }
    }
    else {
        entry->invokable = entry->cap;
    }

    return SYS_ERR_OK;
}

//--- List management helper functions for paging regions ------------------------------------------

static void init_paging_region_list(struct paging_region_list *list) {
    assert(list);
    thread_mutex_init(&list->mutex);
    list->head = NULL;
    list->size = 0;
}

// assumes paging region not in list
// assumes paging region at this address may be inserted
static void insert_paging_region_sorted_by_address(struct paging_region_list *list, struct paging_region *pr){
    assert(list);
    assert(pr);
    assert(!pr->prev);
    assert(!pr->next);

    //lock
    thread_mutex_lock_nested(&list->mutex);
    assert(list->mutex.locked == 1); // no recursion allowed here

    if (!list->head) {
        // empty list
        list->head = pr;
        goto exit;
    }

    struct paging_region *cur = list->head;
    struct paging_region *pred = NULL;

    while (cur) {
        if (pr->base_addr < cur->base_addr) {
            // insert pr before cur
            if (cur == list->head) {
                list->head = pr;
            }

            pr->prev = cur->prev;
            cur->prev = pr;
            pr->next = cur;
            goto exit;
        }

        pred = cur;
        cur = cur->next;
    }

    // insert pr as last element
    pred->next = pr;
    pr->prev = pred;

exit:
    list->size++;
    thread_mutex_unlock(&list->mutex);
}


// assumes direct pointer to paging region to be removed is passed in pr
__attribute__((unused))
static void remove_paging_region(struct paging_region_list *list, struct paging_region *pr){
    assert(list);
    assert(pr);

    //lock
    thread_mutex_lock_nested(&list->mutex);
    assert(list->mutex.locked == 1); // no recursion allowed here


    if (!pr->prev){
        // first element
        list->head = pr->next;
    }
    else {
        pr->prev->next = pr->next;
    }

    if (pr->next) {
        // not last element
        pr->next->prev = pr->prev;
    }

    list->size--;
    thread_mutex_unlock(&list->mutex);
}


static struct paging_region* find_paging_region_by_address(struct paging_region_list *list, lvaddr_t vaddr){
    assert(list);

    //lock
    thread_mutex_lock_nested(&list->mutex);
    assert(list->mutex.locked == 1); // no recursion allowed here

    struct paging_region *cur = list->head;
    while (cur) {
        // check if vaddr lies in address range of current
        if (cur->base_addr <= vaddr && vaddr < (cur->base_addr + cur->region_size)) {
            goto exit;
        }

        cur = cur->next;
    }

exit:
    // unlock
    thread_mutex_unlock(&list->mutex);
    return cur;
}

static void print_paging_region_list(struct paging_region_list *list, const char* msg)
{
    assert(list);
    assert(msg);
    struct paging_region *cur = list->head;

    debug_printf("%s: Paging regions (size %u)\n", msg, list->size);
    while (cur) {
        print_paging_region(cur, "");
        cur = cur->next;
    }
    debug_printf("\n");
}

//--------------------------------------------------------------------------------------------------

errval_t paging_init_state(struct paging_state *st, lvaddr_t start_vaddr,
                           struct capref pdir, struct slot_allocator *ca)
{
    // DONE (M2): implement state struct initialization
    // DONE (M4): Implement page fault handler that installs frames when a page fault
    // occurs and keeps track of the virtual address space.
    assert(st);
    assert(ca);
    st->slot_alloc = ca;

    // init lock; note: the paging region list has its own separate lock
    thread_mutex_init(&st->mutex);

    //round up to next 4KB alignment
    lvaddr_t actual_start_vaddr = ROUND_UP(start_vaddr, BASE_PAGE_SIZE);

    st->vspace_base_addr = actual_start_vaddr;
    st->allocated_region_end = actual_start_vaddr;

    st->l1_pagetable = pdir;

    for (int i = 0; i < ARM_L1_MAX_ENTRIES; i++) {
        st->l2_pts[i].cap = NULL_CAP;
        st->l2_pts[i].invokable = NULL_CAP;
        st->l2_pts[i].mapping = NULL_CAP;
        st->l2_pts[i].mapped = false;
    }
    st->l2_pts_reserve.cap = NULL_CAP;
    st->l2_pts_reserve.invokable = NULL_CAP;
    st->l2_pts_reserve.mapping = NULL_CAP;
    st->l2_pts_reserve.mapped = false;

    // assure that the bitmap is cleared
    memset((void *)st->mapped_pages_bitmap, 0, BITMAP_SIZE * sizeof(bitmap_t));
    st->mapped_pages_count = 0;
    st->allocated_in_reserved_space = 0;

    // check boundaries on 4 KiB alignments
    assert(!(st->vspace_base_addr & BASE_PAGE_MASK));

    // initialize list
    init_paging_region_list(&st->paging_regions);

    // initially, no ELF info available
    memset(&st->elf_info, 0, sizeof(struct paging_elf_key_data));

    /* // comment toggle: only during development / debugging (just add a / in front of multiline comment sign )
    debug_printf("%s: VSpace:\n"
                 "- reserved 0x%x to 0x%x\n"
                 "- managed 0x%x to 0x%x, size %u MiB\n"
                 "- kernel protected (unused in user space) 0x%x to 0x%x\n",
                 __func__, 0, st->vspace_base_addr - 1,
                 st->vspace_base_addr, (PAGING_END - 1), (PAGING_END - st->vspace_base_addr) / 1024 / 1024,
                 PAGING_END, 0xffffffff);
    //*/

    // default
    st->bootstrap_mode = false;

    return SYS_ERR_OK;
}

/**
 * \brief This function initializes the paging for this domain
 * It is called once before main.
 */
errval_t paging_init(void)
{
    // DONE (M2): Call paging_init_state for &current
    // DONE (M4): initialize self-paging handler
    // TIP: use thread_set_exception_handler() to setup a page fault handler
    // TIP: Think about the fact that later on, you'll have to make sure that
    // you can handle page faults in any thread of a domain.
    // TIP: it might be a good idea to call paging_init_state() from here to
    // avoid code duplication.

    errval_t err;

    struct capref l1_pagetable_init = {
        .cnode = cnode_page,
        .slot = 0,
    };

    err = paging_init_state(&current, PAGING_RESERVED, l1_pagetable_init, get_default_slot_allocator());
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_init_state_failed in paging_init\n");
        return err;
    }

    set_current_paging_state(&current);

    paging_init_onthread_helper(thread_self(), first_exception_stack, EXCEPTION_STACK_SIZE);

    return SYS_ERR_OK;
}


/**
 * \brief Initialize per-thread paging state
 */
static void paging_init_onthread_helper(struct thread *t, void *exception_stack, size_t exception_stack_size)
{
    assert(t);
    assert(exception_stack);
    assert(BASE_PAGE_SIZE <= exception_stack_size);
    assert(!(exception_stack_size & BASE_PAGE_MASK));

    size_t end = (size_t)exception_stack + exception_stack_size;
    void *exception_stack_top = (void *) ROUND_DOWN(end - 1, EXCEPTION_STACK_ALIGNMENT);

    errval_t err = thread_set_exception_handler_for_thread(t, exception_handler,
                       NULL, exception_stack, exception_stack_top,
                       NULL, NULL);

    if (err_is_fail(err)) {
        debug_printf("%s: set_exception_handler_for_thread() failed. Exception handler NOT installed.\n");
    }
}

void paging_init_onthread(struct thread *t)
{
    // DONE (M4): setup exception handler for thread `t'.
    assert(t);

    // note: the exception stack region is embedded in the thread struct
    errval_t err = paging_region_init(get_current_paging_state(), &(t->exception_stack_region), EXCEPTION_STACK_SIZE);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error allocating exception stack region for new thread\n");
        return;
    }

    void *exception_stack = NULL;
    size_t ret_size = 0;
    // note: paging_region_map does not yet actually map frames; they will be filled by the page fault handler
    err = paging_region_map(&(t->exception_stack_region), EXCEPTION_STACK_SIZE,
                            &exception_stack, &ret_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error reserving stack region for new thread\n");
        return;
    }

    // this memset is used to trigger the page fault handler to map the entire exception stack space
    // note: each exception handler stack has its own guard page at the bottom
    memset(exception_stack, 0, ret_size);

    paging_init_onthread_helper(t, exception_stack, ret_size);
}


//--- serialize / deserialize ----------------------------------------------------------------------

struct paging_region_list_serialize_data {
    size_t off;
    size_t len;
};

struct paging_state_serialize_data {
    size_t l2_pts_off;
    size_t l2_pts_len;
    struct capref reserve_cap;
    lvaddr_t vspace_base_addr;
    lvaddr_t allocated_region_end;
    struct paging_region_list_serialize_data paging_regions;
    bitmap_t mapped_pages_bitmap[BITMAP_SIZE];
    size_t mapped_pages_count;
    size_t allocated_in_reserved_space;
    struct paging_elf_key_data elf_info;
};

struct paging_serialize_l2pt_entry {
    size_t slot;
    struct capref cap;
    struct capref mapping;
};

static errval_t paging_regions_list_serialize(struct paging_state *st, struct paging_region_list *list, char *buf, size_t buflen, size_t *size)
{
    assert(st);
    assert(list);
    assert(buf);

    *size = 0;
    if (*size + sizeof(struct paging_region_list_serialize_data) > buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }
    struct paging_region_list_serialize_data *data = (void*)buf;
    buf += sizeof(struct paging_region_list_serialize_data);
    *size += sizeof(struct paging_region_list_serialize_data);

    if (*size + list->size*sizeof(struct paging_region) > buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }

    data->off = buf - (char*)&data->off;
    data->len = 0;
    struct paging_region *cur = list->head;
    struct paging_region *entries = (void*)buf;

    while (cur) {
        assert (data->len < list->size);

        memcpy(&entries[data->len], cur, sizeof(struct paging_region));

        data->len++;
        cur = cur->next;
    }
    assert (data->len == list->size);

    *size += data->len*sizeof(struct paging_region);

    return SYS_ERR_OK;
}

// TODO: needs to be adjusted to new state infra structure
errval_t paging_state_serialize(struct paging_state *st, char *buf, size_t buflen, size_t *size)
{
    assert(st);
    assert(buf);
    assert((lvaddr_t)buf == ROUND_UP((lvaddr_t)buf, sizeof(void*)));
    assert(size);

    assert(!st->bootstrap_mode);

    *size = 0;
    if (*size + sizeof(struct paging_state_serialize_data) > buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }
    struct paging_state_serialize_data *data = (void*)buf;
    buf += sizeof(struct paging_state_serialize_data);
    *size += sizeof(struct paging_state_serialize_data);

    data->l2_pts_off = buf - (char*)&data->l2_pts_off;
    data->l2_pts_len = 0;
    for (size_t i = 0; i < ARM_L1_MAX_ENTRIES; i++) {
        if (!st->l2_pts[i].mapped) {
            continue;
        }
        if (*size + sizeof(struct paging_serialize_l2pt_entry) > buflen) {
            return LIB_ERR_SERIALISE_BUFOVERFLOW;
        }
        struct paging_serialize_l2pt_entry* entry = (void*)buf;
        entry->slot = i;
        entry->cap = st->l2_pts[i].cap;
        entry->mapping = st->l2_pts[i].mapping;
        buf += sizeof(struct paging_serialize_l2pt_entry);
        *size += sizeof(struct paging_serialize_l2pt_entry);
        data->l2_pts_len++;
    }

    data->reserve_cap = st->l2_pts_reserve.cap;

    data->vspace_base_addr = st->vspace_base_addr;
    data->allocated_region_end = st->allocated_region_end;

    size_t paging_regions_list_size;
    errval_t err = paging_regions_list_serialize(st, &st->paging_regions, buf, buflen - *size, &paging_regions_list_size);

    if (err_is_fail(err)) {
        return err;
    }
    buf += paging_regions_list_size;
    *size += paging_regions_list_size;

    memcpy(data->mapped_pages_bitmap, (void*)st->mapped_pages_bitmap, sizeof(bitmap_t)*BITMAP_SIZE);
    data->mapped_pages_count = st->mapped_pages_count;
    data->allocated_in_reserved_space = st->allocated_in_reserved_space;

    data->elf_info = st->elf_info;

    return SYS_ERR_OK;
}

static errval_t paging_regions_list_deserialize(struct paging_state *st, struct paging_region_list *list, char *buf, size_t buflen)
{
    assert(list);
    assert(buf);

    if (sizeof(struct paging_region_list_serialize_data) > buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }
    struct paging_region_list_serialize_data *data = (void*)buf;
    buf += sizeof(struct paging_region_list_serialize_data);
    buflen -= sizeof(struct paging_region_list_serialize_data);

    if (data->len == 0) {
        st->paging_regions.head = NULL;
        st->paging_regions.size = 0;
        return SYS_ERR_OK;
    }
    st->paging_regions.head = (void*)&data->off + data->off;
    st->paging_regions.size = data->len;
    struct paging_region *entries = st->paging_regions.head;

    if ((char*)(entries + data->len) > buf + buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }

    entries[0].prev = NULL;
    entries[0].state = st;

    for (size_t i = 1; i < data->len; i++) {
        entries[i-1].next = &entries[i];
        entries[i].prev = &entries[i-1];
        entries[i].state = st;
    }

    entries[data->len-1].next = NULL;

    st->bootstrap_mode = false;

    return SYS_ERR_OK;
}


// TODO: needs to be adjusted to new state infra structure
errval_t paging_state_deserialize(struct paging_state *st, char *buf, size_t buflen)
{
    assert(st);
    assert(buf);

    if (sizeof(struct paging_state_serialize_data) > buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }
    struct paging_state_serialize_data *data = (void*)buf;
    buf += sizeof(struct paging_state_serialize_data);
    buflen -= sizeof(struct paging_state_serialize_data);

    struct paging_serialize_l2pt_entry *entries = (void*)&data->l2_pts_off + data->l2_pts_off;
    if ((char*)(entries + data->l2_pts_len) > buf + buflen) {
        return LIB_ERR_SERIALISE_BUFOVERFLOW;
    }

    for (size_t i = 0; i < data->l2_pts_len; i++) {
        size_t slot = entries[i].slot;
        struct capref entry = entries[i].cap;
        entry.cnode.croot = CPTR_ROOTCN;
        st->l2_pts[slot].cap = entry;
        st->l2_pts[slot].invokable = entry;
        st->l2_pts[slot].mapping = entries[i].mapping;
        st->l2_pts[slot].mapped = true;
    }

    buf += data->l2_pts_len*sizeof(struct paging_serialize_l2pt_entry);
    buflen -= data->l2_pts_len*sizeof(struct paging_serialize_l2pt_entry);

    if (capref_is_null(data->reserve_cap)) {
        st->l2_pts_reserve.cap = NULL_CAP;
        st->l2_pts_reserve.invokable = NULL_CAP;
    }
    else {
        data->reserve_cap.cnode.croot = CPTR_ROOTCN;
        st->l2_pts_reserve.cap = data->reserve_cap;
        st->l2_pts_reserve.invokable = data->reserve_cap;
    }
    st->l2_pts_reserve.mapped = false;

    st->vspace_base_addr = data->vspace_base_addr;
    st->allocated_region_end = data->allocated_region_end;

    errval_t err = paging_regions_list_deserialize(st, &st->paging_regions, buf, buflen);
    if (err_is_fail(err)) {
        return err;
    }

    memcpy((void*)st->mapped_pages_bitmap, data->mapped_pages_bitmap, sizeof(bitmap_t)*BITMAP_SIZE);
    st->mapped_pages_count = data->mapped_pages_count;
    st->allocated_in_reserved_space = data->allocated_in_reserved_space;

    size_t mapped_pages_count = 0;
    for (size_t i = 0; i < 0x80000000; i += BASE_PAGE_SIZE) {
        if (check_bitmap(st->mapped_pages_bitmap, i)) {
            mapped_pages_count++;
        }
    }
    assert(st->mapped_pages_count == mapped_pages_count);

    st->elf_info = data->elf_info;

    return SYS_ERR_OK;
}

//--- paging region specific code and data structures ----------------------------------------------

/**
 * \brief This function is called by the pagefault event handler after the actual paging region has
 * been identified.
 *
 * \param   pr      initialized paging region
 * \param   vaddr   actual virtual address that caused the page fault
 * \return  mapping was only performed correctly in case of SYS_ERR_OK
 *
 * Note: the bitmap is used to check whether an address has already been mapped.
 *
 * causes for error are:
 * - vaddr actually not in range of paging_region
 * - any error during mapping functions
 * - request hits the guard part of the region.
 */
static errval_t paging_region_page_fault_handler(struct paging_region *pr, lvaddr_t vaddr) {
    assert(pr);

    lvaddr_t base_addr = ALIGNED_BASE_ADDRESS(vaddr);
    assert(base_addr <= vaddr && vaddr < (base_addr + BASE_PAGE_SIZE));

    if (base_addr < pr->begin_available_addr || pr->end_available_addr < (base_addr + BASE_PAGE_SIZE)) {
        debug_printf("%s: ERROR requested address %" PRIxLVADDR " is in the GUARD part of this memory region. OVERFLOW! NO mapping.\n",
                     __func__, vaddr);
        return LIB_ERR_VREGION_PAGEFAULT_HANDLER;
    }

    if (check_bitmap(pr->state->mapped_pages_bitmap, base_addr)) {
        // helpful during debugging:
        //debug_printf("%s: address page %x already mapped.\n", __func__, base_addr);
        return SYS_ERR_OK;
    }

    // get 4 KiB frame and map it to given base_addr
    struct capref frame;
    size_t retsize;
    errval_t err = frame_alloc(&frame, BASE_PAGE_SIZE, &retsize);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc() failed.\n");
        return err;
    }

    // all are R/W at the moment
    err = paging_map_fixed_attr_helper(pr->state, base_addr, frame, BASE_PAGE_SIZE, VREGION_FLAGS_READ_WRITE);
    if (err_is_fail(err)) {
        // the frame is lost at the moment
        DEBUG_ERR(err, "paging_map_fixed_attr_helper() failed.\n");
        return err;
    }

    // clear the mapped memory
    memset((void *)base_addr, 0, BASE_PAGE_SIZE);

    // note: set_bitmap() was already done within paging_map_fixed_attr_helper()
    // which is now tracking *all* mappings, even from direct mappings outside of regions.

    return SYS_ERR_OK;
}

/**
 * \brief return a pointer to a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 */
errval_t paging_region_init(struct paging_state *st, struct paging_region *pr, size_t size)
{
    assert(st);
    assert(pr);

    void *base = NULL;
    size = ROUND_UP(size, BASE_PAGE_SIZE);
    size_t adjusted_size = size + BOTTOM_GUARD_SIZE + TOP_GUARD_SIZE;

    errval_t err = paging_alloc(st, &base, adjusted_size);
    if (err_is_fail(err)) {
        debug_printf("%s: paging_alloc_aligned() failed\n", __func__);
        return err_push(err, LIB_ERR_VSPACE_MMU_AWARE_INIT);
    }

    assert(!( ((lvaddr_t)base) & BASE_PAGE_MASK) );

    pr->base_addr    = (lvaddr_t)base;
    pr->region_size  = adjusted_size;

    // for the guards
    pr->begin_available_addr = pr->base_addr + BOTTOM_GUARD_SIZE; // this address is including
    pr->available_size = size;
    pr->end_available_addr = pr->begin_available_addr + pr->available_size; // this address is excluding

    pr->current_addr = pr->begin_available_addr;

    // state is needed while handling page fault requests.
    pr->state = st;
    pr->prev = NULL;
    pr->next = NULL;

    // insert into list
    insert_paging_region_sorted_by_address(&st->paging_regions, pr);

    return SYS_ERR_OK;
}

static void print_paging_region(struct paging_region *pr, const char *message) {
    assert(pr);

    debug_printf("%s: region at guard %p size %u KiB, begin %p, end %p, available size %u KiB\n",
        message, pr->base_addr, pr->region_size / 1024, pr->begin_available_addr, pr->end_available_addr, pr->available_size / 1024);
}


/**
 * \brief return a pointer to a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 */
errval_t paging_region_map(struct paging_region *pr, size_t req_size,
                           void **retbuf, size_t *ret_size)
{
    assert(pr);

    errval_t err = SYS_ERR_OK;

    req_size = ROUND_UP(req_size, BASE_PAGE_SIZE);
    size_t rem = pr->end_available_addr - pr->current_addr;

    if (req_size <= rem) {
        // ok
        *retbuf = (void *)pr->current_addr;
        *ret_size = req_size;
        pr->current_addr += req_size;
    }
    else if (0 < rem) {
        // assure 4 KiB
        assert(!(rem & BASE_PAGE_MASK));

        *retbuf = (void *)pr->current_addr;
        *ret_size = rem;
        pr->current_addr += rem;
        assert(pr->current_addr == pr->end_available_addr);

        debug_printf("%s: exhausted paging region 0x%x to 0x%x (size %u KiB); expect 'badness' on next allocation\n",
                     __func__, pr->begin_available_addr, pr->end_available_addr, pr->available_size / 1024);

    }
    else {
        return LIB_ERR_VSPACE_MMU_AWARE_NO_SPACE;
    }

    return err;
}

/**
 * \brief free a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 * NOTE: Implementing this function is optional.
 */
errval_t paging_region_unmap(struct paging_region *pr, lvaddr_t base, size_t bytes)
{
    // TIP: you will need to keep track of possible holes in the region

    // TODO: not yet implemented

    // TODO pisch: note we need to implement an "extension" here that the entire mapped region is
    // unmapped in case base == NULL. I am already using this extension since it is
    // a no-op at the moment
    return SYS_ERR_OK;
}


//--- paging alloc and map functions ---------------------------------------------------------------


/**
 *
 * \brief Find a bit of free virtual address space that is large enough to
 *        accomodate a buffer of size `bytes`.
 */
errval_t paging_alloc(struct paging_state *st, void **buf, size_t bytes)
{
    assert(st);

    //lock
    thread_mutex_lock_nested(&st->mutex);
    // note: may be recursive!

    errval_t err = SYS_ERR_OK;
    *buf = NULL;
    size_t actual_bytes = ROUND_UP(bytes, BASE_PAGE_SIZE);

    assert(!(st->allocated_region_end & MASK_T(lvaddr_t, BASE_PAGE_BITS)));
    // alignment of the heap to BASE_PAGE_SIZE is considered a given

    // new: check whether there is actually enough free VSpace available
    // note: it has to be done in a way to avoid overflows
    // thus: the entire calculations are done in uint64_t type
    uint64_t used = (uint64_t)st->allocated_region_end - (uint64_t)st->vspace_base_addr;
    uint64_t free = PAGING_MANAGED_MAX_SIZE - used;
    if (free < (uint64_t)bytes) {
        debug_printf("%s: not enough VSpace available to fulfill this request for %u KiB\n.",
                     __func__, bytes / 1024);
        err = LIB_ERR_VSPACE_MMU_AWARE_NO_SPACE;
        goto unlock;
    }

    *buf = (void*) st->allocated_region_end;
    st->allocated_region_end += actual_bytes;

    // all OK; fall through to unlock

unlock:
    thread_mutex_unlock(&st->mutex);
    return err;
}


/**
 * internal helper function, called by both paging_frame_attr functions
 * and also by the region page fault handler.
 */
static errval_t paging_map_fixed_attr_helper(struct paging_state *st, lvaddr_t vaddr,
        struct capref frame, size_t bytes, int flags)
{
    assert(st);

    // vaddr must be page aligned
    assert(!(vaddr & BASE_PAGE_MASK));

    thread_mutex_lock_nested(&st->mutex);
    // note: may be recursive here

    errval_t err = SYS_ERR_OK;

    if (BASE_PAGE_OFFSET(bytes)) {
        err = SYS_ERR_INVALID_SIZE;
        DEBUG_ERR(err, "Size invalid. Multiple of 4KB required.\n");
        goto unlock;
    }

    // frame mappings need to be page aligned
    if (ARM_PAGE_OFFSET(vaddr)) {
        err = LIB_ERR_VREGION_BAD_ALIGNMENT;
        DEBUG_ERR(err, "Wrong alignment for virtual address. Required alignment is 4KB.\n");
        goto unlock;
    }

    //inclusive end
    lvaddr_t vaddr_end = vaddr + bytes - 1;

    uint64_t l1_start_slot = ARM_L1_OFFSET(vaddr);
    uint64_t l1_end_slot = ARM_L1_OFFSET(vaddr_end);


    uint64_t frame_offset = 0;

    for (uint64_t i = l1_start_slot; i <= l1_end_slot; i++) {


        //check first if we need to allocate slot in l1 pagetable
        if (!st->l2_pts[i].mapped) {

            struct paging_l2pt_entry l2_pts_temp = st->l2_pts_reserve;
            st->l2_pts_reserve.cap = NULL_CAP;
            st->l2_pts_reserve.invokable = NULL_CAP;

            err = st->slot_alloc->alloc(st->slot_alloc, &l2_pts_temp.mapping);
            if (err_is_fail(err)) {
                err_push(err, LIB_ERR_VSPACE_MAP);
                DEBUG_ERR(err, "slot_alloc failed in paging_map_fixed_attr_helper\n");
                goto unlock;
            }

            // allocate a new level 2 page table if we had non in reserve
            if (capref_is_null(l2_pts_temp.cap)) {
                err = arml2_alloc(st, &l2_pts_temp);
                if (err_is_fail(err)) {
                    err_push(err, LIB_ERR_VSPACE_MAP);
                    DEBUG_ERR(err, "arml2_alloc failed in paging_map_fixed_attr_helper\n");
                    goto unlock;
                }
            }
            assert(!capref_is_null(l2_pts_temp.cap));
            assert(!capref_is_null(l2_pts_temp.invokable));

            if (!st->l2_pts[i].mapped) {
                // map the new level 2 page table into the level 1 page table
                err = vnode_map(st->l1_pagetable, l2_pts_temp.invokable, i, VREGION_FLAGS_NORIGHTS, 0, 1, l2_pts_temp.mapping);
                if (err_is_fail(err)) {
                    err_push(err, LIB_ERR_VSPACE_MAP);
                    DEBUG_ERR(err, "vnode_map failed in paging_map_fixed_attr_helper\n");
                    goto unlock;
                }
                l2_pts_temp.mapped = true;
                st->l2_pts[i] = l2_pts_temp;
            }
            else {
                st->l2_pts_reserve = l2_pts_temp;
            }
        }

        lvaddr_t l1_start_vaddr = i << ARM_L1_SECTION_BITS;
        lvaddr_t l1_end_vaddr = ((i + 1) << ARM_L1_SECTION_BITS ) - 1;


        uint64_t l2_start_slot = ARM_L2_OFFSET(l1_start_vaddr < vaddr ? vaddr : l1_start_vaddr);
        uint64_t l2_end_slot = ARM_L2_OFFSET(l1_end_vaddr < vaddr_end ? l1_end_vaddr : vaddr_end);

        struct capref vnode_arm_l2_mapping;
        err = st->slot_alloc->alloc(st->slot_alloc, &vnode_arm_l2_mapping);
        if (err_is_fail(err)) {
            err_push(err, LIB_ERR_VSPACE_MAP);
            DEBUG_ERR(err, "slot_alloc failed in paging_map_fixed_attr_helper\n");
            goto unlock;
        }

        uint64_t pte_count = l2_end_slot - l2_start_slot + 1;

        err = vnode_map(st->l2_pts[i].invokable, frame, l2_start_slot, vregion_flags_to_kpi_paging_flags(flags), frame_offset, pte_count, vnode_arm_l2_mapping);
        if (err_is_fail(err)) {
            err_push(err, LIB_ERR_VSPACE_MAP);
            DEBUG_ERR(err, "vnode_map failed in paging_map_fixed_attr_helper\n");
            goto unlock;
        }

        // bitmap book keeping and check
        lvaddr_t current_addr = vaddr + frame_offset;
        for (uint64_t j = 0; j < pte_count; j++) {
            assert(!check_bitmap(st->mapped_pages_bitmap, current_addr));

            set_bitmap(st->mapped_pages_bitmap, current_addr);
            st->mapped_pages_count++;

            if (current_addr < PAGING_RESERVED) {
                st->allocated_in_reserved_space += BASE_PAGE_SIZE;
            }

            current_addr += BASE_PAGE_SIZE;
        }

        // prepare next iteration
        frame_offset += (pte_count << BASE_PAGE_BITS);
    }

unlock:
    thread_mutex_unlock(&st->mutex);
    return err;
}

/**
 * \brief map a user provided frame, and return the VA of the mapped
 *        frame in `buf`.
 */
errval_t paging_map_frame_attr(struct paging_state *st, void **buf,
                               size_t bytes, struct capref frame,
                               int flags, void *arg1, void *arg2)
{
    assert(st);
    bytes = ROUND_UP(bytes, BASE_PAGE_SIZE);
    errval_t err = paging_alloc(st, buf, bytes);
    if (err_is_fail(err)) {
        return err;
    }
    return paging_map_fixed_attr_helper(st, (lvaddr_t)(*buf), frame, bytes, flags);
}

errval_t
slab_refill_no_pagefault(struct slab_allocator *slabs, struct capref frame, size_t minbytes)
{
    // Refill the two-level slot allocator without causing a page-fault
    size_t retbytes;
    errval_t err = frame_create(frame, minbytes, &retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slab_refill_no_pagefault(): failed to create frame.\n");
        return err;
    }
    void* addr;
    err = paging_map_frame_attr(&current, &addr, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slab_refill_no_pagefault(): failed to map frame.\n");
        return err;
    }
    slab_grow(slabs, addr, retbytes);
    return SYS_ERR_OK;
}

static bool vspace_free(struct paging_state *st, lvaddr_t vaddr, size_t size) {
    lvaddr_t base_addr = ALIGNED_BASE_ADDRESS(vaddr);
    lvaddr_t end_addr = base_addr + size;
    for (lvaddr_t addr = base_addr; addr < end_addr; addr += BASE_PAGE_SIZE) {
        if (check_bitmap(st->mapped_pages_bitmap, addr)) {
            return false;
        }
    }

    return true;
}

/**
 * \brief map a user provided frame at user provided VA.
 * DONE(M1): Map a frame assuming all mappings will fit into one L2 pt
 * DONE(M2): General case
 */
errval_t paging_map_fixed_attr(struct paging_state *st, lvaddr_t vaddr,
                               struct capref frame, size_t bytes, int flags)
{
    assert(st);

    // vaddr must be page aligned
    assert(!(vaddr & BASE_PAGE_MASK));

    // all fixed addr mappings must be below the managed space
    // to avoid interference with managed space above
    if (vaddr + bytes >= st->vspace_base_addr) {
        debug_printf("%s: requested region at %p of size %u KiB must be in reserved VSpace below %u KiB.\n", __func__, vaddr, bytes / 1024, st->vspace_base_addr / 1024);
        return LIB_ERR_VREGION_MAP_FIXED;
    }

    // all fixed addr mapping calls must be unique, i.e. not have been mapped before
    if (!vspace_free(st, vaddr, bytes)) {
        debug_printf("%s: requested region at %p of size %u KiB is not available.\n", __func__, vaddr, bytes / 1024);
        return LIB_ERR_VREGION_MAP_FIXED;
    }

    return paging_map_fixed_attr_helper(st, vaddr, frame, bytes, flags);
}


/**
 * \brief unmap region starting at address `region`.
 * NOTE: Implementing this function is optional.
 */
errval_t paging_unmap(struct paging_state *st, const void *region)
{
    assert(st);

    // TODO: implementation still lacking

    return SYS_ERR_OK;
}


//--- Exception Handler ----------------------------------------------------------------------------

void exception_handler(enum exception_type type, int subtype,
                       void *addr, arch_registers_state_t *regs,
                       arch_registers_fpu_state_t *fpuregs) {
    assert(regs);
    //fpu regs is in fact NULL
    //assert(fpuregs);

    errval_t err;

    switch(type) {
        case EXCEPT_PAGEFAULT:
            err = page_fault_handler(subtype, addr);
            if (err_is_fail(err)) {
                debug_stack_trace(regs, "Page fault exception handler: ERROR", "exception");
                USER_PANIC_ERR(err, "Page fault could not be handled. Abort.\n");
            }
            break;

        case EXCEPT_BREAKPOINT:
        case EXCEPT_SINGLESTEP:
        case EXCEPT_OTHER:
        case EXCEPT_NULL:
        default :
            debug_stack_trace(regs, "Default exception handler: ERROR", "exception");
            USER_PANIC("Cannot handle exception: %d/%d\n", type, subtype);
            break;
    }
    return;
}

/** \brief Page fault handler distinguishes at the moment 4 cases
 *          1. Nullpointer (and probable nullpointer exception)
 *          2. Access into addresses below VADDR_OFFSET, where ELF file sits.
 *          3. Access into addresses not belonging to any paging regions.
 *          4. If neither of those, call page fault handler for the respective paging region.
 */
errval_t page_fault_handler(int subtype, void *addr) {
    //for now we ignore subtype as this is always 0

    if (addr < (void *)PAGING_RESERVED) {

        if (addr == NULL) {
            DEBUG_ERR(LIB_ERR_VREGION_PAGEFAULT_HANDLER, "Nullpointer Exception\n");
            return LIB_ERR_VREGION_PAGEFAULT_HANDLER;
        }

        if (addr < (void *)BASE_PAGE_SIZE) {
            // note: low addresses but != NULL often also indicate null pointer exceptions:
            // see access into a field of a struct whose base address is NULL
            DEBUG_ERR(LIB_ERR_VREGION_PAGEFAULT_HANDLER, "Probable Nullpointer Exception at %p.\n", addr);
            return LIB_ERR_VREGION_PAGEFAULT_HANDLER;

        }

        lvaddr_t base_addr = ALIGNED_BASE_ADDRESS((lvaddr_t)addr);
        assert(base_addr <= (lvaddr_t)addr && (lvaddr_t)addr < (base_addr + BASE_PAGE_SIZE));

        if (check_bitmap(get_current_paging_state()->mapped_pages_bitmap, (lvaddr_t)addr)) {
            // a page fault in this protected region on an address that has already been mapped
            // -> must be some form of access violation
            DEBUG_ERR(LIB_ERR_VREGION_PAGEFAULT_HANDLER, "Access to already mapped privileged virtual space detected at %p. Violation of access rights.\n", addr);
            return LIB_ERR_VREGION_PAGEFAULT_HANDLER;
        }

        // has not been mapped before
        DEBUG_ERR(LIB_ERR_VREGION_PAGEFAULT_HANDLER, "Access to unmapped reserved virtual space detected at %p.\n", addr);
        return LIB_ERR_VREGION_PAGEFAULT_HANDLER;
    }

    if (addr >= (void *)PAGING_END) {
        DEBUG_ERR(LIB_ERR_VREGION_PAGEFAULT_HANDLER, "Access to kernel virtual space detected at %p\n", addr);
        return LIB_ERR_VREGION_PAGEFAULT_HANDLER;
    }

    struct paging_region *pr;

    pr = find_paging_region_by_address(&(get_current_paging_state()->paging_regions), (lvaddr_t) addr);

    if (!pr) {
        DEBUG_ERR(LIB_ERR_VREGION_PAGEFAULT_HANDLER, "Access into unmapped virtual address region at %p.\n", addr);
        return LIB_ERR_VREGION_PAGEFAULT_HANDLER;
    }

    errval_t err = paging_region_page_fault_handler(pr, (lvaddr_t) addr);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Paging region could not handle exception (e.g. guard page or error).\n");
        return err_push(err, LIB_ERR_VREGION_PAGEFAULT_HANDLER);
    }

    return SYS_ERR_OK;
}


//--- extended public interface --------------------------------------------------------------------

void paging_print_state(struct paging_state *st)
{
    assert(st);
    lvaddr_t used = st->allocated_region_end - st->vspace_base_addr;
    // prints split because text was too long for one debug_printf
    debug_printf("Paging state at %p:\n", st);
    debug_printf("System (below 1 GiB): %u KiB reserved, %u KiB allocated.\n", PAGING_RESERVED / 1024, st->allocated_in_reserved_space / 1024);
    debug_printf("Managed: %u KiB available, %u KiB used/reserved, %u regions.\n",
                 (unsigned)((PAGING_MANAGED_MAX_SIZE - used) / 1024),
                 (unsigned)(used / 1024),
                 st->paging_regions.size);
    debug_printf("%u actually backed pages with 4 KiB frames (%f percent of used space).\n",
                 (unsigned)st->mapped_pages_count,
                 100.0 * ((float)(st->mapped_pages_count) * BASE_PAGE_SIZE) /
                 ((float)(st->allocated_in_reserved_space + used)) );

    // note: the percent calculation currently uses only the effectively mapped part of the reserved
    // space below 1 GiB. But, of course we could also consider the entere reserved space < 1 GiB.
    // but then, I think, the percent values will just be very small.
    // TODO: compare and finally use what works best

    print_paging_region_list(&st->paging_regions, "");
}
