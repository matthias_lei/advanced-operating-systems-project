#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_SHELL_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_SHELL_P_

#include <aos/rpc_shared/aos_rpc_shared_shell.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_request_params(struct aos_rpc *rpc, struct shell_params **ret_params);
errval_t aos_rpc_remote_notify_termination(struct aos_rpc *rpc, domainid_t pid);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_SHELL_P_
