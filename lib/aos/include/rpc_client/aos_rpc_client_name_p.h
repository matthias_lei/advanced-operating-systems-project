/**
 * AOS 2017 Group C: individual projects: Nameserver
 * version 2017-11-30, pisch
 */

#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_NAME_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_NAME_P_

#include <aos/rpc_shared/aos_rpc_shared_name.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_name_service_register(struct aos_rpc *rpc, const struct service_registration_request *request,
                                       struct capref contact_cap,
                                       struct service_registration_reply **ret_reply);

errval_t aos_rpc_remote_name_service_deregister(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);

errval_t aos_rpc_remote_name_service_ping(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);

errval_t aos_rpc_remote_name_service_update(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store);

errval_t aos_rpc_remote_name_lookup_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t *ret_handle);

errval_t aos_rpc_remote_name_lookup_service_by_short_name(struct aos_rpc *rpc, const char *short_name, service_handle_t *ret_handle);

errval_t aos_rpc_remote_name_find_any_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count);

errval_t aos_rpc_remote_name_find_any_service_by_filter(struct aos_rpc *rpc, const struct serializable_key_value_store *filter,
                                                        service_handle_t **ret_handles, size_t *ret_count);

errval_t aos_rpc_remote_name_get_service_info(struct aos_rpc *rpc, service_handle_t handle, struct serializable_key_value_store **ret_info);

errval_t aos_rpc_remote_name_bind_service_lowlevel(struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id,
                                                   routing_info_t *ret_routing_info,
                                                   enum aos_rpc_interface *ret_interface,
                                                   size_t *ret_enumerator,
                                                   enum aos_rpc_chan_driver *ret_chan_type,
                                                   struct capref *ret_service_cap);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_NAME_P_
