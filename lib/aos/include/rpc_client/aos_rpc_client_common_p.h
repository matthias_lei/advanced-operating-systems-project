#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_COMMON_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_COMMON_P_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

errval_t aos_rpc_remote_send_number(struct aos_rpc *rpc, uintptr_t val);

errval_t aos_rpc_remote_send_string(struct aos_rpc *rpc, const char *string);

errval_t aos_rpc_remote_connect_with_service(struct capref service_cap,
                                             enum aos_rpc_interface interface,
                                             enum aos_rpc_chan_driver chan_type, routing_info_t routing,
                                             struct aos_rpc **ret_rpc, enum rpc_services_mode *ret_mode);

errval_t aos_rpc_remote_request_new_endpoint(struct aos_rpc *rpc, enum aos_rpc_chan_driver chan_type,
                                             struct capref *ret_cap);

errval_t aos_rpc_remote_benchmarking(struct aos_rpc *rpc, size_t buflen, size_t exp_ret_buflen);

#define AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT (struct aos_rpc_interface_common_vtable) { \
    .send_number = aos_rpc_remote_send_number,                                                 \
    .send_string = aos_rpc_remote_send_string,                                                 \
    .connect_with_service = NULL, /* exception: if a call comes through here, it's a bug! */   \
    .request_new_endpoint = aos_rpc_remote_request_new_endpoint,                               \
    .benchmarking = aos_rpc_remote_benchmarking                                                \
}

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_COMMON_P_
