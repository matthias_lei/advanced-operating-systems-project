#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_MEMORY_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_MEMORY_P_

#include <aos/rpc_shared/aos_rpc_shared_memory.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_get_ram_cap(struct aos_rpc *rpc, size_t size, size_t align,
                                    struct capref *retcap, size_t *ret_size);

errval_t aos_rpc_remote_load_ram(struct aos_rpc *rpc, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size);

errval_t aos_rpc_remote_memoryservice_register(struct aos_rpc *rpc, struct capref nameserver_cap);

errval_t aos_rpc_remote_get_ram_info(struct aos_rpc *rpc, size_t *ret_allocated, size_t *ret_free, size_t *ret_free_largest);


#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_MEMORY_P_
