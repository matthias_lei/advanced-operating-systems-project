#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_NETWORK_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_NETWORK_P_

#include <aos/rpc_shared/aos_rpc_shared_network.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_network_remote_accept(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_remote_bind(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
errval_t aos_rpc_network_remote_connect(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
errval_t aos_rpc_network_remote_getpeername(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_remote_getsockname(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_remote_getsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len);
errval_t aos_rpc_network_remote_listen(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int backlog);
errval_t aos_rpc_network_remote_recv(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags);
errval_t aos_rpc_network_remote_recvfrom(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_remote_recvmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags);
errval_t aos_rpc_network_remote_send(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags);
errval_t aos_rpc_network_remote_sendmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags);
errval_t aos_rpc_network_remote_sendto(struct aos_rpc *rpc ,ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
errval_t aos_rpc_network_remote_setsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len);
errval_t aos_rpc_network_remote_shutdown(struct aos_rpc *rpc,int *ret_value, int *err_no, int socket, int how);
errval_t aos_rpc_network_remote_socket(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol);
errval_t aos_rpc_network_remote_socketpair(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]);

#endif // #define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_NETWORK_P_