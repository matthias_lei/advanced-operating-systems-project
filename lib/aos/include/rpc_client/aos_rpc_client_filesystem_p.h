#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_FILESYSTEM_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_FILESYSTEM_P_

#include <aos/rpc_shared/aos_rpc_shared_filesystem.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_open(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
errval_t aos_rpc_remote_create(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
errval_t aos_rpc_remote_remove(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_remote_read(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read);
errval_t aos_rpc_remote_write(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written);
errval_t aos_rpc_remote_truncate(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes);
errval_t aos_rpc_remote_tell(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos);
errval_t aos_rpc_remote_stat(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info);
errval_t aos_rpc_remote_seek(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset);
errval_t aos_rpc_remote_close(struct aos_rpc *rpc, fs_filehandle_t inhandle);
errval_t aos_rpc_remote_opendir(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle);
errval_t aos_rpc_remote_dir_read_next(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info);
errval_t aos_rpc_remote_closedir(struct aos_rpc *rpc, fs_dirhandle_t inhandle);
errval_t aos_rpc_remote_mkdir(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_remote_rmdir(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_remote_mount(struct aos_rpc *rpc, const char *path, const char *uri);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_FILESYSTEM_P_
