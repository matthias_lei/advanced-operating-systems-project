#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_SERIAL_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_SERIAL_P_

#include <aos/rpc_shared/aos_rpc_shared_serial.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_serial_getchar(struct aos_rpc *rpc, char *retc);

errval_t aos_rpc_remote_serial_putchar(struct aos_rpc *rpc, char c);

errval_t aos_rpc_remote_serial_putstring(struct aos_rpc *rpc, const char *s, size_t len);

errval_t aos_rpc_remote_serial_get_window(struct aos_rpc *rpc, domainid_t window_id);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_SERIAL_P_
