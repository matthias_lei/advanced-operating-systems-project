/**
 * AOS 2017 Group C
 * version 2017-12-02, pisch
 */

#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_MONITOR_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_MONITOR_P_

#include <aos/rpc_shared/aos_rpc_shared_monitor.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_monitor_request_rpc_forward(struct aos_rpc *rpc, enum aos_rpc_interface interface,
                                                    size_t enumerator, uint32_t fid, routing_info_t routing,
                                                    struct aos_rpc_send_msg_env *send_env,
                                                    struct aos_rpc_recv_msg_env *recv_env);

errval_t aos_rpc_remote_monitor_spawn_domain(struct aos_rpc *rpc, const char *name, coreid_t core_id, int shell_id, domainid_t pid);

errval_t aos_rpc_remote_monitor_spawn_domain_from_file(struct aos_rpc *rpc, const char *path,
                                                       const char *command_line, coreid_t core_id,
                                                       int shell_id, domainid_t pid);

errval_t aos_rpc_remote_monitor_bootstrap_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, struct capref service_cap);

errval_t aos_rpc_remote_monitor_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, size_t enumerator,
                                              enum aos_rpc_chan_driver chan_type, struct capref service_cap);

errval_t aos_rpc_remote_monitor_get_bootstrap_pids(struct aos_rpc *rpc, struct aos_rpc_get_bootstrap_pids_res_payload *res);

errval_t aos_rpc_remote_monitor_get_multiboot_module_names(struct aos_rpc *rpc, char **name_list, size_t *list_len);

errval_t aos_rpc_remote_monitor_get_multiboot_module(struct aos_rpc *rpc, const char *name, struct capref *frame);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_MONITOR_P_
