#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_INTERMON_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_INTERMON_P_

#include <aos/rpc_shared/aos_rpc_shared_intermon.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_intermon_request_bootstrap_info(struct aos_rpc *rpc, coreid_t core_id, struct aos_intermon_bootstrap_res *info);

errval_t aos_rpc_remote_intermon_spawn(struct aos_rpc *rpc, const char *name, int shell_id, domainid_t pid);

errval_t aos_rpc_remote_intermon_forward_rpc_message(struct aos_rpc *rpc, struct aos_rpc_intermon_forward_req_payload *send,
                                                     struct aos_rpc_intermon_forward_res_payload *recv);


#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_INTERMON_P_
