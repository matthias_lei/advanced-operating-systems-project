#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_DEVICE_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_DEVICE_P_

#include <aos/rpc_shared/aos_rpc_shared_device.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_get_device_cap(struct aos_rpc *rpc,
                                       lpaddr_t paddr, size_t bytes,
                                       struct capref *frame);

errval_t aos_rpc_remote_get_irq_cap(struct aos_rpc *rpc, struct capref *cap);


#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_DEVICE_P_
