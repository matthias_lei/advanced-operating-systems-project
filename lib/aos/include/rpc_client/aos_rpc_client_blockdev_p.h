#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_BLOCKDEV_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_BLOCKDEV_P_

#include <aos/rpc_shared/aos_rpc_shared_blockdev.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_get_block_size(struct aos_rpc *rpc, size_t *block_size);
errval_t aos_rpc_remote_read_block(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen);
errval_t aos_rpc_remote_write_block(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_BLOCKDEV_P_
