#ifndef LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_PROCESS_P_
#define LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_PROCESS_P_

#include <aos/rpc_shared/aos_rpc_shared_process.h>
#include <rpc_client/aos_rpc_client_common_p.h>

errval_t aos_rpc_remote_process_spawn(struct aos_rpc *rpc, char *name,
                                      coreid_t core_id, domainid_t *newpid);

errval_t aos_rpc_remote_process_get_name(struct aos_rpc *rpc, domainid_t pid,
                                         char **name);

errval_t aos_rpc_remote_process_get_all_pids(struct aos_rpc *rpc,
                                             domainid_t **pids, size_t *pid_count);

errval_t aos_rpc_remote_process_spawn_from_file(struct aos_rpc *rpc, const char *path,
                                                const char *command_line, coreid_t core_id,
                                                int shell_id, domainid_t *newpid);
errval_t aos_rpc_remote_process_spawn_from_shell(struct aos_rpc *rpc, char *name,
                                                coreid_t core_id, int shell_id,
                                                domainid_t *newpid);

#endif // LIB_AOS_INCLUDE_RPC_CLIENT_AOS_RPC_CLIENT_PROCESS_P_
