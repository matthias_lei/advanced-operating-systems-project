#include <rpc_client/aos_rpc_client_common_p.h>

#include <aos/connect/flmp.h>
#include <aos/connect/lmp.h>
#include <aos/connect/ump.h>

void *aos_rpc_interface_vtables[] = {
    &aos_rpc_common_remote_vtable,
    &aos_rpc_monitor_remote_vtable,
    &aos_rpc_intermon_remote_vtable,
    &aos_rpc_name_remote_vtable,
    &aos_rpc_memory_remote_vtable,
    &aos_rpc_process_remote_vtable,
    &aos_rpc_serial_remote_vtable,
    &aos_rpc_device_remote_vtable,
    &aos_rpc_filesystem_remote_vtable,
    &aos_rpc_network_remote_vtable,
    &aos_rpc_blockdev_remote_vtable,
    &aos_rpc_shell_remote_vtable
};

STATIC_ASSERT(sizeof(aos_rpc_interface_vtables) / sizeof(void*) == AOS_RPC_FG_SIZE, "aos_rpc_interface_vtables[] size mismatch");

struct aos_rpc_interface_common_vtable aos_rpc_common_remote_vtable = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT;


errval_t aos_rpc_remote_send_number(struct aos_rpc *rpc, uintptr_t val) {
    assert(rpc);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &val, sizeof(val), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_COMMON_FID_SEND_NUMBER, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    // note: this should be checked separately from the error codes above
    // while the upper err is mainly transmission focused, this error code actually
    // comes as a feedback from the actual event handler
    // here, it may not make much, but in other stubs, these subtle diffrences can be quite relevant.
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // note: other stubs will have to extract payload in the reply, see .words[1] to .words[8]
    // here, we were just interested in getting the ACK

    return err; // should be SYS_ERR_OK by here...
}

errval_t aos_rpc_remote_send_string(struct aos_rpc *rpc, const char *string)
{
    assert(rpc);

    size_t string_len = strlen(string) + 1;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     string, string_len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_COMMON_FID_SEND_STRING, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    // note: this should be checked separately from the error codes above
    // while the upper err is mainly transmission focused, this error code actually
    // comes as a feedback from the actual event handler
    // here, it may not make much, but in other stubs, these subtle diffrences can be quite relevant.
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_connect_with_service(struct capref service_cap,
                                             enum aos_rpc_interface interface,
                                             enum aos_rpc_chan_driver chan_type, routing_info_t routing,
                                             struct aos_rpc **ret_rpc, enum rpc_services_mode *ret_mode)
{
    assert(ret_rpc);
    // ret_mode is optional

    errval_t err = SYS_ERR_OK;

    if (AOS_RPC_FG_SIZE <= interface) {
        // TODO: find better error code
        err = LIB_ERR_NOT_IMPLEMENTED;
        DEBUG_ERR(err, "requested interface number %zu is not available\n", (size_t)interface);
        return err;
    }

    void *vtable = aos_rpc_interface_vtables[interface];
    switch (chan_type) {
        case AOS_RPC_CHAN_DRIVER_LMP:
            err = setup_lmp_client(service_cap, interface, vtable, routing, ret_rpc);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "error in setup_lmp_client\n");
            }
            break;

        case AOS_RPC_CHAN_DRIVER_FLMP:
            err = setup_flmp_client(service_cap, interface, vtable, routing, ret_rpc);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "error in setup_flmp_client\n");
            }
            break;

        case AOS_RPC_CHAN_DRIVER_UMP:
            err = setup_ump_client(service_cap, interface, vtable, routing, ret_rpc);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "error in setup_ump_client\n");
            }
            break;

        case AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP:
            // do nothing
            *ret_rpc = NULL;
            break;

        default:
            err = LIB_ERR_NOT_IMPLEMENTED;
            DEBUG_ERR(err, "unknown chan driver type\n");
            return err;
    }

    // ret mode not yet implemented -> hardcoded at the moment
    // TODO: add mode transfer
    if (ret_mode) {
        *ret_mode = RPC_SERVICES_MODE_DISTRIBUTED;
    }

    return err;
}

errval_t aos_rpc_remote_request_new_endpoint(struct aos_rpc *rpc, enum aos_rpc_chan_driver chan_type,
                                             struct capref *ret_cap)
{
    assert(rpc);
    assert(ret_cap);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_common_request_new_endpoint_req_payload req;
    req.chan_type = chan_type;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req, sizeof(req), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_FRAME);

    //AOS_RPC_EXPECT_NO_PAYLOAD

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_COMMON_FID_REQUEST_NEW_ENDPOINT, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        goto unroll_none;
    }

    *ret_cap = context.recv_env.cap;

    // all OK
    return SYS_ERR_OK;

unroll_none:
    if (context.recv_env.buf) {
        free(context.recv_env.buf);
    }
    return err;
}

errval_t aos_rpc_remote_benchmarking(struct aos_rpc *rpc, size_t buflen,
                                     size_t exp_ret_buflen)
{
    assert(rpc);

    errval_t err = SYS_ERR_OK;

    size_t payload_size = sizeof(struct aos_rpc_common_benchmarking_req_payload)
                        + buflen * sizeof(char);
    struct aos_rpc_common_benchmarking_req_payload *payload = calloc(payload_size, sizeof(char));
    if (!payload) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "calloc failed.\n");
        return err;
    }

    payload->size = buflen;
    payload->exp_buflen = exp_ret_buflen;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     payload, payload_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_COMMON_FID_BENCHMARKING, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        free(payload);
        return err;
    }

    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        free(payload);
        return err;
    }

    size_t res_payload_size = sizeof(struct aos_rpc_common_benchmarking_res_payload)
                             + exp_ret_buflen;

    assert(context.recv_env.buflen == res_payload_size);

    free(payload);
    free(context.recv_env.buf);

    return SYS_ERR_OK;
}

errval_t aos_rpc_send_number(struct aos_rpc *rpc, uintptr_t val)
{
    assert(rpc);
    struct aos_rpc_interface_common_vtable *vtable = rpc->interface_vtable;
    return vtable->send_number(rpc, val);
}

errval_t aos_rpc_send_string(struct aos_rpc *rpc, const char *string)
{
    assert(rpc);
    struct aos_rpc_interface_common_vtable *vtable = rpc->interface_vtable;
    return vtable->send_string(rpc, string);
}

errval_t aos_rpc_connect_with_service(struct capref service_cap,
                                      enum aos_rpc_interface interface,
                                      enum aos_rpc_chan_driver chan_type, routing_info_t routing,
                                      struct aos_rpc **ret_rpc, enum rpc_services_mode *ret_mode)
{
    assert(ret_rpc);
    // ret_mode is optional

    // this is an exception case, where the vtable cannot yet be used
    return aos_rpc_remote_connect_with_service(service_cap, interface, chan_type, routing, ret_rpc, ret_mode);
}

errval_t aos_rpc_request_new_endpoint(struct aos_rpc *rpc, enum aos_rpc_chan_driver chan_type,
                                      struct capref *ret_cap)
{
    assert(rpc);
    struct aos_rpc_interface_common_vtable *vtable = rpc->interface_vtable;
    return vtable->request_new_endpoint(rpc, chan_type, ret_cap);
}

errval_t aos_rpc_benchmarking(struct aos_rpc *rpc, size_t buflen, size_t exp_ret_buflen)
{
    assert(rpc);
    struct aos_rpc_interface_common_vtable *vtable = rpc->interface_vtable;
    return vtable->benchmarking(rpc, buflen, exp_ret_buflen);
}
