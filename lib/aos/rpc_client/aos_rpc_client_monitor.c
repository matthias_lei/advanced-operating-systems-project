/**
 * AOS 2017 Group C
 * version 2017-12-02, pisch
 */

#include <rpc_client/aos_rpc_client_monitor_p.h>
#include <aos/rpc_shared/aos_rpc_shared_intermon.h>
#include <aos/rpc_shared/aos_rpc_shared_memory.h>

struct aos_rpc_interface_monitor_vtable aos_rpc_monitor_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .request_rpc_forward = aos_rpc_remote_monitor_request_rpc_forward,
    .spawn_domain = aos_rpc_remote_monitor_spawn_domain,
    .spawn_domain_from_file = aos_rpc_remote_monitor_spawn_domain_from_file,
    .bootstrap_offer_service = aos_rpc_remote_monitor_bootstrap_offer_service,
    .offer_service = aos_rpc_remote_monitor_offer_service,
    .get_bootstrap_pids = aos_rpc_remote_monitor_get_bootstrap_pids,
    .get_multiboot_module_names = aos_rpc_remote_monitor_get_multiboot_module_names,
    .get_multiboot_module = aos_rpc_remote_monitor_get_multiboot_module
};

//--- _remote_ functions: RPC packaging and unpackaging --------------------------------------------

/**
 * NOTE: this function never is called directy. It is called from within
 * generic_send_and_recv() if it detects a need for re-routing.
 * recv_env must have an allocated buffer or proper size in case any payload is
 * expected with the forwarded request. NO
 */
errval_t aos_rpc_remote_monitor_request_rpc_forward(struct aos_rpc *rpc, enum aos_rpc_interface interface,
                                                    size_t enumerator, uint32_t fid, routing_info_t routing,
                                                    struct aos_rpc_send_msg_env *send_env,
                                                    struct aos_rpc_recv_msg_env *recv_env)
{
    assert(rpc);
    assert(send_env);
    assert(recv_env);

    errval_t err = SYS_ERR_OK;

    // avoid any memory allocation during intermon routing
    // note: this limits the size of messages for intermon routing
    // However, this is OK. There are no large RPC messages associated
    // with cap transfers in all our RPC calls.
    // note: both MUST be aligned to uintptr_t
    uintptr_t memory_buffer_req[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];
    uintptr_t memory_buffer_res[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1];

    // prepare message
    size_t env_size = sizeof(struct aos_rpc_send_msg_env) + send_env->buflen;
    size_t size = sizeof(struct aos_rpc_intermon_forward_req_payload) + env_size;
    struct aos_rpc_intermon_forward_req_payload *req;
    struct aos_rpc_intermon_forward_res_payload *res;

    if (DEFAULT_RPC_BUFFER_SIZE < size) {
        debug_printf("%s: prepared special buffer for AOS_RPC_MEMORY_FID_GET_RAM_CAP is not large enough. need %zu\n", __func__, size);
        return LIB_ERR_MALLOC_FAIL;
    }
    req = (void *)memory_buffer_req;
    res = (void *)memory_buffer_res;
    memset(req, 0, sizeof(memory_buffer_req));
    memset(res, 0, sizeof(memory_buffer_res));

    req->interface = interface;
    req->enumerator = enumerator;
    req->fid = fid;
    req->routing = routing;
    req->send_cap_type = send_env->cap_type;
    req->recv_cap_type = recv_env->expected_cap_type;
    req->env_size = env_size;
    memcpy(req->env, send_env, sizeof(struct aos_rpc_send_msg_env));
    if (0 < send_env->buflen) {
        memcpy(req->env + sizeof(struct aos_rpc_send_msg_env), send_env->buf, send_env->buflen);
    }

    if (send_env->cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE) {
        // note: only limited types of caps can be transferred, of course
        struct frame_identity info;
        // works also for RAM
        err = frame_identify(send_env->cap, &info);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "frame_identify for cap failed\n");
            return err;
        }
        req->cap_base = info.base;
        req->cap_size = info.bytes;
    }

    // sending
    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req, size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     res, DEFAULT_RPC_BUFFER_SIZE, recv_env->expected_cap_type);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_REQUEST_RPC_FORWARD, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // NOTE: deliberately NO check of the embedded return code from RPC
    // it is just forwarded to recipient

    // handle return payload
    // NOTE: the returning capref has already been forged in the monitor and is
    // in the received payload
    // the question here is mainly whether memory must be allocated to be freed for the caller
    // (see expectations) or whether a plain memcpy is enough.
    // please note: the received buffer here cannot just be passed on because the pointer
    // would not fit to be freed later.
    //
    // technical note: NO malloc() is allowed on the entire journey over the seas
    // however, being back now, it is allowed if needed.
    // Nevertheless: good RPC remote stubs requiring a capability transfer (and thus rerouting)
    // have small payload only and also fixed size / pre-allocated recv buffers
    // This handling here is for full compatibility only

    bool has_cap = recv_env->expected_cap_type != AOS_RPC_INTERMON_CAPABILITY_NONE;
    res = context.recv_env.buf;
    struct aos_rpc_recv_msg_env *env = (void *)res->env;
    env->buf = (char *)env + sizeof(struct aos_rpc_recv_msg_env);
    recv_env->header = env->header;

    if (has_cap) {
        recv_env->cap = context.recv_env.cap;
    }

    if (0 < env->buflen) {
        if (!recv_env->buf) {
            if (recv_env->buflen == AOS_RPC_EXPECT_PAYLOAD) {
                // malloc allowed here
                recv_env->buf = malloc(env->buflen);
                if (!recv_env->buf) {
                    err = LIB_ERR_MALLOC_FAIL;
                    DEBUG_ERR(err, "payload expected; malloc() failed\n");
                    return err;
                }
            }
            else {
                err = LIB_ERR_SHOULD_NOT_GET_HERE;
                DEBUG_ERR(err, "no payload expected but received\n");
                return err;
            }
        }
        else {
            if (recv_env->buflen < env->buflen) {
                err = LIB_ERR_SHOULD_NOT_GET_HERE;
                DEBUG_ERR(err, "available buffer %zu B too small for received payload %zu B\n",
                          recv_env->buflen, env->buflen);
                return err;
            }
        }

        memcpy(recv_env->buf, env->buf, env->buflen);
    }
    recv_env->buflen = env->buflen;

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_monitor_spawn_domain(struct aos_rpc *rpc, const char *name, coreid_t core_id, int shell_id, domainid_t pid)
{
    assert(rpc);
    assert(name);

    size_t buf_len = strlen(name) + 1 + sizeof(struct aos_rpc_spawn_domain_req_payload);
    struct aos_rpc_spawn_domain_req_payload *req = calloc(1, buf_len);
    if (!req) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req->core_id = core_id;
    req->pid = pid;
    req->shell_id = shell_id;
    strcpy(req->name, name);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req, buf_len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_SPAWN_DOMAIN, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // no payload
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_monitor_spawn_domain_from_file(struct aos_rpc *rpc, const char *path,
                                                       const char *command_line, coreid_t core_id,
                                                       int shell_id, domainid_t pid)
{
    assert(rpc);
    assert(path);
    assert(command_line);

    size_t path_len = strlen(path) + 1;
    size_t command_line_len = strlen(command_line) + 1;
    size_t buf_len = sizeof(struct aos_rpc_spawn_domain_from_file_req_payload) + path_len + command_line_len;
    struct aos_rpc_spawn_domain_from_file_req_payload *req = calloc(1, buf_len);
    if (!req) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req->core_id = core_id;
    req->pid = pid;
    strcpy(req->data, path);
    strcpy(req->data + path_len, command_line);
    req->shell_id = shell_id;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req, buf_len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_SPAWN_DOMAIN_FROM_FILE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // no payload
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_monitor_bootstrap_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, struct capref service_cap)
{
    assert(rpc);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &interface, sizeof(enum aos_rpc_interface), AOS_RPC_INTERMON_CAPABILITY_FRAME,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    context.send_env.cap = service_cap;

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_BOOTSTRAP_OFFER_SERVICE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // no payload
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_monitor_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, size_t enumerator,
                                              enum aos_rpc_chan_driver chan_type, struct capref service_cap)
{
    assert(rpc);

    struct aos_rpc_offer_service_req_payload payload = {
        .interface = interface,
        .enumerator = enumerator,
        .chan_type = chan_type
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &payload, sizeof(payload), AOS_RPC_INTERMON_CAPABILITY_FRAME,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    context.send_env.cap = service_cap;

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_OFFER_SERVICE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // no payload
    return SYS_ERR_OK;
}


errval_t aos_rpc_remote_monitor_get_bootstrap_pids(struct aos_rpc *rpc, struct aos_rpc_get_bootstrap_pids_res_payload *res)
{
    assert(rpc);
    assert(res);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     NULL, 0, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     res, sizeof(*res), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_GET_BOOTSTRAP_PIDS, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // payload already assigned
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_monitor_get_multiboot_module_names(struct aos_rpc *rpc, char **name_list, size_t *list_len)
{
    assert(rpc);
    assert(name_list);

    *name_list = NULL;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     NULL, 0, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_GET_MULTIBOOT_MODULE_NAMES, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    *name_list = context.recv_env.buf;
    *list_len = context.recv_env.buflen;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_monitor_get_multiboot_module(struct aos_rpc *rpc, const char *name, struct capref *frame)
{
    assert(rpc);
    assert(name);

    *frame = NULL_CAP;

    size_t name_len = strlen(name) + 1;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     name, name_len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_FRAME);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MONITOR_FID_GET_MULTIBOOT_MODULE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    *frame = context.recv_env.cap;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}


//--- aos_rpc_ functions: implement the aos_rpc.h interface ----------------------------------------

/**
 * \brief Returns the channel to the name service
 */
static struct aos_rpc *monitor_rpc = NULL;

struct aos_rpc *aos_rpc_get_monitor_channel(void)
{
    if (!monitor_rpc) {
        monitor_rpc = get_init_rpc();
    }

    // no lookup option here, of course

    return monitor_rpc;
}

errval_t aos_rpc_monitor_request_rpc_forward(struct aos_rpc *rpc, enum aos_rpc_interface interface,
                                             size_t enumerator, uint32_t fid, routing_info_t routing,
                                             struct aos_rpc_send_msg_env *send_env,
                                             struct aos_rpc_recv_msg_env *recv_env)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->request_rpc_forward(rpc, interface, enumerator, fid, routing, send_env, recv_env);
}


errval_t aos_rpc_monitor_spawn_domain(struct aos_rpc *rpc, const char *name, coreid_t core_id, int shell_id, domainid_t pid)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->spawn_domain(rpc, name, core_id, shell_id, pid);
}

errval_t aos_rpc_monitor_spawn_domain_from_file(struct aos_rpc *rpc, const char *path,
                                                const char *command_line, coreid_t core_id,
                                                int shell_id, domainid_t pid)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->spawn_domain_from_file(rpc, path, command_line, core_id, shell_id, pid);
}

errval_t aos_rpc_monitor_bootstrap_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, struct capref service_cap)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->bootstrap_offer_service(rpc, interface, service_cap);
}

errval_t aos_rpc_monitor_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, size_t enumerator,
                                       enum aos_rpc_chan_driver chan_type, struct capref service_cap)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->offer_service(rpc, interface, enumerator, chan_type, service_cap);
}


errval_t aos_rpc_monitor_get_bootstrap_pids(struct aos_rpc *rpc, struct aos_rpc_get_bootstrap_pids_res_payload *res)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->get_bootstrap_pids(rpc, res);
}

errval_t aos_rpc_monitor_get_multiboot_module_names(struct aos_rpc *rpc, char **name_list, size_t *list_len)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->get_multiboot_module_names(rpc, name_list, list_len);
}

errval_t aos_rpc_monitor_get_multiboot_module(struct aos_rpc *rpc, const char *name, struct capref *frame)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MONITOR);
    struct aos_rpc_interface_monitor_vtable *vtable = rpc->interface_vtable;
    return vtable->get_multiboot_module(rpc, name, frame);
}
