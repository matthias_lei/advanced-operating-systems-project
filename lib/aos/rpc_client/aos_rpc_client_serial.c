#include <rpc_client/aos_rpc_client_serial_p.h>

struct aos_rpc_interface_serial_vtable aos_rpc_serial_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .get_char = aos_rpc_remote_serial_getchar,
    .put_char = aos_rpc_remote_serial_putchar,
    .put_string = aos_rpc_remote_serial_putstring,
    .get_window = aos_rpc_remote_serial_get_window
};

errval_t aos_rpc_remote_serial_getchar(struct aos_rpc *rpc, char *retc)
{
    assert(rpc);
    // DONE implement functionality to request a character from
    // the serial driver.

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &rpc->domain_id, sizeof(rpc->domain_id), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     retc, sizeof(*retc), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_SERIAL_FID_GETCHAR, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(*retc));

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_serial_putchar(struct aos_rpc *rpc, char c)
{
    assert(rpc);
    // DONE implement functionality to send a character to the
    // serial port.
    struct aos_rpc_serial_putchar_req_payload req_payload;
    req_payload.domain_id = rpc->domain_id;
    req_payload.c = c;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(struct aos_rpc_serial_putchar_req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_SERIAL_FID_PUTCHAR, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        return err;
    }

    return SYS_ERR_OK;
}

//--- Group C: extended interface ------------------------------------------------------------------

errval_t aos_rpc_remote_serial_putstring(struct aos_rpc *rpc, const char *s, size_t len)
{
    assert(rpc);
    assert(s);

    errval_t err = SYS_ERR_OK;
    size_t payload_size = sizeof(struct aos_rpc_serial_putstring_req_payload) + len;
    struct aos_rpc_serial_putstring_req_payload *req_payload = malloc(payload_size);
    if (!req_payload) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "malloc() failed\n");
        return err;
    }
    req_payload->domain_id = rpc->domain_id;
    req_payload->len = len;
    memcpy(req_payload->s, s, len);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, payload_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_SERIAL_FID_PUTSTRING, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        return err;
    }

    free(req_payload);

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_serial_get_window(struct aos_rpc *rpc, domainid_t window_id) {
    assert(rpc);

    errval_t err = SYS_ERR_OK;
    struct aos_rpc_serial_get_window_req_payload req_payload;

    req_payload.domain_id = rpc->domain_id;
    req_payload.window_id = window_id;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                    &req_payload, sizeof(req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_SERIAL_FID_GET_WINDOW, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_block()\n");
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        return err;
    }

    return SYS_ERR_OK;
}


/**
 * \brief Returns the channel to the serial console
 */
static struct aos_rpc *serial_rpc = NULL;

struct aos_rpc *aos_rpc_get_serial_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&serial_rpc, blocking, -1, get_serial_rpc, set_serial_rpc, "/core/serial");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return serial_rpc;
}

errval_t aos_rpc_serial_getchar(struct aos_rpc *rpc, char *retc)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_SERIAL);
    struct aos_rpc_interface_serial_vtable *vtable = rpc->interface_vtable;
    return vtable->get_char(rpc, retc);
}

errval_t aos_rpc_serial_putchar(struct aos_rpc *rpc, char c)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_SERIAL);
    struct aos_rpc_interface_serial_vtable *vtable = rpc->interface_vtable;
    return vtable->put_char(rpc, c);
}

errval_t aos_rpc_serial_putstring(struct aos_rpc *rpc, const char *s, size_t len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_SERIAL);
    struct aos_rpc_interface_serial_vtable *vtable = rpc->interface_vtable;
    return vtable->put_string(rpc, s, len);
}

errval_t aos_rpc_serial_get_window(struct aos_rpc *rpc, domainid_t window_id) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_SERIAL);
    struct aos_rpc_interface_serial_vtable *vtable = rpc->interface_vtable;
    return vtable->get_window(rpc, window_id);
}
