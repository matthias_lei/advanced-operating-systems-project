/**
 * AOS 2017 Group C: individual projects: Nameserver
 * version 2017-11-30, pisch
 */

#include <rpc_client/aos_rpc_client_name_p.h>

#include <aos/connect/all.h>
#include <aos/deferred.h>

#include <stdlib.h>

struct aos_rpc_interface_name_vtable aos_rpc_name_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .service_register = aos_rpc_remote_name_service_register,
    .service_deregister = aos_rpc_remote_name_service_deregister,
    .service_ping = aos_rpc_remote_name_service_ping,
    .service_update = aos_rpc_remote_name_service_update,
    .lookup_service_by_path = aos_rpc_remote_name_lookup_service_by_path,
    .lookup_service_by_short_name = aos_rpc_remote_name_lookup_service_by_short_name,
    .find_any_service_by_path = aos_rpc_remote_name_find_any_service_by_path,
    .find_any_service_by_filter = aos_rpc_remote_name_find_any_service_by_filter,
    .get_service_info = aos_rpc_remote_name_get_service_info,
    .bind_service_lowlevel = aos_rpc_remote_name_bind_service_lowlevel
};

//--- _remote_ functions: RPC packaging and unpackaging --------------------------------------------

errval_t aos_rpc_remote_name_service_register(struct aos_rpc *rpc, const struct service_registration_request *request,
                                              struct capref contact_cap,
                                              struct service_registration_reply **ret_reply)
{
    assert(rpc);
    assert(request);
    assert(ret_reply);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_name_service_register_req_payload *payload = service_registration_request_serialize(request);
    if (!payload) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_none;
    }

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     payload, payload->buf_size, AOS_RPC_INTERMON_CAPABILITY_FRAME,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    // special embedding of the capref
    context.send_env.cap = contact_cap;

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_SERVICE_REGISTER, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_request_serialize;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        goto unroll_request_serialize;
    }

    // note: the recv buffer will be freed by the generic event handler

    // just to be sure
    if (!context.recv_env.buf) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_request_serialize;
    }

    *ret_reply = service_registration_reply_deserialize(context.recv_env.buf);
    if (!*ret_reply) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_request_serialize;
    }

    // all OK: fall through (identical cleanup needed)
    err = SYS_ERR_OK;

unroll_request_serialize:
    free(payload);
unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_service_deregister(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key)
{
    assert(rpc);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_name_handle_key_req_payload payload = {
        .handle = handle,
        .registration_key = registration_key
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &payload, sizeof(payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_SERVICE_DEREGISTER, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        goto unroll_none;
    }

    // all OK
    err = SYS_ERR_OK;
    return err;

unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_service_ping(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key)
{
    assert(rpc);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_name_handle_key_req_payload payload = {
        .handle = handle,
        .registration_key = registration_key
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &payload, sizeof(payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_SERVICE_PING, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        goto unroll_none;
    }

    // all OK
    err = SYS_ERR_OK;
    return err;

unroll_none:
    return err;
}


errval_t aos_rpc_remote_name_service_update(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store)
{
   assert(rpc);

    errval_t err = SYS_ERR_OK;

    struct serialized_key_value_store *serialized_store = serialize_kv_store(kv_store);
    if (!serialized_store) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_none;
    }

    struct aos_rpc_name_update_req_payload *payload = malloc(sizeof(struct serialized_key_value_store) + serialized_store->buf_size);
    if (!payload) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_serialized;
    }

    // unfortunately, a bit of copying around
    payload->handle = handle;
    payload->registration_key = registration_key;
    memcpy(payload->serialized_store, serialized_store, serialized_store->buf_size);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     payload, sizeof(struct serialized_key_value_store) + serialized_store->buf_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_SERVICE_UPDATE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_payload;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        goto unroll_payload;
    }

    // all OK
    err = SYS_ERR_OK;

unroll_payload:
    free(payload);
unroll_serialized:
    free(serialized_store);
unroll_none:
    return err;
}


errval_t aos_rpc_remote_name_lookup_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t *ret_handle)
{
    assert(rpc);
    assert(abs_path_prefix);
    assert(ret_handle);

    errval_t err = SYS_ERR_OK;

    // the string is directly the payload; always sent including terminator \0.

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     abs_path_prefix, strlen(abs_path_prefix) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     ret_handle, sizeof(*ret_handle), AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_LOOKUP_SERVICE_BY_PATH, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        if (err != LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
            DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        }
        goto unroll_none;
    }

    // all OK: fall through (identical cleanup needed)
    err = SYS_ERR_OK;

unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_lookup_service_by_short_name(struct aos_rpc *rpc, const char *short_name, service_handle_t *ret_handle)
{
    assert(rpc);
    assert(short_name);
    assert(ret_handle);

    errval_t err = SYS_ERR_OK;

    // the string is directly the payload; always sent including terminator \0.

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     short_name, strlen(short_name) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     ret_handle, sizeof(*ret_handle), AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_LOOKUP_SERVICE_BY_SHORT_NAME, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        if (err != LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
            DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        }
        goto unroll_none;
    }

    // all OK: fall through (identical cleanup needed)
    err = SYS_ERR_OK;

unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_find_any_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count)
{
    assert(rpc);
    assert(abs_path_prefix);
    assert(ret_handles);
    assert(ret_count);

    errval_t err = SYS_ERR_OK;

    *ret_handles = NULL;
    *ret_count = 0;

    // the string is directly the payload; always sent including terminator \0.

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     abs_path_prefix, strlen(abs_path_prefix) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_FIND_ANY_SERVICE_BY_PATH, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        if (err != LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
            DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        }
        goto unroll_none;
    }

    assert(context.recv_env.buflen >= sizeof(struct aos_rpc_name_find_res_payload));
    struct aos_rpc_name_find_res_payload *reply = context.recv_env.buf;

    // the user expects a malloced result -> copy
    service_handle_t *handles = malloc(reply->count * sizeof(service_handle_t));
    if (!handles) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_recv_buf;
    }

    *ret_handles = handles;
    memcpy(*ret_handles, reply->handles, reply->count * sizeof(service_handle_t));
    *ret_count = reply->count;

    // all OK: fall through
    err = SYS_ERR_OK;

unroll_recv_buf:
    if (context.recv_env.buf) {
        free(context.recv_env.buf);
    }
unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_find_any_service_by_filter(struct aos_rpc *rpc, const struct serializable_key_value_store *filter,
                                                        service_handle_t **ret_handles, size_t *ret_count)
{
    assert(rpc);
    assert(filter);
    assert(ret_handles);
    assert(ret_count);

    errval_t err = SYS_ERR_OK;

    *ret_handles = NULL;
    *ret_count = 0;

    struct serialized_key_value_store *serialized_filter = serialize_kv_store(filter);
    if (!serialized_filter) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "Could not serialize the KV store\n");
        goto unroll_none;
    }

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     serialized_filter, serialized_filter->buf_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_FIND_ANY_SERVICE_BY_FILTER, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        if (err != LIB_ERR_NAMESERVICE_UNKNOWN_NAME) {
            DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        }
        goto unroll_none;
    }

    assert(context.recv_env.buflen >= sizeof(struct aos_rpc_name_find_res_payload));
    struct aos_rpc_name_find_res_payload *reply = context.recv_env.buf;

    // the user expects a malloced result -> copy
    service_handle_t *handles = malloc(reply->count * sizeof(service_handle_t));
    if (!handles) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_recv_buf;
    }

    *ret_handles = handles;
    memcpy(*ret_handles, reply->handles, reply->count * sizeof(service_handle_t));
    *ret_count = reply->count;

    // all OK: fall through
    err = SYS_ERR_OK;

unroll_recv_buf:
    if (context.recv_env.buf) {
        free(context.recv_env.buf);
    }
unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_get_service_info(struct aos_rpc *rpc, service_handle_t handle, struct serializable_key_value_store **ret_info)
{
    assert(rpc);
    assert(ret_info);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &handle, sizeof(handle), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_GET_SERVICE_INFO, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        goto unroll_none;
    }

    assert(context.recv_env.buflen >= sizeof(struct serialized_key_value_store));
    struct serialized_key_value_store *serialized_store = context.recv_env.buf;

    *ret_info = deserialize_kv_store(serialized_store, 0);
    if(!(*ret_info)) {
        err = LIB_ERR_MALLOC_FAIL;
        DEBUG_ERR(err, "error while deserializing the KV store.\n");
        goto unroll_buffer;
    }

    // note: the serialized buffer is needed inside of the deserialized serializeable store
    // it will be freed upon destruction of the generated serializable store

    // all OK
    err = SYS_ERR_OK;
    return err;

unroll_buffer:
    free(serialized_store);
unroll_none:
    return err;
}

errval_t aos_rpc_remote_name_bind_service_lowlevel(struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id,
                                                   routing_info_t *ret_routing_info,
                                                   enum aos_rpc_interface *ret_interface,
                                                   size_t *ret_enumerator,
                                                   enum aos_rpc_chan_driver *ret_chan_type,
                                                   struct capref *ret_service_cap)
{
    assert(rpc);
    assert(ret_routing_info);
    assert(ret_chan_type);
    assert(ret_service_cap);

    errval_t err = SYS_ERR_OK;

    struct aos_rpc_name_bind_service_lowlevel_req_payload req_payload = {
        .handle = handle,
        .core_id = core_id
    };
    struct aos_rpc_name_bind_service_lowlevel_res_payload res_payload;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_payload, sizeof(res_payload), AOS_RPC_INTERMON_CAPABILITY_FRAME);

    err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NAME_FID_BIND_SERVICE_LOWLEVEL, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        goto unroll_none;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        goto unroll_none;
    }

    *ret_routing_info = res_payload.routing_info;
    *ret_interface = res_payload.interface;
    *ret_enumerator = res_payload.enumerator;
    *ret_chan_type = res_payload.chan_type;
    *ret_service_cap = context.recv_env.cap;

    /* // debugging
    debug_printf("%s: routing %x, interface %u, chan type %u\n",
                 __func__, *ret_routing_info, *ret_interface, *ret_chan_type);
    //*/

    // all OK: fall through (identical cleanup needed)
    err = SYS_ERR_OK;

unroll_none:
    return err;
}


//--- aos_rpc_ functions: implement the aos_rpc.h interface ----------------------------------------

/**
 * \brief Returns the channel to the name service
 */
static struct aos_rpc *name_rpc = NULL;

struct aos_rpc *aos_rpc_get_name_channel(void)
{
    if (!name_rpc) {
        name_rpc = get_name_rpc();
    }

    // no lookup option here, of course

    return name_rpc;
}

//--- server / service side: service comes first
errval_t aos_rpc_name_service_register(struct aos_rpc *rpc, const struct service_registration_request *request,
                                       struct capref contact_cap,
                                       struct service_registration_reply **ret_reply)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->service_register(rpc, request, contact_cap, ret_reply);
}

errval_t aos_rpc_service_register(const char *abs_path, const char *short_name_prefix,
                                  enum aos_rpc_interface interface, bool high_bandwidth_service,
                                  struct serializable_key_value_store *kv_store,
                                  waitlist_event_handler_t event_handler,
                                  struct service_registration_reply **ret_reply)
{
    assert(abs_path);
    assert(short_name_prefix);
    assert(kv_store);
    assert(ret_reply);

    struct aos_rpc *rpc = aos_rpc_get_name_channel();
    if (!rpc) {
        debug_printf("%s: name RPC channel not found.\n", __func__);
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    // create new service endpoint
    struct capref service_cap;
    errval_t err = slot_alloc(&service_cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in slot_alloc()\n");
        return err;
    }

    #define NAME_CORE 0
    bool same_core = NAME_CORE == disp_get_core_id();

    struct ump_service_create_data data;
    if (same_core) {
        err = setup_lmp_service(service_cap, MKCLOSURE(event_handler, NULL));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in setup_lmp_service()\n");
            return err;
        }
    }
    else {
        // frame needs to be allocated before calling the setup
        size_t size;
        err = frame_alloc(&service_cap, BASE_PAGE_SIZE, &size);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Cannot allocate frame\n");
            return err;
        }

        err = setup_ump_service_part1(service_cap, &data);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in setup_ump_service_part1()\n");
            return err;
        }
    }

    // register
    struct service_registration_request request = {
        .core_id = disp_get_core_id(),
        .interface = interface,
        .contact_chan_type = same_core ? AOS_RPC_CHAN_DRIVER_LMP : AOS_RPC_CHAN_DRIVER_UMP,
        .high_bandwidth_service = high_bandwidth_service,
        .abs_path = (char *)abs_path,
        .short_name_prefix = (char *)short_name_prefix,
        .kv_store = kv_store
    };

    struct service_registration_reply *reply;
    err = aos_rpc_name_service_register(rpc, &request, service_cap, &reply);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_name_service_register() failed\n");
        return err;
    }
    *ret_reply = reply;

    if (!same_core) {
        err = setup_ump_service_part2(&data, MKCLOSURE(event_handler, NULL));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in setup_ump_service_part2()\n");
            return err;
        }
    }

    // debugging
    //debug_printf("%s: service registered as %s\n", __func__, reply->name);

    /* code toggle: currently no auto-ping; preliminary tests show working; not yet enough experience
    err = aos_rpc_name_service_start_auto_ping(rpc, reply->handle, reply->registration_key);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_name_service_start_auto_ping()\n");
        return err;
    }
    //*/

    // offer service to monitor for intermon services
    // only capref transfer functions are called -> LMP sufficient; no highspeed FLMP needed
    // of course, only for non-monitors
    if (is_init_domain()) {
        return SYS_ERR_OK;
    }

    struct capref service_for_monitor;
    err = slot_alloc(&service_for_monitor);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slot_alloc for service_for_monitor failed.\n");
        return 1;
    }

    err = setup_lmp_service(service_for_monitor, MKCLOSURE(event_handler, NULL));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not create LMP service channel for monitor.\n");
        return 1;
    }

    err = aos_rpc_monitor_offer_service(aos_rpc_get_monitor_channel(),
                                        interface, reply->enumeration,
                                        AOS_RPC_CHAN_DRIVER_LMP, service_for_monitor);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_monitor_offer_service() failed");
        return 1;
    }

    return SYS_ERR_OK;
}

errval_t aos_rpc_name_service_deregister(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->service_deregister(rpc, handle, registration_key);
}

errval_t aos_rpc_name_service_ping(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->service_ping(rpc, handle, registration_key);
}

// closure: _data and _lambda
struct auto_ping_data {
    struct aos_rpc *rpc;
    service_handle_t handle;
    uint64_t registration_key;
};

static void auto_ping_lambda(void *arg)
{
    struct auto_ping_data *d = arg;
    aos_rpc_name_service_ping(d->rpc, d->handle, d->registration_key);
}

// this is a convenience function
errval_t aos_rpc_name_service_start_auto_ping(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key)
{
    // memory needs to be persistent
    struct periodic_event *pe = malloc(sizeof(*pe));
    if (!pe) {
        return LIB_ERR_MALLOC_FAIL;
    }

    struct auto_ping_data *data = malloc(sizeof(*data));
    if (!data) {
        free(pe);
        return LIB_ERR_MALLOC_FAIL;
    }

    data->rpc = rpc;
    data->handle = handle;
    data->registration_key = registration_key;

    // period is in us
    errval_t err = periodic_event_create(pe, get_default_waitset(),
                                         NAMESERVER_AUTO_PING_PERIOD * 1000000,
                                         MKCLOSURE(auto_ping_lambda, data));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Could not create periodic event\n");
    }
    return err;
}


errval_t aos_rpc_name_service_update(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->service_update(rpc, handle, registration_key, kv_store);
}


//--- client side: service comes second

errval_t aos_rpc_name_lookup_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t *ret_handle)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->lookup_service_by_path(rpc, abs_path_prefix, ret_handle);
}

errval_t aos_rpc_name_lookup_service_by_short_name(struct aos_rpc *rpc, const char *short_name, service_handle_t *ret_handle)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->lookup_service_by_short_name(rpc, short_name, ret_handle);
}

errval_t aos_rpc_name_find_any_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->find_any_service_by_path(rpc, abs_path_prefix, ret_handles, ret_count);
}

errval_t aos_rpc_name_find_any_service_by_filter(struct aos_rpc *rpc, const struct serializable_key_value_store *filter,
                                                 service_handle_t **ret_handles, size_t *ret_count)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->find_any_service_by_filter(rpc, filter, ret_handles, ret_count);
}

errval_t aos_rpc_name_get_service_info(struct aos_rpc *rpc, service_handle_t handle, struct serializable_key_value_store **ret_info)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->get_service_info(rpc, handle, ret_info);
}

// input is a (struct serializable_key_value_store **) type
static int cmp_kv_store_ptrs_by_name(const void *a, const void *b)
{
    struct serializable_key_value_store *a2 = *((struct serializable_key_value_store **)a);
    struct serializable_key_value_store *b2 = *((struct serializable_key_value_store **)b);
    const char *a_name = kv_store_get(a2, "name");
    const char *b_name = kv_store_get(b2, "name");

    // no comparison possible if no name stored
    // these error cases should not happen, of course
    if (!a_name) {
        return 0;
    }
    if (!b_name) {
        return 0;
    }

    return strcmp(a_name, b_name);
}

// this is a convenience function for clients
void aos_rpc_name_sort_info_ptr_array(struct aos_rpc *rpc /* unused */, struct serializable_key_value_store **info_ptr_array, size_t count)
{
    assert(info_ptr_array);

    qsort(info_ptr_array, count, sizeof(struct serializable_key_value_store *), cmp_kv_store_ptrs_by_name);
}

errval_t aos_rpc_name_bind_service_lowlevel(struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id,
                                            routing_info_t *ret_routing_info,
                                            enum aos_rpc_interface *ret_interface,
                                            size_t *ret_enumerator,
                                            enum aos_rpc_chan_driver *ret_chan_type,
                                            struct capref *ret_service_cap)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NAME);
    struct aos_rpc_interface_name_vtable *vtable = rpc->interface_vtable;
    return vtable->bind_service_lowlevel(rpc, handle, core_id, ret_routing_info, ret_interface, ret_enumerator, ret_chan_type, ret_service_cap);
}

// note: this function is implemented here and does not actually send one single request
// it's basically a convenience wrapper for the things that are also done during spawn:
// on spawning side and on launched child side.
errval_t aos_rpc_name_bind_service(struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id, struct aos_rpc **ret_rpc)
{
    assert(rpc);

    routing_info_t routing_info;
    enum aos_rpc_interface interface;
    size_t enumerator;
    enum aos_rpc_chan_driver chan_type;

    struct capref service_cap;
    errval_t err = aos_rpc_name_bind_service_lowlevel(rpc, handle, core_id,
                                                      &routing_info,
                                                      &interface,
                                                      &enumerator,
                                                      &chan_type,
                                                      &service_cap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_name_bind_service_lowlevel()\n");
        return err;
    }

    /* // for debugging
    debug_printf("%s: handle %" PRIu64 ", routing info 0x%04zx, interface %zu, chan_type %zu\n",
                 __func__, handle, routing_info, (size_t)interface, (size_t)chan_type);
    //*/

    err = aos_rpc_connect_with_service(service_cap, interface, chan_type,
                                       routing_info, ret_rpc, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_connect_with_service()\n");
        return err;
    }

    (*ret_rpc)->enumerator = enumerator;

    return SYS_ERR_OK;
}
