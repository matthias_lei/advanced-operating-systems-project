#include <rpc_client/aos_rpc_client_memory_p.h>

struct aos_rpc_interface_memory_vtable aos_rpc_memory_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .get_ram_cap = aos_rpc_remote_get_ram_cap,
    .load_ram = aos_rpc_remote_load_ram,
    .memoryservice_register = aos_rpc_remote_memoryservice_register,
    .get_ram_info = aos_rpc_remote_get_ram_info
};

errval_t aos_rpc_remote_get_ram_cap(struct aos_rpc *rpc, size_t size, size_t align,
                                    struct capref *retcap, size_t *ret_size)
{
    assert(rpc);

    // DONE: implement functionality to request a RAM capability over the
    // given channel and wait until it is delivered.

    struct aos_rpc_get_ram_cap_req_payload req_payload;
    req_payload.size = size;
    req_payload.align = align;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(struct aos_rpc_get_ram_cap_req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     ret_size, sizeof(*ret_size), AOS_RPC_INTERMON_CAPABILITY_RAM);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MEMORY_FID_GET_RAM_CAP, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    //extract payload
    assert(context.recv_env.buflen >= sizeof(*ret_size));
    *retcap = context.recv_env.cap;

    // if retcap is NULL_CAP we should have caught an error earlier
    assert(!capref_is_null(*retcap));

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_load_ram(struct aos_rpc *rpc, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size)
{
    assert(rpc);
    struct aos_rpc_load_ram_req_payload req;
    req.base = ram_base;
    req.size = ram_size;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req, sizeof(struct aos_rpc_load_ram_req_payload), AOS_RPC_INTERMON_CAPABILITY_RAM,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    context.send_env.cap = ram_cap;

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MEMORY_FID_LOAD_RAM, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // no payload
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_memoryservice_register(struct aos_rpc *rpc, struct capref nameserver_cap)
{
    assert(rpc);

    // dummy memory buffer; the servers do not react well if no payload is sent
    size_t dummy;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &dummy, sizeof(dummy), AOS_RPC_INTERMON_CAPABILITY_FRAME,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);
    context.send_env.cap = nameserver_cap;

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_MEMORY_FID_REGISTER, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    // no payload
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_get_ram_info(struct aos_rpc *rpc, size_t *ret_allocated, size_t *ret_free, size_t *ret_free_largest)
{
    // TODO
    return LIB_ERR_NOT_IMPLEMENTED;
}


/**
 * \brief Returns the channel to the memory server
 */
static struct aos_rpc *memory_rpc = NULL;

struct aos_rpc *aos_rpc_get_memory_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&memory_rpc, blocking, -1, get_memory_rpc, set_memory_rpc, "/core/memory");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return memory_rpc;
}

errval_t aos_rpc_get_ram_cap(struct aos_rpc *rpc, size_t size, size_t align,
                             struct capref *retcap, size_t *ret_size)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MEMORY);
    struct aos_rpc_interface_memory_vtable *vtable = rpc->interface_vtable;
    return vtable->get_ram_cap(rpc, size, align, retcap, ret_size);
}

errval_t aos_rpc_load_ram(struct aos_rpc *rpc, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MEMORY);
    struct aos_rpc_interface_memory_vtable *vtable = rpc->interface_vtable;
    return vtable->load_ram(rpc, ram_cap, ram_base, ram_size);
}

errval_t aos_rpc_memoryservice_register(struct aos_rpc *rpc, struct capref nameserver_cap)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MEMORY);
    struct aos_rpc_interface_memory_vtable *vtable = rpc->interface_vtable;
    return vtable->memoryservice_register(rpc, nameserver_cap);
}

errval_t aos_rpc_get_ram_info(struct aos_rpc *rpc, size_t *ret_allocated,
                              size_t *ret_free, size_t *ret_free_largest)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_MEMORY);
    struct aos_rpc_interface_memory_vtable *vtable = rpc->interface_vtable;
    return vtable->get_ram_info(rpc, ret_allocated, ret_free, ret_free_largest);
}
