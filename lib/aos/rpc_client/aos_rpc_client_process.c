#include <rpc_client/aos_rpc_client_process_p.h>

struct aos_rpc_interface_process_vtable aos_rpc_process_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .spawn = aos_rpc_remote_process_spawn,
    .get_name = aos_rpc_remote_process_get_name,
    .get_all_pids = aos_rpc_remote_process_get_all_pids,
    .spawn_from_file = aos_rpc_remote_process_spawn_from_file,
    .spawn_from_shell = aos_rpc_remote_process_spawn_from_shell
};

errval_t aos_rpc_remote_process_spawn(struct aos_rpc *rpc, char *name,
                                      coreid_t core_id, domainid_t *newpid)
{
    assert(rpc);
    assert(name);

    // DONE (milestone 5): implement spawn new process rpc

    size_t name_len = strlen(name) + 1;
    size_t req_payload_size = sizeof(struct aos_rpc_process_spawn_req_payload) + name_len;
    struct aos_rpc_process_spawn_req_payload *req_payload;
    req_payload = malloc(req_payload_size);
    if (!req_payload) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req_payload->core_id = core_id;
    strcpy(req_payload->name, name);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, req_payload_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     newpid, sizeof(*newpid), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_PROCESS_FID_SPAWN, &context);
    free(req_payload);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(*newpid));

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_process_get_name(struct aos_rpc *rpc, domainid_t pid,
                                         char **name)
{
    assert(rpc);
    assert(name);
    // DONE (milestone 5): implement name lookup for process given a process
    // id

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &pid, sizeof(domainid_t), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_PROCESS_FID_GET_NAME, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *name = context.recv_env.buf;

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_process_get_all_pids(struct aos_rpc *rpc,
                                             domainid_t **pids, size_t *pid_count)
{
    assert(rpc);
    assert(pids);
    // DONE (milestone 5): implement process id discovery

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     NULL, 0, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_PROCESS_FID_GET_ALL_PID, &context);
    if (err_is_fail(err)) {
        free(context.recv_env.buf);
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        free(context.recv_env.buf);
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(struct aos_rpc_process_get_all_pids_res_payload));
    struct aos_rpc_process_get_all_pids_res_payload *payload = context.recv_env.buf;

    *pid_count = payload->size;

    assert(context.recv_env.buflen - sizeof(struct aos_rpc_process_get_all_pids_res_payload) >= sizeof(**pids)*payload->size);

    size_t pid_array_size = *pid_count*sizeof(**pids);
    *pids = malloc(pid_array_size);
    if (!*pids) {
        free(context.recv_env.buf);
        return LIB_ERR_MALLOC_FAIL;
    }
    memcpy(*pids, payload->domain_ids, pid_array_size);

    free(context.recv_env.buf);
    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_process_spawn_from_file(struct aos_rpc *rpc, const char *path,
                                                const char *command_line, coreid_t core_id,
                                                int shell_id, domainid_t *newpid)
{
    assert(rpc);
    assert(path);
    assert(command_line);
    assert(newpid);

    // DONE (milestone 5): implement spawn new process rpc

    size_t path_len = strlen(path) + 1;
    size_t command_line_len = strlen(command_line) + 1;
    size_t req_payload_size = sizeof(struct aos_rpc_process_spawn_from_file_req_payload) + path_len + command_line_len;
    struct aos_rpc_process_spawn_from_file_req_payload *req_payload;
    req_payload = malloc(req_payload_size);
    if (!req_payload) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req_payload->core_id = core_id;
    strcpy(req_payload->data, path);
    strcpy(req_payload->data + path_len, command_line);
    req_payload->shell_id = shell_id;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, req_payload_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     newpid, sizeof(*newpid), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_PROCESS_FID_SPAWN_FROM_FILE, &context);
    free(req_payload);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(*newpid));

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_process_spawn_from_shell(struct aos_rpc *rpc, char *name,
                                      coreid_t core_id, int shell_id, domainid_t *newpid)
{
    assert(rpc);
    assert(name);

    // DONE (milestone 5): implement spawn new process rpc

    size_t name_len = strlen(name) + 1;
    size_t req_payload_size = sizeof(struct aos_rpc_process_spawn_from_shell_req_payload) + name_len;
    struct aos_rpc_process_spawn_from_shell_req_payload *req_payload;
    req_payload = malloc(req_payload_size);
    if (!req_payload) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req_payload->core_id = core_id;
    req_payload->shell_id = shell_id;
    strcpy(req_payload->name, name);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, req_payload_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     newpid, sizeof(*newpid), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_PROCESS_FID_SPAWN_FROM_SHELL, &context);
    free(req_payload);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(*newpid));

    return SYS_ERR_OK;
}

/**
 * \brief Returns the channel to the process manager
 */
static struct aos_rpc *process_rpc = NULL;

struct aos_rpc *aos_rpc_get_process_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&process_rpc, blocking, -1, get_process_rpc, set_process_rpc, "/core/process");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return process_rpc;
}

errval_t aos_rpc_process_spawn(struct aos_rpc *rpc, char *name,
                               coreid_t core_id, domainid_t *newpid)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_PROCESS);
    struct aos_rpc_interface_process_vtable *vtable = rpc->interface_vtable;
    return vtable->spawn(rpc, name, core_id, newpid);
}

errval_t aos_rpc_process_spawn_from_file(struct aos_rpc *rpc, const char *path,
                                         const char *command_line, coreid_t core_id,
                                         int shell_id, domainid_t *newpid)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_PROCESS);
    struct aos_rpc_interface_process_vtable *vtable = rpc->interface_vtable;
    return vtable->spawn_from_file(rpc, path, command_line, core_id, shell_id, newpid);
}

errval_t aos_rpc_process_spawn_from_shell(struct aos_rpc *rpc, char *name,
                                            coreid_t core_id, int shell_id,
                                            domainid_t *newpid)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_PROCESS);
    struct aos_rpc_interface_process_vtable *vtable = rpc->interface_vtable;
    return vtable->spawn_from_shell(rpc, name, core_id, shell_id, newpid);
}

errval_t aos_rpc_process_get_name(struct aos_rpc *rpc, domainid_t pid,
                                  char **name)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_PROCESS);
    struct aos_rpc_interface_process_vtable *vtable = rpc->interface_vtable;
    return vtable->get_name(rpc, pid, name);
}

errval_t aos_rpc_process_get_all_pids(struct aos_rpc *rpc,
                                      domainid_t **pids, size_t *pid_count)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_PROCESS);
    struct aos_rpc_interface_process_vtable *vtable = rpc->interface_vtable;
    return vtable->get_all_pids(rpc, pids, pid_count);
}
