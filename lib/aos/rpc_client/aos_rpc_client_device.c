#include <rpc_client/aos_rpc_client_device_p.h>

struct aos_rpc_interface_device_vtable aos_rpc_device_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .get_device_cap = aos_rpc_remote_get_device_cap,
    .get_irq_cap = aos_rpc_remote_get_irq_cap
};

errval_t aos_rpc_remote_get_device_cap(struct aos_rpc *rpc,
                                       lpaddr_t paddr, size_t bytes,
                                       struct capref *frame) {
    assert(rpc);

    struct aos_rpc_get_device_cap_req_payload req_payload;
    req_payload.paddr = paddr;
    req_payload.bytes = bytes;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(struct aos_rpc_get_device_cap_req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_IRQ);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_DEVICE_FID_GET_DEVICE_CAP, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_block()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *frame = context.recv_env.cap;

    // if retcap is NULL_CAP we should have caught an error earlier
    assert(!capref_is_null(*frame));

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_get_irq_cap(struct aos_rpc *rpc, struct capref *cap) {
    assert(rpc);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     NULL, 0, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_DEVFRAME);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_DEVICE_FID_GET_IRQ_CAP, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_block()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *cap = context.recv_env.cap;  
    // if cap is NULL_CAP we should have caught an error earlier
    assert(!capref_is_null(*cap));

    return SYS_ERR_OK;
}

errval_t aos_rpc_get_device_cap(struct aos_rpc *rpc,
                                lpaddr_t paddr, size_t bytes,
                                struct capref *frame)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_DEVICE);
    struct aos_rpc_interface_device_vtable *vtable = rpc->interface_vtable;
    return vtable->get_device_cap(rpc, paddr, bytes, frame);
}

errval_t aos_rpc_device_get_irq_cap(struct aos_rpc *rpc, struct capref *cap) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_DEVICE);
    struct aos_rpc_interface_device_vtable *vtable = rpc->interface_vtable;
    return vtable->get_irq_cap(rpc, cap);
}

static struct aos_rpc *device_rpc = NULL;

struct aos_rpc *aos_rpc_get_device_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&device_rpc, blocking, -1, get_device_rpc, set_device_rpc, "/core/device");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return device_rpc;
}
