#include <rpc_client/aos_rpc_client_filesystem_p.h>

struct aos_rpc_interface_filesystem_vtable aos_rpc_filesystem_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .open = aos_rpc_remote_open,
    .create = aos_rpc_remote_create,
    .remove = aos_rpc_remote_remove,
    .read = aos_rpc_remote_read,
    .write = aos_rpc_remote_write,
    .truncate = aos_rpc_remote_truncate,
    .tell = aos_rpc_remote_tell,
    .stat = aos_rpc_remote_stat,
    .seek = aos_rpc_remote_seek,
    .close = aos_rpc_remote_close,
    .opendir = aos_rpc_remote_opendir,
    .dir_read_next = aos_rpc_remote_dir_read_next,
    .closedir = aos_rpc_remote_closedir,
    .mkdir = aos_rpc_remote_mkdir,
    .rmdir = aos_rpc_remote_rmdir,
    .mount = aos_rpc_remote_mount
};

errval_t aos_rpc_remote_open(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle) {
    assert(rpc);
    assert(path);
    assert(outhandle);

    *outhandle = NULL;

    fs_filehandle_t res_outhandle;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     path, strlen(path) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_outhandle, sizeof(res_outhandle), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_OPEN, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *outhandle = res_outhandle;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_create(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle) {
    assert(rpc);
    assert(path);
    assert(outhandle);

    *outhandle = NULL;

    fs_filehandle_t res_outhandle;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     path, strlen(path) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_outhandle, sizeof(res_outhandle), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_CREATE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *outhandle = res_outhandle;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_remove(struct aos_rpc *rpc, const char *path) {
    assert(rpc);
    assert(path);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     path, strlen(path) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_REMOVE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_read(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read) {
    assert(rpc);
    assert(buffer);
    assert(bytes_read);

    *bytes_read = 0;

    struct aos_rpc_read_req_payload req_payload = {
        .inhandle = inhandle,
        .bytes = bytes
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_READ, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        free(context.recv_env.buf);
        return err;
    }

    assert(sizeof(struct aos_rpc_read_res_payload) <= context.recv_env.buflen);

    struct aos_rpc_read_res_payload *res_payload = context.recv_env.buf;

    assert(res_payload->bytes_read == context.recv_env.buflen - sizeof(struct aos_rpc_read_res_payload));

    memcpy(buffer, res_payload->buffer, res_payload->bytes_read);

    *bytes_read = res_payload->bytes_read;

    free(context.recv_env.buf);

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_write(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written) {
    assert(rpc);
    assert(buffer);
    assert(bytes_written);

    *bytes_written = 0;

    uint32_t len = sizeof(struct aos_rpc_write_req_payload) + bytes;
    struct aos_rpc_write_req_payload *req_payload = malloc(len);
    if (!req_payload) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req_payload->inhandle = inhandle;
    req_payload->bytes = bytes;
    memcpy(req_payload->buffer, buffer, bytes);

    uint32_t res_bytes_written;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_bytes_written, sizeof(res_bytes_written), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_WRITE, &context);
    free(req_payload);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *bytes_written = res_bytes_written;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_truncate(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes) {
    assert(rpc);

    struct aos_rpc_truncate_req_payload req_payload = {
        .inhandle = inhandle,
        .bytes = bytes
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(struct aos_rpc_truncate_req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_TRUNCATE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_tell(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos) {
    assert(rpc);
    assert(pos);

    *pos = 0;

    size_t res_pos;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &inhandle, sizeof(inhandle), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_pos, sizeof(res_pos), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_TELL, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *pos = res_pos;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_stat(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info) {
    assert(rpc);
    assert(info);

    info->size = 0;
    info->type = FS_FILE;

    struct fs_fileinfo res_info;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &inhandle, sizeof(inhandle), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_info, sizeof(res_info), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_STAT, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *info = res_info;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_seek(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset) {
    assert(rpc);

    struct aos_rpc_seek_req_payload req_payload = {
        .inhandle = inhandle,
        .whence = whence,
        .offset = offset
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_SEEK, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_close(struct aos_rpc *rpc, fs_filehandle_t inhandle) {
    assert(rpc);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &inhandle, sizeof(inhandle), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_CLOSE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_opendir(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle) {
    assert(rpc);
    assert(path);
    assert(outhandle);

    *outhandle = NULL;

    fs_dirhandle_t res_outhandle;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     path, strlen(path) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_outhandle, sizeof(res_outhandle), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_OPENDIR, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *outhandle = res_outhandle;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_dir_read_next(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info) {
    assert(rpc);
    assert(retname);
    // info can be null

    *retname = NULL;
    if (info) {
        info->size = 0;
        info->type = FS_FILE;
    }

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &inhandle, sizeof(inhandle), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_DIR_READ_NEXT, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(struct aos_rpc_dir_read_next_res_payload));

    struct aos_rpc_dir_read_next_res_payload *res_payload = context.recv_env.buf;
    size_t retname_len = context.recv_env.buflen - sizeof(struct aos_rpc_dir_read_next_res_payload);

    // strnlen always returns the string length without the zero terminator, however, we always
    // send the zero terminator of strings, so this assertion must hold
    assert(strnlen(res_payload->retname, retname_len) < retname_len);

    *retname = strdup(res_payload->retname);
    if (!*retname) {
        free(res_payload);
        return LIB_ERR_MALLOC_FAIL;
    }

    if (info) {
        *info = res_payload->info;
    }
    free(res_payload);

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_closedir(struct aos_rpc *rpc, fs_dirhandle_t inhandle) {
    assert(rpc);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &inhandle, sizeof(inhandle), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_CLOSEDIR, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_mkdir(struct aos_rpc *rpc, const char *path) {
    assert(rpc);
    assert(path);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     path, strlen(path) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_MKDIR, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_rmdir(struct aos_rpc *rpc, const char *path) {
    assert(rpc);
    assert(path);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     path, strlen(path) + 1, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_RMDIR, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_mount(struct aos_rpc *rpc, const char *path, const char *uri) {
    assert(rpc);

    size_t path_len = strlen(path) + 1;
    size_t uri_len = strlen(uri) + 1;
    uint32_t len = path_len + uri_len;
    char *req_payload = malloc(len);
    if (!req_payload) {
        return LIB_ERR_MALLOC_FAIL;
    }

    strcpy(req_payload, path);
    strcpy(req_payload + path_len, uri);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_FILESYSTEM_FID_MOUNT, &context);
    free(req_payload);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}


/**
 * \brief Returns the channel to the process manager
 */
static struct aos_rpc *filesystem_rpc = NULL;

struct aos_rpc *aos_rpc_get_filesystem_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&filesystem_rpc, blocking, -1, get_filesystem_rpc, set_filesystem_rpc, "/dev/filesystem");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return filesystem_rpc;
}

errval_t aos_rpc_filesystem_open(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->open(rpc, path, outhandle);
}

errval_t aos_rpc_filesystem_create(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->create(rpc, path, outhandle);
}

errval_t aos_rpc_filesystem_remove(struct aos_rpc *rpc, const char *path) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->remove(rpc, path);
}

errval_t aos_rpc_filesystem_read(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->read(rpc, inhandle, buffer, bytes, bytes_read);
}

errval_t aos_rpc_filesystem_write(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->write(rpc, inhandle, buffer, bytes, bytes_written);
}

errval_t aos_rpc_filesystem_truncate(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->truncate(rpc, inhandle, bytes);
}

errval_t aos_rpc_filesystem_tell(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->tell(rpc, inhandle, pos);
}

errval_t aos_rpc_filesystem_stat(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->stat(rpc, inhandle, info);
}

errval_t aos_rpc_filesystem_seek(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->seek(rpc, inhandle, whence, offset);
}

errval_t aos_rpc_filesystem_close(struct aos_rpc *rpc, fs_filehandle_t inhandle) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->close(rpc, inhandle);
}

errval_t aos_rpc_filesystem_opendir(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->opendir(rpc, path, outhandle);
}

errval_t aos_rpc_filesystem_dir_read_next(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->dir_read_next(rpc, inhandle, retname, info);
}

errval_t aos_rpc_filesystem_closedir(struct aos_rpc *rpc, fs_dirhandle_t inhandle) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->closedir(rpc, inhandle);
}

errval_t aos_rpc_filesystem_mkdir(struct aos_rpc *rpc, const char *path) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->mkdir(rpc, path);
}

errval_t aos_rpc_filesystem_rmdir(struct aos_rpc *rpc, const char *path) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->rmdir(rpc, path);
}

errval_t aos_rpc_filesystem_mount(struct aos_rpc *rpc, const char *path, const char *uri) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_FILESYSTEM);
    struct aos_rpc_interface_filesystem_vtable *vtable = rpc->interface_vtable;
    return vtable->mount(rpc, path, uri);
}
