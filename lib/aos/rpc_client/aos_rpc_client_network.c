#include <rpc_client/aos_rpc_client_network_p.h>
#include <errno.h>

struct aos_rpc_interface_network_vtable aos_rpc_network_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .accept = aos_rpc_network_remote_accept,
    .bind = aos_rpc_network_remote_bind,
    .connect = aos_rpc_network_remote_connect,
    .getpeername = aos_rpc_network_remote_getpeername,
    .getsockname = aos_rpc_network_remote_getsockname,
    .getsockopt = aos_rpc_network_remote_getsockopt,
    .listen = aos_rpc_network_remote_listen,
    .recv = aos_rpc_network_remote_recv,
    .recvfrom = aos_rpc_network_remote_recvfrom,
    .recvmsg = aos_rpc_network_remote_recvmsg,
    .send = aos_rpc_network_remote_send,
    .sendmsg = aos_rpc_network_remote_sendmsg,
    .sendto = aos_rpc_network_remote_sendto,
    .setsockopt = aos_rpc_network_remote_setsockopt,
    .shutdown = aos_rpc_network_remote_shutdown,
    .socket = aos_rpc_network_remote_socket,
    .socketpair = aos_rpc_network_remote_socketpair
};

errval_t aos_rpc_network_remote_accept(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_bind(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len) {
    assert(rpc);

    struct aos_rpc_network_bind_req_payload req;
    req.socket = socket;
    req.addr = *address;
    req.addr_len = address_len;

    struct aos_rpc_network_bind_res_payload res;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req, sizeof(req), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res, sizeof(res), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NETWORK_FID_BIND, &context);
    if (err_is_fail(err)) {
        *err_no = ENETUNREACH;
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        *err_no = res.err_no;
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *ret_value = res.ret_value;
    *err_no = res.err_no;

    return SYS_ERR_OK;
}

errval_t aos_rpc_network_remote_connect(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_getpeername(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_getsockname(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    assert(rpc);

    struct aos_rpc_network_getsockname_req_payload req;
    req.socket = socket;

    struct aos_rpc_network_getsockname_res_payload res;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req, sizeof(req), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res, sizeof(res), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NETWORK_FID_GETSOCKNAME, &context);
    if (err_is_fail(err)) {
        *err_no = ENETUNREACH;
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        *err_no = res.err_no;
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *ret_value = res.ret_value;
    *err_no = res.err_no;
    *address = res.addr;
    *address_len = res.addr_len;

    return SYS_ERR_OK;
}

errval_t aos_rpc_network_remote_getsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_listen(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int backlog) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_recv(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_recvfrom(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {
    assert(rpc);

    struct aos_rpc_network_recvfrom_req_payload req;
    req.socket = socket;
    req.length = length;
    req.flags = flags;

    struct aos_rpc_network_recvfrom_res_payload *res = malloc(sizeof(struct aos_rpc_network_recvfrom_res_payload) + length);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req, sizeof(req), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     res, sizeof(struct aos_rpc_network_recvfrom_res_payload) + length, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NETWORK_FID_RECVFROM, &context);
    if (err_is_fail(err)) {
        *err_no = ENETUNREACH;
        free(res);
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        *err_no = res->err_no;
        free(res);
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *ret_size = MIN(res->ret_size, length);
    *err_no = res->err_no;
    *address = res->addr;
    *address_len = res->addr_len;
    if (*ret_size != -1) {
        memcpy(buffer, &res->buffer, *ret_size);
    }

    free(res);
    return SYS_ERR_OK;
}

errval_t aos_rpc_network_remote_recvmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_send(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_sendmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_sendto(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
    assert(rpc);

    struct aos_rpc_network_sendto_req_payload *req = malloc(sizeof(struct aos_rpc_network_sendto_req_payload) + length);
    req->socket = socket;
    req->flags = flags;
    req->dest_addr = *dest_addr;
    req->dest_len = dest_len;
    req->length = length;
    memcpy(&req->message, message, length);

    struct aos_rpc_network_sendto_res_payload res;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req, sizeof(struct aos_rpc_network_sendto_req_payload) + length, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res, sizeof(res), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NETWORK_FID_SENDTO, &context);
    if (err_is_fail(err)) {
        *err_no = ENETUNREACH;
        free(req);
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        *err_no = res.err_no;
        free(req);
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *ret_size = res.ret_size;
    *err_no = res.err_no;
    free(req);
    return SYS_ERR_OK;
}

errval_t aos_rpc_network_remote_setsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_shutdown(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int how) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_remote_socket(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol) {
    assert(rpc);

    struct aos_rpc_network_socket_req_payload req;
    req.domain = domain;
    req.type = type;
    req.protocol = protocol;

    struct aos_rpc_network_socket_res_payload res;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req, sizeof(req), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res, sizeof(res), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_NETWORK_FID_SOCKET, &context);
    if (err_is_fail(err)) {
        *err_no = ENETUNREACH;
        DEBUG_ERR(err, "%s: error while calling aos_rpc_generic_send_and_recv()\n", __func__);
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        *err_no = res.err_no;
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    *ret_value = res.ret_value;
    *err_no = res.err_no;

    return SYS_ERR_OK;
}

errval_t aos_rpc_network_remote_socketpair(struct aos_rpc *rpc,int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_rpc_network_accept(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->accept(rpc, ret_value, err_no, socket, address, address_len);
}

errval_t aos_rpc_network_bind(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->bind(rpc, ret_value, err_no, socket, address, address_len);
}

errval_t aos_rpc_network_connect(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->connect(rpc, ret_value, err_no, socket, address, address_len);
}

errval_t aos_rpc_network_getpeername(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->getpeername(rpc, ret_value, err_no, socket, address, address_len);
}

errval_t aos_rpc_network_getsockname(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->getsockname(rpc, ret_value, err_no, socket, address, address_len);
}

errval_t aos_rpc_network_getsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->getsockopt(rpc, ret_value, err_no, socket, level, option_name, option_value, option_len);
}

errval_t aos_rpc_network_listen(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int backlog) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->listen(rpc, ret_value, err_no, socket, backlog);
}

errval_t aos_rpc_network_recv(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->recv(rpc, ret_size, err_no, socket, buffer, length, flags);
}

errval_t aos_rpc_network_recvfrom(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->recvfrom(rpc, ret_size, err_no, socket, buffer, length, flags, address, address_len);
}

errval_t aos_rpc_network_recvmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->recvmsg(rpc, ret_size, err_no, socket, message, flags);
}

errval_t aos_rpc_network_send(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->send(rpc, ret_size, err_no, socket, message, length, flags);
}

errval_t aos_rpc_network_sendmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->sendmsg(rpc, ret_size, err_no, socket, message, flags);
}

errval_t aos_rpc_network_sendto(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->sendto(rpc, ret_size, err_no, socket, message, length, flags, dest_addr, dest_len);
}

errval_t aos_rpc_network_setsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->setsockopt(rpc, ret_value, err_no, socket, level, option_name, option_value, option_len);
}

errval_t aos_rpc_network_shutdown(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int how) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->shutdown(rpc, ret_value, err_no, socket, how);
}

errval_t aos_rpc_network_socket(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->socket(rpc, ret_value, err_no, domain, type, protocol);
}

errval_t aos_rpc_network_socketpair(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_NETWORK);
    struct aos_rpc_interface_network_vtable *vtable = rpc->interface_vtable;
    return vtable->socketpair(rpc, ret_value, err_no, domain, type, protocol, socket_vector);
}


/**
 * \brief Returns the channel to the process manager
 */
static struct aos_rpc *network_rpc = NULL;

struct aos_rpc *aos_rpc_get_network_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&network_rpc, blocking, -1, get_network_rpc, set_network_rpc, "/dev/network");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return network_rpc;
}
