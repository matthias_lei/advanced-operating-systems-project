#include <rpc_client/aos_rpc_client_intermon_p.h>

#include <domman/domman.h>
#include <spawn/spawn.h>

struct aos_rpc_interface_intermon_vtable aos_rpc_intermon_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .request_bootstrap_info = aos_rpc_remote_intermon_request_bootstrap_info,
    .forward_rpc_message = aos_rpc_remote_intermon_forward_rpc_message
};


errval_t aos_rpc_remote_intermon_request_bootstrap_info(struct aos_rpc *rpc, coreid_t core_id, struct aos_intermon_bootstrap_res *info)
{
    assert(rpc);
    assert(info);

    size_t payload = (size_t)core_id;
    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &payload, sizeof(size_t), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     info, sizeof(*info), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_INTERMON_FID_REQUEST_BOOTSTRAP_INFO, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error received from RPC call (error in event handler).\n");
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(*info));

    return SYS_ERR_OK;
}

errval_t aos_rpc_remote_intermon_forward_rpc_message(struct aos_rpc *rpc, struct aos_rpc_intermon_forward_req_payload *send,
                                                     struct aos_rpc_intermon_forward_res_payload *recv)
{
    assert(rpc);
    assert(send);
    assert(recv);

    struct aos_rpc_comm_context context;
    // note: it is known (fixed convention) that a provided buffer in recv
    // has at least size of DEFAULT_RPC_BUFFER_SIZE and is properly aligned
    aos_rpc_comm_context_client_init(&context,
                                     send, sizeof(struct aos_rpc_intermon_forward_req_payload) + send->env_size, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     recv, DEFAULT_RPC_BUFFER_SIZE, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_INTERMON_FID_FORWARD_RPC_MESSAGE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error while calling aos_rpc_generic_send_and_recv()\n");
        return err;
    }

    // NOTE: deliberately NO check of the embedded return code from RPC
    // it is just forwarded to recipient

    assert(context.recv_env.buflen >= sizeof(struct aos_rpc_intermon_forward_res_payload));
    return SYS_ERR_OK;
}


/**
 * \brief Returns the channel to the intermon server
 */
struct aos_rpc *aos_rpc_get_intermon_channel(coreid_t core)
{
    // must be a monitor
    assert(is_init_domain());

    // no caching here
    return get_intermon_rpc(core);
}


errval_t aos_rpc_intermon_request_bootstrap_info(struct aos_rpc *rpc, coreid_t core_id, struct aos_intermon_bootstrap_res *info)
{
    assert(rpc);
    assert(info);
    assert(rpc->interface == AOS_RPC_INTERFACE_INTERMON);

    // must be a monitor
    assert(is_init_domain());

    struct aos_rpc_interface_intermon_vtable *vtable = rpc->interface_vtable;
    return vtable->request_bootstrap_info(rpc, core_id, info);
}


errval_t aos_rpc_intermon_forward_rpc_message(struct aos_rpc *rpc, struct aos_rpc_intermon_forward_req_payload *send,
                                              struct aos_rpc_intermon_forward_res_payload *recv)
{
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_INTERMON);

    // must be a monitor
    assert(is_init_domain());

    struct aos_rpc_interface_intermon_vtable *vtable = rpc->interface_vtable;
    return vtable->forward_rpc_message(rpc, send, recv);
}
