#include <rpc_client/aos_rpc_client_shell_p.h>

struct aos_rpc_interface_shell_vtable aos_rpc_shell_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .request_params = aos_rpc_remote_request_params,
    .notify_termination = aos_rpc_remote_notify_termination
};

errval_t aos_rpc_remote_request_params(struct aos_rpc *rpc, struct shell_params **ret_params) {
    assert(rpc);
    assert(ret_params);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     NULL, 0, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_SHELL_FID_REQUEST_PARAMS, &context);
    if (err_is_fail(err)) {
        free(context.recv_env.buf);
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    // also check the return code from RPC
    err = AOS_RPC_GET_ERR_NO(context.recv_env.header);
    if (err_is_fail(err)) {
        free(context.recv_env.buf);
        DEBUG_ERR(err, "%s: error received from RPC call (error in event handler).\n", __func__);
        return err;
    }

    assert(context.recv_env.buflen >= sizeof(struct shell_params));

    *ret_params =  context.recv_env.buf;


    // debug_printf("background: %s\n", *ret->background ? "True" : "False");

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_notify_termination(struct aos_rpc *rpc, domainid_t pid) {
    assert(rpc);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &pid, sizeof(pid), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_SHELL_FID_NOTIFY_TERMINATION, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

/**
 * \brief Returns the channel to the process manager
 */
static struct aos_rpc *shell_rpc = NULL;

errval_t aos_rpc_get_shell_channel_multi(size_t enumerator, struct aos_rpc **ret_shell_rpc)
{
    assert(ret_shell_rpc);

    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(ret_shell_rpc, blocking, enumerator, NULL, NULL, "/app/shell");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return err;
}

struct aos_rpc *aos_rpc_get_shell_channel(size_t enumerator)
{
    errval_t err = aos_rpc_get_shell_channel_multi(enumerator, &shell_rpc);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return shell_rpc;
}

errval_t aos_rpc_shell_request_params(struct aos_rpc *rpc, struct shell_params **ret_params) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_SHELL);
    struct aos_rpc_interface_shell_vtable *vtable = rpc->interface_vtable;
    return vtable->request_params(rpc, ret_params);
}

errval_t aos_rpc_shell_notify_termination(struct aos_rpc *rpc, domainid_t pid) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_SHELL);
    struct aos_rpc_interface_shell_vtable *vtable = rpc->interface_vtable;
    return vtable->notify_termination(rpc, pid);
}
