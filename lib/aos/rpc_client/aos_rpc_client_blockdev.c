#include <rpc_client/aos_rpc_client_blockdev_p.h>

struct aos_rpc_interface_blockdev_vtable aos_rpc_blockdev_remote_vtable = {
    .common = AOS_RPC_INTERFACE_COMMON_REMOTE_VTABLE_INIT,
    .get_block_size = aos_rpc_remote_get_block_size,
    .read_block = aos_rpc_remote_read_block,
    .write_block = aos_rpc_remote_write_block
};

errval_t aos_rpc_remote_get_block_size(struct aos_rpc *rpc, size_t *block_size) {
    assert(rpc);
    assert(block_size);

    *block_size = 0;

    size_t res_block_size;

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     NULL, 0, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     &res_block_size, sizeof(res_block_size), AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_BLOCKDEV_FID_GET_BLOCK_SIZE, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    *block_size = res_block_size;

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_read_block(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen) {
    assert(rpc);
    assert(buf);

    struct aos_rpc_read_block_req_payload req_payload = {
        .block_nr = block_nr,
        .buflen = buflen
    };

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     &req_payload, sizeof(struct aos_rpc_read_block_req_payload), AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     buf, buflen, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_BLOCKDEV_FID_READ_BLOCK, &context);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

errval_t aos_rpc_remote_write_block(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen) {
    assert(rpc);
    assert(buf);

    uint32_t len =  sizeof(struct aos_rpc_write_block_req_payload) + buflen;
    struct aos_rpc_write_block_req_payload *req_payload = malloc(len);
    if (!req_payload) {
        return LIB_ERR_MALLOC_FAIL;
    }

    req_payload->block_nr = block_nr;
    req_payload->buflen = buflen;
    memcpy(req_payload->buf, buf, buflen);

    struct aos_rpc_comm_context context;
    aos_rpc_comm_context_client_init(&context,
                                     req_payload, len, AOS_RPC_INTERMON_CAPABILITY_NONE,
                                     NULL, AOS_RPC_EXPECT_NO_PAYLOAD, AOS_RPC_INTERMON_CAPABILITY_NONE);

    errval_t err = aos_rpc_generic_send_and_recv(rpc, AOS_RPC_BLOCKDEV_FID_WRITE_BLOCK, &context);
    free(req_payload);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_generic_send_and_recv failed.\n");
        return err;
    }

    return AOS_RPC_GET_ERR_NO(context.recv_env.header);
}

static struct aos_rpc *blockdev_rpc = NULL;

struct aos_rpc *aos_rpc_get_blockdev_channel(void)
{
    // can be defined for each channel getter
    // at least for core services, it is recommended to leave it on true;
    bool blocking = true;

    errval_t err = aos_rpc_get_channel(&blockdev_rpc, blocking, -1, get_blockdev_rpc, set_blockdev_rpc, "/dev/blockdev");
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in aos_rpc_get_channel().\n");
    }

    return blockdev_rpc;
}

errval_t aos_rpc_blockdev_get_block_size(struct aos_rpc *rpc, size_t *block_size) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_BLOCKDEV);
    struct aos_rpc_interface_blockdev_vtable *vtable = rpc->interface_vtable;
    return vtable->get_block_size(rpc, block_size);
}

errval_t aos_rpc_blockdev_read_block(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_BLOCKDEV);
    struct aos_rpc_interface_blockdev_vtable *vtable = rpc->interface_vtable;
    return vtable->read_block(rpc, block_nr, buf, buflen);
}

errval_t aos_rpc_blockdev_write_block(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen) {
    assert(rpc);
    assert(rpc->interface == AOS_RPC_INTERFACE_BLOCKDEV);
    struct aos_rpc_interface_blockdev_vtable *vtable = rpc->interface_vtable;
    return vtable->write_block(rpc, block_nr, buf, buflen);
}
