/**
 * \file
 * \brief Barrelfish library initialization.
 */

/*
 * Copyright (c) 2007-2016, ETH Zurich.
 * Copyright (c) 2014, HP Labs.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

#include <stdio.h>
#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/dispatch.h>
#include <aos/curdispatcher_arch.h>
#include <aos/dispatcher_arch.h>
#include <barrelfish_kpi/dispatcher_shared.h>
#include <aos/morecore.h>
#include <aos/paging.h>
#include <barrelfish_kpi/domain_params.h>
#include <rpc_client/aos_rpc_client_common_p.h>
#include "threads_priv.h"
#include "init.h"

#include <aos/systime.h>

#include <aos/connect/all.h>

#include <fs/fs.h>

#include <aos/rpc_shared/aos_rpc_shared_shell.h>
#include <unistd.h>

//--- dynamic runtime configuration section --------------------------------------------------------
/// Are we the init domain (and thus need to take some special paths)?
static bool init_domain;

// extended: morecore.c e.g. needs to know whether in init domain or not
// and I did not just want to make a static variable read/write global
// thus this getter

bool is_init_domain(void) {
    return init_domain;
}


struct dynamic_config {
    bool spawn_load_symbols;
    bool bootstrapping_without_memory;
    bool memory_service_has_connection;
    bool memory_service_is_connected;
};

static struct dynamic_config dynamic_config = {
    // default configuration
    .spawn_load_symbols = true,
    .bootstrapping_without_memory = false,
    .memory_service_has_connection = false,
    .memory_service_is_connected = false
};

bool config_get_spawn_load_symbols(void)
{
    return dynamic_config.spawn_load_symbols;
}

void config_set_spawn_load_symbols(bool value)
{
    dynamic_config.spawn_load_symbols = value;
}


bool is_bootstrapping_without_memory(void)
{
    return dynamic_config.bootstrapping_without_memory;
}

void set_bootstrapping_without_memory(void)
{
    dynamic_config.bootstrapping_without_memory = true;
}


bool has_memory_service_connection(void)
{
    return dynamic_config.memory_service_has_connection;
}

void set_has_memory_service_connection(void)
{
    dynamic_config.memory_service_has_connection = true;
}


bool is_memory_service_connected(void)
{
    return dynamic_config.memory_service_is_connected;
}

void set_is_memory_service_connected(void)
{
    dynamic_config.memory_service_is_connected = true;
}

//--------------------------------------------------------------------------------------------------

static struct aos_rpc *my_shell_rpc = NULL;


extern size_t (*_libc_terminal_read_func)(char *, size_t);
extern size_t (*_libc_terminal_write_func)(const char *, size_t);
extern void (*_libc_exit_func)(int);
extern void (*_libc_assert_func)(const char *, const char *, const char *, int);

void libc_exit(int);

void libc_exit(int status)
{
    // Use spawnd if spawned through spawnd
    if(disp_get_domain_id() == 0) {
        errval_t err = cap_revoke(cap_dispatcher);
        if (err_is_fail(err)) {
            sys_print("revoking dispatcher failed in _Exit, spinning!", 100);
            while (1) {}
        }
        err = cap_delete(cap_dispatcher);
        sys_print("deleting dispatcher failed in _Exit, spinning!", 100);

        // XXX: Leak all other domain allocations
    } else {
        // close io pointers and notify shell
        if(my_shell_rpc) {
            int errf = 0;
            if (stdin->_file != STDIN_FILENO) {
                errf = fclose(stdin);
                if (errf != 0) {
                    debug_printf("closing stdin failed.\n");
                }
            }
            if (stdout->_file != STDOUT_FILENO) {
                errf = fclose(stdout);
                if (errf != 0) {
                    debug_printf("closing stdout failed.\n");
                }
            }
            errval_t err = aos_rpc_shell_notify_termination(my_shell_rpc, disp_get_domain_id());
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "aos_rpc_shell_notify_termination failed.\n");
            }
        }
    }

    thread_exit(status);
    // If we're not dead by now, we wait
    while (1) {}
}

static void libc_assert(const char *expression, const char *file,
                        const char *function, int line)
{
    char buf[512];
    size_t len;

    /* Formatting as per suggestion in C99 spec 7.2.1.1 */
    len = snprintf(buf, sizeof(buf), "Assertion failed on core %d in %.*s: %s,"
                   " function %s, file %s, line %d.\n",
                   disp_get_core_id(), DISP_NAME_LEN,
                   disp_name(), expression, function, file, line);
    sys_print(buf, len < sizeof(buf) ? len : sizeof(buf));
}

static size_t syscall_terminal_write(const char *buf, size_t len)
{
    if (len) {
        if (err_is_ok(sys_print(buf, len))) {
            return len;
        } else {
            return 0;
        }
    }
    return 0;
}

static size_t dummy_terminal_read(char *buf, size_t len)
{
    // originally provided code
    //debug_printf("terminal read NYI! returning %d characters read\n", len);
    //return len;

    // pisch: implemented this temporary terminal reader
    // until proper user-space serial driver is available
    // later in the project. see comments in barrelfish_libc_glue_init(void) below

    // simple solution at the moment:
    // return input char by char
    // note: stdio does the buffering

    // note 2: sys_getchar() does not block, of course (no system call would ever block),
    // but returns char 0 if no key was pressed

    // However, the client stdio expects some blocking: Thus the compromise
    // 1) block until first char but then return to caller with size 1
    //    if there are no other chars readily available
    // 2) keep adding chars up to length if they are available
    //    note: but I think we should not keep blocking after the first char
    //    since we do not really know how many chars are expected.
    //    input argument len only refers to buffer length but not number
    //    of absolutely needed chars

    if (len == 0) {
        return 0;
    }

    size_t i = 0;
    char c = 0;
    errval_t e = SYS_ERR_OK;

    // first block for the first char
    while (e == SYS_ERR_OK && c == 0) {
        e = sys_getchar(&c);
    }

    // then keep adding if possible, but do not block
    while (err_is_ok(e) && c != 0 && i < len) {
        buf[i++] = c;
        e = sys_getchar(&c);
    }

    if (err_is_ok(e)) {
        return i;
    } else {
        return 0;
    }
}

static size_t rpc_terminal_write(const char *buf, size_t len) {
    errval_t err;

    if (len == 0) {
        return 0;
    }

    assert(buf);

    struct aos_rpc *serial_rpc = aos_rpc_get_serial_channel();

    err = aos_rpc_serial_putstring(serial_rpc, buf, len);
    if (err_is_fail(err)) {
        return 0;
    }

    return len;
}

static size_t rpc_terminal_read(char *buf, size_t len) {
    /*
        implemented non-blocking because aos_rpc_serial_getchar()
        already implemented blocking, and guaranteed to have received
        char after succesful execution of aos_rpc_serial_getchar()
    */
    errval_t err = SYS_ERR_OK;

    if (len == 0) {
        return 0;
    }

    assert(buf);

    struct aos_rpc *serial_rpc = aos_rpc_get_serial_channel();

    char c = 0;

    err = aos_rpc_serial_getchar(serial_rpc, &c);
    if (err_is_fail(err)) {
        buf[0] = 0;
        return 0;
    }

    buf[0] = c;

    return 1;
}

/* Set libc function pointers */
void barrelfish_libc_glue_init(void)
{
    // XXX: FIXME: Check whether we can use the proper kernel serial, and
    // what we need for that
    // DONE: change these to use the user-space serial driver if possible

    // assign I/O functions according to process
    if (init_domain) {
        _libc_terminal_write_func = syscall_terminal_write;
        _libc_terminal_read_func = dummy_terminal_read;
    }
    else {
        _libc_terminal_write_func = rpc_terminal_write;
        _libc_terminal_read_func = rpc_terminal_read;
    }

    _libc_exit_func = libc_exit;
    _libc_assert_func = libc_assert;

    /* morecore func is setup by morecore_init() */

    // XXX: set a static buffer for stdout
    // this avoids an implicit call to malloc() on the first printf
    static char buf[BUFSIZ];
    setvbuf(stdout, buf, _IOLBF, sizeof(buf));
    static char ebuf[BUFSIZ];
    setvbuf(stderr, ebuf, _IOLBF, sizeof(buf));
}


extern char **environ;

/** \brief Initialise libbarrelfish.
 *
 * This runs on a thread in every domain, after the dispatcher is setup but
 * before main() runs.
 */
errval_t barrelfish_init_onthread(struct spawn_domain_params *params)
{
    errval_t err;

    // do we have an environment?
    if (params != NULL && params->envp[0] != NULL) {
        environ = params->envp;
    }

    environ = NULL;

    // store the received systime_frequency into the global variable
    dispatcher_handle_t dh = curdispatcher();
    struct dispatcher_shared_generic *ds = get_dispatcher_shared_generic(dh);
    systime_frequency = ds->systime_frequency;

    // Init default waitset for this dispatcher
    struct waitset *default_ws = get_default_waitset();
    waitset_init(default_ws);

    // Initialize ram_alloc state
    ram_alloc_init();
    /* All domains use smallcn to initialize */
    err = ram_alloc_set(ram_alloc_fixed);
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_RAM_ALLOC_SET);
    }

    err = paging_init();
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_VSPACE_INIT);
    }

    if (params->vspace_buf && params->vspace_buf_len) {
        err = paging_state_deserialize(get_current_paging_state(), params->vspace_buf, params->vspace_buf_len);
        if (err_is_fail(err)) {
            return err_push(err, LIB_ERR_VSPACE_INIT);
        }
    }

    err = slot_alloc_init();
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_SLOT_ALLOC_INIT);
    }

    err = morecore_init();
    if (err_is_fail(err)) {
        return err_push(err, LIB_ERR_MORECORE_INIT);
    }

    lmp_endpoint_init();

    // initialize file system
    err = filesystem_init();
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "filesystem_init failed.\n");
        return err;
    }


    // init domains only get partial init
    if (init_domain) {
        // all services (including in init) need an endpoint
        // for the others it is crated during spawn
        err = cap_retype(cap_selfep, cap_dispatcher, 0, ObjType_EndPoint, 0, 1);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Failed to create endpoint to self.\n");
            abort();
        }

        return SYS_ERR_OK;
    }

    // MONITOR
    // DONE MILESTONE 3: register ourselves with the monitor
    // note: this MUST be a LMP or FLMP connection. NO UMP allowed
    struct aos_rpc *monitor_rpc;
    struct spawn_chan_params *mon = &params->monitor_params;
    switch (mon->chan_type) {
        case AOS_RPC_CHAN_DRIVER_LMP:
        case AOS_RPC_CHAN_DRIVER_FLMP:
            // both ok
            break;
        default:
            err = SPAWN_ERR_GET_CMDLINE_ARGS;
            DEBUG_ERR(err, "RPC chan driver for init/monitor must be LMP or FLMP\n");
            return err;
    }
    err = aos_rpc_connect_with_service(cap_initep, mon->interface, mon->chan_type,
                                       mon->routing_info, &monitor_rpc, NULL);
    if (err_is_fail(err)) {
        return err;
    }
    monitor_rpc->enumerator = mon->enumerator;
    set_init_rpc(monitor_rpc);

    // NAME
    // except during bootstrap of memory service Dionysos and name service Gaia
    struct spawn_chan_params *name = &params->name_params;
    if (name->chan_type == AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP) {
        set_name_rpc(NULL);
    }
    else {
        struct aos_rpc *name_rpc;
        err = aos_rpc_connect_with_service(cap_nameep, name->interface, name->chan_type,
                                           name->routing_info, &name_rpc, NULL);
        if (err_is_fail(err)) {
            return err;
        }
        name_rpc->enumerator = name->enumerator;
        set_name_rpc(name_rpc);
    }

    // MEMORY
    // except during bootstrap of memory service Dionysos
    struct spawn_chan_params *mem = &params->memory_params;
    if (mem->chan_type == AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP) {
        set_memory_rpc(NULL);
        // keep the builtin ram_alloc_fixed

        // also set bootstrapping flag for later use
        set_bootstrapping_without_memory();
    }
    else if (mem->chan_type == AOS_RPC_CHAN_DRIVER_NONE_LATE_BINDING) {
        set_memory_rpc(NULL);

        struct aos_rpc *mem_rpc  = aos_rpc_get_memory_channel();
        if (!mem_rpc) {
            debug_printf("could not establish connection with memory channel");
            return LIB_ERR_SHOULD_NOT_GET_HERE;
        }

        // new late switching system to use as much from provided 1 MiB as possible
        set_has_memory_service_connection();
    }
    else {
        // TODO pisch: this mode is not used at the moment.
        // can be removed if all keeps working well
        struct aos_rpc *memory_rpc;

        err = aos_rpc_connect_with_service(cap_memep, mem->interface, mem->chan_type,
                                           mem->routing_info, &memory_rpc, NULL);
        if (err_is_fail(err)) {
            return err;
        }
        memory_rpc->enumerator = mem->enumerator;
        set_memory_rpc(memory_rpc);

        /* DONE MILESTONE 3: now we should have a channel with init set up and can
         * use it for the ram allocator */

        // new late switching system to use as much from provided 1 MiB as possible
        set_has_memory_service_connection();
    }

    if (params->shell_id >= 0) {
        my_shell_rpc = aos_rpc_get_shell_channel(params->shell_id);
        if (!my_shell_rpc) {
            err = LIB_ERR_SHOULD_NOT_GET_HERE;
            DEBUG_ERR(err, "Error when binding with shell.\n");
            return err;
        }

        struct shell_params *par;
        err = aos_rpc_shell_request_params(my_shell_rpc, &par);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "aos_rpc_shell_request_params failed.\n");
            return err;
        }

        struct aos_rpc *serial_rpc = aos_rpc_get_serial_channel();

        if (!par->background) {
            err = aos_rpc_serial_get_window(serial_rpc, par->shell_pid);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "rpc_serial_get_window failed.\n");
                free(par);
                return err;
            }
        }

        // get redirection parameters
        char *std_in_path = par->io_redirection_paths;
        char *std_out_path = par->io_redirection_paths + strlen(par->io_redirection_paths) + 1;

        // set stdin, stdout
        if (strlen(std_in_path) > 0) {
            FILE *stream_in = fopen(std_in_path, "r");
            if (stream_in == NULL) {
                debug_printf("Could not open file %s for reading. Not redirecting input.\n", std_in_path);
            }
            stdin = stream_in ? stream_in : stdin;
        }

        if (strlen(std_out_path) > 0) {
            FILE *stream_out = fopen(std_out_path, "w");
            if (stream_out == NULL) {
                debug_printf("Could not open file %s for writing. Not redirecting output.\n", std_out_path);
            }
            stdout = stream_out ? stream_out : stdout;
        }


        free(par);

    }
    // all OK
    return SYS_ERR_OK;
}


/**
 *  \brief Initialise libbarrelfish, while disabled.
 *
 * This runs on the dispatcher's stack, while disabled, before the dispatcher is
 * setup. We can't call anything that needs to be enabled (ie. cap invocations)
 * or uses threads. This is called from crt0.
 */
void barrelfish_init_disabled(dispatcher_handle_t handle, bool init_dom_arg);
void barrelfish_init_disabled(dispatcher_handle_t handle, bool init_dom_arg)
{
    init_domain = init_dom_arg;
    disp_init_disabled(handle);
    thread_init_disabled(handle, init_dom_arg);
}
