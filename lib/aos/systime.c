/**
 * \file
 * \brief System time, constants and convertors
 *
 */

/*
 * Copyright (c) 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/systime.h>

/**
 * Modification Group C
 *
 * We have added 2 fixes that complete auto-configuration of systime_frequency
 * directly from the kernel.
 * - dispatcher->systime_frequency (added)
 *     - for spawned regular domains in sys_dispatcher_setup() in kernel/syscall.c (was provided)
 *     - for init: spawn_module() in kernel/startup.c (added)
 * - copy this frequency from dispatcher to this global variable
 *   in barrelfish_init_onthread()  (added)
 *
 * We know from boot output and from testing:
 * CortexA9 platform: System counter frequency is 300000000Hz
 * CortexA9 platform: Timeslice interrupt every 6000000 system ticks (20ms).
 * -> 300000 ticks / ms
 * see: actually per second needed: 300000000 ticks / s (Hz)
 *
 * Thus, in contrast to several documentation where ms or us are mentioned as
 * system time, on the given Pandaboard system the following conclusions hold:
 * - system time is always in cycles (including wakeup system)
 * - the following functions below are defined that systime_frequence is
 *   needed in Hz.
 */

/// Number of system ticks per one millisecond
/// pisch: no: based on the calculations below, the number of ticks / s are actually expected
systime_t systime_frequency = 1;


/// Convert nanoseconds to system ticks
systime_t ns_to_systime(uint64_t nanoseconds)
{
    uint64_t q, r;

    q = nanoseconds / 1000000000;
    r = nanoseconds % 1000000000;

    // Adding half a tick to round properly
    return q * systime_frequency + (r * systime_frequency + 500000000) / 1000000000;
}

/// Convert system ticks to nanoseconds
uint64_t systime_to_ns(systime_t time)
{
    systime_t q, r;

    q = time / systime_frequency;
    r = time % systime_frequency;

    // Adding half a nanosecond to round properly
    return q * 1000000000 + (r * 1000000000 + systime_frequency / 2) / systime_frequency;
}
