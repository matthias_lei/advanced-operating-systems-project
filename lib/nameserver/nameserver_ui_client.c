/**
 * Actual nameserver UI client (Apollo) functionality.
 *
 * NOTE: to be compatible with the established signature for built-in commands in the shell,
 * - fprintf(std_out,...) is used for printf()
 * - and printf() is used for things, that would regularly go to fprintf(stderr,...)
 *   alternatively, the DEBUG_ERR() construct is used.
 *
 * Group C. individual projects: nameserver
 * version 2017-12-21
 */

#include <nameserver_ui_client.h>
#include <nameserver_types.h>

#include <aos/aos.h>

#include <hashtable/serializable_key_value_store.h>

#include <getopt.h>

static void show_help(void)
{
    printf("\nUSAGE: apollo [-d] [-p <path>] [-k <key> -v <value>] [-l <key2> -w <value2>] [-h]\n");
    printf("-d detailed output\n");
    printf("-p <path>  absolute path or complete namme\n");
    printf("-k <key> -v <value>    for filter\n");
    printf("-l <key2> -w <value2>  for filter\n");
    printf("-h help\n\n");
}

int nameserver_ui_client(int argc, char **argv, FILE *std_in, FILE *std_out)
{
    fprintf(std_out, "\nApollo name service lookup tool\n");

    int retval = 1;

    bool detail_flag = false;
    bool path_flag = false;
    char *path = NULL;
    bool key_flag = false;
    char *key = NULL;
    bool value_flag = false;
    char *value = NULL;
    bool key2_flag = false;
    char *key2 = NULL;
    bool value2_flag = false;
    char *value2 = NULL;
    bool help_flag = false;
    bool fail_flag = false;

    int option = 0;
    while ((option = getopt(argc, argv, "dp:k:v:l:w:h")) != -1) {
        switch(option) {
            case 'd':
                detail_flag = true;
                break;

            case 'p':
                path_flag = true;
                path = optarg;
                break;

            case 'k':
                key_flag = true;
                key = optarg;
                break;

            case 'v':
                value_flag = true;
                value = optarg;
                break;

            case 'l':
                key2_flag = true;
                key2 = optarg;
                break;

            case 'w':
                value2_flag = true;
                value2 = optarg;
                break;

            case 'h':
                help_flag = true;
                break;
            default:
                printf("Unrecognized option: -%c\n", option);
                fail_flag = true;
                break;
        }
    }

    if (fail_flag || help_flag) {
        show_help();
        return 0;
    }


    // input check
    if (key_flag ^ value_flag) {
        printf("-k <key> and -v <value> must both be provided\n");
        show_help();
        return 1;
    }

    if (key2_flag ^ value2_flag) {
        printf("-l <key2> and -w <value2> must both be provided\n");
        show_help();
        return 1;
    }

    if (path_flag && (key_flag || key2_flag)) {
        printf("with -p <path> option, neither key/value nor key2/value2 can be used\n");
        show_help();
        return 1;
    }


    // data selection
    struct aos_rpc *name_rpc = aos_rpc_get_name_channel();
    if (!name_rpc) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not connect with name RPC channel\n");
        goto unroll_nothing;
    }

    service_handle_t *handles;
    size_t count;
    errval_t err = SYS_ERR_OK;

    if (path_flag) {
        err = aos_rpc_name_find_any_service_by_path(name_rpc, path, &handles, &count);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_name_enumerate_all_services()\n");
            goto unroll_nothing;
        }
    }
    else if (key_flag || key2_flag) {
        struct serializable_key_value_store *filter = create_kv_store(5);
        if (!filter) {
            DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "could not create filter\n");
            goto unroll_nothing;
        }
        if (key_flag) {
            kv_store_set(filter, key, value);
        }
        if (key2_flag) {
            kv_store_set(filter, key2, value2);
        }
        err = aos_rpc_name_find_any_service_by_filter(name_rpc, filter, &handles, &count);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_name_enumerate_all_services()\n");
            destroy_kv_store(&filter);
            goto unroll_nothing;
        }
    }
    else {
        err = aos_rpc_name_enumerate_all_services(name_rpc, &handles, &count);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "error in aos_rpc_name_enumerate_all_services()\n");
            goto unroll_nothing;
        }
    }


    // output
    fprintf(std_out, "\n%zu services registered:\n", count);


    // get all service info now to allow sorting later
    struct serializable_key_value_store **stores = calloc(count, sizeof(struct serializable_key_value_store));
    if (!stores) {
        DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "malloc failed\n");
        goto unroll_handles;
    }

    size_t i; // declared here for later rollback if needed
    for (i = 0; i < count; i++) {
        err = aos_rpc_name_get_service_info(name_rpc, handles[i], &stores[i]);
        if (err_is_fail(err)) {
            DEBUG_ERR(LIB_ERR_MALLOC_FAIL, "error in aos_rpc_name_get_service_info()\n");
            goto unroll_info;
        }
    }


    // sort
    aos_rpc_name_sort_info_ptr_array(name_rpc, stores, count);


    // print all
    for (size_t j = 0; j < count; j++) {
        const char *name = kv_store_get(stores[j], "name");
        if (detail_flag) {
            if (name) {
                fprintf(std_out, "\n%s:\n", name);
            }
            else {
                fprintf(std_out, "\n\n");
            }
            fprint_kv_store(std_out, stores[j]);
        }
        else {
            if (name) {
                fprintf(std_out, "%s\n", name);
            }
        }
    }

    // all OK
    retval = 0;

unroll_info:
    for (size_t j = 0; j < i; j++) {
        destroy_kv_store(&stores[j]);
    }
//unroll_stores:
    free(stores);
unroll_handles:
    free(handles);
unroll_nothing:
    return retval;
}
