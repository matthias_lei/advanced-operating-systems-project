/**
 * AOS Class 2017 Group C -- Individual Project: Nameserver Gaia
 *
 * This library implements a system wide nameserver for our Barrelfish OlympOS project.
 * It is mainly for services at the moment, but can also be used for other types of resources.
 *
 * See header file for detailed information
 *
 * version 2017-12-18, Pirmin Schmid
 */

#include <nameserver.h>

#include <collections/hash_table.h>

#include <aos_rpc_server/aos_rpc_server_common.h>

#include <aos/systime.h>
#include <aos/deferred.h>

// Implementation details
// ----------------------
// multiple types of hashmaps / hashtables are used due to different functionality
// - hashtable/serializable_key_value_store (bases on hashtable/hashtable)
//   string keys and string values (in simplified kv_store form)
//   -> used for the key value store that must be serializable
//      has added update (set) functionality and iterator compared to hashtable/hashtable
//
// - collections/hash_table: uint64_t keys and pointer values
//   used for quick lookup of service definitions based on service_handle_t
//
// - hashtable/serializable_key_value_store is used to keep track of *next* max enumeration
//   NOTE: the size_t values are stored as "pointers".
//   This avoids doing lots of integer <-> string conversions.
//   This also matches the semantics: not found -> next is 0, otherwise next is the value to use.
//   The key value store is used because the original hashtable/hashtable does not offer
//   update (set) functionality, which would be ugly to add without changing the dictionary interface
//   that is also used by other implementations, e.g. multimap, not available to us.
//
// A conventional list is used for prefix lookup and initial filtering
// A miniature set implementation is used for filtering based on key/value combinations

//=== private data / interface =====================================================================

// this flag is used to recognize Dionysos upon registration
// and then use the opportunity to connect with the memory service
static volatile bool memory_ok = false;

#define ADDITIONAL_KEY_VALAUE_PAIRS 8
static const char *name_key = "name";
static const char *short_name_key = "short_name";
static const char *class_key = "class";
static const char *type_key = "type";
static const char *subtype_key = "subtype";
static const char *enumeration_key = "enumeration";
static const char *bandwidth_key = "bandwidth";
static const char *core_key = "core";

static const char *empty = "";
static const char *low = "low";
static const char *high = "high";
static const char *zero = "0";
static const char *one = "1";

// see used low-level implementation hashtable/hashtable
// the value corresponds there more to a "number of buckets" configuration value
// it's often good to pick prime numbers for them.
#define MAX_ENUMERATORS_MAP_CAPACITY 1013

// arbitrary choice
#define MAX_ENUMERATOR_DIGITS 12

// namespace (number of slashes)
#define NAMESPACE_MIN_NUMBER_OF_LAYERS 2
#define NAMESPACE_MAX_NUMBER_OF_LAYERS 3

struct service_def {
    // for prefix searches, it's easiest to have a list
    // it can be optimized into skip list or other data structures in the future
    struct service_def *prev;
    struct service_def *next;

    service_handle_t handle;
    uint64_t registration_key;
    uint64_t last_ping; // is stored in ms

    // flag that this is the nameserver itself
    bool myself;

    // note: the hashmap does not actually store any strings
    // thus, it's best to keep them all just here
    char *name;
    char *short_name;
    char *class;
    char *type;
    char *subtype;
    size_t enumeration;
    char *enumeration_str;

    coreid_t core_id;
    enum aos_rpc_interface interface;
    enum aos_rpc_chan_driver contact_chan_type;
    bool high_bandwidth_service; // helps making decision for LMP or FLMP upon binding

    struct capref contact_cap;
    struct aos_rpc *contact_rpc;

    // the actual store
    struct serializable_key_value_store *store;

    // just keeping the data alive that was used for cloning
    struct serialized_key_value_store *store_buf;
};

//--- simple unsorted list at the moment -----------------------------------------------------------

struct service_def_list {
    size_t size;
    struct service_def *head;
};

static void list_insert(struct service_def_list *list, struct service_def *node)
{
    assert(list);
    assert(node);
    assert(!node->prev);
    assert(!node->next);

    if (list->head) {
        node->next = list->head;
        list->head->prev = node;

    }

    list->head = node;
    list->size++;
}

static void list_remove(struct service_def_list *list, struct service_def *node)
{
    assert(list);
    assert(node);

    struct service_def *prev = node->prev;
    struct service_def *next = node->next;
    if (list->head == node) {
        list->head = node->next;
    }
    if (prev) {
        prev->next = node->next;
    }
    if (next) {
        next->prev = node->prev;
    }
    list->size--;
}

typedef void (*service_def_list_iterator_lambda_t) (const struct service_def *e, void *arg);

static void list_iterate(struct service_def_list *list, service_def_list_iterator_lambda_t lambda, void *arg)
{
    struct service_def *current = list->head;
    while (current) {
        lambda(current, arg);
        current = current->next;
    }
}

//--- registry -------------------------------------------------------------------------------------

struct service_registry {
    // list for prefix searches and man data storage / ownership
    // also short_name lookup uses this at the moment
    struct service_def_list defs;

    // hashmap with handle keys
    collections_hash_table *handle_map;

    // next enumerator value per prefix (default 0 if not found)
    struct serializable_key_value_store *next_enumerators;

    bool initialized;
};

/**
 * note: currently, there is one global lock on all registry operations
 */
struct thread_mutex r_mutex = THREAD_MUTEX_INITIALIZER;
static struct service_registry r; // is initialized to all 0 by definition

static service_handle_t last_handle = 0;

static void handle_map_free(void *value)
{
    // this is a no-op on purpose
    // the map is NOT responsible in handling memory (no ownership)
    return;
}


/**
 * copied function from hashtable.c
 * \brief get a hash value for a string
 * \param str the string
 * \return the hash value
 */
static inline int hash(const char *str, size_t key_len)
{
    register int _hash = 5381;
    register int _c;

    for(size_t i = 0; i < key_len; i++) {
        _c = str[i];
        _hash = ((_hash << 5) + _hash) + _c;
    }
    return _hash;
}

// also removes terminal /
// len is used as input and output
static bool check_name(char *name, size_t *len, size_t *slash_count)
{
    *slash_count = 0;

    while (0 < *len && name[*len - 1] == '/') {
        *len -= 1;
    }

    if (*len < 1) {
        return false;
    }

    if (name[0] != '/') {
        return false;
    }

    for (size_t i = 0; i < *len; i++) {
        const char c = name[i];
        if ('a' <= c && c <= 'z') {
            continue;
        }
        if ('0' <= c && c <= '9') {
            continue;
        }
        if (c == '/') {
            (*slash_count)++;
            continue;
        }
        if (c == '_') {
            continue;
        }

        return false;
    }

    return true;
}


// len is used as input and output
static bool check_short_name(char *name, size_t *len)
{
    for (size_t i = 0; i < *len; i++) {
        const char c = name[i];
        if ('a' <= c && c <= 'z') {
            continue;
        }

        return false;
    }

    return true;
}


/**
 * initializes service def, checks enumeration value, clones the key value store,
 * and prepares reply message
 * \return is NULL in case of error
 */
static struct service_registration_reply *service_def_init(const struct service_registration_request *request,
                                                           struct capref contact_cap)
{
    thread_mutex_lock_nested(&r_mutex);
    struct service_def *d = calloc(1, sizeof(*d));
    if (!d) {
        goto error_exit;
    }

    struct service_registration_reply *ret = calloc(1, sizeof(*ret));
    if (!ret) {
        goto unroll_def_alloc;
    }

    // the easiest and fastest way to clone a key value store is to serialize it
    // this also allows the buffer to provide the data space needed for the keys and values in the store
    // remember: the store itself does not keep track of memory

    d->store_buf = serialize_kv_store(request->kv_store);
    if (!d->store_buf) {
        goto unroll_retval_alloc;
    }

    d->store = deserialize_kv_store(d->store_buf, ADDITIONAL_KEY_VALAUE_PAIRS);
    if (!d->store) {
        goto unroll_store_buf;
    }

    // handle
    d->handle = ++last_handle;
    d->core_id = request->core_id;
    d->interface = request->interface;
    d->contact_chan_type = request->contact_chan_type;
    d->high_bandwidth_service = request->high_bandwidth_service;
    d->contact_cap = contact_cap;
    d->myself = false;

    // name handling & find enumerator & create key from hash
    size_t name_len = strlen(request->abs_path);
    d->name = calloc(1, name_len + MAX_ENUMERATOR_DIGITS + 1); // +1 for the slash
    if (!d->name) {
        goto unroll_store;
    }
    memcpy(d->name, request->abs_path, name_len);

    size_t slash_count = 0;
    if (!check_name(d->name, &name_len, &slash_count)) {
        goto unroll_name;
    }

    if (slash_count < NAMESPACE_MIN_NUMBER_OF_LAYERS || NAMESPACE_MAX_NUMBER_OF_LAYERS < slash_count) {
        goto unroll_name;
    }

    size_t enumerator = 0;
    char *key = d->name;
    // removing the const modifier here; need to manually assure that we know what we are doing.
    // note: the value for the next enumerator is stored in the list
    char *enumerator_pseudo_ptr = (char *)kv_store_get(r.next_enumerators, key);
    if (enumerator_pseudo_ptr) {
        enumerator = (size_t)enumerator_pseudo_ptr;
    }
    else {
        // we need a key string for the hashtable that can "survive" even
        // the case that temporarily all registered datasets with the same path
        // have been deleted/removed
        key = strdup(d->name);
        if (!key) {
            goto unroll_name;
        }
    }

    // store value of next enumerator
    enumerator_pseudo_ptr = (char *)(enumerator + 1);
    kv_store_set(r.next_enumerators, key, enumerator_pseudo_ptr);

    char enumerator_str_buf[MAX_ENUMERATOR_DIGITS + 1];
    sprintf(enumerator_str_buf, "%zu", enumerator);

    // adjust name
    d->name[name_len] = '/';
    strcpy(d->name + name_len + 1, enumerator_str_buf);
    d->registration_key = hash(d->name, strlen(d->name));

    // short name with identical enumerator
    size_t sname_len = strlen(request->short_name_prefix);
    d->short_name = calloc(1, sname_len + MAX_ENUMERATOR_DIGITS);
    if (!d->short_name) {
        goto unroll_name;
    }
    strcpy(d->short_name, request->short_name_prefix);

    if (!check_short_name(d->short_name, &sname_len)) {
        goto unroll_short_name;
    }
    strcpy(d->short_name + sname_len, enumerator_str_buf);

    // integrate name and short name into KV store
    kv_store_set(d->store, name_key, d->name);
    kv_store_set(d->store, short_name_key, d->short_name);
    kv_store_set(d->store, enumeration_key, d->name + name_len + 1);

    // this is actually the buffer for all the following substrings
    char *class_str = strdup(d->name);
    if (!class_str) {
        goto unroll_short_name;
    }

    char *dummy = strtok(class_str, "/");
    if (!dummy) {
        goto skip_classification;
    }
    kv_store_set(d->store, class_key, dummy);

    dummy = strtok(NULL, "/");
    if (!dummy) {
        goto skip_classification;
    }
    kv_store_set(d->store, type_key, dummy);

    if (slash_count == NAMESPACE_MAX_NUMBER_OF_LAYERS) {
        dummy = strtok(NULL, "/");
        if (!dummy) {
            goto skip_classification;
        }
        kv_store_set(d->store, subtype_key, dummy);
    }
    else {
        kv_store_set(d->store, subtype_key, empty);
    }

skip_classification:

    // core: currently adjusted / simplified to known 2 core situation
    if (d->core_id == 0) {
        kv_store_set(d->store, core_key, zero);
    }
    else {
        kv_store_set(d->store, core_key, one);
    }

    if (d->high_bandwidth_service) {
        kv_store_set(d->store, bandwidth_key, high);
    }
    else {
        kv_store_set(d->store, bandwidth_key, low);
    }

    // establish a channel -- but not to self
    if (d->contact_chan_type == AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP) {
        if (strcmp(request->abs_path, "/core/name") == 0) {
            d->contact_rpc = NULL;
            d->myself = true;
        }
        else {
            debug_printf("%s: invalid contact_chan_type. no registration\n", __func__);
            goto unroll_class_str;
        }
    }
    else if (d->contact_chan_type == AOS_RPC_CHAN_DRIVER_UMP) {
        // no connection here
    }
    else {
        errval_t err = aos_rpc_connect_with_service(contact_cap, d->interface, d->contact_chan_type,
                                                    AOS_RPC_ROUTING_SET(d->core_id, disp_get_core_id()),
                                                    &d->contact_rpc, NULL);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "aos_rpc_connect_with_service() failed.\n");
            goto unroll_class_str;
        }
    }

    // insert def into list
    list_insert(&r.defs, d);

    // insert def into service_handle map
    collections_hash_insert(r.handle_map, d->handle, d);

    // complete info in return struct
    ret->handle = d->handle;
    ret->registration_key = d->registration_key;
    ret->enumeration = d->enumeration;
    ret->name = d->name;
    ret->short_name = d->short_name;

    // update with current systime
    d->last_ping = systime_to_ms(systime_now());

    // all OK
    thread_mutex_unlock(&r_mutex);
    return ret;

unroll_class_str:
    free(class_str);
unroll_short_name:
    free(d->short_name);
unroll_name:
    free(d->name);
unroll_store:
    free(d->store);
unroll_store_buf:
    free(d->store_buf);
unroll_retval_alloc:
    free(ret);
unroll_def_alloc:
    free(d);
error_exit:
    thread_mutex_unlock(&r_mutex);
    return NULL;
}

//=== public interface =============================================================================

//--- initialization -------------------------------------------------------------------------------

bool init_nameserver(void)
{
    thread_mutex_lock_nested(&r_mutex);
    if (r.initialized) {
        thread_mutex_unlock(&r_mutex);
        return true;
    }

    r.defs.size = 0;
    r.defs.head = NULL;

    collections_hash_create(&r.handle_map, handle_map_free);
    if (!r.handle_map) {
        goto error_exit;
    }

    r.next_enumerators = create_kv_store(MAX_ENUMERATORS_MAP_CAPACITY);
    if (!r.next_enumerators) {
        goto unroll_handle_map;
    }

    r.initialized = true;
    thread_mutex_unlock(&r_mutex);
    return true;


unroll_handle_map:
    collections_hash_release(r.handle_map);
error_exit:
    thread_mutex_unlock(&r_mutex);
    return false;
}


//--- service side: resource comes first -----------------------------------------------------------

//--- registration ---

errval_t service_register(const struct service_registration_request *request,
                          struct capref contact_cap,
                          struct service_registration_reply **ret_reply)
{
    assert(request);
    assert(ret_reply);
    assert(r.initialized);

    errval_t err = service_register_part1(request, contact_cap, ret_reply);
    if (err_is_fail(err)) {
        return err;
    }

    struct service_registration_reply *reply = *ret_reply;
    err = service_register_part2(reply->handle, reply->registration_key);
    if (err_is_fail(err)) {
        return err;
    }

    return SYS_ERR_OK;
}

errval_t service_register_part1(const struct service_registration_request *request,
                                struct capref contact_cap,
                                struct service_registration_reply **ret_reply)
{
    assert(request);
    assert(ret_reply);
    assert(r.initialized);

    *ret_reply = service_def_init(request, contact_cap);
    if (!*ret_reply) {
        debug_printf("%s: error in service_def_init()\n", __func__);
        return LIB_ERR_MALLOC_FAIL;
    }

    return SYS_ERR_OK;
}

__attribute__((unused))
static int control_function(void *arg)
{
    debug_printf("%s: started\n", __func__);

    while (true) {
        // period expected in ms
        // debug_printf("%s: running\n", __func__);
        nameservice_check(NAMESERVER_CHECK_PERIOD * 1000);
        barrelfish_msleep(NAMESERVER_CHECK_PERIOD * 1000);
    }

    // never reached
    return 0;
}

errval_t service_register_part2(service_handle_t handle, uint64_t registration_key)
{
    assert(r.initialized);

    errval_t err = SYS_ERR_OK;

    // lookup needed information
    thread_mutex_lock_nested(&r_mutex);
    const struct service_def *d = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    if (   (d->contact_chan_type != AOS_RPC_CHAN_DRIVER_UMP)
        && (memory_ok) ) {
        thread_mutex_unlock(&r_mutex);
        return SYS_ERR_OK;
    }

    if (d->registration_key != registration_key) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }

    struct capref contact_cap = d->contact_cap;
    struct aos_rpc *contact_rpc = d->contact_rpc; // is set in case of LMP
    enum aos_rpc_interface interface = d->interface;
    enum aos_rpc_chan_driver contact_chan_type = d->contact_chan_type;
    coreid_t core_id = d->core_id;
    thread_mutex_unlock(&r_mutex);

    if (   (contact_chan_type != AOS_RPC_CHAN_DRIVER_UMP)
        && (!memory_ok) ) {

        if (interface != AOS_RPC_INTERFACE_MEMORY) {
            return SYS_ERR_OK;
        }

        // register with memory service
        memory_ok = true;
        set_memory_rpc(contact_rpc);
        struct aos_rpc *mem_rpc  = aos_rpc_get_memory_channel();
        if (!mem_rpc) {
            debug_printf("%s: could not establish connection with memory channel", __func__);
            return LIB_ERR_SHOULD_NOT_GET_HERE;
        }
        // late switching system to use as much from provided 1 MiB as possible
        set_has_memory_service_connection();


        /* code toggle: currently deactivated; preliminary tests show working feature; not yet tested enough
        // with memory service available, also the check thread can be launched
        struct thread *control_thread = thread_create(control_function, NULL);
        if (!control_thread) {
            debug_printf("Could not launch control thread; no auto-removal of inactive services\n");
            // but we do not stop Gaia, at least not for the current test setting
        }
        //*/

        return SYS_ERR_OK;
    }

    err = aos_rpc_connect_with_service(contact_cap, interface, contact_chan_type,
                                       AOS_RPC_ROUTING_SET(core_id, disp_get_core_id()),
                                       &contact_rpc, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "aos_rpc_connect_with_service() failed.\n");
        return err;
    }

    thread_mutex_lock_nested(&r_mutex);
    struct service_def *d2 = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    d2->contact_rpc = contact_rpc;
    thread_mutex_unlock(&r_mutex);

    return SYS_ERR_OK;
}

//--- deregistration ---

errval_t service_deregister(service_handle_t handle, uint64_t registration_key)
{
    errval_t err = SYS_ERR_OK;

    // lookup needed information
    thread_mutex_lock_nested(&r_mutex);
    struct service_def *d = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    if (registration_key != d->registration_key) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    // remove from list
    list_remove(&r.defs, d);

    // remove from service_handle map
    collections_hash_delete(r.handle_map, d->handle);

    // during testing / demonstration / debugging
    debug_printf("%s: service %s deregistered\n", __func__, d->name);

    thread_mutex_unlock(&r_mutex);

    return err;
}


//--- ping ---

errval_t service_ping(service_handle_t handle, uint64_t registration_key)
{
    errval_t err = SYS_ERR_OK;

    // lookup needed information
    thread_mutex_lock_nested(&r_mutex);
    struct service_def *d = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    if (registration_key != d->registration_key) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    // update systime
    d->last_ping = systime_to_ms(systime_now());

    thread_mutex_unlock(&r_mutex);

    return err;
}

//--- update key value store ---

//--- update_closure: consists of _data and _lambda
//    NOTE: this lambda function iterates on the KV store and not on the data sets
struct update_data {
    struct serializable_key_value_store *store;
};

static void update_lambda(const struct _ht_entry *entry, void *arg)
{
    struct update_data *d = arg;

    kv_store_set(d->store, entry->key, entry->value);
}

errval_t service_update(service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store)
{
    errval_t err = SYS_ERR_OK;

    // lookup needed information
    thread_mutex_lock_nested(&r_mutex);
    struct service_def *d = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    if (registration_key != d->registration_key) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    struct update_data data = {
        .store = d->store
    };

    kv_store_iterate(kv_store, update_lambda, &data);

    // update systime
    d->last_ping = systime_to_ms(systime_now());

    thread_mutex_unlock(&r_mutex);

    return err;
}

//--- client side: resource comes second -----------------------------------------------------------

//--- lookup / find ---

// basic functions: detected service must be unique, error otherwise
errval_t lookup_service_by_path(const char *abs_path_prefix, service_handle_t *ret_handle)
{
    assert(abs_path_prefix);
    assert(ret_handle);

    service_handle_t *handles;
    size_t count;
    errval_t err = find_any_service_by_path(abs_path_prefix, &handles, &count);
    if (err_is_fail(err)) {
        return err;
    }

    switch (count) {
        case 0:
            err = LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
            break;

        case 1:
            *ret_handle = handles[0];
            break;

        default:
            // a unique name is needed for lookup_service_by_path()
            err = LIB_ERR_NAMESERVICE_INVALID_NAME;
            break;
    }

    // free of the list in OK and error cases
    if (handles) {
        free(handles);
    }
    return err;
}


//--- lookup_short_name_closure: consists of _data and _lambda
struct lookup_short_name_data {
    const char *sname;
    size_t sname_len;

    service_handle_t *handles;
    size_t count; // also indicates the next index position
};

static void lookup_short_name_lambda(const struct service_def *e, void *arg)
{
    struct lookup_short_name_data *d = arg;

    if (strncmp(d->sname, e->short_name, d->sname_len) == 0) {
        d->handles[d->count] = e->handle;
        d->count += 1;
    }
}

errval_t lookup_service_by_short_name(const char *short_name, service_handle_t *ret_handle)
{
    // To avoid 2 iterations over the linked list, an array is allocated
    // that is large enough to potentially hold handles for the entire list.
    // This is actually a real use case: see enumerate_all_services()
    // that is run by find_any_service_by_path("/",...)

    thread_mutex_lock_nested(&r_mutex);

    *ret_handle = 0;

    errval_t err = SYS_ERR_OK;

    if (r.defs.size == 0) {
        err = LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
        goto unroll_lock;
    }

    service_handle_t *handles = calloc(1, r.defs.size * sizeof(service_handle_t));
    if (!handles) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_lock;
    }

    struct lookup_short_name_data data = {
        .sname = short_name,
        .sname_len = strlen(short_name),
        .handles = handles,
        .count = 0,
    };

    list_iterate(&r.defs, lookup_short_name_lambda, &data);
    thread_mutex_unlock(&r_mutex);

    switch (data.count) {
        case 0:
            err = LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
            break;

        case 1:
            *ret_handle = handles[0];
            break;

        default:
            // a unique name is needed for lookup_service_by_short_name()
            err = LIB_ERR_NAMESERVICE_INVALID_NAME;
            break;
    }

    // free of the list in OK and error cases
    if (handles) {
        free(handles);
    }

    return err;

unroll_lock:
    thread_mutex_unlock(&r_mutex);
    return err;
}


//--- find_prefix_closure: consists of _data and _lambda
struct find_prefix_data {
    const char *prefix;
    size_t prefix_len;

    service_handle_t *handles;
    size_t count; // also indicates the next index position
};

static void find_prefix_lambda(const struct service_def *e, void *arg)
{
    struct find_prefix_data *d = arg;

    if (strncmp(d->prefix, e->name, d->prefix_len) == 0) {
        d->handles[d->count] = e->handle;
        d->count += 1;
    }
}

// advanced functions: may return 0 to many service handles
errval_t find_any_service_by_path(const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count)
{
    // To avoid 2 iterations over the linked list, an array is allocated
    // that is large enough to potentially hold handles for the entire list.
    // This is actually a real use case: see enumerate_all_services()
    // that is run by find_any_service_by_path("/",...)

    thread_mutex_lock_nested(&r_mutex);

    errval_t err = SYS_ERR_OK;
    *ret_handles = NULL;
    *ret_count = 0;

    if (r.defs.size == 0) {
        goto unroll_lock;
    }

    service_handle_t *handles = calloc(1, r.defs.size * sizeof(service_handle_t));
    if (!handles) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_lock;
    }

    struct find_prefix_data data = {
        .prefix = abs_path_prefix,
        .prefix_len = strlen(abs_path_prefix),
        .handles = handles,
        .count = 0,
    };

    list_iterate(&r.defs, find_prefix_lambda, &data);

    // ownership transfer to client
    *ret_handles = data.handles;
    *ret_count = data.count;

unroll_lock:
    thread_mutex_unlock(&r_mutex);
    return err;
}


//--- find_prefix_closure: consists of _data and _lambda
struct find_filter_data {
    const char *key;
    const char *value;

    service_handle_t *handles;
    size_t count; // also indicates the next index position
};

static void find_filter_lambda(const struct service_def *e, void *arg)
{
    struct find_filter_data *d = arg;

    const char *e_val = kv_store_get(e->store, d->key);
    if (e_val) {
        if (strcmp(e_val, d->value) == 0) {
        d->handles[d->count] = e->handle;
        d->count += 1;
        }
    }
}

// runs one iteration of the search (one key-value pair)
// lock has already been taken by the parent function
static errval_t find_any_service_by_filter_helper(const char *key, const char *value,
                                                  service_handle_t **ret_handles, size_t *ret_count)
{
    // To avoid 2 iterations over the linked list, an array is allocated
    // that is large enough to potentially hold handles for the entire list.
    // This is actually a real use case: see enumerate_all_services()
    // that is run by find_any_service_by_path("/",...)

    errval_t err = SYS_ERR_OK;
    *ret_handles = NULL;
    *ret_count = 0;

    if (r.defs.size == 0) {
        goto unroll_none;
    }

    service_handle_t *handles = calloc(1, r.defs.size * sizeof(service_handle_t));
    if (!handles) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_none;
    }

    struct find_filter_data data = {
        .key = key,
        .value = value,
        .handles = handles,
        .count = 0,
    };

    list_iterate(&r.defs, find_filter_lambda, &data);

    // ownership transfer to client
    *ret_handles = data.handles;
    *ret_count = data.count;

unroll_none:
    return err;
}

// assumes each handle to be unique in each list, which is given
// we could even assume stronger that the order is the same in both lists
// this is not used at the moment, thus O(n^2) algorithm
static void result_and(service_handle_t *target, size_t *target_count,
                       service_handle_t *a, size_t a_count,
                       service_handle_t *b, size_t b_count)
{
    assert(target);
    assert(target_count);
    assert(a);
    assert(b);

    *target_count = 0;
    if (a_count == 0 || b_count == 0) {
        return;
    }

    size_t count = 0;
    for (size_t i = 0; i < a_count; i++) {
        service_handle_t x = a[i];
        for (size_t j = 0; j < b_count; j++) {
            if (x == b[j]) {
                target[count++] = x;
                break;
            }
        }
    }
    *target_count = count;
}

//--- find_prefix_closure: consists of _data and _lambda
//    NOTE: this lambda function iterates on the KV store and not on the data sets
struct find_big_filter_data {
    size_t n; // to keep things simple, memory is always allocated for the full list size

    service_handle_t *handles;
    size_t count; // also indicates the next index position
};

static void find_big_filter_lambda(const struct _ht_entry *entry, void *arg)
{
    struct find_big_filter_data *d = arg;

    service_handle_t *b = NULL;
    size_t b_count = 0;

    errval_t err = find_any_service_by_filter_helper(entry->key, entry->value, &b, &b_count);
    if (err_is_fail(err)) {
        // not much that can be done
        return;
    }

    if (!d->handles) {
        d->handles = b;
        d->count = b_count;
        return;
    }

    // merge
    service_handle_t *a = d->handles;
    size_t a_count = d->count;

    d->handles = calloc(1, d->n * sizeof(service_handle_t));
    if (!d->handles) {
        // not much that can be done...
        return;
    }
    d->count = 0;
    result_and(d->handles, &d->count, a, a_count, b, b_count);
    free(a);
    free(b);
}

errval_t find_any_service_by_filter(const struct serializable_key_value_store *filter,
                                    service_handle_t **ret_handles, size_t *ret_count)
{

    thread_mutex_lock_nested(&r_mutex);

    errval_t err = SYS_ERR_OK;
    *ret_handles = NULL;
    *ret_count = 0;

    if (r.defs.size == 0) {
        goto unroll_lock;
    }

    struct find_big_filter_data data = {
        .n = r.defs.size,
        .handles = NULL, // this is different in comparison with the former lambda functions
        .count = 0,
    };

    // iterating over the filter
    kv_store_iterate(filter, find_big_filter_lambda, &data);

    // ownership transfer to client
    *ret_handles = data.handles;
    *ret_count = data.count;

unroll_lock:
    thread_mutex_unlock(&r_mutex);
    return err;
}


//--- get info ---

errval_t get_service_info_serialized(service_handle_t handle, struct serialized_key_value_store **ret_serialized_info)
{
    assert(ret_serialized_info);

    *ret_serialized_info = NULL;

    // lookup needed information
    thread_mutex_lock_nested(&r_mutex);
    struct service_def *d = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    // the easiest and fastest way to clone a key value store is to serialize it
    // this also allows the buffer to provide the data space needed for the keys and values in the store
    // remember: the store itself does not keep track of memory

    struct serialized_key_value_store *serialized_store = serialize_kv_store(d->store);
    if (!serialized_store) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_MALLOC_FAIL;
    }

    thread_mutex_unlock(&r_mutex);

    *ret_serialized_info = serialized_store;
    return SYS_ERR_OK;
}

errval_t get_service_info(service_handle_t handle, struct serializable_key_value_store **ret_info)
{
    assert(ret_info);

    *ret_info = NULL;
    struct serialized_key_value_store *serialized_store;
    errval_t err = get_service_info_serialized(handle, &serialized_store);
    if (err_is_fail(err)) {
        return err;
    }

    *ret_info = deserialize_kv_store(serialized_store, 0);
    if (!(*ret_info)) {
        err = LIB_ERR_MALLOC_FAIL;
        goto unroll_store_buf;
    }

    return err;

unroll_store_buf:
    free(serialized_store);
    return err;
}


//--- bind ---

// state is only needed to handle nameserver itself
// this is the reason that this function is sending reply messages directly!
errval_t bind_service_lowlevel_part1(service_handle_t handle, coreid_t core_id,
                                     routing_info_t *ret_routing_info,
                                     enum aos_rpc_interface *ret_interface,
                                     size_t *ret_enumerator,
                                     enum aos_rpc_chan_driver *ret_chan_type,
                                     struct capref *ret_service_cap, bool *ret_self,
                                     void **ret_data)
{
    assert(ret_routing_info);
    assert(ret_interface);
    assert(ret_enumerator);
    assert(ret_chan_type);
    assert(ret_service_cap);
    assert(ret_self);
    assert(ret_data);

    errval_t err = SYS_ERR_OK;

    // lookup needed information
    thread_mutex_lock_nested(&r_mutex);
    const struct service_def *d = collections_hash_find(r.handle_map, handle);
    if (!d) {
        thread_mutex_unlock(&r_mutex);
        return LIB_ERR_NAMESERVICE_UNKNOWN_NAME;
    }

    coreid_t service_core_id = d->core_id;
    enum aos_rpc_interface interface = d->interface;
    *ret_enumerator = d->enumeration;
    bool high_bandwidth_service = d->high_bandwidth_service;
    struct aos_rpc *rpc = d->contact_rpc;
    *ret_self = d->myself;

    thread_mutex_unlock(&r_mutex);

    // request new cap from service
    // note: rpc is used after unlock on purpose
    // if -- by chance -- the registered rpc is destroyed while communicating,
    // there should be some error code from the message stack
    // but at least, not the entire nameserver is locked until that happens
    // see optimistic view: general case in the system: registered services typically remain active

    bool same_core = core_id == service_core_id;

    // additional condition: LMP endpoints cannot be transferred to other cores
    // thus: LMP only on the core where nameserver is running at the moment
    same_core = same_core && (core_id == disp_get_core_id());

    enum aos_rpc_chan_driver chan_type = same_core ?
            (high_bandwidth_service ? AOS_RPC_CHAN_DRIVER_FLMP : AOS_RPC_CHAN_DRIVER_LMP) :
            AOS_RPC_CHAN_DRIVER_UMP;
    if (*ret_self) {
        err = aos_rpc_local_request_new_endpoint_part1(chan_type, ret_service_cap, ret_data);
    }
    else {
        err = aos_rpc_request_new_endpoint(rpc, chan_type, ret_service_cap);
        *ret_data = NULL;
        if (err_is_fail(err)) {
            return err;
        }
    }

    *ret_routing_info = AOS_RPC_ROUTING_SET(service_core_id, core_id);
    *ret_interface = interface;
    *ret_chan_type = chan_type;
    return err;
}


void bind_service_lowlevel_part2(enum aos_rpc_chan_driver chan_type,
                                 struct capref cap, bool self, void *data)
{
    if (!self) {
        return;
    }

    aos_rpc_local_request_new_endpoint_part2(chan_type, cap, data);
}


//--- find_prefix_closure: consists of _data and _lambda
struct check_data {
    uint64_t current;
    uint64_t cutoff;

    service_handle_t *handles;
    uint64_t *keys;
    size_t count; // also indicates the next index position
};

static void check_lambda(const struct service_def *e, void *arg)
{
    struct check_data *d = arg;

    if (e->myself) {
        return;
    }

    // calculation of delta and then comp with cutoff is needed
    // to avoid problems with system clocks potentially starting at arbitrary values
    // I agree, a bit on the theoretical side here, but better do it properly
    uint64_t delta = d->current - e->last_ping;
    if (delta > d->cutoff) {
        d->handles[d->count] = e->handle;
        d->keys[d->count] = e->registration_key;
        d->count += 1;
    }
}

void nameservice_check(uint64_t cutoff_delta_ms)
{
    thread_mutex_lock_nested(&r_mutex);

    if (r.defs.size == 0) {
        goto unroll_lock;
    }

    // note: it would not be good to modify the list while iterating over it
    // thus: 1) collect entries to be deleted
    //       2) actually delete them

    service_handle_t *handles = calloc(1, r.defs.size * sizeof(service_handle_t));
    if (!handles) {
        goto unroll_lock;
    }

    uint64_t *keys = calloc(1, r.defs.size * sizeof(uint64_t));
    if (!keys) {
        goto unroll_handles;
    }

    struct check_data data = {
        .current = systime_to_ms(systime_now()),
        .cutoff = cutoff_delta_ms,
        .handles = handles,
        .keys = keys,
        .count = 0,
    };

    list_iterate(&r.defs, check_lambda, &data);

    for (size_t i = 0; i < data.count; i++) {
        service_deregister(handles[i], keys[i]);
    }

// unroll_keys:
    free(keys);
unroll_handles:
    free(handles);
unroll_lock:
    thread_mutex_unlock(&r_mutex);
}
