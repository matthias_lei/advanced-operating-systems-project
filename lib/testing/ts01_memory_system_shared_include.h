/**
 * static data and code used in both, ti and tp implementations of the library.
 * version 2017-10-25, GroupC
 */

#ifndef LIB_TESTING_TS01_MEMORY_SYSTEM_INCLUDE_H_
#define LIB_TESTING_TS01_MEMORY_SYSTEM_INCLUDE_H_

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <aos/aos.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>


//--- each test definition should follow this principle

// this test can be removed after the first real tests are here
static const char *hello_world_description = "Hello, World! from the memory system test system";

static int hello_world_test(void) {
    debug_printf("%s\n", hello_world_description);
    return 0;
}


//--- some helper functions for all tests

/**
 * returns 1 if alignment is OK
 *         0 if not
 */
static int check_alignment(genpaddr_t address, size_t alignment) {
    genpaddr_t a = (genpaddr_t)alignment;
    return (address & (a - 1)) == 0;
}

/**
 * \brief  size and alignment are separate to also check how the system reacts on not-allowed calls
 *         errval_t is returned by reference
 * \return values of this function are.
 *         0 OK
 *         1 error during allocation (can be expected) -> check for that in the calling function
 *         2 error during frame_identify -> never OK
 *         3 successfully allocated block does not meet size criterion -> never OK
 *         4 successfully allocated block does not meet alignment criterion
 *           note: it's OK to have size and/or alignment > required size / alignment
 */
__attribute__((unused))
static int allocate_and_assert(size_t size, size_t alignment,
                               size_t expected_size, size_t expected_alignment,
                               struct capref *ret_cap, errval_t *ret_error) {
    *ret_error = ram_alloc_aligned(ret_cap, size, alignment);
    if (!err_is_ok(*ret_error)) {
        DEBUG_ERR(*ret_error, "in ram_alloc_aligned()");
        return 1;
    }

    struct frame_identity info;
    *ret_error = frame_identify(*ret_cap, &info);
    if (!err_is_ok(*ret_error)) {
        DEBUG_ERR(*ret_error, "in frame_identify()");
        return 2;
    }

    if (info.bytes < expected_size) {
        debug_printf("Error: expected %u B, but received %" PRIuGENSIZE " B.\n", size, info.bytes);
        return 3;
    }

    if (!check_alignment(info.base, expected_alignment)) {
        debug_printf("Error: expected alignment of %u, but received address with lower alignment 0x%" PRIxGENPADDR ".\n", alignment, info.base);
        return 4;
    }

    return 0;
}


//--- a key test that shall also be available in TP

//running out of slab space --> allocate many many nodes
#define REFILL_TEST_N 333
static const char *milestone1_running_out_of_slab_and_slot_space_description = "MM: Milestone 1: Allocating 333 RAM capabilities (test refill mechanisms)";
static int milestone1_running_out_of_slab_and_slot_space_test(void) {

#ifdef __INIT__
    debug_printf("mm nodes before testing\n");
    mm_dump_mmnodes(&aos_mm);
#endif

    size_t request_size = 4096;
    size_t request_alignment = 4096;

    static struct capref caprefs[REFILL_TEST_N];

    debug_printf("allocating & freeing %d blocks of size %d KiB.\n", REFILL_TEST_N, request_size / 1024);
    debug_printf("this puts stress on all parts: slab allocation/refill, slot allocation/refill, memory manager\n");

    for (int i = 0; i < REFILL_TEST_N; i++) {
        // slab allocate
        errval_t e = ram_alloc_aligned(&(caprefs[i]), request_size, request_alignment);
        if (!err_is_ok(e)) {
            debug_printf("error in ram_alloc_aligned(): problems with too many allocations!\n");
            return e;
        }
        printf("allocated #%d\n", i);

#ifdef __INIT__
        if (i % 100 == 0) {
            mm_dump_mmnodes(&aos_mm);
        }
#endif
    }

    debug_printf("all %d blocks allocated.\n", REFILL_TEST_N);
#ifdef __INIT__
    mm_dump_mmnodes(&aos_mm);
#endif


#ifdef __INIT__
    // free after all allocations, s.t. 4KB blocks cannot be reused directly
    for (int i = 0; i < REFILL_TEST_N; i++) {
        // slab free
        errval_t e = mm_free(&aos_mm, caprefs[i], 0, 0);
        // note: address and size are ignored in mm_free (as discussed with AOS support)
        if(!err_is_ok(e)){
            debug_printf("error in mm_free(): slab space doesn't get refilled (non-tracking mode)!\n");
            debug_printf("==> might be because refill not yet implemented.\n");
            return e;
        }
    }

    debug_printf("released mm nodes after testing\n");
    mm_dump_mmnodes(&aos_mm);

    debug_printf("No errors observed. Refill works!\n");
    debug_printf("Please note: this puts stress on all parts: slab allocation/refill, slot allocation/refill, memory manager\n");

#else
    debug_printf("Freeing of blocks cannot yet be tested from a common user program.\n");
    debug_printf("Please use the low-level test suite for that.\n");

#endif

    return 0;
}


static const char *paging_map_frame_to_two_virtual_addresses_description = "Paging: Map a frame to two different virtual addresses.";

static int paging_map_frame_to_two_virtual_addresses_test(void) {
    struct capref frame;
    size_t bytes = 4096;
    size_t retbytes = 0;

    errval_t err = frame_alloc(&frame, bytes, &retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc failed.\n");
        return 1;
    }
    if (retbytes < bytes) {
        debug_printf("frame_alloc returned less bytes than requested.\n");
        return 2;
    }
    if (ARM_PAGE_OFFSET(retbytes)) {
        debug_printf("frame_alloc returned a frame with a size that is not a multiple of the page size.\n");
        return 3;
    }

    void* buf1;
    void* buf2;

    err = paging_map_frame_attr(get_current_paging_state(), &buf1, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_fixed_attr failed.\n");
        return 4;
    }
    debug_printf("Mapped a frame at 0x%" PRIxLVADDR " in virtual address space.\n", buf1);
    char* address1 = (char*)buf1;
    err = paging_map_frame_attr(get_current_paging_state(), &buf2, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_fixed_attr failed.\n");
        return 5;
    }
    debug_printf("Mapped the same frame again at 0x%" PRIxLVADDR " in virtual address space.\n", buf2);
    char* address2 = (char*)buf2;

    const char* test_string = "test";
    strcpy(address1, test_string);
    debug_printf("Copied test string to first virtual address 0x%" PRIxLVADDR ".\n", address1);
    if (strcmp(address2, test_string)) {
        debug_printf("address1 and address2 should both correspond to the same physical address.\n");
        return 6;
    }
    debug_printf("Successfully read test string from second virtual address 0x%" PRIxLVADDR ".\n", address2);

    return 0;
}


/**
 *  This test allocates blocks of size "size" until it encounters an error.
 *  It is therefore considered an "infinitely" running test.
 */
static int mm_stress_test(size_t size) {
    errval_t err;
    struct capref cap;
    size_t count = 0;
    while (err_is_ok(err = ram_alloc_aligned(&cap, size, 1))) {
        count++;
        if (count % 100 == 0) {
            debug_printf("Allocated %d blocks of size %d KiB.\n", count, size / 1024);
        }
    }
    debug_printf("Allocated %d blocks of size %d KiB.\n", count, size / 1024);
    debug_printf("This is a total of %d MiB of memory.\n", (count * size) / 1024 / 1024);
    if (err_is_fail(err)) {
        if (err_no(err) != MM_ERR_OUT_OF_BOUNDS) {
            DEBUG_ERR(err, "Encountered unexpected error.\n");
            return 1;
        }
        debug_printf("If the total size of allocated memory is close to the total memory available on your platform, the test was probably successful.\n");
        return 0;
    }
    debug_printf("We should never reach here. We expect an MM_ERR_OUT_OF_BOUNDS error.\n");
    return 2;
}

static const char *mm_stress_4_description = "Infinitely allocate 4 KiB";

static int mm_stress_4_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE);
}

static const char *mm_stress_8_description = "Infinitely allocate 8 KiB";

static int mm_stress_8_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 1);
}

static const char *mm_stress_16_description = "Infinitely allocate 16 KiB";

static int mm_stress_16_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 2);
}

static const char *mm_stress_32_description = "Infinitely allocate 32 KiB";

static int mm_stress_32_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 3);
}

static const char *mm_stress_64_description = "Infinitely allocate 64 KiB";

static int mm_stress_64_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 4);
}

static const char *mm_stress_128_description = "Infinitely allocate 128 KiB";

static int mm_stress_128_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 5);
}

static const char *mm_stress_256_description = "Infinitely allocate 256 KiB";

static int mm_stress_256_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 6);
}

static const char *mm_stress_512_description = "Infinitely allocate 512 KiB";

static int mm_stress_512_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 7);
}

static const char *mm_stress_1024_description = "Infinitely allocate 1 MiB";

static int mm_stress_1024_test(void) {
    return mm_stress_test(BASE_PAGE_SIZE << 8);
}


#endif // LIB_TESTING_TS01_MEMORY_SYSTEM_INCLUDE_H_
