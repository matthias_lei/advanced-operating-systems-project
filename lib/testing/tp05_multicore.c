/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/tp05_multicore.h>

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>

#include <string.h>


//---- test helper functions -------




//--- specific Multicore tests ---------------------------------------------------------------------

// NOTE: hello world can be replaced here as soon as real tests are added

static const char *hello_world_description = "Hello, World! Working on Multicore!";

static int hello_world_test(void) {

    printf("Hello, World!\n");

    return 0;
}





//--- Zeus is kept in the list for convenience purpose ---------------------------------------------

static const char *zeus_description = "Launch domain manager Zeus (note: not returning to test menu at the moment)";

// TODO: we can launch Zeus on the other core after multicore works.

static int zeus_test(void) {
    // launch of the manager directly here
    struct aos_rpc *demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("Could not find Demeter process service to launch Zeus\n");
        return 1;
    }

    domainid_t zeus_id = 0;
    errval_t err = aos_rpc_process_spawn(demeter, "zeus", 0, &zeus_id);
    if (err_is_fail(err)) {
        debug_printf("Could not launch Zeus\n");
        return 2;        
    }

    // current workaround to avoid the test menu interfering with Zeus
    while (true) {
        // nothing
    }

    return 0;
}


//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "Multicore";

void testing_add_tests_05_multicore(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // tests that succeed
    testing_add_test(h, hello_world_test, hello_world_description, true);

    // tests that will not return



    // milestone integration tests


    // launch Zeus
    testing_add_test(h, zeus_test, zeus_description, false);
}
