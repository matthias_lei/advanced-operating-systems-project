/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/tp04_self_paging.h>

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>

#include <string.h>


//---- test helper functions -------




//--- specific Access to vspace locations ----------------------------------------------------------

static const char *hello_world_description = "Hello, World! Self Paging works!";

static int hello_world_test(void) {
    int test = 42;

    printf("Hello, World!\n");
    
    printf("No problem in accessing own mapped VSpace: %d (at %p).\n", test, &test);
    return 0;
}


static const char *kernel_reserved_space_description = "Accessing protected kernel vspace? Bad idea.";

static int kernel_reserved_space_test(void) {
    int *test = (int *)0x80000040;

    printf("Testing access to memory in vspace that is reserved for kernel at %p.\n", test);
    
    printf("Does this value here show? %d\n", *test);

    return 0;
}

static const char *probable_nullpointer_description = "Accessing address close to 0x0. Throws \"probable\" nullpointer exception.";

static int probable_nullpointer_test(void) {

    char *test = (char *) BASE_PAGE_SIZE - 1;

    printf("Accessing address close to 0, at %p.\n", BASE_PAGE_SIZE - 1);
    
    printf("Does this value here show? %d\n", *test);

    return 0;
}

static const char *unmapped_privileged_description = "Accessing unmapped privileged vspace.";

static int unmapped_privileged_test(void) {
    int *test = (int *)0x20000000;

    printf("Testing access to unmapped privileged part of vspace at %p.\n", test);
    
    printf("Does this value here show? %d\n", *test);

    return 0;
}

static const char *mapped_privileged_description = "Accessing mapped privileged vspace.";

static int mapped_privileged_test(void) {
    int *test = (int *)0x40015C;

    printf("Testing access to mapped privileged part of vspace at %p.\n", test);
    printf("Note: This address should be a PC address, i.e. it should be mapped only with R/X rights and yield page-fault on write.\n");
    
    *test = 42;

    return 0;
}

static const char *unmapped_address_description = "Accessing unmapped vspace, i.e. not in any paging region.";

static int unmapped_address_test(void) {
    char *test = (char *) 0x70898001;
    
    paging_print_state(get_current_paging_state());

    printf("Testing access to unmapped part of vspace at %p.\n", test);
    
    printf("Does this value here show? %c\n", *test);

    return 0;
}


//--- Zeus is kept in the list for convenience purpose ---------------------------------------------

static const char *zeus_description = "Launch domain manager Zeus (note: not returning to test menu at the moment)";

static int zeus_test(void) {
    // launch of the manager directly here
    struct aos_rpc *demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("Could not find Demeter process service to launch Zeus\n");
        return 1;
    }

    domainid_t zeus_id = 0;
    errval_t err = aos_rpc_process_spawn(demeter, "zeus", 0, &zeus_id);
    if (err_is_fail(err)) {
        debug_printf("Could not launch Zeus\n");
        return 2;        
    }

    // current workaround to avoid the test menu interfering with Zeus
    while (true) {
        // nothing
    }

    return 0;
}


//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "Self Paging";

void testing_add_tests_04_self_paging(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // tests that succeed
    testing_add_test(h, hello_world_test, hello_world_description, true);

    // tests that will not return
    testing_add_test(h, kernel_reserved_space_test, kernel_reserved_space_description, false);
    testing_add_test(h, probable_nullpointer_test, probable_nullpointer_description, false);
    testing_add_test(h, unmapped_privileged_test, unmapped_privileged_description, false);
    testing_add_test(h, mapped_privileged_test, mapped_privileged_description, false);
    testing_add_test(h, unmapped_address_test, unmapped_address_description, false);


    // milestone integration tests
    testing_add_test(h, zeus_test, zeus_description, false);
}
