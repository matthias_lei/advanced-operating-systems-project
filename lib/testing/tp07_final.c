/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/tp07_final.h>

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>

#include <string.h>

#include <hashtable/serializable_key_value_store.h>

//---- test helper functions -------




//--- specific tests -------------------------------------------------------------------------------

// NOTE: hello world can be replaced here as soon as real tests are added

static const char *hello_world_description = "Hello, World! Working on the final Milestone!";

static int hello_world_test(void) {

    printf("Hello, World!\n");

    return 0;
}

// multiple colors
#define RED     "\033[1;31m"
#define ORANGE  "\033[31m"
#define YELLOW  "\033[1;33m"
#define GREEN   "\033[1;32m"
#define CYAN    "\033[1;36m"
#define BLUE    "\033[1;34m"
#define PURPLE  "\033[1;35m"

#define RESET   "\033[0m"

static const char *multi_color_description = "Using multiple colors on a line.";

static int multi_color_test(void) {
    printf("Printing a rainbow string!\n");
    printf("Note: The coloring should also still be visible after a window switch.\n");

    printf(RED "R" ORANGE "A" YELLOW "I" GREEN "N" BLUE "B" CYAN "O" PURPLE "W" RESET "\n");

    return 0;
}


//--- Nameserver :: Gaia tests ---------------------------------------------------------------------

static const char *kv_store_description = "Nameserver: Serializable Key Value Store";

static int kv_store_test(void) {

    printf("\nCreate store and add elements\n");
    int err = 0;
    struct serializable_key_value_store *store = create_kv_store(5);
    if (!store) {
        err = 1;
        goto unroll_nothing;
    }

    kv_store_set(store, "one", "value 1");
    kv_store_set(store, "two", "value 2");
    kv_store_set(store, "three", "value 3");
    print_kv_store(store);

    printf("\nSerialize\n");
    struct serialized_key_value_store *buf = serialize_kv_store(store);
    if (!buf) {
        err = 2;
        goto unroll_store;
    }
    printf("\nSerialized buffer has size %zu\n", buf->buf_size);

    struct serializable_key_value_store *store2 = deserialize_kv_store(buf, 0);
    if (!store2) {
        err = 3;
        goto unroll_buf;
    }

    // please note: the buf that was used to deserialize the kv_store
    // *must not* be freed as long as the store is being used.
    //
    // following the semantics of the Hashtable implementation (provided)
    // the the kv store never takes ownership of the provided keys and values.
    // they are just stored as pointers.

    printf("\nDeserialized store\n");
    print_kv_store(store2);

    printf("\nUpdate value; feature was not available in hashtable\n");
    kv_store_set(store2, "two", "updated value 2");
    print_kv_store(store2);

    printf("\nRemove value, size now %zu\n", kv_store_size(store2));
    kv_store_remove(store2, "three");
    printf("\nafter: size now %zu\n", kv_store_size(store2));
    print_kv_store(store2);

    printf("\nAdd new value\n");
    kv_store_set(store2, "new key", "new value");
    printf("\nafter: size now %zu\n", kv_store_size(store2));
    print_kv_store(store2);

//unroll_store2:
    destroy_kv_store_borrowed_buffer(&store2);
    assert(!store2);
unroll_buf:
    free(buf);
unroll_store:
    destroy_kv_store(&store);
    assert(!store);
unroll_nothing:
    return err;
}




//--- Zeus is kept in the list for convenience purpose ---------------------------------------------

static const char *zeus_description = "Launch domain manager Zeus (note: not returning to test menu at the moment)";

// TODO: we can launch Zeus on the other core after multicore works.

static int zeus_test(void) {
    // launch of the manager directly here
    struct aos_rpc *demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("Could not find Demeter process service to launch Zeus\n");
        return 1;
    }

    domainid_t zeus_id = 0;
    errval_t err = aos_rpc_process_spawn(demeter, "zeus", 0, &zeus_id);
    if (err_is_fail(err)) {
        debug_printf("Could not launch Zeus\n");
        return 2;
    }

    // current workaround to avoid the test menu interfering with Zeus
    while (true) {
        // nothing
    }

    return 0;
}


//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "Final Milestone";

void testing_add_tests_07_final(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // tests that succeed
    testing_add_test(h, hello_world_test, hello_world_description, true);
    testing_add_test(h, kv_store_test, kv_store_description, true);
    testing_add_test(h, multi_color_test, multi_color_description, true);

    // tests that will not return



    // milestone integration tests


    // launch Zeus
    testing_add_test(h, zeus_test, zeus_description, false);
}
