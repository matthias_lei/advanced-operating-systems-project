/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v1.1 2017-09-29 / 2017-10-25 pisch
 */

#include <testing/tp01_memory_system.h>

//--- import shared functions
#include "ts01_memory_system_shared_include.h"


// TODO: porting tests from ti with potential adjustments that can be run from a test program
// without special privileges

// note: this is limited at the moment by:
// - no free RPC call available
// - no dump RPC call available


//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "Memory system";

void testing_add_tests_01_memory_system(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // initial test for the menu until first real tests are written
    testing_add_test(h, hello_world_test, hello_world_description, true);

    // libmm tests
    testing_add_test(h, mm_stress_4_test, mm_stress_4_description, false);
    testing_add_test(h, mm_stress_8_test, mm_stress_8_description, false);
    testing_add_test(h, mm_stress_16_test, mm_stress_16_description, false);
    testing_add_test(h, mm_stress_32_test, mm_stress_32_description, false);
    testing_add_test(h, mm_stress_64_test, mm_stress_64_description, false);
    testing_add_test(h, mm_stress_128_test, mm_stress_128_description, false);
    testing_add_test(h, mm_stress_256_test, mm_stress_256_description, false);
    testing_add_test(h, mm_stress_512_test, mm_stress_512_description, false);
    testing_add_test(h, mm_stress_1024_test, mm_stress_1024_description, false);


    // official milestone tests
    testing_add_test(h, milestone1_running_out_of_slab_and_slot_space_test, milestone1_running_out_of_slab_and_slot_space_description, true);

    // paging tests
    testing_add_test(h, paging_map_frame_to_two_virtual_addresses_test, paging_map_frame_to_two_virtual_addresses_description, true);

}
