/**
 * static data and code used in both, ti and tp implementations of the library.
 * version 2017-10-25, GroupC
 */

#ifndef LIB_TESTING_TS02_PROCESS_CREATION_SHARED_INCLUDE_H_
#define LIB_TESTING_TS02_PROCESS_CREATION_SHARED_INCLUDE_H_

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <aos/aos.h>
#include <aos/systime.h>
#include <aos/deferred.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>


static struct paging_state* ps_aos_pt;

static const int INTS_PER_BASE_PAGE = BASE_PAGE_SIZE/sizeof(int);
static const int sample_interval = 20;

#define SLEEP_CONST 300

static void sleep_helper(int approx_ms) {
    volatile int dummy = 0;
    for (int i = 0; i < approx_ms; i++) {
        for (int j = 0; j < SLEEP_CONST; j++) {
            dummy = 0;
            for (int k = 0; k < SLEEP_CONST; k++) {
                dummy += 1;
            }
        }
    }
}

//--- each test definition should follow this principle

static const char *t02_test_input_output_description = "Test improved terminal I/O glue code (note: needs terminal set to --omap crlf --echo";
static int t02_test_input_output_test(void) {
    printf("\ngetline(): please enter a line of text\n");

    char *line = NULL;
    size_t capacity = 0;
    ssize_t size = getline(&line, &capacity, stdin);
    if (size < 0) {
        perror("getline failed");
    }
    else {
        line[size] = 0;
        printf("You entered: %s (len %d)\n", line, size);
    }

    // line must be freed in success and fail of getline() !
    if (line) {
        free(line);
    }


    printf("\nscanf(): please enter an integer\n");
    int integer = 0;
    int r = scanf("%d", &integer);
    if (r < 0) {
        printf("error in scanf()\n");
    }
    else {
        printf("You entered the integer %d\n", integer);
    }

    return 0;
}


static int recursive_test_function(int i) {
    DEBUGGING_ENTER;
    if (i <= 0) {
        DEBUGGING_EXIT;
        return 1;
    }

    int r = 2 * recursive_test_function(i - 1);
    DEBUGGING_PRINTF("intermediary result %d\n", r);
    DEBUGGING_EXIT;
    return r;
}

static const char *t02_test_debugging_helper_description = "Test added debugging helper (output depends on flags settings)";
static int t02_test_debugging_helper_test(void) {
    DEBUGGING_ENTER;
    DEBUGGING_PRINTF("Current max. indentation: %d\n", aos_testing_debugging_max_indent);
    debug_printf("2^5 is calculated by a recursive function\n");
    int r = recursive_test_function(5);
    debug_printf("result: %d\n", r);
#ifdef __INIT__
    // to test RUN, also dump the MM state
    DEBUGGING_RUN(mm_dump_mmnodes(&aos_mm));
#endif
    DEBUGGING_RUN(uint64_t tic = systime_now());
    DEBUGGING_RUN(sleep_helper(100));
    DEBUGGING_RUN(uint64_t toc = systime_now());
    DEBUGGING_RUN(printf("sleep_helper(100) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(sleep_helper(500));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("sleep_helper(500) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(sleep_helper(1000));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("sleep_helper(1000) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(sleep_helper(2000));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("sleep_helper(2000) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));  
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(barrelfish_msleep(100));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("barrelfish_msleep(100) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(barrelfish_msleep(500));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("barrelfish_msleep(500) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(barrelfish_msleep(1000));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("barrelfish_msleep(1000) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));   
    DEBUGGING_RUN(tic = systime_now());
    DEBUGGING_RUN(barrelfish_msleep(2000));
    DEBUGGING_RUN(toc = systime_now());
    DEBUGGING_RUN(printf("barrelfish_msleep(2000) took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc - tic)));
    DEBUGGING_EXIT;
    return 0;
}


//--- page mapping / virtual memory tests

static const char *simple_pagemapping_description = "Request 5KB Frame and map to page. Then writes a map_counter into that page and reads it out.";

static int simple_pagemapping_test(void) {
    errval_t err;

    struct capref frame;
    size_t bytes = 5000;
    size_t retbytes = 0;

    err = frame_alloc(&frame, bytes, &retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc failed.\n");
        return 1;
    }

    size_t exp_size = ROUND_UP(bytes, BASE_PAGE_SIZE);
    if (retbytes != exp_size) {
        debug_printf("frame_alloc failed. Returned size did not match expected size.\n");
        debug_printf("Expected: %d bytes\n", exp_size);
        debug_printf("Actual: %d bytes\n", retbytes);
        return 2;
    }

    debug_printf("Mapping frame %d bytes (actual: %d bytes)\n", bytes, retbytes);

    void *buf = NULL;
    ps_aos_pt = get_current_paging_state();

    debug_printf("Paging_state before:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);


    err = paging_map_frame_attr(ps_aos_pt, &buf, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed.\n");
        return 3;
    }

    debug_printf("Frame mapped to VA: 0x%x\n\n", buf);

    debug_printf("Paging_state after:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);

    int n_page = DIVIDE_ROUND_UP(bytes, BASE_PAGE_SIZE);

    debug_printf("Writing counters into the beginning of every page starting at VA 0x%x\n", buf);

    //write mapcount into all the pages of the mapping
    for (int i = 0; i < n_page; i++) {

        int* write_addr = (((int*) buf) + i*INTS_PER_BASE_PAGE);

        *write_addr = i;

        debug_printf("Writing %d into VA 0x%x\n", i, write_addr);
    }

    debug_printf("Reading out the counters from every page starting at VA 0x%x\n", buf);

    bool failed = false;

    //read out all values from every page and check
    for (int i = 0; i < n_page; i++) {
        int* read_address = ((int*) buf) + i*INTS_PER_BASE_PAGE;
        int read_out_int = *read_address;

        if (read_out_int != i) {
            debug_printf("counter does not match value written into page with VA 0x%x\n", read_address );
            debug_printf("Expected: 0x%x\n", i);
            debug_printf("Read out: 0x%x\n", read_out_int);
            failed = true;
        }

        debug_printf("Read %d at VA 0x%x\n", read_out_int, read_address);
    }

    if (failed) return 4;

    debug_printf("Read and Write successful: All values from every page match\n");

    return 0;
}

static const char *moderate_pagemapping_description = "Request 257*4KB Frame and map to page (new l1 pagetable slot).\n   Then writes a map_counter into that page and reads it out.";

static int moderate_pagemapping_test(void) {
    errval_t err;

    struct capref frame;
    size_t bytes = BASE_PAGE_SIZE * 257;
    size_t retbytes = 0;

    err = frame_alloc(&frame, bytes, &retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc failed.\n");
        return 1;
    }

    size_t exp_size = ROUND_UP(bytes, BASE_PAGE_SIZE);
    if (retbytes != exp_size) {
        debug_printf("frame_alloc failed. Returned size did not match expected size.\n");
        debug_printf("Expected: %d bytes\n", exp_size);
        debug_printf("Actual: %d bytes\n", retbytes);
        return 2;
    }

    debug_printf("Mapping frame %d bytes (actual: %d bytes)\n\n", bytes, retbytes);

    void *buf = NULL;
    ps_aos_pt = get_current_paging_state();

    debug_printf("Paging_state before:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);


    err = paging_map_frame_attr(ps_aos_pt, &buf, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed.\n");
        return 3;
    }

    debug_printf("Frame mapped to VA: 0x%x\n\n", buf);

    debug_printf("Paging_state after:\n\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n", ps_aos_pt->allocated_region_end);

    int n_page = DIVIDE_ROUND_UP(bytes, BASE_PAGE_SIZE);

    debug_printf("Writing counters into the beginning of every page starting at VA 0x%x\n", buf);
    debug_printf("Samples:\n");

    //write mapcount into all the pages of the mapping
    for (int i = 0; i < n_page; i++) {

        int* write_addr = (((int*) buf) + i*INTS_PER_BASE_PAGE);

        *write_addr = i;

        if(i % sample_interval == 0) {
            debug_printf("Writing %d into VA 0x%x\n", i, write_addr);
        }

    }

    debug_printf("Reading out the counters from every page starting at VA 0x%x\n", buf);
    debug_printf("Samples:\n");

    bool failed = false;

    //read out all values from every page and check
    for (int i = 0; i < n_page; i++) {
        int* read_address = ((int*) buf) + i*INTS_PER_BASE_PAGE;
        int read_out_int = *read_address;

        if (read_out_int != i) {
            debug_printf("counter does not match value written into page with VA 0x%x\n", read_address );
            debug_printf("Expected: 0x%x\n", i);
            debug_printf("Read out: 0x%x\n", read_out_int);
            failed = true;
        }

        if (i % sample_interval == 0) {
            debug_printf("Read %d at VA 0x%x\n", read_out_int, read_address);
        }
    }

    if (failed) return 4;

    debug_printf("Read and Write successful: All values from every page match\n");

    return 0;
}

static const char *extensive_pagemapping_description = "Request 514*4KB Frame and map to page (this triggers slab_refill AND new l1 pagetable slot).\n   Then writes a map_counter into that page and reads it out.";

static int extensive_pagemapping_test(void) {
    errval_t err;

    struct capref frame;
    size_t bytes = BASE_PAGE_SIZE * 514;
    size_t retbytes = 0;

    err = frame_alloc(&frame, bytes, &retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc failed.\n");
        return 1;
    }

    size_t exp_size = ROUND_UP(bytes, BASE_PAGE_SIZE);
    if (retbytes != exp_size) {
        debug_printf("frame_alloc failed. Returned size did not match expected size.\n");
        debug_printf("Expected: %d bytes\n", exp_size);
        debug_printf("Actual: %d bytes\n", retbytes);
        return 2;
    }

    debug_printf("Mapping frame %d bytes (actual: %d bytes)\n", bytes, retbytes);

    void *buf = NULL;
    ps_aos_pt = get_current_paging_state();

    debug_printf("Paging_state before:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);


    err = paging_map_frame_attr(ps_aos_pt, &buf, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed.\n");
        return 3;
    }

    debug_printf("Frame mapped to VA: 0x%x\n\n", buf);

    debug_printf("Paging_state after:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);

    int n_page = DIVIDE_ROUND_UP(bytes, BASE_PAGE_SIZE);

    debug_printf("Writing counters into the beginning of every page starting at VA 0x%x\n", buf);
    debug_printf("Samples:\n");

    //write mapcount into all the pages of the mapping
    for (int i = 0; i < n_page; i++) {

        int* write_addr = (((int*) buf) + i*INTS_PER_BASE_PAGE);

        *write_addr = i;

        if(i % sample_interval == 0) {
            debug_printf("Writing %d into VA 0x%x\n", i, write_addr);
        }

    }

    debug_printf("Reading out the counters from every page starting at VA 0x%x\n", buf);
    debug_printf("Samples:\n");

    bool failed = false;

    //read out all values from every page and check
    for (int i = 0; i < n_page; i++) {
        int* read_address = ((int*) buf) + i*INTS_PER_BASE_PAGE;
        int read_out_int = *read_address;

        if (read_out_int != i) {
            debug_printf("counter does not match value written into page with VA 0x%x\n", read_address );
            debug_printf("Expected: 0x%x\n", i);
            debug_printf("Read out: 0x%x\n", read_out_int);
            failed = true;
        }

        if (i % sample_interval == 0) {
            debug_printf("Read %d at VA 0x%x\n", read_out_int, read_address);
        }
    }

    if (failed) return 4;

    debug_printf("Read and Write successful: All values from every page match\n");

    return 0;
}

static const char *write_to_ro_page_description = "Try to write to a read-only page (NOTE: will throw unhandled page fault error and dump all l2 pagetables).";

static int write_to_ro_page_test(void) {
	errval_t err;

    struct capref frame;
    size_t bytes = 5000;
    size_t retbytes = 0;

    err = frame_alloc(&frame, bytes, &retbytes);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "frame_alloc failed.\n");
        return 1;
    }

    debug_printf("Mapping frame %d bytes as read only (actual: %d bytes)\n", bytes, retbytes);

    void *buf = NULL;
    ps_aos_pt = get_current_paging_state();

    debug_printf("Paging_state before:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);


    err = paging_map_frame_attr(ps_aos_pt, &buf, retbytes, frame, VREGION_FLAGS_READ, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "paging_map_frame_attr failed.\n");
        return 2;
    }

    debug_printf("Frame mapped to VA: 0x%x\n\n", buf);

    debug_printf("Paging_state after:\n");
    debug_printf("vspace_base_addr: 0x%x\n", ps_aos_pt->vspace_base_addr);
    debug_printf("allocated_region_end: 0x%x\n\n", ps_aos_pt->allocated_region_end);

    debug_printf("Trying to write into VA of a read only page: 0x%x\n", buf);
    *(int*)buf = 0xcafecafe;

    //if this gets executed, then the read-only flag did not work.
    if (*(int*)buf == 0xcafecafe) {
        debug_printf("Value previously (illegaly) written matches with expected value (Test still counts as failed)\n");
        return 3;
    }
    else {
        debug_printf("Value previously (illegaly) written does not match with expected value (Test failed before, when value could be written to the read-only page).\n");
        return 4;
    }

    return 5;
}



#endif // LIB_TESTING_TS02_PROCESS_CREATION_SHARED_INCLUDE_H_
