/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-11-15 pisch
 */

#include <testing/tp06_ump.h>

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>

#include <string.h>

#include <benchmark/benchmark.h>
#include <aos/deferred.h>


//---- test helper functions -------




//--- specific Multicore tests ---------------------------------------------------------------------

// NOTE: hello world can be replaced here as soon as real tests are added

static const char *hello_world_description = "Hello, World! Working with UMP!";

static int hello_world_test(void) {

    printf("Hello, World!\n");

    return 0;
}


//--- Quick tests for benchmarking library ---------------------------------------------------------

static const char *benchmark1_description = "Simple benchmark library test";

static int benchmark1_test(void) {

    int n_measurements = 30;
    if (!create_testbench(n_measurements)) {
        return 1;
    }

    // measure the time needed to print a short text over the RPC message system
    for (int i = 0; i < n_measurements; i ++) {
        uint64_t start = systime_now();
        printf("Hello, World!\n");
        uint64_t stop = systime_now();
        add_measurement(start, stop);
    }

    struct testbench_statistics stat = testbench_get_statistics();

    // note: I recommend leaving outlier detection OFF (default) for our setting
    // it should only be used in specific cases and certainly not for reports.
    // see some examples in upstream github archive.

    // statistics in cycles
    print_testbench_statistics("print short message", &stat, NULL);
    print_histogram(NULL, &stat, NULL);

    // statistics in us
    print_testbench_statistics("print short message", &stat, &testbench_pandaboard_microseconds);

    // This part still needs testing whether the conversion works well also for the histogram
    print_histogram(NULL, &stat, &testbench_pandaboard_microseconds);

    delete_testbench();
    return 0;
}

#define MS_TO_US_FACTOR 1000
#define S_TO_US_FACTOR (1000000.0)

#define N_MEASUREMENTS 5
#define N_WARMUP 2

// for RTT / latency tests
#define MSG_SIZE_SMALL 5

// for LMP and UMP; (on LMP only about 295 KiB/s possible at the moment)
#define MSG_SIZE_MEDIUM (512 * 1024)

// only for UMP (on UMP about 29 MiB/s possible at the moment)
#define MSG_SIZE_LARGE (8 * 1024 * 1024)


// new unit types for throughput, that can be used after testbench was converted
// to "bytes/s" unit using one of the below lambda functions

static struct testbench_time_unit tb_unit_kib = {
    .name = "KiB/s",
    .cycles_per_unit = 1024
};

__attribute__((unused))
static struct testbench_time_unit tb_unit_mib = {
    .name = "MiB/s",
    .cycles_per_unit = 1024*1024
};


// lamdba functions passed to benchmark library function development_map_values()
// to convert testbench values to "bytes/s" unit
static inline uint64_t convert_clock_to_byte_per_s(uint64_t clocks, uint64_t size_in_bytes) {
    if (clocks == 0) {
        return UINT64_MAX;
    }

    double in = (double)clocks;
    double size = (double)size_in_bytes;

    double out = size * 300.0 * S_TO_US_FACTOR / in;
    return (uint64_t)round(out);
}


static uint64_t lambda_short_short(uint64_t clocks) {
    return convert_clock_to_byte_per_s(clocks, MSG_SIZE_SMALL + MSG_SIZE_SMALL);
}

static uint64_t lambda_short_medium(uint64_t clocks) {
    return convert_clock_to_byte_per_s(clocks, MSG_SIZE_SMALL + MSG_SIZE_MEDIUM);
}

static uint64_t lambda_medium_medium(uint64_t clocks) {
    return convert_clock_to_byte_per_s(clocks, MSG_SIZE_MEDIUM + MSG_SIZE_MEDIUM);
}

static uint64_t lambda_short_long(uint64_t clocks) {
    return convert_clock_to_byte_per_s(clocks, MSG_SIZE_SMALL + MSG_SIZE_LARGE);
}

static uint64_t lambda_long_long(uint64_t clocks) {
    return convert_clock_to_byte_per_s(clocks, MSG_SIZE_LARGE + MSG_SIZE_LARGE);
}

static inline errval_t benchmark_generic(size_t size_req, size_t size_rep,
                                        size_t n_measurements,
                                        size_t warmup_rounds,
                                        struct aos_rpc *rpc_chan) {
    uint64_t tic, toc;
    errval_t err = SYS_ERR_OK;

    // do warmup and measurements
    for (int i = 0; i < warmup_rounds + n_measurements; i++) {

        // as in real life: racing after a good night's sleep
        barrelfish_msleep(100);

        tic = systime_now();
        err = aos_rpc_benchmarking(rpc_chan, size_req, size_rep);
        toc = systime_now();
        if (err_is_fail(err)) {
            debug_printf("%s: aos_rpc_benchmarking() failed.\n", __func__);
            return err;
        }

        if (i >= warmup_rounds) {
            add_measurement(tic, toc);
        }
    }

    return SYS_ERR_OK;
}


static const char *benchmark_description = "Benchmarking of different RPC calls.";

static int benchmark_test(void) {
    errval_t err = SYS_ERR_OK;
    struct testbench_statistics tb_stat;

    // send benchmarking requests to serial server,
    // as it is only running on core 0 and thus allows
    // to test both LMP (requests from core 0) and
    // UMP (requests from core 1)
    struct aos_rpc *rpc_chan = aos_rpc_get_serial_channel();

    printf("Benchmarking variants of RPC calls.\n");
    printf("Depending on where this test is executed, we can benchmark LMP (on core 0) or UMP (on core 1).\n");
    printf("Now benchmarking %s.\n", disp_get_core_id() == 0 ? "LMP" : "UMP");
    printf("Measuring each RPC %d times.\n", N_MEASUREMENTS);

    printf("Enable warmup? y/n\n");
    size_t n_warmup = getchar() == 'y' ? N_WARMUP : 0;

    if (!create_testbench(N_MEASUREMENTS)) {
        debug_printf("%s: create_testbench() failed.\n");
        return 1;
    }
    printf("\n");

    // Mimicking typical RPC requests abstracted to sizes
    // testing:
    // 1) RPC with small arg (interesting for RTT / latency)
    // 2) RPC with medium arg
    // 3) RPC with medium reply
    // 4) RPC with large arg (UMP only)
    // 5) RPC with large reply (UMP only)

    printf("---------- 1) RTT: \"small\" request (%d bytes), \"small\" reply (%d bytes)\n", MSG_SIZE_SMALL, MSG_SIZE_SMALL);

    err = benchmark_generic(MSG_SIZE_SMALL, MSG_SIZE_SMALL, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_short_short);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in KiB/s", &tb_stat, &tb_unit_kib);
    print_histogram(NULL, &tb_stat, &tb_unit_kib);

    printf("\n\n");


    reset_testbench();

    printf("---------- 2) Throughput: \"medium\" request (%d KiB), \"small\" reply (%d bytes)\n", MSG_SIZE_MEDIUM / 1024, MSG_SIZE_SMALL);

    err = benchmark_generic(MSG_SIZE_MEDIUM, MSG_SIZE_SMALL, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_short_medium);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in KiB/s", &tb_stat, &tb_unit_kib);
    print_histogram(NULL, &tb_stat, &tb_unit_kib);

    printf("\n\n");


    reset_testbench();

    printf("---------- 3) Throughput \"small\" request (%d bytes), \"medium\" reply (%d KiB)\n", MSG_SIZE_SMALL, MSG_SIZE_MEDIUM / 1024);

    err = benchmark_generic(MSG_SIZE_SMALL, MSG_SIZE_MEDIUM, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_short_medium);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in KiB/s", &tb_stat, &tb_unit_kib);
    print_histogram(NULL, &tb_stat, &tb_unit_kib);

    printf("\n\n");


    reset_testbench();

    printf("---------- 4) Throughput \"medium\" request (%d KiB), \"medium\" reply (%d KiB)\n", MSG_SIZE_MEDIUM / 1024, MSG_SIZE_MEDIUM / 1024);

    err = benchmark_generic(MSG_SIZE_MEDIUM, MSG_SIZE_MEDIUM, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_medium_medium);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in KiB/s", &tb_stat, &tb_unit_kib);
    print_histogram(NULL, &tb_stat, &tb_unit_kib);

    printf("\n\n");

    // do not run the large test with a throughput below 2 MiB / s
    #define MIN_THROUGHPUT_NEEDED (2 * 1024 * 1024)
    if (tb_stat.mean < MIN_THROUGHPUT_NEEDED) {
        printf("Slow system detected with mean throughput %zu KiB/s (LMP?): large messages (%zu MiB) are only tested if a min. throughput of %zu MiB/s is achieved. Try UMP from the other core!",
               (size_t)tb_stat.mean / 1024, (size_t)MSG_SIZE_LARGE / 1024 / 1024, (size_t)MIN_THROUGHPUT_NEEDED / 1024 / 1024);
        goto cleanup;
    }

    reset_testbench();

    printf("---------- 5) Throughput: \"large\" request (%d KiB), \"small\" reply (%d bytes)\n", MSG_SIZE_LARGE / 1024, MSG_SIZE_SMALL);

    err = benchmark_generic(MSG_SIZE_LARGE, MSG_SIZE_SMALL, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_short_long);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in MiB/s", &tb_stat, &tb_unit_mib);
    print_histogram(NULL, &tb_stat, &tb_unit_mib);

    printf("\n\n");


    reset_testbench();

    printf("---------- 6) Throughput: \"small\" request (%d bytes), \"large\" reply (%d KiB)\n", MSG_SIZE_SMALL, MSG_SIZE_LARGE / 1024);

    err = benchmark_generic(MSG_SIZE_SMALL, MSG_SIZE_LARGE, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_short_long);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in MiB/s", &tb_stat, &tb_unit_mib);
    print_histogram(NULL, &tb_stat, &tb_unit_mib);

    printf("\n\n");


    reset_testbench();

    printf("---------- 7) Throughput: \"large\" request (%d KiB), \"large\" reply (%d KiB)\n", MSG_SIZE_LARGE / 1024, MSG_SIZE_LARGE / 1024);

    err = benchmark_generic(MSG_SIZE_LARGE, MSG_SIZE_LARGE, N_MEASUREMENTS, n_warmup, rpc_chan);
    if (err_is_fail(err)) {
        debug_printf("%s: benchmark_generic() failed.\n", __func__);
        return 1;
    }

    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in us", &tb_stat, &testbench_pandaboard_microseconds);
    print_histogram(NULL, &tb_stat, &testbench_pandaboard_microseconds);

    // convert values in testbench according to lambda function
    development_map_values(lambda_long_long);
    tb_stat = testbench_get_statistics();

    print_testbench_statistics("statistics in MiB/s", &tb_stat, &tb_unit_mib);
    print_histogram(NULL, &tb_stat, &tb_unit_mib);

    printf("\n\n");

cleanup:
    delete_testbench();
    return 0;
}

static const char *benchmark_spawn_description = "Benchmarking local or remote spawning of a process.";

static int benchmark_spawn_test(void) {
    errval_t err = SYS_ERR_OK;
    uint64_t tic, toc;

    printf("Spawn locally or remotely? (l/r)\n");
    coreid_t dest_core = getchar() == 'r' ? ((disp_get_core_id() + 1) % 2) : disp_get_core_id();

    printf("Measuring time to spawn a process from core %d on core %d.\n", disp_get_core_id(), dest_core);

    struct aos_rpc *rpc_chan = aos_rpc_get_process_channel();

    domainid_t domain_id;
    tic = systime_now();
    err = aos_rpc_process_spawn(rpc_chan, "tests/t02_spawn_two_1", dest_core, &domain_id);
    toc = systime_now();
    if (err_is_fail(err)) {
        debug_printf("%s: aos_rpc_process_spawn() failed.\n");
        return 1;
    }

    printf("Took %" PRIuSYSTIME " ms.\n", systime_to_ms(toc-tic));

    return 0;
}

//--- Tests for Timing / Deferred ------------------------------------------------------------------

static const char *tic_toc_description = "Tic Toc: periodic events (keeps running)";

static void tic(void *arg) {
    printf("   Tic            \n");
}

static void toc(void *arg) {
    printf("               Toc\n");
}

static int tic_toc_test(void) {
    struct waitset *ws = get_default_waitset();

    struct periodic_event tic_event, toc_event;

    bool running = true; // not yet used for event control

    errval_t err = periodic_event_create(&tic_event, ws, 2000000, MKCLOSURE(tic, &running));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not launch periodic event tic\n");
        return 1;
    }

    err = barrelfish_usleep(1000000);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not sleep\n");
        return 2;
    }

    err = periodic_event_create(&toc_event, ws, 2000000, MKCLOSURE(toc, &running));
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "could not launch periodic event toc\n");
        return 3;
    }

    while (true) {
        err = event_dispatch(ws);
        if (err_is_fail(err)) {
            // fatal error
            DEBUG_ERR(err, "event_dispatch failed.\n");
            return 4;
        }
    }
}



//--- Zeus is kept in the list for convenience purpose ---------------------------------------------

static const char *zeus_description = "Launch domain manager Zeus (note: not returning to test menu at the moment)";

// TODO: we can launch Zeus on the other core after multicore works.

static int zeus_test(void) {
    // launch of the manager directly here
    struct aos_rpc *demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("Could not find Demeter process service to launch Zeus\n");
        return 1;
    }

    domainid_t zeus_id = 0;
    errval_t err = aos_rpc_process_spawn(demeter, "zeus", 0, &zeus_id);
    if (err_is_fail(err)) {
        debug_printf("Could not launch Zeus\n");
        return 2;
    }

    // current workaround to avoid the test menu interfering with Zeus
    while (true) {
        // nothing
    }

    return 0;
}




//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "User-level Message Passing";

void testing_add_tests_06_ump(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // tests that succeed
    testing_add_test(h, hello_world_test, hello_world_description, true);
    testing_add_test(h, benchmark1_test, benchmark1_description, true);

    // tests that will not return or have user input
    testing_add_test(h, benchmark_test, benchmark_description, false);
    testing_add_test(h, benchmark_spawn_test, benchmark_spawn_description, false);
    testing_add_test(h, tic_toc_test, tic_toc_description, false);


    // milestone integration tests


    // launch Zeus
    testing_add_test(h, zeus_test, zeus_description, false);
}
