/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 02 process creation
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/ti02_process_creation.h>

#include <mm/mm.h>
extern struct mm aos_init_mm;

#include <spawn/multiboot.h>
#include <elf/elf.h>
#include <spawn/spawn.h>


//--- import shared functions

#include "ts02_process_creation_shared_include.h"


//--- process creation tests -----------------------------------------------------------------------
// these tests should be run in init and not in a generic user program

extern struct bootinfo *bi;

errval_t spawn_map_module(struct spawninfo *si, void **binary, struct mem_region **mr);

static const char *t02_finding_and_mapping_elf_binary_description = "Finds and maps ELF binary of hello and reads out its magic number";
static int t02_finding_and_mapping_elf_binary_test(void) {
    errval_t err;

    struct spawninfo si;
    si.binary_name = "hello";
    char *binary;
    struct mem_region *mr;

    err = spawn_map_module(&si, (void**)&binary, &mr);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "spawn_map_module failed.\n");
        return 2;
    }

    //read out magic number
    char magic[SELFMAG] = ELFMAG;
    for (int i = 0; i < SELFMAG; i++) {
        if (*(binary + i) != magic[i]) {
            debug_printf("Read out number is not equal to ELF magic number.\n");
            return 3;
        }
    }


    debug_printf("Read out magic number: %x %c %c %c\n", *binary, *(binary + 1), *(binary + 2), *(binary + 3));

    debug_printf("Succesfully read out ELF magic number.\n");

    return 0;
}

/*
// it launches 2 processes together
// but actual killing can be done in domain manager
static const char *t02_spawn_two_description = "Spawns two new processes and prints out messages (use domain manager to kill them)";
static int t02_spawn_two_test(void) {
    // 10 whitespaces
    debug_printf("                   Spawning first process.\n");

    spawninfo_node_t *node = calloc(1, sizeof(*node));
    if (!node) {
        debug_printf("could not allocate memory for spawninfo\n");
        return 1;
    }

    errval_t err = spawn_load_by_name("t02_spawn_two_1", &node->si);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "spawn_load_by_name() failed.\n");
        free(node);
        return 2;
    }

    add_spawninfo_node(node);

    debug_printf("                   Succesfully spawned new domain with program t02_spawn_two_1.\n");



    // 20 whitespaces
    debug_printf("                                       Spawning second process.\n");

    node = calloc(1, sizeof(*node));
    if (!node) {
        debug_printf("could not allocate memory for spawninfo\n");
        return 3;
    }

    err = spawn_load_by_name("t02_spawn_two_2", &node->si);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "spawn_load_by_name() failed.\n");
        free(node);
        return 4;
    }

    add_spawninfo_node(node);

    debug_printf("                                      Succesfully spawned new domain with program t02_spawn_two_2.\n");

    return 0;
}

//--- helper functions to call at least the cap_delete_last() --------------------------------------
// since actual revoke is not possible at the moment (see missing monitor)
// the function calls here are a WORKAROUND and are coming from upstream barrelfish
// usr/monitor/invocations.c and usr/monitor/include/monitor_invocations.h
// and usr/monitor/include/domcap.h

struct domcapref {
    struct capref croot;
    capaddr_t cptr;
    uint8_t level;
};

static inline struct domcapref get_cap_domref(struct capref cap) {
    return (struct domcapref) {
        // XXX: should be get_croot_addr(cap)?
        .croot = cap_root,
        .cptr  = get_cap_addr(cap),
        .level = get_cap_level(cap),
    };
}

//{{{1 Delete and revoke state machine stepping
static inline errval_t
invoke_monitor_delete_last(capaddr_t root, int rlevel, capaddr_t cap, int clevel,
                           capaddr_t retcn, int retcnlevel, cslot_t retslot)
{
    return cap_invoke8(cap_kernel, KernelCmd_Delete_last, root, rlevel, cap,
                       clevel, retcn, retcnlevel, retslot).error;
}


static errval_t monitor_delete_last(struct capref croot, capaddr_t cptr, int level, struct capref ret_cap)
{
    capaddr_t root_addr = get_cap_addr(croot);
    uint8_t root_level = get_cap_level(croot);
    capaddr_t ret_cn = get_cnode_addr(ret_cap);
    uint8_t ret_cn_level = get_cnode_level(ret_cap);
    cslot_t ret_slot = ret_cap.slot;
    return invoke_monitor_delete_last(root_addr, root_level, cptr, level,
                                      ret_cn, ret_cn_level, ret_slot);
}*/


/*static void kill_program(spawninfo_node_t *current) {
    debug_printf("\nkilling %4" PRIuDOMAINID " %s\n", current->si.domain_id, current->si.binary_name);

    remove_spawninfo_node(current);
*/
    /* // not working at the moment; but this is the goal
    errval_t err = cap_revoke(current->si.dcb);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "cap_revoke() failed for domain %s.\n", current->si.binary_name);
    }


    err = cap_delete(current->si.dcb);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "cap_delete() failed for domain %s.\n", current->si.binary_name);
    }
    //*/


    //* // currently used
    // alternative approach using some imported monitor functions from upstream barrelfish
    // first delete the cap in the foreign / child space
/*    errval_t err = cap_delete(current->si.dcb_in_child_space);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "cap_delete() failed\n");
        free(current);
        return;
    }


    // see above
    struct capref newcap; // could be used to return RAM to RAM manager; not yet done
    err = slot_alloc(&newcap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "slot_alloc() failed\n");
        free(current);
        return;
    }

    // note: it is crucial to use the dcb here that is mappen in our CSpace
    // otherwise, the dcb of this process itself is killed (i.e. using a capref of well known location)
    struct domcapref dom_dcb = get_cap_domref(current->si.dcb);
    err = monitor_delete_last(dom_dcb.croot, dom_dcb.cptr, dom_dcb.level, newcap);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "monitor_delete_last() failed for dcb for domain %s.\n", current->si.binary_name);
        free(current);
        return;
    }

    //*/

    // note: there is absolutely no cleanup at the moment, which leads to quite a resource leak
    // as discussed in class. Cleanup is a tricky problem and at the moment even more so
    // without a working cap_revoke() implementation.
    // yes: the newcap capref is also part of the leak at the moment.

    // but at least not our internally used node. :-)
/*    free(current);
}*/

//--- add the tests here to load them to the test suite --------------------------------------------

static const char *t02_menu_description = "Mapping and process creation";

void testing_add_tests_02_process_creation(void) {
    testing_menu_handle_t h = testing_add_menu(t02_menu_description);

    // generic / demo test case: false because it needs user input and special terminal setting.
    testing_add_test(h, t02_test_input_output_test, t02_test_input_output_description, false);
    testing_add_test(h, t02_test_debugging_helper_test, t02_test_debugging_helper_description, false);

    // page mapping / virtual memory tests
    testing_add_test(h, simple_pagemapping_test, simple_pagemapping_description, true);
    testing_add_test(h, moderate_pagemapping_test, moderate_pagemapping_description, true);
    testing_add_test(h, extensive_pagemapping_test, extensive_pagemapping_description, true);
    testing_add_test(h, write_to_ro_page_test, write_to_ro_page_description, false);

    // process creation tests
    testing_add_test(h, t02_finding_and_mapping_elf_binary_test, t02_finding_and_mapping_elf_binary_description, true);
}
