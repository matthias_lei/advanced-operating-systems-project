/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/tp03_message_passing.h>

#define _WITH_GETLINE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>

#include <testing/testing.h>

//#define AOS_TESTING_DEBUGGING_LOCALLY_OFF
#include <testing/debugging.h>

#include <string.h>

static const int INTS_PER_BASE_PAGE = BASE_PAGE_SIZE/sizeof(int);

//---- test helper functions -------


static int read_write_assert(void* buf, size_t bytes, int sample_interval, errval_t *ret_error) {

    int n_page = DIVIDE_ROUND_UP(bytes, BASE_PAGE_SIZE);

    int *int_buf = (int*) buf;

    debug_printf("Read/Write check\n");
    debug_printf("Writing counters into the beginning of every page starting at VA 0x%x\n", int_buf);
    debug_printf("Samples (every %d-th):\n", sample_interval);

    //write mapcount into all the pages of the mapping
    for (int i = 0; i < n_page; i++) {

        int* write_addr = &int_buf[i*INTS_PER_BASE_PAGE];

        *write_addr = i;

        if(i % sample_interval == 0) {
            debug_printf("Writing %d into VA 0x%x\n", i, write_addr);
        }

    }

    debug_printf("Reading out the counters from every page starting at VA 0x%x\n", int_buf);
    debug_printf("Samples (every %d-th):\n", sample_interval);

    bool failed = false;

    //read out all values from every page and check
    for (int i = 0; i < n_page; i++) {
        int* read_address = &int_buf[i*INTS_PER_BASE_PAGE];
        int read_out_int = *read_address;

        if (read_out_int != i) {
            debug_printf("Counter does not match value written into page with VA 0x%x\n", read_address );
            debug_printf("Expected: 0x%x\n", i);
            debug_printf("Read out: 0x%x\n", read_out_int);
            failed = true;
        }

        if (i % sample_interval == 0) {
            debug_printf("Read %d at VA 0x%x\n", read_out_int, read_address);
        }
    }

    if (failed) {
        return 1;
    }

    debug_printf("Read/Write check successful: All values from every page match\n\n");

    return 0;
}


//--- small messages one core ----------------------------------------------------------------------

static const char *rpc_get_ram_cap_description = "Directly requests a ram capability of size 7*4096 bytes via RPC call";

static int rpc_get_ram_cap_test(void) {

    size_t bytes = 7*BASE_PAGE_SIZE;

    printf("Requesting %d KiB RAM via RPC\n", bytes);
    struct capref ram;
    size_t ret_size;
    errval_t err = aos_rpc_get_ram_cap(aos_rpc_get_memory_channel(), bytes, BASE_PAGE_SIZE, &ram, &ret_size);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: failure during get aos_rpc_get_ram_cap\n");
        return 1;
    }

    if (ret_size < bytes) {
        debug_printf("aos_rpc_get_ram_cap failed. Returned size is smaller than requested size.\n");
        debug_printf("Requested: %d bytes\n", bytes);
        debug_printf("Got: %d bytes\n", ret_size);
        return 2;
    }

    debug_printf("RAM successfully requested. Received size: %d KiB\n", ret_size);

    //Retype to Frame
    struct capref frame;
    err = slot_alloc(&frame);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failure during slot_alloc");
        return 3;
    }

    err = cap_retype(frame, ram, 0, ObjType_Frame, ret_size, 1);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "failure during cap_retype");
    }

    debug_printf("RAM successfully retyped to Frame\n");

    //Map frame
    void *buf;

    err = paging_map_frame_attr(get_current_paging_state(), &buf, ret_size, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: failure during first paging_map_frame_attr\n");
        return 4;
    }

    debug_printf("Frame successfully mapped at VA: 0x%x\n", buf);

    //Read/Write check

    errval_t ret_error = 0;

    err = read_write_assert(buf, bytes, 1, &ret_error);
    if (ret_error) {
        DEBUG_ERR(err, "Read/Write check failed\n");
        return 5;
    }

    return 0;
}

static const char *frame_alloc_call_description = "Test that frame_alloc now remotely requests RAM of size 33*4096 bytes by using RPC";

static int frame_alloc_call_test(void) {

    size_t bytes = 33*BASE_PAGE_SIZE;
    size_t ret_size;
    struct capref frame;

    printf("Requesting %d KiB RAM via frame_alloc\n", bytes);

    errval_t err = frame_alloc(&frame, bytes, &ret_size);
    if (err_is_fail(err)) {
        debug_printf("frame_alloc failed\n");
        return 1;
    }

    if (ret_size < bytes) {
        debug_printf("frame_alloc failed. Returned size is smaller than requested size.\n");
        debug_printf("Requested: %d bytes\n", bytes);
        debug_printf("Received: %d bytes\n", ret_size);
        return 2;
    }

    debug_printf("Frame successfully requested. Received size: %d KiB\n", ret_size);

    //Map frame
    void *buf;

    err = paging_map_frame_attr(get_current_paging_state(), &buf, ret_size, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "%s: failure during paging_map_frame_attr\n");
        return 3;
    }

    debug_printf("Frame successfully mapped at VA: 0x%x\n", buf);

    int ret_err = 0;
    ret_err = read_write_assert(buf, bytes, 1, &err);
    if (ret_err) {
        DEBUG_ERR(err, "Read/Write check failed\n");
        return 4;
    }

    return 0;
}


//NOTE: this does currently not work as slot alloc does not reserve enough slots which triggers another frame_alloc
static const char *request_many_frames_description = "Request 261 RAM capabilities at once (this will guaranteed trigger a slot refill)";

static int request_many_frames_test(void) {

    errval_t err;

    //int sample_interval = 20;
    int sample_interval = 4;

    int n_allocs = 261;
    size_t bytes = BASE_PAGE_SIZE;
    size_t ret_size_arr[n_allocs];
    struct capref frame_arr[n_allocs];

    bool failed = false;

    debug_printf("Requesting %d frames\n", n_allocs);
    debug_printf("Sample (every %d-th):\n", sample_interval);

    for (int i = 0; i < n_allocs; i++) {

        err = frame_alloc(&(frame_arr[i]), bytes, &(ret_size_arr[i]));
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "%d-th frame_alloc failed\n", i);
            failed = true;
        }
        else if (i % sample_interval == 0) {
            debug_printf("Successfully requested %d-th frame\n", i);
        }
    }

    if (failed) {
        return 1;
    }


    int *buf_arr[n_allocs];

    debug_printf("Mapping %d frames\n", n_allocs);
    debug_printf("Sample (every %d-th):\n", sample_interval);

    for (int i = 0; i < n_allocs; i++) {

        err = paging_map_frame_attr(get_current_paging_state(), (void *) &buf_arr[i], ret_size_arr[i], frame_arr[i], VREGION_FLAGS_READ_WRITE, NULL, NULL);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "%s: failure during %d-th paging_map_frame_attr\n", i);
            failed = true;
        }
        else if (i % sample_interval == 0) {
            debug_printf("Successfully mapped %d-th frame at VA: 0x%x\n", i, buf_arr[i]);
        }
    }

    if (failed) {
        return 2;
    }

    debug_printf("Read/Write check\n");
    debug_printf("Sample (every %d-th):\n", sample_interval);

    for (int i = 0; i < n_allocs; i++) {

        buf_arr[i][0] = i;
        if (i % sample_interval == 0) {
            debug_printf("Writing %d into %d-th frame at VA: 0x%x\n", i, i, buf_arr[i]);
        }
    }

    for (int i = 0; i < n_allocs; i++) {

        if (i != buf_arr[i][0]) {
            debug_printf("%d-th Read/Write check failed at 0x%x\n", i, buf_arr[i]);
            debug_printf("Expected: %d\n", i);
            debug_printf("Read: %d\n", *((int*) buf_arr[i]));
            failed = true;
        }
    }

    if (failed) {
        return 3;
    }

    return 0;
}

//--- large messages one core ----------------------------------------------------------------------



//--- integration tests ----------------------------------------------------------------------------


static const char *memeater_description = "Launch memeater (PRESS ANY KEY afterwards to refresh menu)";

static int memeater_test(void) {
    // launch of the manager directly here
    struct aos_rpc *demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("Could not find Demeter process service to launch Zeus\n");
        return 1;
    }

    domainid_t memeater_id = 0;
    errval_t err = aos_rpc_process_spawn(demeter, "memeater", 0, &memeater_id);
    if (err_is_fail(err)) {
        debug_printf("Could not launch memeater\n");
        return 2;
    }

    // PRESS ANY KEY
    getchar();

    return 0;
}

static const char *zeus_description = "Launch domain manager Zeus (note: not returning to test menu at the moment)";

static int zeus_test(void) {
    // launch of the manager directly here
    struct aos_rpc *demeter = aos_rpc_get_process_channel();
    if (!demeter) {
        debug_printf("Could not find Demeter process service to launch Zeus\n");
        return 1;
    }

    domainid_t zeus_id = 0;
    errval_t err = aos_rpc_process_spawn(demeter, "zeus", 0, &zeus_id);
    if (err_is_fail(err)) {
        debug_printf("Could not launch Zeus\n");
        return 2;
    }

    // current workaround to avoid the test menu interfering with Zeus
    while (true) {
        // nothing
    }

    return 0;
}


//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "Message Passing";

void testing_add_tests_03_message_passing(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // small messages one core

    testing_add_test(h, rpc_get_ram_cap_test, rpc_get_ram_cap_description, true);
    testing_add_test(h, frame_alloc_call_test, frame_alloc_call_description, true);
    testing_add_test(h, request_many_frames_test, request_many_frames_description, true);

    // large messages one core


    // later small and large messages multi core


    // milestone integration tests
    testing_add_test(h, memeater_test, memeater_description, false);
    testing_add_test(h, zeus_test, zeus_description, false);
}
