/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * group C
 * v1.1 2017-09-29 / 2017-11-15 pisch
 */

#include <testing/testing.h>
#include <stdio.h>
#include <stdlib.h>

// add includes of submodules here
#ifdef __TESTING_INIT__
#include <testing/ti01_memory_system.h>
#include <testing/ti02_process_creation.h>
#define MENU_STR "Group C AOS Barrelfish low-level test suite"
#define EXIT_STR "exit (use this to launch 2nd core, launch start program (Zeus or Artemis), and start event handlers)"

#elif __TESTING_PROGRAM__
#include <testing/tp01_memory_system.h>
#include <testing/tp02_process_creation.h>
#include <testing/tp03_message_passing.h>
#include <testing/tp04_self_paging.h>
#include <testing/tp05_multicore.h>
#include <testing/tp06_ump.h>
#include <testing/tp07_final.h>
#define MENU_STR "Artemis: Group C AOS Barrelfish test program"
#define EXIT_STR "exit (ends Artemis)"

#else
#error Type of library must be defined

#endif

typedef struct testing_test_descriptor {
    struct testing_test_descriptor *next;
    testing_test_function_t f;
    const char * description;
    char menu_char;
} testing_test_descriptor_t;


struct testing_menu_descriptor {
    testing_menu_descriptor_t *next;
    testing_test_descriptor_t *head_i;
    testing_test_descriptor_t *tail_i;
    testing_test_descriptor_t *head_e;
    testing_test_descriptor_t *tail_e;
    int count;
    int count_i;
    const char *description;
    char menu_char;
};


static testing_menu_descriptor_t *main_menu_head = NULL;
static testing_menu_descriptor_t *main_menu_tail = NULL;
static int main_menu_count = 0;

static int all_tests_count = 0;
static int running_all_tests_count = 0;


#define MAX_MENU_NR 61

static const char chars[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
// 0 is used for all tests and only here for easier index use

#define ALL_TESTS_CHAR '0'
#define EXIT_SHELL_CHAR '#'


//--- adding tests ---------------------------------------------------------------------------------

#ifdef __TESTING_INIT__
void testing_add_tests(void) {
    // loads the ti versions of the test cases
    testing_add_tests_01_memory_system();
    testing_add_tests_02_process_creation();
    // add more submodules here (if needed)
    // most modules should go to program now
}

#elif __TESTING_PROGRAM__
void testing_add_tests(void) {
    // loads the tp versions of the test cases
    testing_add_tests_01_memory_system();
    testing_add_tests_02_process_creation();
    testing_add_tests_03_message_passing();
    testing_add_tests_04_self_paging();
    testing_add_tests_05_multicore();
    testing_add_tests_06_ump();
    testing_add_tests_07_final();
    // add more submodules here
}

#else
#error Type of library must be defined

#endif


testing_menu_handle_t testing_add_menu(const char *description) {
    if (main_menu_count >= MAX_MENU_NR) {
        debug_printf("ERROR in %s(): max number of menu items reached.\n", __func__);
        return NULL;
    }

    testing_menu_descriptor_t *item = calloc(1, sizeof(*item)); // initializes all 0 values
    if (!item) {
        debug_printf("ERROR in %s(): not enough memory in heap.\n", __func__);
        return NULL;
    }
    main_menu_count++; // inc here because chars[] is use with 1 base index

    item->description = description;
    item->menu_char = chars[main_menu_count];

    if (main_menu_tail) {
        main_menu_tail->next = item;
    }
    else {
        main_menu_head = item;

    }
    main_menu_tail = item;

    return item;
}


void testing_add_test(testing_menu_handle_t menu, testing_test_function_t f,
                      const char *description, bool run_in_all_tests) {

    assert(menu);
    assert(f);
    assert(description);

    if (menu->count >= MAX_MENU_NR) {
        debug_printf("ERROR in %s(): too many tests for this sub menu.\n", __func__);
        return;
    }

    testing_test_descriptor_t *test = calloc(1, sizeof(*test));
    if (!test) {
        debug_printf("ERROR in %s(): not enough memory in heap.\n", __func__);
        return;
    }

    test->f = f;
    test->description = description;
    // char is assigned dynamically during print
    menu->count++;
    all_tests_count++;
    if (run_in_all_tests) {
        menu->count_i++;
        running_all_tests_count++;
        if (menu->tail_i) {
            menu->tail_i->next = test;
        }
        else {
            menu->head_i = test;
        }
        menu->tail_i = test;
    }
    else {
        if (menu->tail_e) {
            menu->tail_e->next = test;
        }
        else {
            menu->head_e = test;
        }
        menu->tail_e = test;
    }
}


//--- testing and menu helper functions ------------------------------------------------------------

#define NEUTRAL_LINE  "####################################################################################################"
#define POSITIVE_LINE "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
#define NEGATIVE_LINE "----------------------------------------------------------------------------------------------------"

#define WHITE "\033[0m"
#define RED   "\033[31m"
#define GREEN "\033[32m"
#define BLUE  "\033[34m"

// runs one test
static int run_test_helper(testing_test_descriptor_t *test) {
    assert(test);
    printf("\n\n" BLUE NEUTRAL_LINE WHITE "\n"
           BLUE "Running test %s." WHITE "\n"
           BLUE NEUTRAL_LINE WHITE "\n\n",
           test->description);
    int r = test->f();
    if (r == TESTING_ALL_OK) {
        printf("\n\n" GREEN POSITIVE_LINE WHITE "\n"
               GREEN "Test %s OK." WHITE "\n"
               GREEN POSITIVE_LINE WHITE "\n\n",
               test->description);
    }
    else {
        printf("\n\n" RED NEGATIVE_LINE WHITE "\n"
               RED "ERROR %d in test %s. See error messages above for details." WHITE "\n"
               RED NEGATIVE_LINE WHITE "\n\n", r,
               test->description);
    }
    return r;
}

// runs the tests of all menus, if menu == NULL
static int run_all_tests_helper(testing_menu_descriptor_t *menu) {
    if (menu) {
        printf("\n\nRunning all tests of menu %s.\n\n", menu->description);
        testing_test_descriptor_t *current = menu->head_i;
        while (current) {
            int r = run_test_helper(current);
            if (r != TESTING_ALL_OK) {
                printf("\n\nTest %s failed with error code %d.\n"
                       "Running all tests of menu %s stopped.\n\n",
                       current->description, r, menu->description);
                return r;
            }
            current = current->next;
        }
    }
    else {
        printf("\n\nRunning all tests of the test suite.\n\n");
        testing_menu_descriptor_t *current = main_menu_head;
        while (current) {
            int r = run_all_tests_helper(current);
            if (r != TESTING_ALL_OK) {
                printf("\nERROR in last executed test\n");
                return r;
            }
            current = current->next;
        }
    }

    return TESTING_ALL_OK;
}


//--- testing based on menu choice -----------------------------------------------------------------

static int run_test(testing_menu_descriptor_t *menu, char c) {
    assert(menu);

    if (c == EXIT_SHELL_CHAR) {
        return TESTING_EXIT_SHELL;
    }

    if (c == ALL_TESTS_CHAR) {
        if (0 < menu->count_i) {
            return run_all_tests_helper(menu);
        }
        else {
            return TESTING_OUT_OF_RANGE;
        }
    }

    testing_test_descriptor_t *current = menu->head_i;
    while (current) {
        if (current->menu_char == c) {
            return run_test_helper(current);
        }
        current = current->next;
    }

    current = menu->head_e;
    while (current) {
        if (current->menu_char == c) {
            return run_test_helper(current);
        }
        current = current->next;
    }


    return TESTING_OUT_OF_RANGE;
}

static void print_submenu(testing_menu_descriptor_t *menu) {
    assert(menu);
    printf(" \n");
    printf("------ " MENU_STR " ------\n");
    printf("\nMenu %s (%d tests):\n", menu->description, menu->count);

    int char_count = 1;
    int line_count = 0;
    if (0 < menu->count_i) {
        printf(" 0 Run all %d non-infinitely running tests of this test menu\n", menu->count_i);
        line_count++;
        testing_test_descriptor_t *current = menu->head_i;
        while (current) {
            current->menu_char = chars[char_count];
            char_count++;
            printf(" %c %s\n", current->menu_char, current->description);
            line_count++;
            if (line_count == 10) {
                printf(" \n");
                line_count = 0;
            }
            current = current->next;
        }
        if (line_count < 10) {
            printf("\n");
        }
    }
    else {
        printf(" 0 not available (no non-infinetely running tests defined at the moment)\n\n");
    }

    if (menu->head_e) {
        printf("Tests that do not terminate by design (excluded from 'run all tests')\n");
        line_count = 0;
        testing_test_descriptor_t *current = menu->head_e;
        while (current) {
            current->menu_char = chars[char_count];
            char_count++;
            printf(" %c %s\n", current->menu_char, current->description);
            line_count++;
            if (line_count == 10) {
                printf(" \n");
                line_count = 0;
            }
            current = current->next;
        }
        if (line_count < 10) {
            printf(" \n");
        }
    }

    printf(" %c back to main menu\n", EXIT_SHELL_CHAR);
    printf(" \n");
    printf("Please select\n");
}

static void run_submenu_helper(testing_menu_descriptor_t *menu) {
    bool run = true;
    while (run) {
        print_submenu(menu);
        char c = getchar();
        printf("%c\n", c, (int)c); // echo needed (difference to local testing)
        int r = run_test(menu, c);
        switch (r) {
            case TESTING_ALL_OK:
                printf("Test OK\n");
                break;
            case TESTING_OUT_OF_RANGE:
                printf("unknown command\n");
                break;
            case TESTING_EXIT_SHELL:
                run = false;
                break;
            default:
                printf("Unknown code %d\n", r);
        }
    }
    printf("Back to main menu.\n");
}

static int run_submenu(char c) {
    if (c == EXIT_SHELL_CHAR) {
        return TESTING_EXIT_SHELL;
    }

    if (c == ALL_TESTS_CHAR) {
        return testing_run_all_tests();
    }

    testing_menu_descriptor_t *current = main_menu_head;
    while (current) {
        if (current->menu_char == c) {
            run_submenu_helper(current);
            return TESTING_ALL_OK;
        }
        current = current->next;
    }

    return TESTING_OUT_OF_RANGE;
}

static void print_shell_menu(void) {
    printf("\n------ " MENU_STR ": %d tests available ------\n", all_tests_count);
    printf("\nMain menu:\n");
    printf(" 0 Run all %d non-infinitely running tests of the full test suite\n", running_all_tests_count);
    int line_count = 1;
    testing_menu_descriptor_t *current = main_menu_head;
    while (current) {
        printf(" %c %s\n", current->menu_char, current->description);
        line_count++;
        if (line_count == 10) {
            printf(" \n");
            line_count = 0;
        }
        current = current->next;
    }

    if (line_count < 10) {
        printf(" \n");
    }
    printf(" %c " EXIT_STR "\n", EXIT_SHELL_CHAR);
    printf(" \n");
    printf("Please select\n");
}


//--- again functions from the public interface ----------------------------------------------------

void testing_run_shell(void) {
    bool run = true;
    while (run) {
        print_shell_menu();
        char c = getchar();
        printf("%c\n", c, (int)c); // echo needed (difference to local testing)
        int r = run_submenu(c);
        switch (r) {
            case TESTING_ALL_OK:
                break;
            case TESTING_OUT_OF_RANGE:
                printf("unknown command\n");
                break;
            case TESTING_EXIT_SHELL:
                run = false;
                break;
            default:
                printf("Unknown code %d\n", r);
        }
    }
    printf("Bye.\n");
}


int testing_run_all_tests(void) {
    return run_all_tests_helper(NULL);
}


// deletes all lists of tests the menu
static void cleanup_helper(testing_menu_descriptor_t *menu) {
    testing_test_descriptor_t *current = menu->head_i;
    while (current) {
        testing_test_descriptor_t *to_delete = current;
        current = current->next;
        free(to_delete);
    }

    current = menu->head_e;
    while (current) {
        testing_test_descriptor_t *to_delete = current;
        current = current->next;
        free(to_delete);
    }
}

void testing_cleanup(void) {
    testing_menu_descriptor_t *current = main_menu_head;
    while (current) {
        cleanup_helper(current);
        testing_menu_descriptor_t *to_delete = current;
        current = current->next;
        free(to_delete);
    }
}
