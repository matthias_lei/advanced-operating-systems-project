/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 01 memory system
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/ti01_memory_system.h>

// note: this global variable is only available where the RAM server (Dionysos)
// îs running. It's currently allocated in mem_alloc.c from usr/init
#include <mm/mm.h>
extern struct mm aos_mm;


//--- import shared functions

#include "ts01_memory_system_shared_include.h"


//--- the tests assume an initialized aos_mm -------------------------------------------------------

//--- specific tests:
// - note: mm_dump_mmnodes needs to be run on the server side
// - note: mm_free needs to be run on the server side

static const char *mm_some_allocations_and_frees_description = "MM: Allocate and free some RAM capabilities of various sizes";

static int mm_some_allocations_and_frees_test(void) {
    debug_printf("mm nodes before testing\n");
    mm_dump_mmnodes(&aos_mm);

    // allocations
    struct capref cr1;
    errval_t e;
    int r = allocate_and_assert(16384, 16384, 16384, 16384, &cr1, &e);
    if (r != 0) {
        debug_printf("error %d in first mm_alloc_aligned()\n", r);
        return 1;
    }

    struct capref cr2;
    r = allocate_and_assert(1024, 1024, MM_MIN_MEMORY_ALLOCATION, MM_MIN_MEMORY_ALLOCATION, &cr2, &e);
    if (r != 0) {
        debug_printf("error %d in second mm_alloc_aligned()\n", r);
        return 2;
    }

    struct capref cr3;
    r = allocate_and_assert(8192, 8192, 8192, 8192, &cr3, &e);
    if (r != 0) {
        debug_printf("error %d in third mm_alloc_aligned()\n", r);
        return 3;
    }

    struct capref cr4;
    r = allocate_and_assert(16384, 16384, 16384, 16384, &cr4, &e);
    if (r != 0) {
        debug_printf("error %d in 4th mm_alloc_aligned()\n", r);
        return 4;
    }

    // more tests

    debug_printf("mm nodes after 4 successful allocations.\n");
    mm_dump_mmnodes(&aos_mm);


    debug_printf("mm_free() test. additionally, freeing happens in a permutated order than allocation.\n");

    // note: the base and size arguments in mm_free() are ignored!
    e = mm_free(&aos_mm, cr3, 0, 0);
    if (!err_is_ok(e)) {
        DEBUG_ERR(e, "in first mm_free()");
        return 5;
    }

    e = mm_free(&aos_mm, cr2, 0, 0);
    if (!err_is_ok(e)) {
        DEBUG_ERR(e, "in second mm_free()");
        return 6;
    }

    e = mm_free(&aos_mm, cr1, 0, 0);
    if (!err_is_ok(e)) {
        DEBUG_ERR(e, "in third mm_free()");
        return 7;
    }

    e = mm_free(&aos_mm, cr4, 0, 0);
    if (!err_is_ok(e)) {
        DEBUG_ERR(e, "in 4th mm_free()");
        return 8;
    }

    debug_printf("mm nodes after 4 frees in different order. No leakage\n");
    mm_dump_mmnodes(&aos_mm);

    return 0;
}


static const char *mm_check_upgrade_description = "MM: Request 6 KiB, expect 8 KiB (is additionally a corner case)";

static int mm_check_upgrade_test(void) {
    debug_printf("mm nodes before testing\n");
    mm_dump_mmnodes(&aos_mm);

    struct capref cr1;
    errval_t e;
    int r = allocate_and_assert(6144, 6144, 8192, 8192, &cr1, &e);
    if (r != 0) {
        debug_printf("error %d in mm_alloc_aligned()\n", r);
        return 1;
    }

    debug_printf("mm nodes after testing\n");
    mm_dump_mmnodes(&aos_mm);

    return 0;
}


static const char *mm_check_way_too_much_description = "MM: request way too much memory: 3 GiB (corner case)";

static int mm_check_way_too_much_test(void) {
    debug_printf("mm nodes before testing\n");
    mm_dump_mmnodes(&aos_mm);

    struct capref cr1;
    errval_t e;
    size_t request = (1 << 31) + (1 << 30);
    debug_printf("Requesting %u MiB of memory\n", request / 1024 / 1024);
    debug_printf(" \n");
    int r = allocate_and_assert(request, request, request, request, &cr1, &e);
    if (r != 1) {
        debug_printf("error %d in mm_alloc_aligned(). Should not be able to allocate that.\n", r);
        return 1;
    } else {
        debug_printf(" \n");
        debug_printf("==> Correct result: mm_alloc_aligned() cannot allocate that.\n");
    }

    debug_printf("mm nodes after testing\n");
    mm_dump_mmnodes(&aos_mm);

    return 0;
}

static const char *mm_alignment_smaller_ok_description = "MM: alignment <= size (ok)";

static int mm_alignment_smaller_ok_test(void) {
    debug_printf("mm nodes before testing\n");
    mm_dump_mmnodes(&aos_mm);

    struct capref cr1;
    errval_t e;
    debug_printf("MM has the limitation/requirement at the moment that alignment <= size for all requests.\n");
    debug_printf(" \n");
    int r = allocate_and_assert(16384, 8192,  16384, 8192, &cr1, &e);
    if (r != 0) {
        debug_printf("error %d in mm_alloc_aligned()\n", r);
        return 1;
    }

    debug_printf("mm nodes after testing\n");
    mm_dump_mmnodes(&aos_mm);

    return 0;
}

static const char *mm_alignment_too_large_description = "MM: alignment > size (out of scope at the moment)";

static int mm_alignment_too_large_test(void) {
    debug_printf("mm nodes before testing\n");
    mm_dump_mmnodes(&aos_mm);

    struct capref cr1;
    errval_t e;
    debug_printf("MM has the limitation/requirement at the moment that alignment <= size for all requests.\n");
    debug_printf(" \n");
    int r = allocate_and_assert(8192, 16384, 8192, 16384, &cr1, &e);
    if (r != 1) {
        debug_printf("error %d in mm_alloc_aligned(). Should not be able to allocate that at the moment.\n", r);
        return 1;
    } else {
        debug_printf(" \n");
        debug_printf("==> Correct result: mm_alloc_aligned() cannot allocate that at the moment.\n");
    }

    debug_printf("mm nodes after testing\n");
    mm_dump_mmnodes(&aos_mm);

    return 0;
}


//test that reproduces bug jmeier found in barrelfish code
#define BF_BUG_N_BLOCKS 15000
static const char *mm_barrelfish_bug_reproduction_description = "Bug in Barrelfish: Trying to allocate 15'000 4 KiB RAM capabilities to reproduce barrelfish bug";
static int mm_barrelfish_bug_reproduction_test(void) {
    debug_printf("No dumping of mmnodes for less console output.\n");

    size_t request_size = 4096;
    size_t request_alignment = 4096;

    static struct capref caprefs[BF_BUG_N_BLOCKS];

    debug_printf("allocating & freeing %d blocks of size %d KiB.\n", BF_BUG_N_BLOCKS, request_size / 1024);
    debug_printf("this puts stress on all parts: slab allocation/refill, slot allocation/refill, memory manager\n");

    for (int i = 0; i < BF_BUG_N_BLOCKS; i++) {
        // slab allocate
        errval_t e = mm_alloc_aligned(&aos_mm, request_size, request_alignment, &(caprefs[i]));
        if (!err_is_ok(e)) {
            debug_printf("error in mm_alloc_aligned(): problems with too many allocations!\n");
            return e;
        }
        //printf("allocated #%d\n", i);
    }

    debug_printf("all %d blocks allocated.\n", BF_BUG_N_BLOCKS);

    // free after all allocations, s.t. 4KB blocks cannot be reused directly
    for (int i = 0; i < BF_BUG_N_BLOCKS; i++) {
        // slab free
        errval_t e = mm_free(&aos_mm, caprefs[i], 0, 0);
        // note: address and size are ignored in mm_free (as discussed with AOS support)
        if(!err_is_ok(e)){
            debug_printf("error in mm_free(): slab space doesn't get refilled (non-tracking mode)!\n");
            debug_printf("==> might be because refill not yet implemented.\n");
            return e;
        }
    }

    debug_printf("released mm nodes after testing\n");

    debug_printf("No errors observed. Refill works!\n");
    debug_printf("Please note: this puts stress on all parts: slab allocation/refill, slot allocation/refill, memory manager\n");

    return 0;
}

//test that infinitely, alternatingly allocates and frees blocks of various sizes
#define MAX_REQUEST_SIZE_LOG MM_BUCKETS_SIZE - 5 //gives 2^26 bytes = 64 MB of maximum memory block request size
static const char *mm_infinite_allocation_and_freeing_description = "MM: infinitely allocates and frees blocks of various sizes (WARNING: DOES NOT STOP).";
static int mm_infinite_allocation_and_freeing_test(void) {
    //initialize size array 
    size_t sizes[MAX_REQUEST_SIZE_LOG]; //gives a max size for requests of 2^26 bytes = 64MB
    for (int j = 0; j < MAX_REQUEST_SIZE_LOG; j++) {
        sizes[j] = 0x1 << j;
    }
    
    debug_printf("Start infinite loop:\n");
    struct capref cr; //one location suffices, for testing purposes we can overwrite and forget old capabilities
    errval_t e;
    int r;
    uint64_t i = 0;
    while (true) {
        
        for (int j = 0; j < MAX_REQUEST_SIZE_LOG; j++) {
            size_t current_size = sizes[j];
            r = allocate_and_assert(current_size, current_size, current_size, current_size, &cr, &e);
            if (r != 0) {
                debug_printf("error %d in mm_alloc_aligned()\n", r);
                return 1;
            }

            e = mm_free(&aos_mm, cr, 0, 0);
            if (!err_is_ok(e)) {
                debug_printf("error %d in mm_free()\n", e);
                return 2;
            }

        }

        i++;
        if (i % 10000 == 0) {
            debug_printf("Completed %" PRIuGENSIZE " rounds of allocations and freeings of various sizes.\n", i);
            mm_dump_mmnodes(&aos_mm);
        }

    }

    return 0; //unreachable
}


//--- additional terminal test considering \n in the printf()
//    see observation: \n at the end of printf is considered;
//    at the beginning / middle of the string, it leads to printing only first part up to first \n

static const char *terminal_lf_description = "Bug in std lib glue code: testing LF output in printf(); fixed in latest commit";

#include <errno.h>

static int terminal_lf_test(void) {
    printf("Please observe terminal output\n");

    printf("\n");
    printf("printf(): 4 lines should be printed, named line 1 to line 4:\n");
    printf("line 1\nline 2\n");
    printf("line 3\nline 4\n");

    printf("\n");
    printf("debug_printf(): 4 lines should be printed, named line 1 to line 4:\n");
    debug_printf("line 1\nline 2\n");
    debug_printf("line 3\nline 4\n");

    // extended
    printf("\n");
    fprintf(stderr, "fprintf(stderr, ...): 4 lines should be printed, named line 1 to line 4:\n");
    fprintf(stderr, "line 1\nline 2\n");
    fprintf(stderr, "line 3\nline 4\n");

    return 0;
}



// This function tests whether merging blocks in mm_free works as expected.
// The function probes for the largest block that can be allocated. It subsequently
// tries to allocated at most 15 blocks of size only an eighth of the largest block,
// such that the largest block has to be split up to serve the requests (note: 16 available
// blocks of size an eigth of the largest block would imply there is a larger block than
// the supposedly largest block). Allocating the largest block found before should be
// impossible now. After freeing the at most 15 smaller blocks, the largest block should
// no be allocatable again, which indicates that merging is working.
// Note: The test only works under the assumption that aos_mm was initialized with a single
//       RAM block.

static const char *mm_block_merging_description = "Test merging of blocks in mm_free.";

static int mm_block_merging_test(void) {
    mm_dump_mmnodes(&aos_mm);
    errval_t err;
    struct capref cap;
    size_t start_size = 4096;
    while (err_is_ok(err = mm_alloc_aligned(&aos_mm, start_size, 1, &cap))) {
        err = mm_free(&aos_mm, cap, 0, 0);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Failed to free previously allocated RAM.\n");
            return 1;
        }
        start_size <<= 1;
    }
    if (err_no(err) != MM_ERR_OUT_OF_BOUNDS) {
        DEBUG_ERR(err, "Expected a memory out of bounds error.\n");
        return 2;
    }
    size_t largest_size = start_size >> 1;
    debug_printf("Found largest allocatable block to be of size %d KiB.\n", largest_size / 1024);
    size_t res_size = largest_size >> 3;
    while (res_size < 4096) {
        res_size <<= 1;
    }
    if (res_size >= largest_size) {
        debug_printf("There is not enough RAM available to conduct the test.\n");
        return 3;
    }
    debug_printf("Try to allocate at most 15 blocks of size %d KiB.\n", res_size / 1024);
    struct capref caps[15];
    int i = 0;
    for (; i < 15 && err_is_ok(err = mm_alloc_aligned(&aos_mm, res_size, 1, &caps[i])); i++);
    if (err_is_fail(err) && err_no(err) != MM_ERR_OUT_OF_BOUNDS) {
        DEBUG_ERR(err, "Expected a memory out of bounds error.\n");
        return 4;
    }
    debug_printf("Allocated %d blocks of size %d KiB.\n", i, res_size / 1024);
    if (err_is_ok(err = mm_alloc_aligned(&aos_mm, largest_size, 1, &cap))) {
        debug_printf("Should not have been able to allocate a block of size %d KiB.\n", largest_size / 1024);
        return 5;
    }
    debug_printf("Could not allocate a block of %d KiB as expected, since it was split up to serve for the smaller blocks allocated before.\n", largest_size / 1024);
    for (int j = 0; j < i; j++) {
        err = mm_free(&aos_mm, caps[j], 0, 0);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Failed to free previously allocated RAM.\n");
            return 6;
        }
    }
    debug_printf("Freed the %d blocks of size %d KiB that were allocated above.\n", i, res_size / 1024);
    if (err_is_fail(err = mm_alloc_aligned(&aos_mm, largest_size, 1, &cap))) {
        DEBUG_ERR(err, "Should have been able to allocate a block of size %d KiB.\n", largest_size / 1024);
        return 7;
    }
    debug_printf("Successfully allocated a block of %d KiB as expected, since while freeing the smaller blocks, they were merged again to larger blocks.\n", largest_size / 1024);
    err = mm_free(&aos_mm, cap, 0, 0);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Failed to free previously allocated RAM.\n");
        return 8;
    }
    return 0;
}

// A test case that allocates all available memory in the system, frees it, and checks that there is no memory
// leak. Crucial is in particular the part when invariant assurance may fail because of lacking memory
// and thus mm_alloc_aligned() returns with an error number -> check that there is no leak of the allocated
// memory

struct ram_list {
    struct capref cap;
    struct ram_list *next; 
};

static const char *mm_allocate_and_free_all_description = "Allocate all RAM and free it; check for leaks during invariant assurance.";

static void mm_allocate_and_free_all_test_cleanup(struct ram_list *head) {
    struct ram_list *current = head;
    while (current) {
        struct ram_list *to_remove = current;
        current = current->next;

        errval_t err = mm_free(&aos_mm, to_remove->cap, 0, 0);
        if (err_is_fail(err)) {
            debug_printf("ERROR while cleaning up in mm_free()\n");
        }

        free(to_remove);
    }
}

static int mm_allocate_and_free_all_test(void) {
    size_t iteration = 0;
    while (true) {
        iteration++;
        struct ram_list *head = NULL;
        struct ram_list *tail = NULL;
        int bit = 31;

        debug_printf("\nBefore start of iteration %u:\n", iteration);
        mm_dump_mmnodes(&aos_mm);
        getchar();

        // allocate
        while (MM_MIN_BUCKET_INDEX <= bit) {
            size_t size = (1 << bit);

            struct ram_list *node = calloc(1, sizeof(*node));
            if (!node) {
                debug_printf("ERROR: not enough heap for the test.\n");
                mm_allocate_and_free_all_test_cleanup(head);
                return 1;
            }

            debug_printf("Allocating %u KiB RAM\n", size / 1024);

            errval_t err = mm_alloc(&aos_mm, size, &(node->cap));
            if (err_is_fail(err)) {
                bit--;
                free(node);
                continue;
            }

            debug_printf("Allocated %u KiB RAM\n", size / 1024);

            if (tail) {
                tail->next = node;
                tail = node;
            }
            else {
                head = node;
                tail = node;
            }
        }

        debug_printf("\nAfter allocation iteration %u:\n", iteration);
        mm_dump_mmnodes(&aos_mm);
        getchar();

        // free
        mm_allocate_and_free_all_test_cleanup(head);
        debug_printf("\nAfter free iteration %u:\n", iteration);
        mm_dump_mmnodes(&aos_mm);
        getchar();

        // if there is a leak, it will show during the dump that checks for leaks
    }
}

// Note that in general interleavings of allocation and free sequences, a double free can not reliably be detected.
// Consider for example the sequence alloc_1, free_1, alloc_2, free_1, free_2 where the subscript indicates the
// memory block reference involved in the operation. Furthermore, assume that alloc_2 hands out the memory block
// previously freed in free_1. The second free_1, which is the actual a double free, will just silently free the
// memory block retrieved in alloc_2. However, the semantically legal free_2 will fail, as the memory block is
// already marked free. Therefore, the following function only tests sequences alloc_i, free_i, free_i, without
// interleavings.

static const char *mm_double_free_description = "Test double free detection in allocated -> free -> free sequences.";

static int mm_double_free_test(void) {
    errval_t err;
    struct capref cap;
    size_t start_size = 4096;
    while (err_is_ok(err = mm_alloc_aligned(&aos_mm, start_size, 1, &cap))) {
        err = mm_free(&aos_mm, cap, 0, 0);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Failed to free previously allocated RAM.\n");
            return 1;
        }
        // double free should fail
        err = mm_free(&aos_mm, cap, 0, 0);
        if (err_is_ok(err)) {
            debug_printf("Double free remained undetected.\n");
            return 2;
        }
        debug_printf("Detected double free on block of size %d.\n", start_size / 1024);
        start_size <<= 1;
    }
    debug_printf("Successfully detected all tested double frees.\n");
    return 0;
}


// some quick prototype testing to expand MM towards returning 1 KiB RAM block if requested
// and if not needed for frames.
// note: there is a Kernel check that prevents splitting RAM caps using offsets < 4 KiB. However, it is not needed
// if one is only splitting RAM. It is e.g. allowed to split RAM into 1 KiB blocks.
//
// we have to see where to go. The lookup table for min 1 KiB blocks needs 16 MiB (not available in init)
// compared to 4 MiB with min 4 KiB blocks.

static const char *t01_split_small_ram_description = "Get 1 KiB RAM (see limitations in MM and mainly also in Kernel!)";
static int t01_split_small_ram_test(void) {
    struct capref ram;

    errval_t err = mm_alloc_aligned(&aos_mm, 4096, 4096, &ram);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in mm_alloc_aligned().\n");
        return 1;
    }

    struct capref small_ram;
    err = aos_mm.slot_alloc(aos_mm.slot_alloc_inst, 4, &small_ram);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in slot alloc.\n");
        return 2;
    }

    // note: no kernel hack is needed to get 1 KiB large blocks as long
    // as no offset is needed, or offset is in 4 KiB steps.
    err = cap_retype(small_ram, ram, 0, ObjType_RAM, 1024, 4);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error cap_retype() splitting 4 KiB into 4x 1 KiB.\n");
        return 3;
    }

    struct capref frame;
    err = aos_mm.slot_alloc(aos_mm.slot_alloc_inst, 1, &frame);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error in slot alloc.\n");
        return 4;
    }

    err = cap_retype(frame, small_ram, 0, ObjType_Frame, 1024, 1);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error frame retype. --> expected error for RAM size 1 KiB.\n");
    }
    else {
        printf("here, an error is actually expected for a retype of 1 KiB RAM to a frame\n");
        return 5;
    }

    // final test with kernel modification:
    // just request 1 KiB RAM from memory manager

    debug_printf("Request 1 KiB RAM from MM\n");

    struct capref small_ram_from_mm;

    err = mm_alloc_aligned(&aos_mm, 1024, 1024, &small_ram_from_mm);    
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "error small memory request.\n");
        return err;
    }

    struct frame_identity info;
    err = frame_identify(small_ram_from_mm, &info);
    if (!err_is_ok(err)) {
        DEBUG_ERR(err, "in frame_identify()");
        return 6;
    }

    debug_printf("Received RAM size %" PRIuGENSIZE " KiB.\n", info.bytes / 1024);

    if (info.bytes != MM_MIN_MEMORY_ALLOCATION) {
        debug_printf("ERROR: allocated memory size for request 1 KiB should be %u KiB.\n", MM_MIN_MEMORY_ALLOCATION);
        return 7;
    }

    debug_printf("Note: Request for 1 KiB will currently return a %u KiB block. But we are working on allowing 1 KiB blocks\n",
           MM_MIN_MEMORY_ALLOCATION / 1024);

    mm_dump_mmnodes(&aos_mm);

    return 0;
}



//--- add the tests here to load them to the test suite

static const char *t01_menu_description = "Memory manager";

void testing_add_tests_01_memory_system(void) {
    testing_menu_handle_t h = testing_add_menu(t01_menu_description);

    testing_add_test(h, hello_world_test, hello_world_description, true);

    // libmm tests
    testing_add_test(h, mm_some_allocations_and_frees_test, mm_some_allocations_and_frees_description, true);
    testing_add_test(h, mm_check_upgrade_test, mm_check_upgrade_description, true);
    testing_add_test(h, mm_check_way_too_much_test, mm_check_way_too_much_description, true);
    testing_add_test(h, mm_alignment_smaller_ok_test, mm_alignment_smaller_ok_description, true);
    testing_add_test(h, mm_alignment_too_large_test, mm_alignment_too_large_description, true);
    testing_add_test(h, mm_infinite_allocation_and_freeing_test, mm_infinite_allocation_and_freeing_description, false);
    testing_add_test(h, mm_block_merging_test, mm_block_merging_description, true);
    testing_add_test(h, mm_double_free_test, mm_double_free_description, true);
    testing_add_test(h, mm_stress_4_test, mm_stress_4_description, false);
    testing_add_test(h, mm_stress_8_test, mm_stress_8_description, false);
    testing_add_test(h, mm_stress_16_test, mm_stress_16_description, false);
    testing_add_test(h, mm_stress_32_test, mm_stress_32_description, false);
    testing_add_test(h, mm_stress_64_test, mm_stress_64_description, false);
    testing_add_test(h, mm_stress_128_test, mm_stress_128_description, false);
    testing_add_test(h, mm_stress_256_test, mm_stress_256_description, false);
    testing_add_test(h, mm_stress_512_test, mm_stress_512_description, false);
    testing_add_test(h, mm_stress_1024_test, mm_stress_1024_description, false);
    testing_add_test(h, t01_split_small_ram_test, t01_split_small_ram_description, false);
    // TODO: more
    testing_add_test(h, mm_allocate_and_free_all_test, mm_allocate_and_free_all_description, false);

    // official milestone tests
    testing_add_test(h, milestone1_running_out_of_slab_and_slot_space_test, milestone1_running_out_of_slab_and_slot_space_description, true);

    // paging tests
    testing_add_test(h, paging_map_frame_to_two_virtual_addresses_test, paging_map_frame_to_two_virtual_addresses_description, true);
    // TODO: more

    // terminal test: bug
    testing_add_test(h, terminal_lf_test, terminal_lf_description, false);

    // barrelfish bug reproduction tests
    testing_add_test(h, mm_barrelfish_bug_reproduction_test, mm_barrelfish_bug_reproduction_description, false);

}
