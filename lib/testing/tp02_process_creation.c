/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * implementation and private interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch
 */

#include <testing/tp02_process_creation.h>

//--- import shared functions

#include "ts02_process_creation_shared_include.h"


//--- add the tests here to load them to the test suite --------------------------------------------

static const char *menu_description = "Mapping and process creation";

void testing_add_tests_02_process_creation(void) {
    testing_menu_handle_t h = testing_add_menu(menu_description);

    // generic / demo test case: false because it needs user input and special terminal setting.
    testing_add_test(h, t02_test_input_output_test, t02_test_input_output_description, false);
    testing_add_test(h, t02_test_debugging_helper_test, t02_test_debugging_helper_description, false);

    // page mapping / virtual memory tests
    testing_add_test(h, simple_pagemapping_test, simple_pagemapping_description, true);
    testing_add_test(h, moderate_pagemapping_test, moderate_pagemapping_description, true);
    testing_add_test(h, extensive_pagemapping_test, extensive_pagemapping_description, true);
    testing_add_test(h, write_to_ro_page_test, write_to_ro_page_description, false);

    // process spawning and domain manager do not make sense here
    // they must be called from the low-level test suite
    // and some functionality is now inside of Zeus that is using the Demetra service
}
