/**
 * Domain Manager library that is used by the process service (Demeter).
 * version 2017-10-27, group C
 */

#include <domman/domman.h>

struct domain_info {
    char *name;
    domainid_t domain_id;
};

struct domain_info_list_entry {
    struct domain_info info;
    struct domain_info_list_entry *next;
};

struct domain_info_list {
    struct domain_info_list_entry *head;
    size_t size;
};

struct domain_manager {
    struct domain_info_list domain_list;
    struct domain_info_list free_domain_id_list;
    domainid_t next_id;
};

static struct domain_manager domain_manager_inst;

static void domain_info_list_init(struct domain_info_list *list) {
    list->head = NULL;
    list->size = 0;
}

static void domain_info_list_insert(struct domain_info_list *list, struct domain_info_list_entry *entry) {
    entry->next = list->head;
    list->head = entry;
    list->size++;
}

static struct domain_info_list_entry* domain_info_list_retrieve_head(struct domain_info_list *list) {
    struct domain_info_list_entry *head = list->head;
    if (head) {
        list->head = head->next;
        list->size--;
    }
    return head;
}

static struct domain_info* domain_info_list_find(const struct domain_info_list *list, domainid_t domain_id) {
    struct domain_info_list_entry *current = list->head;
    while (current) {
        if (current->info.domain_id == domain_id) {
            break;
        }
        current = current->next;
    }
    return &current->info;
}

errval_t domain_manager_init(void) {
    domain_info_list_init(&domain_manager_inst.domain_list);
    domain_info_list_init(&domain_manager_inst.free_domain_id_list);
    domain_manager_inst.next_id = 0;
    return SYS_ERR_OK;
}

// length of core description string " (core xxx)",
// where xxx is the core_id (max 255, thus 3 characters),
// also note the preceeding whitespace as well as the zero terminator
#define CORE_DESCRIPTION_LEN 12

errval_t add_domain(const struct spawninfo *si, coreid_t core_id, domainid_t *domain_id) {
    assert(si);
    assert(domain_id);

    struct domain_info_list_entry *free_domain_id = domain_info_list_retrieve_head(&domain_manager_inst.free_domain_id_list);
    if (!free_domain_id) {
        free_domain_id = malloc(sizeof(struct domain_info_list_entry));
        if (!free_domain_id) {
            return LIB_ERR_MALLOC_FAIL;
        }
        free_domain_id->info.domain_id = domain_manager_inst.next_id++;
    }
    char core_desc[CORE_DESCRIPTION_LEN];
    size_t name_len = strlen(si->binary_name) + 1 + snprintf(core_desc, sizeof(core_desc), " (core %d)", core_id);
    free_domain_id->info.name = malloc(name_len);
    if (!free_domain_id->info.name) {
        domain_info_list_insert(&domain_manager_inst.free_domain_id_list, free_domain_id);
        return LIB_ERR_MALLOC_FAIL;
    }
    strncpy(free_domain_id->info.name, si->binary_name, name_len);
    strcat(free_domain_id->info.name, core_desc);
    *domain_id = free_domain_id->info.domain_id;
    domain_info_list_insert(&domain_manager_inst.domain_list, free_domain_id);

    return SYS_ERR_OK;
}

errval_t get_domain_from_id(const domainid_t domain_id, char **name) {
    struct domain_info *info = domain_info_list_find(&domain_manager_inst.domain_list, domain_id);
    if (!info) {
        *name = NULL;
        return SPAWN_ERR_DOMAIN_NOTFOUND;
    }
    size_t name_len = strlen(info->name);
    *name = malloc(name_len);
    if (!*name) {
        return LIB_ERR_MALLOC_FAIL;
    }
    strcpy(*name, info->name);
    return SYS_ERR_OK;
}

errval_t get_all_domain_ids(size_t *size, domainid_t **domain_ids) {
    *size = domain_manager_inst.domain_list.size;
    if (domain_ids == NULL) {
        return SYS_ERR_OK;
    }

    *domain_ids = malloc(*size * sizeof(domainid_t));
    if (!*domain_ids) {
        *size = 0;
        return LIB_ERR_MALLOC_FAIL;
    }

    struct domain_info_list_entry *current = domain_manager_inst.domain_list.head;
    for (size_t i = 0; i < *size; i++) {
        assert(current);
        (*domain_ids)[i] = current->info.domain_id;
        current = current->next;
    }
    return SYS_ERR_OK;
}
