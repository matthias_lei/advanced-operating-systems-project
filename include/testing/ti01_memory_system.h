/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * public interface for 01 memory system
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch 
 */

#ifndef AOS_TESTING_TI01_MEMORY_SYSTEM_H_
#define AOS_TESTING_TI01_MEMORY_SYSTEM_H_

//__BEGIN_DECLS

void testing_add_tests_01_memory_system(void);

//__END_DECLS

#endif // AOS_TESTING_TI01_MEMORY_SYSTEM_H_
