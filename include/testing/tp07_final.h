/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * public interface for 05 multicore
 *
 * group C
 * v0.1 2017-09-29 / 2017-11-28 pisch
 */

#ifndef AOS_TESTING_TP07_FINAL_H_
#define AOS_TESTING_TP07_FINAL_H_

//__BEGIN_DECLS

void testing_add_tests_07_final(void);

//__END_DECLS

#endif // AOS_TESTING_TP07_FINAL_H_
