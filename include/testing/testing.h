/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * the typical usage at the desired test site is
 * - for blocking / sync situations:
 *   testing_add_tests(); // once
 *   testing_run_shell(); // exits on input #; can be launched again later
 *
 * - for non-blocking / async situations:
 *   testing_add_tests(); // once
 *   ....
 *   repetitive use of
 *   testing_print_shell_menu();
 *   some async c = getchar();
 *   int r = testing_run_test(c);
 *   check r for or or error codes (specifically out of range and exit shell error codes)
 *
 *
 * - improved version with hierarchical menu structure
 *   Level 1 and level 2
 * - all tests are shown in menu
 * - but the tests that may be endless / blocking due to bugs at the end of each menu
 * - number of tests only limited to number of characters available in sub menu
 * - same amount of characters available in menus -> at the moment up to 61^2 tests possible
 *
 * - 2 libraries are built for linking: one for init and one for a test program
 *   that uses message passing to run the tests, see prefixes ti and tp in header and C files
 *
 * group C
 * v1.1 2017-09-29 / 2017-10-25 pisch 
 */

#ifndef AOS_TESTING_TESTING_H_
#define AOS_TESTING_TESTING_H_

#include <aos/debug.h>

//__BEGIN_DECLS

// specific return codes for testing_run_test(char c)
#define TESTING_ALL_OK 0
#define TESTING_OUT_OF_RANGE (-99)
#define TESTING_EXIT_SHELL (-98)

/**
 * all tests should conform to this signature.
 * \return 0 = OK
 *         otherwise = error code   
 */
typedef int (*testing_test_function_t)(void);


/**
 * used to have a handle for a main menu to add more tests to this submenu
 * it shall remain opaque to the user
 */

struct testing_menu_descriptor;
typedef struct testing_menu_descriptor testing_menu_descriptor_t;
typedef testing_menu_descriptor_t *testing_menu_handle_t;


/**
 * adds a new menu to the menu structure
 * the handle must be supplied when tests are added
 * note: may return NULL in case of error
 */
testing_menu_handle_t testing_add_menu(const char *description);


/**
 * \brief adds a test function to the test frame work.
 * \param menu              valid menu handle
 * \param f                 test function
 * \param description       ptr to a string that must be available during the entire time (no copy is made)
 *                          may be NULL. Then list_in_shell is always false (0) but test is run as part of
 *                          running all tests
 * \param run_in_all_tests  true (1) or false (0) -> allows excluding infinitely running tests
 */
void testing_add_test(testing_menu_handle_t menu, testing_test_function_t f,
	                  const char *description, bool run_in_all_tests);


/**
 * \brief convenience function that adds all defined functions to the test.
 *        calls the same function in the testing modules -> allows modular adjusting of functions
 *        added to the system
 */
void testing_add_tests(void);


/**
 * typical main entry point after adding the tests
 */
void testing_run_shell(void);


/**
 * runs only the tests with the flag run_in_all_tests set.
 * stops with the error code of the first failing test
 */
int testing_run_all_tests(void);


/**
 * proper cleanup
 */
void testing_cleanup(void);

//__END_DECLS

#endif // AOS_TESTING_TESTING_H_
