/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * public interface for 03 message passing
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch 
 */

#ifndef AOS_TESTING_TP03_MESSAGE_PASSING_H_
#define AOS_TESTING_TP03_MESSAGE_PASSING_H_

//__BEGIN_DECLS

void testing_add_tests_03_message_passing(void);

//__END_DECLS

#endif // AOS_TESTING_TP03_MESSAGE_PASSING_H_
