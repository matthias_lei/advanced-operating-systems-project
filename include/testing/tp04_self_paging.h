/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * public interface for 04 self paging
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch 
 */

#ifndef AOS_TESTING_TP04_SELF_PAGING_H_
#define AOS_TESTING_TP04_SELF_PAGING_H_

//__BEGIN_DECLS

void testing_add_tests_04_self_paging(void);

//__END_DECLS

#endif // AOS_TESTING_TP04_SELF_PAGING_H_
