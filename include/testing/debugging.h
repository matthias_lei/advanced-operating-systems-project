/**
 * Debug helper functions for simplified tracking of recursive code paths.
 *
 * This is a header only library. Include it in all C files where you would like to use the macros.
 * Typically, the macros are used as shown in this example:
 *
 * void function() {
 *     DEBUGGING_ENTER; // also prints function name
 *     // doing some work
 *     DEBUGGING_PRINTF("Some state %d\n", var)
 *     // note: varargs are accepted as for printf
 *     // use \n at end of line as usually (none is added by the DEBUGGING_PRINTF)
 *     if (error) {
 *         DEBUGGING_RUN(dump_state());
 *         DEBUGGING_EXIT;
 *         return;
 *     }
 *     // more work
 *     DEBUGGING_RUN(dump_state());
 *     // more things
 *     DEBUGGING_EXIT;  
 * }
 *
 * NOTE: make sure that the statement in DEBUGGING_RUN(statement) does not have any sideffects!
 *
 * Actual debugging ON and OFF are set in this header file by the AOS_TESTING_DEBUGGING_ON flag
 *
 * You can define a macro AOS_TESTING_DEBUGGING_LOCALLY_OFF before including this header file
 * to deactivate debug output for the compilation unit. Note: this flag is without effect
 * if also the next flag is defined in the main program.
 *
 * Finally, define a macro AOS_TESTING_DEBUGGING_MAIN in *one* of the binary files linked together
 * (typically the main program). This is used to instantiate the globally used variables.
 * Note: this macro must be defined *before* including this header file.
 *

 *
 * version 2017-11-12 pisch
 */


#ifndef AOS_TESTING_DEBUGGING_H_
#define AOS_TESTING_DEBUGGING_H_

#include <aos/debug.h>

// switch it off by commenting it out
#define AOS_TESTING_DEBUGGING_ON



// handle logic with optional external mofifiers
#ifdef AOS_TESTING_DEBUGGING_ON
    #ifdef AOS_TESTING_DEBUGGING_LOCALLY_OFF
        // locally off shall only take effect if MAIN is not activated
        // otherwise the needed instantiations are missing for other modules
        #ifndef AOS_TESTING_DEBUGGING_MAIN
            #undef AOS_TESTING_DEBUGGING_ON
        #endif
    #endif
#endif


// do the work
#ifdef AOS_TESTING_DEBUGGING_ON

#ifdef AOS_TESTING_DEBUGGING_MAIN
int aos_testing_debugging_indent = 0;
const char aos_testing_debugging_indents[] = "                              #D#: ";

const int aos_testing_debugging_max_indent = sizeof(aos_testing_debugging_indents) - 6;

#else
extern int aos_testing_debugging_indent;
extern const char aos_testing_debugging_indents[];
extern const int aos_testing_debugging_max_indent;
#endif // AOS_TESTING_DEBUGGING_MAIN

#define DEBUGGING_MIN(a, b) ((a) < (b) ? (a) : (b))

#define DEBUGGING_PRINT_HELPER(indent, format, data...) debug_printf("%s" format,  \
        &aos_testing_debugging_indents[aos_testing_debugging_max_indent - DEBUGGING_MIN((indent), aos_testing_debugging_max_indent)],  \
        data)

#define DEBUGGING_ENTER do {                                                       \
    aos_testing_debugging_indent++;                                                \
    DEBUGGING_PRINT_HELPER(aos_testing_debugging_indent, "ENTER %s\n", __func__);  \
} while (false)

#define DEBUGGING_EXIT do {                                                       \
    DEBUGGING_PRINT_HELPER(aos_testing_debugging_indent, "EXIT %s\n", __func__);  \
    aos_testing_debugging_indent--;                                               \
    if (aos_testing_debugging_indent < 0) { aos_testing_debugging_indent = 0; }   \
} while (false)

#define DEBUGGING_PRINTF(format, data...) DEBUGGING_PRINT_HELPER(aos_testing_debugging_indent, format, data)

#define DEBUGGING_RUN(statement) statement


#else

#define DEBUGGING_ENTER
#define DEBUGGING_EXIT
#define DEBUGGING_PRINTF(format, data...)
#define DEBUGGING_RUN(statements)

#endif // AOS_TESTING_DEBUGGING_ON

#endif // AOS_TESTING_DEBUGGING_H_
