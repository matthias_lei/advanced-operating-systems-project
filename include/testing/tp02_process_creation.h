/**
 * \file
 * \brief Small testing library / framework for our implementations in AOS.
 *
 * public interface for 02 process creation
 *
 * group C
 * v0.1 2017-09-29 / 2017-09-30 pisch 
 */

#ifndef AOS_TESTING_TP02_PROCESS_CREATION_H_
#define AOS_TESTING_TP02_PROCESS_CREATION_H_

//__BEGIN_DECLS

void testing_add_tests_02_process_creation(void);

//__END_DECLS

#endif // AOS_TESTING_TP02_PROCESS_CREATION_H_
