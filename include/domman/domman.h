#include <aos/aos.h>

#include <spawn/spawn.h>

errval_t domain_manager_init(void);

errval_t add_domain(const struct spawninfo *si, coreid_t core_id, domainid_t *domain_id);

errval_t get_domain_from_id(const domainid_t, char **name);

errval_t get_all_domain_ids(size_t *size, domainid_t **domain_ids);
