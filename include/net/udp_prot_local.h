/**
 * \file
 * \brief UDP local protocol header
 *
 * v1.0 2017-12-06 Group C
 */

#ifndef INCLUDE_NET_UDP_PROT_LOCAL_H_
#define INCLUDE_NET_UDP_PROT_LOCAL_H_


#include <aos/aos.h>
#include <net/inet_descriptorring.h>
#include <net/inet_util.h>
#include <net/ip_prot.h>
#include <net/socket_interface_local.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sys/socket.h>

struct inet_transport_vtable udp_prot_local_vtable;

int udp_prot_local_bind(void *state, const struct sockaddr *address, socklen_t address_len);
int udp_prot_local_getsockname(void *state, struct sockaddr *address, socklen_t *address_len);
ssize_t udp_prot_local_recvfrom(void *state, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
ssize_t udp_prot_local_sendto(void *state, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
void *udp_prot_local_socket(int domain, int type, int protocol);

void udp_prot_local_set_recv_handler(void *state, struct event_closure closure);
bool udp_prot_local_can_recv(void *state);

#endif /* INCLUDE_NET_UDP_PROT_LOCAL_H_ */