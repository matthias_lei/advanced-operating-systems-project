/**
 * \file
 * \brief socket interface header
 *
 * v1.0 2017-12-08 Group C
 */

#ifndef INCLUDE_NET_SOCKET_LOCAL_INTERFACE_H_
#define INCLUDE_NET_SOCKET_LOCAL_INTERFACE_H_

#include <net/inet_descriptorring.h>
#include <net/ip_prot.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sys/socket.h>

struct inet_transport_vtable {
    int (*accept)(void *, struct sockaddr *, socklen_t *);
    int (*bind)(void *, const struct sockaddr *, socklen_t);
    int (*connect)(void *, const struct sockaddr *, socklen_t);
    int (*getpeername)(void *, struct sockaddr *, socklen_t *);
    int (*getsockname)(void *, struct sockaddr *, socklen_t *);
    int (*getsockopt)(void *, int , int , void *, socklen_t *);
    int (*listen)(void *, int);
    ssize_t (*recv)(void *, void *, size_t, int);
    ssize_t (*recvfrom)(void *, void *, size_t, int, struct sockaddr *, socklen_t *);
    ssize_t (*recvmsg)(void *, struct msghdr *, int);
    ssize_t (*send)(void *, const void *, size_t, int);
    ssize_t (*sendmsg)(void *, const struct msghdr *, int);
    ssize_t (*sendto)(void *, const void *, size_t, int, const struct sockaddr *, socklen_t);
    int (*setsockopt)(void *, int, int, const void *, socklen_t);
    int (*shutdown)(void *, int);
    void *(*socket)(int, int, int);
    int (*socketpair)(int, int, int, int[2]);
    bool (*can_recv)(void *);
    void (*set_recv_handler)(void *, struct event_closure);
};

errval_t socket_interface_local_init(void);
int socket_interface_local_can_recv(int socket, bool *can_recv);
int socket_interface_local_set_recv_handler(int socket, struct event_closure closure);

int socket_interface_local_accept(int socket, struct sockaddr *address, socklen_t *address_len);
int socket_interface_local_bind(int socket, const struct sockaddr *address, socklen_t address_len);
int socket_interface_local_connect(int socket, const struct sockaddr *address, socklen_t address_len);
int socket_interface_local_getpeername(int socket, struct sockaddr *address, socklen_t *address_len);
int socket_interface_local_getsockname(int socket, struct sockaddr *address, socklen_t *address_len);
int socket_interface_local_getsockopt(int socket, int level, int option_name, void *option_value, socklen_t *option_len);
int socket_interface_local_listen(int socket, int backlog);
ssize_t socket_interface_local_recv(int socket, void *buffer, size_t length, int flags);
ssize_t socket_interface_local_recvfrom(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
ssize_t socket_interface_local_recvmsg(int socket, struct msghdr *message, int flags);
ssize_t socket_interface_local_send(int socket, const void *message, size_t length, int flags);
ssize_t socket_interface_local_sendmsg(int socket, const struct msghdr *message, int flags);
ssize_t socket_interface_local_sendto(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
int socket_interface_local_setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len);
int socket_interface_local_shutdown(int socket, int how);
int socket_interface_local_socket(int domain, int type, int protocol);
int socket_interface_local_socketpair(int domain, int type, int protocol, int socket_vector[2]);

#endif /* INCLUDE_NET_SOCKET_LOCAL_INTERFACE_H_ */