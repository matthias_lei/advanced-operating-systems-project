/**
 * \file
 * \brief SLIP header
 *
 * v1.0 2017-11-29 Group C
 */

#ifndef INCLUDE_NET_SLIP_H_
#define INCLUDE_NET_SLIP_H_

#include <aos/aos.h>

struct slip_write_buffer_data {
    void *buf;
    size_t len;
};

typedef void (*slip_input_handler_t)(uint8_t);
typedef void (*slip_packet_end_handler_t)(void);

errval_t slip_init(void);

void slip_register_input_handler(slip_input_handler_t input_handler);

void slip_register_packet_end_handler(slip_packet_end_handler_t packet_end_handler);

void slip_write(void *buf, size_t len);

void slip_write_buf_arr(struct slip_write_buffer_data **buf_arr, size_t len);

#endif /* INCLUDE_NET_SLIP_H_ */
