/**
 * \file
 * \brief remote socket interface header
 *
 * v1.0 2017-12-09 Group C
 */

#ifndef INCLUDE_NET_SOCKET_INTERFACE_REMOTE_H_
#define INCLUDE_NET_SOCKET_INTERFACE_REMOTE_H_


#include <aos/aos.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sys/types.h>
#include <sys/socket.h>

int socket_interface_remote_accept(int socket, struct sockaddr *address, socklen_t *address_len);
int socket_interface_remote_bind(int socket, const struct sockaddr *address, socklen_t address_len);
int socket_interface_remote_connect(int socket, const struct sockaddr *address, socklen_t address_len);
int socket_interface_remote_getpeername(int socket, struct sockaddr *address, socklen_t *address_len);
int socket_interface_remote_getsockname(int socket, struct sockaddr *address, socklen_t *address_len);
int socket_interface_remote_getsockopt(int socket, int level, int option_name, void *option_value, socklen_t *option_len);
int socket_interface_remote_listen(int socket, int backlog);
ssize_t socket_interface_remote_recv(int socket, void *buffer, size_t length, int flags);
ssize_t socket_interface_remote_recvfrom(int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
ssize_t socket_interface_remote_recvmsg(int socket, struct msghdr *message, int flags);
ssize_t socket_interface_remote_send(int socket, const void *message, size_t length, int flags);
ssize_t socket_interface_remote_sendmsg(int socket, const struct msghdr *message, int flags);
ssize_t socket_interface_remote_sendto(int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
int socket_interface_remote_setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len);
int socket_interface_remote_shutdown(int socket, int how);
int socket_interface_remote_socket(int domain, int type, int protocol);
int socket_interface_remote_socketpair(int domain, int type, int protocol, int socket_vector[2]);

#endif /* INCLUDE_NET_SOCKET_INTERFACE_REMOTE_H_ */