/**
 * \file
 * \brief IP sublayer implementation for packet processing.
 *        No handling of services.
 *
 * v1.0 2017-11-29 Group C
 */

#ifndef INCLUDE_NET_IP_IMPL_H_
#define INCLUDE_NET_IP_IMPL_H_

#include <aos/aos.h>
#include <netinet/in.h>
#include <netinet/ip.h>

typedef void (*ip_impl_packet_recv_handler_t)(struct ip *);

errval_t ip_impl_init(void);

void ip_impl_register_packet_recv_handler(ip_impl_packet_recv_handler_t packet_recv_handler);

void dump_ip_header(struct ip *header);

bool ip_impl_send_packet_no_options(void *buf, size_t len, uint8_t prot, struct in_addr src_addr, struct in_addr dest_addr);

#endif /* INCLUDE_NET_IP_IMPL_H_ */
