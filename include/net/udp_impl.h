/**
 * \file
 * \brief UDP Impl header
 *        The current implementation 
 *
 * v1.0 2017-12-04 Group C
 */


#ifndef INCLUDE_NET_UDP_IMPL_H_
#define INCLUDE_NET_UDP_IMPL_H_

#include <aos/aos.h>
#include <net/inet_descriptorring.h>
#include <net/ip_prot.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>


#define UDP_DGRAM_DEFAULT_SEND_BUF_SIZE 128
#define UDP_DGRAM_DEFAULT_RECV_BUF_SIZE 128

#define UDP_SOCKET_DESCRIPTOR_MAX IPPORT_MAX

struct udp {
    struct udphdr header;
    char payload[0];
};

struct udp_impl_state {
    u_short next_free_dynamic_port;

    // TODO mlei: this should be a domain-wide variable
    uint32_t next_free_id;
    struct udp_socket_state *socket_head;
    // maps port to sockets states
    struct udp_socket_state *udp_ports[IPPORT_MAX + 1];
};

struct udp_socket_state {
    struct sockaddr_in addr;
    uint32_t id;
    struct waitset_chanstate chanstate;
    struct event_closure recv_closure;

    struct inet_descriptorring recv_buf;
    struct inet_descriptorring send_buf;
};

void udp_impl_init(void);
errval_t udp_impl_socket_state_init(struct udp_socket_state *ss, size_t send_buf_size, size_t recv_buf_size);
bool udp_impl_bind_socket_state(struct udp_socket_state *ss, struct sockaddr_in *addr);
bool udp_impl_read_from_buffer(struct udp_socket_state *ss, struct ip **buf, size_t *len);
void udp_impl_dispatch_packet(void *arg);
bool udp_impl_generate_udp_header(struct udp_socket_state *ss, struct sockaddr_in *addr, struct udp *udp_packet, size_t len);
bool udp_impl_send_to_buffer(struct udp_socket_state *ss, struct udp *udp_packet, size_t udp_packet_len, struct sockaddr_in *dest_addr);

#endif /* INCLUDE_NET_UDP_IMPL_H_ */
