/**
 * \file
 * \brief single threaded descriptorring header
 *
 * v1.0 2017-12-04 Group C
 */

#ifndef INCLUDE_NET_INET_DESCRIPTORRING_H_
#define INCLUDE_NET_INET_DESCRIPTORRING_H_

#include <aos/aos.h>

struct inet_descriptorring {
	struct waitset_chanstate waitset_state;

	struct inet_descriptor_field *field;

	size_t size;
	bool is_sender;
	size_t send_idx;
	size_t recv_idx;
};

struct inet_descriptor_field {
	size_t data_len;
	void *data_buf;
	size_t meta_len;
	void *meta_buf;
	short flags;
};


void inet_descriptorring_init(struct inet_descriptorring *dr, void *buf, size_t len);

bool inet_descriptorring_can_send(struct inet_descriptorring *dr);
bool inet_descriptorring_can_recv(struct inet_descriptorring *dr);

bool inet_descriptorring_send(struct inet_descriptorring *dr, void *data_buf, size_t data_len, void *meta_buf, size_t meta_len);
bool inet_descriptorring_recv(struct inet_descriptorring *dr, void **data_buf, size_t *data_len, void **meta_buf, size_t *meta_len);

#endif /* INCLUDE_NET_INET_DESCRIPTORRING_H_ */