/**
 * \file
 * \brief network driver header
 *
 * v1.0 2017-12-09 Group C
 */

#ifndef INCLUDE_NET_NETWORK_DRIVER_H_
#define INCLUDE_NET_NETWORK_DRIVER_H_

#include <aos/aos.h>

errval_t network_driver_init(void);

#endif /* INCLUDE_NET_NETWORK_DRIVER_H_ */