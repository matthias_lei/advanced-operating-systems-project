/**
 * \file
 * \brief IP protocol header
 *
 * v1.0 2017-12-01 Group C
 */

#ifndef INCLUDE_NET_IP_PROT_H_
#define INCLUDE_NET_IP_PROT_H_

#include <aos/aos.h>
#include <netinet/in.h>
#include <netinet/ip.h>

errval_t ip_prot_init(void);

// ---- handler declaration for all protocols in the IPv4 protocol field ----
// ---- more protocol handlers can be added here                         ----

// receive handlers (must be implemented in the upper layers)
void ip_prot_icmp_packet_handler(struct ip *packet);
void ip_prot_udp_packet_handler(struct ip *packet);

// send functions
bool ip_prot_send_icmp_packet(void *packet_buf, size_t len, struct in_addr src_addr, struct in_addr dest_addr);
bool ip_prot_send_udp_packet(void *packet_buf, size_t len, struct in_addr src_addr, struct in_addr dest_addr);

#endif /* INCLUDE_NET_IP_PROT_H_ */
