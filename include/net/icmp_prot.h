/**
 * \file
 * \brief ICMP header
 *        ICMP Code fields as defined in:
 *          https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml#icmp-parameters-codes-0
 *        ICMP Type fields as defined in:
 *          https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml#icmp-parameters-types
 *         
 *
 * v1.0 2017-12-01 Group C
 */

#ifndef INCLUDE_NET_ICMP_PROT_H_
#define INCLUDE_NET_ICMP_PROT_H_

#include <aos/aos.h>
#include <net/ip_prot.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#define ICMP_TYPE_MAX 256

// --- add more ICMP types here ---
#define ICMP_ECHO_TYPE_REQ 8
#define ICMP_ECHO_TYPE_REP 0

// --- add more ICMP codes here ---
#define ICMP_ECHO_CODE_REQ 0
#define ICMP_ECHO_CODE_REP 0

// icmp header
struct icmp {
    u_char type;
    u_char code;
    u_short sum;
} __attribute__((packed));

struct icmp_echo {
    struct icmp icmp_header;
    u_short id;
    u_short seq;
    u_char payload[0];
} __attribute__((packed));

void icmp_prot_init(void);

#endif /* INCLUDE_NET_ICMP_PROT_H_ */
