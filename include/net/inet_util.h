/**
 * \file
 * \brief inet header file
 *
 * v1.0 2017-12-04 Group C
 */

#ifndef INCLUDE_NET_INET_UTIL_H_
#define INCLUDE_NET_INET_UTIL_H_

#include <aos/aos.h>

#include <net/inet_descriptorring.h>
#include <net/ip_prot.h>
#include <net/udp_impl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sys/socket.h>

#define SOCKET_ID_FIRST 4

// uncomment for no debug output of the network layer
// #define _INET_DEBUG

/**
 *  This structs is the solution to a very recurrent performance issue
 *  throughout the different network layers: memcpys.
 *  By just preallocating an array of pointers the number of memcopies can be minimized.
 */
struct inet_buffer_data {
    void *buf;
    size_t len;
};

u_short inet_rfc_checksum_buf_arr(struct inet_buffer_data **buf_arr, size_t len);

#endif /* INCLUDE_NET_INET_UTIL_H_ */