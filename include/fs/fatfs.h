/*
 * Implementation of the filesystem API for FAT
 *
 * Group C
 */

#ifndef INCLUDE_FS_FATFS_H_
#define INCLUDE_FS_FATFS_H_

#include <fs/fs.h>

typedef void *fat_handle_t;
typedef void *fat_mount_t;

errval_t fat_lookup(void *st, const char *path);

errval_t fat_open(void *st, const char *path, fat_handle_t *rethandle);

errval_t fat_create(void *st, const char *path, fat_handle_t *rethandle);

errval_t fat_remove(void *st, const char *path);

errval_t fat_read(void *st, fat_handle_t handle, void *buffer, size_t bytes,
                    size_t *bytes_read);

errval_t fat_write(void *st, fat_handle_t handle, const void *buffer,
                     size_t bytes, size_t *bytes_written);

errval_t fat_truncate(void *st, fat_handle_t handle, size_t bytes);

errval_t fat_tell(void *st, fat_handle_t handle, size_t *pos);

errval_t fat_stat(void *st, fat_handle_t inhandle, struct fs_fileinfo *info);

errval_t fat_seek(void *st, fat_handle_t handle, enum fs_seekpos whence,
                    off_t offset);

errval_t fat_close(void *st, fat_handle_t inhandle);

errval_t fat_opendir(void *st, const char *path, fat_handle_t *rethandle);

errval_t fat_dir_read_next(void *st, fat_handle_t inhandle, char **retname,
                             struct fs_fileinfo *info);

errval_t fat_closedir(void *st, fat_handle_t dhandle);

errval_t fat_mkdir(void *st, const char *path);

errval_t fat_rmdir(void *st, const char *path);

errval_t fat_mount(const char *uri, fat_mount_t *retst);

#endif // INCLUDE_FS_FATFS_H_
