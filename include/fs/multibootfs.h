/*
 * Implementation of the filesystem API for Multiboot
 *
 * Group C
 */

#ifndef INCLUDE_FS_MULTIBOOTFS_H_
#define INCLUDE_FS_MULTIBOOTFS_H_

#include <fs/fs.h>

typedef void *multiboot_handle_t;
typedef void *multiboot_mount_t;

errval_t multiboot_lookup(void *st, const char *path);

errval_t multiboot_open(void *st, const char *path, multiboot_handle_t *rethandle);

errval_t multiboot_create(void *st, const char *path, multiboot_handle_t *rethandle);

errval_t multiboot_remove(void *st, const char *path);

errval_t multiboot_read(void *st, multiboot_handle_t handle, void *buffer, size_t bytes,
                        size_t *bytes_read);

errval_t multiboot_write(void *st, multiboot_handle_t handle, const void *buffer,
                         size_t bytes, size_t *bytes_written);

errval_t multiboot_truncate(void *st, multiboot_handle_t handle, size_t bytes);

errval_t multiboot_tell(void *st, multiboot_handle_t handle, size_t *pos);

errval_t multiboot_stat(void *st, multiboot_handle_t inhandle, struct fs_fileinfo *info);

errval_t multiboot_seek(void *st, multiboot_handle_t handle, enum fs_seekpos whence,
                        off_t offset);

errval_t multiboot_close(void *st, multiboot_handle_t inhandle);

errval_t multiboot_opendir(void *st, const char *path, multiboot_handle_t *rethandle);

errval_t multiboot_dir_read_next(void *st, multiboot_handle_t inhandle, char **retname,
                                 struct fs_fileinfo *info);

errval_t multiboot_closedir(void *st, multiboot_handle_t dhandle);

errval_t multiboot_mkdir(void *st, const char *path);

errval_t multiboot_rmdir(void *st, const char *path);

errval_t multiboot_mount(const char *uri, multiboot_mount_t *retst);

#endif // INCLUDE_FS_MULTIBOOTFS_H_
