/*
 * Implementation of the filesystem API for VFS
 *
 * Group C
 */

#ifndef INCLUDE_FS_VFS_H_
#define INCLUDE_FS_VFS_H_

#include <fs/fs.h>

typedef void *vfs_handle_t;
typedef void *vfs_mount_t;

errval_t vfs_open(const char *path, vfs_handle_t *rethandle);

errval_t vfs_create(const char *path, vfs_handle_t *rethandle);

errval_t vfs_remove(const char *path);

errval_t vfs_read(vfs_handle_t handle, void *buffer, size_t bytes,
                    size_t *bytes_read);

errval_t vfs_write(vfs_handle_t handle, const void *buffer,
                     size_t bytes, size_t *bytes_written);

errval_t vfs_truncate(vfs_handle_t handle, size_t bytes);

errval_t vfs_tell(vfs_handle_t handle, size_t *pos);

errval_t vfs_stat(vfs_handle_t inhandle, struct fs_fileinfo *info);

errval_t vfs_seek(vfs_handle_t handle, enum fs_seekpos whence,
                    off_t offset);

errval_t vfs_close(vfs_handle_t inhandle);

errval_t vfs_opendir(const char *path, vfs_handle_t *rethandle);

errval_t vfs_dir_read_next(vfs_handle_t inhandle, char **retname,
                             struct fs_fileinfo *info);

errval_t vfs_closedir(vfs_handle_t dhandle);

errval_t vfs_mkdir(const char *path);

errval_t vfs_rmdir(const char *path);

errval_t vfs_mount(const char *path, const char *uri);

#endif // INCLUDE_FS_VFS_H_
