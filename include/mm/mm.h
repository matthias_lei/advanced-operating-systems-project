/**
 * \file
 * \brief Memory manager header
 *
 * modified; v1.0 2017-10-05 Group C
 */

/*
 * Copyright (c) 2008, 2011, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#ifndef AOS_MM_H
#define AOS_MM_H

#include <sys/cdefs.h>
#include <errors/errno.h>
#include <aos/types.h>
#include <aos/capabilities.h>
#include <aos/slab.h>
#include "slot_alloc.h"

__BEGIN_DECLS

#define MM_BUCKETS_SIZE 32

// this is based on the observation that offsets during cap_retype should be
// divisible by this size
#define MM_MIN_BUCKET_INDEX 11
#define MM_MIN_MEMORY_ALLOCATION (1 << (MM_MIN_BUCKET_INDEX))

#if MM_MIN_BUCKET_INDEX == 10
    #define MM_MAP_L1_MAX_ENTRIES 4096
    // needs 16 MiB for lookup table
#elif MM_MIN_BUCKET_INDEX == 11
    #define MM_MAP_L1_MAX_ENTRIES 2048
    // needs 8 MiB for lookup table
#elif MM_MIN_BUCKET_INDEX == 12
    #define MM_MAP_L1_MAX_ENTRIES 1024
    // needs 4 MiB for lookup table
#else
    #error Invalid MM_MIN_BUCKET_INDEX
#endif

#define MM_MAP_L2_MAX_ENTRIES 1024


// comment out this flag if additional debug info is wanted
// note: may lead to much output
//#define MM_OPTIONAL_DETAILED_DEBUG_INFO 1


enum nodetype {
    NodeType_Free,      ///< This region exists and is free
    NodeType_Allocated, ///< This region exists and is allocated
    NodeType_Parent     ///< This region was split up into subregions
};

/**
 * \brief Node in Memory manager
 */
struct mmnode {
    //TODO jmeier: find out optimal order of member for alignment
    // data part
    struct capref cap;  ///< Cap for this region
    genpaddr_t base;    ///< Base address of the region
    gensize_t size;     ///< Size of the region
    enum nodetype type; ///< Type of `this` node.

    struct mmnode *next;   ///< Next node in the list.
    struct mmnode *prev;   ///< Previous node in the list.

    struct mmnode *parent; ///< Parent node from which `this` node split away
};

struct mmnode_list_header {
    struct mmnode *head;
    size_t size;
};

struct capref_pair_list_elem {
    struct capref cap;
    struct capref_pair_list_elem *next;
};

struct free_slot_pair_list {
    struct capref single_reserve; // single free slot resulting from splitting a pair
    struct capref_pair_list_elem *head; // head of free slot pair list
    size_t size; // number of slot pairs contained in list starting at head
};

/**
 * \brief Memory manager instance data
 *
 * modified to support buckets
 *
 * This should be opaque from the perspective of the client, but to allow
 * them to allocate its memory, we declare it in the public header.
 */
struct mm {
    struct slab_allocator slabs; ///< Slab allocator used for allocating nodes
    slot_alloc_t slot_alloc;     ///< Slot allocator for allocating cspace
    slot_refill_t slot_refill;   ///< Slot allocator refill function
    void *slot_alloc_inst;       ///< Opaque instance pointer for slot allocator
    enum objtype objtype;        ///< Type of capabilities stored

    bool is_internal_alloc;      ///< Flag to detect recursive calls into mm_alloc_aligned

    struct mmnode_list_header buckets[MM_BUCKETS_SIZE]; ///< Array of free block lists

    struct mmnode* map_table[MM_MAP_L1_MAX_ENTRIES][MM_MAP_L2_MAX_ENTRIES]; ///< Table mapping physical memory addresses to mmnodes

    struct free_slot_pair_list free_slot_pair_list; ///< List of free slot pairs

    gensize_t managed_mem; ///< Stores the total number of bytes managed by this instance
};

errval_t mm_init(struct mm *mm, enum objtype objtype,
                     slab_refill_func_t slab_refill_func,
                     slot_alloc_t slot_alloc_func,
                     slot_refill_t slot_refill_func,
                     void *slot_alloc_inst);
errval_t mm_add(struct mm *mm, struct capref cap, genpaddr_t base, size_t size);
errval_t mm_alloc_aligned(struct mm *mm, size_t size, size_t alignment,
                              struct capref *retcap);
errval_t mm_alloc(struct mm *mm, size_t size, struct capref *retcap);

/** base and size are ignored as discussed with AOS support by mail */
errval_t mm_free(struct mm *mm, struct capref cap, genpaddr_t base /* ignored */, gensize_t size /* ignored */);

void mm_dump_mmnodes(struct mm *mm);

/** provokes NYI assert failure. not to implement as discussed with AOS support by mail */
void mm_destroy(struct mm *mm);

__END_DECLS

#endif /* AOS_MM_H */
