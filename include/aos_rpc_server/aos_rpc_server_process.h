/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * version 2017-10-24, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_PROCESS_
#define INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_PROCESS_

#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_process.h>

errval_t init_spawn_rpcs(void);

extern struct aos_rpc_interface_process_vtable aos_rpc_process_local_vtable;

#endif // INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_PROCESS_
