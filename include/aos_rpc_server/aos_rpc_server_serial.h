/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * version 2017-10-24, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_SERIAL_
#define INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_SERIAL_

#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_serial.h>

extern struct aos_rpc_interface_serial_vtable aos_rpc_serial_local_vtable;

#define N_LINES 40
#define N_COLUMNS 150
#define IN_BUF_LEN 30

struct window_data_list_header {
    struct window_data *head;
    size_t size;
};

enum window_write_state {
    NORMAL,
    EXPECT_ESC,
    ESC_STATE,
    READ_VALUE,
    VALUE_LIST
};

struct window_write_state_machine {
    enum window_write_state state;
    // up to 3 values possible in color escape sequence
    uint8_t index;
    uint8_t values[3];
};

struct character {
    bool color_valid;
    char c;
    uint8_t color[3];
};

struct window_data {

    domainid_t domain_id;
    char *domain_name;

    char in_buffer[IN_BUF_LEN];
    struct character out_buffer[N_LINES][N_COLUMNS + 1];

    // indices in out_buffer[][], are wrapped around via modulo
    uint32_t current_line;
    uint32_t current_column;

    // indices to in_buffer[], are wrapped around via modulo
    uint32_t in;
    uint32_t out;

    // marks this window as one that is blocking on getchar()
    bool pending;

    // counts how many lines in buffer are actually used, must be <= N_LINES
    uint32_t lines_used;

    // associated state, used to retrieve lmp channel and reregister server event handler at lmp chan
    struct aos_rpc_server_comm_state *state;

    // doubly-linked-list pointers
    struct window_data *prev;
    struct window_data *next;

    // tracks state of control sequences
    struct window_write_state_machine write_state_machine;

    // flag needed to handle colors
    bool next_char_has_color;
};

errval_t handle_input_characters(void);

#endif // INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_SERIAL_
