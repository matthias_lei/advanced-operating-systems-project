/**
 * version 2017-10-24, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_PROCESS_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_PROCESS_

#include <aos/rpc_shared/aos_rpc_shared_process.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>

void aos_rpc_processserver_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_PROCESS_
