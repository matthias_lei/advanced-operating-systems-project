/**
 * version 2017-11-23, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_INTERMON_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_INTERMON_

#include <aos/rpc_shared/aos_rpc_shared_intermon.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>

void aos_rpc_intermonserver_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_INTERMON_
