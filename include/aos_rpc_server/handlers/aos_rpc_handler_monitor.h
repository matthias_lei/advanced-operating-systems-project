/**
 * version 2017-12-03, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_MONITOR_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_MONITOR_

#include <aos/rpc_shared/aos_rpc_shared_monitor.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>

void aos_rpc_monitor_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_MONITOR_
