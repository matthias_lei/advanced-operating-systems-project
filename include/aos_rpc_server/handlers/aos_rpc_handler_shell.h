/**
 * version 2017-11-06, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_SHELL_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_SHELL_

#include <aos/rpc_shared/aos_rpc_shared_shell.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_shell.h>

void aos_rpc_shell_server_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_SHELL_
