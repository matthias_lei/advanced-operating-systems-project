/**
 * version 2017-11-06, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_NETWORK_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_NETWORK_

#include <aos/rpc_shared/aos_rpc_shared_network.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_network.h>

void aos_rpc_network_server_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_NETWORK_
