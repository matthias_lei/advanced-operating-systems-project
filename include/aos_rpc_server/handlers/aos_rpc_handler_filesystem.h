/**
 * version 2017-11-06, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_FILESYSTEM_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_FILESYSTEM_

#include <aos/rpc_shared/aos_rpc_shared_filesystem.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_filesystem.h>

void aos_rpc_filesystem_server_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_FILESYSTEM_
