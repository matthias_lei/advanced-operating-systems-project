/**
 * version 2017-10-24, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_COMMON_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_COMMON_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

void aos_rpc_common_event_handler(void *arg);

// this data structure is given as closure argument
struct aos_rpc_server_comm_state {
    struct aos_rpc_channel *chan;
    uintptr_t fid;              // function ID of the current communication round

    // points to the struct on the stack of the currently used event handler
    // holding its in and out buffers
    // note: these data sections are cleared before and after use
    struct aos_rpc_comm_context *context;

    // can be used to store additional data, e.g. used by serial server to store window data
    // note: in contrast to the context that is refreshed with every request,
    // the data here is persistent over all requests
    // the opaque pointer here allows using this field for various different implementation
    // specific types in different server types
    void *server_persistent_data;
};


// initialized with the first event handler that is used in a server
// but only with the first
extern waitlist_event_handler_t my_event_handler;

static inline void set_my_event_handler(waitlist_event_handler_t handler)
{
    if (!my_event_handler) {
        my_event_handler = handler;
    }
}

static inline waitlist_event_handler_t get_my_event_handler(void)
{
    return my_event_handler;
}

// needs to be public because it is called in the name server directly
errval_t aos_rpc_local_request_new_endpoint_part1(enum aos_rpc_chan_driver chan_type,
                                                  struct capref *ret_cap, void **ret_data);

void aos_rpc_local_request_new_endpoint_part2(enum aos_rpc_chan_driver chan_type,
                                              struct capref cap, void *data);

// initializes the state info
void aos_rpc_server_comm_state_init(struct aos_rpc_server_comm_state *state, struct aos_rpc_channel *chan);

//--- serialized RPC request handling API ---------------------------------------------------

struct aos_rpc_request_queue {
    struct waitset ws;          // serialization queue for RPC requests
    bool is_handling_sync_req;  // used to signal that some event handler is already handling
                                // a synchronous request for this channel
};

extern struct aos_rpc_request_queue *my_request_queue;

void aos_rpc_request_queue_init(struct aos_rpc_request_queue *queue);

static inline void aos_rpc_set_my_request_queue(struct aos_rpc_request_queue *queue) {
    my_request_queue = queue;
}

/**
 * Helper function to implement a serializing RPC request handler.
 *
 * It should be called directly from the asynchronous event handler,
 * which receives the aos_rpc_server_comm_state.
 *
 * \param state         The state passed to the asynchronous event handler.
 * \param handler_sync  Function pointer to the event handler that must not
 *                      be called recursively. The aos_rpc_generic_handler_async
 *                      function serializes all RPC requests and forwards them
 *                      one-by-one to the handler_sync function. While one request
 *                      is handled by handler_sync, all incoming new requests are
 *                      queued up.
 */
void aos_rpc_generic_handler_async(struct aos_rpc_server_comm_state *state, aos_rpc_fg_event_handler_t handler_sync);


#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_COMMON_
