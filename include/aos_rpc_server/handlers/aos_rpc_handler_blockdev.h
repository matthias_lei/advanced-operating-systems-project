/**
 * version 2017-12-09, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_BLOCKDEV_
#define INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_BLOCKDEV_

#include <aos/rpc_shared/aos_rpc_shared_blockdev.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>

void aos_rpc_blockdev_server_event_handler(void *arg);

#endif // INCLUDE_AOS_RPC_SERVER_HANDLERS_AOS_RPC_HANDLER_BLOCKDEV_
