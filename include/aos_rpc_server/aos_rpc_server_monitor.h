/**
 * Defines the counterpart to the aos_rpc.h defined RPC stubs.
 *
 * These event handlers can be used in the associated processes (e.g. init)
 * to handle received RPCs.
 *
 * The naming convention of function groups and function ids are used as defined
 * in aos/aos_rpc_function_ids.h
 *
 * note: function groups allow easy dropping of false calls for specialized event handlers,
 * such as memory or process event handlers.
 *
 * version 2017-12-03, Group C
 */

#ifndef INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_MONITOR_
#define INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_MONITOR_

#include <aos_rpc_server/handlers/aos_rpc_handler_common.h>
#include <aos_rpc_server/handlers/aos_rpc_handler_monitor.h>

extern volatile bool bootstrap_memory_ready;
extern struct capref bootstrap_memory_service_cap;

extern volatile bool bootstrap_name_ready;
extern struct capref bootstrap_name_service_cap;

extern volatile bool bootstrap_process_ready;

extern struct aos_rpc_interface_monitor_vtable aos_rpc_monitor_local_vtable;

// for intermon services
extern size_t monitor_shell_rpcs_max;
extern struct aos_rpc *monitor_shell_rpcs[];
extern struct aos_rpc *monitor_service_rpcs[];

#endif // INCLUDE_AOS_RPC_SERVER_AOS_RPC_SERVER_MONITOR_
