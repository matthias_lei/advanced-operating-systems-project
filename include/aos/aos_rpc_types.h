/**
 * prevents some circular declarations / redefinitions
 *
 * AOS Group C
 */

#ifndef INCLUDE_AOS_AOS_RPC_TYPES_H_
#define INCLUDE_AOS_AOS_RPC_TYPES_H_

#include <barrelfish_kpi/types.h>

#include <aos/aos_rpc_function_ids.h>

// note: the virtual AOS_RPC_INTERFACE_GLOBAL interface covers all interfaces
enum aos_rpc_interface {
    AOS_RPC_INTERFACE_COMMON = 0,
    AOS_RPC_INTERFACE_MONITOR,
    AOS_RPC_INTERFACE_INTERMON,
    AOS_RPC_INTERFACE_NAME,
    AOS_RPC_INTERFACE_MEMORY,
    AOS_RPC_INTERFACE_PROCESS,
    AOS_RPC_INTERFACE_SERIAL,
    AOS_RPC_INTERFACE_DEVICE,
    AOS_RPC_INTERFACE_FILESYSTEM,
    AOS_RPC_INTERFACE_NETWORK,
    AOS_RPC_INTERFACE_BLOCKDEV,
    AOS_RPC_INTERFACE_SHELL
};

STATIC_ASSERT(AOS_RPC_INTERFACE_MONITOR == AOS_RPC_MONITOR_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_INTERMON == AOS_RPC_INTERMON_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_NAME == AOS_RPC_NAME_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_MEMORY == AOS_RPC_MEMORY_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_PROCESS == AOS_RPC_PROCESS_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_SERIAL == AOS_RPC_SERIAL_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_DEVICE == AOS_RPC_DEVICE_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_FILESYSTEM == AOS_RPC_FILESYSTEM_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_NETWORK == AOS_RPC_NETWORK_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_BLOCKDEV == AOS_RPC_BLOCKDEV_FG, "");
STATIC_ASSERT(AOS_RPC_INTERFACE_SHELL == AOS_RPC_SHELL_FG, "");

enum aos_rpc_intermon_capability_type {
    AOS_RPC_INTERMON_CAPABILITY_NONE, /* quick signal that no capability is embedded */
    AOS_RPC_INTERMON_CAPABILITY_RAM,
    AOS_RPC_INTERMON_CAPABILITY_FRAME,
    AOS_RPC_INTERMON_CAPABILITY_DEVFRAME,
    AOS_RPC_INTERMON_CAPABILITY_IRQ
};

struct aos_rpc_channel;

// pre-define them here to avoid the need to include nameserver_types.h already for
// this header file (would lead to circular dependency)
typedef uint64_t service_handle_t;
typedef uint32_t routing_info_t;

struct aos_rpc {
    struct aos_rpc_channel *chan;
    enum aos_rpc_interface interface;
    void *interface_vtable;
    size_t enumerator;      // used for intermon for services that allow multiple instances to choose from
    domainid_t domain_id;   // used for serial
    routing_info_t routing; // pisch: used for intermon routing of capability transfers
};

typedef void (*waitlist_event_handler_t)(void *);

// pre-defined here
struct aos_rpc_server_comm_state;

typedef void (*aos_rpc_fg_event_handler_t)(struct aos_rpc_server_comm_state *);

#endif // INCLUDE_AOS_AOS_RPC_TYPES_H_
