#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_BLOCKDEV_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_BLOCKDEV_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct aos_rpc_interface_blockdev_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*get_block_size)(struct aos_rpc *rpc, size_t *block_size);
    errval_t (*read_block)(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen);
    errval_t (*write_block)(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen);
};

struct aos_rpc_read_block_req_payload {
    size_t block_nr;
    size_t buflen;
};

struct aos_rpc_write_block_req_payload {
    size_t block_nr;
    size_t buflen;
    char buf[0];
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_BLOCKDEV_
