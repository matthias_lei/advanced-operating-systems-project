#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_SERIAL_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_SERIAL_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct aos_rpc_interface_serial_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*get_char)(struct aos_rpc *rpc, char *retc);
    errval_t (*put_char)(struct aos_rpc *rpc, char c);
    errval_t (*put_string)(struct aos_rpc *rpc, const char *s, size_t len);
    errval_t (*get_window)(struct aos_rpc *rpc, domainid_t window_id);
};

struct aos_rpc_serial_common_req_payload {
    domainid_t domain_id;
};

struct aos_rpc_serial_getchar_req_payload {
    domainid_t domain_id;
};

struct aos_rpc_serial_getchar_reply_payload {
    char c;
};

struct aos_rpc_serial_putchar_req_payload {
    domainid_t domain_id;
    char c;
};

struct aos_rpc_serial_putstring_req_payload {
    domainid_t domain_id;
    size_t len;
    char s[0];
};

struct aos_rpc_serial_get_window_req_payload {
    domainid_t domain_id;
    domainid_t window_id;
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_SERIAL_
