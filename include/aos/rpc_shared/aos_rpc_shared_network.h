#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_NETWORK_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_NETWORK_

#include <aos/rpc_shared/aos_rpc_shared_common.h>
#include <sys/socket.h>

struct aos_rpc_interface_network_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*accept)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
    errval_t (*bind)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
    errval_t (*connect)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
    errval_t (*getpeername)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
    errval_t (*getsockname)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
    errval_t (*getsockopt)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len);
    errval_t (*listen)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int backlog);
    errval_t (*recv)(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags);
    errval_t (*recvfrom)(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
    errval_t (*recvmsg)(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags);
    errval_t (*send)(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags);
    errval_t (*sendmsg)(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags);
    errval_t (*sendto)(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
    errval_t (*setsockopt)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len);
    errval_t (*shutdown)(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int how);
    errval_t (*socket)(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol);
    errval_t (*socketpair)(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]);
};

struct aos_rpc_network_accept_req_payload {
    int socket;
};

struct aos_rpc_network_accept_res_payload {
    int err_no;
    int ret_value;
    struct sockaddr addr;
    socklen_t addr_len;
};

struct aos_rpc_network_bind_req_payload {
    int socket;
    struct sockaddr addr;
    socklen_t addr_len;
};

struct aos_rpc_network_bind_res_payload {
    int err_no;
    int ret_value;
};

struct aos_rpc_network_connect_req_payload {
    int socket;
    struct sockaddr addr;
    socklen_t addr_len; 
};

struct aos_rpc_network_connect_res_payload {
    int err_no;
    int ret_value;
};

struct aos_rpc_network_getpeername_req_payload {
    int socket;
};

struct aos_rpc_network_getpeername_res_payload {
    int err_no;
    int ret_value;
    struct sockaddr addr;
    socklen_t addr_len;
};

struct aos_rpc_network_getsockname_req_payload {
    int socket;
};

struct aos_rpc_network_getsockname_res_payload {
    int err_no;
    int ret_value;
    struct sockaddr addr;
    socklen_t addr_len; 
};

struct aos_rpc_network_getsockopt_req_payload {
    int socket;
    int level;
    int option_name;
    socklen_t option_len;
};

struct aos_rpc_network_getsockopt_res_payload {
    int err_no;
    int ret_value;
    socklen_t option_len;
    char option_value[0];
};

struct aos_rpc_network_listen_req_payload {
    int socket;
    int backlog;
};

struct aos_rpc_network_listen_res_payload {
    int err_no;
    int ret_value;
};

struct aos_rpc_network_recv_req_payload {
    int socket;
    size_t length;
    int flags;
};

struct aos_rpc_network_recv_res_payload {
    int err_no;
    ssize_t ret_size;
    char buffer[0];
};

struct aos_rpc_network_recvfrom_req_payload {
    int socket;
    size_t length;
    int flags;
};

struct aos_rpc_network_recvfrom_res_payload {
    int err_no;
    ssize_t ret_size;
    struct sockaddr addr;
    socklen_t addr_len;
    char buffer[0];
};

struct aos_rpc_network_recvmsg_req_payload {
    int socket;
    size_t length;
    int flags;
};

struct aos_rpc_network_recvmsg_res_payload {
    int err_no;
    ssize_t ret_size;
    size_t length;
    char message[0];
};

struct aos_rpc_network_send_req_payload {
    int socket;
    int flags;
    size_t length;
    char message[0];
};

struct aos_rpc_network_send_res_payload {
    int err_no;
    ssize_t ret_size;
};

struct aos_rpc_network_sendmsg_req_payload {
    int socket;
    int flags;
    size_t length;
    char message[0];
};

struct aos_rpc_network_sendmsg_res_payload {
    int err_no;
    ssize_t ret_size;
};

struct aos_rpc_network_sendto_req_payload {
    int socket;
    int flags;
    struct sockaddr dest_addr;
    socklen_t dest_len;
    size_t length;
    char message[0];
};

struct aos_rpc_network_sendto_res_payload {
    int err_no;
    ssize_t ret_size;
};

struct aos_rpc_network_setsockopt_req_payload {
    int socket;
    int option_name;
    socklen_t option_len;
    char option_value[0];
};

struct aos_rpc_network_setsockopt_res_payload {
    int err_no;
    int ret_value;
};

struct aos_rpc_network_shutdown_req_payload {
    int socket;
    int how;
};

struct aos_rpc_network_shutdown_res_payload {
    int err_no;
    int ret_value;
};

struct aos_rpc_network_socket_req_payload {
    int domain;
    int type;
    int protocol;
};

struct aos_rpc_network_socket_res_payload {
    int err_no;
    int ret_value;
};

struct aos_rpc_network_socketpair_req_payload {
    int domain;
    int type;
    int protocol;
    int socket_vector[2];
};

struct aos_rpc_network_socketpair_res_payload {
    int err_no;
    int ret_value;
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_NETWORK_
