/**
 * AOS 2017 Group C: individual project Nameserver
 * version 2017-11-30, pisch
 */

#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_NAME_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_NAME_

#include <aos/rpc_shared/aos_rpc_shared_common.h>
#include <hashtable/serializable_key_value_store.h>
#include <nameserver_types.h>

struct aos_rpc_interface_name_vtable {
    struct aos_rpc_interface_common_vtable common;
    //--- services
    errval_t (*service_register) (struct aos_rpc *rpc, const struct service_registration_request *request,
                                  struct capref contact_cap,
                                  struct service_registration_reply **ret_reply);
    errval_t (*service_deregister) (struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);
    errval_t (*service_ping) (struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);
    errval_t (*service_update) (struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store);

    //--- clients
    errval_t (*lookup_service_by_path) (struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t *ret_handle);
    errval_t (*lookup_service_by_short_name) (struct aos_rpc *rpc, const char *short_name, service_handle_t *ret_handle);
    errval_t (*find_any_service_by_path) (struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count);
    errval_t (*find_any_service_by_filter) (struct aos_rpc *rpc, const struct serializable_key_value_store *filter,
                                            service_handle_t **ret_handles, size_t *ret_count);
    errval_t (*get_service_info) (struct aos_rpc *rpc, service_handle_t handle, struct serializable_key_value_store **ret_info);
    errval_t (*bind_service_lowlevel) (struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id,
                                       routing_info_t *ret_routing_info,
                                       enum aos_rpc_interface *ret_interface,
                                       size_t *ret_enumerator,
                                       enum aos_rpc_chan_driver *ret_chan_type,
                                       struct capref *ret_service_cap);
};


//--- service_register
struct aos_rpc_name_service_register_req_payload {
    coreid_t core_id;
    enum aos_rpc_interface interface;
    enum aos_rpc_chan_driver contact_chan_type;
    bool high_bandwidth_service;

    size_t buf_size;
    size_t abs_path_offset;
    size_t short_name_offset;
    struct serialized_key_value_store store[0];
    // and then the 2 strings follow after that
    // as in the original structure, capref is transported otherwise
};

// note: the __attribute__((unused)) are added here since typically only one pair of
// serialize / deserialize functions are used in clients in regular clients/servers
// but spatial proximity to the payload definitions here helps keeping them consistent
// thus: I prefer such a flag instead moving them to separate files.

/**
 * memory: the returned buffer is allocated by malloc() and needs to be freed by the client.
 * (ownership transfer)
 */
__attribute__((unused))
static struct aos_rpc_name_service_register_req_payload *service_registration_request_serialize(const struct service_registration_request *request)
{
    assert(request);

    struct serialized_key_value_store *store = serialize_kv_store(request->kv_store);
    if (!store) {
        goto error_exit;
    }

    size_t path_buf_len = strlen(request->abs_path) + 1;
    size_t short_name_buf_len = strlen(request->short_name_prefix) + 1;
    size_t size = sizeof(struct aos_rpc_name_service_register_req_payload);
    size += store->buf_size + path_buf_len + short_name_buf_len;
    struct aos_rpc_name_service_register_req_payload *ret = calloc(1, size);
    if (!ret) {
        goto unroll_serialized_store;
    }

    ret->buf_size = size;
    ret->core_id = request->core_id;
    ret->interface = request->interface;
    ret->contact_chan_type = request->contact_chan_type;
    ret->high_bandwidth_service = request->high_bandwidth_service;
    memcpy(ret->store, store, store->buf_size);

    ret->abs_path_offset = sizeof(struct aos_rpc_name_service_register_req_payload) + store->buf_size;
    ret->short_name_offset = ret->abs_path_offset + path_buf_len;

    strcpy((char *)ret + ret->abs_path_offset, request->abs_path);
    strcpy((char *)ret + ret->short_name_offset, request->short_name_prefix);

    free(store);
    return ret;

unroll_serialized_store:
    free(store);
error_exit:
    return NULL;
}

/**
 * memory: the returned buffer and embedded kv_store are allocated by malloc() and need
 * to be freed by the client (ownership transfer).
 * The strings are already part of this buffer and do not need to be freed separately.
 */
__attribute__((unused))
static struct service_registration_request *service_registration_request_deserialize(const struct aos_rpc_name_service_register_req_payload *serialized_request)
{
    assert(serialized_request);

    struct service_registration_request *ret = malloc(serialized_request->buf_size);
    if (!ret) {
        return NULL;
    }

    // fills the non-pointer fields and prepares the string buffers
    memcpy(ret, serialized_request, serialized_request->buf_size);

    ret->abs_path = (char *)serialized_request + serialized_request->abs_path_offset;
    ret->short_name_prefix = (char *)serialized_request + serialized_request->short_name_offset;
    ret->kv_store = deserialize_kv_store(serialized_request->store, 0);
    if (!ret->kv_store) {
        free(ret);
        return NULL;
    }

    return ret;
}


struct aos_rpc_name_service_register_res_payload {
    service_handle_t handle;
    uint64_t registration_key;
    size_t enumeration;

    size_t buf_size;
    size_t name_offset;
    size_t short_name_offset;
};

/**
 * memory: the returned buffer is allocated by malloc() and needs to be freed by the client.
 * (ownership transfer)
 */
__attribute__((unused))
static struct aos_rpc_name_service_register_res_payload *service_registration_reply_serialize(const struct service_registration_reply *reply)
{
    assert(reply);

    size_t name_buf_len = strlen(reply->name) + 1;
    size_t sname_buf_len = strlen(reply->short_name) + 1;
    size_t size = name_buf_len + sname_buf_len + sizeof(struct aos_rpc_name_service_register_res_payload);
    struct aos_rpc_name_service_register_res_payload *ret = calloc(1, size);
    if (!ret) {
        return NULL;
    }

    ret->buf_size = size;
    ret->handle = reply->handle;
    ret->registration_key = reply->registration_key;
    ret->enumeration = reply->enumeration;
    ret->name_offset = sizeof(struct aos_rpc_name_service_register_res_payload);
    ret->short_name_offset = ret->name_offset + name_buf_len;

    strcpy((char *)ret + ret->name_offset, reply->name);
    strcpy((char *)ret + ret->short_name_offset, reply->short_name);
    return ret;
}

/**
 * memory: the returned buffer is allocated by malloc() and needs to be freed by the client
 * (ownership transfer). The strings are already part of this buffer and do not need
 * to be freed separately.
 */
__attribute__((unused))
static struct service_registration_reply *service_registration_reply_deserialize(const struct aos_rpc_name_service_register_res_payload *serialized_reply)
{
    assert(serialized_reply);

    struct service_registration_reply *ret = malloc(serialized_reply->buf_size);
    if (!ret) {
        return NULL;
    }

    // fills the non-pointer fields and prepares the string buffers
    memcpy(ret, serialized_reply, serialized_reply->buf_size);

    ret->name = (char *)serialized_reply + serialized_reply->name_offset;
    ret->short_name = (char *)serialized_reply + serialized_reply->short_name_offset;

    return ret;
}


//--- deregister / ping
struct aos_rpc_name_handle_key_req_payload {
    service_handle_t handle;
    uint64_t registration_key;
};

//--- update
struct aos_rpc_name_update_req_payload {
    service_handle_t handle;
    uint64_t registration_key;
    struct serialized_key_value_store serialized_store[0];
};

//--- bind_service_lowlevel
struct aos_rpc_name_bind_service_lowlevel_req_payload {
    service_handle_t handle;
    coreid_t core_id;
};

struct aos_rpc_name_bind_service_lowlevel_res_payload {
    routing_info_t routing_info;
    enum aos_rpc_interface interface;
    size_t enumerator;
    enum aos_rpc_chan_driver chan_type;
    // the capref is transported separately
};


//--- lookup/find
struct aos_rpc_name_find_res_payload {
    size_t count;
    service_handle_t handles[0];
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_NAME_
