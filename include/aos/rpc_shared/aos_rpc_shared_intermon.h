#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_INTERMON_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_INTERMON_

#include <aos/rpc_shared/aos_rpc_shared_common.h>
#include <aos/aos_rpc_types.h>


// aos_intermon_bootstrap_req header only request from core 1 to core 0; no payload
struct aos_intermon_bootstrap_res {
    genpaddr_t bootinfo_base;
    gensize_t bootinfo_size;
    genpaddr_t name_base;
    gensize_t name_size;
    gensize_t mmstrings_base;
    gensize_t mmstrings_size;
};

struct aos_rpc_intermon_spawn_req_payload {
    domainid_t pid;
    int shell_id;
    char name[0];
};


struct aos_rpc_intermon_forward_req_payload {
    enum aos_rpc_interface interface; // needed for the routing on the target monitor
                                      // see: all services offer common FG 0
    uint32_t fid;                     // original FG and FID
    routing_info_t routing;
    size_t enumerator; // used for services that allow multiple instances to choose from

    // keeping these info here simplifies management in the intermon server
    enum aos_rpc_intermon_capability_type send_cap_type;
    enum aos_rpc_intermon_capability_type recv_cap_type;
    // capability type is in the envelope
    genpaddr_t cap_base;
    gensize_t cap_size;

    // envelope
    size_t env_size;
    char env[0]; // basically a serialized envelope
};

struct aos_rpc_intermon_forward_res_payload {
    // capability type is in the envelope
    genpaddr_t cap_base;
    gensize_t cap_size;
    size_t env_size;
    char env[0]; // basically a serialized envelope
};

struct aos_rpc_interface_intermon_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*request_bootstrap_info)(struct aos_rpc *rpc, coreid_t core_id, struct aos_intermon_bootstrap_res *info);
    errval_t (*forward_rpc_message) (struct aos_rpc *rpc, struct aos_rpc_intermon_forward_req_payload *send,
                                     struct aos_rpc_intermon_forward_res_payload *recv);

};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_INTERMON_
