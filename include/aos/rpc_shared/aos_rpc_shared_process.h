#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_PROCESS_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_PROCESS_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct aos_rpc_interface_process_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*spawn)(struct aos_rpc *rpc, char *name, coreid_t core_id, domainid_t *newpid);
    errval_t (*get_name)(struct aos_rpc *rpc, domainid_t pid, char **name);
    errval_t (*get_all_pids)(struct aos_rpc *rpc, domainid_t **pids, size_t *pid_count);
    errval_t (*spawn_from_file)(struct aos_rpc *rpc, const char* path, const char *command_line, coreid_t core_id, int shell_id, domainid_t *newpid);
    errval_t (*spawn_from_shell)(struct aos_rpc *rpc, char* name, coreid_t core_id, int shell_id, domainid_t *newpid);
};

struct aos_rpc_process_spawn_req_payload {
    coreid_t core_id;
    char name[0];
};

struct aos_rpc_process_get_all_pids_res_payload {
    size_t size;
    domainid_t domain_ids[0];
};

struct aos_rpc_process_spawn_from_file_req_payload {
    coreid_t core_id;
    int shell_id;
    char data[0];
};

struct aos_rpc_process_spawn_from_shell_req_payload {
    coreid_t core_id;
    int shell_id;
    char name[0];
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_PROCESS_
