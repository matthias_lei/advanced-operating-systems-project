#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_SHELL_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_SHELL_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct aos_rpc_interface_shell_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*request_params)(struct aos_rpc *rpc, struct shell_params **ret_params);
    errval_t (*notify_termination)(struct aos_rpc *rpc, domainid_t pid);
};

struct shell_params {
    bool background;
    domainid_t shell_pid;
    // [path of stdin] '\0' [path of stdout] '\0'
    // if stdin/stdout is not redirected, its path will be empty
    char io_redirection_paths[0];
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_SHELL_
