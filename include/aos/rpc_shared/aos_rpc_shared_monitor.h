#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_MONITOR_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_MONITOR_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

#define MONITOR_BOOTSTRAP_MAX_PIDS 8
#define MONITOR_BOOTSTRAP_MAX_NAME_LEN 20
struct aos_rpc_get_bootstrap_pids_res_payload {
    size_t count;
    coreid_t cores[MONITOR_BOOTSTRAP_MAX_PIDS];
    char names[MONITOR_BOOTSTRAP_MAX_PIDS][MONITOR_BOOTSTRAP_MAX_NAME_LEN];
};

struct aos_rpc_interface_monitor_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*request_rpc_forward) (struct aos_rpc *rpc, enum aos_rpc_interface interface,
                                     size_t enumerator, uint32_t fid, routing_info_t routing,
                                     struct aos_rpc_send_msg_env *send_env,
                                     struct aos_rpc_recv_msg_env *recv_env);
    errval_t (*spawn_domain) (struct aos_rpc *rpc, const char *name, coreid_t core_id, int shell_id, domainid_t pid);
    errval_t (*spawn_domain_from_file) (struct aos_rpc *rpc, const char *path, const char *command_line, coreid_t core_id, int shell_id, domainid_t pid);
    errval_t (*bootstrap_offer_service) (struct aos_rpc *rpc, enum aos_rpc_interface interface, struct capref service_cap);
    errval_t (*offer_service) (struct aos_rpc *rpc, enum aos_rpc_interface interface, size_t enumerator,
                               enum aos_rpc_chan_driver chan_type, struct capref service_cap);
    errval_t (*get_bootstrap_pids) (struct aos_rpc *rpc, struct aos_rpc_get_bootstrap_pids_res_payload *res);
    errval_t (*get_multiboot_module_names) (struct aos_rpc *rpc, char **name_list, size_t *list_len);
    errval_t (*get_multiboot_module) (struct aos_rpc *rpc, const char *name, struct capref *frame);
};


struct aos_rpc_spawn_domain_req_payload {
    coreid_t core_id;
    domainid_t pid;
    int shell_id;
    char name[0];
};

struct aos_rpc_spawn_domain_from_file_req_payload {
    coreid_t core_id;
    domainid_t pid;
    int shell_id;
    char data[0];
};

struct aos_rpc_offer_service_req_payload {
    enum aos_rpc_interface interface;
    size_t enumerator;
    enum aos_rpc_chan_driver chan_type;
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_MONITOR_
