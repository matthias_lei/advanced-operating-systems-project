/**
 *  The memory management of the receive buffer in aos_rpc_generic_recv works currently
 *  as follows:
 *  1. If a receive buffer is passed in aos_rpc_comm_context_client_init the buffer is used for
 *     storing the received message.
 *  2. i. If no receive buffer is passed in aos_rpc_comm_context_client_init and the receive
 *     buffer length is set to AOS_RPC_EXPECT_PAYLOAD then aos_rpc_generic_recv will allocate
 *     memory for the incoming message via malloc.
 *     It is then the callers responsibility to call free once the buffer is not used anymore
 *     ii. If no receive buffer is passed and the receive buffer length is set to AOS_RPC_EXPECT_NO_PAYLOAD
 *     then no malloc must be used as no payload (except header und received message length) is expected.
 *     However if the message has a payload even though none is expected an error will be thrown.
 *
 *
 * Group C
 * version 2017-12-02
 */

#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_COMMON_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_COMMON_

#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_function_ids.h>
#include <aos/aos_rpc_types.h>

extern struct aos_rpc_interface_common_vtable aos_rpc_common_remote_vtable;
extern struct aos_rpc_interface_monitor_vtable aos_rpc_monitor_remote_vtable;
extern struct aos_rpc_interface_intermon_vtable aos_rpc_intermon_remote_vtable;
extern struct aos_rpc_interface_name_vtable aos_rpc_name_remote_vtable;
extern struct aos_rpc_interface_memory_vtable aos_rpc_memory_remote_vtable;
extern struct aos_rpc_interface_process_vtable aos_rpc_process_remote_vtable;
extern struct aos_rpc_interface_serial_vtable aos_rpc_serial_remote_vtable;
extern struct aos_rpc_interface_device_vtable aos_rpc_device_remote_vtable;
extern struct aos_rpc_interface_filesystem_vtable aos_rpc_filesystem_remote_vtable;
extern struct aos_rpc_interface_network_vtable aos_rpc_network_remote_vtable;
extern struct aos_rpc_interface_blockdev_vtable aos_rpc_blockdev_remote_vtable;
extern struct aos_rpc_interface_shell_vtable aos_rpc_shell_remote_vtable;

extern void *aos_rpc_interface_vtables[];

// used for allocations where no malloc() is allowed during RPC
// -> forwarding of messages: there is a fixed max size of messages that can be
//    re-routed through the monitors. However, that is not a problem. All messages
//    with caps are quite short.
//
// Also used on server side to avoid malloc()/free() during receive of short messages
// in general. This was initially needed for the critical paths during forward
// and RAM allocations, but also works/helps in general.
#define DEFAULT_RPC_BUFFER_SIZE 512

// structure for keeping the state of a message send operation
struct aos_rpc_send_fragment_state {
    bool completed;                                 //< indicates whether sending has completed
    errval_t err;                                   //< stores any error that happened during sending
    size_t offset;                                  //< offset of the already processed msg buffer WITHOUT header
    struct aos_rpc_channel *chan;                   //< the channel we are sending over
    const struct aos_rpc_send_msg_env *send_env;    //< the message envelope to send
    struct event_closure cont;                      //< a continuation that is called after successfully sending or in case of an error
};

// structure for keeping the state of a message receive operation
struct aos_rpc_recv_fragment_state {
    bool completed;                         //< indicates whether receiving has completed
    errval_t err;                           //< stores any error that happened during receiving
    size_t offset;                          //< offset of the already processed msg buffer WITHOUT header
    struct aos_rpc_channel *chan;           //< the channel we are receiving from
    struct aos_rpc_recv_msg_env *recv_env;  //< the message envelope we receive
    void *server_default_recv_buffer;       //< default buffer used on the server side to avoid mallocs in critical paths
    bool is_server;                         //< server flag (used in conjunction with the server_default_recv_buffer)
    struct event_closure cont;              //< a continuation that is called after successfully receiving or in case of an error
};

// structure for keeping the state of a communication round (send with associated receive)
// also needed in intermon server
struct aos_rpc_client_comm_state {
    struct aos_rpc_client_comm_state *prev;         //< previous state in the message queue
    struct aos_rpc_client_comm_state *next;         //< next state in the message queue
    struct aos_rpc_channel *chan;                   //< the channel over which this communication round happens
    errval_t err;                                   //< stores any error that happend during the communication round
    bool complete;                                  //< indicates whether the communication round has completed
    struct aos_rpc_comm_context *context;           //< context of the communication round (send and receive envelopes)
    struct waitset_chanstate ws_chan;               //< used to enqueue the communication round onto the channel
    struct capref recv_cap;                         //< cap slot to be installed on the channel in case we expect to receive a capability
    struct aos_rpc_send_fragment_state send_state;  //< state of the send operation in this communication round
    struct aos_rpc_recv_fragment_state recv_state;  //< state of the receive operation in this communication round
};

struct aos_rpc_interface_common_vtable {
    errval_t (*send_number) (struct aos_rpc *rpc, uintptr_t val);
    errval_t (*send_string) (struct aos_rpc *rpc, const char *string);
    errval_t (*connect_with_service) (struct capref service_cap, routing_info_t routing_info,
                                      enum aos_rpc_interface interface, enum aos_rpc_chan_driver chan_type,
                                      struct aos_rpc **ret_rpc, enum rpc_services_mode *ret_mode);
    errval_t (*request_new_endpoint) (struct aos_rpc *rpc, enum aos_rpc_chan_driver chan_type,
                                      struct capref *ret_cap);
    errval_t (*benchmarking) (struct aos_rpc *rpc, size_t buflen, size_t exp_ret_buflen);
};

enum aos_rpc_chan_driver {
    AOS_RPC_CHAN_DRIVER_NONE_BOOTSTRAP,     /* for bootstrap only */
    AOS_RPC_CHAN_DRIVER_NONE_LATE_BINDING,  /* for spawn: see memory service */
    AOS_RPC_CHAN_DRIVER_LMP,
    AOS_RPC_CHAN_DRIVER_FLMP,
    AOS_RPC_CHAN_DRIVER_UMP
};

//--- channel queue

struct aos_rpc_channel_queue {
    size_t size;
    struct aos_rpc_client_comm_state *head;
    struct aos_rpc_client_comm_state *tail;
};

static inline void aos_rpc_channel_queue_init(struct aos_rpc_channel_queue *queue) {
    queue->size = 0;
    queue->head = NULL;
    queue->tail = NULL;
}

static inline struct aos_rpc_client_comm_state* aos_rpc_channel_queue_front(struct aos_rpc_channel_queue *queue) {
    return queue->head;
}

static inline void aos_rpc_channel_queue_enqueue(struct aos_rpc_channel_queue *queue, struct aos_rpc_client_comm_state *state) {
    assert(queue);
    assert(state);
    assert(!state->next);
    assert(!state->prev);

    state->prev = queue->tail;
    state->next = NULL;
    queue->tail = state;
    if (state->prev) {
        assert(queue->size);
        state->prev->next = state;
    }
    else {
        assert(!queue->size);
        queue->head = state;
    }
    queue->size++;
}

static inline void aos_rpc_channel_queue_dequeue(struct aos_rpc_channel_queue *queue) {
    assert(queue);
    assert(queue->size);

    struct aos_rpc_client_comm_state *rem = queue->head;
    queue->head = queue->head->next;
    queue->size--;
    if (queue->head) {
        assert(queue->size);
        queue->head->prev = NULL;
    }
    else {
        assert(!queue->size);
        queue->tail = NULL;
    }
    rem->next = NULL;
    rem->prev = NULL;
}

//--- channel

struct aos_rpc_channel {
    union {
        struct lmp_chan *lmp_chan;
        struct flmp_chan *flmp_chan;
        struct ump_chan *ump_chan;
    };
    enum aos_rpc_chan_driver driver;
    struct aos_rpc_channel_driver_vtable *vtable;
    struct thread_mutex mutex;  // lock for threadsafe usage of the channel
                                // NOTE jmeier: we only need to lock the channel on the
                                //              RPC client side, all our servers are single
                                //              threaded and therefore do not need locking
                                //       pisch: we need to remember this, in case
                                // we change this on server side (might happen)
    struct aos_rpc_channel_queue queue;
};

struct aos_rpc_channel_driver_vtable {
    errval_t (*register_recv)(struct aos_rpc_channel *chan, struct waitset *ws, struct event_closure closure);
    void (*send_handler)(void *args);
    void (*recv_handler)(void *args);
    bool (*can_recv)(struct aos_rpc_channel *chan);
    size_t (*get_busy_wait_iterations)(void);
    void (*install_recv_slot)(struct aos_rpc_channel *chan, struct capref cap);
};

extern struct aos_rpc_channel_driver_vtable aos_rpc_channel_lmp_driver_vtable;
extern struct aos_rpc_channel_driver_vtable aos_rpc_channel_ump_driver_vtable;

struct aos_rpc_send_msg_env {
    uintptr_t header;
    enum aos_rpc_intermon_capability_type cap_type; // used to determine re-routing
    struct capref cap;
    size_t buflen;
    const void *buf;
};

struct aos_rpc_recv_msg_env {
    uintptr_t header;
    enum aos_rpc_intermon_capability_type expected_cap_type; // used to determine re-routing
    struct capref cap;
    size_t buflen;
    void *buf;
};

struct aos_rpc_comm_context {
    struct aos_rpc_send_msg_env send_env;
    struct aos_rpc_recv_msg_env recv_env;

    // this buffer is mainly to avoid any malloc() in any path that involves AOS_RPC_MEMORY_FID_GET_RAM_CAP
    // however, it also avoids lots of malloc/free cycles in other short messages. may be beneficial.
    // disadvantage: more stack usage
    bool is_server; // flag needed at the moment to avoid changing lots of other frees in clients
    uintptr_t recv_buffer[(DEFAULT_RPC_BUFFER_SIZE >> 2) + 1]; // note: MUST be aligned to uintptr_t
};

struct aos_rpc_common_request_new_endpoint_req_payload {
    enum aos_rpc_chan_driver chan_type;
};

struct aos_rpc_common_benchmarking_res_payload {
    size_t size;
    char buf[0];
};

struct aos_rpc_common_benchmarking_req_payload {
    size_t size;
    size_t exp_buflen;
    char buf[0];
};

void aos_rpc_channel_lmp_init(struct aos_rpc_channel *chan, struct lmp_chan *lmp_chan);

void aos_rpc_channel_flmp_init(struct aos_rpc_channel *chan, struct flmp_chan *flmp_chan);

void aos_rpc_channel_ump_init(struct aos_rpc_channel *chan, struct ump_chan *ump_chan);

errval_t aos_rpc_generic_send(struct aos_rpc_channel *chan, const struct aos_rpc_send_msg_env *send_env);

errval_t aos_rpc_generic_recv(struct aos_rpc_channel *chan, struct aos_rpc_recv_msg_env *recv_env,
                                       bool is_server, void *server_default_recv_buffer);

/**
 * NOTE: the type of the capability refers to the type if it is transferred by UMP and
 * needs re-routing. No limitations re caps when transferred by LMP/FLMP.
 */
void aos_rpc_comm_context_client_init(struct aos_rpc_comm_context *context,
                                      const void *send_buf, size_t send_buflen, enum aos_rpc_intermon_capability_type send_cap_type,
                                      void *recv_buf, size_t recv_buflen, enum aos_rpc_intermon_capability_type expected_recv_cap_type);

errval_t aos_rpc_generic_send_and_recv(struct aos_rpc *chan, uintptr_t fid, struct aos_rpc_comm_context *context);

void aos_rpc_generic_recv_handler(void *rpc_);


/**
 * This function is used internally to create service channels by late binding.
 */
errval_t late_binding(const char *service_name, struct aos_rpc **ret_rpc);

/**
 * This function is used internally to create the service channels
 *
 * \param target_rpc   the created RPC is stored in *target_rpc
 * \param blocking     true: the function keeps trying in case not yet found in nameserver
 * \param enumerator   a specfic enumerator can be used; use -1 for an arbitrary service (first found)
 * \param getter       getter for the domain 2nd level RPC cache
 * \param setter       setter for the domain 2nd level RPC cache
 *        NOTE: both, getter and setter, can be NULL. Then they are not called.
 * \param name         service base name
 *
 * Contract: \return == SYS_ERR_OK <=> *target_rpc != NULL
 */
errval_t aos_rpc_get_channel(struct aos_rpc **target_rpc,
                             bool blocking,
                             ssize_t enumerator,
                             aos_rpc_getter_function_t getter,
                             aos_rpc_setter_function_t setter,
                             const char *name);

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_COMMON_
