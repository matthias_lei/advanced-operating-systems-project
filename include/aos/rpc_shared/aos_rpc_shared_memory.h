#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_MEMORY_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_MEMORY_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct aos_rpc_interface_memory_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*get_ram_cap) (struct aos_rpc *rpc, size_t size, size_t align, struct capref *retcap, size_t *ret_size);
    errval_t (*load_ram) (struct aos_rpc *rpc, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size);
    errval_t (*memoryservice_register) (struct aos_rpc *rpc, struct capref nameserver_cap);
    errval_t (*get_ram_info) (struct aos_rpc *rpc, size_t *ret_allocated, size_t *ret_free, size_t *ret_free_largest);
};

struct aos_rpc_get_ram_cap_req_payload {
    size_t size;
    size_t align;
};

struct aos_rpc_load_ram_req_payload {
    genpaddr_t base;
    gensize_t size;
};

struct aos_rpc_get_ram_info_res_payload {
    size_t allocated;
    size_t free;
    size_t free_largest;
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_MEMORY_
