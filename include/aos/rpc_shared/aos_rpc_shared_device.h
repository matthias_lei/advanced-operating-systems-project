#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_DEVICE_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_DEVICE_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct aos_rpc_interface_device_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*get_device_cap)(struct aos_rpc *rpc, lpaddr_t paddr, size_t bytes, struct capref *frame);
    errval_t (*get_irq_cap)(struct aos_rpc *rpc, struct capref *cap);
};


struct aos_rpc_get_device_cap_req_payload {
	lpaddr_t paddr;
	size_t bytes;
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_DEVICE_
