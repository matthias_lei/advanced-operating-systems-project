#ifndef INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_FILESYSTEM_
#define INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_FILESYSTEM_

#include <aos/rpc_shared/aos_rpc_shared_common.h>

#include <fs/fs.h>

struct aos_rpc_interface_filesystem_vtable {
    struct aos_rpc_interface_common_vtable common;
    errval_t (*open)(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
    errval_t (*create)(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
    errval_t (*remove)(struct aos_rpc *rpc, const char *path);
    errval_t (*read)(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read);
    errval_t (*write)(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written);
    errval_t (*truncate)(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes);
    errval_t (*tell)(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos);
    errval_t (*stat)(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info);
    errval_t (*seek)(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset);
    errval_t (*close)(struct aos_rpc *rpc, fs_filehandle_t inhandle);
    errval_t (*opendir)(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle);
    errval_t (*dir_read_next)(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info);
    errval_t (*closedir)(struct aos_rpc *rpc, fs_dirhandle_t inhandle);
    errval_t (*mkdir)(struct aos_rpc *rpc, const char *path);
    errval_t (*rmdir)(struct aos_rpc *rpc, const char *path);
    errval_t (*mount)(struct aos_rpc *rpc, const char *path, const char *uri);
};

struct aos_rpc_read_req_payload {
    fs_filehandle_t inhandle;
    size_t bytes;
};

struct aos_rpc_read_res_payload {
    size_t bytes_read;
    char buffer[0];
};

struct aos_rpc_write_req_payload {
    fs_filehandle_t inhandle;
    size_t bytes;
    char buffer[0];
};

struct aos_rpc_truncate_req_payload {
    fs_filehandle_t inhandle;
    size_t bytes;
};

struct aos_rpc_seek_req_payload {
    fs_filehandle_t inhandle;
    uint32_t whence;
    off_t offset;
};

struct aos_rpc_dir_read_next_res_payload {
    struct fs_fileinfo info;
    char retname[0];
};

#endif // INCLUDE_AOS_RPC_SHARED_AOS_RPC_SHARED_FILESYSTEM_
