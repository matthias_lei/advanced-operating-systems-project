/**
 * \file
 * \brief
 */

/*
 * Copyright (c) 2009, 2010, 2011, 2012, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

#ifndef BARRELFISH_DOMAIN_H
#define BARRELFISH_DOMAIN_H

#include <sys/cdefs.h>
#include <aos/threads.h>

#include <aos/aos_rpc_types.h>

__BEGIN_DECLS

typedef void (*domain_spanned_callback_t)(void *arg, errval_t err);

struct waitset;

struct waitset *get_default_waitset(void);
void disp_set_core_id(coreid_t core_id);
coreid_t disp_get_core_id(void);
coreid_t disp_get_current_core_id(void);
void disp_get_eh_frame(lvaddr_t *eh_frame, size_t *eh_frame_size);
void disp_get_eh_frame_hdr(lvaddr_t *eh_frame_hdr, size_t *eh_frame_hdr_size);
domainid_t disp_get_domain_id(void);
coreid_t disp_handle_get_core_id(dispatcher_handle_t handle);
void set_init_rpc(struct aos_rpc *initrpc);
struct aos_rpc *get_init_rpc(void);
struct morecore_state *get_morecore_state(void);
struct paging_state *get_current_paging_state(void);
void set_current_paging_state(struct paging_state *st);
struct ram_alloc_state *get_ram_alloc_state(void);
struct slot_alloc_state *get_slot_alloc_state(void);


/**
 * -------Group C extensions
 *
 */

typedef void (*aos_rpc_setter_function_t)(struct aos_rpc *rpc);
typedef struct aos_rpc *(*aos_rpc_getter_function_t)(void);

void set_blockdev_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_blockdev_rpc(void);

void set_device_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_device_rpc(void);

void set_filesystem_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_filesystem_rpc(void);

void set_memory_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_memory_rpc(void);

void set_name_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_name_rpc(void);

void set_network_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_network_rpc(void);

void set_process_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_process_rpc(void);

void set_serial_rpc(struct aos_rpc *rpc);
struct aos_rpc *get_serial_rpc(void);

// NOTE: deliberately no RPC setters/getters for shell here

void set_intermon_rpc(coreid_t core, struct aos_rpc *rpc);
struct aos_rpc *get_intermon_rpc(coreid_t core);

__END_DECLS

#endif
