/**
 * Function IDs for our generic RPC message calls (see aos_rpc.h and aos_rpc_event_handlers.h)
 *
 * version 2017-12-02, Group C
 */

#ifndef INCLUDE_AOS_AOS_RPC_FUNCTION_IDS_
#define INCLUDE_AOS_AOS_RPC_FUNCTION_IDS_

#include <aos/aos.h>

// during lmp messaging
#define AOS_RPC_HEADER_WORD 0
#define AOS_RPC_SIZE_WORD 1
#define AOS_RPC_PAYLOAD_WORD 2

// when sending large messages, the payload is not used by actual
// message content, so we use it to transfer the frame size in which
// the message content is stored
#define AOS_RPC_LARGE_MESSAGE_FRAME_SIZE AOS_RPC_PAYLOAD_WORD

// the other words 2-8 are for the actual payload
#define AOS_RPC_HEADER_SIZE 2
#define AOS_RPC_PAYLOAD_SIZE (LMP_MSG_LENGTH - AOS_RPC_HEADER_SIZE)

// signal for generic_send_and_block() that no payload is expected
// these macros are only relevant if NULL is passed as receive buffer
#define AOS_RPC_EXPECT_NO_PAYLOAD 0xFFFFFFFF
#define AOS_RPC_EXPECT_PAYLOAD 0x0

//---  helper macros
#define AOS_RPC_FG_OFFSET 24
#define AOS_RPC_FID_OFFSET 16

#define AOS_RPC_FG_BITMASK MASK_T(uintptr_t, 8)
#define AOS_RPC_FID_BITMASK (MASK_T(uintptr_t, 16) << 16)
#define AOS_RPC_ERR_NO_BITMASK MASK_T(uintptr_t, 16)

// note: there is no bitmask defined for errors in the fugu generated errno.h. I copied the hard coded
// masking part verbatim from there. Thus, also no use of bitmacros.
STATIC_ASSERT(AOS_RPC_ERR_NO_BITMASK >= ((1 << 10) - 1), "Bitmask for errno not large enough");


// input header is an uintptr_t; output is 1 byte
#define AOS_RPC_GET_FG(header) ( (uint8_t) ( (((uintptr_t)(header)) >> AOS_RPC_FG_OFFSET) & AOS_RPC_FG_BITMASK ) )


// note: the FID includes the FG and is 2 bytes large
// but these 2 bytes are in the MSB side of the word to have compatible values with
// the defined function numbers below
// input and output of this macro are uintptr_t (words)
#define AOS_RPC_GET_FID(header) ( ((uintptr_t)(header)) & AOS_RPC_FID_BITMASK )


// types here also just to make sure that all works as planned
// additionally the err is going thru a bitmast to not unintentionally change the FG / FID
#define AOS_RPC_ADD_ERROR(fid, err) ( ((uintptr_t)(fid)) | (((uintptr_t)(err)) & AOS_RPC_ERR_NO_BITMASK) )

#define AOS_RPC_GET_ERR_NO(header) ((header) & (AOS_RPC_ERR_NO_BITMASK))

//--- define the function groups here
// note: they must match precisely with the fg_handlers array, defined in the rpc event handlers
// note: there is no actual INIT FG. It is used for NAME FG already for a long time

#define AOS_RPC_COMMON_FG 0
#define AOS_RPC_INIT_FG 1
#define AOS_RPC_MONITOR_FG AOS_RPC_INIT_FG
#define AOS_RPC_INTERMON_FG 2
#define AOS_RPC_NAME_FG 3
#define AOS_RPC_MEMORY_FG 4
#define AOS_RPC_PROCESS_FG 5
#define AOS_RPC_SERIAL_FG 6
#define AOS_RPC_DEVICE_FG 7
#define AOS_RPC_FILESYSTEM_FG 8
#define AOS_RPC_NETWORK_FG 9
#define AOS_RPC_BLOCKDEV_FG 10
#define AOS_RPC_SHELL_FG 11
#define AOS_RPC_FG_SIZE 12

STATIC_ASSERT(AOS_RPC_COMMON_FG == 0, "");

// function group that is reserved for implementation specific internal use
// it is deliberately excluded from AOS_RPC_FG_SIZE, since it should never
// be used externally
#define AOS_RPC_RESERVED_FG 0xff


//--- define function id here
// note: they already include the proper function group

#define AOS_RPC_FG_FID_HELPER(fg, fid) ((uintptr_t) ( ((uintptr_t)(fg) << (AOS_RPC_FG_OFFSET)) | ((uintptr_t)(fid) << (AOS_RPC_FID_OFFSET)) ) )

//--- COMMON: is handled by all services / function groups
// official
#define AOS_RPC_COMMON_FID_SEND_NUMBER             AOS_RPC_FG_FID_HELPER(AOS_RPC_COMMON_FG, 1)
#define AOS_RPC_COMMON_FID_SEND_STRING             AOS_RPC_FG_FID_HELPER(AOS_RPC_COMMON_FG, 2)

// extended
// this function is called by clients and provides the initial contact of a client with the service
// note: this FID compiles to 0 on purpose
#define AOS_RPC_COMMON_FID_CONNECT_WITH_SERVICE    AOS_RPC_FG_FID_HELPER(AOS_RPC_COMMON_FG, 0)
STATIC_ASSERT(AOS_RPC_COMMON_FID_CONNECT_WITH_SERVICE == 0, "");

// this function is called by the name service while handling the lookup request from a client
// it prepares a new domain endpoint that will accept a connection from a client and returns that back to the name service
#define AOS_RPC_COMMON_FID_REQUEST_NEW_ENDPOINT    AOS_RPC_FG_FID_HELPER(AOS_RPC_COMMON_FG, 3)

#define AOS_RPC_COMMON_FID_BENCHMARKING            AOS_RPC_FG_FID_HELPER(AOS_RPC_COMMON_FG, 4)


//--- MEMORY: Dionysos
// official
#define AOS_RPC_MEMORY_FID_GET_RAM_CAP   AOS_RPC_FG_FID_HELPER(AOS_RPC_MEMORY_FG, 1)
// extended
#define AOS_RPC_MEMORY_FID_LOAD_RAM      AOS_RPC_FG_FID_HELPER(AOS_RPC_MEMORY_FG, 2)
#define AOS_RPC_MEMORY_FID_REGISTER      AOS_RPC_FG_FID_HELPER(AOS_RPC_MEMORY_FG, 3)
#define AOS_RPC_MEMORY_FID_GET_INFO      AOS_RPC_FG_FID_HELPER(AOS_RPC_MEMORY_FG, 4)


//--- PROCESS: Demeter
// official
#define AOS_RPC_PROCESS_FID_SPAWN               AOS_RPC_FG_FID_HELPER(AOS_RPC_PROCESS_FG, 1)
#define AOS_RPC_PROCESS_FID_GET_NAME            AOS_RPC_FG_FID_HELPER(AOS_RPC_PROCESS_FG, 2)
#define AOS_RPC_PROCESS_FID_GET_ALL_PID         AOS_RPC_FG_FID_HELPER(AOS_RPC_PROCESS_FG, 3)
// extended
#define AOS_RPC_PROCESS_FID_SPAWN_FROM_FILE     AOS_RPC_FG_FID_HELPER(AOS_RPC_PROCESS_FG, 4)
#define AOS_RPC_PROCESS_FID_SPAWN_FROM_SHELL    AOS_RPC_FG_FID_HELPER(AOS_RPC_PROCESS_FG, 5)


//--- SERIAL I/O: Hermes
// official
#define AOS_RPC_SERIAL_FID_GETCHAR     AOS_RPC_FG_FID_HELPER(AOS_RPC_SERIAL_FG, 1)
#define AOS_RPC_SERIAL_FID_PUTCHAR     AOS_RPC_FG_FID_HELPER(AOS_RPC_SERIAL_FG, 2)
// extended
#define AOS_RPC_SERIAL_FID_PUTSTRING   AOS_RPC_FG_FID_HELPER(AOS_RPC_SERIAL_FG, 3)
#define AOS_RPC_SERIAL_FID_GET_WINDOW AOS_RPC_FG_FID_HELPER(AOS_RPC_SERIAL_FG, 4)

//--- DEVICE: Prometheus
// official
#define AOS_RPC_DEVICE_FID_GET_DEVICE_CAP    AOS_RPC_FG_FID_HELPER(AOS_RPC_DEVICE_FG, 1)
// extended
#define AOS_RPC_DEVICE_FID_GET_IRQ_CAP       AOS_RPC_FG_FID_HELPER(AOS_RPC_DEVICE_FG, 2)

//--- FG associated with distributed system and nameserver -----------------------------------------

//--- MONITOR: Poseidon -- communication with the monitor
// extended
#define AOS_RPC_MONITOR_FID_REQUEST_RPC_FORWARD        AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 1)
#define AOS_RPC_MONITOR_FID_SPAWN_DOMAIN               AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 2)
#define AOS_RPC_MONITOR_FID_SPAWN_DOMAIN_FROM_FILE     AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 3)
#define AOS_RPC_MONITOR_FID_BOOTSTRAP_OFFER_SERVICE    AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 4)
#define AOS_RPC_MONITOR_FID_OFFER_SERVICE              AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 5)
#define AOS_RPC_MONITOR_FID_GET_BOOTSTRAP_PIDS         AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 6)
#define AOS_RPC_MONITOR_FID_GET_MULTIBOOT_MODULE_NAMES AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 7)
#define AOS_RPC_MONITOR_FID_GET_MULTIBOOT_MODULE       AOS_RPC_FG_FID_HELPER(AOS_RPC_MONITOR_FG, 8)

//--- INTERMON: Poseidon's wake -- explicit communication between monitors
// extended
#define AOS_RPC_INTERMON_FID_REQUEST_BOOTSTRAP_INFO    AOS_RPC_FG_FID_HELPER(AOS_RPC_INTERMON_FG, 1)
#define AOS_RPC_INTERMON_FID_FORWARD_RPC_MESSAGE       AOS_RPC_FG_FID_HELPER(AOS_RPC_INTERMON_FG, 2)

//--- NAME: Gaia
// extended
// - service side
#define AOS_RPC_NAME_FID_SERVICE_REGISTER              AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 1)
#define AOS_RPC_NAME_FID_SERVICE_DEREGISTER            AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 2)
#define AOS_RPC_NAME_FID_SERVICE_PING                  AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 3)
#define AOS_RPC_NAME_FID_SERVICE_UPDATE                AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 4)
//- client side
#define AOS_RPC_NAME_FID_LOOKUP_SERVICE_BY_PATH        AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 5)
#define AOS_RPC_NAME_FID_LOOKUP_SERVICE_BY_SHORT_NAME  AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 6)
#define AOS_RPC_NAME_FID_FIND_ANY_SERVICE_BY_PATH      AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 7)
#define AOS_RPC_NAME_FID_FIND_ANY_SERVICE_BY_FILTER    AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 8)
#define AOS_RPC_NAME_FID_GET_SERVICE_INFO              AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 9)
#define AOS_RPC_NAME_FID_BIND_SERVICE_LOWLEVEL         AOS_RPC_FG_FID_HELPER(AOS_RPC_NAME_FG, 10)

//--- FILESYSTEM: Plutos
// extended
#define AOS_RPC_FILESYSTEM_FID_OPEN                    AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 1)
#define AOS_RPC_FILESYSTEM_FID_CREATE                  AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 2)
#define AOS_RPC_FILESYSTEM_FID_REMOVE                  AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 3)
#define AOS_RPC_FILESYSTEM_FID_READ                    AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 4)
#define AOS_RPC_FILESYSTEM_FID_WRITE                   AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 5)
#define AOS_RPC_FILESYSTEM_FID_TRUNCATE                AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 6)
#define AOS_RPC_FILESYSTEM_FID_TELL                    AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 7)
#define AOS_RPC_FILESYSTEM_FID_STAT                    AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 8)
#define AOS_RPC_FILESYSTEM_FID_SEEK                    AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 9)
#define AOS_RPC_FILESYSTEM_FID_CLOSE                   AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 10)
#define AOS_RPC_FILESYSTEM_FID_OPENDIR                 AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 11)
#define AOS_RPC_FILESYSTEM_FID_DIR_READ_NEXT           AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 12)
#define AOS_RPC_FILESYSTEM_FID_CLOSEDIR                AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 13)
#define AOS_RPC_FILESYSTEM_FID_MKDIR                   AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 14)
#define AOS_RPC_FILESYSTEM_FID_RMDIR                   AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 15)
#define AOS_RPC_FILESYSTEM_FID_MOUNT                   AOS_RPC_FG_FID_HELPER(AOS_RPC_FILESYSTEM_FG, 16)

//--- NETWORK: Charon
// extended
#define AOS_RPC_NETWORK_FID_ACCEPT           AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 1)
#define AOS_RPC_NETWORK_FID_BIND             AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 2)
#define AOS_RPC_NETWORK_FID_CONNECT          AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 3)
#define AOS_RPC_NETWORK_FID_GETPEERNAME      AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 4)
#define AOS_RPC_NETWORK_FID_GETSOCKNAME      AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 5)
#define AOS_RPC_NETWORK_FID_GETSOCKOPT       AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 6)
#define AOS_RPC_NETWORK_FID_LISTEN           AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 7)
#define AOS_RPC_NETWORK_FID_RECV             AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 8)
#define AOS_RPC_NETWORK_FID_RECVFROM         AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 9)
#define AOS_RPC_NETWORK_FID_RECVMSG          AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 10)
#define AOS_RPC_NETWORK_FID_SEND             AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 11)
#define AOS_RPC_NETWORK_FID_SENDMSG          AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 12)
#define AOS_RPC_NETWORK_FID_SENDTO           AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 13)
#define AOS_RPC_NETWORK_FID_SETSOCKOPT       AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 14)
#define AOS_RPC_NETWORK_FID_SHUTDOWN         AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 15)
#define AOS_RPC_NETWORK_FID_SOCKET           AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 16)
#define AOS_RPC_NETWORK_FID_SOCKETPAIR       AOS_RPC_FG_FID_HELPER(AOS_RPC_NETWORK_FG, 17)

//--- BLOCKDEV
// extended
#define AOS_RPC_BLOCKDEV_FID_GET_BLOCK_SIZE            AOS_RPC_FG_FID_HELPER(AOS_RPC_BLOCKDEV_FG, 1)
#define AOS_RPC_BLOCKDEV_FID_READ_BLOCK                AOS_RPC_FG_FID_HELPER(AOS_RPC_BLOCKDEV_FG, 2)
#define AOS_RPC_BLOCKDEV_FID_WRITE_BLOCK               AOS_RPC_FG_FID_HELPER(AOS_RPC_BLOCKDEV_FG, 3)

//--- SHELL: Zeus
// extended
#define AOS_RPC_SHELL_FID_REQUEST_PARAMS               AOS_RPC_FG_FID_HELPER(AOS_RPC_SHELL_FG, 1)
#define AOS_RPC_SHELL_FID_NOTIFY_TERMINATION           AOS_RPC_FG_FID_HELPER(AOS_RPC_SHELL_FG, 2)

#endif // INCLUDE_AOS_AOS_RPC_FUNCTION_IDS_
