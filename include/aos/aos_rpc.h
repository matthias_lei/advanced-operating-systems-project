/**
 * \file
 * \brief Interface declaration for AOS rpc-like messaging
 */

/*
 * Copyright (c) 2013, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#ifndef _LIB_BARRELFISH_AOS_MESSAGES_H
#define _LIB_BARRELFISH_AOS_MESSAGES_H

#include <aos/aos.h>

#include <hashtable/serializable_key_value_store.h>

#include <aos/aos_rpc_types.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// pre-define them here to avoid the need to include nameserver_types.h already for
// this header file (would lead to circular dependency)
typedef uint64_t service_handle_t;
typedef uint32_t routing_info_t;
struct service_registration_request;
struct service_registration_reply;

// pre-define them here to avoid the need to include aos/rpc_shared/aos_rpc_shared_common.h already for
// this header file (would lead to circular dependency)
enum aos_rpc_interface;
enum aos_rpc_chan_driver;

struct aos_rpc_send_msg_env;
struct aos_rpc_recv_msg_env;

// pre-define them here to avoid the need to include aos/rpc_shared/aos_rpc_shared_intermon.h already for
// this header file (would lead to circular dependency
struct aos_rpc_intermon_forward_req_payload;
struct aos_rpc_intermon_forward_res_payload;

// pre-define them here to avoid the need to include aos/rpc_shared/aos_rpc_shared_monitor.h already for
// this header file (would lead to circular dependency
struct aos_rpc_get_bootstrap_pids_res_payload;

// routing macros (in little endian, destination server first, return address client second)
#define AOS_RPC_ROUTING_SET(target_server_core_id, client_core_id) \
    ( (((routing_info_t)(target_server_core_id)) & 0xff) | ((((routing_info_t)(client_core_id)) & 0xff) << 8) )

#define AOS_RPC_ROUTING_SERVER_CORE_ID(routing_info) ((coreid_t)((routing_info_t)(routing_info) & 0xff))
#define AOS_RPC_ROUTING_CLIENT_CORE_ID(routing_info) ((coreid_t)(( ((routing_info_t)(routing_info)) >> 8) & 0xff))

#define AOS_RPC_ROUTING_NEEDED(routing_info, cap_in_request, cap_in_reply)                                          \
    ( ((cap_in_request) != AOS_RPC_INTERMON_CAPABILITY_NONE || (cap_in_reply) != AOS_RPC_INTERMON_CAPABILITY_NONE)  \
    && (AOS_RPC_ROUTING_SERVER_CORE_ID(routing_info) != AOS_RPC_ROUTING_CLIENT_CORE_ID(routing_info) ) )


// constant that can be used to acknowledge a request without expected return value
// see aos/aos.h for RPC services mode
#define AOS_RPC_ACK(rpc_mode) ((uintptr_t)('A' | ('C' << 8) | ('K' << 16) | ( ((uint8_t)(rpc_mode)) << 24)))

#define AOS_RPC_ACK_MASK 0x00ffffff

#define AOS_RPC_ACK_OK(value) ( ((value) & AOS_RPC_ACK_MASK) == (AOS_RPC_ACK(0) & AOS_RPC_ACK_MASK) )

#define AOS_RPC_ACK_MODE(value) ((enum rpc_services_mode)((value) >> 24))


struct aos_rpc_channel;

//=== RPC service / function group COMMON: can be handled by all services ==========================
//--- official RPC API
/**
 * \brief send a number over the given channel
 */
errval_t aos_rpc_send_number(struct aos_rpc *rpc, uintptr_t val);

/**
 * \brief send a string over the given channel
 */
errval_t aos_rpc_send_string(struct aos_rpc *rpc, const char *string);

//--- extended API
/**
 * \brief Builds a channel with the indicated service.
 *
 * \param service_cap   as received(*): either LMP endpoint or frame for UMP ringbuffers
 * \param interface     as defined(*)
 * \param chan_type     as defined(*)
 * \param routing_info  as defined(*)
 * \param ret_rpc       complete usable RPC service connection
 *                      contract: (\return == SYS_ERR_OK) <=> (*ret_rpc != NULL)
 * \param ret_mode      service mode of the sysstem; optional: is not set in case it is provided as NULL
 *
 * (*) The parameters typically have been received from earlier call of aos_rpc_name_bind_service_lowlevel().
 * Exception: During bootstrapping of the domain, a few selected channels are provided with the needed
 * information by the spawninfo, which allows calling this RPC function from the freshly spawned
 * domain, too. This unifies the code paths of establishing channels during spawn and later
 * lazy binding with other services.
 */
errval_t aos_rpc_connect_with_service(struct capref service_cap,
                                      enum aos_rpc_interface interface,
                                      enum aos_rpc_chan_driver chan_type, routing_info_t routing,
                                      struct aos_rpc **ret_rpc, enum rpc_services_mode *ret_mode);

/**
 * \brief This function is called from name service while handling a bind request from a client.
 * It requests the target service to prepare a new endpoint that can accept a new connection.
 * The returned ret_cap is then sent back to the client doing the lookup, who in turn
 * can connect with this service afterwards. This is part of the bind_service() protocol.
 *
 * \param rpc        contact RPC channel from name server to registered service
 * \param chan_type  one of the available channel types as determined by nameserver
 * \param ret_cap    either a LMP endpoint (LMP/FLMP) or a frame for ringbuffers (UMP)
 *
 * Note: *ret_cap may be NULL_CAP in case of error.
 * but \return == SYS_ERR_OK => *ret_cap != NULL_CAP
 *
 * Technical detail: In addition to returning the new cap to build a proper channel,
 * the server prepares itself for getting an aos_rpc_connect_with_service() request
 * sometime soon in the future from the client that called bind() on the nameserver.
 */
errval_t aos_rpc_request_new_endpoint(struct aos_rpc *rpc, enum aos_rpc_chan_driver chan_type,
                                      struct capref *ret_cap);

/**
 * \brief send and receive a buffer of desired size over channel
 */
errval_t aos_rpc_benchmarking(struct aos_rpc *rpc, size_t buflen, size_t exp_ret_buflen);


//--- additional COMMON helper functions -----------------------------------------------------------

/**
 * Initializes a freshly allocated rpc to become a local channel for a particular interface.
 * This is used in servers to allow them access to their own functions directly.
 * NOTE: not all services offer such a valid local vtable (see: NULL in the vtables are not allowed to call)
 *
 * Additional note: if a domain offers multiple such interfaces as a service, then  also multiple
 * RPC need to be generated, one for each of the interfaces. However, it is best to have one
 * shared event handler only that can handle all of these events. Please ask for details. pisch
 *
 * \param rpc
 * \param interface
 * \param vtable
 */
static inline void init_local_channel(struct aos_rpc *rpc, enum aos_rpc_interface interface, void *vtable)
{
    rpc->chan = NULL;
    rpc->interface = interface;
    rpc->interface_vtable = vtable;
    rpc->domain_id = disp_get_domain_id();
    coreid_t c = disp_get_core_id();
    rpc->routing = AOS_RPC_ROUTING_SET(c, c);
}

//=== RPC service / function group RAM: Dionysos ===================================================
//--- official RPC API
/**
 * \brief request a RAM capability with size >= bytes.
 */
errval_t aos_rpc_get_ram_cap(struct aos_rpc *rpc, size_t bytes, size_t align,
                             struct capref *retcap, size_t *ret_bytes);

//--- extended API

/**
 * This RPC is used by monitor/0 during bootstrap.
 * \param ram_cap
 * \param ram_base
 * \param ram_size
 * define the memory region to be added to the memory service for general use.
 */
errval_t aos_rpc_load_ram(struct aos_rpc *rpc, struct capref ram_cap, genpaddr_t ram_base, gensize_t ram_size);

/**
 * This RPC is used by monitor/0 during bootstrap.
 * \param nameserver_cap  A freshly acquired endpoint to connect with the name service.
 *                        endpoint was acquired by monitor0 by aos_rpc_request_new_endpoint()
 *
 * Upon success of this function, the memory service is registered in the name service.
 */
errval_t aos_rpc_memoryservice_register(struct aos_rpc *rpc, struct capref nameserver_cap);

/**
 * Returns current state of the memory system.
 */
errval_t aos_rpc_get_ram_info(struct aos_rpc *rpc, size_t *ret_allocated, size_t *ret_free, size_t *ret_free_largest);


//=== RPC service / function group serial I/O: Hermes ==============================================
//--- official RPC API
/**
 * \brief get one character from the serial port
 */
errval_t aos_rpc_serial_getchar(struct aos_rpc *rpc, char *retc);

/**
 * \brief send one character to the serial port
 */
errval_t aos_rpc_serial_putchar(struct aos_rpc *rpc, char c);

//--- extended API
/**
 * \brief send a string of len characters to the serial port
 */
errval_t aos_rpc_serial_putstring(struct aos_rpc *rpc, const char *s, size_t len);

/**
 * \brief get window of other domain
 */
errval_t aos_rpc_serial_get_window(struct aos_rpc *rpc, domainid_t window_id);


//=== RPC service / function group process/domain management: Demeter ==============================
//--- official RPC API
/**
 * \brief Request process manager to start a new process from the multiboot image
 * \arg name the name of the process that needs to be spawned (without a
 *           path prefix)
 * \arg newpid the process id of the newly spawned process
 */
errval_t aos_rpc_process_spawn(struct aos_rpc *rpc, char *name,
                               coreid_t core_id, domainid_t *newpid);

/**
 * \brief Get name of process with id pid.
 * \arg pid the process id to lookup
 * \arg name A null-terminated character array with the name of the process
 * that is allocated by the rpc implementation. Freeing is the caller's
 * responsibility.
 */
errval_t aos_rpc_process_get_name(struct aos_rpc *rpc, domainid_t pid,
                                  char **name);

/**
 * \brief Get process ids of all running processes
 * \arg pids An array containing the process ids of all currently active
 * processes. Will be allocated by the rpc implementation. Freeing is the
 * caller's  responsibility.
 * \arg pid_count The number of entries in `pids' if the call was successful
 */
errval_t aos_rpc_process_get_all_pids(struct aos_rpc *rpc,
                                      domainid_t **pids, size_t *pid_count);

//--- extended API

/**
 * \brief Request process manager to start a new process from the filesystem
 * \arg path the path of the binary
 * \arg command_line the command line to be forwarded to the process
 * \arg core_id the id of the core to spawn on
 * \arg shell_id the nameserver id of the shell spawning this domain, -1 if not spawned by shell
 * \arg newpid the process id of the newly spawned process
 */
errval_t aos_rpc_process_spawn_from_file(struct aos_rpc *rpc, const char *path,
                                         const char *command_line, coreid_t core_id,
                                         int shell_id, domainid_t *newpid);

/**
 * \brief Request process manager to start a new process from the multiboot image
 * \arg name the name of the process that needs to be spawned (without a
 *           path prefix)
 * \arg newpid the process id of the newly spawned process
 * \arg shell_id the nameserver id of the shell spawning the process
 */
errval_t aos_rpc_process_spawn_from_shell(struct aos_rpc *rpc, char *name,
                               coreid_t core_id, int shell_id, domainid_t *newpid);


//=== RPC service / function group device management: Prometheus ===================================
//--- official RPC API
/**
 * \brief Gets a capability to device registers
 * \param rpc  the rpc channel
 * \param paddr physical address of the device
 * \param bytes number of bytes of the device memory
 * \param frame returned devframe
 */
errval_t aos_rpc_get_device_cap(struct aos_rpc *rpc, lpaddr_t paddr, size_t bytes,
                                struct capref *frame);


//--- extended API
/**
 * \brief Gets the IRQ capability
 * \param rpc  the rpc channel
 * \param frame returned IRQ capability
 */
errval_t aos_rpc_device_get_irq_cap(struct aos_rpc *rpc, struct capref *cap);


//=== RPC service / function group MONITOR: Poseidon ===============================================
//    currently mainly used to request forwarding of an RPC via INTERMON service
//--- extended API
/**
 * Forwards a full payload of an RPC request via INTERMON service to another core to let it
 * execute there and then the result is returned back via INTERMON service.
 * This is needed to transfer capabilities between cores.
 * \param rpc
 * \param interface   destination interface / FG
 * \param enumerator  enumerator of the destination service/interface/FG for services that can be
 *                    offered in multiple instances
 * \param fid         the requested FID
 * \param routing     routing information that is used by the monitor to route the request
 * \param send_env    RPC send envelope to be forwarded
 * \param recv_env    RPC recv envelope for the reply
 *
 * NOTE: this function never actually is called directy. It is called from within
 * generic_send_and_recv() if it detects a need for re-routing.
 */
errval_t aos_rpc_monitor_request_rpc_forward(struct aos_rpc *rpc, enum aos_rpc_interface interface,
                                             size_t enumerator, uint32_t fid, routing_info_t routing,
                                             struct aos_rpc_send_msg_env *send_env,
                                             struct aos_rpc_recv_msg_env *recv_env);

/**
 * Process service calls this function to spawn a program from the multiboot image.
 * If core_id is different to the id of the monitor, it will forward the request to the appropriate monitor.
 * Note: The actual spawn services are in the monitors.
 * \param rpc
 * \param shell_id
 * \param pid  designated unique domain id
 */
errval_t aos_rpc_monitor_spawn_domain(struct aos_rpc *rpc, const char *name, coreid_t core_id, int shell_id, domainid_t pid);

/**
 * Process service calls this function to spawn a program from a file.
 * If core_id is different to the id of the monitor, it will forward the request to the appropriate monitor.
 * Note: The actual spawn services are in the monitors.
 * \param rpc
 * \param pid  designated unique domain id
 */
errval_t aos_rpc_monitor_spawn_domain_from_file(struct aos_rpc *rpc, const char *path,
                                                const char *command_line, coreid_t core_id,
                                                int shell_id, domainid_t pid);


/**
 * This function is used by memory and name services to allow monitor/0 calling them
 * during bootstrap & spawn processes
 * \param rpc
 * \param interface   (either for memory or name)
 * \param service_cap (used to build a channel)
 */
errval_t aos_rpc_monitor_bootstrap_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, struct capref service_cap);

/**
 * This function is used by application services that are generated dynamically and be around with various
 * enumerator settings. If they actively notify their monitors about their services, later intermon
 * resolution becomes much simpler. Although similar functionality, the RPC is kept separate from bootstrap
 * function on purpose.
 * \param rpc
 * \param interface   (currently only for shell FG)
 * \param enumerator  (which enum)
 * \param chan_type   (offers flexibility)
 * \param service_cap (used to build a channel)
 */
errval_t aos_rpc_monitor_offer_service(struct aos_rpc *rpc, enum aos_rpc_interface interface, size_t enumerator,
                                       enum aos_rpc_chan_driver chan_type, struct capref service_cap);

/**
 * This function is used by Demeter to get the pids of the domains that were spawned by
 * monitor/0 during the bootstrap. This list includes Demeter itself.
 * \param rpc
 * \param res  is provided by client and just filled during the call.
 */
errval_t aos_rpc_monitor_get_bootstrap_pids(struct aos_rpc *rpc, struct aos_rpc_get_bootstrap_pids_res_payload *res);

/**
 * This function returns the list of names of available multiboot images. The list contains
 * all names separated by a zero termination symbol and ends with two zero termination symbols.
 * It is used in the multiboot filesystem
 */
errval_t aos_rpc_monitor_get_multiboot_module_names(struct aos_rpc *rpc, char **name_list, size_t *list_len);

/**
 * This function returns the frame capability of the specified multiboot module name.
 */
errval_t aos_rpc_monitor_get_multiboot_module(struct aos_rpc *rpc, const char *name, struct capref *frame);


//=== RPC service / function group INTERMON: privileged communication between monitors =============
//    provided by the monitors (a.k.a. Poseidon's wake)

//--- extended API
struct aos_intermon_bootstrap_res; // to avoid cyclic includes
/**
 * A freshly spawnded monitor requests key information from the spawning monitor 0.
 * \param rpc      intermon RPC
 * \param core_id  id of the fresh spawned core
 * \param info     reply
 */
errval_t aos_rpc_intermon_request_bootstrap_info(struct aos_rpc *rpc, coreid_t core_id, struct aos_intermon_bootstrap_res *info);

/**
 * Forward an entire RPC message from one monitor to another to let it execute there.
 * This RPC is called in a monitor while handling an aos_rpc_monitor_request_rpc_forward() request.
 * One RAM / frame capability can be transported
 * \param rpc
 * \param send
 * \param recv  buffer for reply. Size of this buffer must be >= DEFAULT_RPC_BUFFER_SIZE
 *              and uintptr_t aligned
 */
errval_t aos_rpc_intermon_forward_rpc_message(struct aos_rpc *rpc, struct aos_rpc_intermon_forward_req_payload *send,
                                              struct aos_rpc_intermon_forward_res_payload *recv);


//=== RPC service / function group NAME: Gaia ======================================================
//-- extended API

//--- service side: service comes first ------------------------------------------------------------

//--- registration ---

/**
 * \param request      a copy of the content in request is made internally
 *                     can be freed afterwards
 * \param contact_cap  either LMP endpoint or UMP ringbuffer frame
 *                     decision is made based on core_ids of nameserver and registering service
 * \param *ret_reply   will have the replies for the calling service in case of success
 *                     NOTE: this struct is allocated by malloc() and must be freed by
 *                     the client after use.
 *                     It crucially holds the service handle and registration key that are both
 *                     used to deregister a service.
 */
errval_t aos_rpc_name_service_register(struct aos_rpc *rpc, const struct service_registration_request *request,
                                       struct capref contact_cap,
                                       struct service_registration_reply **ret_reply);

/**
 * This is a convenience wrapper that simplifies registration of a service:
 * - it registers the proper event handler
 * - it registers to the name service via aos_rpc_name_service_register().
 * - it registers the service with its monitor for intermon services
 *   -> simplifies intermon service resolution later
 *
 * Thus, typically this function here is used.
 *
 * It also creates a fresh contact end point (either LMP or UMP) that allows nameservice to contact.
 * And finally, the service is also registered for auto-ping to the nameserver.
 *
 * \param abs_path                 valid absolute path that is used to define the full name of the registered service
 * \param short_name_prefix        analogously for the short name
 * \param interface                offered interface of the service
 * \param high_bandwidth_service   true: FLMP / false: LMP will be used upon binding; does not matter for UMP
 * \param kv_store                 any kind of meta data; note: all meta data is searchable
 # \param event_handler            the event handler functionthat is handling the events in this service
 * \param *ret_reply   will have the replies for the calling service in case of success
 *                     NOTE: this struct is allocated by malloc() and must be freed by
 *                     the client after use.
 *                     It crucially holds the service handle and registration key that are both
 *                     used to deregister a service.
 */
errval_t aos_rpc_service_register(const char *abs_path, const char *short_name_prefix,
                                  enum aos_rpc_interface interface, bool high_bandwidth_service,
                                  struct serializable_key_value_store *kv_store,
                                  waitlist_event_handler_t event_handler,
                                  struct service_registration_reply **ret_reply);


//--- deregistration ---

errval_t aos_rpc_name_service_deregister(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);


//--- ping ---

errval_t aos_rpc_name_service_ping(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);


//--- update key value store ---
errval_t aos_rpc_name_service_update(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store);


//--- helper function for the RPC server -----------------------------------------------------------
// these are not mapped to RPC functions but actually register a perodic event in the service
// that calls ping()
errval_t aos_rpc_name_service_start_auto_ping(struct aos_rpc *rpc, service_handle_t handle, uint64_t registration_key);


//--- client side: service comes second ------------------------------------------------------------

//--- lookup / find ---

// basic functions: detected service must be unique, error otherwise
errval_t aos_rpc_name_lookup_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t *ret_handle);

static inline errval_t aos_rpc_name_lookup_service_by_name(struct aos_rpc *rpc, const char *name, service_handle_t *ret_handle)
{
    return aos_rpc_name_lookup_service_by_path(rpc, name, ret_handle);
}

errval_t aos_rpc_name_lookup_service_by_short_name(struct aos_rpc *rpc, const char *short_name, service_handle_t *ret_handle);

// advanced functions: may return 0 to many service handles
errval_t aos_rpc_name_find_any_service_by_path(struct aos_rpc *rpc, const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count);

errval_t aos_rpc_name_find_any_service_by_filter(struct aos_rpc *rpc, const struct serializable_key_value_store *filter,
                                                 service_handle_t **ret_handles, size_t *ret_count);


//--- enumerate ---

static inline errval_t aos_rpc_name_enumerate_all_services(struct aos_rpc *rpc, service_handle_t **ret_handles, size_t *ret_count)
{
    return aos_rpc_name_find_any_service_by_path(rpc, "/", ret_handles, ret_count);
}


//--- get info ---

errval_t aos_rpc_name_get_service_info(struct aos_rpc *rpc, service_handle_t handle, struct serializable_key_value_store **ret_info);

//--- bind ---

/**
 * \brief This is the actual RPC functionality that is used by the more convenient bind_service()
 * function that is offered within the RPC interface.
 * \param handle   a valid service handle
 * \param core_id  core_id of the client requesting a bind
 * \param ret_routing_info  routing info is only needed for RPC calls that need to transport
 *                          capabilities. In case client and server are not running on the
 *                          same core, a special routing protocol is used via intermon service
 * \param ret_interface     one of the defined RPC interface types.
 * \param ret_enumerator    enumerator of the service as registered at the nameserver
 * \param ret_chan_type     defines the protocol that is used to establish a channel
 *                          LMP: use cap as end point
 *                          FLMP: like LMP, but server sends additional frame for ringbuffers
 *                          UMP: use cap as frame for ringbuffers
 * \param ret_service_cap   capability to establish a channel (endpoint for LMP/FLMP; frame for UMP)
 */
errval_t aos_rpc_name_bind_service_lowlevel(struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id,
                                            routing_info_t *ret_routing_info,
                                            enum aos_rpc_interface *ret_interface,
                                            size_t *ret_enumerator,
                                            enum aos_rpc_chan_driver *ret_chan_type,
                                            struct capref *ret_service_cap);

/**
 * \brief This is the function that is used by clients. Internally, it uses the lowlevel function
 * to communicate with the nameserver and various other functions to establish a working RPC channel.
 * \param handle   a valid service handle
 * \param core_id  core_id of the client requesting a bind
 * \param ret_rpc  an established RPC communication channel for the requested service in case of success.
 */
errval_t aos_rpc_name_bind_service(struct aos_rpc *rpc, service_handle_t handle, coreid_t core_id, struct aos_rpc **ret_rpc);

//--- client side convenience functions

/**
 * Since such a sort function may be needed potentially all the time when a find / get_info cycle is happening,
 * it is written here for convenience of the user/client.
 * It sorts the array by the name (that contains the full path)
 * \param rpc             currently not used, just for symmetry with the other functions
 * \param info_ptr_array  the function expects an array of info key-value store pointers
 * \param count           number of elements
 */
void aos_rpc_name_sort_info_ptr_array(struct aos_rpc *rpc, struct serializable_key_value_store **info_ptr_array, size_t count);

//=== RPC service / function group FILESYSTEM: Plutos ==============================================
//-- extended API

struct fs_fileinfo;
typedef void * fs_filehandle_t;
typedef void * fs_dirhandle_t;

errval_t aos_rpc_filesystem_open(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
errval_t aos_rpc_filesystem_create(struct aos_rpc *rpc, const char *path, fs_filehandle_t *outhandle);
errval_t aos_rpc_filesystem_remove(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_filesystem_read(struct aos_rpc *rpc, fs_filehandle_t inhandle, void *buffer, size_t bytes, size_t *bytes_read);
errval_t aos_rpc_filesystem_write(struct aos_rpc *rpc, fs_filehandle_t inhandle, const void *buffer, size_t bytes, size_t *bytes_written);
errval_t aos_rpc_filesystem_truncate(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t bytes);
errval_t aos_rpc_filesystem_tell(struct aos_rpc *rpc, fs_filehandle_t inhandle, size_t *pos);
errval_t aos_rpc_filesystem_stat(struct aos_rpc *rpc, fs_filehandle_t inhandle, struct fs_fileinfo *info);
errval_t aos_rpc_filesystem_seek(struct aos_rpc *rpc, fs_filehandle_t inhandle, uint32_t whence, off_t offset);
errval_t aos_rpc_filesystem_close(struct aos_rpc *rpc, fs_filehandle_t inhandle);
errval_t aos_rpc_filesystem_opendir(struct aos_rpc *rpc, const char *path, fs_dirhandle_t *outhandle);
errval_t aos_rpc_filesystem_dir_read_next(struct aos_rpc *rpc, fs_dirhandle_t inhandle, char **retname, struct fs_fileinfo *info);
errval_t aos_rpc_filesystem_closedir(struct aos_rpc *rpc, fs_dirhandle_t inhandle);
errval_t aos_rpc_filesystem_mkdir(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_filesystem_rmdir(struct aos_rpc *rpc, const char *path);
errval_t aos_rpc_filesystem_mount(struct aos_rpc *rpc, const char *path, const char *uri);

//=== RPC service / function group NETWORK: Charon =================================================
//-- extended API

errval_t aos_rpc_network_accept(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_bind(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
errval_t aos_rpc_network_connect(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, const struct sockaddr *address, socklen_t address_len);
errval_t aos_rpc_network_getpeername(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_getsockname(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_getsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, void *option_value, socklen_t *option_len);
errval_t aos_rpc_network_listen(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int backlog);
errval_t aos_rpc_network_recv(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags);
errval_t aos_rpc_network_recvfrom(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, void *buffer, size_t length, int flags, struct sockaddr *address, socklen_t *address_len);
errval_t aos_rpc_network_recvmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, struct msghdr *message, int flags);
errval_t aos_rpc_network_send(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags);
errval_t aos_rpc_network_sendmsg(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const struct msghdr *message, int flags);
errval_t aos_rpc_network_sendto(struct aos_rpc *rpc, ssize_t *ret_size, int *err_no, int socket, const void *message, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
errval_t aos_rpc_network_setsockopt(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int level, int option_name, const void *option_value, socklen_t option_len);
errval_t aos_rpc_network_shutdown(struct aos_rpc *rpc, int *ret_value, int *err_no, int socket, int how);
errval_t aos_rpc_network_socket(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol);
errval_t aos_rpc_network_socketpair(struct aos_rpc *rpc, int *ret_value, int *err_no, int domain, int type, int protocol, int socket_vector[2]);

//=== RPC service / function group BLOCKDEV: Elektra ===============================================
//-- extended API

errval_t aos_rpc_blockdev_get_block_size(struct aos_rpc *rpc, size_t *block_size);
errval_t aos_rpc_blockdev_read_block(struct aos_rpc *rpc, size_t block_nr, void *buf, size_t buflen);
errval_t aos_rpc_blockdev_write_block(struct aos_rpc *rpc, size_t block_nr, const void *buf, size_t buflen);

//== RPC service / function group SHELL: Zeus ======================================================

struct shell_params;

/**
 * \brief requests parameters from shell
 */
errval_t aos_rpc_shell_request_params(struct aos_rpc *rpc, struct shell_params **ret_params);

/**
 * \brief notifies shell that process is about to terminate
 */
errval_t aos_rpc_shell_notify_termination(struct aos_rpc *rpc, domainid_t pid);

//--- channel initialization -----------------------------------------------------------------------

/**
 * \brief Initialize given rpc channel.
 * DONE: you may want to change the inteface of your init function, depending
 * on how you design your message passing code.
 *
 * \param rpc               RPC to be initialized
 * \param chan              initialized RPC channel (LMP, FLMP, UMP)
 * \param interface         one of the defined interfaces
 * \param interface_vtable  either local or remote vtable
 * \param routing           routing information for intermon capability transfers
 */
void aos_rpc_init(struct aos_rpc *rpc, struct aos_rpc_channel *chan,
                  enum aos_rpc_interface interface, void *interface_vtable,
                  routing_info_t routing);


//--- RPC service / function group name (for other services): Gaia ---------------------------------
// note: this service mainly works under the hood using the added extended API functions

//--- official RPC API

/**
 * \brief Returns the RPC channel to init.
 * kept for compatibility / completeness with the provided interface;
 * current use distinguishes: monitor_channel() and name_channel()
 *
 * returns monitor_channel() at the moment.
 */
struct aos_rpc *aos_rpc_get_init_channel(void);

/**
 * \brief Returns the channel to the memory server
 */
struct aos_rpc *aos_rpc_get_memory_channel(void);

/**
 * \brief Returns the channel to the process manager
 */
struct aos_rpc *aos_rpc_get_process_channel(void);

/**
 * \brief Returns the channel to the serial console
 */
struct aos_rpc *aos_rpc_get_serial_channel(void);

/**
 * \brief Returns the channel to the device server
 */
struct aos_rpc *aos_rpc_get_device_channel(void);

//--- extended API
/**
 * \brief Returns the channel to the domain's monitor.
 */
struct aos_rpc *aos_rpc_get_monitor_channel(void);

/**
 * \brief Returns the channel to the nameserver.
 */
struct aos_rpc *aos_rpc_get_name_channel(void);

/**
 * \brief Returns the channel to the network server
 */
struct aos_rpc *aos_rpc_get_network_channel(void);

/**
 * \brief Returns the channel to inter-monitor communication
 *
 * Note: this is only allowed on monitors (init) domains
 */
struct aos_rpc *aos_rpc_get_intermon_channel(coreid_t core);

/**
 * \brief Returns the channel to the filesystem service
 */
struct aos_rpc *aos_rpc_get_filesystem_channel(void);

/**
 * \brief Returns the channel to the block device service
 */
struct aos_rpc *aos_rpc_get_blockdev_channel(void);

/**
 * \brief Returns the channel to the shell service with number enumerator
 *
 * note: _multi variant is used for the intermon server
 */
errval_t aos_rpc_get_shell_channel_multi(size_t enumerator, struct aos_rpc **ret_shell_rpc);
struct aos_rpc *aos_rpc_get_shell_channel(size_t enumerator);


#endif // _LIB_BARRELFISH_AOS_MESSAGES_H
