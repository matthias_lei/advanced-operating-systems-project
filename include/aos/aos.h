/**
 * \file
 * \brief Top-level header for convenient inclusion of standard
 * libbarrelfish headers.
 */

/*
 * Copyright (c) 2007, 2008, 2009, 2010, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#ifndef LIBBARRELFISH_BARRELFISH_H
#define LIBBARRELFISH_BARRELFISH_H

/* standard libc types and assert */
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

/* utility macros */
#include <bitmacros.h>

/* Barrelfish kernel interface definitions */
#include <errors/errno.h>
#include <barrelfish_kpi/types.h>
#include <barrelfish_kpi/capabilities.h>
#include <barrelfish_kpi/cpu.h> // XXX!?
#include <barrelfish_kpi/paging_arch.h>
#include <barrelfish_kpi/syscalls.h>
#include <barrelfish_kpi/init.h> // kernel-defined part of cspace
#include <barrelfish_kpi/registers_arch.h>
#include <barrelfish_kpi/dispatcher_handle.h>

/* libbarrelfish API */
#include <aos/types.h>
#include <aos/capabilities.h>
#include <aos/slab.h>
#include <aos/threads.h>
#include <aos/slot_alloc.h>
#include <aos/ram_alloc.h>
#include <aos/syscalls.h>
#include <aos/cspace.h>
#include <aos/domain.h>
#include <aos/debug.h>
#include <aos/static_assert.h>
#include <aos/paging.h>

// LMP headers
#include <aos/connect/lmp_chan.h>
#include <aos/connect/lmp_endpoints.h>

// added FLMP headers
#include <aos/connect/flmp_chan.h>

// added UMP headers
#include <aos/connect/ump_chan.h>
#include <aos/connect/ump_ringbuffer.h>

/* XXX: utility macros. not sure where to put these */

/* Duplicate memory */
static inline void * memdup(const void *ptr, size_t size) {
    void *res = malloc(size);
    assert(res);
    memcpy(res, ptr, size);
    return res;
}

/* XXX: glue junk for old IDC system, to be removed!! */

void messages_wait_and_handle_next(void);
void __attribute__((noreturn)) messages_handler_loop(void);


//--- extension: dynamic runtime configuration section ---------------------------------------------
// extended: morecore.c e.g. needs to know whether in init domain or not
// and I did not just want to make a static variable read/write global
// thus this getter, which is implemented in aos/init.c

bool is_init_domain(void);

bool config_get_spawn_load_symbols(void);
void config_set_spawn_load_symbols(bool value);

// used for Memory service Dionysos & Name service Gaia
bool is_bootstrapping_without_memory(void);
void set_bootstrapping_without_memory(void);

// improves usage of initially provided 1 MiB of RAM
// and reduces stress on memory service during initial moments
// of domain spawn / initialization

bool has_memory_service_connection(void);
void set_has_memory_service_connection(void);

bool is_memory_service_connected(void);
void set_is_memory_service_connected(void);

//--- services mode
// services can be launched in 3 different modes. Note: this decision can be made during
// runtime of init! This is interesting for comparison purposes

enum rpc_services_mode {
    RPC_SERVICES_MODE_UNKNOWN,
    RPC_SERVICES_MODE_MONOLITHIC,
    RPC_SERVICES_MODE_DISTRIBUTED
};

/**
 * note: this function can only be called within init process
 * it fails in other domains. But it must be called before
 * services are initialized.
 */
void set_rpc_services_mode(enum rpc_services_mode mode);

enum rpc_services_mode get_rpc_services_mode(void);


#endif
