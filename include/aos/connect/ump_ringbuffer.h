/**
 * Group C
 *
 * 15.11.2017
 *
 * This file contains the interface for ringbuffer functionality
 * for ump messages. As example served the endpoints in the lmp
 * in the lmp model given from the handout.
 * Note that the implementation does prevents the user to call
 * ump_ringbuffer_recv if is_sender is set to true.
 * The opposite applies if is_sender is false i.e., ump_ringbuffer_send
 * cannot be called.
 */


#ifndef INCLUDE_AOS_UMP_RINGBUFFER_H_
#define INCLUDE_AOS_UMP_RINGBUFFER_H_

#include <string.h>
#include <arch/arm/machine/atomic.h>
#include <aos/waitset_chan.h>

#define UMP_RB_CACHE_LINE_SIZE 32

// UMP_RB_INTERNAL_PAYLOAD_SIZE only exists in order to conform with the struct definition.
// The key idea behind is that we can use the first 3 bytes of the flags field.
#define UMP_RB_INTERNAL_PAYLOAD_SIZE (UMP_RB_CACHE_LINE_SIZE-sizeof(uintptr_t))
#define UMP_RB_PAYLOAD_SIZE (UMP_RB_CACHE_LINE_SIZE-sizeof(char))

#define UMP_RB_RECV_FLAG_BIT 24
#define UMP_RB_RECV_FLAG_MASK (1 << UMP_RB_RECV_FLAG_BIT)
#define UMP_RB_RECV_FLAG(flag) ((flag & UMP_RB_RECV_FLAG_MASK) >> UMP_RB_RECV_FLAG_BIT)


//--- one UMP ringbuffer corresponds to one LMP endpoint -------------------------------------------

//=== low level implementation

struct ump_ringbuffer_field {
    char payload[UMP_RB_INTERNAL_PAYLOAD_SIZE];
    uintptr_t flags;
};

struct ump_ringbuffer {
    //--- meta data similar to lmp_endpoint
    struct waitset_chanstate waitset_state;  // Waitset for receiving; must be first element in ringbuffer
    // NO separate poll list needed as in LMP
    // NO receive slot for UMP: capabilities are transferred between cores by a specific protocol

    //--- actual ringbuffer data
    struct ump_ringbuffer_field *buf;

    size_t length;

    // if is_sender is true idx takes the function of an producer idx
    // otherwise it is used as consumer idx
    bool is_sender;

    size_t idx;
};


errval_t ump_ringbuffer_init(struct ump_ringbuffer *rb, void *frame_ptr, size_t size, bool is_sender, size_t *ret_payload_size);

static inline bool ump_ringbuffer_can_recv(struct ump_ringbuffer *rb) {
    assert(rb);
    assert(!rb->is_sender);
    return UMP_RB_RECV_FLAG(rb->buf[rb->idx].flags) == 1;
}

static inline bool ump_ringbuffer_can_send(struct ump_ringbuffer *rb) {
    assert(rb);
    assert(rb->is_sender);
    return UMP_RB_RECV_FLAG(rb->buf[rb->idx].flags) == 0;
}

/**
 * \brief Send a message on the given UMP ringbuffer, if possible
 *
 * Non-blocking, may fail if there is no space in the in-buffer.
 *
 */
static inline errval_t ump_ringbuffer_send(struct ump_ringbuffer *rb, const void *buf, size_t size) {
    assert(rb);
    assert(rb->is_sender);
    assert(buf);
    assert(size <= UMP_RB_PAYLOAD_SIZE);

    if (!ump_ringbuffer_can_send(rb)) {
        return LIB_ERR_UMP_CHAN_FULL;
    }

    atomic_thread_fence_acq();

    const size_t current_idx = rb->idx;

    //copy content
    memcpy(rb->buf[current_idx].payload, buf, size);

    uintptr_t *const current_flag = &rb->buf[current_idx].flags;

    //set flag
    //this function has internal fences
    atomic_store_rel_32(current_flag, (*current_flag | UMP_RB_RECV_FLAG_MASK));

    //increase producer index
    rb->idx = (current_idx + 1) % rb->length;

    return SYS_ERR_OK;
}

/**
 * \brief Receive a message from an UMP ringbuffer, if possible
 *
 * Non-blocking. May fail if no message is available.
 */
static inline errval_t ump_ringbuffer_recv(struct ump_ringbuffer *rb, void *buf, size_t size) {
    assert(rb);
    assert(!rb->is_sender);
    assert(buf);
    assert(size <= UMP_RB_PAYLOAD_SIZE);

    if (!ump_ringbuffer_can_recv(rb)) {
        return LIB_ERR_NO_UMP_MSG;
    }

    atomic_thread_fence_acq();

    const size_t current_idx = rb->idx;

    //extract content
    memcpy(buf, rb->buf[current_idx].payload, size);

    uintptr_t *const current_flag = &rb->buf[current_idx].flags;

    //clear flag
    //this function has internal fences
    atomic_store_rel_32(current_flag, (*current_flag & ~UMP_RB_RECV_FLAG_MASK));

    rb->idx = (current_idx + 1) % rb->length;

    return SYS_ERR_OK;
}



//=== interface / implementation of additional functions / structs defined in lmp_endpoint
//    modifications made to adapt to UMP setting
//    note: no specific UMP message type used at the level of ump_ringbuffer or ump_chan
//    only buffers of max. payload size are used on this level.

/**
 * not implemented for UMP on this level. Transmission of capabilities over UMP,
 * which needs a monitor as recevier and is only allowed from a monitor as sender as well
 * per our protocol, is handled on a higher level than ump_ringbuffer or ump_chan
 */
//void ump_ringbuffer_set_recv_slot(struct ump_ringbuffer *rb, struct capref slot);


/**
 * This function is called from poll_channels_disabled() in waitset.c for each waiting channel
 * see contrast, where the sibling function is called from disp_run() in dispatch.c and iterates over all
 * registered LMP channels at once.
 *
 * Due to subtle differences between UMP and LMP, it's better to poll for UMP directly in the waitset
 * (see also preparations for that there).
 * - LMP actually uses a multi layer system between receiving the upcall from kernel, copying the
 *   payload int a small ringbuffer, then poll on this ringbuffer, and finally trigger the event.
 * - UMP does not receive an upcall upon incoming messages, and the information is written directly
 *   in the the ringbuffer that needs active polling to find the next incoming message. Thus,
 *   a call once every disp_run() at the start of the thread timeslice (preempted at 20 ms) would
 *   lead to latence times that are way too long. In contrast, poll_channels_disabled() is called
 *   from various locations much more frequently and at least just in time before the waitset would
 *   be considered empty while still messages in the buffer.
 *
 * Note: UMP waitset chans could even be registered to be persistent (also prepared).
 * However, we do not use that feature at the moment to remain max. symmetric with the LMP channels
 * that need manual re-registration.
 *
 * Please feel free to ask me, if you have questions about these details. pisch
 *
 * note: internally, ws_chan is casted into an ump_ringbuffer
 */
bool ump_ringbuffer_poll_disabled(struct waitset_chanstate *ws_chan, dispatcher_handle_t handle);

errval_t ump_ringbuffer_register(struct ump_ringbuffer *rb, struct waitset *ws,
                                 struct event_closure closure);

static inline errval_t ump_ringbuffer_deregister(struct ump_ringbuffer *rb)
{
    assert(rb);
    return waitset_chan_deregister(&rb->waitset_state);
}


static inline void ump_ringbuffer_migrate(struct ump_ringbuffer *rb, struct waitset *ws)
{
    assert(rb);
    assert(ws);
    waitset_chan_migrate(&rb->waitset_state, ws);
}

/* // not (yet) implemented LMP sibling functions
errval_t lmp_endpoint_alloc(size_t buflen, struct lmp_endpoint **retep);
void lmp_endpoint_free(struct lmp_endpoint *ep);
errval_t lmp_endpoint_create_in_slot(size_t buflen, struct capref dest,
                                     struct lmp_endpoint **retep);
void lmp_endpoint_store_lrpc_disabled(struct lmp_endpoint *ep, uint32_t bufpos,
                                      uintptr_t arg1, uintptr_t arg2,
                                      uintptr_t arg3, uintptr_t arg4);
*/

#endif // INCLUDE_AOS_UMP_RINGBUFFER_H_
