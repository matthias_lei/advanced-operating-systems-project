/**
 * Provides UMP specific messaging functions.
 * AOS class 2017, group C
 */

#ifndef INCLUDE_AOS_CONNECT_UMP_H_
#define INCLUDE_AOS_CONNECT_UMP_H_

#include <aos/aos.h>

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct ump_service_create_data {
    struct frame_identity info;
    void *ump_chan_buf;
    struct ump_chan *ump_chan;
    struct aos_rpc_channel *rpc_chan;
    struct aos_rpc_server_comm_state *init_handler_state;
};

/**
 * This part is split into 2 parts that one can be run before returning from the RPC call
 * and one happens afterwards. This reduces the risk of failure. All could also be run
 * after returning the info for the frame_cap to the caller.
 */

// note: the frame_cap must have been already allocated before using this function.
// this is needed because the one frame is used for both sides, and these functions
// want to leave the choice to the user where the frame is initially allocated and
// where it is allocated and where it is forged.
// Actual mapping of the frame is then performed in these functions here.
errval_t setup_ump_service_part1(struct capref frame_cap, struct ump_service_create_data *data);
errval_t setup_ump_service_part2(struct ump_service_create_data *data, struct event_closure event_handler);

errval_t setup_ump_client(struct capref frame_cap,
                          enum aos_rpc_interface interface, void *interface_vtable,
                          routing_info_t routing, struct aos_rpc **ret_rpc);

// these helpers are used during core spawn
errval_t setup_ump_service_part1_from_buffer(void *buf, size_t buf_size,
                                             struct ump_service_create_data *data);
// _part2 is the regular one

errval_t setup_ump_client_from_buffer(void *buf, size_t buf_size,
                                      enum aos_rpc_interface interface, void *interface_vtable,
                                      routing_info_t routing, struct aos_rpc **ret_rpc);

#endif // INCLUDE_AOS_CONNECT_UMP_H_
