/**
 * \file
 * \brief Bidirectional LMP channel
 */

/*
 * Copyright (c) 2009, 2010, 2011, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#ifndef BARRELFISH_FLMP_CHAN_H
#define BARRELFISH_FLMP_CHAN_H

#include <sys/cdefs.h>

#include <aos/waitset.h>
#include <aos/connect/lmp_chan.h>
#include <aos/connect/lmp_endpoints.h>
#include <aos/connect/ump_ringbuffer.h>
#include <assert.h>

__BEGIN_DECLS

/**
 * Implementation of fast LMP message passing driver.
 *
 * This is basically just a thin wrapper of lmp_chan with two additional
 * ringbuffers that are used for sending and receiving large messages, such
 * that the many and expensive system calls and context switches involved
 * with fragmenting large messages over an lmp_chan can be significantly
 * reduced.
 */

struct flmp_chan;
struct event_queue_node;

/// A bidirectional LMP channel with ringbuffer support for large messages
struct flmp_chan {
    struct lmp_chan lmp_chan;       //< underlying LMP channel
    struct ump_ringbuffer send_rb;  //< send ringbuffer
    struct ump_ringbuffer recv_rb;  //< receive ringbuffer
    size_t max_payload_size;        //< maximal payload size for a ringbuffer message
};

errval_t flmp_chan_connect(struct flmp_chan *flc, void *frame_ptr, size_t size, bool as_client);

/**
 * \brief Initialise a new FLMP channel
 *
 * \param flc  Storage for channel state
 */
static inline void flmp_chan_init(struct flmp_chan *flc)
{
    lmp_chan_init(&flc->lmp_chan);
}

/// Destroy the local state associated with a given channel
static inline void flmp_chan_destroy(struct flmp_chan *flc)
{
    lmp_chan_destroy(&flc->lmp_chan);
}

/**
 * \brief Initialise a new FLMP channel to accept an incoming binding request
 *
 * \param flc  Storage for channel state
 * \param buflen_words Size of incoming buffer, in words
 * \param endpoint Capability to remote LMP endpoint
 */
static inline errval_t flmp_chan_accept(struct flmp_chan *flc,
                         size_t buflen_words, struct capref endpoint)
{
    return lmp_chan_accept(&flc->lmp_chan, buflen_words, endpoint);
}

/**
 * \brief Register an event handler to be notified when messages can be sent
 *
 * In the future, call the closure on the given waitset when it is likely that
 * a message can be sent on the channel. A channel may only be registered
 * with a single send event handler on a single waitset at any one time.
 *
 * \param flc FLMP channel
 * \param ws Waitset
 * \param closure Event handler
 */
static inline errval_t flmp_chan_register_send(struct flmp_chan *flc, struct waitset *ws,
                                 struct event_closure closure)
{
    return lmp_chan_register_send(&flc->lmp_chan, ws, closure);
}

/**
 * \brief Cancel an event registration made with flmp_chan_register_send()
 *
 * \param flc FLMP channel
 */
static inline errval_t flmp_chan_deregister_send(struct flmp_chan *flc)
{
    return lmp_chan_deregister_send(&flc->lmp_chan);
}

/**
 * \brief Migrate an event registration to a new waitset.
 *
 * \param flc FLMP channel
 * \param ws New waitset to migrate to
 */
static inline void flmp_chan_migrate_send(struct flmp_chan *flc, struct waitset *ws)
{
    lmp_chan_migrate_send(&flc->lmp_chan, ws);
}

/**
 * \brief Allocate a new receive capability slot for an FLMP channel
 *
 * This utility function allocates a new receive slot (using #slot_alloc)
 * and sets it on the channel (using #lmp_chan_set_recv_slot).
 *
 * \param flc FLMP channel
 */
static inline errval_t flmp_chan_alloc_recv_slot(struct flmp_chan *flc)
{
    return lmp_chan_alloc_recv_slot(&flc->lmp_chan);
}

/**
 * \brief Register an event handler to be notified when messages can be received
 *
 * In the future, call the closure on the given waitset when it is likely that
 * a message can be received on the channel. A channel may only be registered
 * with a single receive event handler on a single waitset at any one time.
 *
 * \param flc FLMP channel
 * \param ws Waitset
 * \param closure Event handler
 */
static inline errval_t flmp_chan_register_recv(struct flmp_chan *flc,
                                               struct waitset *ws,
                                               struct event_closure closure)
{
    return lmp_chan_register_recv(&flc->lmp_chan, ws, closure);
}

/**
 * \brief Cancel an event registration made with lmp_chan_register_recv()
 *
 * \param flc FLMP channel
 */
static inline errval_t flmp_chan_deregister_recv(struct flmp_chan *flc)
{
    return lmp_chan_deregister_recv(&flc->lmp_chan);
}

/**
 * \brief Migrate an event registration made with
 * lmp_chan_register_recv() to a new waitset
 *
 * \param flc FLMP channel
 * \param ws New waitset
 */
static inline void flmp_chan_migrate_recv(struct flmp_chan *flc,
                                          struct waitset *ws)
{
    lmp_chan_migrate_recv(&flc->lmp_chan, ws);
}

/**
 * \brief Receive a message from an LMP channel, if possible
 *
 * Non-blocking. May fail if no message is available.
 *
 * \param flc  FLMP channel
 * \param msg LMP message buffer, to be filled-in
 * \param cap If non-NULL, filled-in with location of received capability, if any
 */
static inline errval_t flmp_chan_recv_short(struct flmp_chan *flc,
                                            struct lmp_recv_msg *msg,
                                            struct capref *cap)
{
    return lmp_chan_recv(&flc->lmp_chan, msg, cap);
}

static inline errval_t flmp_chan_recv_long(struct flmp_chan *flc, void *buf, size_t size)
{
    return ump_ringbuffer_recv(&flc->recv_rb, buf, size);
}

/**
 * \brief Check if a channel has data to receive
 */

static inline bool flmp_chan_can_recv_short(struct flmp_chan *flc)
{
    return lmp_chan_can_recv(&flc->lmp_chan);
}

static inline bool flmp_chan_can_recv_long(struct flmp_chan *flc)
{
    return ump_ringbuffer_can_recv(&flc->recv_rb);
}

/**
 * \brief Set the receive capability slot for an LMP channel
 *
 * \param flc FLMP channel
 * \param slot Receive slot
 */
static inline void flmp_chan_set_recv_slot(struct flmp_chan *flc,
                                           struct capref slot)
{
    lmp_chan_set_recv_slot(&flc->lmp_chan, slot);
}

/**
 * \brief Is the given error value a transient FLMP error
 *
 * Returns true iff the given error code indicates a transient
 * LMP send error condition that may succeed on a subsequent retry.
 */
static inline bool flmp_err_is_transient(errval_t err)
{
    return lmp_err_is_transient(err);
}


/**
 * \brief Get a receiving chanstate of FLMP channel
 */
static inline struct waitset_chanstate * flmp_chan_get_receiving_channel(struct flmp_chan *flc)
{
    return lmp_chan_get_receiving_channel(&flc->lmp_chan);
}

static inline errval_t flmp_chan_send_short(struct flmp_chan *flc, lmp_send_flags_t flags,
                                            struct capref send_cap, const void *buf, size_t size)
{
    uintptr_t payload[LMP_MSG_LENGTH];

    assert(size <= sizeof(payload));

    memset(payload, 0, sizeof(payload));
    memcpy(payload, buf, size);

    return lmp_chan_send9(&flc->lmp_chan, flags, send_cap,
                          payload[0], payload[1], payload[2],
                          payload[3], payload[4], payload[5],
                          payload[6], payload[7], payload[8]);
}

static inline bool flmp_chan_can_send_long(struct flmp_chan *flc)
{
    return ump_ringbuffer_can_send(&flc->send_rb);
}

static inline errval_t flmp_chan_send_long(struct flmp_chan *flc, const void *buf, size_t size)
{
    return ump_ringbuffer_send(&flc->send_rb, buf, size);
}

__END_DECLS

#endif // BARRELFISH_LMP_CHAN_H
