/**
 * Provides LMP specific messaging functions.
 * AOS class 2017, group C
 */

#ifndef INCLUDE_AOS_CONNECT_LMP_H_
#define INCLUDE_AOS_CONNECT_LMP_H_

#include <aos/aos.h>

#include <aos/rpc_shared/aos_rpc_shared_common.h>

struct spawninfo;

// the freshly crated endpoint will be stored in the slot referenced by endpoint_cap
errval_t setup_lmp_service(struct capref endpoint_cap, struct event_closure event_handler);

errval_t setup_lmp_client(struct capref remote_cap,
                          enum aos_rpc_interface interface, void *interface_vtable,
                          routing_info_t routing, struct aos_rpc **ret_rpc);

#endif // INCLUDE_AOS_CONNECT_LMP_H_
