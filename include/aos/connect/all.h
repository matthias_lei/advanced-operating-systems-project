/**
 * Provides UMP specific messaging functions.
 * AOS class 2017, group C
 */

#ifndef INCLUDE_AOS_CONNECT_ALL_H_
#define INCLUDE_AOS_CONNECT_ALL_H_

#include <aos/connect/flmp.h>
#include <aos/connect/lmp.h>
#include <aos/connect/ump.h>

#endif // INCLUDE_AOS_CONNECT_ALL_H_
