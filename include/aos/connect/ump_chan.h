/**
 * UMP Version of the LMP chan implementation.
 * Also here, it implements a bidirectional communication channel.
 *
 * In the current setting, it is designed to offer one client<->server connection,
 * i.e. one side is a client and the other a server. If both domains shall offer client and server
 * functionalities with another domain, then 2 UMP channels need to be created.
 *
 * note: then one of them must initialize the 2 channels as 1) client 2) server,
 * and the other as 1) server 2) client. See the handshake during the init.
 *
 * Function interfaces and data structs are modeled with high similarity to LMP chan implementation
 * on purpose to allow a shared abstraction layer on top of it. However, some differences are there
 * due to the nature of UMP that does not directly allow transmission of capabilities.
 * -> see implemented protocol for that on higher levels of our messaging stack
 *
 * Group C
 * version 2017-11-21
 */

#ifndef INCLUDE_AOS_UMP_CHAN_H_
#define INCLUDE_AOS_UMP_CHAN_H_

#include <assert.h>
#include <sys/cdefs.h>

#include <aos/debug.h>
#include <aos/connect/ump_ringbuffer.h>
#include <aos/waitset.h>


//--- A bidirectional UMP channel
struct ump_chan {
    // No registration for auto-resend for UMP at the moment:
    // -> no waitset_chanstate and no next/prev in list of channels with send events

    struct ump_ringbuffer send_rb;
    struct ump_ringbuffer recv_rb;

    size_t max_payload_size;

    // connection state (currently inherited from LMP; modifications may follow)
    enum {UMP_DISCONNECTED,     // Disconnected (used)
          UMP_BIND_WAIT,        // Waiting for bind reply (used)
          UMP_MONITOR_ACCEPT,   // Special case for monitor binding: waiting for cap; not sure whether this makes sense in UMP
          UMP_CONNECTED,        // Connection established (used)
    } connstate;
};

/**
 * note: init method has a different signature than lmp sibling function
 *
 * \param  as_client: is used to connect the ringbuffers properly; false indicates as_server
 *
 * the provided frame will be split into 2 buffers of equal size.
 * each will be initialized as an ump_ringbuffer, one for sending and one for receiving.
 * note: the ringbuffer checks and adjusts for specifically needed alignments to work properly.
 */
errval_t ump_chan_init(struct ump_chan *uc, void *frame_ptr, size_t size, bool as_client);


/**
 * Currently: unregisters potentially registered closures (can only have 1 send and 1 recv at max)
 * Add more content if needed
 */
void ump_chan_destroy(struct ump_chan *uc);


/**
 * comment from LMP, modified
 * \brief Register an event handler to be notified when messages can be received
 *
 * In the future, call the closure on the given waitset when it is likely that
 * a message can be received on the channel. A channel may only be registered
 * with a single receive event handler on a single waitset at any one time.
 *
 * \param uc UMP channel
 * \param ws Waitset
 * \param closure Event handler
 */
static inline errval_t ump_chan_register_recv(struct ump_chan *uc,
                                              struct waitset *ws,
                                              struct event_closure closure)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);
    assert(ws);

    return ump_ringbuffer_register(&uc->recv_rb, ws, closure);
}

/**
 * \brief Cancel an event registration made with ump_chan_register_recv()
 *
 * \param uc UMP channel
 */
static inline errval_t ump_chan_deregister_recv(struct ump_chan *uc)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);

    return ump_ringbuffer_deregister(&uc->recv_rb);
}

/**
 * \brief Migrate an event registration made with
 * ump_chan_register_recv() to a new waitset
 *
 * \param uc UMP channel
 * \param ws New waitset
 */
static inline void ump_chan_migrate_recv(struct ump_chan *uc, struct waitset *ws)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);
    assert(ws);

    ump_ringbuffer_migrate(&uc->recv_rb, ws);
}

/**
 * modified in comparison with LMP
 *
 * \brief Receive a message from an UMP channel, if possible
 *
 * Non-blocking. May fail if no message is available.
 *
 * \param uc    UMP channel
 * \param buf   message buffer, to be filled-in
 * \param size  size to read
 *              note: must be in range [0, max_payload_size]
 *
 * note: no capref in signature
 */
static inline errval_t ump_chan_recv(struct ump_chan *uc, void *buf, size_t size)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);
    assert(buf);

    assert(size <= uc->max_payload_size);
    return ump_ringbuffer_recv(&uc->recv_rb, buf, size);
}

/**
 * \brief Check if a channel has data to receive
 */
static inline bool ump_chan_can_recv(struct ump_chan *uc)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);

    return ump_ringbuffer_can_recv(&uc->recv_rb);
}


/**
 * \brief Is the given error value a transient UMP error
 *
 * Returns true iff the given error code indicates a transient
 * UMP send error condition that may succeed on a subsequent retry.
 */
static inline bool ump_err_is_transient(errval_t err)
{
    enum err_code ec = err_no(err);
    return ec == LIB_ERR_UMP_CHAN_FULL;
}


/**
 * \brief Get a receiving chanstate of UMP channel
 *
 * this function currently assumes the server side
 */
static inline struct waitset_chanstate *ump_chan_get_receiving_channel(struct ump_chan *uc)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);

    return &uc->recv_rb.waitset_state;
}

/**
 * from lmp_chan_arch.h -> only one send method here instead of one for each number of registers;
 *
 * This function is non-blocking. It fails, if the buffer is full and the message cannot be sent.
 *
 * \param uc    UMP channel
 * \param buf   message buffer with the payload
 * \param size  size to send
 *              note: must be in range [0, max_payload_size]
 */
static inline errval_t ump_chan_send(struct ump_chan *uc, const void *buf, size_t size)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);
    assert(buf);

    assert(size <= uc->max_payload_size);
    return ump_ringbuffer_send(&uc->send_rb, buf, size);
}


/**
 * \brief Check if a channel can send
 */
static inline bool ump_chan_can_send(struct ump_chan *uc)
{
    assert(uc);
    assert(uc->connstate != UMP_DISCONNECTED);

    return ump_ringbuffer_can_send(&uc->send_rb);
}


//--- currently not implemented functions ----------------------------------------------------------

/**
 * these 4 functions are not used at the moment, but may be useful in the future.
 * They will be implemented then.
 */
//errval_t ump_chan_register_send(struct ump_chan *uc, struct waitset *ws,
//                                struct event_closure closure);
//errval_t ump_chan_deregister_send(struct ump_chan *uc);
//void ump_chan_migrate_send(struct ump_chan *uc, struct waitset *ws);
//void ump_channels_retry_send_disabled(dispatcher_handle_t handle);

/**
 * This function is not needed for UMP
 * also changed signature compared to lmp_sibling:
 * - buflen_words is set from ringbuffer (not needed here)
 * - no endpoint capref needed
 */
//errval_t ump_chan_accept(struct ump_chan *uc);

// not possible in UMP: see higher protocol layers for capabilities transfer over UMP
//errval_t ump_chan_alloc_recv_slot(struct ump_chan *uc);

// not usable function in the setting of UMP. See cap transfer protocol that is implemented
// on higher layer of our message passing stack.
//static inline void ump_chan_set_recv_slot(struct ump_chan *uc, struct capref slot)

#endif // INCLUDE_AOS_UMP_CHAN_H_
