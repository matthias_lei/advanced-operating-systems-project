/**
 * \file
 * \brief Barrelfish paging helpers.
 */

/*
 * Copyright (c) 2012, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */


#ifndef LIBBARRELFISH_PAGING_H
#define LIBBARRELFISH_PAGING_H

#include <errors/errno.h>
#include <aos/capabilities.h>
#include <aos/slab.h>
#include <barrelfish_kpi/paging_arm_v7.h>

#include <arch/arm/machine/atomic.h>

typedef int paging_flags_t;

#define VADDR_OFFSET ((lvaddr_t)1UL*1024*1024*1024) // 1GB

#define PAGING_SLAB_BUFSIZE 12

#define VREGION_FLAGS_NORIGHTS 0x00 // Nothing allowed
#define VREGION_FLAGS_READ     0x01 // Reading allowed
#define VREGION_FLAGS_WRITE    0x02 // Writing allowed
#define VREGION_FLAGS_EXECUTE  0x04 // Execute allowed
#define VREGION_FLAGS_NOCACHE  0x08 // Caching disabled
#define VREGION_FLAGS_MPB      0x10 // Message passing buffer
#define VREGION_FLAGS_GUARD    0x20 // Guard page
#define VREGION_FLAGS_MASK     0x2f // Mask of all individual VREGION_FLAGS

#define VREGION_FLAGS_READ_WRITE \
    (VREGION_FLAGS_READ | VREGION_FLAGS_WRITE)
#define VREGION_FLAGS_READ_EXECUTE \
    (VREGION_FLAGS_READ | VREGION_FLAGS_EXECUTE)
#define VREGION_FLAGS_READ_WRITE_NOCACHE \
    (VREGION_FLAGS_READ | VREGION_FLAGS_WRITE | VREGION_FLAGS_NOCACHE)
#define VREGION_FLAGS_READ_WRITE_MPB \
    (VREGION_FLAGS_READ | VREGION_FLAGS_WRITE | VREGION_FLAGS_MPB)

struct paging_l2pt_entry {
    struct capref cap;
    struct capref invokable;
    struct capref mapping;
    bool mapped;
};


//--- some key constants for our implementation ----------------------------------------------------

// for all programs
// Addresses in [0, 2[ GiB are managed by paging.c and *tracked* by the BITMAP
// - Addresses in [0, 1[ GiB are *reserved* for mapping at specific addresses during spawn
// - Addresses in [1, 2[ GiB are *managed* by paging.c (including regions)
// Addresses in [2, 4[ are reserved to the kernel and not available for mapping for us
#define PAGING_RESERVED VADDR_OFFSET
#define PAGING_MANAGED_MAX_SIZE 0x40000000

// end is non-inclusive
#define PAGING_END      (VADDR_OFFSET + PAGING_MANAGED_MAX_SIZE)

STATIC_ASSERT(!(PAGING_RESERVED & BASE_PAGE_MASK), "");
STATIC_ASSERT(!(PAGING_MANAGED_MAX_SIZE & BASE_PAGE_MASK), "");
STATIC_ASSERT(!(PAGING_END & BASE_PAGE_MASK), "");

// guards in regions: bottom is a must; top is optional (thus == 0 at the moment)
#define BOTTOM_GUARD_SIZE BASE_PAGE_SIZE
#define TOP_GUARD_SIZE 0

STATIC_ASSERT(!(BOTTOM_GUARD_SIZE & BASE_PAGE_MASK), "");
STATIC_ASSERT(!(TOP_GUARD_SIZE & BASE_PAGE_MASK), "");

//--- helper macros for the BITMAP used to track mapped pages --------------------------------------

// note: the bitmap is an uint32_t[] of appropriate size to have 1 bit per page (4 KiB at the moment)
// for tracking. To avoid any runtime allocations for variable size bitmaps / region, one global bitmap
// is used for all regions -- and also for all other map allocations.
// Each region tracks a unique partition of this bitmap.
//
// hardware specific atomic operations are used to achieve save bitmap manipulations even in the
// multi-threaded setting of this paging operation
//
// note: while macros are defined to preselect the 32 bit versions of the atomic operations,
// the operations are used in hardcoded manner to assure matching data types and functions.

// they need to go together
typedef uint32_t bitmap_t;
#define BITMAP_TYPE_SIZE 32
#define BITMAP_TYPE_BITS 5

#define BITMAP_SIZE (PAGING_END >> (BASE_PAGE_BITS + BITMAP_TYPE_BITS))

// get the round_down address with alignment 4 KiB
#define ALIGNED_BASE_ADDRESS(address) ROUND_DOWN(address, BASE_PAGE_SIZE)

#define ADDRESS_TO_INDEX(address) ( (address) >> (BASE_PAGE_BITS + BITMAP_TYPE_BITS) )

// bit mask is one bit set within bits 0 to 31 of an uint32_t
#define ADDRESS_TO_BITMASK(address) BIT_T(bitmap_t, ((address) >> BASE_PAGE_BITS) & (MASK(BITMAP_TYPE_BITS)) )

static inline bool check_bitmap(volatile bitmap_t *bitmap, lvaddr_t vaddr) {
    size_t index = ADDRESS_TO_INDEX(vaddr);
    assert(index < BITMAP_SIZE);
    bitmap_t bitmask = ADDRESS_TO_BITMASK(vaddr);

    bitmap_t data = atomic_load_32(&bitmap[index]);
    return !!(data & bitmask);
}

static inline void set_bitmap(volatile bitmap_t *bitmap, lvaddr_t vaddr) {
    size_t index = ADDRESS_TO_INDEX(vaddr);
    assert(index < BITMAP_SIZE);
    bitmap_t bitmask = ADDRESS_TO_BITMASK(vaddr);

    atomic_set_32(&bitmap[index], bitmask);
}

static inline void clear_bitmap(volatile bitmap_t *bitmap, lvaddr_t vaddr) {
    size_t index = ADDRESS_TO_INDEX(vaddr);
    assert(index < BITMAP_SIZE);
    bitmap_t bitmask = ADDRESS_TO_BITMASK(vaddr);

    atomic_clear_32(&bitmap[index], bitmask);
}


struct paging_region_list {
    struct thread_mutex mutex;
    struct paging_region *head;
    size_t size;
};

struct paging_elf_key_data {
    // note: it's OK to serialize / deserialize lvaddr pointers in the VSpace

    // actual base with 2 segments loaded
    // - LOAD segment 01 with R/X rights and these sections: .text .rodata .ARM.exidx
    // - LOAD segment 02 with R/W rights and these sections: .fini_array .ctors .data.rel.ro .got .data .bss
    lvaddr_t base;
    size_t size;

    // additional metadata (e.g. for runtime lookup of symbol names)
    // .symtab and .strtab
    // they are loaded by our extension into read-only pages at the moment.
    // This may be changed into R/W if adjustments are needed due to code relocations.
    lvaddr_t symtab_base;
    size_t symtab_size;

    lvaddr_t strtab_base;
    size_t strtab_size;

    // more may come in context with relocation
};


// struct to store the paging status of a process
struct paging_state {
    struct thread_mutex mutex;

    struct slot_allocator* slot_alloc;

    //location of the level 1 page table capability
    struct capref l1_pagetable;

    // DONE: add struct members to keep track of the page tables etc
    struct paging_l2pt_entry l2_pts[ARM_L1_MAX_ENTRIES];
    struct paging_l2pt_entry l2_pts_reserve;

    lvaddr_t vspace_base_addr;
    // for now implemented just contigiously growing VA allocation
    lvaddr_t allocated_region_end;

    // paging regions
    struct paging_region_list paging_regions;

    // tracks all mapped pages of the entire system
    // needs 64 KiB of space.
    bitmap_t volatile mapped_pages_bitmap[BITMAP_SIZE];
    size_t mapped_pages_count;
    size_t allocated_in_reserved_space; // below 1 GiB limit

    struct paging_elf_key_data elf_info;

    bool bootstrap_mode;
};


struct thread;
/// Initialize paging_state struct
errval_t paging_init_state(struct paging_state *st, lvaddr_t start_vaddr,
        struct capref pdir, struct slot_allocator * ca);
/// initialize self-paging module
errval_t paging_init(void);
/// setup paging on new thread (used for user-level threads)
void paging_init_onthread(struct thread *t);

errval_t paging_state_serialize(struct paging_state *st, char *buf, size_t buflen, size_t *size);

errval_t paging_state_deserialize(struct paging_state *st, char *buf, size_t buflen);

struct paging_region {
    struct paging_region *prev;
    struct paging_region *next;
    struct paging_state *state;
    lvaddr_t base_addr;
    size_t region_size;

    // regions allow having one or several guard pages at the bottom of their address space
    lvaddr_t begin_available_addr; // inclusive
    lvaddr_t end_available_addr;   // exclusive (simplifies some testing)
                                   // and also prepars for optional additional guard pages at the top of the region
    size_t available_size;
    lvaddr_t current_addr;         // is in range [start_available_addr, stop_available_addr[

    // TODO: if needed add struct members for tracking free / allocated state
    // for unmapping
    // partially done (for bitmap)

    // bitmap stuff
    // currently in global bitmap
};

errval_t paging_region_init(struct paging_state *st,
                            struct paging_region *pr, size_t size);

/**
 * \brief return a pointer to a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 */
errval_t paging_region_map(struct paging_region *pr, size_t req_size,
                           void **retbuf, size_t *ret_size);
/**
 * \brief free a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 * We ignore unmap requests right now.
 */
errval_t paging_region_unmap(struct paging_region *pr, lvaddr_t base, size_t bytes);

/**
 * \brief Find a bit of free virtual address space that is large enough to
 *        accomodate a buffer of size `bytes`.
 */
errval_t paging_alloc(struct paging_state *st, void **buf, size_t bytes);

/**
 * Functions to map a user provided frame.
 */
/// Map user provided frame with given flags while allocating VA space for it
errval_t paging_map_frame_attr(struct paging_state *st, void **buf,
                               size_t bytes, struct capref frame,
                               int flags, void *arg1, void *arg2);
/// Map user provided frame at user provided VA with given flags.
errval_t paging_map_fixed_attr(struct paging_state *st, lvaddr_t vaddr,
                               struct capref frame, size_t bytes, int flags);

/**
 * refill slab allocator without causing a page fault
 */
errval_t slab_refill_no_pagefault(struct slab_allocator *slabs,
                                  struct capref frame, size_t minbytes);

/**
 * \brief unmap region starting at address `region`.
 * NOTE: this function is currently here to make libbarrelfish compile. As
 * noted on paging_region_unmap we ignore unmap requests right now.
 */
errval_t paging_unmap(struct paging_state *st, const void *region);


/// Map user provided frame while allocating VA space for it
static inline errval_t paging_map_frame(struct paging_state *st, void **buf,
                                        size_t bytes, struct capref frame,
                                        void *arg1, void *arg2)
{
    return paging_map_frame_attr(st, buf, bytes, frame,
            VREGION_FLAGS_READ_WRITE, arg1, arg2);
}

/// Map user provided frame at user provided VA.
static inline errval_t paging_map_fixed(struct paging_state *st, lvaddr_t vaddr,
                                        struct capref frame, size_t bytes)
{
    return paging_map_fixed_attr(st, vaddr, frame, bytes,
            VREGION_FLAGS_READ_WRITE);
}

static inline lvaddr_t paging_genvaddr_to_lvaddr(genvaddr_t genvaddr) {
    return (lvaddr_t) genvaddr;
}



//--- extended public interface --------------------------------------------------------------------

/**
 * uses debug_printf() to print some key information of the paging state.
 */
void paging_print_state(struct paging_state *st);

#endif // LIBBARRELFISH_PAGING_H
