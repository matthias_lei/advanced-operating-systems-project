/**
 * The actual functionality of the Nameserver UI client is implemented in this library
 * to make it easily available as built-in command in the shell Zeus, and as a standalone
 * program apollo.
 *
 * Group C, individual projects: Nameserver
 *
 * version 2017-12-20, Pirmin Schmid
 */

#ifndef INCLUDE_NAMESERVER_NAMESERVER_UI_CLIENT_H_
#define INCLUDE_NAMESERVER_NAMESERVER_UI_CLIENT_H_

#include <stdio.h>

int nameserver_ui_client(int argc, char **argv, FILE *std_in, FILE *std_out);

#endif // INCLUDE_NAMESERVER_NAMESERVER_UI_CLIENT_H_
