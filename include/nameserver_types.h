/**
 * AOS Class 2017 Group C -- Individual Project: Nameserver Gaia
 *
 * This library implements a system wide nameserver for our Barrelfish OlympOS project.
 * It is mainly for services at the moment, but can also be used for other types of resources.
 *
 * Here, the types are defined that are also used e.g. in the RPC header file.
 *
 * RE nameserver: see nameserver.h for details.
 *
 * version 2017-11-30, Pirmin Schmid
 */

#ifndef INCLUDE_NAMESERVER_TYPES_H_
#define INCLUDE_NAMESERVER_TYPES_H_

#include <hashtable/serializable_key_value_store.h>

#include <aos/rpc_shared/aos_rpc_shared_common.h>

//=== public interface =============================================================================

// in seconds
#define NAMESERVER_AUTO_PING_PERIOD 60
#define NAMESERVER_CHECK_PERIOD 128

typedef uint64_t service_handle_t;

typedef uint32_t routing_info_t;

// serialization note: all non-pointer fields must be at the beginning
// contact_cap is separate for technical reasons (see LMP/UMP messaging and capref transfer)
struct service_registration_request {
    coreid_t core_id;
    enum aos_rpc_interface interface;
    enum aos_rpc_chan_driver contact_chan_type;
    bool high_bandwidth_service; // helps making decision for LMP or FLMP upon binding

    char *abs_path;
    char *short_name_prefix;
    struct serializable_key_value_store *kv_store;
};

// serialization note: all non-pointer fields must be at the beginning
struct service_registration_reply {
    service_handle_t handle;
    uint64_t registration_key;
    size_t enumeration;

    char *name;
    char *short_name;
};

#endif // INCLUDE_NAMESERVER_TYPES_H_
