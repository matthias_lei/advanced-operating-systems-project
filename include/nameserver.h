/**
 * AOS Class 2017 Group C -- Individual Project: Nameserver Gaia
 *
 * This library implements a system wide nameserver for our Barrelfish OlympOS project.
 * It is mainly for services at the moment, but can also be used for other types of resources.
 *
 * It offers a key-value store for each of the registered entries that can be used for
 * filtering / lookup. It offers a auto-ping() method for services that assures the
 * database to be uptodate. It can establish an RPC channel to any of the registered
 * resources using a proper messaging layer (LMP/UMP) including capability transfer
 * over the monitors to establish the connection.
 *
 * This library provides the actual implementations of the functionality of this service
 * that is linked to the gaia server program that offers this service.
 * See also the wrapper functions in our client / server RPC collection for the
 * actual aos_rpc_ functions that will be used by actual clients of this library
 * to have full consistency with the rest of the project.
 *
 * PLEASE NOTE: Not all functionality is available from the beginning.
 * Simple registration, lookup and bind functions will be the first to be implemented
 * Advanced lookup (key/value filters) will follow later
 *
 *
 * Namespace
 * ---------
 * All entries in the name space must adhere to the following rules:
 * - all paths are absolute (starting with /)
 * - all paths conform to this scheme:
 *   /<class>/<type>/<subtype>/<enumeration>
 * - the characters of all <class>, <type>, <subtype> parts must be in {'a'..'z', '0'..'9', '_'},
 *   TODO discuss: <subtype> may be empty
 * - <enumeration> string representation of a valid size_t value and is assigned by the
 *   name server upon registration
 * - the name of a service is the full path
 *
 * Short name
 * ----------
 * Optional, a short name can be used for each service. It must adhere to the following rules:
 * - all short names conform to this scheme:
 *   <prefix><enumeration>
 * - <prefix> is submitted during registration and basically is a shortcut for
 *   /<class>/<type>/<subtype>/
 * - <enumeration> is added during registration (identical to <enumeration> used in name above)
 * - the characters of <prefix> must be in {'a'..'z'}
 * NOTE: registration is not possible if another service is already registered with the
 * same short name.
 *
 * Key-value store
 * ---------------
 * - Mainly used for arbitrary meta data that can be stored and retrieved with each entry.
 * - ALLOWED: Keys must be in {'a'..'z', 0'..'9', '_'}
 * - All keywords listed below are forbidden for use when setting the key value store for an
 *   entry. They are set upon registration based on the name information as provided.
 *   RESERVED: class, type, subtype, enumeration, name, short_name
 * - The reserved keywords can be used for lookups, of course.
 * - Values are strings at the moment; if there is a need for structs; a typed version will be introduced
 *
 * Functionality
 * -------------
 * for services
 * - register()   -> called by services to register themselves on the nameserver
 * - deregister() -> a service can deregister itself if it is stopped
 * - ping()       -> each service should ping the name server periodically (as defined below).
 *                   otherwise, it will be considered deactivated/crashed and be removed
 * - auto-ping method registers a peroidic deferred event on the server
 *
 * Note: metadata updates are not implemented at the moment on purpose because no system
 * is in place to automatically propagate such updates also to the clients using the services.
 * Updates may be implemented if there is a need.
 *
 * for clients (which also includes service providers)
 * - lookup()     -> basic form of lookup to find the matching service (must be unique)
 * - find()       -> advanced form of lookup to find a matching service (answers multiple possible)
 *                   either by name prefix or by key value store filter
 * - enumerate()  -> returns list of all services
 * - get_info()   -> returns all meta data associated with a specified service
 * - bind()       -> actually establishes a connection of a client with a service
 *                   This function handles the full package (UMP/LMP choice, Intermon routing,...)
 *
 * Lookup options
 * --------------
 * There are 3 ways of finding a specific entry on the name server.
 * These lookups will return a / several service_handle(s) that can be used for
 * binding to services and to get meta data information.
 * - Full path (== name) or prefix path in the name space
 * - short_name
 * - Selection of keys in the key value store
 *
 *
 * Bootstrap
 * ---------
 * The spawn service in the monitors provides the fresh domains with these infos:
 * - the capability for a client connection to the nameserver
 *   see added RPC to common that allows requesting a new channel capability (UMP/LMP)
 *   from any server, which is used to get a new nameserver capability before
 *   spawning any new domain
 * - core_id on which the nameserver is running (used to decide LMP/UMP, intermon routing)
 *
 *
 * TODO to discuss:
 * ----------------
 * - additional requirements needed for other services of the individual projects?
 * - any needs for access rights management?
 * - upcoming changes of the bootstrap and spawn mechanisms. OK for all other projects?
 * - shared requirements e.g. for the serializable_key_value_store?
 * - ?
 *
 * version 2017-11-29, Pirmin Schmid
 */


#ifndef INCLUDE_NAMESERVER_H_
#define INCLUDE_NAMESERVER_H_

#include <assert.h>
#include <stdint.h>

#include <aos/aos.h>

#include <nameserver_types.h>

//=== public interface =============================================================================

//--- initialization -------------------------------------------------------------------------------

/**
 * Needs to be called in the nameserver before any of the actual functions.
 * \return true in case of success; false otherwise
 */
bool init_nameserver(void);

//--- service side: resource comes first -----------------------------------------------------------

//--- registration ---

/**
 * \param request      a copy of the content in request is made internally
 *                     can be freed afterwards
 * \param contact_cap  either LMP endpoint or UMP ringbuffer frame
 *                     decision is made based on core_ids of nameserver and registering service
 * \param *ret_reply   will have the replies for the calling service in case of success
 *                     NOTE: this struct is allocaded by malloc() and must be freed by
 *                     the client after use.
 */
errval_t service_register(const struct service_registration_request *request,
                          struct capref contact_cap,
                          struct service_registration_reply **ret_reply);

/**
 * Due to technical reasons (establishing contact UMP channel cross core with the registering service)
 * registration happens in 2 parts through the RPC interface:
 * - part 1 registration and memory allocations as far as possible (and establishing LMP channel)
 * - sending reply message
 * - part 2 establishing cross-core UMP channel (doing nothing for LMP)
 * Both are called from within the RPC server.
 *
 * NOTE: running the 2nd part after the actual OK message has been sent comes with the
 * small risk that the UMP channel cannot be establish, which leads to deregistration
 * of the service. The risk for this inconsistency is low. Nevertheless, this is the
 * reason, why LMP connections are completed already in part 1 (no inconsistency
 * possible there).
 * Additionally, should use a barrelfish_msleep() of about 100 ms after successful
 * registration and then lookup its own registration. If the registration is
 * visible then, all is OK.
 */
errval_t service_register_part1(const struct service_registration_request *request,
                                struct capref contact_cap,
                                struct service_registration_reply **ret_reply);

errval_t service_register_part2(service_handle_t handle, uint64_t registration_key);

//--- deregistration ---

errval_t service_deregister(service_handle_t handle, uint64_t registration_key);


//--- ping ---

errval_t service_ping(service_handle_t handle, uint64_t registration_key);


//--- update key value store ---
errval_t service_update(service_handle_t handle, uint64_t registration_key, struct serializable_key_value_store *kv_store);


//--- client side: resource comes second -----------------------------------------------------------

//--- lookup / find ---

// basic functions: detected service must be unique, error otherwise
errval_t lookup_service_by_path(const char *abs_path_prefix, service_handle_t *ret_handle);

static inline errval_t lookup_service_by_name(const char *name, service_handle_t *ret_handle)
{
    return lookup_service_by_path(name, ret_handle);
}

errval_t lookup_service_by_short_name(const char *short_name, service_handle_t *ret_handle);

// advanced functions: may return 0 to many service handles
// *ret_handles is NULL in case of *ret_count == 0
errval_t find_any_service_by_path(const char *abs_path_prefix, service_handle_t **ret_handles, size_t *ret_count);

errval_t find_any_service_by_filter(const struct serializable_key_value_store *filter,
                                    service_handle_t **ret_handles, size_t *ret_count);


//--- enumerate ---

static inline errval_t enumerate_all_services(service_handle_t **ret_handles, size_t *ret_count)
{
    return find_any_service_by_path("/", ret_handles, ret_count);
}


//--- get info ---


/**
 * Function is offered in a serialized and regular version. No point in deserializing (after cloning)
 * and then to just serialize again for the remote access.
 * NOTE: the easiest way to clone a KV store here is to serialize it and then deserialize it.
 */
errval_t get_service_info_serialized(service_handle_t handle, struct serialized_key_value_store **ret_serialized_info);
errval_t get_service_info(service_handle_t handle, struct serializable_key_value_store **ret_info);

//--- bind ---

/**
 * \brief This is the actual RPC functionality that is used by the more convenient bind_service()
 * function that is offered within the RPC interface.
 * \param handle   a valid service handle
 * \param core_id  core_id of the client requesting a bind
 * \param ret_routing_info  routing info is only needed for RPC calls that need to transport
 *                          capabilities. In case client and server are not running on the
 *                          same core, a special routing protocol is used via intermon service
 * \param ret_interface     one of the defined RPC interface types.
 * \param ret_enumerator    enumerator of the service as registered with the nameserver
 * \param ret_chan_type     defines the protocol that is used to establish a channel
 *                          LMP: use cap as end point
 *                          FLMP: like LMP, but server sends additional frame for ringbuffers
 *                          UMP: use cap as frame for ringbuffers
 * \param ret_service_cap   capability to establish a channel (endpoint for LMP/FLMP; frame for UMP)
 *
 * The function is split into 2 internal parts. The service cap needs to be sent back
 * to the calling client of the nameservice between these 2 parts.
 * The 2nd part should only be called if the first returned with an err with err_is_ok(err).
 */
errval_t bind_service_lowlevel_part1(service_handle_t handle, coreid_t core_id,
                                     routing_info_t *ret_routing_info,
                                     enum aos_rpc_interface *ret_interface,
                                     size_t *ret_enumerator,
                                     enum aos_rpc_chan_driver *ret_chan_type,
                                     struct capref *ret_service_cap, bool *ret_self,
                                     void **ret_data);

void bind_service_lowlevel_part2(enum aos_rpc_chan_driver chan_type,
                                 struct capref cap, bool self, void *data);

// actual bind_service() function
// NOTE: since this function directly creates a full AOS RPC channel with the service,
// this function is offered directly inside of aos/aos_rpc.h (aos_rpc_client_name.c)
// and not here on this level. Internally, it uses bind_service_lowlevel(),
// among many other things.
//errval_t bind_service(service_handle_t handle, coreid_t core_id, struct aos_rpc **ret_rpc);


//--- helper function for the RPC server -----------------------------------------------------------
// - auto ping start/stop -> declared & implemented in AOS_RPC client wrappers

//--- helper function for the RPC client -----------------------------------------------------------
// - sorting of (info *) list -> declared & implemented in AOS_RPC client wrappers

//--- helper function for Gaia ---------------------------------------------------------------------
//    note: this is called by a separate thread running in Gaia

void nameservice_check(uint64_t cutoff_delta_ms);

#endif // INCLUDE_NAMESERVER_H_
