/**
 * \file
 * \brief create child process library
 */

/*
 * Copyright (c) 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetsstrasse 6, CH-8092 Zurich. Attn: Systems Group.
 */

#ifndef _INIT_SPAWN_H_
#define _INIT_SPAWN_H_

#include "aos/slot_alloc.h"
#include "aos/paging.h"

/**
 * Definition of struct spawninfo follows the recommended code part of original Barrelfish
 * http://git.barrelfish.org/?p=barrelfish;a=blob;f=lib/spawndomain/spawn.c;h=c5f2d943c921b2a332499d2367cfa8d3906e0405;hb=HEAD
 * and associated headerfile for struct spawninfo: include/spawndomain/spawndomain.h
 *
 * with various adjustments to different / limited settings of this version here on the Panda board.
 *
 * version 2017-10-11
 */
struct spawninfo {

    // Information about the binary
    const char *binary_name;     // Name of the binary

    const char *command_line;

    // identifies shell registered on nameserver
    // set if process spawned by shell, otherwise == -1
    int shell_id;

    domainid_t domain_id;
    coreid_t core_id;

    void *binary;
    size_t binary_size;

    // TODO: Use this structure to keep track
    // of information you need for building/starting
    // your new process!

    // the alignment to workaround here is verbatim copied from
    // the source header file in upstream include/spawndomain/spawndomain.h
    // it was introduced there to fix an arm-gcc bug
    // which generated (potentially) unaligned access code to those fields

    struct cnoderef rootcn __attribute__ ((aligned(4)));
    struct cnoderef taskcn __attribute__ ((aligned(4)));
    //struct cnoderef segcn  __attribute__ ((aligned(4)));
    struct cnoderef pagecn __attribute__ ((aligned(4)));
    struct capref   rootcn_cap __attribute__ ((aligned(4)));
    struct capref   taskcn_cap __attribute__ ((aligned(4)));
    struct capref   pagecn_cap __attribute__ ((aligned(4)));
    struct capref   dispframe __attribute__ ((aligned(4)));
    struct capref   dcb __attribute__ ((aligned(4)));
    struct capref   argspg __attribute__ ((aligned(4)));
    //struct capref     vtree __attribute__ ((aligned(4)));

    // to be closer to our defs. using pdir instead of vtree
    struct capref   pdir __attribute__ ((aligned(4)));

    // pisch: let's keep the original capref around that was used
    // for creating the 256 RAM capabilities of SLOT_BASE_PAGE_CN
    // it will be easier to return it to our ram / mm free
    struct capref   orig_base_page_cn_ram_capref __attribute__ ((aligned(4)));

    // pisch: needed for current kill domain workaround solution
    struct capref   dcb_in_child_space __attribute__ ((aligned(4)));

    // Slot (in segcn) from where elfload_allocate should allocate frames from
    cslot_t elfload_slot;

    // vspace of spawned domain
    struct paging_state *pstate;
    //struct vregion *vregion[16];
    //genvaddr_t base[16];
    //unsigned int vregions;

    dispatcher_handle_t handle;
    //enum cpu_type cpu_type; // our CPU type is known by default
    //int codeword;
    //char *append_args;

    // slot allocator for pagecn
    struct single_slot_allocator pagecn_slot_alloc;

    // TLS data
    //genvaddr_t tls_init_base;
    //size_t tls_init_len, tls_total_len;

    // Error handling data
    genvaddr_t eh_frame;
    size_t eh_frame_size;
    genvaddr_t eh_frame_hdr;
    size_t eh_frame_hdr_size;

    // name of the image
    //const char *name;
    // see binary_name above

    // spawn flags
    //uint8_t flags;
    //struct mem_region *binary_region;
};

// Start a child process by binary name. Fills in si
errval_t spawn_load_by_name(const void * binary_name, struct spawninfo * si);

// Group C: extended public interface

errval_t spawn_bootstrap_domain_monitor_only(const void *binary_name, domainid_t domain_id);

errval_t spawn_bootstrap_domain_monitor_and_memory(const void *binary_name, domainid_t domain_id);

errval_t spawn_load_by_name_with_id(const void *binary_name, int shell_id, domainid_t domain_id);

errval_t spawn_load_by_path_with_id(const char *path, const char *command_line, int shell_id, domainid_t domain_id);

/**
 * This function is only provided for init because all the other symbols are loaded during spawn when
 * the module / elf file is already in memory for loading of the other sections.
 *
 * Similar to the spawn process, this function checks config_get_spawn_load_symbols() before
 * actually loading the symbols.
 *
 * Note: this function must be called after memory management and paging infrastructure has been
 * established in init.
 */
errval_t load_elf_symbols_for_init(void);

#endif /* _INIT_SPAWN_H_ */
