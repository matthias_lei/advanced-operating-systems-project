/**
 * AOS Class 2017 Group C -- Individual Project: Shell Zeus
 *
 * version 2017-12-13
 */

#ifndef INCLUDE_SHELL_SHARED_H_
#define INCLUDE_SHELL_SHARED_H_

#include <aos/aos.h>

struct process_config {
    char *name;
    char *path;
    char *command_line;
    bool background;
    coreid_t core;
    int shell_id;
    domainid_t shell_pid;
    char *std_in_path;
    char *std_out_path;
};

extern struct process_config current_process_config;

extern bool waiting_for_termination;

#endif // INCLUDE_SHELL_SHARED_H_
