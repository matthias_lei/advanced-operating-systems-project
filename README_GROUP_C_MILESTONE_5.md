Milestone 5: Multicore
======================

# Spawning of the application core
We are now able to activate the application core and pass all the necessary pieces of information to it in order to:
- boot up the CPU driver.
- initialize the monitor, which in our case is the ```init``` process.
- establish a rudimentary message passing (URPC) channel between the ```init``` processes on both cores.

For this milestone the application core is given a fixed size of memory. This memory is used to manage all functionalities (e.g., pagefault handling) and services on this core. For the next milestones all of those will then be centralized back to the bootstrap core and and the application core will have to request memory via UMP. 

## Booting up the application core
One important aspect is in which address space the information is passed. Almost all addresses have to be passed in kernel-virtual space as the CPU driver of the application core is acting in this space. At the same time ```init``` on the bootstrap core acts in its virtual space, so extra care has to be taken when passing information to the new core.

Another important aspect is the clean and invalidate to the point of coherency for all the data prepared and written for booting the application core. Otherwise the application core, which has to boot directly from main memory, will not necessarily see the most recent data written by the bootstrap core, since it may not have been written back through the
whole cache hierarchy back to main memory.

## Creating the core data
By invoking a Kernel Control Block Capability the kernel gives us a struct where some of the necessary information is already provided. This struct acts as a set of starting parameters handed to the new core. However some of the fields have yet to be filled in.

## Passing the init ELF
After the CPU driver of the application core has booted up it still needs to start his first process, ```init```. This requires the core to be able to find the binaries for it. For this we have to pass the kernel-virtual base address of the ```init``` module located in the multiboot image to the application core.

## Loading the relocatable segment of the CPU driver
The text segment of the CPU driver is shared between both cores. However, the relocatable segment, which contains the data and BSS sections must not be shared (each core needs its own data). So it needs to be loaded into an own frame with its base kernel-virtual address passed to the new core. During the loading it is also necessary to rewrite the relative address into absolute addresses depending on the base kernel virtual address of this segment.

## Passing the URPC frame
This shared frame is the initial method of communication between the two cores so its kernel-virtual base address needs to be passed as well. On both sides this frame sits at a well known location in the CSpace ready to be mapped for the ```init``` processes.

# Initial UMP frame
While setting up ```init``` on the bootstrap core, we already allocate and initialize a URPC frame early
before starting to boot the application core. The capability to this frame is stored at a well-known location
in the CSpace of ```init```. During the preparation for the boot of the application core, we have to set
the physical address and size of the URPC frame in the ```arm_core_data``` struct, such that the kernel
on the application core is able to set up this frame for ```init``` on its core at the same well-known
location in the CSpace as it was installed on the bootstrap core. Only later, when ```init``` is setting up
itself on the application core, we map the URPC frame there and finish establishing the URPC connection to
```init``` on the bootstrap core.

Since the call to ```invoke_monitor_spawn_core``` on the bootstrap core only returns after the application
core has already enabled its caches and the MMU, we can already start sending the bootstrapping information
to the application core (such as the starting address and size of the RAM region to use for memory allocation,
the bootinfo structure, as well as the mmstrings capability for the modules) via URPC, while at the same time
the application core is still spawning ```init```.  As soon as the application core has established its URPC
connection to ```init``` on the application core, it starts listening on this channel for the bootstrapping
message. It is likely that by this time, the bootstrap core has already finished sending the bootstrapping
message, such that the application core is able to immediately start receiving the bootstrapping message and
finish configuring itself accordingly. Finally, the application core acknowledges the receipt of the
bootstrapping message by sending back the error code returned by its configuring procedure. If the error code
does not signify a failure, both cores should now be running concurrently.

For this milestone, the application core will now simply start listening on the URPC channel for spawn requests
from the bootstrap core, such that any applications can now be launched on both cores.

# Implementation details of URPC

For this milestone, user-level message passing is implemented in ```lib/aos/aos_urpc.c```. In the following we
shortly describe how the communication over this message passing channel works and use a simple state machine
to argue why the implementation is correct.

## How does it work

The channel is represented by a single flag and a message buffer. As long as the flag is cleared, the message
buffer is owned by the client. Likewise, when the flag is set, the buffer is owned by the server. Therefore,
setting the flag amounts to sending a request from the client to the server, whereas clearing the flag sends
back a response to the client. The request or response itself is stored in the buffer. Fences make sure that
the message buffer is fully visible to the respective recipient when the flag switches its state, as well as
that no write happens to the message buffer after transferring ownership.

## State machine

The following state machine is used to argue about the correctness of the implementation.

No request -> Prepare request -> Send request -> Read request -> Send response -> Read response -> No request

## Why does it work:

### No request
This is the initial state, where the flag stays cleared and we don't care about the data in the buffer.
- client: does not touch any state
- server: polls the flag but does nothing, since the flag stays cleared

### Prepare request
The flag stays cleared and buffer gets filled with data by the client.
- client: fills data into buffer, does not touch the flag
- server: polls the flag but does nothing since the flag stays cleared

### Send request
The flag is set by the client and the buffer is not touched.
- client: atomically sets the flag, which ensures that the flag is only set after all the write operations during prepare request (atomically writing the flag installs a fence with release semantics before the write)
- server: polls the flag and eventually sees it set, after which a fence with acquire semantics is installed, which ensures that no read or write operation following the fence, is reordered before the fence, i.e. the data buffer is neither read nor written up to this point

### Read request
The flag stays set and the request is read from the buffer, afterwards handled and finally an answer is written to the buffer by the server.
- client: polls the flag, but keeps polling since the flag stays set
- server: reads and writes to the buffer

### Send response
The flag is set by the server and the buffer is not touched.
- client: eventually sees the flag cleared and stops polling
- server: atomically cleares the flag, which ensures that the flag is only cleared after all the read and write operations during the request handling are done (atomically writing the flag installs a fence with release semantics before the write)

### Read response
The flag stays cleared and the buffer is read by the client.
- client: a fence with acquire semantics is installed before the buffer is read to ensure that no data is read from or written to the buffer up to this point
- server: polls the flag but does nothing since the flag stays cleared

### No request
The initial state is reached again, where the flag stays cleared and we don't care about the data in the buffer.
- client: does not touch any state
- server: polls the flag but does nothing since the flag stays cleared

# Windowing system: Hermes
As already mentioned at earlier stages, we finally implemented a windowing system to allow
a meaningful sharing of the console between processes. It is part of our serial server Hermes.

Every process gets assigned his window data structure on his first request to Hermes.
In this structure, there are two buffers, one to store serial output and one to store serial input.
Every RPC request to Hermes ( ```getchar()``` & ```putchar()``` ) is now directed to these buffers,
i.e. reads chars from the input buffer or writes chars to the output buffer.

Hermes maintains a list of windows corresponding to processes. Only one of these processes can use
the console at a point in time. This means that its serial requests are not only directed to its
window buffers but also to the console, s.t. ```putchar()``` are visible in the console and ```getchar()```
can be served directly by the user.

Direct user input is buffered, s.t. not directly consumed input will only be consumed on the next
```getchar()``` issued by this process. This can also happen, when this process does not have focus.
Processes issuing a ```getchar()``` and having an empty input buffer will block until they get focus
and receive user input.

Via the ```tab``` character a switch of the currently active window can be executed. This  flushes
the output buffer of the next window in Hermes' list to the console and gives the corresponding
process focus.


# Stack trace
We extended the exception handling by a stack trace routine that unwinds the stack frames. It helps
us finding the location of a specific bug including runtime lookup of the function symbol names.
For this purpose, .symtab and .strtab sections are loaded additionally during the spawn process
of programs. The ```init``` process (spawned by the kernel), loads its own symbols during initialization.
This functionality is machine dependent. We have implemented it in lib/aos/arch/arm/debug.c


# Milestone
Our submission fulfills all necessary requirements of milestone 5. We can

- boot the application core
- communicate between ```init.0``` and ```init.1```*
- handle page faults on both cores**
- start programs running on the application core from ```init.0```***

\* implicitly needed in several steps during boot up (e.g. transmit physical memory split from the bootstrap core to the application core) or to spawn processes from ```init.0``` on the application core

** can be seen e.g. by launching test "stackeater" from our domain manager UI "Zeus" on both cores

*** in general we can start any program available in Zeus (running on the bootstrap core) on both cores

# Bugs and fixes
These detected bugs and patches will be reported separately

## Spawning of the application core
Unfortunately, in about 10% of the cases, booting up the application core got stuck on our boards.
We traced down the issue to happen while the function ```platform_boot_aps``` runs on the bootstrap
core. There are only two places inside this function, where the execution can get stuck. There is a
spinlock that needs to be acquired and we need to wait for the application core to signal us that it
has finished setting up its MMU. From the application core, we do not see any serial line output and
since printing something on the serial line is pretty much the first thing, the application core does
after it is woken up for booting, only a tiny amount of instructions need to be considered on the 
application core.

We tried to narrow down the range of failing code further, but interestingly, as soon as we insert any
new ```printk``` statements into the function ```platform_boot_aps``` at any place (even at places that
are known not to be executed, when booting gets stuck, i.e. after the only two places that can block),
the bug is gone and the board boots reliably again. Moreover, we found out that for some compiler
versions the bug does not appear in the first place. Sadly, we did not have the time to investigate
this issue further and therefore "fixed" the problem by simply keeping a single empty ```printk```
statement in the mentioned function. However, according to the observations stated above, we suspect
that an unfortunate layout of the binary exposes this bug and changing the binary layout either by
an additional ```printk``` statement, or changing the compiler version simply hides the real issue better.
Alternatively, a timing problem (race condition) could be the issue. Also in this case, the working fixes
(```printk```, different compiler with slightly different code generation and the boot LEDs below) would fit.
However, manually enforcing e.g. the alignment of ```struct armv7_boot_record``` to fit into an entire cache
line did not fix the problem. So, we tested a lot, learned a lot, but have a fix that may only put a cover over
the true problem.

While debugging this problem, we wanted some form of signaling of during the boot process,
when we cannot really use any form of printf(). Thus, we added a few C functions that can
access LEDs D1 and D2 on the Pandaboard. Then these functions were called from within boot.S
To signal testing after waking up the cores (no LED), application core active (D1) and both cores
active (D1 and D2). This entire functionality runs on the application core directly on physical addresses
as the entire decision process whether the application core shall boot or not. While the code works well
(and also fixes the mentioned boot problem above), we decided not to include it into
the master branche to avoid any potential compatibility problem with any test boards.
However, it's available with the tag milestone_5_boot_LEDs in a separate branch.


## ELF library
While implementing the symbol name lookup functionality, we detected 2 bugs in the ELF library.
Additionally, we extended the functionality of the symbol lookup functions to allow the use of
the function even in the case of multiple symbols associated with a specific address (incomplete
implementation so far). We will provide the bugfix patch and the extension patch separately
by eMail.


# Some binaries

## Domain manager - Zeus
Basically "just" a user interface for the process service Demeter. It has the potential
to evolve into a shell in the future. It has been adjusted to spawn programs on both the
bootstrap and the application core according to the users choice.

## Test suite - Artemis
This program offers our established modular test suite system.

## Various test programs
In particular ```memeater```, ```stackeater```, ```threads``` and a collection of
test programs triggering specific exceptions, and testing Hermes.


# Usage
After booting, our low-level test suite allows launching some tests from earlier
milestones that are not (yet) available in Artemis. Upon exit of this menu (use #),
the services are initialized and the event handler loop starts listening to
incoming requests. Also the test program Artemis is launched that offers
various tests, as well as Zeus, our command line interface for the process server.

## Arguments passing from modified usbboot to init domain
We have implemented a mechanism to pass arguments from the host to our Barrelfish
instance without the need for recompilation or modification of the final binary on
the disk that is used for transmission to the Pandaboard.
Usage with the patched usbboot: ```tools/bin/usbboot <image> --initargs <args...>```.
More detailed description in milestone 4.


Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
