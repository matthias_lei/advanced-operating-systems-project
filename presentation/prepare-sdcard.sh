#!/bin/bash

AOS_ROOT=$1
SD_CARD=$2

head -c 20MB < /dev/urandom > $SD_CARD/random.dat
cp -r $AOS_ROOT/groupc $SD_CARD
mv $SD_CARD/groupc/lib/fs/fat/fatfs.c $SD_CARD/groupc/lib/fs/fat/fatfs_copy.c
tail -500 $SD_CARD/groupc/lib/fs/fat/fatfs_copy.c > $SD_CARD/groupc/lib/fs/fat/fatfs.c
rm $SD_CARD/groupc/lib/fs/fat/fatfs_copy.c
mkdir $SD_CARD/armbinaries
cp -r $AOS_ROOT/build/armv7/sbin/* $SD_CARD/armbinaries
rm $SD_CARD/random.dat

