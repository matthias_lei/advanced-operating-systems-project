\section{Memory Manager}

The memory manager, which started as a library and was later moved into its own service called Dionysos, is responsible for managing the physical memory of the OS. It is initialized with all the free available physical memory in the form of RAM capabilities after booting the bootstrap core. Every domain can allocate and free physical memory through the memory manager. A memory allocation consists of a size and an alignment requirement. If the memory manager can fulfill the request, i.e. there is a free memory block of the requested size (or larger) and with the requested alignment available, a RAM capability corresponding to this block is handed back to the requesting domain.

\subsection{Buddy Allocator}

Our memory manager internally uses the buddy allocator scheme to efficiently handle the allocation and free requests. A buddy allocator keeps track of the free memory regions in as large as possible blocks being aligned to their size and having sizes that are a power of two.

Upon a memory allocation, the requested number of bytes is first rounded up to the next power of two. Thus, from now on we assume the requested size to always be a power of two. If the allocator happens to have a free block with the requested size, this block is marked allocated and directly returned. If no free block with the requested size can be found, a free block with two times the requested size is split in half, where one half is marked allocated and returned and the other half is kept as a free block. The two halves resulting from such a split are called buddies. Of course it is not guaranteed that a free block of two times the requested size exists, when we could not find a free block matching the requested size, however, the procedure of looking for a free block twice as large and splitting it up can be applied recursively. As long as a free block of size at least as large as the requested size exists, the allocation request can be fulfilled.

Upon freeing a block of memory, the memory manager looks for the buddy of the block to be freed. If the buddy is currently allocated or does not exist because it has itself already been split up further, the block is simply marked free. However, if the buddy is currently free, the two buddies are merged together to form a new free block that is twice as large. Again the procedure of merging buddies can be applied recursively, i.e. after merging two buddies, the new free block can potentially be merged with its buddy if it exists and is free.

As a consequence of this design, we do not need to care about the alignment of an allocation request, as long as it is not larger than the size, since every block returned upon allocation is guaranteed to be aligned to its size. For allocation request with an alignment larger than the size, we could try to find a block that satisfies the alignment, however, we did not encounter any such allocation in our OS and therefore simply reject such allocations.

\subsection{Fragmentation}

Our buddy allocator suffers from both internal and external fragmentation. With internal fragmentation, we can waste up to a half of the total available memory in the worst case, where each allocation requests a size of $2^n + 1$ bytes for some integer $n$. Since we always round up to the next power of two, an allocation of size $2^n + 1$ bytes would actually get a block of size $2^{n+1}$ bytes and thus wasting $2^{n+1} - (2^n + 1)=2^n - 1$ bytes, which is almost 50\%. However, in practice internal fragmentation will be much lower, since the typical requested allocation sizes in our OS are actually already powers of two.

Since we did not implement any memory compaction, we also waste up to half of the total available memory through external fragmentation in the worst case. An allocation sequence that allocates all available memory in equal blocks of size $n$ bytes but only frees every second block, will leave us with half of the available memory free, but the largest allocation request that can be fulfilled is only $n$ bytes.

\subsection{Data Structures}

In order to efficiently serve allocation requests, the free blocks are stored in separate lists for each single block size. These lists are stored in an array with increasing block size. We support block sizes between 2 KiB ($2^{11}$ bytes) and 2 GiB ($2^{31}$ bytes) for reasons explained below, which gives us a total of 21 different block sizes. Therefore, for an allocation request, we can find the corresponding list in constant time and directly hand back a memory block, if the list is not empty. In the worst case (i.e. we have an allocation request for a 2 KiB block but the only available block is 2 GiB large), we need to recursively split 21 memory blocks.

\subsection{Buddy Merging}
% buddy merging was already perfectly explained above, thus here only some details about what is needed to do it
To be able to perform buddy merging the meta data of each block had to be extended. It must answer the questions, whether a block has a buddy and whether this buddy is free and not further split up. For this purpose we added a parent pointer to the meta data, that points to the block that produced the current block when being split. If a block has no parent, it was never split up and thus also has no buddy. If the parents of two buddies are different, one of them must have been further split up. To be able to look up a buddy's meta data, a table referencing all blocks had to be added. It uses base addresses of blocks as an index. The base address of a block's buddy can be easily found out by flipping the bit in the base address at index "size of the block". When a merge can be done, all resources needed for the tracking of one block can be released and reused by other blocks.

\subsection{Invariants For Slot/Slab Allocators}
As memory is such a basic system ressource, there are dependencies that can lead to further recursive allocations when serving a request. In fact, any arbitrary memory allocation request can recursively trigger up to 4 additional requests:
\begin{itemize}
\item a new L2CNode needs to be allocated, additional 16 KiB needed
\item the slab allocator needs to be refilled, additional 4 KiB needed
\item an ARM L2 pagetable needs to be installed, additional 4 KiB needed
\item the L1CNode needs to grow, additional memory of arbitrary size needed
\end{itemize}
If one of these additional requests cannot be fulfilled, this leads to an infinite recursion. We handle this problem by establishing two invariants before the first and at the end of any non-recursively triggered memory allocation request. Our two invariants are:
\begin{enumerate}
\item there must always be a certain number of free slots and slabs available, s.t. two arbitrary requests can be fulfilled \label{inv_one}
\item there must always be a certain number of blocks of certain sizes available to serve recursive requests of the slab and slot allocator, that do not need further slabs/slots \label{inv_two}
\end{enumerate}
With these two invariants all four types of recursively triggered allocation requests can be fulfilled. Invariant \ref{inv_one} assures that the growing of an L1CNode as well as an arbitrary request can be handled. We use 32 as our minimum count of available slabs and slot-pairs. This is due to our free-list with 32 buckets. A request can lead to possibly 31 block splitting operations. Any splitting needs two additional slots and two additional slabs. However, the way we do it, the usage of a new pair of slots frees a slab, which limits the need of fresh slabs to 32. Invariant \ref{inv_two} makes sure that the cases of  refilling the slab allocator, installing a fresh ARM L1 pagetable and allocating a new L1CNode, can be handled. The allocation requests of these operations can be limited by 4 KiB or 16 KiB respectively. Assuring a minimal bucket count of 4 for the 4 KiB bucket and 3 for the 16 KiB bucket avoids infinite recursions, as at most two arbitrary request and 2 resp. 1 fixed size request for a block of these sizes can happen.
