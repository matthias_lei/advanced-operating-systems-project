  \section{Shell}

\begin{center}
\textit{Bear up, my child, bear up; \\
Zeus who oversees and directs all things is still mighty in heaven.}

(Sophocles)
\end{center}

Individual project by Loris Diana.

\subsection{The Basis: Serial Service Hermes}
We already invested some time during the group part of the project in building a serial server, that can actually process RPC requests to use the console in a meaningful way. It is described in subsection \ref{windowing_system}.

\subsection{Basic Design Of The Shell Zeus}
Our goal for Zeus was to make it as user-friendly as possible and in usage similar to a UNIX shell. We tried to design it in a modular way, it should be easy and not too much overhead to add additional built-in functions. All functions are referenced in a global hashtable, where they are retrieved from on input from the user. With every function also some description and information about its options is stored. As typically for a shell, Zeus basically runs an infinite loop, that waits for an input line by the user, parses it into several chunks, that are then interpreted and result in executions of one of the different built-in functions. Luckily only small modifications allowed us to make use of the library Linenoise for the reading of the user input. It allows direct editing with deletion and cursor movements and has some nice additional features. We still had to take care of the parsing, which is done in two stages and supports specifying non-standard I/O endpoints for all shell commands. We colored and extended the prompt with the current working directory path, which is tracked by the environment variable \texttt{PWD}. Environment variables can be defined and initialized in a separate structure in \texttt{environment.c}. However we did not implement the passing of environment variables, as our only program that actually makes use of them is the shell itself. The whole shell code lies in the folder \texttt{usr/zeus}.

\subsection{Commands}
The following commands are available in our shell:
General commands:
\begin{itemize}
\item \verb|help|
\item \verb|echo|
\item \verb|run|
\end{itemize}
Filesystem commands:
\begin{itemize}
\item \verb|mkdir|
\item \verb|rm|\footnote{including option -r for recursive removal}
\item \verb|rmdir|
\item \verb|cd|
\item \verb|touch|\footnote{done by J.Meier}
\item \verb|cat|\footnotemark[\value{footnote}]
\item \verb|ls|
\item \verb|cp|
\item \verb|mount|\footnotemark[\value{footnote}]
\item \verb|apollo|\footnote{done by P.Schmid}
\end{itemize}
They all conform to the same signature

\begin{center}
\begin{small}
\verb|int fun(int argc, char **argv,|
\verb|FILE *std_in, FILE *std_out)|
\end{small}
\end{center}
Thus every function allows specification of different IO endpoints, which are then e.g. written to using \texttt{fprintf}. This also holds for the run command, where the redirected \texttt{stdin} and \texttt{stdout} are passed to the newly spawned process. Every command parses options using \texttt{getopt}. With option \texttt{-h} e.g. every command displays a small description of its options and arguments. Especially the filesystem commands work a lot with file paths. There, conversion of the input with respect to the current working directory is considered and shortcuts, like \texttt{\texttildelow} for the \texttt{HOME} folder or \texttt{..} for the parent folder are supported.

\subsection{Linenoise}
To make the shell minimally user-friendly, a sophisticated \texttt{readline()} function is necessary. We were told about Linenoise, an implementation of an editable command-line, that is also present in the Barrelfish repository. It consists of not much more than 1000 lines of C code and uses only very few POSIX functions. Stripping out of these POSIX functions, exchanging the \texttt{read()} and \texttt{write()} functions by our own terminal function implementations, hardcoding the width of our window and a small change in our serial server made it already work. As mentioned, Linenoise among more also tracks history of input and makes it accessible with arrow-up, which is very useful. The cursor can be moved with the arrow keys, or more reliably with \texttt{CTRL-B} / \texttt{CTRL-F}. Also, it offers callback functions for completions and hints. Especially completions are very useful, once one wants to enter long filepaths. We implemented a completion function, that for one completes commands and also whole filepaths, folder by folder, given a prefix. To be able to use colors inside the prompt, we added a small state machine to Linenoise. It computes the length of the prompt without taking color codes into account. This is needed for a correct display.
\subsection{Parsing}
Zeus accepts input in the following format:

\begin{center}
\texttt{\scriptsize command arg1 arg2 ... argn > std\_out < std\_in}
\end{center}
The input is first tokenized into maximally 3 tokens, consisting out of the command-line string, the std-in and std-out specification. This is done via a state machine, that filters out invalid syntax and does not care about the order stdin and stdout are specified. The command-line string is then processed in a second step, where it is tokenized in its arguments and brought into the form conforming to the signature of the built-in functions. The idea of this 2-step-parsing was also done knowing, that our spawning functionality actually accepts a whole untokenized command-line string. Thus the second parsing step would not be necessary in case of spawning a new process.
\subsection{Running Processes}
Using the \texttt{run} command we can launch new processes. Using option \texttt{-s} we can display its menu, where all our multiboot programs and associated launch characters are listed. We can launch a program with two options:
\begin{itemize}
\item{\verb|-b|: launch process in background}
\item{\verb|-c [0 or 1]|: launch process on core 0 or 1}
\end{itemize}
In the usual case, that is without the option \texttt{-b}, the shell will block until the launched process is finished and, if not specified differently via I/O redirection, the process will write its output to the shell's window. In detail, when it starts, the shell registers itself at the nameserver Gaia. From Gaia, it receives an id, that can be used by other processes to get an RPC channel to the shell. The shell passes this id to every process it spawns. After spawning a new process, the shell itself goes into a server state, to receive a request from its child for some parameters, that it needs to know how it is being run. Upon receiving this request, the shell sends some needed parameters, like a flag specifying if the new process runs in the background, its pid, or the (file-)names of the redirected \texttt{stdin} / \texttt{stdout}. The process then sets itself up accordingly and starts running. Before it terminates, it again sends a notification to the shell, which then overtakes its window at Hermes.
If \texttt{run} is executed with option \verb|-b|, the shell will not block and the process will start executing in its own window, where we directly switch to on its first output. With option \verb|-c| we can specify the core where the process will run on.
\subsection{Multiple Instances Of Zeus}
It is possible to spawn Zeus on both core 0 and core 1. Theoretically, a total of up to 1024 instances can be run, neglecting other resource limits posed by the system. Any additionally spawned Zeus can be looked up via a unique path on Gaia and is thus able to spawn domains, which then establish a connection to it.
\subsection{IO Redirection}
Zeus supports the redirection of I/O for both the built-in commands but also processes launched via \texttt{run}. In the case of built-in commands, the parsed user inserted \texttt{stdin} / \texttt{stdout} filepath is opened and directly passed to the function as an argument. On termination the file is closed and thus also flushed, s.t. the output can be looked at via the filesystem. In the case of a process, the \texttt{stdin} / \texttt{stdout} names are sent as an answer to its request after spawning. The process then itself opens the files and sets them as its I/O streams.
\subsection{Escape Sequences On Serial Server}
Our decision to use Linenoise posed some new requirements to our serial server Hermes. Linenoise works a lot with ANSI escape codes, e.g. to clear a line, move the cursor or also to display colors. In our original version of Hermes, every character that was printed by a process, was also buffered in a 2-dimensional window buffer, to make switches of the output window possible. Linenoise completely rewrites the input line, whenever a character is inserted and additionally issues many escape sequences. This leads to poor display of the buffered data on a window switch, as the buffer is limited in width and it happened that escape sequences were wrapped around lines. We decided thus to parse these escape sequences using a state machine and perform the required manipulations in the buffer directly. To also handle the case of colors, we defined a \texttt{struct character} that can additionally to a \texttt{char} store 3 color values, if it should be displayed in a fresh color. On a window switch, when the buffer is written out, these values are recomposed to a color escape sequence and sent to the serial driver, such that the subsequent characters are displayed in this specific color.
