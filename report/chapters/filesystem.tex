\section{Filesystem}

Individual project by Jonathan Meier.

The filesystem is implemented as a global service in our OS. That is, each domain has the same view on the filesystem at any given time. Therefore, the filesystem service is an RPC server running in its own domain. Besides the client side of the filesystem RPC interface, hereafter referred to as client library, and the server side of the filesystem RPC interface, referred to as filesystem service, there are a few other components constituting to the whole filesystem that will all be described in detail below, namely a VFS layer, a FAT, RAM and multiboot filesystem driver, as well as a block device driver.

\subsection{Filesystem RPC Interface}

The filesystem RPC interface offers all the functionality that is available for interaction with the filesystem on our OS. The following list is a brief summary over the supported functions:

\begin{itemize}
\item \texttt{mount}: mounts a filesystem on a device at a specified mountpoint
\item \texttt{mkdir}: creates a new directory
\item \texttt{rmdir}: removes an existing and empty directory
\item \texttt{opendir}: opens an existing directory for enumeration
\item \texttt{dir\_read\_next}: reads the next directory entry, i.e. file or directory name, from a directory enumeration, or signals the end of the enumeration
\item \texttt{closedir}: closes a directory that is open for enumeration
\item \texttt{open}: opens an existing file
\item \texttt{create}: creates and opens a new file
\item \texttt{remove}: removes an existing file
\item \texttt{read}: reads a specified amount of bytes from an open file
\item \texttt{write}: writes a specified amount of bytes into an open file
\item \texttt{truncate}: truncates a file to the specified amount of bytes, i.e. discards bytes, if the file is larger than the specified new size or zero-extends the file in the other case
\item \texttt{tell}: returns the current file pointer position
\item \texttt{seek}: sets the current file pointer position, either using an absolute position or relative to the current position, or to the end of the file
\item \texttt{close}: closes a file that is open for reading or writing
\item \texttt{stat}: returns type (i.e. file or directory) and size of an open file or directory

\end{itemize}

\subsection{Client Library}

The client library is essentially a small wrapper around the filesystem RPC interface. It is basically the same code that was already provided for us using the RAM filesystem, where all function invocations to the RAM filesystem were replaced by function invocations to the new filesystem RPC interface. In particular, the client library integrates the filesystem RPC interface into some of the \texttt{libc} file interaction functions, such as \texttt{fopen}, \texttt{fread}, \texttt{fwrite}, \texttt{fseek}, \texttt{ftell} and \texttt{fclose}, by providing a mapping infrastructure from the filesystem file and directory handles to the file descriptors used in \texttt{libc}. All directory operations as well as \texttt{remove} and \texttt{stat} are simple wrappers to the corresponding RPC function.

\subsection{Filesystem Service}

The filesystem service, Plutos, implements the server side of the RPC interface. It is brought up late in the booting phase but before the shell is launched, in order to be readily available for use. During initialization, a RAM filesystem is automatically mounted as the root filesystem, such that any file operations are already available in the shell, without the need to mount a filesystem first. As with all services, Plutos registers itself with the nameserver, Gaia, in order to accept binding requests from domains requiring filesystem access. Note that we did not implement any file or directory access permissions, that is all domains are allowed to connect to the filesystem service as well as to read, create, modify and delete any data across the whole filesystem.

After initialization the filesystem service is ready to accept binding requests and process RPC calls. All it does is unmarshalling the RPC messages and relay the RPC invocations to function calls in the VFS layer.

However, in contrast to all other services on our OS, the filesystem service needs a strict serialization of the incoming RPC requests. This requirement comes from the fact, that the filesystem drivers used below the VFS layer may use further RPC invocations themselves to other services, as is the case with the FAT filesystem driver, which needs to access the block device driver via RPC. As described in section \ref{mps}, our messaging stack is asynchronous, i.e. blocking operations, such as a combined send and receive operation used on any RPC invocation on the client side, are not implemented with busy waiting but with event dispatching. If we would not have strict RPC request serialization, the following scenario would be plausible: A first RPC request arrives at the filesystem service, that wants to create a new directory on the FAT filesystem. The FAT filesystem driver needs to find a new free directory entry in the respective directory table and therefore reads the corresponding data blocks from the SD card using the block device driver via RPC. While sending or receiving messages during this RPC invocation, we might need to block and therefore start dispatching events. While dispatching events, a second RPC request arrives at the filesystem server that wants to create a new directory in the same parent directory as the first RPC request. The event dispatching loop will pick up the RPC request event and start processing it. Obviously, again we need to find a new free directory entry in the same directory table as for the first RPC request and therefore contact the block device driver a second time for the very same block as before. Note that we are requesting this block while we are still in the process of receiving this block for the first request. This is not a problem for the block device channel, since channels are properly implemented to provide FIFO order, however, both requests will eventually receive the same data block and therefore, both requests will find the same free directory entry. As a consequence, the second request will write the new directory entry into the directory table and the event loop of the first request will return after finishing the second request. Then, the first request will proceed by writing its directory entry into the directory table as well, but this overwrites the directory entry from the second request, since both requests found the same free directory entry.

With serialization of RPC requests, processing a new request only ever starts after all previous requests have finished. From a filesystem RPC interface viewpoint, this means that every filesystem RPC invocation can be considered atomic.

The request serialization is implemented via a waitset that is shared among all channels that are established with the service. Upon receiving a new RPC request, we first find out, whether the server is already processing a request (i.e. we are in an event handler that was dispatched while processing a request). If this is the case, the request information is copied and the request is enqueued on the waitset. If the server is not already processing an event, we process the current event directly and after handling the event, we process all events that were enqueued in the meantime (if any). Therefore, we have established the invariant, that whenever the service is not processing any events, the event queue is actually empty.

Even though it turned out that request serialization is only really necessary for the filesystem service, the serialization infrastructure is implemented generically, such that any service that does not serialize requests only needs to add initialization for the waitset and redirect all events to the synchronizing generic event handler, in order to add serialization. Such a change amounts to less than 10 lines of code.

\subsection{VFS Layer}

The VFS layer is a simple abstraction layer, that glues together the three different filesystem drivers that we have. It provides a unified filesystem tree view, that allows to mount a filesystem tree root provided by a filesystem driver at an arbitrary existing directory called the mountpoint, such that the whole mounted filesystem tree is then available over the mountpoint.

A global list of mount handles is stored in the VFS layer. Each mount handle describes a currently mounted filesystem by storing the device where the filesystem lives on, the path of the mountpoint, a pointer to a virtual table with function pointers to the filesystem driver specific implementations of the driver functions as well as an opaque pointer to driver specific data.

Upon a mount operation the VFS layer first determines whether the mountpoint is an existing directory (note that root always exists, to bootstrap mounting of the first filesystem) and determines whether the specified device has not yet been mounted, by inspecting the list of mount handles. It then calls the filesystem drivers specific mount function and sets the virtual table accordingly.

Since we only deal with three different filesystem drivers, the virtual table for each driver is statically defined directly in the VFS and the VFS mount operation decides what filesystem to mount directly based on the provided URI, unlike VFS in UNIX-like systems, where the different filesystems register themselves with VFS.

When opening file or directory handles, the VFS layer simply wraps the filesystem drivers specific handle in a handle that additionally stores the mount handle of the filesystem driver, such that whenever an operation on a file or directory handle is requested, VFS can get the filesystem drivers virtual table via the mount handle to invoke the right filesystem driver function.

When operating with file or directory paths, VFS first needs to determine the correct responsible mount handle. Thus, it scans through the mount list and matches the longest mountpoint with the prefix of the requested path. Note that matching is always successful, since the root needs to be mounted by default. After determining the responsible mount handle, the mountpoint prefix is cut away from the path and the corresponding filesystem function is invoked in the filesystem driver through the virtual table of the mount handle.

\subsection{FAT Filesystem}

The FAT filesystem driver enables access to SD cards that are formatted with the well-known FAT32 filesystem format. It uses the block device driver to actually read the data blocks from the card. The driver supports any valid formatting parameters according to the specification, e.g. all combinations of 512, 1024, 2048 or 4096 bytes per cluster with 1, 2, 4, 8, 16, 32, 64, 128 sectors per cluster should be handled correctly. Reading and writing both files and directories is available as well as long filename support from the VFAT extension.

There are a few interesting implementation details that we like to point out, namely sector and directory iterators and the sector caching mechanism.

\subsubsection{Sector Iterators}

The FAT32 filesystem uses two different types of data sizes, which are sectors and clusters. A sector is the smallest atomic unit of data that a FAT32 driver should read and write. It is an aligned to its size, contiguous, fixed size block of data on the storage device. The sector size does not need to match the underlying device block size but it usually does. A cluster is a fixed number of contiguous sectors and it is the unit that is actually indexed by the file allocation table (FAT), which means files and directories always occupy storage space with a total size that is a multiple of the cluster size on the device.

Files and directories are essentially stored by chaining clusters together. A directory is basically a file containing a table of directory entries. Each cluster has an index and at its index in the file allocation table, the index of the next cluster is stored, or an end-of-chain marker. The first cluster of a file or directory is stored in the corresponding directory table entry in the parent directory. The root cluster is stored in the boot sector.

Therefore, when reading and writing files or directories (sequentially), we want to iterate over the associated cluster chain. However, the atomic unit of data to read are sectors, which may be smaller than a cluster. Thus, a sector iterator is a basic structure that can be used often throughout the driver implementation. It stores the current cluster index, as well as the relative sector index within the cluster. When the next sector is requested from the iterator, the sector index is incremented by one. If the sector index is smaller than the number of sectors per cluster, we are already done. However, if the sector index is equal to the number of sectors per cluster, we need to reset the sector index and load the next cluster from the cluster chain. The end of a sector iterator is reached, when the cluster index read from the cluster chain marks the end of the chain.

\subsubsection{Directory Iterators}

As already mentioned above, directories in FAT32 are just files that store a table of directory entries. However, especially with long filename support, not every directory entry describes an actual file or directory. There may be arbitrary directory entries marked free in the table from previous deletions, in the root directory table there is one entry that stores the volume name, the long filename format uses multiple contiguous directory entries to store information for a single file and directory and finally, each directory (except for the root directory) contains a '.' (dot) and a '..' (dotdot) entry that point to itself and the parent respectively, but we want to filter them out, since we do not use them.

Therefore, a directory iterator is used to enumerate actual file and directory information from the directory table. Free entries, volume name entries as well as the dot and dotdot entries are simply ignored and long filenames are transparently assembled, such that on advancing the iterator, we always get an actual file or directory object with a possible attached long name, or we reach the end of the iterator.

\subsubsection{Sector Caching Mechanism}

Unfortunately, the block device driver is really slow in reading data (see section \ref{fsperf}). Hence, we need some kind of sector caching in the FAT filesystem driver. This is achieved with a simple LRU cache. A predefined fixed size of cache slots is allocated upon initialization of the driver. A cache slot stores the previous and next cache slot in an LRU list, the previous and next cache slot in a hash bucket list, as well as the sector number it caches and a pointer to the sector data. The cache itself stores the head and tail of the LRU list, as well as an array of hash buckets.

The basic idea is, that whenever we have a cache hit (i.e. we read or write a sector that is stored in the cache), we can quickly look it up through the hash buckets (a simple modulo operation on the block number to find the bucket index), read or write the sector and move the cache slot from wherever it lives in the LRU list to the head. Whenever we don't have a cache hit, we can simply take the cache slot at the tail of the LRU list (this is the least recently used cache slot) to cache the new sector. Hence, we adjust the sector number of the cache slot, insert it at the head of the LRU list and move it to its new bucket in the hash table (if necessary). Collisions in the hash table are resolved by chaining the cache slots using the previous and next pointers mentioned before. The cache is a implemented as a write-through cache, in order to guarantee that the content on the disk is always up-to-date, which is important in case of a crash.

\subsection{RAM Filesystem}

The RAM filesystem can be seen as a volatile storage device. All the file and directory information is stored in memory and lost again after reboot. However, this is a really useful root filesystem on our OS, since then it is not mandatory to have an SD card or some other storage device attached to the board, but the shell and all its filesystem commands can still be used.

The RAM filesystem is still based on the implementation that was initially provided. However, a lot of bugfixes were necessary in order for it to work correctly, since opening and closing handles was not implemented consistently.

\subsection{Multiboot Filesystem}

The multiboot filesystem is a read-only filesystem that provides access to the modules stored in the multiboot image. This proved useful when implementing the functionality of spawning domains from the filesystem in the shell, when either the FAT filesystem had bugs, no SD card was available, or (most likely) because the FAT filesystem is simply too slow for efficient testing. Upon initialization, the multiboot filesystem driver requests a list of all available modules in the multiboot image from its monitor. It then assembles the corresponding directory structure in memory. On the first access to a specific file, the respective module capability is requested from the monitor and mapped into the virtual address space in the multiboot filesystem driver, where the data can now be read directly for the current and every following access.

\subsection{Block Device Driver}

The block device driver provides access to the data stored on the SD card in aligned, fixed sized blocks. It is implemented as a separate service, with the idea in mind, that multiple instances of a block device driver, each managing a different storage device, could be desirable on a system that support multiple disks. Three functions are available in the RPC interface that allow to query the block size as well as reading and writing a single block. Upon initialization, the block driver enables and initializes the SD card controller and registers itself with the nameserver. The block device driver is brought up in the boot sequence right before the filesystem service is started. Therefore it is important that if the SD card should be used later via mounting the FAT filesystem, the SD card must already be inserted before booting. This limitation could be avoided by starting the block device driver only before mounting the FAT filesystem.

The performance of the provided block device driver is really poor, as can be seen in detail in section \ref{fsperf}. However, a simple extension that did make use of Advanced Direct Memory Access (ADMA) did not improve the performance at all. Therefore, the current implementation is still the one that was originally provided, but packed into a separate library, such that it can easily be wrapped and linked in our RPC server library structure.

\subsection{Performance} \label{fsperf}

In the following performance analysis, we consider the read bandwidth from the SD card over the FAT filesystem driver on core 0. We use a file of exactly 1 MiB placed in the root directory of a formatted SD card and filled with random data generated on Ubuntu using\newline\texttt{head -c 1M < /dev/urandom > test.rnd}\newline All the measurements were taken after a freshly booted system without any additional domains running apart from the services spawned during boot, except when noted otherwise.

A first measurement was taken inside the block device driver to determine the bandwidth of the SD card driver without any other processing or messaging overhead. Reading the test file took 2051 block reads of 512 bytes of which 2048 are for the file content and the remaining 3 origin from reading the FAT and directory information. Over these 2051 block reads, an interesting pattern of measurements can be observed. About 74\% of the block reads take less than 11.1ms (mean 11.08ms, standard deviation of 33.08us), whereas the remaining 26\% of block reads are roughly between 60ms and 70ms. It turns out that in the 26\% of block reads taking significantly longer, the block device driver is preempted within the measurement section.

Therefore, a special measurement was taken inside the monitor on core 0 before any of the core services are started, in order to determine the true SD card bandwidth. Indeed, in this measurement, all block reads took less than 11.1ms (mean 11.07, standard deviation of 35us), which gives us a card bandwidth of 512 bytes per 11.07ms, or 46.25 KiB/s.

Coming back to the measurement inside the block device driver, over the 2051 block reads, we have an average block read time of 25.86ms, which corresponds to a bandwidth of 19.80 KiB/s.

A second measurement was taken inside the FAT filesystem driver to determine the bandwidth of the block device driver over RPC. Interestingly, here the average block read time over the 2051 block reads was 24.20ms, which is less than above, even though we have RPC messaging in addition! A closer inspection of the single block read measurements shows, that now the percentage of interrupted block reads dropped from 26\% to 23\%, which makes the difference. It seems that the small overhead introduced by the measurement code in the first measurement is enough to increase the probability of being preempted. In this second measurement, we have this overhead too, but we do not increase the probability of being preempted, since we actively trigger intended context switches by sending and receiving LMP messages. Considering only the block read times without preemption, we have an average block read time of 11.24ms (standard deviation of 48.19us), which compared with the special measurements indicates that the overhead of RPC is roughly between 100us and 200us on average per call.

A third measurement was taken on the client side of the filesystem RPC interface, where we measure reading 1024 blocks of 1 KiB in the client library via \texttt{fread}, which takes 49.73ms on average. This is 1.33ms more than double the average block size read time from the second measurement. The overhead can be explained by the \texttt{libc} internal function calls and the RPC overhead. Finally, this gives us a bandwidth of 20.59 KiB/s on the client.

In summary, these measurements show that the read bandwidth is almost absolutely determined by the block device driver performance and the domain scheduling. The overhead introduced by the message passing is minimal. This means in particular, that the number of domains running on core 0 directly impacts the read bandwidth.

A few measurements were also taken on core 1, where we have a UMP channel to the filesystem service, instead of an FLMP channel as is the case on core 0. However, here the measurements heavily depend on the starting time of the benchmarking. In contrast to FLMP, the domain receiving a UMP message does not get activated immediately, but only polls the channel when it is scheduled, or before yielding when dispatching events. Therefore, if a message is available on the channel shortly after the receiving domain was preempted by the scheduler, we need to wait until it is scheduled again. Since reading a file with \texttt{fread} happens in chunks of 1 KiB, there are a lot of relatively short messages on the UMP channel. In some cases a good rhythm of these messages could be observed, where many messages were sent shortly before the receiving domain polled the channel. In other cases a bad rhythm was observed, where many messages were sent shortly after the receiving domain was preempted. Therefore, the results on core 1 are sometimes nearly as good as on core 0, but in some measurements reading the 1 MiB file even took 2-3 times longer than on core 0. A possible approach to mitigate this problem could be to experiment with cross-core interrupts, to notify a domain when a UMP message is incoming.

\subsection{Limitations}

A big challenge when implementing filesystems, is handling of concurrent access to the same file or directory. There are many tricky corner cases. For example what happens, when a directory is being enumerated and a new file or folder is concurrently created in the same directory? Or, what happens, when a file with open handles is deleted? In our filesystem these cases are all avoided by simply allowing only one single open handle per file or directory. Moreover, files or directories can only be deleted, when there are no open handles. To ensure these invariants, the VFS keeps a list of all open handles and rejects the creation of two file or directory handles.

The FAT32 specification allows to have multiple file allocation tables, mainly for backup reasons in case of device block corruption. However, the FAT filesystem driver in our OS neither would use any of the backup file allocation tables in case of a device block corruption, nor does it update other file allocation tables than the first one, when data on the volume is modified. All additional file allocation tables are left inconsistent.

Moreover, there can be an FSInfo structure on the volume storing metadata about the free clusters in the file allocation table. Our FAT filesystem driver neither uses this information, nor updates or invalidates this structure on volume modification.

\subsection{Future Work}

There is a lot of room for improvement in many areas of the filesystem. First and foremost, performance is really poor and should be increased in order to get a more usable system. Possible speedups could be achieved by improving the block device driver itself, e.g. by using DMA, read-ahead and write-behind, etc. Moreover, the FAT filesystem driver could implement a more sophisticated caching strategy. Currently, the cache size is limited to 8 MiB, which means that if you read a file of size 8 MiB or larger, the cache is completely overwritten. This is especially bad for the sectors containing the fat allocation table, which are frequently used. It would be interesting to see the effects of a least frequently used eviction strategy or the effects of having separate caches for file allocation table sectors and data sectors.

Furthermore, the VFS could be made more modular and generic, such that new filesystem drivers can easily be integrated. Currently, there is also no possibility to unmount a previously mounted filesystem. Other interesting directions of improvement would be the implementation of permissions or support for bind mounts.

Finally, the present integration of the filesystem into the \texttt{libc} interface is unsatisfactory. For example all the directory read and write functionality is only accessible via a custom interface. Moreover, filesystem utility functions such as \texttt{realpath} can not be used, since important filesystem functions like \texttt{lstat} or \texttt{readlink} are not implemented.