\section{Paging}

We implemented a page fault handler using the LMP system from the previous milestone. The use cases for page fault handling occur in thread stacks and the heap which are both treated as paging regions in the OS. Therefore, the management of the virtual address space of a domain is crucial and we will also be looking at it in more detail.

\subsection{Organization of the Virtual Space}

The virtual space layout of each domain is as follows:
\begin{enumerate}
  \item \textit{reserved region} [0-1) GiB: The ELF binary is loaded into this section (r/w and r/x rights as indicated by ELF). Additionally the first 4 KiB page is reserved for the Nullpointer. Any dereferencing of these addresses results in an Nullpointer Exception in case of \texttt{0x0} or in a "Probable Nullpointer Exception" if the address is non-zero. Similar to guard pages this page is never mapped to a physical frame so every access is detected during the page fault.
  \item \textit{managed region} [1,2) GiB: This region itself is divided into two subregions: the thread stacks and the heap.
  \item \textit{restricted kernel space} [2,4) GiB: This region is not accessible from user space. Any dereferencing to this area causes a page fault because of insufficient access rights.
\end{enumerate}

For the managed region we defined the heap region to be 768 MiB large and the remaining 256 MiB is allocated to stacks and slab allocators. Consequently, stacks of new user-level threads receive their own paging region rather than being malloced in the heap.

Reason for this decision was the introduction of guard pages. Currently, every paging region is lower bounded by a guard page which is never mapped to a physical frame. In the case of stacks this provides a mechanism to detect stack overflows for every thread. In the case of heaps, guard pages will protect the reserved region for the ELF binary.

We also implemented an option to activate upper guard pages. But at the moment this option is not active. With the bottom guards there is already one guard page between adjacent regions.

\subsection{Management of Paging Regions}

The tracking of allocated paging regions is implemented as a list sorted by address. If a page fault is thrown in the managed region of the virtual address space, the list is walked in order to check which paging region it belongs to. This means that the complexity of allocations, destructions and lookups is linear to the number of paging regions. But as we most of the time maintain only a few additional user-level threads (if any) and one heap per domain, this non-optimal complexity was accepted for the benefit of a simpler implementation. A more optimal solution would be a tree datastructure for logarithmic allocations, destructions and lookups.

Currently, every thread stack is given a stack size of 64 KiB.

\subsection{Page Fault Handler}

Our page fault handler is able to distinguish different types of page faults and check if those are valid or not. The types of page faults which are treated as errors are:
\begin{itemize}
  \item Nullpointer exception when the first page [0-4) KiB is accessed
  \item Access into guard pages
  \item Access into restricted kernel space
  \item Access right violation into the reserved region where the ELF binary is loaded
  \item Page fault during page fault handling
\end{itemize}
As required, accesses into the heap or the stack will be backed by physical frames.

A very helpful tool during the development of this OS proved to be our stack trace tool, described in subsection \ref{stacktrace}. Every page fault which cannot be served prints out the stack trace. This saved us a lot of time during the debugging of later features.
