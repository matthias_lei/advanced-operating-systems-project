\section{Lightweight Message Passing} \label{lmp}

In our OS message passing is the only means of communication between different domains. We differentiate between intra-core communication (i.e. communication between domains running on the same core) and inter-core communication (i.e. communication between domains running on different cores). This section is about intra-core communication, which is achieved using Lightweight Message Passing (LMP).

Over the course of the project, the whole message passing infrastructure evolved, however, the core structure of our initial design still remains. We can distinguish between three different layers that are described separately below: the Remote Procedure Call (RPC) layer, which allows to invoke methods in another domain using the message passing infrastructure, the Message Passing Stack providing utility functions to send and receive messages over the different channels and finally the Message Passing Drivers which operate below the Message Passing Stack and handle the actual sending and receiving of bytes.

\subsection{RPC}

A remote procedure call (RPC) is a method invocation of one domain, we call it the client, in another domain, we call it the server. Since no two domains share the same address space, we need a communication system between domains. This is achieved via RPC channels, that allow the sending of arbitrary size messages from one domain to another.

Therefore, an RPC invocation essentially boils down to the following sequence of operations: 1. invocation of the RPC on the client, 2. marshalling of the method information and arguments into a message, 3. sending of the message on the client to the server, 4. receiving of the message on the server, 5. unmarshalling of the method information and arguments, 6. invocation of the method in the server, 7. marshalling of the result into a message, 8. sending the result message on the server to the client, 9. receving the result message on the client, 10. unmarshalling the result message.

The RPC layer is responsible for the marshalling and unmarshalling of the method information, arguments and results. We organized our different RPC methods into interfaces (sometimes also referred to as function groups). Each interface is a collection of one or more RPC methods. Each RPC channel has exactly one interface associated with it, i.e. only the methods of the specific interface associated with the channel can be invoked on this channel by a client. However, a server may choose to implement as many interfaces as it wants. If a client needs to call RPC methods from different interfaces that happen to be implemented by the same server, it needs a separate channel for each interface. We chose this design, such that we could begin by making the \texttt{init} domain the server for each interface and then gradually move the server functionality of certain interfaces into their own domains, without ever needing to change the clients.

RPC methods are identified by their function ID, which is the interface number and a unique number per method in their interface. Each RPC method has a defined request and response message payload format. Each message payload format forms a contiguous block of data that can then subsequently be sent and received over the message channel. In most cases this is a structure holding all the method in and out parameters respectively.

\subsection{Message Passing Stack} \label{mps}

The message passing stack glues the RPC layer together with the message passing drivers. It provides a blocking send and a blocking receive method over an arbitrary channel. The send method takes an RPC function ID, a pointer to the message payload and its size, as well as an optional capability to send over the channel. The receive message returns an RPC function ID, a pointer to the received message payload and its size, as well as a possibly received capability.

The message passing stack then uses the channel drivers to send and receive messages. The message format is simple. Each message has a short fixed size header that contains the function ID, an error code as well as the size of the message payload. After the header, the payload follows immediately.

Since both clients and servers need to send and receive messages, these functions are shared between clients and servers. For clients there is an additional method that sends a message and waits until it received a reply message from the server. This method also makes sure, that no interleaving of messages happens. This is important, since whenever the message stack is waiting to send or receive a message, it does not simply block but starts to dispatch events. However, dispatched events possibly again use the same channel in an RPC invocation, so we need to make sure, that no message is sent, before the previous message has finished sending and that the reply message is returned to the right event handler.

Finally, all a client needs to do, in order to invoke an RPC method, is to assemble the request message payload (marshalling), then call the message passing stack to send the message and wait for a reply message and finally unmarshall the reply message payload.

On the server side, a generic event handler is provided by the message passing stack. It is registered as an event handler for message receive events on each channel upon channel establishment. Each server announces a specific event handler for each interface it supports to the generic event handler. All the generic event handler does, is receive an arbitrary message, find out the corresponding interface from the message and invoke the specific event handler of the server, giving it the message.

\subsection{Lightweight Message Passing}

Lightweight Message Passing (LMP) is the primitive to send a fixed size short message to a domain on the same core. It uses the registers of the core to transfer data into the other domain via a system call. Initially, our LMP driver was implemented such that if the whole message (i.e. header and payload) fitted into a single short LMP message, it was sent over right away. If it was larger than the short LMP message, a frame was allocated and mapped and the whole message payload was copied into the frame. Then simply the header and the payload size accompanied with the frame capability was sent in the short LMP message. On the receiver side, when a size larger than the short LMP message was seen, the capability sent in the message was assumed to be a frame, was mapped and the payload could be accessed over this mapping.

However, we soon realized that since we have no real means to free a frame, or rather the associated physical memory anymore, this is a huge waste of memory. So we implemented a simple fragmenting scheme for large messages over LMP which is rather slow, since each LMP message essentially implies two context switches per fragment and instead we invented Fast LMP as described in section \ref{ump}.

\subsection{Channel Establishment}

Until the start of our individual projects, channel establishment for both intra-core and inter-core channels was hardcoded into the logic of spawning new domains and spawning the second core. For each new channel that was introduced, we needed to extend the corresponding code parts individually. This was later greatly simplified with the introduction of a nameserver, providing the ability to dynamically establish channels between domains. This is described in detail in section \ref{nameserver}.


