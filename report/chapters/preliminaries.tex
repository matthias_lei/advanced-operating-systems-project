\section{Preliminaries}

\subsection{General Philosophy}

During the course of this project, we learned the importance of defining clear layers of abstraction with corresponding interfaces, as well as modularization of functionality into manageable and preferably independent parts. Therefore, the general philosophy of our OS is to be as reasonably distributed as possible. We introduced a lot of services that run in their own domain, e.g. a memory service, a process management service, a serial IO service, etc. This proved to be particularly helpful when working on the individual projects, since the different components are communicating with each other over well defined RPC interfaces.

However, the decision for a distributed system like this also comes with a certain price, which is a (sometimes quite significant) performance overhead. We are aware of this and our main goal never was to have a system with the best performance but rather to keep it stable and manageable. Nevertheless, when designing individual components of the system, we always kept performance in mind and tried to come up with efficient solutions.

Moreover, resource clean-up is often much more difficult than allocation. In some cases it is even impossible, for example cross-core capability destruction and revocation are not implemented in our OS, since none of us took the individual project capabilities. Thus, we tried to do resource clean-up as good as possible wherever we could reasonably afford the time. However, we sometimes simply had to cut the corner and do without clean-up at all, since it was just not worth the trouble.

\subsection{Testing Library}

Another key point of our philosophy is: strong development tools help creating better products. Thus, we put a lot of effort into establishing a testing framework from milestone 1. It is built as a library that is linked in different flavors to init (monitor0) and to a separate test program Artemis (launched with \texttt{run a}). This framework helped us writing test cases quite easily and in a standardized pattern. Therefore, we probably wrote more test cases than we would have written if all had to be written from scratch. Our additional extensions (e.g. stack trace with function symbol resolution) also simplified debugging and therefore development speed. They are discussed later in more detail.

\subsection{Software development process}

In our philosophy, a robust software product has its foundation in a strong software development process. Thus, we discussed design ideas in the group and considered dependencies with other/future parts of the project before starting to write code. Of course, not everything could be foreseen. But good planning and then modularization of the work into individual code segments with responsibilities for each member of the team helped us a lot to keep the project organized.

Additionally, we had a very strict quality assurance by formalized merge requests (192 in total) and code reviews before merging new code into the main development branch. Besides detecting bugs before they became part of the stable code base, this helped us as a group in particular to get to know the entire code base and learn from each other strengths in writing good quality software in C. This needed more time to begin with. But, the robustness pays off in the long run. On the other hand, all hands were always "on deck" when a bug was detected. It was amazing to see so many efforts even late in the nights.

We think we could not have been able to write such a robust software system without these key components.