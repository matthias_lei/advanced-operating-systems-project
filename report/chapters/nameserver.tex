\newpage
\section{Nameserver} \label{nameserver}
Individual project by Pirmin Schmid.

\mypar{Associated services} new nameserver (Gaia), extended monitor service (Poseidon), new intermon service (Poseidon's wake).

\mypar{Associated user program} Name service user interface (Apollo).

From the beginning, we had our services well organized in separate function groups and separate implementation files that could be linked together as needed. Nevertheless, the entire system was kept monolithic, i.e. all services provided by init (monitor/0) for the entire time of the core development.

We realized early on, that a strong name service is needed for establishing a distributed system where each service can be offered by a separate domain also running on either of the available cores. Also the advantages of late binding became very clear. It would be almost unfeasible to extend the number of message capabilities in well-known locations during spawn every time that a new function group / service is added. And looking at the individual projects, several of such new services could be seen.

The additional limitations of the communication channels (LMP not cross-core; UMP with ringbuffers without direct cross-core interrupts in our implementation) and limitations on sending capabilities by messages (no direct capability transfer possible between cores) proved to be harder challenges than initially anticipated. Additionally, this project brought many changes to the established messaging and spawn systems, and needed a reorganization of the bootstrap mechanisms, which is harder to accomplish than in the configuration before with all services available within monitor/0.

\subsection{Concept}
Based on our established unified messaging concept with agnostic RPC channels with respect to actually used messaging system (LMP, FLMP, UMP) and even "local channels" without actual message transfer, a distributed system of services was built with the following key elements:
\begin{itemize}[noitemsep]
\item Low number of services needed at well-known locations during spawn, which also reduces the bootstrap problem
\item Strict but flexible hierarchic namespace that guarantees unique names for each registered service
\item Nameserver that allows registration and lookup of available services, including rich meta data
\item Late binding with services with as little manual adjustment as possible
\item Transparent intermon re-routing of messages with capabilities during send or receive
\item While only RAM and frame forging could be used in monitors, protect against "forging arbitrary address regions"
\end{itemize}
And indeed, such a concept could be established that works in the background as much as possible with as little input needed from higher code levels as possible. Additionally, it was a goal to provide basic functionality (registration, lookup, bind) early during the individual projects such that all team members could start using the late binding system.

\subsection{Namespace}
A hierarchical namespace is established for all services. A service name must adhere to the following rules:
\begin{itemize}[noitemsep]
\item Scheme: \texttt{<class>/<type>[/<subtype>]/} \\ \texttt{<enumeration>}
\item a service name is the full absolute paths starting with /
\item subtype is optional and not used at the moment for the small current system
\item the characters of all \texttt{<class>}, \texttt{<type>}, \texttt{<subtype>} parts must be in \{'a'..'z', '0'..'9', '\_'\}
\item \texttt{<enumeration>} is a string representation of a valid size\_t value and is assigned by the name server upon registration
\end{itemize}

At the moment, the following classes are used:
\begin{itemize}[noitemsep]
\item \textbf{core} for core system services like monitors, memory, serial I/O,...
\item \textbf{dev} for devices such as filesystem and network
\item \textbf{app} for applications such as shell services
\end{itemize}

Additionally, a short name can be used that conforms to the rule \texttt{<prefix><enumeration>}. \texttt{<enumeration>} is identical to the full name. \texttt{<prefix>} can be considered a shortcut for \texttt{/<class>/<type>/<subtype>/}. Characters of \texttt{<prefix>} must be in \{'a'..'z'\}

And finally, services are registered with rich meta data information, that is stored in a serializable key value store and that can also be used for lookup. Keys must be in \{'a'..'z', 0'..'9', '\_'\} with reserved keywords such as class, type, subtype, enumeration, name, short\_name, which are added during registration. Values are strings.\footnote{see also \texttt{include/nameserver.h} for additional details}

\subsection{Name service: Gaia}
The name service provides data collection about all available services of the system, and most importantly plays the role of a "chaperon" when a domain (user program or service) needs a connection with an already existing service.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{images/late_binding_1.png}
\caption{Service registration and lookup}
\label{fig:gaia1}
\end{figure}

\subsubsection{Registration} 
Services register themselves at the nameservice providing key information (Figure \ref{fig:gaia1}): a name in the namespace (without the enumeration), interface/service type (corresponding to the function group FG), core\_id, bandwidth information.\footnote{high bandwidth is used for FLMP on same core connections, low bandwidth for LMP; UMP is used for cross-core connections; note: because LMP endpoints cannot be forged, UMP is also used for same-core connections on the core 1 (Gaia runs on core 0).} These and additional meta data are stored in the name service. The registering service receives its handle and a specific registration key (basically a hashsum) upon success. This registration key is used for additional functions like deregister, ping and update. It prevents arbitrary other domains to modify the service data.

A unique enumeration is provided for each service. Thus, based on provided registration name and this enumeration, a truely unique name is established for each registering service. Each monitor e.g. registers itself as \texttt{/core/monitor}; the first becomes \texttt{/core/monitor/0} and the second \\ \texttt{/core/monitor/1}.

In detail, the registration wrapper \\ \texttt{aos\_rpc\_service\_register()} actually performs various registrations.
\begin{itemize}[noitemsep]
\item registers the proper event handler in the server that calls this function
\item registers the service to the name service via \\ \texttt{aos\_rpc\_name\_service\_register()}
\item registers the service with its monitor for intermon services; this simplifies intermon service resolution later
\item registers periodic auto-ping event in the server (deliberately not active at the moment; not yet tested enough)
\end{itemize}

Please note: a good documentation of our extended RPC API can be found in \texttt{include/aos/aos\_rpc.h}.

\subsubsection{Lookup}
Various lookup methods are offered to find the desired service (Figure \ref{fig:gaia1}). Typically, a full name (or prefix) is used at the moment for simplicity. However, any information can be used for filtering / searching that is stored in the key value store. The received service handle can be used to get detailed information about a service (a copy of its key value store is returned) or to bind with that service.

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{images/late_binding_2.png}
\caption{Late binding}
\label{fig:gaia2}
\end{figure}

\subsubsection{Bind}
A services handle, received by a lookup operation, can be used to establish a connection with the desired service. For this, a bind operation is called on the name service (Figure \ref{fig:gaia2}), which allows all domains late binding with any service. The name service plays the role of a chaperon. Based on information in the bind request (e.g. core id of the requesting client) and stored information in its database (e.g. high bandwidth), it requests a matching channel endpoint at the actual service (LMP, FLMP, or UMP). This endpoint is then forwarded to the client that can establish an actual communication channel with the service. Also routing information is provided to the client. This routing information consists of 2 bytes (note: coreid\_t is limited to one byte) that basically holds the sender and target core ids. If they are not equal, the re-routing system can later easily determine whether re-routing is needed. No re-routing is done of course if both core ids match.

For improved convenience of the client program code, there is a wrapper for the actual lowlevel bind operation that makes the actual server connections. The current system provides a very convenient high level function \\ (\texttt{aos\_rpc\_get\_channel()}\footnote{defined in \texttt{lib/aos/rpc\_client/aos\_rpc\_client\_common.c}}) that is used in all regular RPC channel establishing functions. These late binding functions are also used to create socket connections with the network service.

Despite the convenience, several communications need to go correctly in the background to establish proper connections, of course. It works well and robustly so far.

\subsection{Monitor service: Poseidon}
In the initial monolithic system, a "global event handler" in \texttt{init} provided all the services, and some services (e.g. memory service were additionally included into the monitor of core 1). In the new distributed system, each monitor offers only specific monitor services (for any domain; typically running on the same core), a spawn service (used by the process service Demeter), and intermon services (specifically available only for other monitors). Init on core 0 additionally offers the device service (to avoid copying that capability).

\begin{figure}[h]
\centering
\includegraphics[width=\linewidth]{images/rerouting.png}
\caption{Re-routing for capabilities}
\label{fig:gaia3}
\end{figure}

\subsection{Intermon service: Poseidon's wake}
The initial UMP frame, that is prepared while spawning the 2nd core, is used to create two client $\leftrightarrow$ server connections between the monitors. Thus, each monitor can act as client and as server to the other monitor. This system is needed to establish an automatic re-routing of messages that send capabilities or expect to receive capabilities via an UMP channel connection between cores. In contrast to LMP channels used for communication between domains on the same core, capabilities cannot be transported by UMP messages and in particular cannot be transported directly between cores. This intermon communication channel is a key backbone for the re-routing system that is described below.

On a system with more than 2 cores, such an intermon connection would be established between each pair of cores to allow direct routing of messages between arbitrary cores.

\subsection{Automatic re-routing of inter-core messages with capabilities}
The messaging system, that was described before in detail, was extended by additional meta information in the communication context. For each RPC message, there is known whether a capability is sent or expected to be received by the message. Thus, if either of them indicates a capability, and the known routing information of the RPC channel indicates a cross-core communication, the message is automatically re-routed (Figure \ref{fig:gaia3}). This re-routing works completely transparent for the rest of the communication stack above. There is just a check on the described conditions in \texttt{aos\_rpc\_generic\_send\_and\_recv()}\footnote{implemented in lib/rpc\_shared/aos\_rpc\_shared\_common.c. See also detailed comments / documentation there on details.} If a re-routing is needed, then a \\ \texttt{aos\_rpc\_monitor\_request\_rpc\_forward()} is called on the monitor RPC channel despite of proceeding with message sending directly.

The original message is embedded into a specific forwarding envelope that can take additional information, in particular capability information.

The monitor service then forwards the message over the proper intermon channel (based on routing information) to the target core (only 2 cores here at the moment, but the system would work with up to max. coreid cores in principle). On the target intermon server, the proper service is called by proxy, and the reply returned on the path as received.

In practical tests, this systems works robustly. Of course, there is some delay in the RPC calls. At the end, 3 hops are made instead of one hop to fulfill the request.

\mypar{Actual capability transfer} As mentioned, nobody of our group has chosen the capability project. Thus, capability transfer between cores is limited to forging RAM and frame capabilities without additional hardwired checks by the capability system. For the intermon transfer, a capability info (base and size) is determined, these key data embedded in the intermon message envelope, and then the capability is forged (either RAM or frame) on the target core. This forged capabiltiy is then forwarded upstream to the actual recipient of the message (either as request or as reply).

\mypar{Alternative systems} This re-routing over 3 hops and embedding of messages into envelopes seems to be a bit complex and time consuming. Indeed, some tiny bugs showed to be time consuming until resolution. An alternative much simpler system was considered: Regular envelopes used for UMP cross-core connections could just have space reserved to also transport capability information of RAM and frames between arbitrary domains. Upon reception of such a message, the domain could call an RPC in the monitor that could simply return a RAM or frame capability based on these key information. No re-routing needed.

However, this solution was discarded rather quickly because of security concerns. If a domain could request forgery of any kind of capability just by providing base and size of a RAM region, this could open lots of problems. Thus, the current solution was considered to be much more robust. With the current system, it is guaranteed that only monitors actually determine these RAM / frame capability information. Thus, required trust is reduced to monitors. Additionally, the intermon RPC system is "protected" with a check that these RPC calls can indeed only be made from monitors.

We are aware that the actual capability system implementation would have brought additional features that cannot be provided with the current solution that is based on forging RAM and frame capabilities.

\subsection{Adjustments in spawn, common service RPCs, \\ other things}

\mypar{spawn}
Spawn during the monolithic system started to include more and more capabilities at well-known locations to establish connections directly with offered services. This development was additional motivation to implement a name service. More and more capabilities at well-known locations would just not scale. Thus, the spawn system was adjusted to provide monitor and name server capabilities to new domains for establishing RPC channels. Based on these channels, all other RPC channels are connected with late binding (including memory). During bootstrap partially even less information. The spawn process could also work with only a monitor channel capability at a well known location. However, then part of the bootstrap problem (getting name service) would repeat itself with each spawn. Thus, our solution with monitor and name services for regular spawns work quite comfortably. The bootstrap part is an exception to this rule as described.

The current flow of spawning a program goes through process service (Demeter) that keeps track of the domains (see domman). Demeter has direct RPC channels to all available monitors and can send launch RPC calls directly to the target monitor for the core that is requested for a binary. Thus, each monitor also offers a spawn service.


\mypar{common service}
Some functions needed during spawn (e.g. establish LMP connection with spawning monitor) were generalized into the common service function group that is offered by all services. Many of the concepts that were individually implemented at various locations could be integrated und summarized at such generic locations.

\mypar{Late switching from fixed RAM allocator to actual allocator} Each domain is launched with 256 pages of 4 KiB RAM. In earlier versions, this fixed page RAM allocator was replaced by the remote memory allocator that works with Dionysos memory service very early. This leads to loss of several 100 KiB per launched domain (as tested with some domain launches; exact numbers depend on domain). Despite wasting memory, it also puts quite some stress to the late binding and memory system during each launch of a new domain if this switch happened already during \texttt{barrelfish\_init\_onthread()}.

Thus following the principle of late binding, also a late switching from this simple fixed allocator to the actual RAM allocator was implemented. Basically, \texttt{ram\_alloc\_fixed()} was modified to make the switch to the proper RAM allocator (using Dionysos) when it is needed (either none of the 256 pages left, or request that could not be fulfilled due to size $>$ 4 KiB). This saves memory and more importantly removes some stress on the memory and messaging system shortly after launch of a domain.

\subsection{Bootstrap of the distributed multi-core system}
As soon as key services are available -- i.e. monitors, intermon, memory, name and process services -- all new domains can be launched via regular process service RPC call (Demeter). Before this stable situation some specific precautions must be made during the bootstrapping. Gaia e.g. is launched without memory service. It is added later when Dionysos actually registers.

Thus, the entire bootstrap process was reorganized while distributing the services from monolithic system to separate domains. Some specific RPC functions have been introduced just to facilitate this bootstrap mechanisms.

The current bootstrap process follows this sequence, which is also documented in the source code of \texttt{usr/init}. Specific synchronization primitives are used to keep spawning of the services in sync.
\begin{itemize}[noitemsep]
\item spawn memory service Dionysos (only receives \\ /core/monitor/0) $\rightarrow$ /core/memory/0
\item init loads most RAM to Dionysos. Several MiB have been split away for a local memory allocator to guarantee proper bootstrapping and spawning of the 2nd core.
\item spawn nameserver service Gaia (receives /core/monitor/0) $\rightarrow$ /core/name/0; nameserver connects with /core/memory/0 upon registration of the memory service
\item RPC sent to Dionysos to register himself to Gaia; Gaia uses this opportunity to connect with memory service
\item spawn new monitor on core 1; receives /core/monitor/0 as intermon connection and /core/name/0 $\rightarrow$ /core/monitor/1; establishes connection with memory service dynamically using late binding
\item spawn process service Demeter $\rightarrow$ /core/process/0
\item RPC call to Demeter to manually add init, Dionysos, Gaia, monitor/1 and Demeter to the list of spawned domains including domain ids
\item the rest of the system can use regular spawn via Demeter, i.e. the other services may be launched on either core 0 or core 1
\item serial I/O Hermes $\rightarrow$ /core/serial/0 is the first of these "regular" spawns
\item spawn of other services; each registers itself with Gaia
\item Zeus is launched and offers the shell to the user
\end{itemize}


\subsection{Additional goodies}

\subsubsection{Rich key-value store}
As mentioned in the project description, the name service was built to offer rich meta data for each service. For this purpose, a serializable key-value store was implemented. It bases on the provided hashtable implementation but offers various improved features, most notably a built-in serialization / deserialization method that comes very handy for all the message payload transfer in the RPC calls. Additionally, it offers an iterator for a lambda function.

Thanks to this improved container, the nameserver implementation could be kept simpler and also the nameserver client UI program (Apollo) could be kept quite short.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\linewidth]{images/auto_deregister_screenshot.jpg} \\
\includegraphics[width=0.8\linewidth]{images/auto_deregister_screenshot2.jpg}
\caption{Automatic deregistration of a service}
\label{fig:gaia4}
\end{figure}

\subsubsection{Auto-ping with deregistration}
As mentioned in the project description, a mechanism was implemented that allows automatic deregistration of services that may have stopped working. This is implemented by additional timestamps that are stored in the nameserver database for each service. Each server can register a periodic event in its deferred events infrastructure -- for which we have implemented various fixes to make such things work -- that automatically sends a "ping" message to the nameserver (e.g. 1x/min). The nameserver updates the timestamp when receiving this ping.

Additionally, a second thread is launched on the nameserver that periodically (e.g. every 2 minutes) checks whether the timestamps of the services are not outdated. Services with outdated timestamps are deregistered automatically. \\ This worked in testing. There is an additional test service that registers with the nameserver but does not start this autoping. After some time it is deregistered.

Of course, this feature is still of limited usability. There is no proper cleanup implemented for domains. Additionally, there is no automatic re-launch of such stopped services implemented yet. To offer a full service into this direction, this would also need "hot-swapping" of already connected clients with such a non-responsive client and replacing these RPC connections there with a fresh one. This comes with quite some complexity that the present code base is not (yet) prepared for in general.

Thus, based on this limited practical functionality and also late actual implementation of this feature, it was \textbf{deactivated}\footnote{see code toggles in \texttt{lib/nameserver/nameserver.c} lines 640-647 and \texttt{lib/aos/rpc\_client/aos\_rpc\_client\_name.c} lines 631-637} for the demonstration and also for the present submission. This also goes hand in hand with our philosophy of providing a robust operating system as a key priority.

While it was important for me to provide the needed basic functionality of register, lookup and bind as early as possible to my colleagues for their individual projects (which needed quite an effort), this additional feature here could not be implemented before close to the deadline despite having been planned from beginning.

It is obvious that such periodic contacting of the nameserver by all services changes the timing behavior of the entire distributed system. As seen in other chapters, already small changes can have quite an effect on the system whether e.g. a small change leads to loosing the current active time slice and having to wait for a next one.\footnote{Of course, this reminds of the well-known butterfly effect of chaotic systems.}

Thus, it would not have been responsible to have such a feature active with only very limited test experience.

\subsection{User interface: Apollo}
A simple user interface for the name service has been integrated into the shell's built-in commands: apollo. Similar to other commands, it offers an overview of the options with the \texttt{-h} argument. Started without arguments, it gets the entire list of registered services and prints the list sorted by name. The \texttt{-d} argument offers details on all results, i.e. prints the entire copy of the key value store that was received for each of the replies.

It offers two filter modes. One is based on the path in the namespace (\texttt{-p path}). E.g. \texttt{apollo -p /core} would list all core services. The other mode works directly with the key value store. In the currently simpel implementation, up to two key-value pairs can be provided that are tested with logic AND. One pair can be provided with \texttt{-k key -v value}; another pair with \texttt{-l key -w value}. This restriction is only based on the simpel user program. The filter mechanism implemented in the nameserver could handle abritrary number of key-value pairs. As an implementation detail: lambda functions are used in the implementation for both, iterating over the filter key value store and also over the database that is stored in the nameserver.


\subsection{Name service: concluding remarks}
Although almost nothing can be seen from the name service that just works in the background, it has been a very rewarding feeling to see that the current system works robustly despite making quite heavy use of all these late binding mechanisms offered by the name service. They are involved during bootstrap, during spawn of each domain, during connection of the filesystem, while establishing new network sockets, and while spawning new Zeus shell instances, when new additional \texttt{/app/shell} instances are registered for the domains that are launched from within the new shell.

Additionally, it has been a goal of the implementation to hide as much complexity of this system in the background. I think that this goal has been achieved: Intermon re-routing in case of capability transfers works completely transparent within the messaging system. Besides a longer latency, the re-routed RPC channel is completely unaware of this feature. Server registration (on various registration levels) basically happens with creation of a key value store and call of one registration function. And finally, the late binding mechanisms are completely hidden behind the RPC get channel functions (except during bootstrapping of course).

Of course this powerful service could not have been built without the already existing strong messaging system and the very robust base system (e.g. memory and paging system) that had been created together during shared core OS development. I also gratefully acknowledge the insights and feedbacks that I got from my colleagues during testing and bug hunting in this individual project. Thanks, guys!
