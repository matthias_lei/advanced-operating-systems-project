\section{User-level Message Passing} \label{ump}

This section is about the inter-core communication driver called User-level Message Passing (UMP). Since LMP uses the core registers to pass data to the other domain, this can no longer be used to pass data if the other domain runs on another core. Instead, we use a shared memory region that is mapped into both domains on the different cores to pass data between the two domains.

\subsection{Ringbuffer}

A channel allows bidirectional communication between a client and a server, that is the client needs to be able to send a message and receive a reply and the server needs to be able to receive a message and send a reply. Therefore, in UMP, each channel consists of two ringbuffers, one for each direction.

\subsubsection{Setup}

During the initialization of the UMP channel a 4 KiB page is split between the two ringbuffers. Each of those 2KiB buffers is then set to zero in order to initialize a clean state. We divide every buffer into 32 byte fields with the same alignment in order to exploit the 32 byte cache line size which is crucial for the performance of the UMP ringbuffer. The last byte of a field contains the flag bit representing the ownership of the field. Due to the fact that the entire buffer is set to zero initially the cleared state of the ownership bit represents ownership of the producer. Conversely a set ownership flag represents ownership of the consumer. The remaining 31 bytes are available as payload. At last, the producer index of one side as well as the consumer index of the other side are both set to point to the same first field.

\subsubsection{Sending messages}

A producer sends its message by writing into its current field. After that the ownership flag is set. These two operations must be divided by a fence. Because of the weak memory model we operate in, the order of these two write operations on the producer core are otherwise not guaranteed to be seen by the consumer core. This could lead to an early transfer of ownership such that the consumer reads out the payload before the whole message has been written. The last step is then to advance the current field by one.

\subsubsection{Receiving messages}

In order to receive messages every ringbuffer is associated with a waitset which is enqueued into the POLLING queue. The polling function simply checks whether the ownership flag is set at the current field and if so an event is triggered on the consumer side. Upon dispatching this event the consumer reads out the payload, clears the ownership flag and advances the current field index by one. Also here, we have to make sure to insert a fence between the extraction of the payload and the clearing of the flag in order to guarantee correctness. Otherwise the producer could write preemptively into the field before the payload has been read out.



\subsection{Unified Message Passing Stack}

When designing the message passing stack for LMP, we already had in mind, that we later need to implement message passing between cores, that needs another message passing driver. Therefore, our RPC layer and message passing stack were already written with almost no specific assumptions about the underlying message passing driver.
Up until now, the message passing stack directly used the LMP driver functions to send and receive messages. What we now had to do, is simply replace these direct function invocations with function pointer invocations that are stored in a virtual function table of the channel. When establishing a channel, it is known whether LMP or UMP is used and the function pointers in the virtual function table are set accordingly.

\subsection{Fast Lightweight Message Passing}

As mentioned in section \ref{lmp}, fragmenting large messages over LMP is rather slow. However, with UMP we now have a ring buffer implementation that we could just as well use to speed up LMP. Moreover, with our virtual function table for the message passing driver functions, it became really easy to introduce yet another message passing driver.

Essentially, Fast LMP is just the combination of LMP with a UMP ringbuffer. Messages fitting into the short fixed size LMP message are sent directly, however, for larger messages, only the header and payload size are sent in the LMP message to indicate the arrival of a message. On the receiver side, the message is consequently read from the ringbuffer if it did not fit entirely into the LMP message.

\subsection{Local Channels}

While starting to think about making our system more distributed by moving out server functionality from \texttt{init} into separate domains, we realized that for functionality that is available over RPC, we would need to introduce special cases in the servers implementing the RPC functionality. For example allocating physical memory is an RPC method, but how does the server implementing this method allocate physical memory itself? It would need to have a channel to itself in order to use RPC.

So what we came up with is what we call local channels. Until now, we only had remote channels, that is actual channels using LMP/FLMP/UMP. All remote channels follow the same pattern: 1. invoking the remote procedure, 2. marshalling, 3. sending the message, 4. unmarshalling, 5. invoking the local server procedure. Now, if we are already on the server, we don't actually need points 2-4 but we could just as well invoke the local sever procedure directly and this is exactly what local channels do.

Basically, they are just an additional layer of indirection in the RPC layer. Each RPC channel now has a pointer to a virtual function table storing function pointers for each method of the associated interface. On a client, these function pointers point to the remote implementations, that do the marshalling, message sending, reply receiving and reply unmarshalling, however, on the server, these function pointers point directly to the local server procedures.

Using local channels, we thus don't need any special cases on the servers anymore. We simply need to setup a local channel instead of a remote channel, but then we can use the RPC methods as if we were a normal client.
