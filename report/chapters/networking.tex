\section{Networking}
Individual project by Matthias Lei.

Every user application can now open sockets in order to establish a connection to the network driver and use this socket for sending to and receiving packets from the network.
The network driver, which we named Charon, acts as service with its own domain and can handle arbitrary numbers of open connections up the maximum number of ports.

The main idea for the the network functionality was to process incoming and outgoing packets through the network layers as it is already common in today's network drivers. In the following subchapter we will present the general architecture of the network functionality and give a brief overview of different features we implemented along the way which include:
\begin{enumerate}
  \item UNIX socket API as client interface
  \item decoupled sender/receiver queue
  \item lookahead buffer for incoming messages
\end{enumerate}

\begin{figure*}[h]\centering
  \includegraphics[scale=0.5]{images/aos_network_stack.pdf}
  \caption{Network stack architecture of OlympOS \label{network_arch}}
\end{figure*}

\subsection{Network Stack Architecture}

As depicted on figure \ref{network_arch} the network stack is divided into two parts. On the client side the familiar UNIX socket API is provided to every user application with a subset of its features. The implementation of this interface revolves around mapping between RPC states and socket id's and forwarding the parameters via RPC channels to and from the network driver.\\
On the server side the network driver handles packet processing from the transport layer, where depending on the chosen protocol the packets are passed through the respective protocol handlers, through the common IP layer down to the SLIP layer which represents a simple link layer.

The whole network stack is implemented with the intention to be easily extendible in terms of protocols. For this project only UDP and ICMP have been implemented.

\subsubsection{UNIX Socket API Layer}

Our goal was to use a subset of the functionality of the familiar UNIX socket API and implement the behaviour of it as close as possible such that future users of this OS can work with an already known interface. Out of the 17 functions of the official interface we implemented 5 which are:
\begin{enumerate}
  \item \texttt{socket()}: in the context of this course only takes the argument tuple (AF\_INET, SOCK\_DGRAM, IPPROTO\_UDP) in order to establish a UDP socket.
  \item \texttt{bind()}: additionally to choosing the IP address and port this function also supports dynamic port assignment (with port 0) as well as INADDR\_ANY where then the default IP address 10.0.2.1 is assigned. The address family is limited to AF\_INET.
  \item \texttt{getsockname()}: returns the assigned IP address and port number.
  \item \texttt{recvfrom()}: does not support the flags field and is therefore a blocking call.
  \item \texttt{sendto()}: does not support the flags field and is therefore a blocking call. This function will return as soon as the message is enqueued in the transport layer.
\end{enumerate}
The other functions are kept as stubs. Some because of the fact that in the context of a connectionless protocol like UDP functions like \texttt{send()} and \texttt{recv()} do not make sense. Others would simply go way beyond the scope of this individual project.

\subsubsection{Remote Socket Interface Layer}
This layer stores the mapping between socket ID's and its state. The state of a socket primarily contains the RPC channels to the network driver. The socket ID is unique within a domain. A consequence of this property is that socket ID's from the network driver have to be translated. The benefit of this can be seen as no arbitrary socket ID can be passed to the RPC channels in order to trigger calls to sockets which do not belong to the user application.

The \texttt{socket()} call requires special attention in this layer as this is generating a new socket ID. For the OS one socket represents nothing but one RPC channel from the user application to the network driver. In the case of multiple sockets multiple RPC channels are maintained. When \texttt{socket()} is called a \texttt{bind()} request is made to the name server which then sets up the RPC channel between the two parties (note that this \texttt{bind()} comes from the nameserver, not from the socket API). After the setup the parameters of the \texttt{socket()} call is simply forwarded to the network driver. Upon successful reply a new ID is assigned to this RPC channel.

\subsubsection{RPC Layer}
This layer does nothing else than forwarding the parameters handed from the upper and lower layers. In the case of a buffer pointer handed down during a \texttt{receive()} just the length is passed and the buffer acts as the receive buffer.

\subsubsection{Local Socket Interface Layer}
Similar to the Remote Socket Interface Layer the local version stores also a mapping between generic states and its ID. The genericity comes from the fact the socket supports different protocols. For every state the protocol and the pointer to its specific state is stored. From the protocol the driver calls the corresponding vtable in order to process request and passes down the state pointer.

\subsubsection{UDP Protocol Layer}
Until and including this layer almost all the public functions were UNIX socket API conform. As the lower layer does not adhere to this interface anymore this layer acts as the gluecode between the two layers.

One big challenge posed to be the blocking \texttt{recvfrom()} behaviour on the server side. In previous versions of the driver this call would just poll on its receive buffer for incoming packets while dispatching events in order to ensure progress of every socket state. Such a dispatched event could be another \texttt{recvfrom()} call causing polling within a polling loop. Therefore an incoming packet dedicated to the first \texttt{recvfrom()} call will not be picked up until the second \texttt{recvfrom()} has finished its polling. This situation can occur recursively such that the packets have to arrive in reverse order of the \texttt{recvfrom()} call order for unwinding this recursive polling. The countermeasure we applied was not replying to the RPC request from the upper layer and let only the client block. At the same time a reply callback is installed for this socket which is called as soon as a new packet for this socket is placed in its receive buffer.

\subsubsection{UDP Implementation Layer}
The UDP Implementation Layer keeps track of the states of all open UDP sockets in the entire OS. A socket state contains information about the IP address the socket is bound to as well as its port. But more importantly in regard to the performance of the network driver is that in every socket one sender and one receiver queue is stored. This decouples the upper layers from the lower layers. In the case of incoming messages this means that the path from the serial line to the first buffer is kept shorter than delivering it directly all the way to the user application. This is essential as the UART FIFO buffer is only 64 bytes small and can easily be overrun while a packet is processed to the client. The same argument applies for the opposite direction. Having an intermediate buffer when the client sends a packet to the network opens up more opportunities for the driver to handle serial interrupts before a UART buffer overflow occurs.

This layer also handles all the packet processing on UDP level. This includes checksum calculation, port multiplexing, header generation and parsing.

\subsubsection{IP Protocol Layer}

The IP Protocol stores and declares an array of protocol receive handlers which have to be implemented by the upper layers. For every incoming IP packet the respective receive handler is called according to its protocol field in the header.

For the sending packets the transport layer protocols can use their already provided send functions which will then set the according identifier in the IP protocol field.

If future workers wish to add more transport protocols to the OS the receive handler has to be implemented and added to the protocol array. Furthermore the sending function has to be implemented which sets the protocol field before it is being handed down to the lower IP layer.

\subsubsection{IP Implementation Layer}

The IP implementation layer takes a similar role to the UDP Implementation Layer in regards of packet processing. The IP checksum check and generation are done on this layer. Additionaly it is responsible for assembling characters from the lower SLIP layer into IP packets towards the upper layer. From the upper layers it will accept transport layer (and ICMP) packets and send it characterwise via the SLIP layer over the network.

For assembling characters from the SLIP layer into IP packets we implemented a state machine. The events of the state machine are triggered by two handlers which are registered in the lower SLIP layer. The \textit{character handler} is called whenever a regular characters is handed up from the SLIP layer. The \textit{end handler} is called whenever the SLIP layer receives a SLIP\_END character signalizing that the transmission of the current packent is completed. The state machine consists of three states:
\begin{enumerate}
  \item STATE UNKNOWN: This is the initial state of the state machine. Only when the end handler is called the state changes to STATE HEADER. STATE UNKNOWN also acts as the fallback state whenever errors occur. During that state all incoming characters are ignored. Instances of those errors can be: unmatched packet size with the IP length field, wrong checksum, wrong ip version, etc.
  \item STATE HEADER: All characters received from the character handler will be written into a static lookahead buffer. The size of the buffer is the maximum IP header length size which is 60 bytes (if all the optional fields are exhausted, otherwise the non-header space is used for payload). The minimal size is 20 bytes. If the buffer is full the content is first checked by the checksum and upon success a memory region with size of the packet will be dynamically allocated. The content of the lookahead buffer is then copied to the newly allocated region and the state transition to STATE PAYLOAD is made.

  If the end handler is called during this state and the number of characters so far received has not reached the minimum header size the packet is dropped and the state goes back to STATE UNKNOWN. If the minimum header is reached and the header is correct then the header and the payload are also copied into an allocated memory region and its pointer is passed directly to the upper layer. After dispatching the packet the state is remains in STATE HEADER and the lookahead buffer is cleared.
  \item STATE PAYLOAD: All characters received during this state are appended to the header in the allocated region. If the end handler is called and the length of the packet matches the size in the IP total length field the pointer to this region is passed to the next layer. If this condition does not hold then the packet is dropped. In both cases the next state will be STATE HEADER again.
\end{enumerate}
Our choice of implementing a static lookahead buffer was to reduce the total amount of memory the network driver requires. Especially since the maximum packet size is very small due to the 64 byte UART FIFO buffer. The alternative would have been to allocate an array of 64 KiB (maximum IP packet size) as a FIFO queue.

\subsubsection{SLIP Layer}

The SLIP Layer is responsible for escaping and deescaping the control charactes of the SLIP protocol. For incoming characters this is also implemented using a state machine with two states:
\begin{enumerate}
  \item STATE REGULAR: This states simply forwards all characters to the upper layer except for the SLIP\_ESC and the SLIP\_END character. The escape character will switch the state machine to the STATE ESCAPED state. The SLIP\_END character triggers whatever end handler is registered from the upper layer signaling the completion of an IP packet.
  \item STATE ESCAPED: This state only takes one character before switching back to STATE REGULAR. The characters expected are the escaped characters of the SLIP protocol. If an unexpected character arrives it will be dropped and the state switched back.
\end{enumerate}

\subsubsection{ICMP Protocol Layer}

Alongside the whole UDP Layer an ICMP Layer is also implemented. ICMP only operates in the network driver and for the course requirement only the ping protocol is implemented. Also here additional ICMP messages can easily be added.

\subsection{Performance}

We measure the performance of the network stack in terms of time required to cross all layers from top to bottom and vice versa. Therefore we will send and receive UDP packages and measure the time it takes between the network client and the serial input.

\subsubsection{Setup}

Our benchmarking setup for sending and receiving UDP packets is as follows:
\begin{itemize}
  \item A UDP packet of 64 bytes size is sent/received every second (including IP and UDP header).
  \item In both, the receive and the send benchmark, 64 packets are used.
  \item The measurements were taken after a fresh boot. This means no other user applications are running.
  \item The benchmarking client and the network driver are running on the bootstrapping core.
\end{itemize}

We sample the time in 4 phases. These are listed in the order of receiving a packet:
\begin{enumerate}
  \item The time spent between the serializing/deserializing the packet in the serial driver and the UDP buffer.
  \item The time spent in the UDP buffer.
  \item The time spent between putting/fetching the UDP packet at the buffer and the RPC channel in the network driver.
  \item The time spent for a UDP packet in the RPC messaging system between network driver and client.
\end{enumerate}

Therefore in the case of a receive we call these phases in the order stated above: \textit{prebuffer-, buffer-, postbuffer-, rpc-phase}.
For sending a packet these phases are denoted in the reverse order.

\subsubsection{Send Benchmark Evaluation}

\begin{figure}[h]
  \includegraphics[width=\linewidth]{images/net_send_bm.pdf}
  \caption{Benchmark results for sending a UDP packet \label{net_send_bm}}
\end{figure}

As depicted in figure \ref{net_send_bm} the average time for sending a UDP packet from the client domain into the network lies around 40 milliseconds. The vast majority of the time is spent sending the package over the RPC channel. The second biggest timeframe lies in the post-buffer phase where the latency is directly tied to the baud rate of the UART serial driver. Increasing the baud rate helps to reduce the latency of this phase. The pre-buffer and buffer phase are insignificant compared to the rest. In the pre-buffer phase the UDP header is generated for the received payload and therefore the latency is only dependent on the clock frequency and the implementation. The time spent in the buffer phase varies heavily on the load of the network driver. As soon as the packet is put into the FIFO buffer the network driver triggers an event and enters the event loop. Potential other events will cause the packet to stay longer in the buffer. Because no other events are pending the packet will immediately fetched by the other side.


\subsubsection{Receive Benchmark Evaluation}

\begin{figure}[h]
  \includegraphics[width=\linewidth]{images/net_recv_bm.pdf}
  \caption{Benchmark results for receiving a UDP packet \label{net_recv_bm}}
\end{figure}

Figure \ref{net_recv_bm} represents average time from the SLIP layer up to the client. It is easily observed that the latency on the receive path is massively greater then on the send path. This asymmetry is due to a performance tweak we made on the \texttt{serial\_poll()} function. Instead of reading out characters until no characters can be read it now will still try to read from the UART for a given number of tries. The latency increase is found in the buffer phase. The polling starts after the last character of a packet has triggered the end handler of the IP layer which in return adds the packet to the buffer. This \textit{post-polling} mechanism prevents the UART buffer from overrunning faster. But it comes with higher latency.


\subsection{Future work}
Again, because of the small UART buffer the greatest benefit in terms of throughput and reliability of incoming characters is to shorten the distance even more between reading out from the serial line and the first intermediate buffer. Thus the interrupt handler's only function should be to write into a FIFO queue without any header parsing and potential mallocs as it is currently the case. Essentially this boils down to a serial driver as an own process and service. As a buffer ideally the shared ringbuffer of the UMP or FLMP system could be used.

The serial driver and the network driver should operate on different cores. This avoids context switches between the two drivers and therefore reduce the likelihood losing characters while the network driver processes packets.
