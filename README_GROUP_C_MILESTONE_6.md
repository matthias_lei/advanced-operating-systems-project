Milestone 6: User-level Message Passing (UMP)
=============================================

Based on our simple URPC communication between both cores in the last week, we implemented UMP channels that can be used between arbitrary domains. The outputs of all programs running on both cores can be directly sent to our window server Hermes; new domains can be spawned from any core to run on any of the two cores. Communication chanels between clients and services on the same core are implemented using LMP, and with UMP between different cores.

# User-level Message Passing (UMP)
For a cross-core client<->server communication channel, a shared frame is mapped in both domains. We use 2 ring buffers for each channel. Fences are set properly to allow proper communication using the cache-coherency protocol without need to force data to be written to RAM. Between the monitors, we have 2 client<->server channels to allow each monitor being server and client of the other.

We designed the UMP channels to keep the offered functionality and the interfaces as close as possible to the existing LMP variant. This made the unification of the two message passing interfaces simpler. This also means that we now poll on UMP channels in the check_for_events() function and trigger events which then are delivered to the right channel. Every UMP channel consists of two ringbuffers, one for sending requests and the other one for receiving ACKs/replies. The role of the caller (client/server) determines which of the two buffers acts producer and consumer. In the same way as LMP, UMP channels will also fragment long messages into short messages. Those fragments will then be reassembled on the receiver side.

Capabilities cannot be sent directly between cores. Thus, we transfer the key information of e.g. frame capabilities (base address and size) by UMP between monitors and the receiving monitor forges again a valid capability based on that. This fresh capability is then e.g. sent to a freshly spawned program by LMP, which allows then the freshly spawned domain to establish direct cross core UMP communication channels to services on the other core. We still run 2 memory servers with half of total RAM on each of the cores.

Please note that we have described various details of the used communication by our ring buffer implementation in the README of milestone 5.


# Unified message passing stack
Thanks to our design, we could unify both communication methods, and actually also a third method (local channels for direct call of local functions) into one API: the defined RPC function interfaces as described in the `aos/aos_rpc.h` header file. Using vtables, the functions are mapped to the actual executing functions, either by UMP, LMP, or locally as configured automatially during initialization of the new domain in `aos/init.c` during the launch. This transparent interface really simplifies actual use of all these RPCs.


# Benchmarking
## Library
We use our own benchmarking library that was started earlier during CASP class. It's available on github with MIT license. It provides data collection and simple descriptive statistics functions including histograms.
URL: https://github.com/pirminschmid/benchmarkC

## Tests
We did benchmarking of both LMP and UMP. For this purpose we extended the RPC interface with a specialized benchmarking call. It allows sending a buffer of arbitrary size to the server and receiving a buffer of arbitrary size as a reply. We measured round trip time (RTT) and throughput using different configurations of message sizes and (2 rounds of) 'warm-up', i.e. first sending some messages before actually measuring, enabled or disabled. As the throughputs of LMP and UMP differ a lot, we chose different sizes for the large messages in the benchmarking. This benchmarking test is in the file ```lib/testing/tp06_ump.c```, where the constants defining the message sizes can be set manually. It can be run using our test suite "Artemis" either on core 0 for LMP or on core 1 for UMP benchmarking.

## Results
Using numerical and histogram output from the mentioned statistics library. The measured times correspond to the time between sending and receiving a reply, i.e. similar to the Round Trip Time (RTT) of a request.

### LMP
We used the following configurations:

1) small request (5 Bytes), small reply (5 Bytes); only RTT
2) large request (300 KiB), small reply (5 Bytes); RTT and throughput; additionally with results after warmup
3) small request (5 Bytes), large reply (300 KiB); RTT and throughput

Note:
The sizes of configuration 1 correspond to sizes of single messages. This results can thus be used to determine the actual latency (RTT/2) of the system.

Our results for LMP:

```
1)
statistics in us:
- robust:       median 153.8 us, IQR [153.5, 154.3], min 153.4, max 155.4, n=5, denominator=1, baseline=2
- normal dist.: 154.0 ± 0.8 us (mean ± sd), 95% CI for the mean [153.0, 155.0], min 153.4, max 155.4, n=5, denominator=1, baseline=2
(3 bins of size 1)
 153 [  4]: ********************************************************************************
 154 [  0]:
 155 [  1]: ********************
```

```
2)
statistics in us:
- robust:       median 1021039.8 us, IQR [1021026.7, 1110084.9], min 1021007.5, max 1377101.3, n=5, denominator=1, baseline=2
- normal dist.: 1092252.2 ± 159235.5 us (mean ± sd), 95% CI for the mean [894566.9, 1289937.5], min 1021007.5, max 1377101.3, n=5, denominator=1, b
aseline=2
(11 bins of size 32768)
1021007 - 1053774 [  4]: ********************************************************************************
1053775 - 1086542 [  0]:
1086543 - 1119310 [  0]:
1119311 - 1152078 [  0]:
1152079 - 1184846 [  0]:
1184847 - 1217614 [  0]:
1217615 - 1250382 [  0]:
1250383 - 1283150 [  0]:
1283151 - 1315918 [  0]:
1315919 - 1348686 [  0]:
1348687 - 1381454 [  1]: ********************

statistics in KiB/s:
- robust:       median 293.8 KiB/s, IQR [274.8, 293.8], min 217.9, max 293.8, n=5, denominator=1, baseline=0
- normal dist.: 278.6 ± 34.0 KiB/s (mean ± sd), 95% CI for the mean [236.4, 320.8], min 217.9, max 293.8, n=5, denominator=1, baseline=0
(10 bins of size 8)
 217 -  224 [  1]: ********************
 225 -  232 [  0]:
 233 -  240 [  0]:
 241 -  248 [  0]:
 249 -  256 [  0]:
 257 -  264 [  0]:
 265 -  272 [  0]:
 273 -  280 [  0]:
 281 -  288 [  0]:
 289 -  296 [  4]: ********************************************************************************
```

```
2) measurements after warmup
statistics in us:
- robust:       median 1021328.0 us, IQR [1021302.5, 1021361.9], min 1021295.9, max 1021403.9, n=5, denominator=1, baseline=2
- normal dist.: 1021336.1 ± 43.0 us (mean ± sd), 95% CI for the mean [1021282.6, 1021389.5], min 1021295.9, max 1021403.9, n=5, denominator=1, base
line=2
(14 bins of size 8)
1021295 - 1021302 [  1]: ********************
1021303 - 1021310 [  1]: ********************
1021311 - 1021318 [  0]:
1021319 - 1021326 [  0]:
1021327 - 1021334 [  1]: ********************
1021335 - 1021342 [  0]:
1021343 - 1021350 [  1]: ********************
1021351 - 1021358 [  0]:
1021359 - 1021366 [  0]:
1021367 - 1021374 [  0]:
1021375 - 1021382 [  0]:
1021383 - 1021390 [  0]:
1021391 - 1021398 [  0]:
1021399 - 1021406 [  1]: ********************

statistics in KiB/s:
- robust:       median 293.7 KiB/s, IQR [293.7, 293.7], min 293.7, max 293.7, n=5, denominator=1, baseline=0
- normal dist.: 293.7 ± 0.0 KiB/s (mean ± sd), 95% CI for the mean [293.7, 293.8], min 293.7, max 293.7, n=5, denominator=1, baseline=0
(1 bins of size 1)
 293 [  5]: ****************************************************************************************************
```

```
3)
statistics in us:
- robust:       median 1023197.0 us, IQR [1023171.8, 1023252.8], min 1023106.9, max 1023259.6, n=5, denominator=1, baseline=2
- normal dist.: 1023201.5 ± 60.8 us (mean ± sd), 95% CI for the mean [1023126.0, 1023277.0], min 1023106.9, max 1023259.6, n=5, denominator=1, base
line=2
(10 bins of size 16)
1023106 - 1023121 [  1]: ********************
1023122 - 1023137 [  0]:
1023138 - 1023153 [  0]:
1023154 - 1023169 [  0]:
1023170 - 1023185 [  0]:
1023186 - 1023201 [  2]: ****************************************
1023202 - 1023217 [  0]:
1023218 - 1023233 [  0]:
1023234 - 1023249 [  0]:
1023250 - 1023265 [  2]: ****************************************

statistics in KiB/s:
- robust:       median 293.2 KiB/s, IQR [293.2, 293.2], min 293.2, max 293.2, n=5, denominator=1, baseline=0
- normal dist.: 293.2 ± 0.0 KiB/s (mean ± sd), 95% CI for the mean [293.2, 293.2], min 293.2, max 293.2, n=5, denominator=1, baseline=0
(1 bins of size 1)
 293 [  5]: ****************************************************************************************************
```

Interpretation:
- good results for single messages, RTT ~150us
- bad scaling to large messages as throughput smaller than 300 KiB/s, i.e. sending 1 MiB already takes >3s
    - this is due to the total absence of parallelism during sending and receiving of messages
    - additionally every transmission over LMP executes two context switches, which costs time
    - this is particularly expensive in our system as we split a large message into several individually sent LMP messages
- outlier on client side: first malloc for large message sent, needs to get more memory from morecore
    - removed when using measurements after warm-up
    - other measurements do not differ significantly with warm-up enabled

### UMP
We used the following configurations:

1) small request (5 Bytes), small reply (5 Bytes); only latency
2) large request (8 MiB), small reply (5 Bytes); latency and throughput; additionally with results after warmup
3) small request (5 Bytes), large reply (8 MiB); latency and throughput

Note: all measurements after warm-up, without warmup similar observations as with LMP

Our results for UMP:
```
1)
statistics in us:
- robust:       median 40039.6 us, IQR [40039.4, 40040.0], min 40039.2, max 40041.1, n=5, denominator=1, baseline=2
- normal dist.: 40039.8 ± 0.8 us (mean ± sd), 95% CI for the mean [40038.9, 40040.8], min 40039.2, max 40041.1, n=5, denominator=1, baseline=2
(3 bins of size 1)
40039 [  4]: ********************************************************************************
40040 [  0]:
40041 [  1]: ********************
```

```
2)
statistics in us:
- robust:       median 280553.8 us, IQR [280553.4, 280554.2], min 280553.2, max 280554.8, n=5, denominator=1, baseline=2
- normal dist.: 280553.9 ± 0.6 us (mean ± sd), 95% CI for the mean [280553.1, 280554.6], min 280553.2, max 280554.8, n=5, denominator=1, baseline=2

(2 bins of size 1)
280553 [  3]: ************************************************************
280554 [  2]: ****************************************

statistics in KiB/s:
- robust:       median 29199.4 KiB/s, IQR [29199.4, 29199.4], min 29199.3, max 29199.5, n=5, denominator=1, baseline=0
- normal dist.: 29199.4 ± 0.1 KiB/s (mean ± sd), 95% CI for the mean [29199.3, 29199.5], min 29199.3, max 29199.5, n=5, denominator=1, baseline=0
(1 bins of size 1)
29199 [  5]: ****************************************************************************************************
```

```
3)
statistics in us:
- robust:       median 280582.4 us, IQR [280340.4, 280691.0], min 280254.2, max 280944.3, n=5, denominator=1, baseline=2
- normal dist.: 280551.3 ± 264.6 us (mean ± sd), 95% CI for the mean [280222.9, 280879.8], min 280254.2, max 280944.3, n=5, denominator=1, baseline
=2
(11 bins of size 64)
280254 - 280317 [  1]: ********************
280318 - 280381 [  1]: ********************
280382 - 280445 [  0]:
280446 - 280509 [  0]:
280510 - 280573 [  0]:
280574 - 280637 [  2]: ****************************************
280638 - 280701 [  0]:
280702 - 280765 [  0]:
280766 - 280829 [  0]:
280830 - 280893 [  0]:
280894 - 280957 [  1]: ********************

statistics in KiB/s:
- robust:       median 29196.4 KiB/s, IQR [29185.1, 29221.6], min 29158.8, max 29230.6, n=5, denominator=1, baseline=0
- normal dist.: 29199.7 ± 27.5 KiB/s (mean ± sd), 95% CI for the mean [29165.5, 29233.9], min 29158.8, max 29230.6, n=5, denominator=1, baseline=0
(10 bins of size 8)
29158 - 29165 [  1]: ********************
29166 - 29173 [  0]:
29174 - 29181 [  0]:
29182 - 29189 [  0]:
29190 - 29197 [  2]: ****************************************
29198 - 29205 [  0]:
29206 - 29213 [  0]:
29214 - 29221 [  1]: ********************
29222 - 29229 [  0]:
29230 - 29237 [  1]: ********************
```

Interpretation:

- RTT of UMP much higher compared to LMP: ~40ms vs. ~150us: Here we suspect a suboptimal polling algorithm at the moment, which will be fixed in the next day. Concretely, these 40 ms correspond to 2 time slices of the scheduler. We currently assume that our algorithm that quits polling if no message is readily available on UMP and re-registers itself again as receive handler will not be activated before the next time slice will activate the thread. We will adjust this to a more "aggressive" polling in a loop before re-registering the receive handler. This high latency also limits the output of text through stdio across cores.
- but throughput of UMP much higher than throughput of LMP: ~30 MiB/s vs. ~300 KiB/s
    - this comes from the present parallelism in sending and receiving UMP messages, as they are on different cores, the sending and the receiving can work in parallel
- performance of UMP strongly dominated by latency
    - we expect to be able to optimize our performance by not directly yielding the polling thread, when not observing a new message on the UMP channel


# Milestone
Our submission for milestone 6 can

Required:
- start applications that run on the second core from a program running on the BSP core*
- manage processes across cores**
- let processes on the second core access RPC services on the first core e.g. memory server, or serial console***

Optional:
- measure and demonstrate the performance of your communication implementation****

*This was already possible in milestone 5. Additionally our system can also spawn processes from core 1 on core 0.

**In our domain manager "Zeus" processes can be started on both cores and all running processes on both cores are listed.

***In our system e.g. all (non init) processes on both cores share the serial server Hermes (lying on core 0), to perform their console I/O operations.

****Done in section "Benchmarking" in this Readme.



# Bugs and fixes
These detected bugs have been reported separately

## Using Floating point values
While testing the benchmarking library, that uses integer and floating point types (double), we detected several bugs in the kernel that prevented the use of floating point values. With the already submitted patches and a workaround, doubles can be fully used on our system. An additional patch re floating point may be submitted next week.

## Timing
We detected several documentation / code mismatches on the topic of time measurements and scheduling of deferred events. While systime was often referred in ms or us, we detected that systime is actually provided in cycles that have a frequency of 300'000'000 Hz. Thus, 300 cycles / us. We extended the provided code to finally have auto-propagation of this systime_frequency from kernel into all domains (including init) via dispatcher struct and from there to the global variable defined in `systime.c`. An additional fix prevents skewing in periodic events. Thus, time measurements, sleep, and periodic events all work on our system. See test programs in the test suite.

## Look-ahead stack allocation
This is not actually a bug but an observation that is associated with the design decision of having self-paging also for regular stacks (either via malloc() or via paging regions). We have paging regions with a guard page for the stacks. In contrast to the exception stacks that we pre-allocate (by a memset at the time of initializaition), the regular stacks are left as self-paging to avoid unnecessary memory usage. This works well in general. But in rare occasions, a fresh page should be mapped while the dispatcher is disabled, which prevents the pagefault handler from running. We have written a tiny patch that guarantees enough stack to be available just before the dispatcher is being deactivated to allow all functions to run smoothly during this critical time: thus, look-ahead stack allocation.


# Some binaries

## Domain manager - Zeus
Basically "just" a user interface for the process service Demeter. It has the potential
to evolve into a shell in the future. It has been adjusted to spawn programs on both the
bootstrap and the application core according to the users choice.

## Test suite - Artemis
This program offers our established modular test suite system.

## Various test programs
In particular ```memeater```, ```stackeater```, ```threads``` and a collection of
test programs triggering specific exceptions, and testing Hermes.


# Usage
After booting, our low-level test suite allows launching some tests from earlier
milestones that are not (yet) available in Artemis. Upon exit of this menu (use #),
the services are initialized and the event handler loop starts listening to
incoming requests.

New with this version, our domain manager Zeus starts first instead of the test suite Artemis.
Artemis can be launched from within Zeus on both cores which allows running all tests on
core 0 and core 1.

So far, we have worked with a serial/terminal connection that had no echo. Echo is provided
by our programs on the Pandaboard. This may change with the upcoming version.
This is the current picocom setting: ```picocom -b 115200 -f n --omap crlf /dev/ttyUSB0```

## Windowing system: Hermes
Our system uses a windowing system that prevents interleaving of output of concurrently running
programs. Also, input is directed towards the program that is in focus. Since this input uses
a window specific buffer, additionol characters that were not readily requested by the program
can even be passed along if the program is out of focus. Please use ```TAB``` to switch between
the windows of various programs. See detailed description of the system in README of milestone 5.

## Arguments passing from modified usbboot to init domain
We have implemented a mechanism to pass arguments from the host to our Barrelfish
instance without the need for recompilation or modification of the final binary on
the disk that is used for transmission to the Pandaboard.
Usage with the patched usbboot: ```tools/bin/usbboot <image> --initargs <args...>```.
More detailed description in milestone 4.


Best regards,

Group C

Loris Diana
Matthias Lei
Jonathan Meier
Pirmin Schmid
